﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	[CustomEditor(typeof(BasicGuiToggle))]
	public class BasicGuiToggleEditor : BasicBaseEditor<BasicGuiToggle>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawDebugFields()
		{
			GUI.enabled = false;
			
			var toggle = TypedTarget.Toggle;
			var interactable = toggle != null && toggle.interactable;
			EditorGUILayout.LabelField("Interactable", interactable.ToString());
			EditorGUILayout.LabelField("Is On", TypedTarget.Value.ToString());

			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var toggle = serializedObject.FindProperty("_toggle");
			var boolSingle = serializedObject.FindProperty("_boolSingle");
			
			Space();
			SectionHeader($"{nameof(BasicGuiToggle).CamelCaseToHumanReadable()} Properties");

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(toggle);
			
			Space();
			BasicEditorGuiLayout.PropertyFieldCanBeNull(boolSingle);
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var valueChanged = serializedObject.FindProperty("_valueChanged");
			
			SectionHeader($"{nameof(BasicGuiToggle).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(valueChanged);
		}
		
	}
}