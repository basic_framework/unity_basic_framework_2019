﻿using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Toggle")]
	public class BasicGuiToggle : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private Toggle _toggle = default;

		[Tooltip("The bool single to synchronize the toggle with")]
		[SerializeField] private BoolSingle _boolSingle = default;

		[Tooltip("Raised when the toggle value changes")]
		[SerializeField] private BoolPlusUnityEvent _valueChanged = new BoolPlusUnityEvent();


		public Toggle Toggle => _toggle;

		public BoolSingle BoolSingle
		{
			get => _boolSingle;
			set
			{
				if (_boolSingle == value) return;
				var valueOld = _boolSingle;
				_boolSingle = value;
				BoolSingleUpdated(valueOld, value);
			}
		}

		public BoolPlusUnityEvent ValueChanged => _valueChanged;
		

		private bool _boolSingleNotNull;
		
		private bool _settingToggleValue;

		private bool _value;
		public bool Value
		{
			get => _value;
			private set
			{
				if (_value == value) return;
				_value = value;
				ValueUpdated(_value);
			}
		}

		
		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		private void Reset()
		{
			_toggle = GetComponentInChildren<Toggle>();
		}
		
		private void Awake()
		{
			_boolSingleNotNull = _boolSingle != null;
		}

		private void OnEnable()
		{
#if UNITY_EDITOR
			if(!Application.isPlaying) return;
#endif

			if (_boolSingleNotNull)
			{
				OnBoolSingleValueChanged(_boolSingle);
				_boolSingle.ValueChanged.Subscribe(OnBoolSingleValueChanged);
			}
			
			_toggle.onValueChanged.AddListener(OnToggleValueChanged);
		}

		private void OnDisable()
		{
#if UNITY_EDITOR
			if(!Application.isPlaying) return;
#endif
			
			if (_boolSingleNotNull)
			{
				_boolSingle.ValueChanged.Unsubscribe(OnBoolSingleValueChanged);
			}
			
			_toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
		}

		private void Start()
		{
#if UNITY_EDITOR
			if(!Application.isPlaying) return;
#endif
			
			_value = _toggle.isOn;
			ValueUpdated(_value);
		}

#if UNITY_EDITOR
		private void Update()
		{
			if (Application.isPlaying) return;
			if(_toggle == null) return;

			if (_boolSingle == null)
				OnToggleValueChanged(_toggle.isOn);
			else
				OnBoolSingleValueChanged(_boolSingle);
		}
#endif
		

		//***** callbacks *****\\

		private void OnBoolSingleValueChanged(ScriptableSingleBase<bool> boolSingle)
		{
			var isOn = boolSingle.Value;
			
			_settingToggleValue = true;
			_toggle.isOn = isOn;
			_settingToggleValue = false;
			
			Value = isOn;
		}
		
		private void OnToggleValueChanged(bool toggleValue)
		{
			if (_settingToggleValue) return;

			var boolSingleNotNull = _boolSingleNotNull;
			
#if UNITY_EDITOR
			if (!Application.isPlaying) 
				boolSingleNotNull = _boolSingle != null;
#endif
			
			// If syncing with bool single let single change the class Value
			if (boolSingleNotNull)
			{
				_boolSingle.Value = toggleValue;
			}
			else
			{
				Value = toggleValue;
			}
		}
		
		
		//***** property updates *****\\
		
		private void BoolSingleUpdated(BoolSingle valueOld, BoolSingle value)
		{
			_boolSingleNotNull = value != null;
			
			if(!enabled) return;
			
			if (valueOld != null)
			{
				valueOld.ValueChanged.Unsubscribe(OnBoolSingleValueChanged);
			}
			
			if (!_boolSingleNotNull) return;
			
			OnBoolSingleValueChanged(value);
			value.ValueChanged.Subscribe(OnBoolSingleValueChanged);
		}
		
		private void ValueUpdated(bool value)
		{
			_valueChanged.Raise(value);
		}
		
	}
}