﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components
{
	[CreateAssetMenu(
		menuName = "Basic Framework/Core/Gui/Text Style", 
		fileName = "text_style_")]
	public class TextStyleAsset : ScriptableAssetBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private Color _color = new Color(0.2f, 0.2f, 0.2f);
		[SerializeField] private Font _font = default;
		[SerializeField] private FontStyle _fontStyle = FontStyle.Normal;
		[SerializeField] private int _fontSize = 14;
		[SerializeField] private float _lineSpacing = 1f;

		public Color Color => _color;

		public Font Font => _font;

		public FontStyle FontStyle => _fontStyle;

		public int FontSize => _fontSize;

		public float LineSpacing => _lineSpacing;

		private readonly BasicEvent<TextStyleAsset> _valueChanged = new BasicEvent<TextStyleAsset>();
		public BasicEvent<TextStyleAsset> ValueChanged => _valueChanged;
		
	
		// ****************************************  METHODS  ****************************************\\

		public void EditorRaise()
		{
			if (!Application.isPlaying) return;
			if (!Application.isEditor) return;
			_valueChanged.Raise(this);
		}
		
	}
}