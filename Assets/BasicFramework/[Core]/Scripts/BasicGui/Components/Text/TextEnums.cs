﻿using System;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components
{
	[Flags]
	public enum TextStylePropertiesFlags
	{
		Color 		= 1 << 0,
		Font 		= 1 << 1,
		FontStyle 	= 1 << 2,
		FontSize 	= 1 << 3,
		LineSpacing = 1 << 4
	}
	
}