﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
    [CustomEditor(typeof(BasicGuiText))][CanEditMultipleObjects]
    public class BasicGuiTextEditor : BasicBaseEditor<BasicGuiText>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawPropertyFields()
        {
	        base.DrawPropertyFields();
			
	        var text = serializedObject.FindProperty("_text");
	        var stringSingle = serializedObject.FindProperty("_stringSingle");
	        var textStyleAsset = serializedObject.FindProperty("_textStyleAsset");
	        var appliedProperties = serializedObject.FindProperty("_appliedProperties");
	        
	        
	        Space();
	        SectionHeader($"{nameof(BasicGuiText).CamelCaseToHumanReadable()} Properties");
	        
	        
	        GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
	        
	        BasicEditorGuiLayout.PropertyFieldNotNull(text);
	        
	        Space();
	        BasicEditorGuiLayout.PropertyFieldCanBeNull(stringSingle);
			
	        Space();
	        BasicEditorGuiLayout.PropertyFieldCanBeNull(textStyleAsset);
	        
	        GUI.enabled = PrevGuiEnabled;
	        
	        EditorGUILayout.PropertyField(appliedProperties);
        }

    }
}