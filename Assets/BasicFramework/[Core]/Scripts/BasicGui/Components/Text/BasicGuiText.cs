﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Text")]
	public class BasicGuiText : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private Text _text = default;
		
		[Tooltip("The string single to synchronize the Text component to")]
		[SerializeField] private StringSingle _stringSingle = default;

		[Tooltip("The referenced text style asset")]
		[SerializeField] private TextStyleAsset _textStyleAsset = default;
		
		[EnumFlag][Tooltip("The text style properties to apply to the Text component")]
		[SerializeField] private TextStylePropertiesFlags _appliedProperties = (TextStylePropertiesFlags) ~0;

		
		public Text Text => _text;
		
		public StringSingle StringSingle
		{
			get => _stringSingle;
			set
			{
				if (_stringSingle == value) return;
				var valueOld = _stringSingle;
				_stringSingle = value;
				StringSingleUpdated(valueOld, value);
			}
		}

		public TextStyleAsset TextStyleAsset
		{
			get => _textStyleAsset;
			set
			{
				if (_textStyleAsset == value) return;
				var valueOld = _textStyleAsset;
				_textStyleAsset = value;
				TextStyleAssetUpdated(valueOld, value);
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		private void Reset()
		{
			_text = GetComponentInChildren<Text>();
		}

		private void OnEnable()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if (_stringSingle != null)
			{
				OnStringSingleValueChanged(_stringSingle);
				_stringSingle.ValueChanged.Subscribe(OnStringSingleValueChanged);
			}
			
			if (_textStyleAsset != null)
			{
				OnTextStyleChanged(_textStyleAsset);
				_textStyleAsset.ValueChanged.Subscribe(OnTextStyleChanged);
			}
		}
		
		private void OnDisable()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if (_stringSingle != null)
			{
				_stringSingle.ValueChanged.Unsubscribe(OnStringSingleValueChanged);
			}
			
			if (_textStyleAsset != null)
			{
				_textStyleAsset.ValueChanged.Unsubscribe(OnTextStyleChanged);
			}
		}

#if UNITY_EDITOR
		private void Update()
		{
			if (Application.isPlaying) return;
			if (_text == null) return;
			
			if (_textStyleAsset != null) 
				OnTextStyleChanged(_textStyleAsset);

			if (_stringSingle != null) 
				OnStringSingleValueChanged(_stringSingle);
		}
#endif

		
		//***** callbacks *****\\
		
		private void OnStringSingleValueChanged(ScriptableSingleBase<string> stringSingle)
		{
			_text.text = stringSingle.Value;
		}
		
		private void OnTextStyleChanged(TextStyleAsset textStyleAsset)
		{
			if ((_appliedProperties & TextStylePropertiesFlags.Color) == TextStylePropertiesFlags.Color) 		
				_text.color = textStyleAsset.Color;
			
			if ((_appliedProperties & TextStylePropertiesFlags.Font) == TextStylePropertiesFlags.Font) 			
				_text.font = textStyleAsset.Font;
			
			if ((_appliedProperties & TextStylePropertiesFlags.FontStyle) == TextStylePropertiesFlags.FontStyle) 	
				_text.fontStyle = textStyleAsset.FontStyle;
			
			if ((_appliedProperties & TextStylePropertiesFlags.FontSize) == TextStylePropertiesFlags.FontSize) 		
				_text.fontSize = textStyleAsset.FontSize;
			
			if ((_appliedProperties & TextStylePropertiesFlags.LineSpacing) == TextStylePropertiesFlags.LineSpacing) 	
				_text.lineSpacing = textStyleAsset.LineSpacing;
		}
		
		
		//***** property updated *****\\

		private void StringSingleUpdated(StringSingle valueOld, StringSingle value)
		{
			if (!enabled) return;
			
			if (valueOld != null)
			{
				valueOld.ValueChanged.Unsubscribe(OnStringSingleValueChanged);
			}

			if (value == null) return;
			
			OnStringSingleValueChanged(value);
			value.ValueChanged.Subscribe(OnStringSingleValueChanged);
		}
		
		private void TextStyleAssetUpdated(TextStyleAsset valueOld, TextStyleAsset value)
		{
			if (!enabled) return;
			
			if (valueOld != null)
			{
				valueOld.ValueChanged.Unsubscribe(OnTextStyleChanged);
			}

			if (value == null) return;
			
			OnTextStyleChanged(value);
			value.ValueChanged.Subscribe(OnTextStyleChanged);
		}

	}
}