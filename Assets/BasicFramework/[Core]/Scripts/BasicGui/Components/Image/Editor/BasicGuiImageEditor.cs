﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
    [CustomEditor(typeof(BasicGuiImage))][CanEditMultipleObjects]
    public class BasicGuiImageEditor : BasicBaseEditor<BasicGuiImage>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
			
            var image = serializedObject.FindProperty("_image");
            var tintColorSingle = serializedObject.FindProperty("_tintColorSingle");
            
            var imageSerializedObject = new SerializedObject(image.objectReferenceValue);
            var preserveAspect = imageSerializedObject.FindProperty("m_PreserveAspect");
            
            
            Space();
            SectionHeader($"{nameof(BasicGuiImage).CamelCaseToHumanReadable()} Properties");
            
            
            GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
            
            BasicEditorGuiLayout.PropertyFieldNotNull(image);
            
            Space();
            BasicEditorGuiLayout.PropertyFieldCanBeNull(tintColorSingle);
            
            GUI.enabled = PrevGuiEnabled;
            
            
            imageSerializedObject.Update();
            
            Space();
            EditorGUILayout.PropertyField(preserveAspect);
            
            imageSerializedObject.ApplyModifiedProperties();
        }
        
    }
}