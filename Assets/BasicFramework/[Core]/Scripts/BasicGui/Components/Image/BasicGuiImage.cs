﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
    [ExecuteAlways]
    [AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Image")]
    public class BasicGuiImage : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private Image _image = default;
		
        [Tooltip("The color single to synchronize the image tint with")]
        [SerializeField] private ColorSingle _tintColorSingle = default;


        public Image Image => _image;

        public ColorSingle TintColorSingle
        {
            get => _tintColorSingle;
            set
            {
                if (_tintColorSingle == value) return;
                var valueOld = _tintColorSingle;
                _tintColorSingle = value;
                TintColorSingleUpdated(valueOld, value);
            }
        }


        // ****************************************  METHODS  ****************************************\\

        //***** mono *****\\
        
        private void Reset()
        {
            _image = GetComponentInChildren<Image>();
        }

        private void OnEnable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif

            if (_tintColorSingle != null)
            {
                OnTintColorSingleChanged(_tintColorSingle);
                _tintColorSingle.ValueChanged.Subscribe(OnTintColorSingleChanged);
            }
        }
		
        private void OnDisable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            
            if (_tintColorSingle != null)
            {
                _tintColorSingle.ValueChanged.Unsubscribe(OnTintColorSingleChanged);
            }
        }
        

#if UNITY_EDITOR
        private void Update()
        {
            if (Application.isPlaying) return;
            if (_image == null) return;
			
            if (_tintColorSingle != null) 
                OnTintColorSingleChanged(_tintColorSingle);
        }
#endif
		
        
        //***** callbacks *****\\
        
        private void OnTintColorSingleChanged(ScriptableSingleBase<Color> colorSingle)
        {
            _image.color = colorSingle.Value;
//            SetTintColor(colorSingle.Value);
        }
        
        
        //***** property updated *****\\
        
        private void TintColorSingleUpdated(ColorSingle valueOld, ColorSingle value)
        {
            if (!enabled) return;
            
            if (valueOld != null)
            {
                valueOld.ValueChanged.Unsubscribe(OnTintColorSingleChanged);
            }

            if (value == null) return;
            
            OnTintColorSingleChanged(value);
            value.ValueChanged.Subscribe(OnTintColorSingleChanged);
        }
        
        
        //***** functions *****\\

        public void SetTintColorSingle(ColorSingle colorSingle)
        {
            TintColorSingle = colorSingle;
        }
		
        public void SetTintColor(Color color)
        {
            if (_tintColorSingle != null)
            {
                Debug.LogWarning($"{name}/{nameof(BasicGuiImage)} => " +
                                 $"Could not set Tint Color. It is being controlled by a ColorSingle");
                return;
            }
            
            _image.color = color;
        }

    }
}