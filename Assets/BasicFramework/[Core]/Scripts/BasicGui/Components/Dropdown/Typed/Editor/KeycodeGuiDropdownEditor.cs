﻿using BasicFramework.Core.BasicGui.Components.Editor;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.TypedDropdowns.Editor
{
	[CustomEditor(typeof(KeycodeGuiDropdown))]
	public class KeycodeGuiDropdownEditor : EnumGuiDropdownBaseEditor
	<
		KeyCode, 
		KeyCodeUnityEvent, 
		KeyCodeCompareUnityEvent, 
		KeycodeGuiDropdown
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}