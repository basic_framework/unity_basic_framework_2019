﻿using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	//[RequireComponent(typeof(Dropdown))]
	public abstract class TypedGuiDropdownBase<T, TUnityEvent, TArrayUnityEvent, TCompareUnityEvent> : MonoBehaviour
	where TUnityEvent : UnityEvent<T>
	where TArrayUnityEvent : UnityEvent<T[]>
	where TCompareUnityEvent : CompareValueUnityEventBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The dropdown component to use as reference")]
		[SerializeField] private Dropdown _dropdown = default;
		
		[Tooltip("Adds default value to the top of the list")]
		[SerializeField] private bool _addDefaultValue = default;

		[Tooltip("Disable selection of the default value. " +
		         "Useful if you don't want to allow selecting the default value after selecting another value")]
		[SerializeField] private bool _disableDefaultValue = default;
		
		[Tooltip("The options to use if no other options are set")]
		[SerializeField] private T[] _defaultOptions = new T[0];
		
		[Tooltip("Raised when the options in the dropdown are changed")]
		[SerializeField] private TArrayUnityEvent _optionsChanged = default;

		[Tooltip("Raised when the selected option changes")]
		[SerializeField] private TUnityEvent _selectedOptionChanged = default;
		
		[Tooltip("Raised when the selected option changes and the condition is met")]
		[SerializeField] private TCompareUnityEvent[] _selectedOptionChangedCompare = new TCompareUnityEvent[0];

		
		public Dropdown Dropdown => _dropdown;

		public TArrayUnityEvent OptionsChanged => _optionsChanged;

		public TUnityEvent SelectedOptionChanged => _selectedOptionChanged;

		
		private T _selectedOption;
		public T SelectedOption
		{
			get => _selectedOption;
			private set
			{
				if (Equals(_selectedOption, value)) return;
				_selectedOption = value;
				SelectedOptionUpdated(_selectedOption);
			}
		}
		
		private T[] _options;
		public T[] Options
		{
			get => _options;
			private set
			{
				if (_options.ArrayEquivalent(value)) return;
				_options = value.CloneArray();
				OptionsUpdated(_options);
			}
		}
		
		public int SelectedOptionIndex => _options?.IndexOf(_selectedOption) ?? -1;

		
		private readonly HashSet<T> _disabledOptions = new HashSet<T>();

		private bool _settingDropdownValue;
		

		// ****************************************  METHODS  ****************************************\\

		//***** Abstract/Virtual Methods *****\\
		
		protected virtual void EditDropdownOptionData(T value, Dropdown.OptionData optionData)
		{
			optionData.text = ReferenceEquals(value, null) ? "NULL" : value.ToString();
			optionData.image = null;
		}
		
		
		//***** Monobehaviour *****\\
		
		protected virtual void Reset()
		{
			_dropdown = GetComponentInChildren<Dropdown>();
		}

//		protected virtual void Awake()
//		{
//			_dropdown = GetComponent<Dropdown>();
//		}

		protected virtual void OnEnable()
		{
			_dropdown.onValueChanged.AddListener(DropdownValueChanged);
		}
		
		protected virtual void OnDisable()
		{
			_dropdown.onValueChanged.RemoveListener(DropdownValueChanged);
		}

		protected virtual void Start()
		{
			// Used to Enable/Disable options
			var eventTriggerEntry = new EventTrigger.Entry {eventID = EventTriggerType.PointerClick};
			eventTriggerEntry.callback.AddListener(data => { DropdownOpened((PointerEventData)data); });
			
			var eventTrigger = _dropdown.gameObject.AddComponent<EventTrigger>();
			eventTrigger.triggers.Add(eventTriggerEntry);

			if (_disableDefaultValue)
			{
				DisableOption(default);
			}
			
			if (_options != null) return;
			SetOptions(_defaultOptions);

			// Set Options will call SelectedOptionUpdated unless
			// the selected option is the default value
			if (!Equals(_selectedOption, default)) return;
			SelectedOptionUpdated(_selectedOption);
		}

		
		//***** Callbacks *****\\
		
		private void DropdownValueChanged(int index)
		{
			if (_settingDropdownValue) return;

			if (!IndexAvailable(index))
			{
				Debug.LogError($"{name} > {GetType().Name} => dropdown index is out of range");
				return;
			}

			SelectedOption = _options[index];
		}
		
		private void DropdownOpened(PointerEventData pointerEventData)
		{
			var dropdownToggles = _dropdown.GetComponentsInChildren<Toggle>();

			for (int i = 0; i < dropdownToggles.Length; i++)
			{
				dropdownToggles[i].interactable = !_disabledOptions.Contains(_options[i]);
			}
		}
		
		
		//***** Properties Updated *****\\

		private void OptionsUpdated(T[] options)
		{
			_optionsChanged.Invoke(options);
		}
		
		private void SelectedOptionUpdated(T selectedOption)
		{
			_settingDropdownValue = true;
			_dropdown.value = SelectedOptionIndex;
			_settingDropdownValue = false;
			
			_selectedOptionChanged.Invoke(selectedOption);

			foreach (var compareUnityEvent in _selectedOptionChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(selectedOption);
			}
		}
		
		
		//***** Setters *****\\

		public void SetSelectedOption(int index)
		{
			if (!IndexAvailable(index))
			{
				Debug.LogError($"{name} > {GetType().Name} => index \"{index}\" is out of range");
				return;
			}

			SelectedOption = _options[index];
		}
		
		public void SetSelectedOption(T option)
		{
			if (!OptionAvailable(option))
			{
				Debug.LogError($"{name} > {GetType().Name} => option \"{option}\" is not available");
				return;
			}

			SelectedOption = option;
		}

		public void SetOptions(T[] options)
		{
			SetOptions(options, _selectedOption);
		}

		public void SetOptions(T[] options, T selectedOption)
		{
			// Filter options list
			
			var filteredOptionsList = new List<T>();

			if (_addDefaultValue)
			{
				filteredOptionsList.Add(default);
			}
			
			if (options != null)
			{
				foreach (var option in options)
				{
					if (filteredOptionsList.Contains(option)) continue;
					filteredOptionsList.Add(option);
				}
			}

			options = filteredOptionsList.ToArray();
			
			if(_options != null && _options.ArrayEquivalent(options)) return;
			_options = options;
			
			
			// Update the shown values
			
			var dropdownOptions = _dropdown.options;

			for (int i = 0; i < options.Length; i++)
			{
				if (dropdownOptions.Count <= i)
				{
					dropdownOptions.Add(new Dropdown.OptionData());
				}

				var optionData = dropdownOptions[i];
				var value = _options[i];
			
				EditDropdownOptionData(value, optionData);
			}

			for (int i = dropdownOptions.Count - 1; i >= options.Length; i--)
			{
				dropdownOptions.RemoveAt(i);
			}

			_dropdown.RefreshShownValue();
			
			OptionsUpdated(_options);
			
			
			// Update the selected value
			
			if (_options.Length == 0)
			{
				SelectedOption = default;
				return;
			}
			
			var selectedIndex = Mathf.Max(0, _options.IndexOf(selectedOption)) ;
			SelectedOption = _options[selectedIndex];
		}
		
		
		//***** Getters *****\\

		public int GetIndexOfOption(T option)
		{
			if (_options == null)
			{
				SetOptions(_defaultOptions);
			}

			return _options.IndexOf(option);
		}

		public T GetOptionAtIndex(int index)
		{
			return IndexAvailable(index) ? _options[index] : default;
		}
		
		
		//***** Query *****\\
		
		public bool OptionAvailable(T option)
		{
			if (_options == null)
			{
				SetOptions(_defaultOptions);
			}
			
			return _options.Contains(option);
		}

		public bool IndexAvailable(int index)
		{
			if (_options == null)
			{
				SetOptions(_defaultOptions);
			}
			
			return index >= 0 && index < _options.Length;
		}
		
		
		//***** Enable/Disable Options *****\\
		
		public void EnableAllOptions()
		{
			if (_options == null)
			{
				SetOptions(_defaultOptions);
			}
			
			EnableOptions(_options);
		}
		
		public void EnableOptions(IEnumerable<T> options)
		{
			foreach (var option in options)
			{
				EnableOption(option);
			}
		}
		
		public void EnableOption(T option)
		{
			_disabledOptions.Remove(option);
		}
		
		public void DisableAllOptions()
		{
			if (_options == null)
			{
				SetOptions(_defaultOptions);
			}
			
			DisableOptions(_options);
		}
		
		public void DisableOptions(IEnumerable<T> options)
		{
			foreach (var option in options)
			{
				DisableOption(option);
			}
		}

		public void DisableOption(T option)
		{
			_disabledOptions.Add(option);
		}

#if UNITY_EDITOR
		//***** Editor *****\\

		public void EditorUpdate()
		{
			if (!Application.isEditor) 	return;
			if (Application.isPlaying) 	return;
			if (_dropdown == null) 		return;
			
			SetOptions(_defaultOptions);
		}
#endif
		
	}
}