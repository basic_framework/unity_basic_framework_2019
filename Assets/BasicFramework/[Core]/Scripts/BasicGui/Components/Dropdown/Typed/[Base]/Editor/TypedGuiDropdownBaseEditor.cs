﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	public abstract class TypedGuiDropdownBaseEditor<T, TUnityEvent, TArrayUnityEvent, TCompareUnityEvent, TTypedDropdown> : BasicBaseEditor<TTypedDropdown>
		where TUnityEvent : UnityEvent<T>
		where TArrayUnityEvent : UnityEvent<T[]>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
		where TTypedDropdown : TypedGuiDropdownBase<T, TUnityEvent, TArrayUnityEvent, TCompareUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _selectedOptionCompareReorderableList;
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_dropdown").isExpanded;
			set => serializedObject.FindProperty("_dropdown").isExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var selectedOptionChangedCompare = serializedObject.FindProperty("_selectedOptionChangedCompare");

			_selectedOptionCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList
					(serializedObject, selectedOptionChangedCompare, true);
		}
		
		protected override void DrawDebugFields()
		{
			var selectedOptionChanged = serializedObject.FindProperty("_selectedOptionChanged");

			var selectedOption = TypedTarget.SelectedOption;
			
			GUI.enabled = false;
			
			EditorGUILayout.LabelField("Selected Option", selectedOption == null ? "NULL" : TypedTarget.SelectedOption.ToString());
			EditorGUILayout.LabelField("Selected Option Index", TypedTarget.SelectedOptionIndex.ToString());

			selectedOptionChanged.isExpanded = EditorGUILayout.Foldout(selectedOptionChanged.isExpanded, "Options");
			
			var options = TypedTarget.Options;
			if (selectedOptionChanged.isExpanded && options != null)
			{
				for (var i = 0; i < options.Length; i++)
				{
					var option = options[i];
					EditorGUILayout.LabelField($"Option {i}", option == null ? "NULL" : option.ToString());
				}
			}
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var dropdown = serializedObject.FindProperty("_dropdown");
			var addDefaultValue = serializedObject.FindProperty("_addDefaultValue");
			var disableDefaultValue = serializedObject.FindProperty("_disableDefaultValue");
			var defaultOptions = serializedObject.FindProperty("_defaultOptions");
			
			Space();
			SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(dropdown);
			EditorGUILayout.PropertyField(addDefaultValue);
			EditorGUILayout.PropertyField(disableDefaultValue);
			EditorGUILayout.PropertyField(defaultOptions);
			
			if (EditorGUI.EndChangeCheck()) RaiseEditorUpdate = true;
		}

		protected override void DrawEventFields()
		{
			SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Events");
			
			var optionsChanged = serializedObject.FindProperty("_optionsChanged");
			var selectedOptionChanged = serializedObject.FindProperty("_selectedOptionChanged");
			
			EditorGUILayout.PropertyField(optionsChanged);
			EditorGUILayout.PropertyField(selectedOptionChanged);
			_selectedOptionCompareReorderableList.DoLayoutList();
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			TypedTarget.EditorUpdate();
		}
		
	}
}