﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.TypedDropdowns
{
	[AddComponentMenu("Basic Framework/Core/Gui/Components/Keycode Gui Dropdown")]
	public class KeycodeGuiDropdown : EnumGuiDropdownBase
	<
		KeyCode, 
		KeyCodeUnityEvent, 
		KeyCodeCompareUnityEvent
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}