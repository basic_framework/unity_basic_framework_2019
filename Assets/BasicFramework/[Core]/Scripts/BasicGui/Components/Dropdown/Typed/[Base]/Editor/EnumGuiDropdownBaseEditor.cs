﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	public abstract class EnumGuiDropdownBaseEditor<T, TUnityEvent, TCompareUnityEvent, TEnumTypedDropdown> : BasicBaseEditor<TEnumTypedDropdown> 
		where T : struct, Enum
		where TUnityEvent : UnityEvent<T> 
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
		where TEnumTypedDropdown : EnumGuiDropdownBase<T, TUnityEvent, TCompareUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private ReorderableList _selectedOptionCompareReorderableList;
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_dropdown").isExpanded;
			set => serializedObject.FindProperty("_dropdown").isExpanded = value;
		}


		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();

			RaiseEditorUpdate = true;
			
			var selectedOptionChangedCompare = serializedObject.FindProperty("_selectedOptionChangedCompare");

			_selectedOptionCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList
					(serializedObject, selectedOptionChangedCompare, true);
		}
		
		protected override void DrawDebugFields()
		{
			var selectedOptionChanged = serializedObject.FindProperty("_selectedOptionChanged");
			
			GUI.enabled = false;
			
			EditorGUILayout.LabelField("Selected Option", TypedTarget.SelectedOption.ToString());
			EditorGUILayout.LabelField("Selected Option Index", TypedTarget.SelectedOptionIndex.ToString());

			selectedOptionChanged.isExpanded = EditorGUILayout.Foldout(selectedOptionChanged.isExpanded, "Options");
			
			var options = TypedTarget.Options;
			if (selectedOptionChanged.isExpanded && options != null)
			{
				for (var i = 0; i < options.Length; i++)
				{
					var option = options[i];
					EditorGUILayout.LabelField($"Option {i}", option);
				}
			}
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var dropdown = serializedObject.FindProperty("_dropdown");
			var defaultSelectedOption = serializedObject.FindProperty("_defaultSelectedOption");
			
			Space();
//			SectionHeader($"{nameof(EnumGuiDropdownBase<T, TUnityEvent, TCompareUnityEvent>).CamelCaseToHumanReadable()} Properties");
			SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(dropdown);
			EditorGUILayout.PropertyField(defaultSelectedOption);

			if (EditorGUI.EndChangeCheck()) RaiseEditorUpdate = true;
		}
		
		protected override void DrawEventFields()
		{
			var selectedOptionChanged = serializedObject.FindProperty("_selectedOptionChanged");
			
			Space();
//			SectionHeader($"{nameof(EnumGuiDropdownBase<T, TUnityEvent, TCompareUnityEvent>).CamelCaseToHumanReadable()} Events");
			SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(selectedOptionChanged);
			_selectedOptionCompareReorderableList.DoLayoutList();
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			TypedTarget.EditorUpdate();
		}
	}
}