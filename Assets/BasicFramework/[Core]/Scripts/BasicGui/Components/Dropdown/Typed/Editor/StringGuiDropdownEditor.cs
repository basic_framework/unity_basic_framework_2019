﻿using BasicFramework.Core.BasicGui.Components.Editor;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.BasicGui.Components.TypedDropdowns.Editor
{
	[CustomEditor(typeof(StringGuiDropdown))]
	public class StringGuiDropdownEditor : TypedGuiDropdownBaseEditor
	<
		string, 
		StringUnityEvent, 
		StringArrayUnityEvent,
		StringCompareUnityEvent, 
		StringGuiDropdown
	> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}