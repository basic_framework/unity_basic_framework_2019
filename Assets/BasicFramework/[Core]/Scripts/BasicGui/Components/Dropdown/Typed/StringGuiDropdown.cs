﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;

namespace BasicFramework.Core.BasicGui.Components.TypedDropdowns
{
	public class StringGuiDropdown : TypedGuiDropdownBase
	<
		string,
		StringUnityEvent,
		StringArrayUnityEvent,
		StringCompareUnityEvent
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}