﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
//	[RequireComponent(typeof(Dropdown))]
	public abstract class EnumGuiDropdownBase<T, TUnityEvent, TCompareUnityEvent> : MonoBehaviour
	where T : struct, Enum
	where TUnityEvent : UnityEvent<T>
	where TCompareUnityEvent : CompareValueUnityEventBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The dropdown component to use as reference")]
		[SerializeField] private Dropdown _dropdown = default;

		[Tooltip("The default value that will be selected")]
		[SerializeField] private T _defaultSelectedOption = default;
		
		[Tooltip("Raised when the selected option changes")]
		[SerializeField] private TUnityEvent _selectedOptionChanged = default;
		
		[Tooltip("Raised when the selected option changes and the condition is met")]
		[SerializeField] private TCompareUnityEvent[] _selectedOptionChangedCompare = new TCompareUnityEvent[0];
		
		
		public Dropdown Dropdown => _dropdown;
		
		public TUnityEvent SelectedOptionChanged => _selectedOptionChanged;
		
		
		private T _selectedOption;
		public T SelectedOption
		{
			get => _selectedOption;
			private set
			{
				if (Equals(_selectedOption, value)) return;
				_selectedOption = value;
				SelectedOptionUpdated(_selectedOption);
			}
		}
		
		private string[] _options;
		public string[] Options => _options;

		public int SelectedOptionIndex => _options?.IndexOf(_selectedOption.ToString()) ?? -1;

		
		private readonly HashSet<string> _disabledOptions = new HashSet<string>();

		private bool _settingDropdownValue;
	
		// ****************************************  METHODS  ****************************************\\

		//***** Abstract/Virtual Methods *****\\
		
		protected virtual void EditDropdownOptionData(T value, Dropdown.OptionData optionData){}
		
		
		//***** Monobehaviour *****\\
		
		protected virtual void Reset()
		{
			_dropdown = GetComponentInChildren<Dropdown>();
		}

//		protected virtual void Awake()
//		{
//			_dropdown = GetComponent<Dropdown>();
//		}
		
		protected virtual void OnEnable()
		{
			_dropdown.onValueChanged.AddListener(DropdownValueChanged);
		}

		protected virtual void OnDisable()
		{
			_dropdown.onValueChanged.RemoveListener(DropdownValueChanged);
		}
		
		protected virtual void Start()
		{
			// Used to Enable/Disable options
			var eventTriggerEntry = new EventTrigger.Entry {eventID = EventTriggerType.PointerClick};
			eventTriggerEntry.callback.AddListener(data => { DropdownOpened((PointerEventData)data); });
			
			var eventTrigger = _dropdown.gameObject.AddComponent<EventTrigger>();
			eventTrigger.triggers.Add(eventTriggerEntry);
			
			if (_options != null) return;
			SetValues();
			SelectedOptionUpdated(_selectedOption);
		}
		
		
		//***** Callbacks *****\\
		
		private void DropdownValueChanged(int index)
		{
			if (_settingDropdownValue) return;
			
			if (index < 0 || index >= _options.Length)
			{
				Debug.LogError($"{name} > {GetType().Name} => dropdown index is out of range");
			}

			if (!Enum.TryParse(_options[index], out T enumOption))
			{
				Debug.LogError($"{name}>{GetType().Name} => Could not parse {_options[index]} to {typeof(T).Name} value");
				return;
			}
			
			SelectedOption = enumOption;
		}
		
		private void DropdownOpened(PointerEventData pointerEventData)
		{
			var dropdownToggles = _dropdown.GetComponentsInChildren<Toggle>();

			for (int i = 0; i < dropdownToggles.Length; i++)
			{
				dropdownToggles[i].interactable = !_disabledOptions.Contains(_options[i]);
			}
		}
		
		
		//***** Setters *****\\
		
		private void SetValues()
		{
			_options = Enum.GetNames(typeof(T));
			
			var optionDatas = new List<Dropdown.OptionData>();
			
			foreach (var option in _options)
			{
				var value = new Dropdown.OptionData(option);
				EditDropdownOptionData((T) Enum.Parse(typeof(T), option), value);
				optionDatas.Add(value);
			}

			_dropdown.options = optionDatas;

			_selectedOption = _defaultSelectedOption;
			_dropdown.value = _options.IndexOf(_selectedOption.ToString());
		}
		
		public void SetSelectedOption(T option)
		{
			if (_options == null || _options.Length == 0)
			{
				SetValues();
			}
			
			var optionIndex = _options?.IndexOf(option.ToString()) ?? -1;
			if (optionIndex < 0)
			{
				Debug.LogError($"{name} > {GetType().Name} => option \"{option}\" is not available");
				return;
			}
			
			SelectedOption = option;
		}
		
		
		//***** Properties Updated *****\\
	
		private void SelectedOptionUpdated(T selectedOption)
		{
			_settingDropdownValue = true;
			_dropdown.value = SelectedOptionIndex;
			_settingDropdownValue = false;
			
			_selectedOptionChanged.Invoke(selectedOption);

			foreach (var compareUnityEvent in _selectedOptionChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(selectedOption);
			}
		}
		
		
		//***** Enable/Disable Options *****\\
		
		public void EnableAllOptions()
		{
			EnableOptions(_options);
		}
		
		public void EnableOptions(IEnumerable<T> options)
		{
			foreach (var option in options)
			{
				EnableOption(option);
			}
		}

		public void EnableOptions(IEnumerable<string> options)
		{
			foreach (var option in options)
			{
				EnableOption(option);
			}
		}

		public void EnableOption(T option)
		{
			EnableOption(option.ToString());
		}

		public void EnableOption(string option)
		{
			_disabledOptions.Remove(option);
		}
		
		public void DisableAllOptions()
		{
			DisableOptions(_options);
		}
		
		public void DisableOptions(IEnumerable<T> options)
		{
			foreach (var option in options)
			{
				DisableOption(option);
			}
		}

		public void DisableOptions(IEnumerable<string> options)
		{
			foreach (var option in options)
			{
				DisableOption(option);
			}
		}

		public void DisableOption(T option)
		{
			DisableOption(option.ToString());
		}
		
		public void DisableOption(string option)
		{
			_disabledOptions.Add(option);
		}
		
#if UNITY_EDITOR
		//***** Editor *****\\
		
		public void EditorUpdate()
		{
			if (!Application.isEditor) return;
			if (Application.isPlaying) return;
			if (_dropdown == null) return;
			SetSelectedOption(_defaultSelectedOption);
		}
#endif
	}
}