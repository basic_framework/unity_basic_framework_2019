﻿namespace BasicFramework.Core.BasicGui.Components
{
	public enum SliderModes
	{
		TextOnly,
		Float,
		NormalizedFloat,
		RangedFloat
	}
}