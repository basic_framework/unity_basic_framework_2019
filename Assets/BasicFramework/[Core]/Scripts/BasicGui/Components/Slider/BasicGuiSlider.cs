﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Slider")]
	public class BasicGuiSlider : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private Slider _slider = default;

		[Tooltip("What type of slider is being used")]
		[SerializeField] private SliderModes _sliderMode = SliderModes.TextOnly;
		
		[Tooltip("The float single to be synced with the slider")]
		[SerializeField] private FloatSingle _floatSingle = default;

		[Tooltip("The normalized float single to sync with the slider")]
		[SerializeField] private NormalizedFloatSingle _normalizedFloatSingle = default;

		[Tooltip("The ranged float to sync with the slider.\n" +
		         "Note: The min and max values of the slider will be set according to the ranged float single")]
		[SerializeField] private RangedFloatSingle _rangedFloatSingle = default;
		
		
		[Tooltip("Raise the value changed string as a percentage i.e. between 0-100")]
		[SerializeField] private bool _raiseStringAsPercentage = default;
		
		[Range(0, 3)]
		[Tooltip("The number of decimal places the string representation of the value should have")]
		[SerializeField] private int _stringDecimalPlaces = 2;

		[Tooltip("Raised when the slider value changes with the string representation of the value.\n" +
		         "Note: Useful to display the value on the UI")]
		[SerializeField] private StringUnityEvent _valueChangedString = new StringUnityEvent();
		
		
		private float _value;
		public float Value
		{
			get => _value;
			private set
			{
				if (Math.Abs(_value - value) < Mathf.Epsilon) return;
				_value = value;
				ValueUpdated(_value);
			}
		}

		private bool _settingSliderValue;
		

		// ****************************************  METHODS  ****************************************\\
	
		private void Reset()
		{
			_slider = GetComponentInChildren<Slider>();
		}

		private void OnEnable()
		{
#if UNITY_EDITOR
			if(!Application.isPlaying) return;
#endif
			
			switch (_sliderMode)
			{
				case SliderModes.TextOnly:
					OnSliderValueChanged(_slider.value);
					break;
				case SliderModes.Float:
					OnFloatSingleValueChanged(_floatSingle);
					_floatSingle.ValueChanged.Subscribe(OnFloatSingleValueChanged);
					break;
				case SliderModes.NormalizedFloat:
					OnNormalizedFloatSingleValueChanged(_normalizedFloatSingle);
					_normalizedFloatSingle.ValueChanged.Subscribe(OnNormalizedFloatSingleValueChanged);
					break;
				case SliderModes.RangedFloat:
					OnRangedFloatSingleValueChanged(_rangedFloatSingle);
					_rangedFloatSingle.ValueChanged.Subscribe(OnRangedFloatSingleValueChanged);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			_slider.onValueChanged.AddListener(OnSliderValueChanged);
		}

		private void OnDisable()
		{
#if UNITY_EDITOR
			if(!Application.isPlaying) return;
#endif
			
			switch (_sliderMode)
			{
				case SliderModes.TextOnly:
					break;
				case SliderModes.Float:
					_floatSingle.ValueChanged.Unsubscribe(OnFloatSingleValueChanged);
					break;
				case SliderModes.NormalizedFloat:
					_normalizedFloatSingle.ValueChanged.Unsubscribe(OnNormalizedFloatSingleValueChanged);
					break;
				case SliderModes.RangedFloat:
					_rangedFloatSingle.ValueChanged.Unsubscribe(OnRangedFloatSingleValueChanged);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			_slider.onValueChanged.RemoveListener(OnSliderValueChanged);
		}
		
		private void Start()
		{
			if (!Application.isPlaying) return;
			ValueUpdated(_value);
		}

#if UNITY_EDITOR
		private void Update()
		{
			if (Application.isPlaying) return;
			if (_slider == null) return;

			switch (_sliderMode)
			{
				case SliderModes.TextOnly:
					OnSliderValueChanged(_slider.value);
					break;
				case SliderModes.Float:
					if (_floatSingle == null) return;
					OnFloatSingleValueChanged(_floatSingle);
					break;
				case SliderModes.NormalizedFloat:
					if (_normalizedFloatSingle == null) return;
					OnNormalizedFloatSingleValueChanged(_normalizedFloatSingle);
					break;
				case SliderModes.RangedFloat:
					if (_rangedFloatSingle == null) return;
					OnRangedFloatSingleValueChanged(_rangedFloatSingle);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
#endif

		
		//***** callbacks *****\\
		
		private void OnSliderValueChanged(float value)
		{
			if (_settingSliderValue) return;

			// If we are syncing with a single let the single update the Value
			switch (_sliderMode)
			{
				case SliderModes.TextOnly:
					Value = value;
					return;
				case SliderModes.Float:
					_floatSingle.Value = value;
					return;
				case SliderModes.NormalizedFloat:
					_normalizedFloatSingle.Value = new NormalizedFloat(value);
					return;
				case SliderModes.RangedFloat:
					_rangedFloatSingle.Value = new RangedFloat(_slider.minValue, value, _slider.maxValue);
					return;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		private void OnFloatSingleValueChanged(ScriptableSingleBase<float> floatSingle)
		{
			SetSliderValue(floatSingle.Value);
		}
		
		private void OnNormalizedFloatSingleValueChanged(ScriptableSingleBase<NormalizedFloat> normalizedFloatSingle)
		{
			SetSliderValue(normalizedFloatSingle.Value.Value, 0, 1);
		}
		
		private void OnRangedFloatSingleValueChanged(ScriptableSingleBase<RangedFloat> rangedFloatSingle)
		{
			var rangedFloat = rangedFloatSingle.Value;
			SetSliderValue(rangedFloat.Value, rangedFloat.Min, rangedFloat.Max);
		}
		
		
		//***** property updated *****\\
		
		private void ValueUpdated(float value)
		{
			var displayValue = value;
			
			if (_raiseStringAsPercentage)
			{
				displayValue = MathUtility.Map(value, _slider.minValue, _slider.maxValue, 0, 100);
			}
			
			displayValue = MathUtility.RoundToDecimalPlace(displayValue, _stringDecimalPlaces);

			_valueChangedString.Invoke(displayValue.ToString());
		}
		
		
		//***** functions *****\\
		
		private void SetSliderValue(float value)
		{			
			SetSliderValue(value, _slider.minValue, _slider.maxValue);
		}
		
		private void SetSliderValue(float value, float min, float max)
		{			
			_settingSliderValue = true;
			_slider.minValue = min;
			_slider.maxValue = max;
			_slider.value = value;
			_settingSliderValue = false;

			Value = value;
		}

	}
}