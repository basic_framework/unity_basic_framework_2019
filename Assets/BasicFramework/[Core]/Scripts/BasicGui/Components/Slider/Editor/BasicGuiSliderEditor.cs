﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	[CustomEditor(typeof(BasicGuiSlider))][CanEditMultipleObjects]
	public class BasicGuiSliderEditor : BasicBaseEditor<BasicGuiSlider>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;


		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawDebugFields()
		{
			GUI.enabled = false;

			EditorGUILayout.LabelField("Value", TypedTarget.Value.ToString());
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var slider = serializedObject.FindProperty("_slider");
			
			
			var sliderMode = serializedObject.FindProperty("_sliderMode");
			var floatSingle = serializedObject.FindProperty("_floatSingle");
			var normalizedFloatSingle = serializedObject.FindProperty("_normalizedFloatSingle");
			var rangedFloatSingle = serializedObject.FindProperty("_rangedFloatSingle");
			
			var raiseStringAsPercentage = serializedObject.FindProperty("_raiseStringAsPercentage");
			var stringDecimalPlaces = serializedObject.FindProperty("_stringDecimalPlaces");
			
			
			Space();
			SectionHeader($"{nameof(BasicGuiSlider).CamelCaseToHumanReadable()} Properties");
			
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			
			BasicEditorGuiLayout.PropertyFieldNotNull(slider);
			
			Space();
			EditorGUILayout.PropertyField(sliderMode);

			var sliderModeEnum =
				EnumUtility.EnumNameToEnumValue<SliderModes>(sliderMode.enumNames[sliderMode.enumValueIndex]);

			switch (sliderModeEnum)
			{
				case SliderModes.TextOnly:
					EditorGUILayout.LabelField("Text Only", "Not synced with anything, still raises string");
					break;
				case SliderModes.Float:
					BasicEditorGuiLayout.PropertyFieldNotNull(floatSingle);
					break;
				case SliderModes.NormalizedFloat:
					BasicEditorGuiLayout.PropertyFieldNotNull(normalizedFloatSingle);
					break;
				case SliderModes.RangedFloat:
					BasicEditorGuiLayout.PropertyFieldNotNull(rangedFloatSingle);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			
			GUI.enabled = PrevGuiEnabled;

			
			Space();
			EditorGUILayout.PropertyField(raiseStringAsPercentage);
			EditorGUILayout.PropertyField(stringDecimalPlaces);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var valueChangedString = serializedObject.FindProperty("_valueChangedString");


			Space();
			SectionHeader($"{nameof(BasicGuiSlider).CamelCaseToHumanReadable()} Events");
			
			
			EditorGUILayout.PropertyField(valueChangedString);
		}

		
	}
}