﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[RequireComponent(typeof(Button))]
	public abstract class TypedGuiButtonBase<T, TUnityEvent> :  MonoBehaviour
	where TUnityEvent : UnityEvent<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Button _button = default;

		[SerializeField] private T _value = default;
		
		[SerializeField] private TUnityEvent _clicked = default;

		
		public Button Button => _button;

		public TUnityEvent Clicked => _clicked;

		public T Value => _value;


		// ****************************************  METHODS  ****************************************\\

		protected virtual void Reset()
		{
			_button = GetComponent<Button>();
		}

		protected virtual void Awake()
		{
			_button = GetComponent<Button>();
		}

		protected virtual void OnEnable()
		{
			_button.onClick.AddListener(OnClick);
		}
		
		protected virtual void OnDisable()
		{
			_button.onClick.RemoveListener(OnClick);
		}
		
		private void OnClick()
		{
			_clicked.Invoke(_value);
		}
	}
}