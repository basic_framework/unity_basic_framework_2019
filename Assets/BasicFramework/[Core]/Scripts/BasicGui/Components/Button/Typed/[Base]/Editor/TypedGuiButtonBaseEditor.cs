﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	public abstract class TypedGuiButtonBaseEditor<T, TUnityEvent, TTypedGuiButton> : BasicBaseEditor<TTypedGuiButton>
		where TUnityEvent : UnityEvent<T>
		where TTypedGuiButton : TypedGuiButtonBase<T, TUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		protected override bool HasDebugSection => true;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			Space();
			SectionHeader("Typed Gui Button Base Debug");
			
			var button = serializedObject.FindProperty("_button");
			
			GUI.enabled = false;

			EditorGUILayout.PropertyField(button);
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			SectionHeader("Typed Gui Button Base Properties");
			
			var value = serializedObject.FindProperty("_value");
			
			EditorGUILayout.PropertyField(value);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			Space();
			SectionHeader("Typed Gui Button Base Events");
			
			var clicked = serializedObject.FindProperty("_clicked");
			
			EditorGUILayout.PropertyField(clicked);
		}
		
	}
}