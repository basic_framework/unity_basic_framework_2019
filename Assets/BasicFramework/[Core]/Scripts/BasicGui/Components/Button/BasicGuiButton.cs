﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Button")]
	public class BasicGuiButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private Button _button = default;
		
		
		//** holding **\\
		
		[Tooltip("How long to hold the button before raising hold timeout")]
		[SerializeField] private FloatSingleReference _holdDuration = new FloatSingleReference(1f);
		
		[Tooltip("If the pointer exits the button area should we stop considering it held")]
		[SerializeField] private bool _cancelHoldOnPointerExit = true;
		
		[Tooltip("Use real delta time to calculate hold progress")]
		[SerializeField] private bool _useUnscaledDeltaTime = default;
		
		
		[Tooltip("Raised when the button state changes")]
		[SerializeField] private BoolPlusUnityEvent _holdingChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the hold time changes, including when the button is released and the time returns to zero")]
		[SerializeField] private FloatUnityEvent _holdTimeChanged = new FloatUnityEvent();
		
		[Tooltip("Raised when the hold progress is changed")]
		[SerializeField] private NormalizedFloatPlusUnityEvent _holdProgressChanged = new NormalizedFloatPlusUnityEvent();
		
		
		
		public Button Button => _button;

		public bool UseUnscaledDeltaTime
		{
			get => _useUnscaledDeltaTime;
			set => _useUnscaledDeltaTime = value;
		}

		public bool CancelHoldOnPointerExit
		{
			get => _cancelHoldOnPointerExit;
			set => _cancelHoldOnPointerExit = value;
		}

		public float HoldDuration
		{
			get => _holdDuration.Value;
			set => _holdDuration.ConstantValue = value;
		}

		public BoolPlusUnityEvent HoldingChanged => _holdingChanged;
		
		public FloatUnityEvent HoldTimeChanged => _holdTimeChanged;
		
		public NormalizedFloatPlusUnityEvent HoldProgressChanged => _holdProgressChanged;


		public bool Interactable
		{
			get => _button.interactable;
			set => _button.interactable = value;
		}

		private bool _holding;
		public bool Holding
		{
			get => _holding;
			private set
			{
				if(_holding == value) return;
				_holding = value;
				HoldingUpdated(_holding);
			}
		}
		
		private float _holdTime;
		public float HoldTime
		{
			get => _holdTime;
			private set
			{
				if (Math.Abs(_holdTime - value) < Mathf.Epsilon) return;
				_holdTime = value;
				HoldTimeUpdated(_holdTime);
			}
		}
		
		private NormalizedFloat _holdProgress;
		public NormalizedFloat HoldProgress
		{
			get => _holdProgress;
			private set
			{
				if (_holdProgress == value) return;
				_holdProgress = value;
				HoldProgressUpdated(_holdProgress);
			}
		}

		
		// ****************************************  METHODS  ****************************************\\
		
		
		//***** mono *****\\
		
		private void Reset()
		{
			_button = GetComponentInChildren<Button>();
		}

		private void OnDisable()
		{
			EndHold();
		}

		private void Start()
		{
			HoldingUpdated(_holding);
			HoldTimeUpdated(_holdTime);
			HoldProgressUpdated(_holdProgress);
		}

		private void Update()
		{
			if (!_holding) return;

			if (!Interactable)
			{
				EndHold();
				return;
			}
			
			HoldTime += _useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
		}
		
		
		//***** callbacks *****\\

		public void OnPointerDown(PointerEventData eventData)
		{
			BeginHold();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			EndHold();
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (!_cancelHoldOnPointerExit) return;
			EndHold();
		}
		
		
		//***** functions *****\\
		
		//** holding **\\
		
		private void BeginHold()
		{
			if (_holding) return;
			if (!Interactable) return;
			Holding = true;
		}
		
		private void EndHold()
		{
			if (!_holding) return;
			Holding = false;
		}
		
		
		private void HoldingUpdated(bool holding)
		{
			_holdingChanged.Raise(holding);
			
			HoldTime = 0;
		}
		
		private void HoldTimeUpdated(float holdTime)
		{
			_holdTimeChanged.Invoke(holdTime);

			HoldProgress = Math.Abs(holdTime) < Mathf.Epsilon 
				? NormalizedFloat.Zero 
				: _holdDuration.Value <= 0 
					? NormalizedFloat.One 
					: new NormalizedFloat(holdTime / _holdDuration.Value);
		}
		
		private void HoldProgressUpdated(NormalizedFloat holdProgress)
		{
			_holdProgressChanged.Raise(holdProgress);
		}

	}
}