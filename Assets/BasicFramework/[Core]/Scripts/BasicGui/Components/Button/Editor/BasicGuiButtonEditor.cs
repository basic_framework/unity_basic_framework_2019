﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components
{
	[CustomEditor(typeof(BasicGuiButton))]
	public class BasicGuiButtonEditor : BasicBaseEditor<BasicGuiButton>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_button").isExpanded;
			set => serializedObject.FindProperty("_button").isExpanded = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawDebugFields()
		{
			GUI.enabled = false;
			
			EditorGUILayout.LabelField("Interactable", TypedTarget.Interactable.ToString());
			EditorGUILayout.LabelField("Holding", TypedTarget.Holding.ToString());
			EditorGUILayout.LabelField("Hold Time", TypedTarget.HoldTime.ToString());
			EditorGUILayout.Slider("Hold Progress", TypedTarget.HoldProgress.Value, 0, 1);
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var button = serializedObject.FindProperty("_button");
			
			var useUnscaledDeltaTime = serializedObject.FindProperty("_useUnscaledDeltaTime");
			var cancelHoldOnPointerExit = serializedObject.FindProperty("_cancelHoldOnPointerExit");
			var holdDuration = serializedObject.FindProperty("_holdDuration");
			
			
			Space();
			SectionHeader($"{nameof(BasicGuiButton).CamelCaseToHumanReadable()} Properties");
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(button);

			GUI.enabled = PrevGuiEnabled;

			Space();
			EditorGUILayout.PropertyField(holdDuration);
			EditorGUILayout.PropertyField(cancelHoldOnPointerExit);
			EditorGUILayout.PropertyField(useUnscaledDeltaTime);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var holdingChanged = serializedObject.FindProperty("_holdingChanged");
			var holdTimeChanged = serializedObject.FindProperty("_holdTimeChanged");
			var holdProgressChanged = serializedObject.FindProperty("_holdProgressChanged");
			
			
			Space();
			SectionHeader($"{nameof(BasicGuiButton).CamelCaseToHumanReadable()} Events");
			
			
			EditorGUILayout.PropertyField(holdingChanged);
			EditorGUILayout.PropertyField(holdProgressChanged);
			EditorGUILayout.PropertyField(holdTimeChanged);
		}
		
	}
}