﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	[CustomEditor(typeof(BasicGuiCanvas))]
	public class BasicGuiCanvasEditor : BasicBaseEditor<BasicGuiCanvas>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var rectTransformCanvas = serializedObject.FindProperty("_rectTransformCanvas");
			var canvasScaler = serializedObject.FindProperty("_canvasScaler");

			GUI.enabled = false;
			BasicEditorGuiLayout.PropertyFieldNotNull(rectTransformCanvas);
			BasicEditorGuiLayout.PropertyFieldNotNull(canvasScaler);
			
			SectionHeader("World Space");
			EditorGUILayout.LabelField(nameof(TypedTarget.PixelsPerUnit).CamelCaseToHumanReadable(), 
				TypedTarget.PixelsPerUnit.ToString());
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var canvas = serializedObject.FindProperty("_canvas");
			var rectTransformCanvas = serializedObject.FindProperty("_rectTransformCanvas");
			var canvasScaler = serializedObject.FindProperty("_canvasScaler");
			
			Space();
			SectionHeader($"{nameof(BasicGuiCanvas).CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			BasicEditorGuiLayout.PropertyFieldNotNull(canvas);
			if (EditorGUI.EndChangeCheck())
			{
				var canvasObject = canvas.objectReferenceValue as Canvas;
				
				rectTransformCanvas.objectReferenceValue = canvasObject == null
						? null
						: canvasObject.transform as RectTransform;

				canvasScaler.objectReferenceValue = canvasObject == null
					? null
					: canvasObject.gameObject.GetComponent<CanvasScaler>();
			}

			Space();
			DrawWorldSpaceCanvasFields();
		}

		
		private void DrawWorldSpaceCanvasFields()
		{
			
			var maxEdgeResolution = serializedObject.FindProperty("_maxEdgeResolution");
			var canvasSizeUnits = serializedObject.FindProperty("_canvasSizeUnits");
			var pixelsPerInch = serializedObject.FindProperty("_pixelsPerInch");
			var pixelsPerUnit = serializedObject.FindProperty("_pixelsPerUnit");
			
			var canvasObject = serializedObject.FindProperty("_canvas").objectReferenceValue as Canvas;
			var isWorldSpace = canvasObject != null && canvasObject.renderMode == RenderMode.WorldSpace;
			
			GUI.enabled = GUI.enabled && isWorldSpace;
			
			maxEdgeResolution.isExpanded = EditorGUILayout.Foldout(
				maxEdgeResolution.isExpanded, "World Space Canvas", BasicEditorStyles.BoldFoldout);

			if (!maxEdgeResolution.isExpanded)
			{
				GUI.enabled = PrevGuiEnabled;
				return;
			}

			EditorGUILayout.PropertyField(maxEdgeResolution);
			EditorGUILayout.PropertyField(canvasSizeUnits);
			EditorGUILayout.PropertyField(pixelsPerInch);

			pixelsPerUnit.floatValue = pixelsPerInch.floatValue * (1 / 0.0254f);
			

			var updateCanvas = GUILayout.Button("Update World Space Canvas"); 
			
			GUI.enabled = PrevGuiEnabled;
			
			if (!updateCanvas) return;
			if (canvasObject == null) return;
			if (canvasObject.renderMode != RenderMode.WorldSpace) return;

			var rectTransformCanvas = TypedTarget.RectTransformCanvas;
			var canvasScaler = TypedTarget.CanvasScaler;

			if (rectTransformCanvas == null) return;
			if (canvasScaler == null) return;
			
			Undo.RecordObject(rectTransformCanvas, "Update world space canvas");
			Undo.RecordObject(canvasScaler, "Update world space canvas");

			TypedTarget.UpdateCanvas();

//			RaiseEditorUpdate = true;
		}

//		protected override void OnRaiseEditorUpdate()
//		{
//			base.OnRaiseEditorUpdate();
//
//			//TODO: figure out how to get canvas to redraw components, can manually do by adjusting game window size
////			Debug.Log("Editor Update");
////			
////			Canvas.ForceUpdateCanvases();
////			var rectTransform = TypedTarget.RectTransformCanvas;
////			if (rectTransform == null) return;
////			LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
//			//UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
//		}
	}
}