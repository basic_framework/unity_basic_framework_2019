﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	public class BasicGuiCanvas : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Canvas _canvas = default;
		
		[SerializeField] private RectTransform _rectTransformCanvas = default;
		[SerializeField] private CanvasScaler _canvasScaler = default;

		
		//***** World Space *****\\
		
		[Tooltip("The reference resolution (in pixels) for the longest side (width or height) of the canvas.\n" +
		         "Keep this consistant across all canvases so that rect sizes and padding are uniform")]
		[SerializeField] private float _maxEdgeResolution = 1920;
		
		[Tooltip("The desired size (width and height) of the canvas in unity units (usually meters)")]
		[SerializeField] private Vector2 _canvasSizeUnits = Vector2.one;

		[Tooltip("The Pixel Density in Pixels Per Inch")]
		[SerializeField] private float _pixelsPerInch = 92;
		
		[Tooltip("The Pixel Density in Pixels Per Unit")]
		[SerializeField] private float _pixelsPerUnit = 12000; 
		
		
		public Canvas Canvas => _canvas;

		public RectTransform RectTransformCanvas => _rectTransformCanvas;

		public CanvasScaler CanvasScaler => _canvasScaler;


		public Vector2 CanvasSizePixels
		{
			get => _rectTransformCanvas.rect.size;
			set => _rectTransformCanvas.sizeDelta = value;
		}

		
		
		//***** World Space *****\\
		
		public float PixelsPerInch
		{
			get => _pixelsPerInch;
			set
			{
				_pixelsPerInch = value;
				_pixelsPerUnit = value * (1 / 0.0254f);
			}
		}

		public float PixelsPerUnit => _pixelsPerUnit;


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Reset()
		{
			_canvas = GetComponentInChildren<Canvas>();
			if (_canvas == null) return;
			_rectTransformCanvas = _canvas.transform as RectTransform;
			_canvasScaler = _canvas.GetComponent<CanvasScaler>();
		}

		private void OnValidate()
		{
			_pixelsPerUnit = _pixelsPerInch * (1 / 0.0254f);
		}


		//***** functions *****\\
		
		public void UpdateCanvas()
		{
			var renderMode = _canvas.renderMode;

			switch (renderMode)
			{
				case RenderMode.ScreenSpaceOverlay:
					break;
				case RenderMode.ScreenSpaceCamera:
					break;
				case RenderMode.WorldSpace:
					UpdateCanvasWorldSpace();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void UpdateCanvasWorldSpace()
		{
			var maxPixels = _maxEdgeResolution;
			var desiredPixelDensity = _pixelsPerUnit;
			
			var widthUnits = _canvasSizeUnits.x;
			var heightUnits = _canvasSizeUnits.y;

			var widthPixels = 0f;
			var heightPixels = 0f;
			
			if (widthUnits > heightUnits)
			{
				widthPixels = maxPixels;
				heightPixels = maxPixels * (heightUnits / widthUnits);
			}
			else
			{
				widthPixels = maxPixels * (widthUnits / heightUnits);
				heightPixels = maxPixels;
			}

			var canvasScale = widthUnits / widthPixels;

			var currentPixelDensity = widthPixels / widthUnits;

			var dynamicPixelsPerUnit = desiredPixelDensity / currentPixelDensity;
			
			// Set canvas size pixels
			CanvasSizePixels = new Vector2(widthPixels, heightPixels);
			
			// Set canvas scale (so canvas size in units matches desired size)
			_rectTransformCanvas.localScale = Vector3.one * canvasScale;

			// Set canvas scaler so canvas matches pixel density
			_canvasScaler.dynamicPixelsPerUnit = dynamicPixelsPerUnit;
		}
		
	}
}