﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Core.BasicGui.Components
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Gui/Basic Gui Input Field")]
	public class BasicGuiInputField : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private InputField _inputField = default;
		
		[Tooltip("The string single that will be referencedd by the input field")]
		[SerializeField] private StringSingle _stringSingle = default;
		
		[Tooltip("Update the string single any time the value is changed, otherwise only updates on end edit")]
		[SerializeField] private bool _updateOnValueChanged = default;
		

		public InputField InputField => _inputField;

		public StringSingle StringSingle
		{
			get => _stringSingle;
			set
			{
				if (_stringSingle == value) return;
				var valueOld = _stringSingle;
				_stringSingle = value;
				StringSingleUpdated(valueOld, value);
			}
		}
		
		public bool UpdateOnValueChanged => _updateOnValueChanged;


		private bool _stringSingleNotNull;
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\
		
		private void Reset()
		{
			_inputField = GetComponentInChildren<InputField>();
		}

		private void Awake()
		{
			_stringSingleNotNull = _stringSingle != null;
		}

		private void OnEnable()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif

			if (_stringSingleNotNull)
			{
				OnStringSingleValueChanged(_stringSingle);
				_stringSingle.ValueChanged.Subscribe(OnStringSingleValueChanged);
			}
			
			_inputField.onValueChanged.AddListener(OnInputFieldValueChanged);
			_inputField.onEndEdit.AddListener(OnInputFieldEndEdit);
		}
		
		private void OnDisable()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif

			if (_stringSingleNotNull)
			{
				_stringSingle.ValueChanged.Unsubscribe(OnStringSingleValueChanged);
			}
			
			_inputField.onValueChanged.RemoveListener(OnInputFieldValueChanged);
			_inputField.onEndEdit.RemoveListener(OnInputFieldEndEdit);
		}
		
#if UNITY_EDITOR
		private void Update()
		{
			if (Application.isPlaying) return;
			if (_inputField == null) return;

			if (_stringSingle != null)
			{
				OnStringSingleValueChanged(_stringSingle);
			}
		}
#endif
		
		
		//***** callbacks *****\\
		
		private void OnStringSingleValueChanged(ScriptableSingleBase<string> stringSingle)
		{
			_inputField.text = stringSingle.Value;
		}
		
		private void OnInputFieldValueChanged(string value)
		{
			if (!_updateOnValueChanged) return;
			if (!_stringSingleNotNull) return;
			_stringSingle.Value = value;
		}
		
		private void OnInputFieldEndEdit(string value)
		{
			if (!_stringSingleNotNull) return;
			_stringSingle.Value = value;
		}
		
		
		//***** property updates *****\\
		
		private void StringSingleUpdated(StringSingle valueOld, StringSingle value)
		{
			_stringSingleNotNull = value != null;

			if (!enabled) return;
			
			if (valueOld != null)
			{
				valueOld.ValueChanged.Unsubscribe(OnStringSingleValueChanged);
			}

			if (!_stringSingleNotNull) return;
			
			OnStringSingleValueChanged(value);
			value.ValueChanged.Subscribe(OnStringSingleValueChanged);
		}
		
	}
}