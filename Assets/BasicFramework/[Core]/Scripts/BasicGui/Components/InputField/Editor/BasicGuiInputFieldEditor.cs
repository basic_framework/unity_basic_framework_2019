﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Components.Editor
{
	[CustomEditor(typeof(BasicGuiInputField))][CanEditMultipleObjects]
	public class BasicGuiInputFieldEditor : BasicBaseEditor<BasicGuiInputField>
	{
	
		// **************************************** VARIABLES ****************************************\\
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var updateOnValueChanged = serializedObject.FindProperty("_updateOnValueChanged");
			var stringSingle = serializedObject.FindProperty("_stringSingle");
			var inputField = serializedObject.FindProperty("_inputField");

			
			Space();
			SectionHeader($"{nameof(BasicGuiInputField).CamelCaseToHumanReadable()} Properties");

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(inputField);
			
			Space();
			BasicEditorGuiLayout.PropertyFieldCanBeNull(stringSingle);

			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.PropertyField(updateOnValueChanged);
		}
	
	}
}