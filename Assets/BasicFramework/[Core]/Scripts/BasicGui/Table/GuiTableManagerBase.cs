﻿using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.BasicGui
{
	public abstract class GuiTableManagerBase<T, TGuiTableItem> : MonoBehaviour
		where TGuiTableItem : GuiTableItemBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private RectTransform _rectTransformParentTableItems = default;
		[SerializeField] private TGuiTableItem _prefabTableItem = default;
		
		
		private readonly List<TGuiTableItem> _tableItems = new List<TGuiTableItem>();


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		protected virtual void Awake()
		{
			ClearTableItems();
		}

		
		
		//***** functions *****\\

		protected void UpdateTableItems(IReadOnlyList<T> elements)
		{
			// Reduce table items number
			for (int i = _tableItems.Count - 1; i > elements.Count - 1; i--)
			{
				Destroy(_tableItems[i].gameObject);
				_tableItems.RemoveAt(i);
			}

			// Increase table items number
			for (int i = _tableItems.Count - 1; i < elements.Count - 1; i++)
			{
				var tableItem = Instantiate(_prefabTableItem, _rectTransformParentTableItems);
				_tableItems.Add(tableItem);
			}

			// Set and Update table item values
			for (int i = 0; i < elements.Count; i++)
			{
				_tableItems[i].Value = elements[i];
				_tableItems[i].UpdateTableItem();
			}
			
		}
		
		private void ClearTableItems()
		{
			_tableItems.Clear();
			
			foreach (Transform child in _rectTransformParentTableItems.transform)
			{
				var tableItem = child.GetComponent<TGuiTableItem>();
				if(tableItem == null) continue;
				Destroy(tableItem.gameObject);
			}
		}
		
	}
}