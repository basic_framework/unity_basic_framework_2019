﻿using System;
using UnityEngine;

namespace BasicFramework.Core.BasicGui
{
	public abstract class GuiTableItemBase<T> : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private T _value;
		public T Value
		{
			get => _value;
			set => _value = value;
		}

		


		// ****************************************  METHODS  ****************************************\\
		
		//***** functions *****\\

		public void UpdateTableItem()
		{
			OnUpdateTableItem(_value);
		}
		
		//***** abstract *****\\

		protected abstract void OnUpdateTableItem(T value);

		
		

	}
}