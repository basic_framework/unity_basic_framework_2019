﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Media
{
    [AddComponentMenu(
        "Basic Framework/Core/Media/Playback States Single Listener", 
        MediaAssetsMenuOrder.PLAYBACK_STATES_SINGLE)]
    public class PlaybackStatesSingleListener : SingleStructListenerBase
    <
        PlaybackStates, 
        PlaybackStatesSingle, 
        PlaybackStatesUnityEvent, 
        PlaybackStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}