﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Media
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Media/Playback States Single", 
        fileName = "single_playback_states_",
        order = MediaAssetsMenuOrder.PLAYBACK_STATES_SINGLE)
    ]
    public class PlaybackStatesSingle : SingleStructBase<PlaybackStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(PlaybackStatesSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class PlaybackStatesSingleReference : SingleStructReferenceBase<PlaybackStates, PlaybackStatesSingle>
    {
        public PlaybackStatesSingleReference()
        {
        }

        public PlaybackStatesSingleReference(PlaybackStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class PlaybackStatesSingleUnityEvent : UnityEvent<PlaybackStatesSingle>{}
	
    [Serializable]
    public class PlaybackStatesSingleArrayUnityEvent : UnityEvent<PlaybackStatesSingle[]>{}
    
}