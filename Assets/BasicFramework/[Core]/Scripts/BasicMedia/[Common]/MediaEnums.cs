﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.Media
{
	public enum PlaybackStates
	{
		Stopped,
		Loading,
		Playing,
		Paused,
		Seeking,
	}
	
	[Serializable]
	public class PlaybackStatesUnityEvent : UnityEvent<PlaybackStates>{}

	[Serializable]
	public class PlaybackStatesCompareUnityEvent : CompareValueUnityEventBase<PlaybackStates>{}
}