﻿using UnityEngine;

namespace BasicFramework.Core.Media
{
	public static class MediaAssetsMenuOrder 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private const int COMMON_START = 0;
		
		public const int PLAYBACK_STATES_SINGLE 	= COMMON_START;

		private const int AUDIO_START = 20;
		
		public const int AUDIO_ASSET 	= AUDIO_START;


		private const int VIDEO_START = 40;
		
		public const int VIDEO_ASSET 		= VIDEO_START;
		public const int VIDEO_ASSET_SINGLE	= VIDEO_START + 1;
		public const int VIDEO_ASSET_EVENT	= VIDEO_START + 2;


		// ****************************************  METHODS  ****************************************\\
	
	
	}
}