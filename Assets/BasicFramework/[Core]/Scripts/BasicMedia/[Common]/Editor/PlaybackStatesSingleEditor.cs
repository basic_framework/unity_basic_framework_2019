﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Media.Editor
{
    [CustomEditor(typeof(PlaybackStatesSingle))][CanEditMultipleObjects]
    public class PlaybackStatesSingleEditor : SingleStructBaseEditor<PlaybackStates, PlaybackStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(PlaybackStatesSingle))]
    public class PlaybackStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(PlaybackStatesSingleReference))]
    public class PlaybackStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}