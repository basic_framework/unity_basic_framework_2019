﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Media.Editor
{
    [CustomEditor(typeof(PlaybackStatesSingleListener))][CanEditMultipleObjects]
    public class PlaybackStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
        PlaybackStates, 
        PlaybackStatesSingle, 
        PlaybackStatesUnityEvent,
        PlaybackStatesCompareUnityEvent,
        PlaybackStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}