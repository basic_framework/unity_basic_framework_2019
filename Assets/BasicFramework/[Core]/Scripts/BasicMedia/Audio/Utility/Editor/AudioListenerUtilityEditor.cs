﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Media.Audio.Editor
{
	[CustomEditor(typeof(AudioListenerUtility))]
	public class AudioListenerUtilityEditor : BasicBaseEditor<AudioListenerUtility>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(AudioListener.volume), 
				MathUtility.RoundToDecimalPlace(AudioListener.volume, 2).ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			SectionHeader($"{nameof(AudioListenerUtility).CamelCaseToHumanReadable()} Properties");
			
			DrawDefaultInspector();
		}
	}
}