﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Media.Audio
{
	[AddComponentMenu("Basic Framework/Core/Media/Audio/Utility/Audio Listener Utility")]
	public class AudioListenerUtility : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		public void SetAudioListenerVolume(NormalizedFloat volume)
		{
			SetAudioListenerVolume(volume.Value);
		}
		
		public void SetAudioListenerVolume(float volume)
		{
			AudioListener.volume = Mathf.Clamp01(volume);
		}
	
	}
}