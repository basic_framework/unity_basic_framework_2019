﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace BasicFramework.Core.Media.Video
{
	[CreateAssetMenu(
		menuName = "Basic Framework/Core/Media/Video/Video Asset", 
		fileName = "video_", 
		order = MediaAssetsMenuOrder.VIDEO_ASSET)]
	public class VideoAsset : ScriptableAssetBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Header("Video")]
		[SerializeField] private VideoClip _videoClip = default;
		
		
		public string Title => AssetName;
		
		public Sprite Thumbnail => AssetIcon;
		
		public VideoClip VideoClip => _videoClip;


		public ulong FrameCount => _videoClip == null ? 0 : _videoClip.frameCount;

		public double FrameRate => _videoClip == null ? 0 : _videoClip.frameRate;
		
		/// <summary>The last frame of the video that was played</summary>
		private long _progressFrame;
		public long ProgressFrame
		{
			get => _progressFrame;
			set => _progressFrame = _videoClip == null || value > (long) _videoClip.frameCount ?  0 : value;
		}
		
		public float ProgressNormalised => FrameCount == 0 
			? 0 
			: Mathf.Clamp01((float) (_progressFrame / (double) FrameCount));

		
		/// <summary>The progress of the video clip from the last time it was played, in seconds</summary>
		public float ProgressSeconds => _videoClip == null ? 0 : (float) (_progressFrame / _videoClip.frameRate);

		/// <summary>The video length, in seconds</summary>
		public float LengthSeconds => _videoClip == null ? 0 : (float) (_videoClip.frameCount / _videoClip.frameRate);

		/// <summary>The remaining time left for the video, in seconds</summary>
		public float RemainingSeconds => LengthSeconds - ProgressSeconds;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			// In editor value is not reset when entering play mode
			if (!Application.isEditor) return;
			_progressFrame = 0;
		}
		
	}
	
	[Serializable]
	public class VideoAssetUnityEvent : UnityEvent<VideoAsset>{}

	[Serializable]
	public class VideoAssetCompareUnityEvent : CompareValueUnityEventBase<VideoAsset>{}
}