﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Media.Video
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Media/Video/Video Asset Single", 
        fileName = "single_video_",
        order = MediaAssetsMenuOrder.VIDEO_ASSET_SINGLE)
    ]
    public class VideoAssetSingle : SingleObjectBase<VideoAsset>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class VideoAssetSingleReference : SingleObjectReferenceBase<VideoAsset, VideoAssetSingle>
    {
        public VideoAssetSingleReference()
        {
        }

        public VideoAssetSingleReference(VideoAsset value)
        {
            _constantValue = value;
        }
    }
}