using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Media.Video.Editor
{
    [CustomEditor(typeof(VideoAssetSingleListener))][CanEditMultipleObjects]
    public class VideoAssetSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        VideoAsset, 
        VideoAssetSingle, 
        VideoAssetUnityEvent,
        VideoAssetCompareUnityEvent,
        VideoAssetSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}