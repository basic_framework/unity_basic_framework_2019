using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.Media.Video.Editor
{
    [CustomEditor(typeof(VideoAssetEvent))][CanEditMultipleObjects]
    public class VideoAssetEventEditor : ScriptableEventBaseEditor<VideoAsset, VideoAssetEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(VideoAssetEvent))]
    public class VideoAssetEventPropertyDrawer : ScriptableEventBasePropertyDrawer<VideoAsset, VideoAssetEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}