﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Media.Video.Editor
{
	[CustomEditor(typeof(BasicVideoPlayer))]
	public class BasicVideoPlayerEditor : BasicBaseEditor<BasicVideoPlayer>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_videoPlayer").isExpanded;
			set => serializedObject.FindProperty("_videoPlayer").isExpanded = value;
		}

		
		private ReorderableList _playbackStateCompareReorderableList;
		private ReorderableList _videoCompareReorderableList;

		private bool _editorRaise;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var playbackStateChangedCompare = serializedObject.FindProperty("_playbackStateChangedCompare");
			var videoChangedCompare = serializedObject.FindProperty("_videoChangedCompare");

			_playbackStateCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, playbackStateChangedCompare, true);
			
			_videoCompareReorderableList = 
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, videoChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			var videoPlayer = serializedObject.FindProperty("_videoPlayer");
			var debugVideo = serializedObject.FindProperty("_debugVideo");

			GUI.enabled = false;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(videoPlayer);
			
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.LabelField("Playback State", TypedTarget.PlaybackState.ToString());

			var video = TypedTarget.Video;
			var videoIsNull = video == null;
			EditorGUILayout.LabelField("Video", videoIsNull ? "NULL" : video.Title);

			var currentFrame = videoIsNull ? 0 : video.ProgressFrame;
			var endFrame = videoIsNull ? 0 : video.FrameCount;
			EditorGUILayout.LabelField("Frame", $"{currentFrame} / {endFrame}");
			
			var currentTime = MathUtility.RoundToDecimalPlace(videoIsNull ? 0 : video.ProgressSeconds, 2);
			var endTime = MathUtility.RoundToDecimalPlace(videoIsNull ? 0 : video.LengthSeconds, 2);
			EditorGUILayout.LabelField("Time", $"{currentTime} / {endTime}");

			var oldProgress = videoIsNull ? 0 : video.ProgressNormalised;

			GUI.enabled = PrevGuiEnabled && Application.isPlaying && !videoIsNull;

			var newProgress = EditorGUILayout.Slider("Progress", oldProgress, 0, 1);

			if (Math.Abs(oldProgress - newProgress) > Mathf.Epsilon)
			{
				TypedTarget.SeekToNormalised(newProgress);
			}

			GUI.enabled = PrevGuiEnabled;

			Space();
			BasicEditorGuiLayout.PropertyFieldNotNull(debugVideo);
			
			EditorGUILayout.BeginHorizontal();

			GUI.enabled = PrevGuiEnabled && Application.isPlaying && debugVideo.objectReferenceValue != null;
			
			if (GUILayout.Button("Play"))
			{
				TypedTarget.Play(TypedTarget.DebugVideo);
			}
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying && TypedTarget.PlaybackState != PlaybackStates.Stopped;
			
			if (GUILayout.Button("Pause"))
			{
				TypedTarget.PauseToggle();
			}
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying && TypedTarget.PlaybackState != PlaybackStates.Stopped;
			
			if (GUILayout.Button("Stop"))
			{
				TypedTarget.StopPlayback();
			}

			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();

			// TODO: VIDEO PLAYING, FRAME COUNT, TIME SECONDS
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			SectionHeader("Basic Video Player Properties");
			
			var startVideo = serializedObject.FindProperty("_startVideo");
			
			var loopPlayback = serializedObject.FindProperty("_loopPlayback");
			var resumeOnPlay = serializedObject.FindProperty("_resumeOnPlay");
			var stoppedColor = serializedObject.FindProperty("_stoppedColor");
			
			BasicEditorGuiLayout.PropertyFieldCanBeNull(startVideo);
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(loopPlayback);
			if (EditorGUI.EndChangeCheck()) _editorRaise = true;
			
			EditorGUILayout.PropertyField(resumeOnPlay);
			EditorGUILayout.PropertyField(stoppedColor);
			
			
			SectionHeader("Audio");
			
			var audioSource = serializedObject.FindProperty("_audioSource");
			BasicEditorGuiLayout.PropertyFieldCanBeNull(audioSource);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			Space();
			SectionHeader("Basic Video Player Events");
			
			var loopPlaybackChanged = serializedObject.FindProperty("_loopPlaybackChanged");
			var playbackLooped = serializedObject.FindProperty("_playbackLooped");
			var playbackEnded = serializedObject.FindProperty("_playbackEnded");
			var playbackStateChanged = serializedObject.FindProperty("_playbackStateChanged");
			
			var videoChanged = serializedObject.FindProperty("_videoChanged");
		
			EditorGUILayout.PropertyField(loopPlaybackChanged);
			EditorGUILayout.PropertyField(playbackLooped);
			
			Space();
			EditorGUILayout.PropertyField(playbackEnded);
			
			Space();
			EditorGUILayout.PropertyField(playbackStateChanged);
			_playbackStateCompareReorderableList.DoLayoutList();
			
			Space();
			EditorGUILayout.PropertyField(videoChanged);
			_videoCompareReorderableList.DoLayoutList();
		}


		protected override void AfterApplyingModifiedProperties()
		{
			base.AfterApplyingModifiedProperties();

			if (!_editorRaise) return;
			_editorRaise = false;
			foreach (var typedTarget in TypedTargets)
			{
				typedTarget.EditorRaise();
			}
		}
	}
}