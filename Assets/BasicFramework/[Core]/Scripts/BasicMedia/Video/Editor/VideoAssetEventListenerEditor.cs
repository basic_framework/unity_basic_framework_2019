using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.Media.Video.Editor
{
    [CustomEditor(typeof(VideoAssetEventListener))][CanEditMultipleObjects]
    public class VideoAssetEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        VideoAsset, 
        VideoAssetEvent, 
        VideoAssetUnityEvent,
        VideoAssetEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            var videoTitleResponse = serializedObject.FindProperty("_videoTitleResponse");
            
            EditorGUILayout.PropertyField(videoTitleResponse);
        }
    }
}