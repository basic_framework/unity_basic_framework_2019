using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Media.Video
{
    [AddComponentMenu(
	    "Basic Framework/Core/Media/Video/Video Asset Single Listener", 
	    MediaAssetsMenuOrder.VIDEO_ASSET_SINGLE)]
    public class VideoAssetSingleListener : SingleObjectListenerBase
    <
        VideoAsset, 
        VideoAssetSingle, 
        VideoAssetUnityEvent, 
        VideoAssetCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}