﻿using System;
using System.Collections;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace BasicFramework.Core.Media.Video
{
	[AddComponentMenu("Basic Framework/Core/Media/Video/Basic Video Player")]
	[RequireComponent(typeof(VideoPlayer))]
	public class BasicVideoPlayer : MonoBehaviour 
	{
	
		// Note: may be some issues with seeking
		
		// **************************************** VARIABLES ****************************************\\

		// Wait after seeking before checking if we looped otherwise causes issues because frame may not be up to date
		private const float AFTER_SEEK_DELAY = 0.2f;
		
		
		[SerializeField] private VideoPlayer _videoPlayer = default;

		[SerializeField] private VideoAsset _debugVideo = default;
		
		
		[Tooltip("Should the playback be looped")]
		[SerializeField] private bool _loopPlayback = default;
		
		[Tooltip("When calling Play should the video resume where it was stopped")]
		[SerializeField] private bool _resumeOnPlay = default;
		
		[Tooltip("The color to set the image to when the video is stopped")]
		[SerializeField] private Color _stoppedColor = Color.black;
		
		
		[Tooltip("Video to play on start, if null won't play anything")]
		[SerializeField] private VideoAsset _startVideo = default;

		[Tooltip("The audio source to use, if null will default to video player settings")]
		[SerializeField] private AudioSource _audioSource = default;
		
		
		[Tooltip("Raised when LoopPlayback is toggled")]
		[SerializeField] private BoolPlusUnityEvent _loopPlaybackChanged = new BoolPlusUnityEvent();
		
		
		[Tooltip("Raised when playback is looped, i.e. jumps from the last frame to the start frame")]
		[SerializeField] private UnityEvent _playbackLooped = new UnityEvent();
		
		[Tooltip("Raised when playback of this video reaches the last frame and stops")]
		[SerializeField] private UnityEvent _playbackEnded = new UnityEvent();
		
		[SerializeField] private PlaybackStatesUnityEvent _playbackStateChanged 
			= new PlaybackStatesUnityEvent();
		
		[SerializeField] private PlaybackStatesCompareUnityEvent[] _playbackStateChangedCompare 
			= new PlaybackStatesCompareUnityEvent[0];
		
		[SerializeField] private VideoAssetUnityEvent _videoChanged = new VideoAssetUnityEvent();
		
		[SerializeField] private VideoAssetCompareUnityEvent[] _videoChangedCompare = new VideoAssetCompareUnityEvent[0];

		
		public VideoPlayer VideoPlayer => _videoPlayer;
		
		public VideoAsset DebugVideo => _debugVideo;

		public bool LoopPlayback
		{
			get => _loopPlayback;
			set
			{
				if (_loopPlayback == value) return;
				_loopPlayback = value;
				LoopPlaybackUpdated(_loopPlayback);
			}
		}

		public bool ResumeOnPlay
		{
			get => _resumeOnPlay;
			set => _resumeOnPlay = value;
		}

		public Color StoppedColor
		{
			get => _stoppedColor;
			set => _stoppedColor = value;
		}
		
		public BoolPlusUnityEvent LoopPlaybackChanged => _loopPlaybackChanged;

		public UnityEvent PlaybackLooped => _playbackLooped;

		public UnityEvent PlaybackEnded => _playbackEnded;

		public PlaybackStatesUnityEvent PlaybackStateChanged => _playbackStateChanged;

		public VideoAssetUnityEvent VideoChanged => _videoChanged;

		
		// The video that is currently being referenced
		private VideoAsset _video;
		public VideoAsset Video
		{
			get => _video;
			private set
			{
				if(_video == value) return;
				_video = value;
				VideoUpdated(_video);
			}
		}
		
		private bool _videoIsNull;

		
		// The players current playback state
		private PlaybackStates _playbackState = PlaybackStates.Stopped;
		public PlaybackStates PlaybackState
		{
			get => _playbackState;
			set
			{
				if (_playbackState == value) return;
				_playbackState = value;
				PlaybackStateUpdated(_playbackState);
			}
		}
		
		
		private bool _pausePlaybackFlag;
		
		/// <summary>
		/// Is the video currently paused
		/// Note: this is also true when there is no video loaded. Use "PlaybackRunning" to check if a video is currently loaded. 
		/// </summary>
		public bool IsPaused => !_videoPlayer.isPlaying;
		
		
		private float _seekDelayTimer;
		private long _seekFrame;
		
		
		private bool _settingUpPlay;

		private bool _playbackCoroutineRunning;
		private Coroutine _playbackCoroutine;
		

		// ****************************************  METHODS  ****************************************\\

		//***** Monobehaviour *****\\

		private void OnValidate()
		{
			SetupVideoPlayer();
		}
		
		private void Awake()
		{
			SetupVideoPlayer();
		}

		private void OnEnable()
		{
			_videoPlayer.seekCompleted += SeekCompleted;
		}

		private void OnDisable()
		{
			_videoPlayer.seekCompleted -= SeekCompleted;
			
			StopPlayback();
		}
		
		private void Start()
		{
			if (_playbackState != PlaybackStates.Stopped) return;
			
			ClearImage(_stoppedColor);
			PlaybackStateUpdated(_playbackState);

			if (_startVideo == null) return;
			Play(_startVideo);
		}


		//***** Property Updated *****\\
		
		private void LoopPlaybackUpdated(bool loopPlayback)
		{
			_loopPlaybackChanged.Raise(loopPlayback);
		}
		
		private void PlaybackStateUpdated(PlaybackStates playbackState)
		{
			_playbackStateChanged.Invoke(playbackState);

			foreach (var compareUnityEvent in _playbackStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(playbackState);
			}
		}
		
		private void VideoUpdated(VideoAsset video)
		{
			_videoIsNull = video == null;
			
			_videoChanged.Invoke(video);

			foreach (var compareUnityEvent in _videoChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(video);
			}
		}

		
		//***** Controls *****\\
		
		public void Play(VideoAsset video)
		{
			if (!isActiveAndEnabled) return;
			if (_settingUpPlay) return;
			if (video == null) return;
			if (video.VideoClip == null) return;
			if (video == _video)
			{
				if (PlaybackState == PlaybackStates.Paused)
				{
					Pause(false);
					return;
				}

				if (PlaybackState == PlaybackStates.Playing)
				{
					return;
				}
			}

			_settingUpPlay = true;
			
			StopPlayback();

			Video = video;

			_playbackCoroutine = StartCoroutine(PlaybackCoroutine());

			_settingUpPlay = false;
		}

		public void StopPlayback()
		{
			StopPlayback(false);
		}

		private void StopPlayback(bool playbackEnded)
		{
			if (PlaybackState == PlaybackStates.Stopped) return;

			if (_playbackCoroutineRunning)
			{
				_playbackCoroutineRunning = false;
				StopCoroutine(_playbackCoroutine);
			}
			
			_videoPlayer.Stop();
			_videoPlayer.clip = null;
			
			ClearImage(_stoppedColor);

			PlaybackState = PlaybackStates.Stopped;
			
			if (playbackEnded)
			{
				_playbackEnded.Invoke();
			}
			
			if (PlaybackState != PlaybackStates.Stopped) return;
			Video = null;
		}
		
		public void PauseToggle()
		{
			_pausePlaybackFlag = !_pausePlaybackFlag;
		}

		public void Pause(bool pause)
		{
			_pausePlaybackFlag = pause;
		}
		
		public virtual void Restart()
		{
			if (!_playbackCoroutineRunning) return;
			SeekToFrame(0);
		}
		
		public void SeekToSeconds(float second)
		{
			if (!_playbackCoroutineRunning) return;

			var length = _video.LengthSeconds;
			
			if (Math.Abs(length) < Mathf.Epsilon) return;

			SeekToNormalised(second / length);
		}
		
		public void SeekToNormalised(float normalisedValue)
		{
			if (!_playbackCoroutineRunning) return;

			SeekToFrame((long) (Mathf.Clamp01(normalisedValue) * _video.FrameCount));
		}

		public void SeekToFrame(long frame)
		{
			if (!_playbackCoroutineRunning) return;
			
			// Clamp long
			if (frame < 0)
			{
				frame = 0;
			}
			else if(frame > (long)_video.FrameCount)
			{
				frame = (long) _video.FrameCount;
			}

			PlaybackState = PlaybackStates.Seeking;
			
			_videoPlayer.frame = frame;
			_seekFrame = frame;
		}

		private void SeekCompleted(VideoPlayer videoPlayer)
		{
			if (!_playbackCoroutineRunning) return;

			_video.ProgressFrame = _seekFrame;
			_seekDelayTimer = AFTER_SEEK_DELAY;
			
			PlaybackState = IsPaused ? PlaybackStates.Paused : PlaybackStates.Playing;
		}
		

		//***** PlayBack *****\\
		
		private IEnumerator PlaybackCoroutine()
		{
			_playbackCoroutineRunning = true;
			
			//***** Setup Audio *****\\

			if (_audioSource != null)
			{
				_videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
				_videoPlayer.SetTargetAudioSource(0, _audioSource);
			}


			//***** Load *****\\
			
			PlaybackState = PlaybackStates.Loading;
			
			_videoPlayer.clip = _video.VideoClip;

			_videoPlayer.Prepare();
			while (!_videoPlayer.isPrepared)
			{
				yield return null;
			}
			
			if (_resumeOnPlay)
			{
				SeekToFrame(_video.ProgressFrame);
			}
			

			//***** Play *****\\

			_videoPlayer.Play();
			while (!_videoPlayer.isPlaying)
			{
				yield return null;
			}

			PlaybackState = PlaybackStates.Playing;
			
			// Ensure the video does not start paused
			_pausePlaybackFlag = false;
			
			_video.ProgressFrame = _videoPlayer.frame;
			
			
			//***** Play loop *****\\
			
			while (_video.ProgressFrame != (long) _video.FrameCount || LoopPlayback)
			{
				//***** Loop Check *****\\
				
				if (_seekDelayTimer > 0)
				{
					_seekDelayTimer -= Time.deltaTime;
				}
				else
				{
					// Check if video looped
					if (_videoPlayer.frame - _video.ProgressFrame < 0 && PlaybackState != PlaybackStates.Seeking)
					{
						// Need to manually stop looping, if VideoPlayer isLooping variable toggled during play video stops
						if (!LoopPlayback)	break;

						_playbackLooped.Invoke();
					}
					
					_video.ProgressFrame = _videoPlayer.frame;
				}
				
				//***** Pause Check *****\\
				
				if (IsPaused != _pausePlaybackFlag)
				{
					// Store _pausePlayback value as it may change while trying to pause
					var pause = _pausePlaybackFlag;

					if (_pausePlaybackFlag)	_videoPlayer.Pause();
					else 					_videoPlayer.Play();

					// Wait until the paused state matches our required pause state
					while (IsPaused != pause)
					{
						yield return null;
					}
					
					PlaybackState = IsPaused ? PlaybackStates.Paused : PlaybackStates.Playing;
				}

				yield return null;
			}

			
			//***** Stopping the video *****\\

			// Reset to start frame so that we don't start at the end
			_video.ProgressFrame = 0;

			StopPlayback(true);
		}
		

		//***** Utility *****\\
		
		private void SetupVideoPlayer()
		{
			if (_videoPlayer == null)
			{
				_videoPlayer = GetComponent<VideoPlayer>();
			}

			_videoPlayer.playOnAwake = false;
			_videoPlayer.isLooping = true;
			_videoPlayer.waitForFirstFrame = true;
		}

		private void ClearImage(Color clearColor)
		{
			switch (_videoPlayer.renderMode)
			{
				case VideoRenderMode.CameraFarPlane:
					break;
				case VideoRenderMode.CameraNearPlane:
					break;
				case VideoRenderMode.RenderTexture:
					
					var renderTexture = _videoPlayer.targetTexture;
					if (renderTexture == null) return;
			
					var activeRenderTexture = RenderTexture.active;
					RenderTexture.active = renderTexture;
					GL.Clear(true, true, clearColor);
					RenderTexture.active = activeRenderTexture;
					
					break;
				
				case VideoRenderMode.MaterialOverride:
					break;
				case VideoRenderMode.APIOnly:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
		}

		
		//***** Editor *****\\
		public void EditorRaise()
		{
			if (!Application.isEditor) return;
			if (!Application.isPlaying) return;
			LoopPlaybackUpdated(_loopPlayback);
		}
		
	}
}