using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.Media.Video
{
    [AddComponentMenu("Basic Framework/Core/Media/Video/Video Asset Event Listener")]
    public class VideoAssetEventListener : ScriptableEventListenerBase<VideoAsset, VideoAssetEvent, VideoAssetUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private StringUnityEvent _videoTitleResponse = new StringUnityEvent();
        
        
        // ****************************************  METHODS  ****************************************\\

        protected override void OnEventRaised(VideoAsset arg0)
        {
            base.OnEventRaised(arg0);
            
            _videoTitleResponse.Invoke(arg0 == null ? "NULL" : arg0.Title);
        }
    }
}