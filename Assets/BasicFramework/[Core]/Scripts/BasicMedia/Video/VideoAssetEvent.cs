using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.Media.Video
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Media/Video/Video Asset Event", 
        fileName = "event_video_",
        order = MediaAssetsMenuOrder.VIDEO_ASSET_EVENT)
    ]
    public class VideoAssetEvent : ScriptableEventBase<VideoAsset>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void Raise(VideoAssetSingle videoAssetSingle)
        {
            Raise(videoAssetSingle.Value);
        }
        
    }
}