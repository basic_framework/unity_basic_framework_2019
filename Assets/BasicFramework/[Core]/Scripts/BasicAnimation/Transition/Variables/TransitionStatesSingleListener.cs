using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [AddComponentMenu("Basic Framework/Core/Animation/Transitioner/Transition States Single Listener")]
    public class TransitionStatesSingleListener : SingleStructListenerBase
    <
        TransitionStates, 
        TransitionStatesSingle, 
        TransitionStatesUnityEvent, 
        TransitionStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}