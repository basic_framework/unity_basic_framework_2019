using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Animation/Transitioner/Transition States Single", 
        fileName = "single_transition_states_")
    ]
    public class TransitionStatesSingle : SingleStructBase<TransitionStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(TransitionStatesSingle value)
        {
            Value = value.Value;
        }

//        public void SetValue(int value)
//        {
//            switch (value)
//            {
//                case 0:
//                    Value = TransitionStates.Zero;
//                    break;
//                
//                case 1:
//                    Value = TransitionStates.ToOne;
//                    break;
//                
//                case 2:
//                    Value = TransitionStates.One;
//                    break;
//                
//                case 3:
//                    Value = TransitionStates.ToZero;
//                    break;
//                
//                default:
//                    throw new ArgumentOutOfRangeException(nameof(value), $"{name} => could not convert {value} to TransitionState");
//            }
//        }

        public void SetValue(string value)
        {
            if (value.Equals("zero", StringComparison.OrdinalIgnoreCase))
            {
                Value = TransitionStates.Zero;
            }
            else if (value.Equals("toone", StringComparison.OrdinalIgnoreCase) 
                     || value.Equals("to one", StringComparison.OrdinalIgnoreCase))
            {
                Value = TransitionStates.ToOne;
            }
            else if (value.Equals("one", StringComparison.OrdinalIgnoreCase))
            {
                Value = TransitionStates.One;
            }
            else if (value.Equals("tozero", StringComparison.OrdinalIgnoreCase) 
                     || value.Equals("to zero", StringComparison.OrdinalIgnoreCase))
            {
                Value = TransitionStates.ToZero;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    nameof(value), $"{name} => could not convert {value} to TransitionState");
            }
        }

    }

    [Serializable]
    public class TransitionStatesSingleReference : SingleStructReferenceBase<TransitionStates, TransitionStatesSingle>
    {
        public TransitionStatesSingleReference()
        {
        }

        public TransitionStatesSingleReference(TransitionStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class TransitionStatesSingleUnityEvent : UnityEvent<TransitionStatesSingle>{}
	
    [Serializable]
    public class TransitionStatesSingleArrayUnityEvent : UnityEvent<TransitionStatesSingle[]>{}
    
}