using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [Serializable]
    public class Vector2TweenParameter : TweenParameterBase<Vector2, Vector2UnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public Vector2TweenParameter()
        {
            _inValue = Vector2.one;
            _outValue = Vector2.zero;
        }

        protected override Vector2 LerpUnclamped(Vector2 from, Vector2 to, float time)
        {
            return Vector2.LerpUnclamped(from, to, time);
        }
        
    }
}