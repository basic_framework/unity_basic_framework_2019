using System;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [Serializable]
    public abstract class TweenParameterBase<T, TUnityEvent> 
        where TUnityEvent : UnityEvent<T>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [SerializeField] protected T _outValue = default;
        [SerializeField] protected T _inValue = default;
		
        [SerializeField] private bool _useCurve = default;
        [SerializeField] private AnimationCurve _curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
        [SerializeField] private EasingTypes _easingType = EasingTypes.EaseInOutQuad;
		
        [SerializeField] private TUnityEvent _valueUpdated = default;
		
		
        public EasingTypes EasingType
        {
            get => _easingType;
            set => _easingType = value;
        }
		
        // Current tweened value
        private T _value;
        public T Value => _value;
		
        private NormalizedFloat _tweenProgress;
        public NormalizedFloat TweenProgress
        {
            get => _tweenProgress;
            set
            {
                //if (_tweenProgress == value) return;
                _tweenProgress = value;
                
                _value = LerpUnclamped(_outValue, _inValue, _useCurve 
                    ? _curve.Evaluate(value.Value) 
                    : Easing.Ease(_easingType, value.Value));
                
                _valueUpdated.Invoke(_value);
            }
        }

	
        // ****************************************  METHODS  ****************************************\\

        protected abstract T LerpUnclamped(T from, T to, float time);

    }
}