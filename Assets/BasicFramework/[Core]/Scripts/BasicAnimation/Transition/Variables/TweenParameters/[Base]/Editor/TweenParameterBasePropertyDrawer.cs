using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
	public abstract class TweenParameterBasePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded)
			{
				return;
			}

			var outValue = property.FindPropertyRelative("_outValue");
			var inValue = property.FindPropertyRelative("_inValue");
			var useCurve = property.FindPropertyRelative("_useCurve");
			var curve = property.FindPropertyRelative("_curve");
			var easingType = property.FindPropertyRelative("_easingType");
			var valueUpdated = property.FindPropertyRelative("_valueUpdated");

			TempRect.height = EditorGUI.GetPropertyHeight(outValue);
			EditorGUI.PropertyField(TempRect, outValue);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			TempRect.height = EditorGUI.GetPropertyHeight(inValue);
			EditorGUI.PropertyField(TempRect, inValue);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			TempRect.height = SingleLineHeight;
			EditorGUI.PropertyField(TempRect, useCurve);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (useCurve.boolValue)
			{
				TempRect.height = SingleLineHeight;
				EditorGUI.PropertyField(TempRect, curve);
				TempRect.y += TempRect.height + BUFFER_HEIGHT;
			}
			else
			{
				TempRect.height = SingleLineHeight;
				EditorGUI.PropertyField(TempRect, easingType);
				TempRect.y += TempRect.height + BUFFER_HEIGHT;
			}

			TempRect.height = EditorGUI.GetPropertyHeight(valueUpdated);
			EditorGUI.PropertyField(TempRect, valueUpdated);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded)
			{
				return height;
			}
			
			var outValue = property.FindPropertyRelative("_outValue");
			var inValue = property.FindPropertyRelative("_inValue");
			var valueUpdated = property.FindPropertyRelative("_valueUpdated");
			
			height += EditorGUI.GetPropertyHeight(outValue) + BUFFER_HEIGHT;
			height += EditorGUI.GetPropertyHeight(inValue) + BUFFER_HEIGHT;
			
			height += (SingleLineHeight + BUFFER_HEIGHT) * 2;
			
			height += EditorGUI.GetPropertyHeight(valueUpdated) + BUFFER_HEIGHT;
			
			return height;
		}
		
	}
}