using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [Serializable]
    public class FloatTweenParameter : TweenParameterBase<float, FloatUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public FloatTweenParameter()
        {
            _inValue = 1f;
            _outValue = 0f;
        }

        protected override float LerpUnclamped(float @from, float to, float time)
        {
            return Mathf.LerpUnclamped(from, to, time);
        }
        
    }
}