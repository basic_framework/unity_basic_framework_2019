using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [Serializable]
    public class Vector3TweenParameter : TweenParameterBase<Vector3, Vector3UnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public Vector3TweenParameter()
        {
            _inValue = Vector3.one;
            _outValue = Vector3.zero;
        }

        protected override Vector3 LerpUnclamped(Vector3 from, Vector3 to, float time)
        {
            return Vector3.LerpUnclamped(from, to, time);
        }
        
    }
}