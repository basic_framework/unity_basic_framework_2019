using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
    [Serializable]
    public class ColorTweenParameter : TweenParameterBase<Color, ColorUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public ColorTweenParameter()
        {
            _inValue = Color.white;
            _outValue = Color.black;
        }

        protected override Color LerpUnclamped(Color @from, Color to, float time)
        {
            return Color.LerpUnclamped(from, to, time);
        }
        
    }
}