using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
    [CustomEditor(typeof(TransitionStatesSingleListener))][CanEditMultipleObjects]
    public class TransitionStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    TransitionStates, 
	    TransitionStatesSingle, 
	    TransitionStatesUnityEvent,
	    TransitionStatesCompareUnityEvent,
	    TransitionStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}