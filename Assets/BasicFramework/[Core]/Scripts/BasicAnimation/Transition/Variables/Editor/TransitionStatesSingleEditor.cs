using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
    [CustomEditor(typeof(TransitionStatesSingle))][CanEditMultipleObjects]
    public class TransitionStatesSingleEditor : SingleStructBaseEditor<TransitionStates, TransitionStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
    }
	
    [CustomPropertyDrawer(typeof(TransitionStatesSingle))]
    public class TransitionStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(TransitionStatesSingleReference))]
    public class TransitionStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}