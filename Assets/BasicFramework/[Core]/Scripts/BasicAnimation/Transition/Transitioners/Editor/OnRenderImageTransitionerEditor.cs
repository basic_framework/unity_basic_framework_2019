﻿using UnityEditor;


namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
	[CustomEditor(typeof(OnRenderImageTransitioner))]
	public class OnRenderImageTransitionerEditor : TransitionerBaseEditor<OnRenderImageTransitioner>
	{
	
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			SectionHeader("On Render Image Transitioner");

			var transitionMaterial = serializedObject.FindProperty("_transitionMaterial");
			var transitionParameterName = serializedObject.FindProperty("_transitionParameterName");
			var transitionRange = serializedObject.FindProperty("_transitionRange");

			EditorGUILayout.PropertyField(transitionMaterial);
			EditorGUILayout.PropertyField(transitionParameterName);
			EditorGUILayout.PropertyField(transitionRange);
		}
		
	}
}