﻿using UnityEditor;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
	[CustomEditor(typeof(BasicTransitioner))]
	public class BasicTransitionerEditor : TransitionerBaseEditor<BasicTransitioner>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			
			SectionHeader("Basic Transitioner");

			var transitionMode = serializedObject.FindProperty("_transitionMode");
			
			EditorGUILayout.PropertyField(transitionMode);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			Space();
			
			SectionHeader("Basic Transitioner Events");

			var transitionProgressChanged = serializedObject.FindProperty("_transitionProgressChanged");
			
			EditorGUILayout.PropertyField(transitionProgressChanged);
		}
		
	}
}