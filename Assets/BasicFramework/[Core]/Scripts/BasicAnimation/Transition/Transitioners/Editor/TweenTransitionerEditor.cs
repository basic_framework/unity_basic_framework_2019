﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
	[CustomEditor(typeof(TweenTransitioner))]
	public class TweenTransitionerEditor : TransitionerBaseEditor<TweenTransitioner>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private ReorderableList _floatParameterReorderableList;
		private ReorderableList _vector2ParameterReorderableList;
		private ReorderableList _vector3ParameterReorderableList;
		private ReorderableList _colorParameterReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var floatTweenParameters = serializedObject.FindProperty("_floatTweenParameters");
			var vector2TweenParameters = serializedObject.FindProperty("_vector2TweenParameters");
			var vector3TweenParameters = serializedObject.FindProperty("_vector3TweenParameters");
			var colorTweenParameters = serializedObject.FindProperty("_colorTweenParameters");

			_floatParameterReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, floatTweenParameters, true);

			_vector2ParameterReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, vector2TweenParameters, true);
			
			_vector3ParameterReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, vector3TweenParameters, true);
			
			_colorParameterReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, colorTweenParameters, true);
		}

		protected override void DrawChildDebugFields()
		{
			base.DrawChildDebugFields();
			
			EditorGUILayout.LabelField("Is Waiting", TypedTarget.IsWaiting.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			
			SectionHeader("Tween Transitioner");

			var transitionMode = serializedObject.FindProperty("_transitionMode");
			var loopDelay = serializedObject.FindProperty("_loopDelay");
			var pingPongOutDelay = serializedObject.FindProperty("_pingPongOutDelay");
			var pingPongInDelay = serializedObject.FindProperty("_pingPongInDelay");

			EditorGUILayout.PropertyField(transitionMode);

			var transitionModeEnum =
				EnumUtility.EnumNameToEnumValue<TransitionModes>(
					transitionMode.enumNames[transitionMode.enumValueIndex]);

			switch (transitionModeEnum)
			{
				case TransitionModes.Straight:
					break;
				case TransitionModes.PingPong:
					EditorGUILayout.PropertyField(pingPongInDelay);
					EditorGUILayout.PropertyField(pingPongOutDelay);
					break;
				case TransitionModes.Loop:
					EditorGUILayout.PropertyField(loopDelay);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			Space();
			
			SectionHeader("Tween Transitioner Events");
			
			_floatParameterReorderableList.DoLayoutList();
			_vector2ParameterReorderableList.DoLayoutList();
			_vector3ParameterReorderableList.DoLayoutList();
			_colorParameterReorderableList.DoLayoutList();
		}
		
	}
}