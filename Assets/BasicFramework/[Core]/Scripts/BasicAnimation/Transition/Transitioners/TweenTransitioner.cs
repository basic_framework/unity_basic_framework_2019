﻿using System;
using System.Collections;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
	[AddComponentMenu("Basic Framework/Core/Animation/Transition/Tween Transitioner")]
	public class TweenTransitioner : TransitionerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The behaviour of the transitioner:\n" +
		         "Straight => transitions to the desired state then stops\n" +
		         "Ping Pong => transitions to the desired state, then immediately starts transitioning back\n" +
		         "Loop => transitions to the desired state, jumps to the other state (zero or one), starts transitioning again")]
		[SerializeField] private TransitionModes _transitionMode = TransitionModes.Straight;
		
		[Tooltip("How long to wait in Loop mode before starting again")]
		[SerializeField] private float _loopDelay = default;
		
		[Tooltip("How long to wait before ping ponging out")]
		[SerializeField] private float _pingPongOutDelay = default;
		
		[Tooltip("How long to wait before ping ponging in")]
		[SerializeField] private float _pingPongInDelay = default;
		
		[SerializeField] private FloatTweenParameter[] _floatTweenParameters = default;
		[SerializeField] private Vector2TweenParameter[] _vector2TweenParameters = default;
		[SerializeField] private Vector3TweenParameter[] _vector3TweenParameters = default;
		[SerializeField] private ColorTweenParameter[] _colorTweenParameters = default;
		
		
		public TransitionModes TransitionMode
		{
			get => _transitionMode;
			set => _transitionMode = value;
		}

		public float LoopDelay
		{
			get => _loopDelay;
			set => _loopDelay = value;
		}

		public float PingPongOutDelay
		{
			get => _pingPongOutDelay;
			set => _pingPongOutDelay = value;
		}

		public float PingPongInDelay
		{
			get => _pingPongInDelay;
			set => _pingPongInDelay = value;
		}

		private bool _waitCoroutineRunning;
		private Coroutine _waitCoroutine;

		public bool IsWaiting => _waitCoroutineRunning;
		public override bool IsTransitioning => base.IsTransitioning || _waitCoroutineRunning;
		
		
		// ****************************************  METHODS  ****************************************\\
		

		//***** Base Overrides *****\\
		
		public override void StopTransitioning()
		{
			base.StopTransitioning();
			StopWaiting();
		}
		
		public override void StopTransitioning(bool jumpToZero)
		{
			base.StopTransitioning(jumpToZero);
			StopWaiting();
		}
		
		protected override void OnTransitionProgressUpdated(NormalizedFloat transitionProgress)
		{
			foreach (var floatTweenParameter in _floatTweenParameters)
			{
				floatTweenParameter.TweenProgress = transitionProgress;
			}

			foreach (var vector2TweenParameter in _vector2TweenParameters)
			{
				vector2TweenParameter.TweenProgress = transitionProgress;
			}

			foreach (var vector3TweenParameter in _vector3TweenParameters)
			{
				vector3TweenParameter.TweenProgress = transitionProgress;
			}

			foreach (var colorTweenParameter in _colorTweenParameters)
			{
				colorTweenParameter.TweenProgress = transitionProgress;
			}
		}

		protected override void OnTransitionStarted()
		{
			base.OnTransitionStarted();
			StopWaiting();
		}

		protected override void OnTransitionEnded()
		{
			base.OnTransitionEnded();

			switch (_transitionMode)
			{
				case TransitionModes.Straight:
					break;
				
				case TransitionModes.PingPong:
					switch (TransitionState)
					{
						case TransitionStates.Zero:
						case TransitionStates.ToZero:
							StartWaiting(_pingPongInDelay);
							break;
						
						case TransitionStates.One:
						case TransitionStates.ToOne:
							StartWaiting(_pingPongOutDelay);
							break;
						
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				
				case TransitionModes.Loop:
					StartWaiting(_loopDelay);
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		
		//***** Waiting *****\\
		
		private void StartWaiting(float waitDuration)
		{
			if (_waitCoroutineRunning) return;
			
			_waitCoroutine = StartCoroutine(WaitCoroutine(waitDuration));
		}
		
		private void StopWaiting()
		{
			if (!_waitCoroutineRunning) return;
			_waitCoroutineRunning = false;

			if (_waitCoroutine != null)
			{
				StopCoroutine(_waitCoroutine);
			}
		}

		private IEnumerator WaitCoroutine(float waitDuration)
		{
			_waitCoroutineRunning = true;

			while (waitDuration > 0)
			{
				waitDuration -= UseUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
				yield return null;
			}
			
			_waitCoroutineRunning = false;

			switch (_transitionMode)
			{
				case TransitionModes.Straight:
					break;
				
				case TransitionModes.PingPong:
					switch (TransitionState)
					{
						case TransitionStates.Zero:
						case TransitionStates.ToZero:
							TransitionToOne();
							break;
						
						case TransitionStates.One:
						case TransitionStates.ToOne:
							TransitionToZero();
							break;
						
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				
				case TransitionModes.Loop:
					switch (TransitionState)
					{
						case TransitionStates.Zero:
						case TransitionStates.ToZero:
							JumpToState(false);
							TransitionToZero();
							break;
						
						case TransitionStates.One:
						case TransitionStates.ToOne:
							JumpToState(true);
							TransitionToOne();
							break;
						
						default:
							throw new ArgumentOutOfRangeException();
					}
					
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}