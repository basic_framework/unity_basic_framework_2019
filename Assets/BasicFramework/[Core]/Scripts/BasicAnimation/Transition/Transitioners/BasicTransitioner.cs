﻿using System;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
	[AddComponentMenu("Basic Framework/Core/Animation/Transition/Basic Transitioner")]
	public class BasicTransitioner : TransitionerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The behaviour of the transitioner:\n" +
		         "Straight => transitions to the desired state then stops\n" +
		         "Ping Pong => transitions to the desired state, then immediately starts transitioning back\n" +
		         "Loop => transitions to the desired state, jumps to the other state (zero or one), starts transitioning again")]
		[SerializeField] private TransitionModes _transitionMode = TransitionModes.Straight;

		[Tooltip("Raised when the transition progress is changed")]
		[SerializeField] private NormalizedFloatPlusUnityEvent _transitionProgressChanged = default;


		public TransitionModes TransitionMode => _transitionMode;

		public NormalizedFloatPlusUnityEvent TransitionProgressChanged => _transitionProgressChanged;


		// ****************************************  METHODS  ****************************************\\

		protected override void OnTransitionProgressUpdated(NormalizedFloat transitionProgress)
		{
			_transitionProgressChanged.Raise(transitionProgress);
		}

		protected override void OnTransitionEnded()
		{
			base.OnTransitionEnded();

			switch (_transitionMode)
			{
				case TransitionModes.Straight:
					break;
				
				case TransitionModes.PingPong:
					switch (TransitionState)
					{
						case TransitionStates.Zero:
						case TransitionStates.ToZero:
							TransitionToOne();
							break;
						
						case TransitionStates.One:
						case TransitionStates.ToOne:
							TransitionToZero();
							break;
						
						default:
							throw new ArgumentOutOfRangeException();
					}
					break;
				
				case TransitionModes.Loop:
					switch (TransitionState)
					{
						case TransitionStates.Zero:
						case TransitionStates.ToZero:
							JumpToState(false);
							TransitionToZero();
							break;
						
						case TransitionStates.One:
						case TransitionStates.ToOne:
							JumpToState(true);
							TransitionToOne();
							break;
						
						default:
							throw new ArgumentOutOfRangeException();
					}
					
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}