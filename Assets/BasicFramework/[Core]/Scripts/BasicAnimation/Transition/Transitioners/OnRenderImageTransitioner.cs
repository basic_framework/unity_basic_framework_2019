﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
	[RequireComponent(typeof(Camera))]
	[AddComponentMenu("Basic Framework/Core/Animation/Transition/On Render Image Transitioner")]
	public class OnRenderImageTransitioner : TransitionerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The material that will be used to modify the image in some way. " +
		         "It have a parameter transitioned")]
		[SerializeField] private Material _transitionMaterial = default;

		[Tooltip("The name of the shader parameter that will be transitioned")]
		[SerializeField] private string _transitionParameterName = "_Progress";
		
		[Tooltip("The range of the parameter that will be transitioned")]
		[SerializeField] private FloatRange _transitionRange = new FloatRange(0, 1);
		
		
		private Camera _camera;
		
		private int _transitionParameterId;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		//***** Monobehaviour Callbacks *****\\
		
		private void Reset()
		{
			_camera = GetComponent<Camera>();

			if (!_camera) return;
			
			_camera.clearFlags = CameraClearFlags.Nothing;
			_camera.cullingMask = 0;
			_camera.allowMSAA = false;
			_camera.depth = 50;
			_camera.stereoTargetEye = StereoTargetEyeMask.Both;
			
			_transitionParameterId = Shader.PropertyToID(_transitionParameterName);
		}
		
		protected override void OnValidate()
		{
			base.OnValidate();
			_transitionParameterId = Shader.PropertyToID(_transitionParameterName);
		}

		private void Awake()
		{
			if (!Application.isPlaying) return;
			_camera = GetComponent<Camera>();
			_transitionParameterId = Shader.PropertyToID(_transitionParameterName);
		}

		private void OnRenderImage(RenderTexture src, RenderTexture dest)
		{
			if (_transitionMaterial == null)
			{
				Graphics.Blit(src, dest);
				return;
			}

			Graphics.Blit(src, dest, _transitionMaterial);
		}
		
		
		//***** Transition Overrides *****\\

		protected override void OnTransitionStateUpdated(TransitionStates transitionState)
		{
			switch (transitionState)
			{
				case TransitionStates.Zero:
					_camera.enabled = false;
					break;
				case TransitionStates.ToOne:
				case TransitionStates.One:
				case TransitionStates.ToZero:
					_camera.enabled = true;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(transitionState), transitionState, null);
			}
		}

		protected override void OnTransitionProgressUpdated(NormalizedFloat transitionProgress)
		{
			if (!Application.isPlaying && _transitionMaterial == null) return;
			_transitionMaterial.SetFloat(_transitionParameterId, _transitionRange.NormalizedToRangedValue(transitionProgress.Value));
		}
		
	}
}