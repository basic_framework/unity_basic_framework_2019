﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
	
	public enum TransitionStates
	{
		Zero,
		ToOne,
		One,
		ToZero
	}
	
	[Serializable]
	public class TransitionStatesUnityEvent : UnityEvent<TransitionStates>{}

	[Serializable]
	public class TransitionStatesCompareUnityEvent : CompareValueUnityEventBase<TransitionStates>{}


	public enum TransitionModes
	{
		Straight,
		PingPong,
		Loop
	}
	
	[Serializable]
	public class TransitionModesUnityEvent : UnityEvent<TransitionModes>{}

	[Serializable]
	public class TransitionModesCompareValueUnityEvent : CompareValueUnityEventBase<TransitionModes>{}

}