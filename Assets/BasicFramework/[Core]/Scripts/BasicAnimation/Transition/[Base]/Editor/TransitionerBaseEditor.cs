﻿using System;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner.Editor
{
	public abstract class TransitionerBaseEditor<T> : BasicBaseEditor<T>
	where T : TransitionerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_transitionInEditor").isExpanded;
			set => serializedObject.FindProperty("_transitionInEditor").isExpanded = value;
		}
		
		
		private ReorderableList _stateChangeCompareReorderableList;
	
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var transitionStateChangedCompare = serializedObject.FindProperty("_transitionStateChangedCompare");

			_stateChangeCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, transitionStateChangedCompare, true);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawTransitionBaseFields();
		}

		protected override void DrawDebugFields()
		{
			var transitionInEditor = serializedObject.FindProperty("_transitionInEditor");

			var transitionProgressEditor = serializedObject.FindProperty("_transitionProgressEditor");

			EditorGUILayout.PropertyField(transitionInEditor);
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying && transitionInEditor.boolValue;
			EditorGUILayout.PropertyField(transitionProgressEditor);
			
			
			GUI.enabled = false;
			EditorGUILayout.Slider("Raw Transition Progress", TypedTarget.RawTransitionProgress.Value, 0f, 1f);
			EditorGUILayout.Slider("Transition Progress", TypedTarget.TransitionProgress.Value, 0f, 1f);
			
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.LabelField("Transition State", TypedTarget.TransitionState.ToString());
			EditorGUILayout.LabelField("Is Transitioning", TypedTarget.IsTransitioning.ToString());
			
			DrawChildDebugFields();

			EditorGUILayout.BeginHorizontal();

			var localEnable = PrevGuiEnabled && Application.isPlaying && TypedTarget.enabled;
			
			GUI.enabled = localEnable &&
			              TypedTarget.TransitionState != TransitionStates.Zero &&
			              TypedTarget.TransitionState != TransitionStates.ToZero; 
			
			if (GUILayout.Button("ToZero"))
			{
				TypedTarget.TransitionToZero();
			}
			
			
			GUI.enabled = localEnable &&
			              TypedTarget.TransitionState != TransitionStates.One &&
			              TypedTarget.TransitionState != TransitionStates.ToOne;
			
			if (GUILayout.Button("ToOne"))
			{
				TypedTarget.TransitionToOne();
			}
			
			GUI.enabled = localEnable;
			
			if (GUILayout.Button("Toggle"))
			{
				TypedTarget.TransitionToggle();
			}
			
			
			GUI.enabled = localEnable && TypedTarget.IsTransitioning;
			
			if (GUILayout.Button("Stop"))
			{
				TypedTarget.StopTransitioning();
			}
			
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();
		}

		protected virtual void DrawChildDebugFields(){}
		
		protected void DrawTransitionBaseFields()
		{
			SectionHeader("Transition Base");
			
			var useUnscaledDeltaTime = serializedObject.FindProperty("_useUnscaledDeltaTime");
			var startTransitionState = serializedObject.FindProperty("_startTransitionState");
			var defaultDuration = serializedObject.FindProperty("_defaultDuration");
			
			
			var updateMode = serializedObject.FindProperty("_updateMode");
			var specifyFramerate = serializedObject.FindProperty("_specifyFramerate");
			var discreteFrameRate = serializedObject.FindProperty("_discreteFrameRate");
			var discreteStepCount = serializedObject.FindProperty("_discreteStepCount");
			
			EditorGUILayout.PropertyField(useUnscaledDeltaTime);
			EditorGUILayout.PropertyField(startTransitionState);
			EditorGUILayout.PropertyField(defaultDuration);

			Space();
			EditorGUILayout.PropertyField(updateMode);

			var updateModeEnum =
				EnumUtility.EnumNameToEnumValue<DataIntervalModes>(updateMode.enumNames[updateMode.enumValueIndex]);
			
			switch(updateModeEnum)
			{
				case DataIntervalModes.Continuous:
					GUI.enabled = false;
					break;
				case DataIntervalModes.Discrete:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			EditorGUILayout.PropertyField(specifyFramerate);

			EditorGUILayout.PropertyField(specifyFramerate.boolValue ? discreteFrameRate : discreteStepCount);

			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawEventFields()
		{
			SectionHeader("Transition Base Events");
			
			var transitionStateChanged = serializedObject.FindProperty("_transitionStateChanged");

			EditorGUILayout.PropertyField(transitionStateChanged);
			_stateChangeCompareReorderableList.DoLayoutList();
		}
	}
}