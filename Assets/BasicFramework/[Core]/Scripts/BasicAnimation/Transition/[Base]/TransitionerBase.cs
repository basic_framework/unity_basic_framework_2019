﻿using System;
using System.Collections;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Transitioner
{
	[ExecuteAlways]
	public abstract class TransitionerBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("Test the transitioner in the editor by moving the progress slider")]
		[SerializeField] private bool _transitionInEditor = default;
		
		[Tooltip("Edit Time Only. The progress of the transitioner")]
		[SerializeField] private NormalizedFloat _transitionProgressEditor = default;
		
		
		[Tooltip("Avoid using unscaled time, unless you need to transition while Time.timeScale is 0")]
		[SerializeField] private bool _useUnscaledDeltaTime = default;
		
		[Tooltip("The state to start the transitioner on")]
		[SerializeField] private TransitionStates _startTransitionState = TransitionStates.Zero;
		
		[Tooltip("The default time it takes to go from Zero to One and vice versa. " +
		         "This duration is used if one is not specified when starting the transition")]
		[SerializeField] private FloatSingleReference _defaultDuration = new FloatSingleReference(0.5f);
		
		
		[Tooltip("Should the transitioner update continously or discretely")]
		[SerializeField] private DataIntervalModes _updateMode = DataIntervalModes.Continuous;

		[Tooltip("Should the discrete resolution be specified in terms of framerate")]
		[SerializeField] private bool _specifyFramerate = default;

		[Tooltip("The frame rate that will be maintained discretely")]
		[SerializeField] private float _discreteFrameRate = 30f;
		
		[Tooltip("The number of discrete steps that will be taken between the zero to one range")]
		[SerializeField] private int _discreteStepCount = 10;
		
		

		[Tooltip("Raised when the transition state is changed")]
		[SerializeField] private TransitionStatesUnityEvent _transitionStateChanged = new TransitionStatesUnityEvent();
		
		[Tooltip("Raised when the transition state is changed and the condition is met")]
		[SerializeField] private TransitionStatesCompareUnityEvent[] _transitionStateChangedCompare = new TransitionStatesCompareUnityEvent[0];

		
		public bool UseUnscaledDeltaTime
		{
			get => _useUnscaledDeltaTime;
			set => _useUnscaledDeltaTime = value;
		}

		public float DefaultDuration
		{
			get => _defaultDuration.Value;
			set => _defaultDuration.ConstantValue = value;
		}
		
		
		public DataIntervalModes IntervalMode
		{
			get => _updateMode;
			set => _updateMode = value;
		}

		public bool SpecifyFramerate
		{
			get => _specifyFramerate;
			set => _specifyFramerate = value;
		}

		public float DiscreteFrameRate
		{
			get => _discreteFrameRate;
			set => _discreteFrameRate = value;
		}

		public int DiscreteStepCount
		{
			get => _discreteStepCount;
			set => _discreteStepCount = value;
		}
		

		public TransitionStatesUnityEvent TransitionStateChanged => _transitionStateChanged;
		
		public TransitionStatesCompareUnityEvent[] TransitionStateChangedCompare => _transitionStateChangedCompare;

		
		private TransitionStates _transitionState = TransitionStates.Zero;
		public TransitionStates TransitionState
		{
			get => _transitionState;
			private set
			{
				if (_transitionState == value) return;
				if (!isActiveAndEnabled &&
				    (value == TransitionStates.ToOne || value == TransitionStates.ToZero)) return;
				_transitionState = value;
				TransitionStateUpdated(_transitionState);
			}
		}

		private NormalizedFloat _rawTransitionProgress = NormalizedFloat.Zero;
		public NormalizedFloat RawTransitionProgress
		{
			get => _rawTransitionProgress;
			private set
			{
				if (_rawTransitionProgress == value) return;
				_rawTransitionProgress = value;
				RawTransitionProgressUpdated(_rawTransitionProgress);
			}
		}

		private NormalizedFloat _transitionProgress = NormalizedFloat.Zero;
		public NormalizedFloat TransitionProgress
		{
			get => _transitionProgress;
			private set
			{
				if (_transitionProgress == value) return;
				_transitionProgress = value;
				TransitionProgressUpdated(_transitionProgress);
			}
		}

		


		private float _currentDuration;
		public float CurrentDuration => _currentDuration;

		private float Speed => _currentDuration > 0 ? 1 / _currentDuration : 9999;

		private bool _transitionCoroutineRunning;
		private Coroutine _transitionCoroutine;

		public virtual bool IsTransitioning => _transitionCoroutineRunning;
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** Abstract/Virtual Methods *****\\
		

		/// <summary>Raised when the transition progress is updated</summary>
		/// <param name="transitionProgress">The updated transition progress</param>
		protected abstract void OnTransitionProgressUpdated(NormalizedFloat transitionProgress);
		
		protected virtual void OnRawTransitionProgressUpdated(NormalizedFloat rawTransitionProgress){}
		
		/// <summary>Raised when the transition state is updated</summary>
		/// <param name="transitionState">The updated transition state</param>
		protected virtual void OnTransitionStateUpdated(TransitionStates transitionState){}


		/// <summary>Raised when the transitioner is started</summary>
		protected virtual void OnTransitionStarted(){}

		/// <summary>Raised when the transitioner is manually stopped</summary>
		protected virtual void OnTransitionStopped(){}

		/// <summary>Raised when the transitioner ends (i.e. it reaches the desired state normally)</summary>
		protected virtual void OnTransitionEnded(){}


		//***** Monobehaviour Callbacks *****\\
		
		protected virtual void OnValidate()
		{
			_discreteStepCount = Mathf.Max(1, _discreteStepCount);
			
			if (Application.isPlaying) return;

			if (!_transitionInEditor)
			{
				RawTransitionProgress = NormalizedFloat.Zero;
				return;
			}
			
			RawTransitionProgress = _transitionProgressEditor;
		}
		
		protected virtual void OnDisable()
		{
			if (!Application.isPlaying) return;
			StopTransitioning();
		}
		
		protected virtual void Start()
		{
			if (!Application.isPlaying) return;

			// Check if transition has already been started
			if (_transitionCoroutineRunning) return;
			if (_startTransitionState == TransitionStates.Zero && _transitionState != TransitionStates.Zero) return;
			
			switch (_startTransitionState)
			{
				case TransitionStates.Zero:
				case TransitionStates.ToOne:
					_rawTransitionProgress = NormalizedFloat.Zero;
					RawTransitionProgressUpdated(_rawTransitionProgress);
					TransitionProgressUpdated(_transitionProgress);
					_transitionState = TransitionStates.Zero;
					TransitionStateUpdated(_transitionState);

					if (_startTransitionState == TransitionStates.ToOne)
					{
						TransitionToOne();
					}
					
					break;
				
				case TransitionStates.One:
				case TransitionStates.ToZero:
					_rawTransitionProgress = NormalizedFloat.One;
					RawTransitionProgressUpdated(_rawTransitionProgress);
					TransitionProgressUpdated(_transitionProgress);
					_transitionState = TransitionStates.One;
					TransitionStateUpdated(_transitionState);
					
					if (_startTransitionState == TransitionStates.ToZero)
					{
						TransitionToZero();
					}
					
					break;
	
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		
		//***** Property Updated Callbacks *****\\
		
		private void RawTransitionProgressUpdated(NormalizedFloat rawTransitionProgress)
		{
			OnRawTransitionProgressUpdated(rawTransitionProgress);
			
			switch (_updateMode)
			{
				case DataIntervalModes.Continuous:
					TransitionProgress = _rawTransitionProgress;
					break;
					
				case DataIntervalModes.Discrete:

					var discreteStepCount = _specifyFramerate 
						? Mathf.CeilToInt(_discreteFrameRate * Mathf.Max(0.001f, CurrentDuration)) 
						: _discreteStepCount; 
					
					var discreteStepNumber = Mathf.RoundToInt(rawTransitionProgress.Value * discreteStepCount);
					TransitionProgress = new NormalizedFloat(discreteStepNumber / (float)discreteStepCount);
	
					break;
					
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		private void TransitionProgressUpdated(NormalizedFloat transitionProgress)
		{
			OnTransitionProgressUpdated(transitionProgress);
		}
		
		private void TransitionStateUpdated(TransitionStates transitionState)
		{
			OnTransitionStateUpdated(transitionState);
			
			_transitionStateChanged.Invoke(transitionState);

			foreach (var compareValueUnityEvent in _transitionStateChangedCompare)
			{
				compareValueUnityEvent.SetCurrentValue(transitionState);
			}
		}
		
		
		//***** Transition Starters *****\\

		public void Transition(bool transitionToOne)
		{
			if(transitionToOne) TransitionToOne();
			else 				TransitionToZero();
		}
		

		//** ToOne **\\

		public void TransitionToOne()
		{
			TransitionToOne(_defaultDuration.Value);
		}

		public void TransitionToOne(FloatSingle duration)
		{
			TransitionToOne(duration.Value);
		}
		
		public void TransitionToOne(float duration)
		{
			if (_transitionState == TransitionStates.One) return;
			if (_transitionState == TransitionStates.ToOne) return;
			
			if (!isActiveAndEnabled) return;
			//if (!interrupt && _transitionState == TransitionStates.ToZero) return;
			
			_currentDuration = duration;
			TransitionState = TransitionStates.ToOne;
			StartTransitionining();
		}

		
		//** ToZero **\\
		
		public void TransitionToZero()
		{
			TransitionToZero(_defaultDuration.Value);
		}
		
		public void TransitionToZero(FloatSingle duration)
		{
			TransitionToZero(duration.Value);
		}
		
		public void TransitionToZero(float duration)
		{
			if (_transitionState == TransitionStates.Zero) return;
			if (_transitionState == TransitionStates.ToZero) return;
			
			if (!isActiveAndEnabled) return;
			//if (!interrupt && _transitionState == TransitionStates.ToOne) return;

			_currentDuration = duration;
			TransitionState = TransitionStates.ToZero;
			StartTransitionining();
		}
		
		
		//** Toggle **\\
		
		public void TransitionToggle()
		{
			TransitionToggle(_defaultDuration.Value);
		}
		
		public void TransitionToggle(FloatSingle duration)
		{
			TransitionToggle(duration.Value);
		}
		
		public void TransitionToggle(float duration)
		{
			switch (_transitionState)
			{
				case TransitionStates.Zero:
				case TransitionStates.ToZero:
					TransitionToOne(duration);
					break;
				
				case TransitionStates.One:
				case TransitionStates.ToOne:
					TransitionToZero(duration);
					break;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		
		//** Jump **\\
		
		public void JumpToState(bool jumpToZero)
		{
			if (jumpToZero)
			{
				RawTransitionProgress = NormalizedFloat.Zero;
				TransitionState = TransitionStates.Zero;
			}
			else
			{
				RawTransitionProgress = NormalizedFloat.One;
				TransitionState = TransitionStates.One;
			}
		}
		
		
		//***** Transitioner Control *****\\

		private void StartTransitionining()
		{
			if (!isActiveAndEnabled)
			{
				// Should never get here
				Debug.LogError($"{name} => is trying to start a coroutine although the component is disabled");
				return;
			}
			
			if (_transitionCoroutineRunning) return;
			_transitionCoroutine = StartCoroutine(TransitionCoroutine());
			OnTransitionStarted();
		}
		
		public virtual void StopTransitioning()
		{
			var jumpToZero = _transitionState == TransitionStates.Zero || _transitionState == TransitionStates.ToZero;
			StopTransitioning(jumpToZero);
		}
		
		public virtual void StopTransitioning(bool jumpToZero)
		{
			if (!_transitionCoroutineRunning) return;
			_transitionCoroutineRunning = false;
			
			if (_transitionCoroutine != null)
			{
				StopCoroutine(_transitionCoroutine);
			}
			
			JumpToState(jumpToZero);

			OnTransitionStopped();
		}
		
		private IEnumerator TransitionCoroutine()
		{
			_transitionCoroutineRunning = true;

			// Wait a frame to negate recursion
			yield return null;
			
			while (true)
			{
				var deltaTime = _useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
				var direction = _transitionState == TransitionStates.ToZero ? -1 : 1;
				
				_rawTransitionProgress.Value += Speed * deltaTime * direction;

				RawTransitionProgressUpdated(_rawTransitionProgress);

				if (_rawTransitionProgress == NormalizedFloat.Zero)
				{
					TransitionState = TransitionStates.Zero;
					break;
				}

				if (_rawTransitionProgress == NormalizedFloat.One)
				{
					TransitionState = TransitionStates.One;
					break;
				}

				yield return null;
			}
			
			_transitionCoroutine = null;
			_transitionCoroutineRunning = false;
			
			OnTransitionEnded();
		}

	}
}