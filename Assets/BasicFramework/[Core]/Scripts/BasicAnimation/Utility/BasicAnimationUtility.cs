﻿using UnityEngine;

namespace BasicFramework.Core.BasicAnimation
{
	public static class BasicAnimationUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		public static string[] GetBlendShapeNames(Mesh mesh)
		{
			if (mesh == null) return null;
			
			var names = new string[mesh.blendShapeCount];

			for (int i = 0; i < names.Length; i++)
			{
				names[i] = mesh.GetBlendShapeName(i);
			}

			return names;
		}
	
	}
}