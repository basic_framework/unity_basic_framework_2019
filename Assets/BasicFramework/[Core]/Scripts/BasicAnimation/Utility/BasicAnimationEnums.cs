﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicAnimation
{
	public enum AnimationRecordingModes
	{
		Stopped,
		Recording,
		Playback,
	}
	
	[Serializable]
	public class AnimationRecordingModesUnityEvent : UnityEvent<AnimationRecordingModes>{}
	
	[Serializable]
	public class AnimationRecordingModesCompareUnityEvent : CompareValueUnityEventBase<AnimationRecordingModes>{}
}