﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Editor
{
	public static class BasicAnimationEditorUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		public static void BlendShapeIndexField(SerializedProperty property, Mesh mesh)
		{
			if (mesh == null)
			{
				EditorGUILayout.LabelField(property.displayName, "Missing Mesh");
				return;
			}

			var blendShapeNames = BasicAnimationUtility.GetBlendShapeNames(mesh);

			var dropdownValues = new string[blendShapeNames.Length + 1];
			dropdownValues[0] = "UNAVAILABLE";
			for (int i = 1; i < dropdownValues.Length; i++)
			{
				dropdownValues[i] = blendShapeNames[i - 1];
			}

			property.intValue = EditorGUILayout.Popup(property.displayName, property.intValue + 1, dropdownValues) - 1;
		}
	
	}
}