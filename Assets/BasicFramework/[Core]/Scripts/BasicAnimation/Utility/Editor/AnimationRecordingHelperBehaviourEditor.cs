﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Editor
{
	[CustomEditor(typeof(AnimationRecordingHelperBehaviour))]
	public class AnimationRecordingHelperBehaviourEditor : BasicBaseEditor<AnimationRecordingHelperBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private ReorderableList _reorderableListRecordingMode;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var recordingModeChangedCompare = serializedObject.FindProperty("_recordingModeChangedCompare");

			_reorderableListRecordingMode =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, recordingModeChangedCompare, true);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var recordingMode = serializedObject.FindProperty("_recordingMode");
			
			Space();
			SectionHeader($"{nameof(AnimationRecordingHelperBehaviour).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.HelpBox(
				$"You must manually set the \"{nameof(TypedTarget.RecordingMode).CamelCaseToHumanReadable()}\"", 
				MessageType.Info);
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(recordingMode);
			if (EditorGUI.EndChangeCheck()) RaiseEditorUpdate = true;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var recordingModeChanged = serializedObject.FindProperty("_recordingModeChanged");
			
			Space();
			SectionHeader($"{nameof(AnimationRecordingHelperBehaviour).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(recordingModeChanged);
			_reorderableListRecordingMode.DoLayoutList();
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			TypedTarget.EditorUpdate();
		}
		
	}
}