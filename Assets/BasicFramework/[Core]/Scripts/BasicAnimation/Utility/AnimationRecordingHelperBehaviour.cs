﻿using UnityEngine;

namespace BasicFramework.Core.BasicAnimation
{
	[AddComponentMenu("Basic Framework/Core/Animation/Utility/Animation Recording Helper Behaviour")]
	public class AnimationRecordingHelperBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The mode the recorder is in\n\nNote: this has to be manually set")]
		[SerializeField] private AnimationRecordingModes _recordingMode = default;

		[Tooltip("Raised when the recording mode changes")]
		[SerializeField] private AnimationRecordingModesUnityEvent _recordingModeChanged = default;
		
		[Tooltip("Raised when the recording mode changes and the conditions are met")]
		[SerializeField] private AnimationRecordingModesCompareUnityEvent[] _recordingModeChangedCompare = default;


		public AnimationRecordingModes RecordingMode
		{
			get => _recordingMode;
			set
			{
				if (_recordingMode == value) return;
				_recordingMode = value;
				RecordingModeUpdated(value);
			}
		}

		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Start()
		{
			RecordingModeUpdated(_recordingMode);
		}
		

		//***** property updates *****\\
		
		private void RecordingModeUpdated(AnimationRecordingModes value)
		{
			_recordingModeChanged.Invoke(value);

			foreach (var compareUnityEvent in _recordingModeChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\
		
		public void SetModePlayback() 	=> RecordingMode = AnimationRecordingModes.Playback;
		public void SetModeRecording() 	=> RecordingMode = AnimationRecordingModes.Recording;
		public void SetModeStopped() 	=> RecordingMode = AnimationRecordingModes.Stopped;
		
		
		
#if UNITY_EDITOR
		//***** editor *****\\

		public void EditorUpdate()
		{
			if (!Application.isPlaying) return;
			RecordingModeUpdated(_recordingMode);
		}
#endif		
		
	}
}