﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.InverseKinematics
{
	[AddComponentMenu("Basic Framework/Core/Animation/IK/Basic IK Target")]
	public class BasicIkTarget : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("Should the IK target be considered as active")]
		[SerializeField] private bool _targetActive = true;
		
		[Tooltip("The transform to use as the IK target")]
		[SerializeField] private Transform _transformTarget = default;
		
		[Tooltip("Raised when the target active status changes")]
		[SerializeField] private BoolPlusUnityEvent _targetActiveChanged = default;
		
		[SerializeField] private TransformUnityEvent _transformTargetChanged = default;

		
		public bool TargetActive
		{
			get => _targetActive;
			set
			{
				if (_targetActive == value) return;
				_targetActive = value;
				TargetActiveUpdated(value);
			}
		}

		public Transform TransformTarget
		{
			get => _transformTarget;
			set
			{
				if (_transformTarget == value) return;
				_transformTarget = value;
				TransformTargetUpdated(value);
			}
		}

		

//		public BoolPlusUnityEvent TargetActiveChanged => _targetActiveChanged;

		public readonly BasicEvent<BasicIkTarget> OnTargetActiveChanged = new BasicEvent<BasicIkTarget>();
		public readonly BasicEvent<BasicIkTarget> OnTargetTransformChanged = new BasicEvent<BasicIkTarget>();
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Reset()
		{
			_transformTarget = transform;
		}

		private void Start()
		{
			TargetActiveUpdated(_targetActive);
		}
		

		//***** property updated *****\\
		
		private void TransformTargetUpdated(Transform value)
		{
			_transformTargetChanged.Invoke(value);
			OnTargetTransformChanged.Raise(this);
		}
		
		private void TargetActiveUpdated(bool value)
		{
			_targetActiveChanged.Raise(value);
			OnTargetActiveChanged.Raise(this);
		}
		
		
		//***** editor *****\\

#if UNITY_EDITOR
		public void EditorUpdate()
		{
			if (!Application.isPlaying) return;
			TransformTargetUpdated(_transformTarget);
			TargetActiveUpdated(_targetActive);
		}
#endif

	}
}