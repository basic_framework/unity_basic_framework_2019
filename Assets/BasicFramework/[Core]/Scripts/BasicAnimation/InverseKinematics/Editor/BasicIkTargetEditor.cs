﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.InverseKinematics.Editor
{
	[CustomEditor(typeof(BasicIkTarget))][CanEditMultipleObjects]
	public class BasicIkTargetEditor : BasicBaseEditor<BasicIkTarget>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var targetActive = serializedObject.FindProperty("_targetActive");
			var transformTarget = serializedObject.FindProperty("_transformTarget");
			
			Space();
			SectionHeader($"{nameof(BasicIkTarget).CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			
			EditorGUILayout.PropertyField(targetActive);
			BasicEditorGuiLayout.PropertyFieldNotNull(transformTarget);
			
			if (EditorGUI.EndChangeCheck()) RaiseEditorUpdate = true;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var targetActiveChanged = serializedObject.FindProperty("_targetActiveChanged");
			var transformTargetChanged = serializedObject.FindProperty("_transformTargetChanged");
			
			Space();
			SectionHeader($"{nameof(BasicIkTarget).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(targetActiveChanged);
			EditorGUILayout.PropertyField(transformTargetChanged);
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			TypedTarget.EditorUpdate();
		}
	}
}