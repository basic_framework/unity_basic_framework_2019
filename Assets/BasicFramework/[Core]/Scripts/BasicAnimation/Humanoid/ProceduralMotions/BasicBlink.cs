﻿using System;
using System.Collections;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid
{
	[AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Utility/Blink")]
	public class BasicBlink : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// How long it takes to open/close the eyelids
		private const float OPEN_CLOSE_DURATION = 0.2f;
		private const float OPEN_CLOSE_SPEED = 1 / OPEN_CLOSE_DURATION;

		[SerializeField] private BasicHumanoid _basicHumanoid = default;
		
		[SerializeField] private int _leftBlinkIndex = default;
		[SerializeField] private int _rightBlinkIndex = default;

		[Tooltip("The delay range in seconds between blinks. A random number between min and max will be chosen")]
		[SerializeField] private FloatRange _blinkDelayRange = new FloatRange(1f, 10f);
		
		[Tooltip("The duration in seconds the eyelids will be held closed. A random number between min and max will be chosen")]
		[SerializeField] private FloatRange _closeDurationRange = new FloatRange(0f, 0.5f);
		
		
		private bool _isBlinking;

		private float _nextBlinkTime;
		
	
		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_basicHumanoid = transform.root.GetComponentInChildren<BasicHumanoid>();
		}

		private void OnEnable()
		{
			_isBlinking = false;
			
			_nextBlinkTime = Time.time + _blinkDelayRange.GetRandomInRange();
		}

		private void Update()
		{
			if (_isBlinking) return;

			if (Time.time < _nextBlinkTime) return;
			
			BlinkEyes(_closeDurationRange.GetRandomInRange());
		}

		public void BlinkEyes(float closeDuration)
		{
			if (_isBlinking) return;
			StartCoroutine(BlinkCoroutine(closeDuration));
		}

		private IEnumerator BlinkCoroutine(float closeDuration)
		{
			_isBlinking = true;
			
			var progressClosed = 0f;
			var skinnedMeshRenderer = _basicHumanoid.SkinnedMeshRenderer;

			// Close
			while (progressClosed < 1)
			{
				progressClosed = MathUtility.MoveTo(progressClosed, 1, OPEN_CLOSE_SPEED * Time.deltaTime);
				skinnedMeshRenderer.SetBlendShapeWeight(_leftBlinkIndex, progressClosed * 100);
				skinnedMeshRenderer.SetBlendShapeWeight(_rightBlinkIndex, progressClosed * 100);
				yield return null;
			}

			// Hold Closed
			var prevTime = Time.time;

			while (Time.time - prevTime < closeDuration)
			{
				yield return null;
			}
			
			// Open
			while (progressClosed > 0)
			{
				progressClosed = MathUtility.MoveTo(progressClosed, 0, OPEN_CLOSE_SPEED * Time.deltaTime);
				skinnedMeshRenderer.SetBlendShapeWeight(_leftBlinkIndex, progressClosed * 100);
				skinnedMeshRenderer.SetBlendShapeWeight(_rightBlinkIndex, progressClosed * 100);
				yield return null;
			}

			_nextBlinkTime = Time.time + _blinkDelayRange.GetRandomInRange();
			
			_isBlinking = false;
		}
	
	}
}