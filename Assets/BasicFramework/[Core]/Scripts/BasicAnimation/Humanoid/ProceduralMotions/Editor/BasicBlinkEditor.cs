﻿using BasicFramework.Core.BasicAnimation.Editor;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Editor
{
	[CustomEditor(typeof(BasicBlink))]
	public class BasicBlinkEditor : BasicBaseEditor<BasicBlink>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			
			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid");
			
			var leftBlinkIndex = serializedObject.FindProperty("_leftBlinkIndex");
			var rightBlinkIndex = serializedObject.FindProperty("_rightBlinkIndex");
			
			var blinkDelayRange = serializedObject.FindProperty("_blinkDelayRange");
			var closeDurationRange = serializedObject.FindProperty("_closeDurationRange");
			
			
			Space();
			SectionHeader($"{nameof(BasicBlink).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(basicHumanoid);
			
			var basicHumanoidObject = basicHumanoid.objectReferenceValue as BasicHumanoid;
			var renderer = basicHumanoidObject != null ? basicHumanoidObject.SkinnedMeshRenderer : default;
			
			Mesh mesh = null;
			
			if (renderer != null)
			{
				mesh = renderer.sharedMesh;
			}
			
			GUI.enabled = false;

			EditorGUILayout.ObjectField("Mesh", mesh, typeof(Mesh), true);
			
			
			GUI.enabled = PrevGuiEnabled && renderer != null && mesh != null;

			Space();
			BasicAnimationEditorUtility.BlendShapeIndexField(leftBlinkIndex, mesh);
			BasicAnimationEditorUtility.BlendShapeIndexField(rightBlinkIndex, mesh);

			GUI.enabled = PrevGuiEnabled;
			
			
			Space();
			EditorGUILayout.PropertyField(blinkDelayRange);
			EditorGUILayout.PropertyField(closeDurationRange);
		}

	}
}