﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility
{
	[AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Utility/Scale To Humanoid")]
	public class ScaleToHumanoid : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// Useful when recording IK points so you can rescale it to the size of the human 
		
		[Tooltip("The humanoid to scale to (to match height with)")]
		[SerializeField] private BasicHumanoid _basicHumanoid = default;

		[Tooltip("The transform that will be scaled to match the humanoid")]
		[SerializeField] private Transform _transformToScale = default;
		
		[Tooltip("The height of TransformToScale when it is scale 1")]
		[SerializeField] private float _heightAtScaleOne = default;
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		private void Reset()
		{
			_transformToScale = transform;
			_heightAtScaleOne = gameObject.GetBounds().max.y;
		}

		private void OnEnable()
		{
			OnHumanoidHeightChanged(_basicHumanoid.Height);
			
			_basicHumanoid.HeightChanged.AddListener(OnHumanoidHeightChanged);
		}
		
		private void OnDisable()
		{
			_basicHumanoid.HeightChanged.RemoveListener(OnHumanoidHeightChanged);
		}
		
		private void OnDrawGizmosSelected()
		{
			if (_transformToScale == null) return;

			var bottom = _transformToScale.position;
			var top = _transformToScale.TransformPoint(Vector3.up * _heightAtScaleOne);

			BasicGizmoUtility.DrawDistance(bottom, top, Color.yellow);
		}
		
		
		//***** callbacks *****\\
		
		private void OnHumanoidHeightChanged(float value)
		{
			_transformToScale.localScale = value / _heightAtScaleOne * Vector3.one;
		}
		
	}
}