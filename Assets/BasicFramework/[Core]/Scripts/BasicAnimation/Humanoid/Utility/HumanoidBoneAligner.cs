﻿using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility
{
    [AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Utility/Humanoid Bone Aligner")]
    public class HumanoidBoneAligner : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BasicHumanoid _basicHumanoid = default;
        
        [SerializeField] private Transform _transformAligning = default;
		
        [SerializeField] private HumanBodyBones _humanBodyBone = default;
		
        
        public Transform TransformAligning => _transformAligning;

        public HumanBodyBones HumanBodyBone => _humanBodyBone;
        

        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
	        _basicHumanoid = transform.root.GetComponentInChildren<BasicHumanoid>();
	        _transformAligning = transform;
        }

        public void AlignToBone()
        {
	        if (_basicHumanoid == null) return;
	        if (_transformAligning == null) return;

	        var bone = _basicHumanoid.GetBone(_humanBodyBone);
	        if (bone == null) return;
	        
	        _transformAligning.position = bone.position;
	        _transformAligning.rotation = bone.rotation;
        }

        private void OnDrawGizmosSelected()
        {
	        if (_basicHumanoid == null) return;
	        var bone = _basicHumanoid.GetBone(_humanBodyBone);
	        if (bone == null) return;
	        BasicGizmoUtility.DrawAxis(bone, 0.1f);
        }
        
    }
}