﻿using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility.Editor
{
	[CustomEditor(typeof(HumanoidBoneAligner))][CanEditMultipleObjects]
	public class HumanoidBoneAlignerEditor : BasicBaseEditor<HumanoidBoneAligner>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid");
			var transformAligning = serializedObject.FindProperty("_transformAligning");
			var humanBodyBone = serializedObject.FindProperty("_humanBodyBone");
			
			Space();
			SectionHeader($"{nameof(HumanoidBoneAligner).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(basicHumanoid);
			
			Space();
			BasicEditorGuiLayout.PropertyFieldNotNull(transformAligning);
			BasicEditorGuiLayout.PropertyFieldNotNull(humanBodyBone);

			if (!GUILayout.Button("Align")) return;
			
			foreach (var typedTarget in TypedTargets)
			{
				var transform = typedTarget.transform;
					
				Undo.RecordObject(transform, 
					$"Align {transform.name} to " +
					$"{humanBodyBone.enumNames[humanBodyBone.enumValueIndex].CamelCaseToHumanReadable()}");

				typedTarget.AlignToBone();
			}

		}
		
	}
}