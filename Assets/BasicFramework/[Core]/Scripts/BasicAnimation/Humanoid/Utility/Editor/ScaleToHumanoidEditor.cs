﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility.Editor
{
	[CustomEditor(typeof(ScaleToHumanoid))]
	public class ScaleToHumanoidEditor : BasicBaseEditor<ScaleToHumanoid>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid");
			var transformToScale = serializedObject.FindProperty("_transformToScale");
			var heightAtScaleOne = serializedObject.FindProperty("_heightAtScaleOne");
			
			Space();
			SectionHeader($"{nameof(ScaleToHumanoid).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(basicHumanoid);
			
			Space();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformToScale);
			EditorGUILayout.PropertyField(heightAtScaleOne);
		}
		
	}
}