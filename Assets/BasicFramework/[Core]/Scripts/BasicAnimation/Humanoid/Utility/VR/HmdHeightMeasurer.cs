﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility
{
	[AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Utility/Hmd Height Measurer")]
	public class HmdHeightMeasurer : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The transorm that is set to the tracked hmd pose")]
		[SerializeField] private Transform _transformHmd = default;

		[Tooltip("An offset that will be applied to the measured height, normally distance from eyes to head top")]
		[SerializeField] private float _offsetHeight = 0.1f;
		
		[Tooltip("Should the measurer begin on awake")]
		[SerializeField] private bool _startOnAwake = default;

		[SerializeField] private BoolPlusUnityEvent _isMeasuringChanged = default;

		[SerializeField] private FloatUnityEvent _heightChanged = default;
		
		
		public float OffsetHeight
		{
			get => _offsetHeight;
			set => _offsetHeight = value;
		}

		public Transform TransformHmd
		{
			get => _transformHmd;
			set => _transformHmd = value;
		}
		
		public BoolPlusUnityEvent IsMeasuringChanged => _isMeasuringChanged;

		public FloatUnityEvent HeightChanged => _heightChanged;
		
		private float _height;
		public float Height
		{
			get => _height;
			private set
			{
				if(Math.Abs(_height - value) < Mathf.Epsilon) return;
				_height = value;
				HeightUpdated(value);
			}
		}

		public bool IsMeasuring => enabled;


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		private void Awake()
		{
			enabled = _startOnAwake;
		}

		private void OnEnable()
		{
			_isMeasuringChanged.Raise(true);
		}

		private void OnDisable()
		{
			_isMeasuringChanged.Raise(false);
		}

		private void Update()
		{
			var height = _transformHmd.localPosition.y + _offsetHeight;
			Height = Mathf.Max(_height, height);
		}


		//***** property updated *****\\
		
		private void HeightUpdated(float value)
		{
			_heightChanged.Invoke(value);
		}
		
		
		//***** functions *****\\

		public void ResetHeight()
		{
			_height = 0;
		}
		
		public void StartMeasuring(bool resetHeight = true)
		{
			if (resetHeight) _height = 0;
			enabled = true;
		}

		public void StopMeasuring()
		{
			enabled = false;
		}

	}
}