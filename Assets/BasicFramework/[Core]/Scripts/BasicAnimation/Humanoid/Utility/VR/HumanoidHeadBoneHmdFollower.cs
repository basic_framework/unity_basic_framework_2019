﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Tracking;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility
{
	[AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Utility/Humanoid Hmd Head Bone Follower")]
	public class HumanoidHeadBoneHmdFollower : TrackingBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// Note: Apply this to your head ik target when using vr
		
		[SerializeField] private BasicHumanoid _basicHumanoid = default;
		

		public BasicHumanoid BasicHumanoid
		{
			get => _basicHumanoid;
			set => _basicHumanoid = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		
		//***** mono *****\\

		protected override void Reset()
		{
			base.Reset();
			
			_basicHumanoid = transform.root.GetComponentInChildren<BasicHumanoid>();
		}

		
		//***** functions *****\\

		protected override void UpdateTracker(PoseSource tracker, BasicPose targetOffsetPose)
		{
			//var targetPose = target.Pose;
			
			var headToHmdOffset = _basicHumanoid.HeadToHmdOffsetWorld;
			targetOffsetPose.Position -= headToHmdOffset;
			
			var headBone = _basicHumanoid.HeadBone;
			Debug.DrawRay(headBone.position, headToHmdOffset, Color.yellow);

			tracker.Pose = targetOffsetPose;
		}

	}
}