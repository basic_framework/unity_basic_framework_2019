﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using BasicFramework.Core.Utility.Tracking.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility.Editor
{
	[CustomEditor(typeof(HumanoidHeadBoneHmdFollower))]
	public class HumanoidHeadBoneHmdFollowerEditor : TrackingBaseEditor<HumanoidHeadBoneHmdFollower>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid");
			
			Space();
			SectionHeader($"{nameof(HumanoidHeadBoneHmdFollower).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(basicHumanoid);
		}
		
	}
}