﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Utility.Editor
{
	[CustomEditor(typeof(HmdHeightMeasurer))]
	public class HmdHeightMeasurerEditor : BasicBaseEditor<HmdHeightMeasurer>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.Height), TypedTarget.Height.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var transformHmd = serializedObject.FindProperty("_transformHmd");
			var offsetHeight = serializedObject.FindProperty("_offsetHeight");
			var startOnAwake = serializedObject.FindProperty("_startOnAwake");
//			var measureInWorldSpace = serializedObject.FindProperty("_measureInWorldSpace");
			
			Space();
			SectionHeader($"{nameof(HmdHeightMeasurer).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(transformHmd);
			EditorGUILayout.PropertyField(offsetHeight);
			EditorGUILayout.PropertyField(startOnAwake);
//			EditorGUILayout.PropertyField(measureInWorldSpace);

		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var isMeasuringChanged = serializedObject.FindProperty("_isMeasuringChanged");
			var heightChanged = serializedObject.FindProperty("_heightChanged");
			
			Space();
			SectionHeader($"{nameof(HmdHeightMeasurer).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(isMeasuringChanged);
			
			Space();
			EditorGUILayout.PropertyField(heightChanged);
		}
	}
}