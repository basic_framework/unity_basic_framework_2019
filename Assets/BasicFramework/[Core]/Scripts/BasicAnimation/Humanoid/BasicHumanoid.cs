﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid
{
	[AddComponentMenu("Basic Framework/Core/Animation/Humanoid/Basic Humanoid")]
	public class BasicHumanoid : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		// Name?, Gender?
		
		public const float MINIMUM_HEIGHT = 0.1f;
		
#if UNITY_EDITOR
		[Range(0.4f, 2.6f)]
		[SerializeField] private float _editorSetHeight = 1.7f;
#endif		
		
		[Tooltip("The animator for this humanoid. Note: The humanoid rig type should be set to humanoid")]
		[SerializeField] private Animator _animator = default;

		// Note: May have multiple of these in the future, will need to be more specific, i.e. head
		[Tooltip("The skinned mesh renderer for this humanoid")]
		[SerializeField] private SkinnedMeshRenderer _skinnedMeshRenderer = default;
		
		[Tooltip("The parent transform of the model, this will be scaled to get the correct height")]
		[SerializeField] private Transform _parentModel = default;
		
		[Tooltip("The height of the humanoid when the parent model is scaled to one")]
		[SerializeField] private float _heightAtScaleOne = 1f;

		[Tooltip("The offset from the head bone to the hmd tracked position, relative to the head bone transform. " +
		         "Note: Mainly used in VR")]
		[SerializeField] private Vector3 _headToHmdOffsetLocal = default;

		[Tooltip("Raised when the humanoids height is changed")]
		[SerializeField] private FloatUnityEvent _heightChanged = default;
		
		
		public Animator Animator => _animator;

		public SkinnedMeshRenderer SkinnedMeshRenderer => _skinnedMeshRenderer;
		
		public Transform ParentModel => _parentModel;

		public FloatUnityEvent HeightChanged => _heightChanged;
		
		public bool IsValidHuman => _animator != null && _animator.isHuman;

//		public Vector3 HeadToHmdOffsetLocal => _headToHmdOffsetLocal;
		public Vector3 HeadToHmdOffsetWorld => HeadBone.TransformVector(_headToHmdOffsetLocal);

		private bool _isHeadBoneCached;
		private Transform _cachedHeadBone;
		public Transform HeadBone => _isHeadBoneCached ? _cachedHeadBone : GetBone(HumanBodyBones.Head);
		
		public float Height
		{
			get => _parentModel.lossyScale.y * _heightAtScaleOne;
			set
			{
				if(value < MINIMUM_HEIGHT) return;
				
				if (Math.Abs(Height - value) < Mathf.Epsilon) return;
				_parentModel.localScale = value / _heightAtScaleOne * Vector3.one;
				HeightUpdated(value);
			}
		}


		// ****************************************  METHODS  ****************************************\\
		

		//***** mono *****\\

		private void Reset()
		{
			_animator = GetComponentInChildren<Animator>();
			_skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
			_parentModel = _animator != null ? _animator.transform : null;

			if (_parentModel != null)
			{
				_heightAtScaleOne = _parentModel.gameObject.GetBounds().max.y;
				
#if UNITY_EDITOR
				_editorSetHeight = _heightAtScaleOne;
#endif
			}
			
			var eyeLeft = GetBone(HumanBodyBones.LeftEye);
			var eyeRight = GetBone(HumanBodyBones.RightEye);
			var head = GetBone(HumanBodyBones.Head);

			if (eyeLeft != null && eyeRight != null && head != null)
			{
				var eyeLeftPosition = eyeLeft.position;
				var eyeRightPosition = eyeRight.position;
				var eyeCentre = (eyeRightPosition - eyeLeftPosition) * 0.5f + eyeLeftPosition;

				_headToHmdOffsetLocal = head.InverseTransformPoint(eyeCentre);
			}
		}
		
		protected virtual void OnValidate()
		{
			if (_animator == null) _animator = GetComponentInChildren<Animator>();

			if (_skinnedMeshRenderer == null)
			{
				_skinnedMeshRenderer = _animator != null 
					? _animator.gameObject.GetComponentInChildren<SkinnedMeshRenderer>() 
					: GetComponentInChildren<SkinnedMeshRenderer>();
			}

			if (_parentModel == null) _parentModel = _animator != null ? _animator.transform : null;
		}
		
		private void OnDrawGizmosSelected()
		{
			if (_parentModel == null) return;

			var colorOld = Gizmos.color;
			
			var topOfHead = _parentModel.TransformPoint(Vector3.up * _heightAtScaleOne);
			BasicGizmoUtility.DrawDistance(_parentModel.position, topOfHead, Color.magenta, 0.1f);
			
			var headBone = GetBone(HumanBodyBones.Head);
			if (headBone != null)
			{
				var eyePositionWorld = headBone.TransformPoint(_headToHmdOffsetLocal);
				
				Gizmos.color = Color.yellow;
				Gizmos.DrawWireSphere(eyePositionWorld, 0.01f);
				Gizmos.DrawLine(headBone.position, eyePositionWorld);
			}

			Gizmos.color = colorOld;
		}

		private void Awake()
		{
			_cachedHeadBone = GetBone(HumanBodyBones.Head);
			_isHeadBoneCached = true;
		}

		private void Start()
		{
			HeightUpdated(Height);
		}


		//***** property updates *****\\
		
		private void HeightUpdated(float value)
		{
			_heightChanged.Invoke(value);
		}
		
		
		//***** functions *****\\
		
		public Transform GetBone(HumanBodyBones humanBodyBone)
		{
			return !IsValidHuman ? null : _animator.GetBoneTransform(humanBodyBone);
		}
		
#if UNITY_EDITOR
		public void EditorHeightUpdate()
		{
			Height = _editorSetHeight;
		}
#endif
		
	}
}