﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicAnimation.Humanoid.Editor
{
	[CustomEditor(typeof(BasicHumanoid))]
	public class BasicHumanoidEditor : BasicBaseEditor<BasicHumanoid>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;
		

		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var editorSetHeight = serializedObject.FindProperty("_editorSetHeight");
			
			if (TypedTarget.ParentModel == null)
			{
				EditorGUILayout.HelpBox($"Cannot display debug data until " +
				                        $"\"{nameof(TypedTarget.ParentModel).CamelCaseToHumanReadable()}\" is set",
					MessageType.Warning);
				return;
			}
			
			EditorGUILayout.LabelField(nameof(TypedTarget.IsValidHuman).CamelCaseToHumanReadable(), 
				TypedTarget.IsValidHuman.ToString());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.Height).CamelCaseToHumanReadable(), 
				TypedTarget.Height.ToString());
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(editorSetHeight);
			if (EditorGUI.EndChangeCheck()) RaiseEditorUpdate = true;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var animator = serializedObject.FindProperty("_animator");
			var skinnedMeshRenderer = serializedObject.FindProperty("_skinnedMeshRenderer");
			
			var parentModel = serializedObject.FindProperty("_parentModel");
			var heightAtScaleOne = serializedObject.FindProperty("_heightAtScaleOne");
			
			var headToHmdOffsetLocal = serializedObject.FindProperty("_headToHmdOffsetLocal");
			
			
			Space();
			SectionHeader($"{nameof(BasicHumanoid)} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(animator);
			BasicEditorGuiLayout.PropertyFieldNotNull(skinnedMeshRenderer);
			
			Space();
			SectionHeader("Measurements");
			BasicEditorGuiLayout.PropertyFieldNotNull(parentModel);
			EditorGUILayout.PropertyField(heightAtScaleOne);
			heightAtScaleOne.floatValue = Mathf.Max(0.01f, heightAtScaleOne.floatValue);

			EditorGUILayout.PropertyField(headToHmdOffsetLocal);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();

			var heightChanged = serializedObject.FindProperty("_heightChanged");
			
			Space();
			SectionHeader($"{nameof(BasicHumanoid)} Events");

			EditorGUILayout.PropertyField(heightChanged);
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			
			var parentModel = serializedObject.FindProperty("_parentModel").objectReferenceValue as Transform;
			if (parentModel == null) return;
			
			Undo.RecordObject(parentModel, $"Changed height of {TypedTarget.name}/{nameof(BasicHumanoid)}");
			TypedTarget.EditorHeightUpdate();
		}
		
	}
}