﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement
{
    [AddComponentMenu("Basic Framework/Core/Scriptable Variables/Single/Scene Switch States Single Listener")]
    public class SceneSwitchStatesSingleListener : SingleStructListenerBase
    <
        SceneSwitchStates, 
        SceneSwitchStatesSingle, 
        SceneSwitchStatesUnityEvent, 
        SceneSwitchStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}