﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.SceneManagement
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scene Management/Scene Switch State", 
        fileName = "single_scene_switch_state_",
        order = SceneManagementAssetMenuOrder.SCENE_SWITCH_STATE_SINGLE)
    ]
    public class SceneSwitchStatesSingle : SingleStructBase<SceneSwitchStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(SceneSwitchStatesSingle value)
        {
            Value = value.Value;
        }
		
    }

    [Serializable]
    public class SceneSwitchStateSingleReference : SingleStructReferenceBase<SceneSwitchStates, SceneSwitchStatesSingle>
    {
        public SceneSwitchStateSingleReference()
        {
        }

        public SceneSwitchStateSingleReference(SceneSwitchStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class SceneSwitchStateSingleUnityEvent : UnityEvent<SceneSwitchStatesSingle>{}
	
    [Serializable]
    public class SceneSwitchStateSingleArrayUnityEvent : UnityEvent<SceneSwitchStatesSingle[]>{}
    
}