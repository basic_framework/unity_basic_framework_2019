﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement
{
    [AddComponentMenu("Basic Framework/Core/Scene Management/Basic Scene Asset Single Listener")]
    public class BasicSceneAssetSingleListener : SingleObjectListenerBase
    <
        BasicSceneAsset, 
        BasicSceneAssetSingle, 
        BasicSceneAssetUnityEvent, 
        BasicSceneAssetCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}