﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scene Management/Basic Scene Asset Single", 
        fileName = "single_basic_scene_")
    ]
    public class BasicSceneAssetSingle : SingleObjectBase<BasicSceneAsset>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(BasicSceneAssetSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class BasicSceneAssetSingleReference : SingleObjectReferenceBase<BasicSceneAsset, BasicSceneAssetSingle>
    {
        public BasicSceneAssetSingleReference()
        {
        }

        public BasicSceneAssetSingleReference(BasicSceneAsset value)
        {
            _constantValue = value;
        }
    }
}