﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(SceneSwitchStatesSingleListener))][CanEditMultipleObjects]
    public class SceneSwitchStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    SceneSwitchStates, 
	    SceneSwitchStatesSingle, 
	    SceneSwitchStatesUnityEvent,
	    SceneSwitchStatesCompareUnityEvent,
	    SceneSwitchStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}