﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(SceneSwitchStatesSingle))][CanEditMultipleObjects]
    public class SceneSwitchStatesSingleEditor : SingleStructBaseEditor<SceneSwitchStates, SceneSwitchStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(SceneSwitchStatesSingle))]
    public class SceneSwitchStateSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(SceneSwitchStateSingleReference))]
    public class SceneSwitchStateSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}