﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(BasicSceneAssetSingleListener))][CanEditMultipleObjects]
    public class BasicSceneAssetSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        BasicSceneAsset, 
        BasicSceneAssetSingle, 
        BasicSceneAssetUnityEvent,
        BasicSceneAssetCompareUnityEvent,
        BasicSceneAssetSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}