﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(BasicSceneAssetSingle))][CanEditMultipleObjects]
    public class BasicSceneAssetSingleEditor : SingleObjectBaseEditor<BasicSceneAsset, BasicSceneAssetSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(BasicSceneAssetSingle))]
    public class BasicSceneAssetSinglePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(BasicSceneAssetSingleReference))]
    public class BasicSceneAssetSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
}