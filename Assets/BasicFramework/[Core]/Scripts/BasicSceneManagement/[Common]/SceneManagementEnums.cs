﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.SceneManagement
{

	public enum SceneSwitchStates
	{
		Sleeping,
		Loading,
		WaitingForSceneActivation,
		FadingOut,
		ActivatingScene,
		FadingIn
	}
	
	[Serializable]
	public class SceneSwitchStatesUnityEvent : UnityEvent<SceneSwitchStates>{}

	[Serializable]
	public class SceneSwitchStatesCompareUnityEvent : CompareValueUnityEventBase<SceneSwitchStates>{}
	
}