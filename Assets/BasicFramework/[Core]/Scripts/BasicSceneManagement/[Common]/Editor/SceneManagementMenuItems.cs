﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
	public static class SceneManagementMenuItems 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		[MenuItem(  BasicEditorUtility.MENU_ITEM_ROOT + "Core/Scene Management/Update Scene Details", priority = 0)]
		public static void UpdateSceneDetailsMenuItem()
		{
			EditorSceneManagementUtility.UpdateBasicSceneDetails();
		}
	
	}
	
}