﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement.Editor
{
	public class SceneManagementPreBuildCallback : IPreprocessBuildWithReport 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		public int callbackOrder { get; }
	
		
		// ****************************************  METHODS  ****************************************\\

		public void OnPreprocessBuild(BuildReport report)
		{
			EditorSceneManagementUtility.UpdateBasicSceneDetails();
			Debug.Log("Basic Framework Scene Management => Basic Scene Asset Details Updated");
		}
		
	}
	
	public static class EditorSceneManagementUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		public static void UpdateBasicSceneDetails()
		{
			var scenes = Resources.FindObjectsOfTypeAll<BasicSceneAsset>();

			foreach (var scene in scenes)
			{
				UpdateBasicSceneDetails(scene);
			}
		}
		
		public static void UpdateBasicSceneDetails(BasicSceneAsset scene)
		{
			if (scene == null) return;
			
			var sceneSerializedObject = new SerializedObject(scene);

			var sceneAsset 	= sceneSerializedObject.FindProperty("_sceneAsset");
			var scenePath 	= sceneSerializedObject.FindProperty("_scenePath");
			var buildIndex 	= sceneSerializedObject.FindProperty("_buildIndex");
			var sceneName 	= sceneSerializedObject.FindProperty("_sceneName");
			var inBuild 	= sceneSerializedObject.FindProperty("_inBuild");

			var sceneAssetObject = sceneAsset.objectReferenceValue as SceneAsset;
			if (sceneAssetObject == null) return;
			
			sceneSerializedObject.Update();

			var scenePathString = AssetDatabase.GetAssetPath(sceneAssetObject);
			
			scenePath.stringValue =  scenePathString;

			var lastForwardSlash 	= scenePathString.LastIndexOf('/');
			var sceneNameString 	= scenePathString.Substring(lastForwardSlash + 1);
			sceneNameString 		= sceneNameString.Replace(".unity", "");

			sceneName.stringValue = sceneNameString;
			
			var buildIndexInt = -1;

			var scenes = EditorBuildSettings.scenes;

			var enabledIndex = -1;
			var sceneInBuild = false;
			
			for (int i = 0; i < scenes.Length; i++)
			{
				var buildScene = scenes[i];
				if (buildScene.enabled) enabledIndex++;
				
				if (!buildScene.path.Equals(scenePathString)) continue;
				buildIndexInt = enabledIndex;
				sceneInBuild = true;
				break;
			}

			buildIndex.intValue = buildIndexInt;
			inBuild.boolValue = sceneInBuild;

			sceneSerializedObject.ApplyModifiedProperties();
		}
	
	}
}