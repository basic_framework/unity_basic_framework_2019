﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement.Editor
{
	[CustomEditor(typeof(SceneSwitchManager))]
	public class SceneSwitchManagerEditor : BasicBaseEditor<SceneSwitchManager>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool HasDebugSection => true;

		
		private ReorderableList _stateChangedEventsReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var sceneChangeStateChangedEvents = serializedObject.FindProperty("_sceneSwitchStateChangedEvents");

			_stateChangedEventsReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, sceneChangeStateChangedEvents,
					true);
		}

//		protected override void DrawPropertyFields()
//		{
//			base.DrawPropertyFields();
//			
//			Space();
//			DrawSettingsFields();
//		}

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();
			
			EditorGUILayout.HelpBox(
				"This gameobject will be set to DontDestroyOnLoad.\n" +
				"There can only be one instance of this component in a scene.\n" +
				"Multiples, and their gameobject's, will be deleted.", 
				MessageType.Warning);
		}

		protected override void DrawDebugFields()
		{
			GUI.enabled = false;

			EditorGUILayout.EnumPopup("Scene Switch State", TypedTarget.SceneSwitchState);
			EditorGUILayout.LabelField("Allow Scene Activation", TypedTarget.AllowSceneActivation.ToString());
			EditorGUILayout.Slider("Load Progress", TypedTarget.LoadProgress.Value, 0, 1);

			GUI.enabled = PrevGuiEnabled;
		}

//		private void DrawSettingsFields()
//		{
//			SectionHeader("Settings");
//			
//			var defaultScreenFadeTransitioner = serializedObject.FindProperty("_defaultScreenFadeTransitioner");
//			
//			BasicEditorGuiLayout.PropertyFieldCanBeNull(defaultScreenFadeTransitioner);
//		}

		protected override void DrawEventFields()
		{
			SectionHeader("Events");
			
			EditorGUILayout.HelpBox(
				"Do not reference scene objects in events. The manager carries across scenes so references will be lost", 
				MessageType.Warning);
			
			var allowSceneActivationChanged = serializedObject.FindProperty("_allowSceneActivationChanged");
			var loadProgressChanged = serializedObject.FindProperty("_loadProgressChanged");
			var sceneChangeStateChanged = serializedObject.FindProperty("_sceneSwitchStateChanged");

			EditorGUILayout.PropertyField(allowSceneActivationChanged);
			
			if(allowSceneActivationChanged.isExpanded) Space();
			EditorGUILayout.PropertyField(loadProgressChanged);
			
			Space();
			EditorGUILayout.PropertyField(sceneChangeStateChanged);
			_stateChangedEventsReorderableList.DoLayoutList();
		}
		
	}
}