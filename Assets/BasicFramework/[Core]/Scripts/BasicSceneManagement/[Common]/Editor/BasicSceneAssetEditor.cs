﻿using BasicFramework.Core.BasicTypes.Editor;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement.Editor
{
	[CustomEditor(typeof(BasicSceneAsset))]
	public class BasicSceneAssetEditor : ScriptableAssetBaseEditor<BasicSceneAsset>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var sceneAsset = serializedObject.FindProperty("_sceneAsset");
			
			var sceneName 	= serializedObject.FindProperty("_sceneName");
			var buildIndex 	= serializedObject.FindProperty("_buildIndex");
			var scenePath 	= serializedObject.FindProperty("_scenePath");
			var inBuild 	= serializedObject.FindProperty("_inBuild");
			
			Space();
			SectionHeader($"{nameof(BasicSceneAsset).CamelCaseToHumanReadable()} Properties");

			if (GUILayout.Button("Update Scene Details"))
			{
				EditorSceneManagementUtility.UpdateBasicSceneDetails();
			}
			
			Space();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(sceneAsset);

			foreach (var t in targets)
			{
				var scene = t as BasicSceneAsset;
				EditorSceneManagementUtility.UpdateBasicSceneDetails(scene);
			}

			if (inBuild.boolValue)
			{
				var sceneEnabled = buildIndex.intValue != -1;
				EditorGUILayout.HelpBox("Scene IN build and " + (sceneEnabled ? "ENABLED" : "DISABLED"), MessageType.Info);
			}
			else
			{
				EditorGUILayout.HelpBox("Scene NOT IN build", MessageType.Info);
			}
			
			EditorGUILayout.HelpBox("Name: " + sceneName.stringValue, MessageType.Info);
			EditorGUILayout.HelpBox("Build Index: " + buildIndex.intValue, MessageType.Info);
			EditorGUILayout.HelpBox("Scene Path: " + scenePath.stringValue, MessageType.Info); 
			
//			EditorGUILayout.Space();
//			EditorGUILayout.Space();
//			
			var unityScene = UnityEngine.SceneManagement.SceneManager.GetSceneByPath(scenePath.stringValue);
//			
//			EditorGUILayout.HelpBox("Is Valid: " + unityScene.IsValid(), MessageType.Info);
			EditorGUILayout.HelpBox("Build Index: " + unityScene.buildIndex, MessageType.Info);
//			EditorGUILayout.HelpBox("Is Dirty: " + unityScene.isDirty, MessageType.Info);
			EditorGUILayout.HelpBox("Is Loaded: " + unityScene.isLoaded, MessageType.Info);
			EditorGUILayout.HelpBox("Name: " + unityScene.name, MessageType.Info);
			EditorGUILayout.HelpBox("Path: " + unityScene.path, MessageType.Info);
//			EditorGUILayout.HelpBox("Root Count: " + unityScene.rootCount, MessageType.Info);
		}

	}
}