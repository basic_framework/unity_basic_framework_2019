﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BasicFramework.Core.SceneManagement
{
	[CreateAssetMenu(
		menuName = "Basic Framework/Core/Scene Management/Basic Scene Asset", 
		fileName = "asset_scene_",
		order = SceneManagementAssetMenuOrder.BASIC_SCENE_ASSET)
	]
	public class BasicSceneAsset : ScriptableAssetBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
#if UNITY_EDITOR
		[SerializeField] private SceneAsset _sceneAsset = default;

		// Only here to get rid on console warning that field is not being used
		protected SceneAsset SceneAsset => _sceneAsset;
#endif

		[SerializeField] private string _scenePath = default;
		[SerializeField] private int _buildIndex = default;
		[SerializeField] private string _sceneName = default;
		[SerializeField] private bool _inBuild = default;
		
		public string ScenePath => _scenePath;
		public int BuildIndex => _buildIndex;
		public string SceneName => _sceneName;
		public bool InBuild => _inBuild;

		public UnityEngine.SceneManagement.Scene UnityScene => SceneManager.GetSceneByPath(_scenePath);

		public bool IsLoaded => UnityScene.isLoaded;
		
	
		// ****************************************  METHODS  ****************************************\\

		public void SwitchToScene()
		{
			SceneSwitchManager.Instance.SwitchToScene(this);
		}
		
		public bool IsValid()
		{
			return Application.CanStreamedLevelBeLoaded(_scenePath);
		}
	
	}
	
	[Serializable]
	public class BasicSceneAssetUnityEvent : UnityEvent<BasicSceneAsset>{}
	
	[Serializable]
	public class BasicSceneAssetArrayUnityEvent : UnityEvent<BasicSceneAsset[]>{}
	
	[Serializable]
	public class BasicSceneAssetCompareUnityEvent : CompareValueUnityEventBase<BasicSceneAsset>{}
}