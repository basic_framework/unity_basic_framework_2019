﻿using System.Collections;
using BasicFramework.Core.BasicAnimation.Transitioner;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.Screen;
using UnityEngine;
using UnitySceneManagement = UnityEngine.SceneManagement;

namespace BasicFramework.Core.SceneManagement
{
	[AddComponentMenu("Basic Framework/Core/Scene Management/Scene Switch Manager")]
	public class SceneSwitchManager : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// Scene does not load 100% when it cannot be activated, the max it loads to is 0.9 then waits for allowSceneActivation to be set true 
		public const float MAX_PROGRESS_NO_ACTIVATE = 0.9f;
		
		// How long to wait in editor before logging reminder that a scene is ready to be activated
		public const float SCENE_READY_REMINDER_DELAY = 5f;
		
		
		//***** INSTANCE *****\\
		
		private static SceneSwitchManager _instance;
		public static SceneSwitchManager Instance
		{
			get
			{
				if (_instance != null) return _instance;
				
				_instance = FindObjectOfType<SceneSwitchManager>();
				if (_instance != null) return _instance;
				
				var go = new GameObject("[SceneChangeManager]");
				_instance = go.AddComponent<SceneSwitchManager>();
				return _instance;
			}
		}
		
		//***** VARIABLES *****\\
		
//		[Tooltip("NOT REQUIRED. Add a default screen fader if desired. " +
//		         "Just make sure it is also set to not destroy on load. " +
//		         "You can still specify a screen fader when changing scenes")]
//		[SerializeField] private TransitionerBase _defaultScreenFadeTransitioner = default;
		

		// These should not reference scene assets
		[Tooltip("Raised when the allow scene activation state changes. DO NOT USE SCENE REFERENCES")]
		[SerializeField] private BoolPlusUnityEvent _allowSceneActivationChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the load progress changes. DO NOT USE SCENE REFERENCES")]
		[SerializeField] private NormalizedFloatPlusUnityEvent _loadProgressChanged = new NormalizedFloatPlusUnityEvent();
		
		[Tooltip("Raised when the scene switch state changes. DO NOT USE SCENE REFERENCES")]
		[SerializeField] private SceneSwitchStatesUnityEvent _sceneSwitchStateChanged = new SceneSwitchStatesUnityEvent();
		
		[Tooltip("Raised when the scene switch state changes and conditions are met. DO NOT USE SCENE REFERENCES")]
		[SerializeField] private SceneSwitchStatesCompareUnityEvent[] _sceneSwitchStateChangedEvents = new SceneSwitchStatesCompareUnityEvent[0];
		
		private bool _allowSceneActivation;
		public bool AllowSceneActivation
		{
			get => _allowSceneActivation;
			set
			{
				if (_allowSceneActivation == value) return;
				_allowSceneActivation = value;
				AllowSceneActivationUpdated(value);
			}
		}
		
		private NormalizedFloat _loadProgress;
		public NormalizedFloat LoadProgress
		{
			get => _loadProgress;
			private set
			{
				if (_loadProgress == value) return;
				_loadProgress = value;
				LoadProgressUpdated(value);
			}
		}
		
		private SceneSwitchStates _sceneSwitchState;
		public SceneSwitchStates SceneSwitchState
		{
			get => _sceneSwitchState;
			private set
			{
				if (_sceneSwitchState == value) return;
				_sceneSwitchState = value;
				SceneChangeStateUpdated(value);
			}
		}

		private BasicSceneAsset _currentLoadingScene;
		private bool _switchSceneCoroutineRunning;


		// ****************************************  METHODS  ****************************************\\

		private void Awake()
		{
			// TODO: Test, it should get the current instance or create a new one
			if (_instance != null && _instance != this)
			{
				Destroy(gameObject);
				return;
			}

			_instance = this;
			
			DontDestroyOnLoad(this);
		}

		private void Start()
		{
			AllowSceneActivationUpdated(_allowSceneActivation);
			LoadProgressUpdated(_loadProgress);
			SceneChangeStateUpdated(_sceneSwitchState);
		}

		private void SceneChangeStateUpdated(SceneSwitchStates sceneSwitchState)
		{
			_sceneSwitchStateChanged.Invoke(sceneSwitchState);
			
			foreach (var stateChangedEvent in _sceneSwitchStateChangedEvents)
			{
				stateChangedEvent.SetCurrentValue(sceneSwitchState);
			}
		}
		
		private void LoadProgressUpdated(NormalizedFloat loadProgress)
		{
			_loadProgressChanged.Raise(loadProgress);
		}
		
		private void AllowSceneActivationUpdated(bool allowSceneActivation)
		{
			_allowSceneActivationChanged.Raise(allowSceneActivation);
		}

		public void SwitchToScene(BasicSceneAssetSingle basicSceneAssetSingle)
		{
			SwitchToScene(basicSceneAssetSingle.Value, null);
		}
		
		public void SwitchToScene(BasicSceneAsset basicSceneAsset)
		{
			SwitchToScene(basicSceneAsset, null);
		}

		public bool SwitchToScene(BasicSceneAsset basicSceneAsset, TransitionerBase screenFadeTransitioner)
		{
			if (basicSceneAsset == null)
			{
				Debug.LogError($"{name} => Cannot change scenes. Passed scene is null");
				return false;
			}
			
			if (!basicSceneAsset.IsValid())
			{
				Debug.LogError($"{name} => Cannot change to scene \"{basicSceneAsset.SceneName}\". It is not valid");
				return false;
			}
			
			if (_sceneSwitchState != SceneSwitchStates.Sleeping)
			{
				Debug.LogError($"{name} => Cannot change to scene \"{basicSceneAsset.SceneName}\". " +
				               $"Another scene is currently being loaded \"{(_currentLoadingScene == null ? "ERROR" : _currentLoadingScene.SceneName)}\"");
				return false;
			}

			if (_switchSceneCoroutineRunning)
			{
				Debug.LogError($"{name} => BIG BUG. Should never get here");
				return false;
			}

//			StartCoroutine(SwitchSceneCoroutine(basicSceneAsset,
//				screenFadeTransitioner == null ? Instance._defaultScreenFadeTransitioner : screenFadeTransitioner));
			StartCoroutine(SwitchSceneCoroutine(basicSceneAsset));

			return true;
		}

//		private IEnumerator SwitchSceneCoroutine(BasicSceneAsset basicSceneAsset, TransitionerBase screenFadeTransitioner)
		private IEnumerator SwitchSceneCoroutine(BasicSceneAsset basicSceneAsset)
		{
			_switchSceneCoroutineRunning = true;
			_currentLoadingScene = basicSceneAsset;
			
			AllowSceneActivation = true;
			LoadProgress = NormalizedFloat.Zero;
			
			
			//***** Load scene async *****\\
			
			// Start the async load process immediately
			var loadSceneAsyncOperation = UnitySceneManagement.SceneManager.LoadSceneAsync(basicSceneAsset.ScenePath, UnitySceneManagement.LoadSceneMode.Single);
			
			// Wait to allow scene activation, will be going off of this managers AllowSceneActivation
			loadSceneAsyncOperation.allowSceneActivation = false;

			SceneSwitchState = SceneSwitchStates.Loading;
			
			// Wait until the scene has loaded to proceed
			while (LoadProgress.Value < 1)
			{
				LoadProgress = new NormalizedFloat(loadSceneAsyncOperation.progress / MAX_PROGRESS_NO_ACTIVATE);
				yield return null;
			}
			
			
			//***** Wait for Allow Scene Activation *****\\
			
			if (!_allowSceneActivation)
			{
				SceneSwitchState = SceneSwitchStates.WaitingForSceneActivation;
				
				// Wait until the scene is activated
				var timer = SCENE_READY_REMINDER_DELAY;
				while (!_allowSceneActivation)
				{
					yield return null;
				
					if(!Application.isEditor) continue;
				
					timer -= Time.unscaledDeltaTime;

					if (timer > 0) continue;
					timer = SCENE_READY_REMINDER_DELAY;
					Debug.LogWarning($"{name} => Waiting for AllowSceneActivation to be true to change scenes \"{basicSceneAsset.SceneName}\"");
				}
			}
			
			
			//***** Fade Out (if necessary) *****\\
			
//			if (screenFadeTransitioner != null)
//			{
//				SceneSwitchState = SceneSwitchStates.FadingOut;
//				
//				while (screenFadeTransitioner != null && screenFadeTransitioner.TransitionState != TransitionStates.One)
//				{
//					if (screenFadeTransitioner.TransitionState != TransitionStates.ToOne)
//					{
//						screenFadeTransitioner.TransitionToOne();
//					}
//
//					yield return null;
//				}
//			}
			
			
			//***** Activate Scene *****\\
			
			SceneSwitchState = SceneSwitchStates.ActivatingScene;

			loadSceneAsyncOperation.allowSceneActivation = true;

			// Wait for the scene to actually finish loading
			while (!loadSceneAsyncOperation.isDone)
			{
				yield return null;
			}
			
			// Wait a frame for everything to run setup functions, may need to wait a bit longer
			yield return null;
			
			
			//***** Fade In (if necessary) *****\\
			
//			if (screenFadeTransitioner != null)
//			{
//				SceneSwitchState = SceneSwitchStates.FadingIn;
//				
//				while (screenFadeTransitioner != null && screenFadeTransitioner.TransitionState != TransitionStates.Zero)
//				{
//					if (screenFadeTransitioner.TransitionState != TransitionStates.ToZero)
//					{
//						screenFadeTransitioner.TransitionToZero();
//					}
//
//					yield return null;
//				}
//			}
			

			//***** Cleanup *****\\

			_currentLoadingScene = null;
			_switchSceneCoroutineRunning = false;
			
			SceneSwitchState = SceneSwitchStates.Sleeping;
		}
		
	}
}