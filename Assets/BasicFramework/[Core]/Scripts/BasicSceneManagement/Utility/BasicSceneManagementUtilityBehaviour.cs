﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Main.Utility
{
	public class BasicSceneManagementUtilityBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private bool _raiseSceneLoadedOnStart = default;
		
		[Space]
		[SerializeField] private UnityEvent _sceneLoaded = default;
		
	
		// ****************************************  METHODS  ****************************************\\

		private void OnEnable()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
		}
		
		private void OnDisable()
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
		}

		private void Start()
		{
			if (!_raiseSceneLoadedOnStart) return;
			//Debug.Log("Scene loaded start callback");
			_sceneLoaded.Invoke();
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
		{
			//Debug.Log($"Scene loaded. Scene: {scene}, Mode: {loadSceneMode}");
			
			switch (loadSceneMode)
			{
				case LoadSceneMode.Single:
					_sceneLoaded.Invoke();
					break;
				case LoadSceneMode.Additive:
					break;
				default:
					Debug.LogError($"{name}/{GetType().Name} => Load Scene Mode out of range, Value: {loadSceneMode}");
					break;
					//throw new ArgumentOutOfRangeException(nameof(loadSceneMode), loadSceneMode, null);
			}
		}
		
	}
}