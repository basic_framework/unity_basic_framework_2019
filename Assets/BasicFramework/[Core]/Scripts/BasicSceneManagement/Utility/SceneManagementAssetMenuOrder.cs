﻿namespace BasicFramework.Core.SceneManagement
{
	public static class SceneManagementAssetMenuOrder 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		public const int NONE = 0;
		
		private const int SECTION_ONE_START 		= 11;
		public const int BASIC_SCENE_ASSET 			= SECTION_ONE_START;
		public const int BASIC_SCENE_ASSET_EVENT 	= SECTION_ONE_START + 1;
		
		private const int SECTION_TWO_START 		= 30;
		public const int SCENE_SWITCH_STATE_SINGLE 	= SECTION_TWO_START + 1;
		

		// ****************************************  METHODS  ****************************************\\
	
	
	}
}