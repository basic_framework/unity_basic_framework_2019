﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement
{
    [AddComponentMenu("Basic Framework/Core/Scene Management/Basic Scene Event Listener")]
    public class BasicSceneAssetEventListener : ScriptableEventListenerBase<BasicSceneAsset, BasicSceneAssetEvent, BasicSceneAssetUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Should the response be raised if the scene is invalid")]
        [SerializeField] private bool _respondIfInvalid = true;
        
        [Tooltip("Response raised with the scene name string")]
        [SerializeField] private StringUnityEvent _responseSceneName = new StringUnityEvent();
        
        
        // ****************************************  METHODS  ****************************************\\

        protected override void OnEventRaised(BasicSceneAsset arg0)
        {
            if (!_respondIfInvalid && (arg0 == null || !arg0.IsValid()))
            {
                Debug.LogError($"{name} => received scene is invalid. Not responding");
                return;
            }
            
            base.OnEventRaised(arg0);

            var sceneName = arg0 == null ? "" : arg0.SceneName;
            
            _responseSceneName.Invoke(sceneName);
        }
        
    }
}