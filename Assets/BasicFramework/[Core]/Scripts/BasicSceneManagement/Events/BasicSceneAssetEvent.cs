﻿using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.SceneManagement
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scene Management/Basic Scene Event", 
        fileName = "event_basic_scene_",
        order = SceneManagementAssetMenuOrder.BASIC_SCENE_ASSET_EVENT)
    ]
    public class BasicSceneAssetEvent : ScriptableEventBase<BasicSceneAsset>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void Raise(BasicSceneAssetSingle basicSceneAssetSingle)
        {
            Raise(basicSceneAssetSingle.Value);
        }
        
    }
}