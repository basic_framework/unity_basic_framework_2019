﻿using BasicFramework.Core.ScriptableEvents.Editor;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(BasicSceneAssetEventListener))][CanEditMultipleObjects]
    public class BasicSceneAssetEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        BasicSceneAsset, 
        BasicSceneAssetEvent, 
        BasicSceneAssetUnityEvent,
        BasicSceneAssetEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            var respondIfInvalid = serializedObject.FindProperty("_respondIfInvalid");
            
            SectionHeader("Basic Scene Event Listener Properties");

            EditorGUILayout.PropertyField(respondIfInvalid);
        }

        protected override void DrawEventFields()
        {
            base.DrawEventFields();
            
            var responseSceneName = serializedObject.FindProperty("_responseSceneName");
            
            SectionHeader("Basic Scene Event Listener Events");
            
            EditorGUILayout.PropertyField(responseSceneName);
        }
    }
}