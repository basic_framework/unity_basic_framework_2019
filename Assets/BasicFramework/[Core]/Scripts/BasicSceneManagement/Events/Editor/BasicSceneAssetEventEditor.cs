﻿using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(BasicSceneAssetEvent))][CanEditMultipleObjects]
    public class BasicSceneAssetEventEditor : ScriptableEventBaseEditor<BasicSceneAsset, BasicSceneAssetEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(BasicSceneAssetEvent))]
    public class BasicSceneAssetEventPropertyDrawer : ScriptableEventBasePropertyDrawer<BasicSceneAsset, BasicSceneAssetEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}