﻿using BasicFramework.Core.BasicGui.Components.Editor;
using UnityEditor;

namespace BasicFramework.Core.SceneManagement.Editor
{
    [CustomEditor(typeof(BasicSceneAssetGuiDropdown))]
    public class BasicSceneAssetGuiDropdownEditor : TypedGuiDropdownBaseEditor
    <
        BasicSceneAsset, 
        BasicSceneAssetUnityEvent, 
        BasicSceneAssetArrayUnityEvent,
        BasicSceneAssetCompareUnityEvent, 
        BasicSceneAssetGuiDropdown
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}