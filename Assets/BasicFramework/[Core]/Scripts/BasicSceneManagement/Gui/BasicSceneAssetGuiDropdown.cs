﻿using BasicFramework.Core.BasicGui.Components;

namespace BasicFramework.Core.SceneManagement
{
    public class BasicSceneAssetGuiDropdown : TypedGuiDropdownBase
    <
	    BasicSceneAsset,
	    BasicSceneAssetUnityEvent,
	    BasicSceneAssetArrayUnityEvent,
	    BasicSceneAssetCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}