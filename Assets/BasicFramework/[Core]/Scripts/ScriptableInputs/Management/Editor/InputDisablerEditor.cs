using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(InputDisabler))]
    public class InputDisablerEditor : BasicBaseEditor<InputDisabler>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        private ReorderableList _inputsReorderableList;
        private ReorderableList _inputListsReorderableList;
		
	
        // ****************************************  METHODS  ****************************************\\

        protected override void OnEnable()
        {
	        base.OnEnable();
	        
            var inputs = serializedObject.FindProperty("_inputs");
            var inputLists = serializedObject.FindProperty("_inputLists");

            _inputsReorderableList = BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, inputs);
            _inputListsReorderableList = BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, inputLists);
        }
        
        protected override void DrawPropertyFields()
        {
	        base.DrawPropertyFields();
	        
	        Space();
	        
	        var maintainAcrossSceneChange = serializedObject.FindProperty("_maintainAcrossSceneChange");
	        var inputs = serializedObject.FindProperty("_inputs");
	        var inputLists = serializedObject.FindProperty("_inputLists");

	        GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
	        EditorGUILayout.PropertyField(maintainAcrossSceneChange);
			
	        Space();
	        BasicEditorGuiLayout.DragAndDropAreaIntoList<ScriptableInputBase>(
		        inputs, 
		        $"Drop {typeof(ScriptableInputBase).Name}'s Here"
	        );
            
	        Space();
	        _inputsReorderableList.DoLayoutList();
            
            
	        Space();
	        BasicEditorGuiLayout.DragAndDropAreaIntoList<ScriptableInputList>(
		        inputLists, 
		        $"Drop {typeof(ScriptableInputList).Name}'s Here"
	        );
            
	        Space();
	        _inputListsReorderableList.DoLayoutList();
	        
	        GUI.enabled = PrevGuiEnabled;
        }

    }
}