using System.Collections.Generic;
using BasicFramework.Core.Utility.Editor;
using UnityEngine;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
	[CustomEditor(typeof(InputManager))]
	public class InputManagerEditor : BasicBaseEditor<InputManager>
	{

		// **************************************** VARIABLES ****************************************\\
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;
		
		private GUIStyle _styleInputEnabled;
		private GUIStyle _styleInputDisabled;

		private static bool _managedInputListExpanded = true;
		private static bool _keepDisabledListExpanded = true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			_styleInputEnabled = new GUIStyle
			{
				normal = {textColor = BasicEditorStyles.Colors.MediumGreen},
				fontStyle = FontStyle.Bold
			};

			_styleInputDisabled = new GUIStyle
			{
				normal = {textColor = BasicEditorStyles.Colors.MediumRed},
				fontStyle = FontStyle.Bold
			};
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			DrawInputBaseList(InputManager.__managed_inputs__, "Managed Inputs", ref _managedInputListExpanded);
			
			Space();
			DrawInputBaseList(InputManager.__keep_disabled_inputs__, "Keep Disabled Inputs", ref _keepDisabledListExpanded);
		}

		private void DrawInputBaseList(IEnumerable<ScriptableInputBase> inputs, string header, ref bool isExpanded)
		{
			isExpanded = EditorGUILayout.Foldout(isExpanded, header, BasicEditorStyles.BoldFoldout);
			
			if (!isExpanded) return;
			
			foreach (var input in inputs)
			{
				var style = input.Enabled ? _styleInputEnabled : _styleInputDisabled;
				EditorGUILayout.LabelField(input.name, input.Enabled ? "Enabled" : "Disabled", style);
			}
		}

	}
}