using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
	[CustomEditor(typeof(AutoInputAdder))]
	public class AutoInputAdderEditor : BasicBaseEditor<AutoInputAdder>
	{

		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnFirstDrawInspectorFields()
		{
			base.OnFirstDrawInspectorFields();
			
			var inputs = serializedObject.FindProperty("_inputs");
			CleanAutoInputAdderList(inputs);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			
			var myStyle = GUI.skin.GetStyle("HelpBox");
			myStyle.richText = true;
			
			EditorGUILayout.HelpBox("This component is automatically controlled by the <b>BasicFramework Input System</b>", MessageType.Warning);
			
			EditorGUILayout.Space();
			EditorGUILayout.HelpBox("This component adds the Inputs below to the scene InputManager when destroyed. It destroys itself on awake.", MessageType.Info);
			EditorGUILayout.HelpBox("<b>DO NOT</b> manually add it to a gameobject", MessageType.Warning);
			EditorGUILayout.HelpBox("This component will <b>DESTROY</b> its GameObject on start", MessageType.Warning);
			
			myStyle.richText = false;
			
			var inputs = serializedObject.FindProperty("_inputs");

			if (GUILayout.Button("Clean List"))
			{
				CleanAutoInputAdderList(inputs);
			}
			
			EditorGUILayout.Space();
			
			EditorGUILayout.LabelField("Inputs", EditorStyles.boldLabel);

			if (inputs.arraySize == 0)
			{
				EditorGUILayout.LabelField("None");
			}
						
			for (int i = inputs.arraySize - 1; i >= 0; i--)
			{
				if (inputs.GetArrayElementAtIndex(i).objectReferenceValue == null)
				{
					BasicEditorArrayUtility.RemoveArrayElement(inputs, i);
				}
				
				EditorGUILayout.PropertyField(inputs.GetArrayElementAtIndex(i));
			}
			
			EditorGUILayout.Space();
			
		}

		private void CleanAutoInputAdderList(SerializedProperty inputs)
		{
			for (int i = inputs.arraySize - 1; i >= 0; i--)
			{
				var input = inputs.GetArrayElementAtIndex(i).objectReferenceValue;
				
				if (input == null)
				{
					BasicEditorArrayUtility.RemoveArrayElement(inputs, i);
					continue;
				}

				var referencedBy = BasicEditorUtility.FindReferencesInScene(input);

				// If it is only referenced by the auto adder, remove it
				if (referencedBy.Length != 1 || referencedBy[0] != TypedTarget) continue;

				//Debug.LogWarning("Removing from AutoInputAdder: " + input.name);
				BasicEditorArrayUtility.RemoveArrayElement(inputs, i);
			}
		}
		
	}
}