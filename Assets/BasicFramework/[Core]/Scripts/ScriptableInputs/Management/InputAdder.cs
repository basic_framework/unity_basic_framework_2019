using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BasicFramework.Core.ScriptableInputs
{
	[AddComponentMenu("Basic Framework/Core/Scriptable Inputs/Input Adder")]
	public class InputAdder : MonoBehaviour 
	{
		// Use this component to add inputs to input manager using the editor
		
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Will add the inputs every time a scene loads. " +
		         "Will set the gameobject to DontDestroyOnLoad if it is the root.")]
		[SerializeField] private bool _addOnSceneLoaded = true;
		
		[SerializeField] private ScriptableInputBase[] _inputs = new ScriptableInputBase[0];
		[SerializeField] private ScriptableInputList[] _inputLists = new ScriptableInputList[0];
		

		// ****************************************  METHODS  ****************************************\\

		private void Awake()
		{
			AddInputsToInputManager();

			if (!_addOnSceneLoaded)
			{
				Destroy(this);
				return;
			}

			if (transform.parent != null) return;
			DontDestroyOnLoad(gameObject);
		}

		private void OnEnable()
		{
			SceneManager.sceneLoaded += SceneLoaded;
		}
		
		private void OnDisable()
		{
			SceneManager.sceneLoaded -= SceneLoaded;
		}
		
		private void SceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
		{
			if (loadSceneMode == LoadSceneMode.Additive) return;
			AddInputsToInputManager();
		}
		
		private void AddInputsToInputManager()
		{
			InputManager.AddInput(_inputs);
			InputManager.AddInput(_inputLists);
		}
		
		// Input Base
		private bool Contains(ScriptableInputBase inputBase, bool lookInLists = false)
		{
			if (!lookInLists) return _inputs.Contains(inputBase);
			
			foreach (var inputList in _inputLists)
			{
				if (inputList != null && inputList.Contains(inputBase))
				{
					return true;
				}
			}

			return _inputs.Contains(inputBase);
		}
		
		// Checks to see if the Input is contained in any of the ScriptableInputAdders, Expensive
		public static bool ContainedInAny(ScriptableInputBase inputBase, bool includeInactive = true)
		{
			var scriptableInputAdders = FindObjectsOfType<InputAdder>();

			foreach (var scriptableInputAdder in scriptableInputAdders)
			{
				if (scriptableInputAdder.Contains(inputBase, true))
				{
					return true;
				}
			}

			return false;
		}
		
	}
}

