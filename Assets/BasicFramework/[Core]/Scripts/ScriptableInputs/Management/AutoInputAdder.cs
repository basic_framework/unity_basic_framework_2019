using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu("")]
    public class AutoInputAdder : MonoBehaviour 
    {

        // DO NOT MANUALLY USE THIS COMPONENT
		
        // **************************************** VARIABLES ****************************************\\
		
        private static AutoInputAdder _instance;
        public static AutoInputAdder Instance
        {
            get
            {
                if (_instance != null) return _instance;
				
                _instance = FindObjectOfType<AutoInputAdder>();
                if (_instance != null) return _instance;

                _instance = new GameObject("BASIC_FRAMEWORK_AUTO_INPUT_ADDER").AddComponent<AutoInputAdder>();
                _instance.transform.SetSiblingIndex(0);

                _createdWhilePlaying = Application.isPlaying;
				
                return _instance;
            }
        }
		
        [SerializeField] private List<ScriptableInputBase> _inputs = new List<ScriptableInputBase>();
        
        private static bool _createdWhilePlaying;
        

        // ****************************************  METHODS  ****************************************\\
		
        private void Awake()
        {
            if (_createdWhilePlaying) return;
            Destroy(gameObject);
        }
		
        private void OnDestroy()
        {
            InputManager.AddInput(_inputs);
        }
		
        private void OnDrawGizmos()
        {
            // Ensures only one of these components is active in the scene
			
            if (_instance == null)
            {
                _instance = this;
                return;
            }
			
            if (_instance == this) return;
			
            foreach (var inputBase in _inputs)
            {
                _instance.AddInput(inputBase);
            }
				
            DestroyImmediate(this);
        }
		
        public bool Contains(ScriptableInputBase inputBase)
        {
            return _inputs.Contains(inputBase);
        }
		
        public void AddInput(ScriptableInputBase input)
        {
            if (input == null) return;
            if (_inputs.Contains(input)) return;
			
            _inputs.Add(input);
        }
		
        public void RemoveInput(ScriptableInputBase input)
        {
            if (input == null) return;
            _inputs.Remove(input);
        }
		
    }
}