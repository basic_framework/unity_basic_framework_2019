using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu("Basic Framework/Core/Scriptable Inputs/Input Disabler")]
    public class InputDisabler : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Keep the inputs disabled across a scene change. This will set the gameobject to dont destroy on load")]
        [SerializeField] private bool _maintainAcrossSceneChange = true;
		
        [SerializeField] private ScriptableInputBase[] _inputs = new ScriptableInputBase[0];
        [SerializeField] private ScriptableInputList[] _inputLists = new ScriptableInputList[0];

        private bool _quitting = default;
		
		
        // ****************************************  METHODS  ****************************************\\

        private void Awake()
        {
            if (!_maintainAcrossSceneChange) return;
            DontDestroyOnLoad(gameObject);
        }

        private void OnEnable()
        {
            SetInputsCanBeEnabled(false);

            if (!_maintainAcrossSceneChange) return;
            SceneManager.sceneLoaded += SceneLoaded;
        }
		
        private void OnDisable()
        {
            SetInputsCanBeEnabled(true);
			
            if (!_maintainAcrossSceneChange) return;
            SceneManager.sceneLoaded -= SceneLoaded;
        }

        private void OnApplicationQuit()
        {
            _quitting = true;
        }

        private void SceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (loadSceneMode == LoadSceneMode.Additive) return;
            SetInputsCanBeEnabled(!enabled);
        }

        private void SetInputsCanBeEnabled(bool canBeEnabled)
        {
            // If we try to create an InputManager whilst quitting unity will throw an error
            if (_quitting) return;
			
            InputManager.SetInputCanBeEnabled(_inputs, canBeEnabled);
            InputManager.SetInputCanBeEnabled(_inputLists, canBeEnabled);
        }
		
    }
}