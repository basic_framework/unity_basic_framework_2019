﻿using System.Collections.Generic;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[AddComponentMenu("")]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_INPUT_MANAGER)]
	public class InputManager : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// TODO: Make sure this doesnt screw anything up
		private static bool _isQuitting;
		
		private static InputManager _instance;
		private static InputManager Instance
		{
			get
			{
				if (_isQuitting) return null;
				
				if (_instance != null) return _instance;
				
				_instance = FindObjectOfType<InputManager>();
				if (_instance != null) return _instance;
				
				var go = new GameObject("[InputManager]");
				_instance = go.AddComponent<InputManager>();
				return _instance;
			}
		}
		
		// List of inputs managed by the input manager
		private readonly HashSet<ScriptableInputBase> _managedInputs = new HashSet<ScriptableInputBase>();

		/// <summary>DO NOT MODIFY, USE AS READ ONLY</summary>
		public static HashSet<ScriptableInputBase> __managed_inputs__ => Instance._managedInputs;

		// List of inputs that need to be kept disabled
		private readonly HashSet<ScriptableInputBase> _keepDisabledInputs = new HashSet<ScriptableInputBase>();
		
		/// <summary>DO NOT MODIFY, USE AS READ ONLY</summary>
		public static HashSet<ScriptableInputBase> __keep_disabled_inputs__ => Instance._keepDisabledInputs;


		// ****************************************  METHODS  ****************************************\\

		private void Awake()
		{
			// The call to Instance will create the instance if it does not exist, or assign this to the instance
			if (Instance == null || Instance == this) return;	
			Debug.LogWarning("There is already an InputManager in this scene. Destroying InputManager on " + gameObject.name);				
			Destroy(this);
		}

		private void OnEnable()
		{
			SetAllInputsEnabled(true);
		}

		private void OnDisable()
		{
			SetAllInputsEnabled(false);
		}
		
		private void Update()
		{
			foreach (var input in _managedInputs)
			{
				input.UpdateInput();
			}
		}

		private void OnApplicationQuit()
		{
			_isQuitting = true;
		}


		//********** Add **********\\

		/// <summary>Adds the inputs in the sets to the InputManager</summary>
		/// <param name="inputLists"></param>
		public static void AddInput(IEnumerable<ScriptableInputList> inputLists)
		{
			if (inputLists == null) return;
			
			foreach (var inputList in inputLists)
			{
				AddInput(inputList.Values);
			}
		}

		/// <summary>Adds the inputs in the set to the InputManager</summary>
		/// <param name="inputList"></param>
		public static void AddInput(ScriptableInputList inputList)
		{
			AddInput(inputList.Values);
		}
		
		/// <summary>Adds the inputs in the array to the InputManager</summary>
		/// <param name="inputs"></param>
		public static void AddInput(IEnumerable<ScriptableInputBase> inputs)
		{
			if (inputs == null) return;
			
			foreach (var input in inputs)
			{
				AddInput(input);
			}
		}

		/// <summary>Adds the input to the InputManager</summary>
		/// <param name="input"></param>
		public static void AddInput(ScriptableInputBase input)
		{
			if (_isQuitting) return;
			if(input == null) return;
			
			if (!Instance._managedInputs.Add(input)) return;
			SetInputEnabled(input, true);
		}
		
		
		//********** Remove **********\\
		
		/// <summary>Removes the inputs in the sets from the InputManager</summary>
		/// <param name="inputLists"></param>
		public static void RemoveInput(IEnumerable<ScriptableInputList> inputLists)
		{
			if(inputLists == null) return;
			
			foreach (var inputList in inputLists)
			{
				RemoveInput(inputList.Values);
			}
		}

		/// <summary>Removes the inputs in the set from the InputManager</summary>
		/// <param name="inputList"></param>
		public static void RemoveInput(ScriptableInputList inputList)
		{
			RemoveInput(inputList.Values);
		}
		
		/// <summary>Removes the inputs in the array from the InputManager</summary>
		/// <param name="inputs"></param>
		public static void RemoveInput(IEnumerable<ScriptableInputBase> inputs)
		{
			if (inputs == null) return;
			
			foreach (var input in inputs)
			{
				RemoveInput(input);
			}
		}
		
		/// <summary>Removes the input from the InputManager</summary>
		/// <param name="input"></param>
		public static void RemoveInput(ScriptableInputBase input)
		{
			if (_isQuitting) return;
			if (input == null) return;

			if (!Instance._managedInputs.Contains(input)) return;

			// Set false before removing from list otherwise it won't get set false
			SetInputEnabled(input, false);
			Instance._managedInputs.Remove(input);
		}
		
		
		//********** Keep Disabled **********\\

		/// <summary>
		/// Set whether the passed inputs can be enabled.
		/// Use this to disable inputs at runtime without removing them from the input manager. 
		/// Note: The inputs DO NOT have to be in the manager at the time of setting this
		/// </summary>
		/// <param name="inputLists"></param>
		/// <param name="canBeEnabled">If the input is allowed to be set Enabled or not</param>
		public static void SetInputCanBeEnabled(IEnumerable<ScriptableInputList> inputLists, bool canBeEnabled)
		{
			if (inputLists == null) return;
			
			foreach (var inputList in inputLists)
			{
				SetInputCanBeEnabled(inputList.Values, canBeEnabled);
			}
		}

		/// <summary>
		/// Set whether the passed inputs can be enabled.
		/// Use this to disable inputs at runtime without removing them from the input manager. 
		/// Note: The inputs DO NOT have to be in the manager at the time of setting this
		/// </summary>
		/// <param name="inputList"></param>
		/// <param name="canBeEnabled">If the input is allowed to be set Enabled or not</param>
		public static void SetInputCanBeEnabled(ScriptableInputList inputList, bool canBeEnabled)
		{
			SetInputCanBeEnabled(inputList.Values, canBeEnabled);
		}

		/// <summary>
		/// Set whether the passed inputs can be enabled.
		/// Use this to disable inputs at runtime without removing them from the input manager. 
		/// Note: The inputs DO NOT have to be in the manager at the time of setting this
		/// </summary>
		/// <param name="inputs"></param>
		/// <param name="canBeEnabled">If the input is allowed to be set Enabled or not</param>
		public static void SetInputCanBeEnabled(IEnumerable<ScriptableInputBase> inputs, bool canBeEnabled)
		{
			if (inputs == null) return;
			
			foreach (var input in inputs)
			{
				SetInputCanBeEnabled(input, canBeEnabled);
			}
		}
		
		/// <summary>
		/// Set whether the passed inputs can be enabled.
		/// Use this to disable inputs at runtime without removing them from the input manager. 
		/// Note: The inputs DO NOT have to be in the manager at the time of setting this
		/// </summary>
		/// <param name="input"></param>
		/// <param name="canBeEnabled">If the input is allowed to be set Enabled or not</param>
		public static void SetInputCanBeEnabled(ScriptableInputBase input, bool canBeEnabled)
		{
			if (_isQuitting) return;
			if (input == null) return;

			if (canBeEnabled)
			{
				// If we succesfully remove the input from our keep disabled list try and enable it
				if (!Instance._keepDisabledInputs.Remove(input)) return;
				SetInputEnabled(input, true);
				return;
			}
			
			// Check if we are already keeping the input disabled
			if(!Instance._keepDisabledInputs.Add(input)) return;
			SetInputEnabled(input, false);
		}
		
		
		//********** Set Enabled ***********\\
		
		// Sets inputs enabled/disabled if they are being managed by the InputManager
		private static void SetInputEnabled(ScriptableInputBase input, bool enabled)
		{
			if (_isQuitting) return;
			if (input == null) return;

			// Can always disable the input, doesn't have to be in our list
			if (!enabled)
			{
				input.Enabled = false;
				return;
			}
			
			// TO enable input it must be in our list
			if (!Instance._managedInputs.Contains(input)) return;

			// If the input needs to be kept disabled, do not enable it
			if (Instance._keepDisabledInputs.Contains(input)) return;
			
			// Only enable input if the input manager is active
			input.Enabled = Instance.gameObject.activeSelf && Instance.enabled;
		}
		
		private void SetAllInputsEnabled(bool enable)
		{
			foreach (var input in _managedInputs)
			{
				SetInputEnabled(input, enable);
			}
		}
		
		
		//********** Utility **********\\
		
		/// <summary>
		/// Checks if the input is contained in the InputManager input list.
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static bool Contains(ScriptableInputBase input)
		{
			return Instance._managedInputs.Contains(input);
		}

	}
}