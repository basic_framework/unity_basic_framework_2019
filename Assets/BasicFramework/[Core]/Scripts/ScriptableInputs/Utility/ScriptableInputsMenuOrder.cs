namespace BasicFramework.Core.ScriptableInputs

{
    public static class ScriptableInputsMenuOrder 
    {
	
        // **************************************** VARIABLES ****************************************\\

        public const int INPUT_LIST = 0;
        
        public const int BOOL = 20;
        public const int ACTION = BOOL + 1;
		
        public const int AXIS 		= 40;
        public const int AXIS2 		= AXIS + 1;
        public const int AXIS3 		= AXIS + 2;
		
        public const int INT 		= 60;
        public const int FLOAT 		= INT + 1;
        public const int VECTOR2 	= INT + 2;
        public const int VECTOR3 	= INT + 3;
        public const int QUATERNION = INT + 4;

		
        // ****************************************  METHODS  ****************************************\\


    }
}