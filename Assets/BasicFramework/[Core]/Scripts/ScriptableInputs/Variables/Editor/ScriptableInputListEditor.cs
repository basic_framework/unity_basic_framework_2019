using BasicFramework.Core.ScriptableVariables.List.Editor;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(ScriptableInputList))][CanEditMultipleObjects]
    public class ScriptableInputListEditor : ScriptableListBaseEditor<ScriptableInputBase, ScriptableInputList>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
       
        
    }
    
    [CustomPropertyDrawer(typeof(ScriptableInputList))]
    public class ScriptableInputListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
}