using System;
using BasicFramework.Core.ScriptableVariables.List;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableInputs
{
	[CreateAssetMenu(
		menuName = "Basic Framework/Core/Scriptable Inputs/Input List",
		fileName = "_input_list", 
		order = ScriptableInputsMenuOrder.INPUT_LIST)
	]
    public class ScriptableInputList : ScriptableListBase<ScriptableInputBase>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class ScriptableInputListUnityEvent : UnityEvent<ScriptableInputList>{}
    
}