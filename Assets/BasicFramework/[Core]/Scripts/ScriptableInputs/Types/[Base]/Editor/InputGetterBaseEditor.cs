using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    public abstract class InputGetterBaseEditor<T, TInputSourceBase, TInputTypeBase, TUnityEvent, TCompareUnityEvent, TInputGetter> : BasicBaseEditor<TInputGetter>
	    where TInputSourceBase 		: InputSourceBase<T>
	    where TInputTypeBase 		: InputTypeBase<T, TInputSourceBase>
	    where TUnityEvent 			: UnityEvent<T>
	    where TCompareUnityEvent 	: CompareValueUnityEventBase<T>
		where TInputGetter			: InputGetterBase<T, TInputSourceBase, TInputTypeBase, TUnityEvent, TCompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

        private ReorderableList _valueCompareReorderableList;
        
	
        // ****************************************  METHODS  ****************************************\\

        protected override void OnEnable()
        {
	        base.OnEnable();
	        
	        var valueChangedCompare = serializedObject.FindProperty("_valueChangedCompare");

	        _valueCompareReorderableList =
		        BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, valueChangedCompare, true);
        }

        protected override void DrawPropertyFields()
        {
	        base.DrawPropertyFields();
	        
	        Space();
	        
	        var input = serializedObject.FindProperty("_input");
	       // var updateValue = serializedObject.FindProperty("_onUpdateValue");
//	        var valueChanged = serializedObject.FindProperty("_valueChanged");
	        
	        var raiseOnStates = serializedObject.FindProperty("_raiseOnStates");
	        
	        GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
	        BasicEditorGuiLayout.PropertyFieldNotNull(input);
	        GUI.enabled = PrevGuiEnabled;
	        
	        EditorGUILayout.PropertyField(raiseOnStates);
	        //if (input.objectReferenceValue == null) return;

	       // input.isExpanded = EditorGUILayout.Foldout(input.isExpanded, "Unity Events");
	        //if (!input.isExpanded) return;
			
//	        EditorGUILayout.Space();
//	        EditorGUILayout.PropertyField(valueChanged);
//	        _valueCompareReorderableList.DoLayoutList();
	        //EditorGUILayout.PropertyField(updateValue);
        }

        protected override void DrawEventFields()
        {
	        base.DrawEventFields();
	        
	        var valueChanged = serializedObject.FindProperty("_valueChanged");

	        Space();
	        
	        EditorGUILayout.PropertyField(valueChanged);
	        _valueCompareReorderableList.DoLayoutList();
        }
        
    }
}