using UnityEngine;
using UnityEditor;
using System;
using BasicFramework.Core.Utility.Editor;
using UnityEditorInternal;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
	public abstract class InputTypeBaseEditor<T, TInputSource, TInputTypeBase> : BasicBaseEditor<TInputTypeBase>
		where TInputSource : InputSourceBase<T>
		where TInputTypeBase : InputTypeBase<T, TInputSource>
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		private GUIStyle _inputActiveGuiStyle;

		private ReorderableList _sourcesReorderableList;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var sources = serializedObject.FindProperty("_sources");

			_sourcesReorderableList = BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, sources, true);

			_sourcesReorderableList.onAddCallback = SourceListAddCallback;
			_sourcesReorderableList.onRemoveCallback = SourceListRemoveCallback;
		}
		
		protected override void OnFirstDrawInspectorFields()
		{
			base.OnFirstDrawInspectorFields();
			
			_inputActiveGuiStyle = new GUIStyle(EditorStyles.boldLabel);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawHeaderFields();

			Space();
			DrawSourceList();
		}

		private void DrawHeaderFields()
		{
			
			SectionHeader("Input Base Fields");

			
//			var sources = serializedObject.FindProperty("_sources");
//
//			GUI.enabled = false;
//			EditorGUILayout.PropertyField(sources, true);
//			GUI.enabled = PrevGuiEnabled;

			var enabledText = TypedTarget.Enabled ? "Input Active" : "Input Inactive";

			BasicEditorStyles.ChangeGuiStyleTextColor(
				_inputActiveGuiStyle,
				TypedTarget.Enabled
					? BasicEditorStyles.Colors.MediumGreen
					: BasicEditorStyles.Colors.MediumRed
			);

			EditorGUILayout.LabelField(enabledText, _inputActiveGuiStyle);
		}
		
		private void DrawSourceList()
		{
			_sourcesReorderableList.DoLayoutList();
			
//			var sources = serializedObject.FindProperty("_sources");
//
//			for (int i = 0; i < sources.arraySize; i++)
//			{
//				var sourceProperty = sources.GetArrayElementAtIndex(i);
//
//				EditorGUILayout.PropertyField(sourceProperty, true);
//			}
		}
		
		private void SourceListAddCallback(ReorderableList list)
		{
			BasicEditorContextMenus.DerivedTypeSelectorContextMenu(typeof(TInputSource), AddSourceCallback);
		}
		
		private void SourceListRemoveCallback(ReorderableList list)
		{
			var index = list.index;
			
			Undo.RegisterCompleteObjectUndo(TypedTarget, $"Removing source at {index} source");
			TypedTarget.RemoveInputSource(index);
		}

		private void AddSourceCallback(Type type)
		{
			if (!type.IsSubclassOf(typeof(TInputSource)))
			{
				
				Debug.LogError($"Cannot assign type \"{type.Name}\" to {typeof(TInputSource).Name}");
				return;
			}
			
			Undo.RegisterCompleteObjectUndo(TypedTarget, $"Added {type.Name} source");
			TypedTarget.AddInputSource(Activator.CreateInstance(type));
		}

	}
}