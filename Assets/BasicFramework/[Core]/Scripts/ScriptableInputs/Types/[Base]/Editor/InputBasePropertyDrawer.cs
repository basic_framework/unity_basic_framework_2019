using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
	[CustomPropertyDrawer(typeof(ScriptableInputBase), true)]
	public class InputBasePropertyDrawer : BasicBasePropertyDrawer 
	{

		// **************************************** VARIABLES ****************************************\\
		
		private static readonly GUIContent AddButtonContent = new GUIContent("+", "Add to AutoInputAdder");
		private static readonly GUIContent DelButtonContent = new GUIContent("-", "Remove from AutoInputAdder");
		private static readonly GUIContent InputAdderLabelContent = new GUIContent("✔", "Input is contained in an InputAdder component");
		private static readonly GUIContent InputNullLabelContent = new GUIContent("NUL", "Please select an input");
		
		private const float BUTTON_WIDTH = 25f;

	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var isPlaying = Application.isPlaying;
			
			GUI.enabled = PrevGuiEnabled && !isPlaying;
			
			var inputBase = property.objectReferenceValue as ScriptableInputBase;
			var autoInputAdder = AutoInputAdder.Instance;
			
			var containedInAdder = !isPlaying && InputAdder.ContainedInAny(inputBase);
			var containedInAuto = !isPlaying &&autoInputAdder.Contains(inputBase);
			//var containedInInputManager = Application.isPlaying && InputManager.Contains(inputBase);
			
			var prevGuiBgColor = GUI.backgroundColor;

			if (inputBase != null)
			{
				GUI.backgroundColor = containedInAdder || containedInAuto 
				                                       //|| containedInInputManager
					? BasicEditorStyles.NotNullFieldColor
					: BasicEditorStyles.IsAndCanBeNullFieldColor;
			}

			var propertyRect = new Rect(position.x, position.y, position.width - BUTTON_WIDTH - BUFFER_WIDTH * 2, position.height);
			
			var buttonRect = new Rect(propertyRect.xMax + BUFFER_WIDTH, position.y, BUTTON_WIDTH, position.height);
			
			EditorGUI.PropertyField(propertyRect, property, label);

			GUI.backgroundColor = prevGuiBgColor;
			
			if (property.objectReferenceValue == null || inputBase == null)
			{
				GUI.Label(buttonRect, InputNullLabelContent);
				GUI.enabled = PrevGuiEnabled;
				return;
			}
			
			GUI.backgroundColor = new Color(0.9f, 0.9f, 0.9f);

			if (containedInAdder)
			{
				GUI.Label(buttonRect, InputAdderLabelContent);
			}
			else if (containedInAuto)
			{
				if (GUI.Button(buttonRect, DelButtonContent))
				{
					autoInputAdder.RemoveInput(inputBase);
				}
			}
			else
			{
				if (GUI.Button(buttonRect, AddButtonContent))
				{
					autoInputAdder.AddInput(inputBase);
				}
			}

			GUI.backgroundColor = prevGuiBgColor;
			
			GUI.enabled = PrevGuiEnabled;
		}
		
	}
}