using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

namespace BasicFramework.Core.ScriptableInputs
{
    public abstract class InputTypeBase<T, TInputSourceBase> : ScriptableInputBase 
        where TInputSourceBase : InputSourceBase<T>
    {
		
        // **************************************** VARIABLES ****************************************\\
        
        [SerializeReference] private List<TInputSourceBase> _sources = new List<TInputSourceBase>();

        private T _value;
		
        /// <summary>The inputs value taking into account if it is active</summary>
        public T Value => !Enabled ? default : _value;

        /// <summary>This inputs value, irrespective of if it is active. IF UNSURE, use Value</summary>
        public T RawValue
        {
            get => _value;
            protected set => _value = value;
        }
        
        protected List<TInputSourceBase> Sources => _sources;
        
        // The number of values that were set since the last frame
        protected int SetValuesCount { get; private set; }
        
        // A list of the input values directly set, i.e. set by code not through an input source
        private readonly T[] SetValues = new T[10];
		
        
        // ****************************************  METHODS  ****************************************\\
        
        protected abstract T CompareInputValues(T left, T right);

        protected override void UpdateRawValue()
        {
            // Get the value that was directly set
            RawValue = GetSetValue();

            // Loop through all of the source values and compare
            foreach (var source in Sources)
            {
                source.Update();
                RawValue = CompareInputValues(RawValue, source.Value);
            }
        }
        
        protected override void OnEnableUpdated(bool enabled)
        {
            base.OnEnableUpdated(enabled);
            
            // Reset set values on enable toggle
            SetValuesCount = 0;
        }
        
        public void SetValue(T value)
        {
            if (SetValuesCount >= SetValues.Length) return;
            SetValues[SetValuesCount] = value;
            SetValuesCount++;
        }
        
        // Goes through all the set values and returns the appropriate value, default(T) if no values set
        private T GetSetValue()
        {
            var value = default(T);

            for (int i = 0; i < SetValuesCount; i++)
            {
                value = CompareInputValues(value, SetValues[i]);
            }

            SetValuesCount = 0;

            return value;
        }
        
        
        //***** Add/Remove Source *****\\

        public void AddInputSource(Object inputSource)
        {
            var typedInputSource = inputSource as TInputSourceBase;
            if (typedInputSource == null)
            {
                Debug.LogError($"{name} => Could not ADD input source of type \"{inputSource.GetType().Name}\" to source list. " +
                               $"It does not derive from \"{typeof(TInputSourceBase).Name}\"");
                return;
            }
            
            AddInputSource(typedInputSource);
        }

        public void AddInputSource(TInputSourceBase inputSource)
        {
            if(inputSource == null) return;
            if (_sources.Contains(inputSource)) return;
            _sources.Add(inputSource);
        }
        
        public void RemoveInputSource(Object inputSource)
        {
            var typedInputSource = inputSource as TInputSourceBase;
            if (typedInputSource == null)
            {
                Debug.LogError($"{name} => Could not REMOVE input source of type \"{inputSource.GetType().Name}\" to source list. " +
                               $"It does not derive from \"{typeof(TInputSourceBase).Name}\"");
                return;
            }
            
            RemoveInputSource(typedInputSource);
        }
        
        public void RemoveInputSource(TInputSourceBase inputSource)
        {
            _sources.Remove(inputSource);
        }
        
        public void RemoveInputSource(int index)
        {
            if (index < 0 || index >= _sources.Count) return;
            _sources.RemoveAt(index);
        }

    }
}