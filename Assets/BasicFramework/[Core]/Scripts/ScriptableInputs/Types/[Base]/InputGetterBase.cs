using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableInputs
{
	public abstract class InputGetterBase<T, TInputSourceBase, TInputTypeBase, TUnityEvent, TCompareUnityEvent> : MonoBehaviour 
        where TInputSourceBase 		: InputSourceBase<T>
        where TInputTypeBase 		: InputTypeBase<T, TInputSourceBase>
        where TUnityEvent 			: UnityEvent<T>
		where TCompareUnityEvent 	: CompareValueUnityEventBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private TInputTypeBase _input = default;
        public TInputTypeBase Input => _input;
        
//        [Tooltip("Raised every update with the current value of the input")]
//        [SerializeField] private TUnityEvent _onUpdateValue = default;
//        public TUnityEvent OnUpdateValue => _onUpdateValue;

        [Tooltip("Raised when the input value is changed")]
        [SerializeField] private TUnityEvent _valueChanged = default;
        public TUnityEvent ValueChanged => _valueChanged;
        
        [Tooltip("Raised when value is changed and the condition is met")]
        [SerializeField] protected TCompareUnityEvent[] _valueChangedCompare = default;
        
        [HideInInspector]
        [EnumFlag("The ValueChanged event will be raised on the selected monobehaviour callbacks, regardless of whether it has changed")]
        [SerializeField] private SetupCallbackFlags _raiseOnStates = 0;
        
        private bool RaiseOnAwake => (_raiseOnStates & SetupCallbackFlags.Awake) != 0;
        private bool RaiseOnEnable => (_raiseOnStates & SetupCallbackFlags.Enable) != 0;
        private bool RaiseOnStart => (_raiseOnStates & SetupCallbackFlags.Start) != 0;
        
        private T _value;
        private T Value
        {
	        set
	        {
		        if (Equals(_value, value)) return;
		        _value = value;
		        ValueUpdated(_value);
	        }
        }
        

        // ****************************************  METHODS  ****************************************\\
        
        private void Awake()
        {
	        if (!RaiseOnAwake) return;
	        _value = _input.Value;
	        ValueUpdated(_value);
        }

        private void OnEnable()
        {
	        _value = _input.Value;
	        if (!RaiseOnEnable) return;
	        ValueUpdated(_value);
        }

        private void Start()
        {
	        if (!RaiseOnStart) return;
	        ValueUpdated(_value);
        }
        
        protected virtual void Update()
        {
	        Value = _input.Value;
        }
        
        private void ValueUpdated(T value)
        {
	        OnValueUpdated(value);
	        
	        foreach (var compareUnityEvent in _valueChangedCompare)
	        {
		        compareUnityEvent.SetCurrentValue(value);
	        }
	        
	        _valueChanged.Invoke(_value);
        }
		
        protected virtual void OnValueUpdated(T value){}
    }
}