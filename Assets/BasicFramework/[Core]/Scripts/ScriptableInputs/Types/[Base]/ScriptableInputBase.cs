using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    public abstract class ScriptableInputBase : ScriptableObject
    {
		
        // **************************************** VARIABLES ****************************************\\
		
        private bool _enabled;
        public bool Enabled
        {
            get => _enabled;
            set
            {
                if (_enabled == value) { return; }
                _enabled = value;
                EnableUpdated(_enabled);
            }
        }
		
        private int _lastFrameUpdated;
		
		
        // ****************************************  METHODS  ****************************************\\
        
        public void UpdateInput()
        {
            if (_lastFrameUpdated == Time.frameCount) return;
            _lastFrameUpdated = Time.frameCount;
            UpdateRawValue();
        }

        private void EnableUpdated(bool enabled)
        {
            OnEnableUpdated(_enabled);
        }
		
        protected abstract void UpdateRawValue();
		
        protected virtual void OnEnableUpdated(bool enabled) {}

        public void AddToInputManager()
        {
            InputManager.AddInput(this);
        }
		
        public void RemoveFromInputManager()
        {
            InputManager.RemoveInput(this);
        }

        public void SetInInputManager(bool addToInputManager)
        {
            if (addToInputManager)
            { 
                InputManager.AddInput(this);   
            }
            else
            {
                InputManager.RemoveInput(this);
            }
            
        }
		
        public void SetCanBeEnabled(bool canBeEnabled)
        {
            InputManager.SetInputCanBeEnabled(this, canBeEnabled);
        }

    }
}