using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
	public abstract class InputSourceBase<T>
    {
		
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private bool _enabled = true;
        public bool Enabled => _enabled;

        private T _value;
        public T Value => !_enabled ? default : _value;

		
        // ****************************************  METHODS  ****************************************\\

        public void Update()
        {
	        if (!_enabled) return;
	        _value = GetUpdateValue();
        }
        
        protected abstract T GetUpdateValue();
        
    }
}