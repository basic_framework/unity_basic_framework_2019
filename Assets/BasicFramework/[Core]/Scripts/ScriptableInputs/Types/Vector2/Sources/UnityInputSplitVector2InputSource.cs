using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputSplitVector2InputSource : Vector2InputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [SerializeField] private UnityInputAxisSplit _xSplitAxis = UnityInputAxisSplit.Default;
        [SerializeField] private UnityInputAxisSplit _ySplitAxis = UnityInputAxisSplit.Default;
	
        
        // ****************************************  METHODS  ****************************************\\

        protected override Vector2 GetUpdateValue()
        {
            return new Vector2(_xSplitAxis.Value, _ySplitAxis.Value);
        }
        
    }
}