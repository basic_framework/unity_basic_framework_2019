using System;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
    public class KeyboardVector2InputSource : Vector2InputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private float _scaler = 1;

        [Space]
        [SerializeField] private KeyCode _xPositive = KeyCode.D;
        [SerializeField] private KeyCode _xNegativie = KeyCode.A;
		
        [Space]
        [SerializeField] private KeyCode _yPositive = KeyCode.W;
        [SerializeField] private KeyCode _yNegative = KeyCode.S;
		

        // ****************************************  METHODS  ****************************************\\

        protected override Vector2 GetUpdateValue()
        {
	        var x = KeyboardUtility.KeyboardAxis(_xPositive, _xNegativie);
	        var y = KeyboardUtility.KeyboardAxis(_yPositive, _yNegative);
				
	        return new Vector2(x, y).normalized * _scaler;
        }
        
    }
}