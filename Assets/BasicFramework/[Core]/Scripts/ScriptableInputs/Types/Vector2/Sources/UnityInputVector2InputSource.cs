using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputVector2InputSource : Vector2InputSource
    {

        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityInputAxis _xAxis = UnityInputAxis.Default;
        [SerializeField] private UnityInputAxis _yAxis = UnityInputAxis.Default;
		

        // ****************************************  METHODS  ****************************************\\

        protected override Vector2 GetUpdateValue()
        {
            return new Vector2(_xAxis.Value, _yAxis.Value);
        }
        
    }
}