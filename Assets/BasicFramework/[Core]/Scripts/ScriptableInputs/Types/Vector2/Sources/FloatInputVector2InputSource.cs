using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
    public class FloatInputVector2InputSource : Vector2InputSource 
    {
		
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private FloatInput _xInput = default;
        [SerializeField] private FloatInput _yInput = default;
		

        // ****************************************  METHODS  ****************************************\\

        protected override Vector2 GetUpdateValue()
        {
	        _xInput.UpdateInput();
	        _yInput.UpdateInput();
			
	        return new Vector2(_xInput.RawValue, _yInput.RawValue);	
        }

    }
}