using System;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public abstract class Vector2InputSource : InputSourceBase<Vector2>{}
    
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Vector2", 
        fileName = "input_vector2_", 
        order = ScriptableInputsMenuOrder.VECTOR2)
    ]
    public class Vector2Input : InputTypeBase<Vector2, Vector2InputSource>
    {
        // TODO: Test
	
        // **************************************** VARIABLES ****************************************\\
		
        [Tooltip("Make the magnitude of the vector one. Use to turn a Vector into a direction")]
        [SerializeField] private bool _normalize = default;
        [SerializeField] private bool _invertX = default;
        [SerializeField] private bool _invertY = default;
		

        // ****************************************  METHODS  ****************************************\\
        
        public float GetSignedAngleFrom(Vector2 fromDirection)
        {
            if (Math.Abs(Value.sqrMagnitude) < Mathf.Epsilon) return 0f;
            return fromDirection == Vector2.zero ? 0f : Vector2.SignedAngle(fromDirection, Value);
        }
        
        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();

            var rawValue = RawValue;

            rawValue.x *= _invertX ? -1f : 1f;
            rawValue.y *= _invertY ? -1f : 1f;

            RawValue = _normalize ? rawValue.normalized : rawValue;
        }

        protected override Vector2 CompareInputValues(Vector2 left, Vector2 right)
        {
            return left.sqrMagnitude > right.sqrMagnitude ? left : right;
        }
        
        public static Vector2 CompareInputValuesStatic(Vector2 left, Vector2 right)
        {
            return left.sqrMagnitude > right.sqrMagnitude ? left : right;
        }
        
    }
}