using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Vector2)", 
	    ScriptableInputsMenuOrder.VECTOR2)
    ]
    public class Vector2InputGetter : InputGetterBase<Vector2, Vector2InputSource, Vector2Input, Vector2UnityEvent, Vector2CompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}