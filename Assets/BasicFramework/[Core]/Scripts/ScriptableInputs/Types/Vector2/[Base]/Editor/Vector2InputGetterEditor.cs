using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Vector2InputGetter))]
    public class Vector2InputGetterEditor : InputGetterBaseEditor
    <
	    Vector2, 
	    Vector2InputSource, 
	    Vector2Input, 
	    Vector2UnityEvent,
	    Vector2CompareUnityEvent,
	    Vector2InputGetter
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}