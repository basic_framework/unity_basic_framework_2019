﻿using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Axis 2", 
        fileName = "input_axis2_", 
        order = ScriptableInputsMenuOrder.AXIS2)]
    public class Axis2Input : Vector2Input 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();
            
            RawValue = Vector2.ClampMagnitude(RawValue, 1);
        }
        
    }
}