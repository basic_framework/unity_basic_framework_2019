using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Axis 2)", 
	    ScriptableInputsMenuOrder.AXIS2)]
    public class Axis2InputGetter : InputGetterBase
    <
	    Vector2, 
	    Vector2InputSource, 
	    Axis2Input, 
	    Vector2UnityEvent, 
	    Vector2CompareUnityEvent
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}