using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Axis2InputGetter))]
    public class Axis2InputGetterEditor : InputGetterBaseEditor
	    <
		    Vector2, 
		    Vector2InputSource, 
		    Axis2Input, 
		    Vector2UnityEvent,
		    Vector2CompareUnityEvent,
		    Axis2InputGetter
		> 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}