using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Axis2Input))]
    public class Axis2InputEditor : Vector2InputEditor
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
	        DrawAxis2EditorFields();
	        
	        Space();
	        base.DrawPropertyFields();
        }

        protected void DrawAxis2EditorFields()
        {
	        SectionHeader("Axis 2 Input Fields");
	        
            EditorGUILayout.HelpBox("Axis 2 will clamp the magnitude of the Vector2 to One", MessageType.Info);
        }

    }
}