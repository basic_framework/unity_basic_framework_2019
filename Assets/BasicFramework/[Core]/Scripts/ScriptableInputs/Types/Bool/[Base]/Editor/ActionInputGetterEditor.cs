using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;


namespace BasicFramework.Core.ScriptableInputs.Editor
{
	[CustomEditor(typeof(ActionInputGetter))]
	public class ActionInputGetterEditor : InputGetterBaseEditor
	<
		bool, 
		ActionInputSource, 
		ActionInput, 
		BoolUnityEvent, 
		BoolCompareValueUnityEvent,
		ActionInputGetter
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawEventFields()
		{
//			base.DrawEventFields();
			
			var valueChangedPlus = serializedObject.FindProperty("_valueChangedPlus");

			Space();
	        
			EditorGUILayout.PropertyField(valueChangedPlus);
		}
		
	}
}