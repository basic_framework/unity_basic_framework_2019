using System;
using UnityEngine;


namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
	public abstract class ActionInputSource : InputSourceBase<bool>{}
	
	[CreateAssetMenu(
		menuName = "Basic Framework/Core/Scriptable Inputs/Action",
		fileName = "input_action_", 
		order = ScriptableInputsMenuOrder.ACTION)
	]
	public class ActionInput : InputTypeBase<bool, ActionInputSource>
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		public enum ActionInputModes
		{
			Normal,
			Count,
			Hold
		}

		[SerializeField] private ActionInputModes _actionInputMode = ActionInputModes.Normal;
		public ActionInputModes Mode
		{
			get => _actionInputMode;
			set => _actionInputMode = value;
		}
	
		private bool _rawValue;
		private bool _prevRawValue;
		
		private bool _forceNextTrue;
		
		
		//***** Action Input Mode == Count *****\\
		
		private const float MULTI_DOWN_DELAY = 0.5f;

		[Tooltip("The number of times the sources need to transition " +
		         "from false to true in rapid succession for action to be true")]
		[SerializeField] private int _requiredCount = 1;
		
		[Tooltip("Should the count continue after reaching required count." +
		         "The count will only reset after required delay without input")]
		[SerializeField] private bool _unlimitedCount = default; 

		private bool _heldPrevReached;
		private float _lastTimeDown;
		private int _currentCount;
		
		/// <summary>
		/// The number of times the sources need to transition 
		/// from false to true in rapid succession for action to be true
		/// </summary>
		public int RequiredCount
		{
			get => _actionInputMode != ActionInputModes.Count ? 1 : _requiredCount;
			set => _requiredCount = Mathf.Max(value, 1);
		}

		/// <summary>
		/// The current count of transitions from false to true
		/// </summary>
		public int CurrentCount => _currentCount;

		/// <summary>
		/// Should the count continue after reaching required count.
		/// The count will only reset after required delay without a true source
		/// </summary>
		public bool UnlimitedCount
		{
			get => _unlimitedCount;
			set => _unlimitedCount = value;
		}
		
		
		//***** Action Input Mode == Hold *****\\
		
		[Tooltip("The duration (in seconds) that the sources " +
		         "needs to be consistantly true for the action to be true")]
		[SerializeField] private float _holdTime = 1;

		[Tooltip("Reset the held duration to zero once the target duration is reached")]
		[SerializeField] private bool _resetHoldOnComplete = default;
		
		[Tooltip("After reaching hold time should the action " +
		         "continue to be true until the sources are false")]
		[SerializeField] private bool _trueUntilReset = default;
		
		private float _holdStartTime;

		/// <summary>
		/// Reset the held duration to zero once the target duration is reached
		/// </summary>
		public bool ResetHoldOnComplete
		{
			get => _resetHoldOnComplete;
			set => _resetHoldOnComplete = value;
		}

		/// <summary>
		/// After reaching hold time should the action 
		/// continue to be true until the sources are false
		/// </summary>
		public bool TrueUntilReset
		{
			get => _trueUntilReset;
			set => _trueUntilReset = value;
		}

		/// <summary>
		/// The duration (in seconds) that the sources needs
		/// to be consistantly true for the action to be true
		/// </summary>
		public float HoldTime
		{
			get => _actionInputMode != ActionInputModes.Hold ? 0 : _holdTime;
			set => _holdTime = Mathf.Max(value, 0);
		}
		
		/// <summary>
		/// How long the sources have been continually true
		/// </summary>
		public float HeldDuration => !_rawValue ? 0 : Time.time - _holdStartTime;

		/// <summary>
		/// The normalized (0 to 1) hold progress. Will be 1 if HeldDuration is greater than HoldTime
		/// </summary>
		public float HeldProgress
		{
			get
			{
				if (_actionInputMode != ActionInputModes.Hold || _holdTime <= float.Epsilon)
				{
					return _rawValue ? 1 : 0;
				}

				return Mathf.Clamp01(HeldDuration / HoldTime);
			}
		}
		
		

		// ****************************************  METHODS  ****************************************\\

		private void OnValidate()
		{
			if (_holdTime < 0)
			{
				_holdTime = 0;
			}

			if (_requiredCount < 1)
			{
				_requiredCount = 1;
			}
		}

		protected override void UpdateRawValue()
		{
			base.UpdateRawValue();
			
			_rawValue = RawValue;
	
			// If normal mode set value to raw value, otherwise false. Will be set true by other conditions
			RawValue = _actionInputMode == ActionInputModes.Normal && _rawValue;
			
			// Check if raw value changed between updates
			if (_prevRawValue != _rawValue)
			{
				_prevRawValue = _rawValue;
				
				if (_prevRawValue)
				{
					// False to True
					
					// Reset hold values
					_holdStartTime = Time.time;
					_heldPrevReached = false;
					
					// Check count values
					if (Time.time - _lastTimeDown <= MULTI_DOWN_DELAY)
					{
						_currentCount++;
						
						if (_actionInputMode == ActionInputModes.Count)
						{
							if (_currentCount == _requiredCount)
							{
								RawValue = true;
							}
							else 
							{
								if (!_unlimitedCount && _currentCount > _requiredCount)
								{
									_currentCount = 1;
								}
								
								RawValue = false;
							}
						}
					}
					else
					{
						_currentCount = 1;
					}

					//Debug.Log("Count: " + _currentCount);
					
					_lastTimeDown = Time.time;					
				}
			}
			
			if (_prevRawValue)
			{
				// True to True
				//Debug.Log("Held Duration: " + HeldDuration + ", Held Progress: " + HeldProgress);
				
				if (_actionInputMode == ActionInputModes.Hold)
				{
					if (HeldProgress < 1) 		{ RawValue = false; }
					else if (_heldPrevReached) 	{ RawValue = _trueUntilReset; }
					else
					{
						RawValue = _heldPrevReached = true;
						
						if (_resetHoldOnComplete)
						{
							_holdStartTime = Time.time;
							_heldPrevReached = false;
						}
					}
				}
				else
				{
					if (!_heldPrevReached) { _heldPrevReached = HeldProgress >= 1; }
				}
			}
			else
			{
				// False to False
				if (_currentCount > 0 
				    && Time.time - _lastTimeDown > MULTI_DOWN_DELAY)
				{
					_currentCount = 0;
					//Debug.Log("Count: " + _currentCount);
				}	
			}

			if(!_forceNextTrue) return;
			_forceNextTrue = false;
			RawValue = true;
		}

		protected override bool CompareInputValues(bool left, bool right)
		{
			return left || right;
		}
		
		public static bool CompareInputValuesStatic(bool left, bool right)
		{
			return left || right;
		}
		
	}
}