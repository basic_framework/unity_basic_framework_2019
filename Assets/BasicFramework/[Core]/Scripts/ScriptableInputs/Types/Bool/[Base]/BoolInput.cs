﻿using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public abstract class BoolInputSource : InputSourceBase<bool>{}
    
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Bool",
        fileName = "input_bool_", 
        order = ScriptableInputsMenuOrder.BOOL)
    ]
    public class BoolInput : InputTypeBase<bool, BoolInputSource>
    {

        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private bool _invert = default;

        public bool Invert
        {
            get => _invert;
            set => _invert = value;
        }


        // ****************************************  METHODS  ****************************************\\
 
        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();

            // Invert as necessary
            if (!_invert) return;
            RawValue = !RawValue;
        }

        protected override bool CompareInputValues(bool left, bool right)
        {
            return left || right;
        }
        
        public static bool CompareInputValuesStatic(bool left, bool right)
        {
            return left || right;
        }
        
    }
}