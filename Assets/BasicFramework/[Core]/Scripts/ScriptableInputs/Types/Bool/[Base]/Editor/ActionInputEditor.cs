﻿using System;
using BasicFramework.Core.Utility;
using UnityEngine;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
	[CustomEditor(typeof(ActionInput))]
	public class ActionInputEditor : InputTypeBaseEditor<bool, ActionInputSource, ActionInput>
	{

		// **************************************** VARIABLES ****************************************\\
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			DrawActionEditorFields();
			
			Space();
			base.DrawPropertyFields();
		}

		private void DrawActionEditorFields()
		{
			SectionHeader("Action Input Fields");
			
			var modeProperty = serializedObject.FindProperty("_actionInputMode");
			
			var reqCount = serializedObject.FindProperty("_requiredCount");
			var unlimitedCount = serializedObject.FindProperty("_unlimitedCount");
			
			var holdTime = serializedObject.FindProperty("_holdTime");
			var resetHoldOnComplete = serializedObject.FindProperty("_resetHoldOnComplete");
			var trueUntilReset = serializedObject.FindProperty("_trueUntilReset");
			
			EditorGUILayout.PropertyField(modeProperty);

			var modeValue =
				EnumUtility.EnumNameToEnumValue<ActionInput.ActionInputModes>(modeProperty.enumNames[modeProperty.enumValueIndex]);
			
			switch (modeValue)
			{
				case ActionInput.ActionInputModes.Normal:
					break;
				
				case ActionInput.ActionInputModes.Count:
					
					EditorGUILayout.HelpBox("The sources need to transition from false to true a specified number " +
					                        "of times in rapid succession for the action to be true", MessageType.Info);
					
					EditorGUILayout.PropertyField(reqCount);
					EditorGUILayout.PropertyField(unlimitedCount);
					
					break;
				
				case ActionInput.ActionInputModes.Hold:
					
					EditorGUILayout.HelpBox(
						"The sources need to be continuously true a specified duration for the action to be true", 
						MessageType.Info);
					
					EditorGUILayout.PropertyField(holdTime);
					EditorGUILayout.PropertyField(resetHoldOnComplete);

					GUI.enabled = PrevGuiEnabled && !resetHoldOnComplete.boolValue;
					
					EditorGUILayout.PropertyField(trueUntilReset);
					
					GUI.enabled = PrevGuiEnabled;
					
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}


