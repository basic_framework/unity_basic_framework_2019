using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(BoolInputGetter))]
    public class BoolInputGetterEditor : InputGetterBaseEditor
    <
	    bool,
	    BoolInputSource, 
	    BoolInput, 
	    BoolUnityEvent, 
	    BoolCompareValueUnityEvent, 
	    BoolInputGetter
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawEventFields()
        {
//			base.DrawEventFields();
			
	        var valueChangedPlus = serializedObject.FindProperty("_valueChangedPlus");

	        Space();
	        
	        EditorGUILayout.PropertyField(valueChangedPlus);
        }
        
    }
}