using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Bool)",
        ScriptableInputsMenuOrder.BOOL
    )]
    public class BoolInputGetter : InputGetterBase
    <
        bool, 
        BoolInputSource, 
        BoolInput, 
        BoolUnityEvent, 
        BoolCompareValueUnityEvent
    >
    {

        // **************************************** VARIABLES ****************************************\\
        
        [SerializeField] private BoolPlusUnityEvent _valueChangedPlus = default;
        

        // ****************************************  METHODS  ****************************************\\

        protected override void OnValueUpdated(bool value)
        {
            base.OnValueUpdated(value);
            
            _valueChangedPlus.Raise(value);
        }
        
    }
}