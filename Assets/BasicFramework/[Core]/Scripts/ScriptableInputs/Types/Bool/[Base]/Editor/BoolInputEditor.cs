using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(BoolInput))]
    public class BoolInputEditor : InputTypeBaseEditor<bool, BoolInputSource, BoolInput>
    {

        // **************************************** VARIABLES ****************************************\\
		

        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            DrawBoolInputEditorFields();
            
            Space();
            base.DrawPropertyFields();
        }

        protected void DrawBoolInputEditorFields()
        {
            SectionHeader("Bool Input Fields");

            var invert = serializedObject.FindProperty("_invert");
            
            EditorGUILayout.PropertyField(invert);
        }
		
    }
}