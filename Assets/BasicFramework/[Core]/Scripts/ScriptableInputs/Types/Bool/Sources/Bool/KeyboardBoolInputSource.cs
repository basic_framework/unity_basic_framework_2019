using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class KeyboardBoolInputSource : BoolInputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private KeyCode _keyCode = KeyCode.None;
        [SerializeField] private KeyCode _modifier = KeyCode.None;
        

        // ****************************************  METHODS  ****************************************\\

        protected override bool GetUpdateValue()
        {
            return _modifier == KeyCode.None
                ? Input.GetKey(_keyCode)
                : Input.GetKey(_keyCode) && Input.GetKey(_modifier);
        }
		
    }
}