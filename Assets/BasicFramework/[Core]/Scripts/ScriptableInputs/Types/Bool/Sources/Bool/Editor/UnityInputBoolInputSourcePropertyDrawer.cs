using BasicFramework.Core.ScriptableInputs;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput.Editor
{
    [CustomPropertyDrawer(typeof(UnityInputBoolInputSource), true)]
    public class UnityInputBoolInputSourcePropertyDrawer : BasicBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
		
        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            TempRect = position;
            TempRect.height = SingleLineHeight;

            property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
            TempRect.y += TempRect.height + BUFFER_HEIGHT;

            if (!property.isExpanded) return;
            
            var enabled = property.FindPropertyRelative("_enabled");
            var button = property.FindPropertyRelative("_button");

            EditorGUI.PropertyField(TempRect, enabled);
            TempRect.y += TempRect.height + BUFFER_HEIGHT;


            //TempRect.width = EditorGUIUtility.labelWidth - BUFFER_WIDTH;
            TempRect = EditorGUI.PrefixLabel(TempRect, GetGuiContent(button));
            UnityInputEditorUtility.UnityAxisNameField(TempRect, button);
            
//            var buttonName = property.FindPropertyRelative("_buttonName");
//            var state = property.FindPropertyRelative("_state");
//            
//            TempRect = EditorGUI.PrefixLabel(TempRect, label);
//            TempRect.width -= KEY_STATE_WIDTH + BUFFER_WIDTH;
//            
//            UnityInputEditorUtility.UnityAxisNameField(TempRect, buttonName);
//
//            TempRect.x += TempRect.width + BUFFER_WIDTH;
//            TempRect.width = KEY_STATE_WIDTH;
//            
//            EditorGUI.PropertyField(TempRect, state, GUIContent.none);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var height = SingleLineHeight;
            
            if (!property.isExpanded)
            {
                return height;
            }
            
            height += SingleLineHeight + BUFFER_HEIGHT;
            height += SingleLineHeight + BUFFER_HEIGHT;
            
            return height;
        }
        
    }
}