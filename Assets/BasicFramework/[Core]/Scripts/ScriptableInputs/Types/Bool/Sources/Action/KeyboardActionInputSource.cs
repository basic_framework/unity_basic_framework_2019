﻿using System;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
	public class KeyboardActionInputSource : ActionInputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private KeyboardInput _keyboardInput = KeyboardInput.Default;
		

		// ****************************************  METHODS  ****************************************\\

		protected override bool GetUpdateValue()
		{
			return _keyboardInput.Value;
		}
		
	}
}