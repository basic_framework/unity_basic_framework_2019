using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputActionInputSource : ActionInputSource
    {

        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private UnityInputButton _button = UnityInputButton.Default;
		
		
        // **************************************** METHODS ****************************************\\
		
        private void OnEnable()
        {
            _button.ResetButtonExistsCheck();
        }

        protected override bool GetUpdateValue()
        {
            return _button.Value;
        }
    }
}