using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputBoolInputSource : BoolInputSource
    {

        // **************************************** VARIABLES ****************************************\\

        [Tooltip("The name of the button in the unity input list")]
        [SerializeField] private string _button = "";
		
        private bool _buttonExists;
        private bool _buttonChecked;
        
		
        // **************************************** METHODS ****************************************\\
		
        private void OnEnable()
        {
            _buttonChecked = false;
        }

        protected override bool GetUpdateValue()
        {
            if (!_buttonChecked)
            {
                _buttonChecked = true;
                _buttonExists = _button != "" && UnityInputUtility.IsAxisInUnityInputManager(_button, Application.isEditor);
            }
            
            return _buttonExists && Input.GetButton(_button);
        }
    }
}