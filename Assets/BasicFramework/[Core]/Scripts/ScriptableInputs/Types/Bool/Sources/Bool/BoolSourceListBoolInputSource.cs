using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    public class BoolInputsBoolInputSource : BoolInputSource
    {
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BoolInput[] _sourceList = default;


        // ****************************************  METHODS  ****************************************\\

        protected override bool GetUpdateValue()
        {
            foreach (var input in _sourceList)
            {
                if (!input.Value) return false;
            }
            
            return true;
            //return Input.GetKey(_keyCode);
        }
    }
}