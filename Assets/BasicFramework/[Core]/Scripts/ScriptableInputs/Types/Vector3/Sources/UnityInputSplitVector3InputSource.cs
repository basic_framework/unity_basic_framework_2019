using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputSplitVector3 : Vector3InputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [SerializeField] private UnityInputAxisSplit _xSplitAxis = UnityInputAxisSplit.Default;
        [SerializeField] private UnityInputAxisSplit _ySplitAxis = UnityInputAxisSplit.Default;
        [SerializeField] private UnityInputAxisSplit _zSplitAxis = UnityInputAxisSplit.Default;
	
        
        // ****************************************  METHODS  ****************************************\\

        protected override Vector3 GetUpdateValue()
        {
            return new Vector3(_xSplitAxis.Value, _ySplitAxis.Value, _zSplitAxis.Value);
        }
        
    }
}