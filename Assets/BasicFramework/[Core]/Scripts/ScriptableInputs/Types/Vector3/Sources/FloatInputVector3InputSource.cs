using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class FloatInputVector3 : Vector3InputSource 
    {
		

        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private FloatInput _xInput = default;
        [SerializeField] private FloatInput _yInput = default;
        [SerializeField] private FloatInput _zInput = default;


        // ****************************************  METHODS  ****************************************\\

        protected override Vector3 GetUpdateValue()
        {
            _xInput.UpdateInput();
            _yInput.UpdateInput();
            _zInput.UpdateInput();
			
            return new Vector3(
                _xInput.RawValue, 
                _yInput.RawValue, 
                _zInput.RawValue
            );	
        }
        
    }
}