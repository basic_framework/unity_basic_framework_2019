using System;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
    public class KeyboardVector3 : Vector3InputSource 
    {
	    
        // **************************************** VARIABLES ****************************************\\
        [SerializeField] private float _scaler = 1;
		
        [Space]
        [SerializeField] private KeyCode _xPositive = KeyCode.D;
        [SerializeField] private KeyCode _xNegativie = KeyCode.A;
		
        [Space]
        [SerializeField] private KeyCode _yPositive = KeyCode.E;
        [SerializeField] private KeyCode _yNegative = KeyCode.Q;
		
        [Space]
        [SerializeField] private KeyCode _zPositive = KeyCode.W;
        [SerializeField] private KeyCode _zNegative = KeyCode.S;
        
		
        // ****************************************  METHODS  ****************************************\\

        protected override Vector3 GetUpdateValue()
        {
	        var x = KeyboardUtility.KeyboardAxis(_xPositive, _xNegativie);
	        var y = KeyboardUtility.KeyboardAxis(_yPositive, _yNegative);
	        var z = KeyboardUtility.KeyboardAxis(_zPositive, _zNegative);
				
	        return new Vector3(x, y, z).normalized * _scaler;
        }
        
    }
}