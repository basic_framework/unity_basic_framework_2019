using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputVector3 : Vector3InputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityInputAxis _xAxis = UnityInputAxis.Default;
        [SerializeField] private UnityInputAxis _yAxis = UnityInputAxis.Default;
        [SerializeField] private UnityInputAxis _zAxis = UnityInputAxis.Default;
		

        // ****************************************  METHODS  ****************************************\\
	
        protected override Vector3 GetUpdateValue()
        {
            return new Vector3( _xAxis.Value, _yAxis.Value, _zAxis.Value);
        }
        
    }
}