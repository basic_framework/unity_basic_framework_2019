using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Axis 3)", 
        ScriptableInputsMenuOrder.AXIS3)]
    public class Axis3InputGetter : InputGetterBase
    <
	    Vector3, 
	    Vector3InputSource, 
	    Axis3Input, 
	    Vector3UnityEvent, 
	    Vector3CompareUnityEvent
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}