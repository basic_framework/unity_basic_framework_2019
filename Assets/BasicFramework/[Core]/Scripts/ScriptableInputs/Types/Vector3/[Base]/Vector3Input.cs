﻿using System;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public abstract class Vector3InputSource : InputSourceBase<Vector3>{}
    
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Vector3", 
        fileName = "input_vector3_", 
        order = ScriptableInputsMenuOrder.VECTOR3)]
    public class Vector3Input : InputTypeBase<Vector3, Vector3InputSource>
    {
        // TODO: Test

        // **************************************** VARIABLES ****************************************\\
		
        [Tooltip("Make the magnitude of the vector one. Use to turn a Vector into a direction")]
        [SerializeField] private bool _normalize = default; 
        [SerializeField] private bool _invertX = default;
        [SerializeField] private bool _invertY = default;
        [SerializeField] private bool _invertZ = default;
		
	
        // ****************************************  METHODS  ****************************************\\

        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();
            
            var rawValue = RawValue;

            rawValue.x *= _invertX ? -1f : 1f;
            rawValue.y *= _invertY ? -1f : 1f;
            rawValue.z *= _invertZ ? -1f : 1f;

            RawValue = _normalize ? rawValue.normalized : rawValue;
        }

        protected override Vector3 CompareInputValues(Vector3 left, Vector3 right)
        {
            return left.sqrMagnitude > right.sqrMagnitude ? left : right;
        }
        
        public static Vector3 CompareInputValuesStatic(Vector3 left, Vector3 right)
        {
            return left.sqrMagnitude > right.sqrMagnitude ? left : right;
        }
        
    }
}