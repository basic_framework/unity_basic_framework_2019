using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Vector3)", 
        ScriptableInputsMenuOrder.VECTOR3)
    ]
    public class Vector3InputGetter : InputGetterBase
    <
	    Vector3, 
	    Vector3InputSource, 
	    Vector3Input, 
	    Vector3UnityEvent, 
	    Vector3CompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}