using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Axis3Input))]
    public class Axis3InputEditor : Vector3InputEditor
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            DrawAxis3EditorFields();
	        
            Space();
            base.DrawPropertyFields();
        }

        protected void DrawAxis3EditorFields()
        {
            SectionHeader("Axis 3 Input Fields");
	        
            EditorGUILayout.HelpBox("Axis 3 will clamp the magnitude of the Vector3 to One", MessageType.Info);
        }

    }
}