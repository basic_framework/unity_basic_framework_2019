using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Vector3InputGetter))]
    public class Vector3InputGetterEditor : InputGetterBaseEditor
    <
        Vector3, 
        Vector3InputSource, 
        Vector3Input, 
        Vector3UnityEvent,
        Vector3CompareUnityEvent,
        Vector3InputGetter
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}