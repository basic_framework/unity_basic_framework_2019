using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Vector3Input))]
    public class Vector3InputEditor : InputTypeBaseEditor<Vector3, Vector3InputSource, Vector3Input> 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        private static readonly GUIContent XGuiContent = new GUIContent("X", "Invert X axis");
        private static readonly GUIContent YGuiContent = new GUIContent("Y", "Invert Y axis");
        private static readonly GUIContent ZGuiContent = new GUIContent("Z", "Invert Z axis");
        private const float INVERT_LABEL_WIDTH = 14f;
        private const float INVERT_FIELD_WIDTH = 30f;
		
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            DrawVector3InputEditorFields();
			
            Space();
            base.DrawPropertyFields();
        }

        private void DrawVector3InputEditorFields()
        {
            SectionHeader("Vector2 Input Fields");
			
            var invertX = serializedObject.FindProperty("_invertX");
            var invertY = serializedObject.FindProperty("_invertY");
            var invertZ = serializedObject.FindProperty("_invertZ");

            var normalize = serializedObject.FindProperty("_normalize");

            EditorGUILayout.BeginHorizontal();
			
            var prevLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = INVERT_LABEL_WIDTH;
			
            EditorGUILayout.LabelField("Invert", GUILayout.MaxWidth(prevLabelWidth - INVERT_LABEL_WIDTH - 4));
			
            EditorGUILayout.PropertyField(invertX, XGuiContent, GUILayout.MaxWidth(INVERT_FIELD_WIDTH));
            EditorGUILayout.PropertyField(invertY, YGuiContent, GUILayout.MaxWidth(INVERT_FIELD_WIDTH));
            EditorGUILayout.PropertyField(invertZ, ZGuiContent, GUILayout.MaxWidth(INVERT_FIELD_WIDTH));
			
            EditorGUIUtility.labelWidth = prevLabelWidth;
			
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(normalize);
        }
	
    }
}