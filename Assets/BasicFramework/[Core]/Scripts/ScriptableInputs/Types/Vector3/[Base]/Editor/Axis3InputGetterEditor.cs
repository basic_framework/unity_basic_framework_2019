using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(Axis3InputGetter))]
    public class Axis3InputGetterEditor : InputGetterBaseEditor
    <
        Vector3, 
        Vector3InputSource, 
        Axis3Input, 
        Vector3UnityEvent,
        Vector3CompareUnityEvent,
        Axis3InputGetter
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}