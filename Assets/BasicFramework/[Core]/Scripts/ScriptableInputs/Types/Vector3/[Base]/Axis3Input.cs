using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Axis 3", 
        fileName = "input_axis3_", 
        order = ScriptableInputsMenuOrder.AXIS3)]
    public class Axis3Input : Vector3Input 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();
            
            RawValue = Vector3.ClampMagnitude(RawValue, 1);
        }
        
    }
}