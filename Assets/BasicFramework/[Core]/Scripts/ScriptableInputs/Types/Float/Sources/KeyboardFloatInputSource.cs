using System;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
	public class KeyboardFloatInputSource : FloatInputSource
	{

		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("Min is the value when negative, Max is the value when positive, Middle is the default value")]
		[SerializeField] private RangedFloat _range = RangedFloat.Default;
		
		[SerializeField] private KeyCode _positiveKey = default;
		[SerializeField] private KeyCode _negativeKey = default;
		
		[Tooltip("Does not require a modifier or no modifier be active")]
		[SerializeField] private bool _anyModifier = true;
		[SerializeField] private KeyboardModifiers _modifier = default;
		
		[Tooltip("If the key changes from positive to negative jump to the default value")]
		[SerializeField] private bool _snap = default;
		
		[Tooltip("How long it takes to go from Min to Max, in seconds")]
		[Range(0, 1)] 
		[SerializeField] private float _rangeChangeTime = 0.5f;
		
		[SerializeField] private SymmetricalEasingTypes _easingType = SymmetricalEasingTypes.Linear;
		
		private float _lastNormalizedValue = 0.5f;	
		
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override float GetUpdateValue()
		{
			// Check the inputs and calculate the raw normalised value
			var rawNormalisedValue = _range.NormalizedValue;
			
			if (_anyModifier || KeyboardUtility.ModifierState == _modifier)
			{
				if (Input.GetKey(_positiveKey))
				{
					// Jump to the middle point
					if (_snap &&  _lastNormalizedValue < _range.NormalizedValue)
					{
						_lastNormalizedValue = _range.NormalizedValue;
					}
					
					rawNormalisedValue = 1f;
				}
				else if (Input.GetKey(_negativeKey))
				{
					// Jump to the middle point
					if (_snap && _lastNormalizedValue > _range.NormalizedValue)
					{
						_lastNormalizedValue = _range.NormalizedValue;
					}

					rawNormalisedValue = 0f;
				}
			}
			
			// If change time is zero jump to the value
			if (_rangeChangeTime < Mathf.Epsilon)
			{
				_lastNormalizedValue = rawNormalisedValue;
				return _range.NormalizedToRangedValue(rawNormalisedValue);
			}
			
			// Move toward the value at required speed
			var speed = _range.Range / _rangeChangeTime;
			
			_lastNormalizedValue = MathUtility.MoveTo(
				_lastNormalizedValue, 
				rawNormalisedValue, 
				speed * Time.deltaTime
				);
			
			return _range.NormalizedToRangedValue(Easing.Ease(_easingType, _lastNormalizedValue));
		}
		
	}
}