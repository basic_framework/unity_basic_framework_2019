using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
	[Serializable]
    public class UnityInputSplitFloatInputSource : FloatInputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private UnityInputAxisSplit _splitAxis = UnityInputAxisSplit.Default;

		
        // **************************************** METHODS ****************************************\\
	
        protected override float GetUpdateValue()
        {
	        return _splitAxis.Value;
        }
    }
}