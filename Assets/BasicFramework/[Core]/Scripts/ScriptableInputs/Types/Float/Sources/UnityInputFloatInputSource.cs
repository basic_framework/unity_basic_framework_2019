using System;
using BasicFramework.Core.Utility.UnityInput;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class UnityInputFloatInputSource : FloatInputSource
    {

        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityInputAxis _axis = UnityInputAxis.Default;

		
        // **************************************** METHODS ****************************************\\

        protected override float GetUpdateValue()
        {
            return _axis.Value;
        }
        
    }
}