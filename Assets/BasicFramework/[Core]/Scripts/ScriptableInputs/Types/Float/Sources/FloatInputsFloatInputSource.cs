using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public class FloatInputsFloatInputSource : FloatInputSource
    {
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Sets the value to the highest value in the array. " +
                 "Note: do not need to add float inputs to Input Manager")]
        [SerializeField] private FloatInput[] _floatInputs = new FloatInput[0];
        
	
        // ****************************************  METHODS  ****************************************\\

        protected override float GetUpdateValue()
        {
            if (_floatInputs.Length == 0) return default;

            var workingValue = 0f;
            
            foreach (var floatInput in _floatInputs)
            {
                floatInput.UpdateInput();
                workingValue = FloatInput.CompareInputValuesStatic(workingValue, floatInput.RawValue);
            }

            return workingValue;
        }
        
    }
}