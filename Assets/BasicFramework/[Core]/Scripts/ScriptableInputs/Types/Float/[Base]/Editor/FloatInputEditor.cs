using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(FloatInput))]
    public class FloatInputEditor : InputTypeBaseEditor<float, FloatInputSource, FloatInput>
    {

        // **************************************** VARIABLES ****************************************\\
		

        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            DrawFloatInputEditorFields();
            
            Space();
            base.DrawPropertyFields();
        }

        protected void DrawFloatInputEditorFields()
        {
            SectionHeader("Float Input Fields");

            var invert = serializedObject.FindProperty("_invert");
            
            EditorGUILayout.PropertyField(invert);
        }
		
    }
}