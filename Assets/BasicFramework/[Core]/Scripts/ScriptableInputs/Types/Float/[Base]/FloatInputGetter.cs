using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Float)",
	    ScriptableInputsMenuOrder.FLOAT
	    )]
    public class FloatInputGetter : InputGetterBase
    <
	    float, 
	    FloatInputSource, 
	    FloatInput, 
	    FloatUnityEvent, 
	    FloatCompareUnityEvent
    >
    {

        // **************************************** VARIABLES ****************************************\\
		
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}