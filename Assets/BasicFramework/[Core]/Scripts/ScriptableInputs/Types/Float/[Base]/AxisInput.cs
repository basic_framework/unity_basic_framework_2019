﻿using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Axis", 
        fileName = "input_axis_", 
        order = ScriptableInputsMenuOrder.AXIS)
    ]
    public class AxisInput : FloatInput
    {
		
        // **************************************** VARIABLES ****************************************\\
        
        [Tooltip("Clamp between 0-1. Negative values are discarded")]
        [SerializeField] private bool _positiveOnly = default;

        /// <summary> Clamp between 0-1. Negative values are discarded </summary>
        public bool PositiveOnly
        {
            get => _positiveOnly;
            set => _positiveOnly = value;
        }
		
        [Tooltip("Stores magnitude of input. Negative values converted to positive")]
        [SerializeField] private bool _magnitudeOnly = default;

        /// <summary> Stores magnitude of input. Negative values converted to positive </summary>
        public bool MagnitudeOnly
        {
            get => _magnitudeOnly;
            set => _magnitudeOnly = value;
        }
        
		
        // ****************************************  METHODS  ****************************************\\

        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();
            
            if (_positiveOnly)
            {
                RawValue = Mathf.Clamp01(RawValue);
                return;
            }

            if (_magnitudeOnly)
            {
                RawValue = Mathf.Abs(RawValue);
            }
				
            RawValue = Mathf.Clamp(RawValue, -1, 1);
        }

    }
}