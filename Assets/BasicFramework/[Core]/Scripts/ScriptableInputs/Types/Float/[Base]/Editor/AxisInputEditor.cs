using UnityEngine;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(AxisInput))]
    public class AxisInputEditor : FloatInputEditor
    {

        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            DrawAxisEditorFields();
            
            Space();
            base.DrawPropertyFields();
        }

        private void DrawAxisEditorFields()
        {
            SectionHeader("Axis Input Fields");
            
            var positiveOnly = serializedObject.FindProperty("_positiveOnly");
            var magnitudeOnly = serializedObject.FindProperty("_magnitudeOnly");
            
            if (positiveOnly.boolValue)
            {
                EditorGUILayout.HelpBox(
                    "The value will be clamped between 0 and 1, negative values will be dropped", 
                    MessageType.Info);
            }
            else if (magnitudeOnly.boolValue)
            {
                EditorGUILayout.HelpBox(
                    "The magnitude of the value will be clamped between 0 and 1", 
                    MessageType.Info);
            }
            else
            {
                EditorGUILayout.HelpBox(
                    "The value will be clamped between -1 and 1. Use FloatInput for unclamped value", 
                    MessageType.Info);
            }
            
            GUI.enabled = PrevGuiEnabled && !magnitudeOnly.boolValue;
            EditorGUILayout.PropertyField(positiveOnly);
			
            GUI.enabled = PrevGuiEnabled && !positiveOnly.boolValue;
            EditorGUILayout.PropertyField(magnitudeOnly);

            GUI.enabled = PrevGuiEnabled;
        }
        
    }
}