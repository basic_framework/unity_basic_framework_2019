using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Inputs/Getter/Input Getter (Axis)", 
	    ScriptableInputsMenuOrder.AXIS)
    ]
    public class AxisInputGetter : InputGetterBase
    <
	    float, 
	    FloatInputSource, 
	    AxisInput, 
	    FloatUnityEvent, 
	    FloatCompareUnityEvent
    >
    {
		
        // **************************************** VARIABLES ****************************************\\

	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}