using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(FloatInputGetter))]
    public class FloatInputGetterEditor : InputGetterBaseEditor
    <
	    float, 
	    FloatInputSource, 
	    FloatInput, 
	    FloatUnityEvent,
	    FloatCompareUnityEvent,
	    FloatInputGetter
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}