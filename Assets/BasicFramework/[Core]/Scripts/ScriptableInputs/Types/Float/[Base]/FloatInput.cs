﻿using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableInputs
{
    [Serializable]
    public abstract class FloatInputSource : InputSourceBase<float>{}
    
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Inputs/Float",
        fileName = "input_float_", 
        order = ScriptableInputsMenuOrder.FLOAT)
    ]
    public class FloatInput : InputTypeBase<float, FloatInputSource>
    {

        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private bool _invert = default;

        public bool Invert
        {
            get => _invert;
            set => _invert = value;
        }
		

        // ****************************************  METHODS  ****************************************\\
        
        protected override void UpdateRawValue()
        {
            base.UpdateRawValue();

            // Invert as necessary
            RawValue *= _invert ? -1 : 1;
        }

        protected override float CompareInputValues(float left, float right)
        {
            return Mathf.Abs(left) > Mathf.Abs(right) ? left : right;
        }
        
        public static float CompareInputValuesStatic(float left, float right)
        {
            return Mathf.Abs(left) > Mathf.Abs(right) ? left : right;
        }
        
    }
}