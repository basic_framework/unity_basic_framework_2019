using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableInputs.Editor
{
    [CustomEditor(typeof(AxisInputGetter))]
    public class AxisInputGetterEditor : InputGetterBaseEditor
	    <
		    float, 
		    FloatInputSource, 
		    AxisInput, 
		    FloatUnityEvent,
		    FloatCompareUnityEvent,
		    AxisInputGetter
		>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}