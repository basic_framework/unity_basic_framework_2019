﻿using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/XR/Debug/Debug Rig XR")]
	public class DebugRigXr : BasicRigXrBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override bool GetUserPresent()
		{
			return false;
		}
		
	}
}