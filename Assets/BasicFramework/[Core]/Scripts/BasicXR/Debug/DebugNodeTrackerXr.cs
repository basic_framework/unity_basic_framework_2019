﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/XR/Debug/Debug Node Tracker XR")]
	public class DebugNodeTrackerXr : BasicNodeTrackerXrBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Set if you want this node to register as being tracked")]
		[SerializeField] private bool _isTrackingDebug = default;

		
		public bool IsTrackingDebug
		{
			get => _isTrackingDebug;
			set => _isTrackingDebug = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		protected override bool GetIsTracking()
		{
			return _isTrackingDebug;
		}

		protected override BasicPose GetLocalPose()
		{
			return new BasicPose(transform);
		}
		
	}
}