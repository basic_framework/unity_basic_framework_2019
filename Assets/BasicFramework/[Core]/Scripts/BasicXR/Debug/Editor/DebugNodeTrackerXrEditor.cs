﻿using BasicFramework.Core.Utility.ExtensionMethods;
using BasicFramework.Core.XR.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug
{
	[CustomEditor(typeof(DebugNodeTrackerXr))]
	public class DebugNodeTrackerXrEditor : BasicNodeTrackerXrBaseEditor<DebugNodeTrackerXr>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var isTrackingDebug = serializedObject.FindProperty("_isTrackingDebug");
			
			Space();
			SectionHeader($"{nameof(DebugNodeTrackerXr).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(isTrackingDebug);
		}
		
	}
}