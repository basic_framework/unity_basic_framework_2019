﻿using BasicFramework.Core.XR.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug.Editor
{
	[CustomEditor(typeof(DebugRigXr))]
	public class DebugRigXrEditor : BasicRigXrBaseEditor<DebugRigXr>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}