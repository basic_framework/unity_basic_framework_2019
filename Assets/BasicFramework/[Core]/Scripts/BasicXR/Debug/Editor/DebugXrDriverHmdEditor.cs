﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug.Editor
{
    [CustomEditor(typeof(DebugXrDriverHmd))]
    public class DebugXrDriverHmdEditor : BasicBaseEditor<DebugXrDriverHmd>
    {
	
        // **************************************** VARIABLES ****************************************\\

        protected override bool HasDebugSection => true;
		
		
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawDebugFields()
        {
            base.DrawDebugFields();
            
            var editorOverrideIsActive = serializedObject.FindProperty("_editorOverrideIsActive");
			
            EditorGUILayout.PropertyField(editorOverrideIsActive);
            
            EditorGUILayout.LabelField(nameof(TypedTarget.IsActive).CamelCaseToHumanReadable(), 
                TypedTarget.IsActive.ToString());
        }

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            var basicRigXr = serializedObject.FindProperty("_basicRigXr");
            var extents = serializedObject.FindProperty("_extents");
            var simulateOriginFloor = serializedObject.FindProperty("_simulateOriginFloor");
            var startHeight = serializedObject.FindProperty("_startHeight");
			
            var speed = serializedObject.FindProperty("_speed");
			
            var rotateSensitivity = serializedObject.FindProperty("_rotateSensitivity");
            var invertVertical = serializedObject.FindProperty("_invertVertical");
			
            Space();
            SectionHeader($"{nameof(DebugXrDriverHmd).CamelCaseToHumanReadable()} Properties");
			
            BasicEditorGuiLayout.PropertyFieldNotNull(basicRigXr);

            Space();
            EditorGUILayout.PropertyField(simulateOriginFloor);

            GUI.enabled = PrevGuiEnabled && simulateOriginFloor.boolValue; 
            EditorGUILayout.PropertyField(startHeight);
            GUI.enabled = PrevGuiEnabled;
			
            //Space();
            EditorGUILayout.PropertyField(extents);
			
            //Space();
            EditorGUILayout.PropertyField(speed);
			
            //Space();
            EditorGUILayout.PropertyField(rotateSensitivity);
            EditorGUILayout.PropertyField(invertVertical);
        }
		
    }
}