﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.XR.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/XR/Debug/Debug XR Driver HMD")]
	public class DebugXrDriverHmd : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Set the driver active in the editor regardless if the hmd is being tracked currently")]
		[SerializeField] private bool _editorOverrideIsActive = default;
		
		[Tooltip("The xr rig component. This is used to disable the behaviour when a user is present")]
		[SerializeField] private BasicRigXrBase _basicRigXr = default;
		
		[Tooltip("Act as if the hmd position is relative to the floor level, i.e. y value will never be negative")]
		[SerializeField] private bool _simulateOriginFloor = true;

		[Tooltip("The height to start off above the floor")]
		[SerializeField] private float _startHeight = 1.7f;
		
		[Tooltip("The extents from the origin the hmd can move (in local space).\n" +
		         "Note: X and Z values are +/-. Y value is half the total height")]
		[SerializeField] private Vector3 _extents = Vector3.one;
		
		[Tooltip("How fast the hmd should translate")]
		[SerializeField] private float _speed = 1f;
		
		[Tooltip("The sensitivity of the mouse rotation")]
		[SerializeField] private float _rotateSensitivity = 0.25f;
		
		[Tooltip("Invert the up/down rotation (rotation about local x)")]
		[SerializeField] private bool _invertVertical = true;

		
		private readonly KeyboardInputAxis _inputAxisX = new KeyboardInputAxis(KeyCode.D, KeyCode.A);
		private readonly KeyboardInputAxis _inputAxisY = new KeyboardInputAxis(KeyCode.E, KeyCode.Q);
		private readonly KeyboardInputAxis _inputAxisZ = new KeyboardInputAxis(KeyCode.W, KeyCode.S);
		
		private Vector3 _mouseDelta;
		private Vector3 _lastMousePosition;

		public bool IsActive => Application.isPlaying && (!_basicRigXr.TrackingHmd || Application.isEditor && _editorOverrideIsActive);
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Reset()
		{
			_basicRigXr = GetComponentInParent<BasicRigXrBase>();
		}

		private void OnEnable()
		{
			_basicRigXr.Recentered.AddListener(OnBasicRigXrRecentered);
			_basicRigXr.TrackingHmdChanged.AddListener(OnTrackingHmdChanged);
		}
		
		private void OnDisable()
		{
			_basicRigXr.Recentered.RemoveListener(OnBasicRigXrRecentered);
			_basicRigXr.TrackingHmdChanged.RemoveListener(OnTrackingHmdChanged);
		}

		private void Start()
		{
			var transformRig = _basicRigXr.TransformRig;
			
			var startPosition = _simulateOriginFloor 
				? transformRig.InverseTransformPoint(Vector3.up * _startHeight) 
				: transformRig.position;
			
			SetPosition(startPosition);
		}

		private void Update()
		{
			_mouseDelta = Input.mousePosition - _lastMousePosition;
			_lastMousePosition = Input.mousePosition;
			
			UpdateTranslation();
			UpdateRotation();
		}

		private void OnDrawGizmosSelected()
		{
			if (_basicRigXr == null) return;
			
			var transformRig = _basicRigXr.TransformRig;
			if(transformRig == null) return;

			var nodeTrackerHmd = _basicRigXr.NodeTrackerHmd;
			if(nodeTrackerHmd == null) return;
			
			var transformHmd = nodeTrackerHmd.TransformTracked;
			if(transformHmd == null) return;
			
			var centre = _simulateOriginFloor 
				? transformRig.TransformPoint(Vector3.up * _extents.y) 
				: transformRig.position;
			
			BasicGizmoUtility.DrawCube(centre, transformRig.rotation, _extents * 2);
		}
		
		
		//***** callbacks *****\\
		
		private void OnBasicRigXrRecentered()
		{
			SetPosition(_basicRigXr.NodeTrackerHmd.TransformTracked.position);
		}
		
		private void OnTrackingHmdChanged(bool value)
		{
			SetPosition(_basicRigXr.NodeTrackerHmd.TransformTracked.position);
		}
		
		
		//***** functions *****\\

		private void UpdateTranslation()
		{
			if (!IsActive) return;
			
			if (!Input.GetKey(KeyCode.Mouse1)) return;
			
			var transformHmd = _basicRigXr.NodeTrackerHmd.TransformTracked;
			var direction = new Vector3(_inputAxisX.Value, _inputAxisY.Value, _inputAxisZ.Value).normalized;
			var velocity = _speed * Time.deltaTime * (transformHmd.rotation * direction);
			
			SetPosition(transformHmd.position + velocity);
		}

		private void UpdateRotation()
		{
			if (!IsActive) return;
			
			if (!Input.GetKey(KeyCode.Mouse1)) return;
			
			var transformHmd = _basicRigXr.NodeTrackerHmd.TransformTracked;
			
			var mouseInput = _mouseDelta * _rotateSensitivity;

			var position = transformHmd.position;
			transformHmd.RotateAround(position, Vector3.up, mouseInput.x);
			transformHmd.RotateAround(position, transformHmd.right, mouseInput.y * (_invertVertical ? -1 : 1));
		}
		
		private void SetPosition(Vector3 desiredPosition)
		{
			if (!IsActive) return;
			
			var transformHmd = _basicRigXr.NodeTrackerHmd.TransformTracked;
			var transformRig = _basicRigXr.TransformRig;

			var desiredPositionRelative = transformRig.InverseTransformPoint(desiredPosition);
			
			desiredPositionRelative.x = Mathf.Clamp(desiredPositionRelative.x, _extents.x * -1f, _extents.x);
			desiredPositionRelative.z = Mathf.Clamp(desiredPositionRelative.z, _extents.z * -1f, _extents.z);
			
			desiredPositionRelative.y = _simulateOriginFloor 
				? Mathf.Clamp(desiredPositionRelative.y, 0, _extents.y * 2f) 
				: Mathf.Clamp(desiredPositionRelative.y, _extents.y * -1f, _extents.y);

			transformHmd.position = transformRig.TransformPoint(desiredPositionRelative);
		}
		
	}
}