﻿using System;
using UnityEngine;

namespace BasicFramework.Core.XR
{
	
	[Flags]
	public enum RecenterProperties
	{
		XZ_Horizontal = 1 << 0,
		Y_Vertical = 1 << 1,
		Yaw_Heading = 1 << 2
	}
	
	
	// TODO: Update SetToLocalZero to SetToLocalPose and allow specifying custom Pose
	// Possibly add another option to move to transform with easing
	public enum TrackingLostModes
	{
		DoNothing,
		SetToLocalZero,
		SetToWorldTransform
	}
	
}