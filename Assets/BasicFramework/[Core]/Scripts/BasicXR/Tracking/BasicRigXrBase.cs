﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.XR
{
	public abstract class BasicRigXrBase : MonoBehaviour 
	{
		// XR Rig Transform Heirarchy
		// parent_rig_xr
		//	- parent_tracked_devices_offset
		//		- tracked_node_hmd
		//		- tracked_node_controller_hand_left
		//		- tracked_node_controller_hand_right
		//		- tracked_node_...
		
		// **************************************** VARIABLES ****************************************\\
		
		// Properties
		[Tooltip("The transform of the rig. This is the transform that should be moved in digital world")]
		[SerializeField] private Transform _transformRig = default;

		[Tooltip("The transform used to offset the tracked devices")]
		[SerializeField] private Transform _transformOffset = default;

		[SerializeField] private BasicNodeTrackerXrBase _nodeTrackerHmd = default;
		[SerializeField] private BasicNodeTrackerXrBase _nodeTrackerControllerHandLeft = default;
		[SerializeField] private BasicNodeTrackerXrBase _nodeTrackerControllerHandRight = default;
		
		[Tooltip("Clear the hmd height on recenter, i.e. the position of the hmd will match the xr rig position")] 
		[SerializeField] private bool _clearHeightOnRecenter = default;
		
		// Events
		[SerializeField] private BoolPlusUnityEvent _userPresentChanged = default;

		// TODO: possibly don't need these hear as they are just pass through events
		[SerializeField] private BoolPlusUnityEvent _trackingHmdChanged = default;
		[SerializeField] private BoolPlusUnityEvent _trackingControllerHandLeftChanged = default;
		[SerializeField] private BoolPlusUnityEvent _trackingControllerHandRightChanged = default;

		[Tooltip("Raised after tracking has been recentered")]
		[SerializeField] private UnityEvent _recentered = default;

		
		public Transform TransformRig => _transformRig;
		
		public BasicNodeTrackerXrBase NodeTrackerHmd => _nodeTrackerHmd;
		public BasicNodeTrackerXrBase NodeTrackerControllerHandLeft => _nodeTrackerControllerHandLeft;
		public BasicNodeTrackerXrBase NodeTrackerControllerHandRight => _nodeTrackerControllerHandRight;

		public bool ClearHeightOnRecenter
		{
			get => _clearHeightOnRecenter;
			set => _clearHeightOnRecenter = value;
		}
		
		public BoolUnityEvent UserPresentChanged => _userPresentChanged.Value;

		public BoolUnityEvent TrackingHmdChanged => _trackingHmdChanged.Value;
		public BoolUnityEvent TrackingControllerHandLeftChanged => _trackingControllerHandLeftChanged.Value;
		public BoolUnityEvent TrackingControllerHandRightChanged => _trackingControllerHandRightChanged.Value;

		public UnityEvent Recentered => _recentered;


		private bool _userPresent;
		public bool UserPresent
		{
			get => _userPresent;
			private set
			{
				if (_userPresent == value) return;
				_userPresent = value;
				UserPresentUpdated(value);
			}
		}

		private bool _trackingHmd;
		public bool TrackingHmd
		{
			get => _trackingHmd;
			private set
			{
				if (_trackingHmd == value) return;
				_trackingHmd = value;
				TrackingHmdUpdated(value);
			}
		}

		private bool _trackingControllerHandLeft;
		public bool TrackingControllerHandLeft
		{
			get => _trackingControllerHandLeft;
			private set
			{
				if (_trackingControllerHandLeft == value) return;
				_trackingControllerHandLeft = value;
				TrackingControllerHandLeftUpdated(value);
			}
		}
		
		private bool _trackingControllerHandRight;
		public bool TrackingControllerHandRight
		{
			get => _trackingControllerHandRight;
			private set
			{
				if (_trackingControllerHandRight == value) return;
				_trackingControllerHandRight = value;
				TrackingControllerHandRightUpdated(value);
			}
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** abstract/virtual *****\\

		protected abstract bool GetUserPresent();
		
		
		//***** mono *****\\

		private void Reset()
		{
			_transformRig = transform;
		}

		protected virtual void Start()
		{
			UserPresentUpdated(_userPresent);
			TrackingHmdUpdated(_trackingHmd);
			TrackingControllerHandLeftUpdated(_trackingControllerHandLeft);
			TrackingControllerHandRightUpdated(_trackingControllerHandRight);
		}

		protected virtual void Update()
		{
			UserPresent = GetUserPresent();
			
			// Possibly don't need to have references to these here
			TrackingHmd = _nodeTrackerHmd.IsTracking;
			TrackingControllerHandLeft = _nodeTrackerControllerHandLeft.IsTracking;
			TrackingControllerHandRight = _nodeTrackerControllerHandRight.IsTracking;
		}


		//***** property updated *****\\
		
		protected virtual void UserPresentUpdated(bool value)
		{
			_userPresentChanged.Raise(value);
		}
		
		protected virtual void TrackingHmdUpdated(bool value)
		{
			_trackingHmdChanged.Raise(value);
		}
		
		protected virtual void TrackingControllerHandLeftUpdated(bool value)
		{
			_trackingControllerHandLeftChanged.Raise(value);
		}
		
		protected virtual void TrackingControllerHandRightUpdated(bool value)
		{
			_trackingControllerHandRightChanged.Raise(value);
		}
		
		
		//***** functions *****\\
		
		public void Recenter()
		{
			var transformHmd = _nodeTrackerHmd.transform;
			var directionUp = _transformRig.up;
			
			// Heading
			var headingDesired 	= Vector3.ProjectOnPlane(_transformRig.forward, directionUp);
			var headingHmd 		= Vector3.ProjectOnPlane(transformHmd.forward, directionUp).normalized;
			var signedAngle 	= Vector3.SignedAngle(headingHmd, headingDesired, directionUp);
			
			_transformOffset.Rotate(directionUp, signedAngle);
			
			// Displacement
			var deltaPos = _transformRig.position - transformHmd.position;

			if (!_clearHeightOnRecenter)
			{
				// Removing height this way allows us to toggle _clearHeightOnRecenter without issue
				deltaPos += transformHmd.localPosition.y * directionUp;
			}
			
			_transformOffset.Translate(deltaPos, Space.World);
			
			_recentered.Invoke();
		}
		
	}
}