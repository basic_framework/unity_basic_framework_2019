﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.XR.Editor
{
	public abstract class BasicNodeTrackerXrBaseEditor<TXrNodeTracker> : BasicBaseEditor<TXrNodeTracker>
		where TXrNodeTracker : BasicNodeTrackerXrBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_transformTracked").isExpanded;
			set => serializedObject.FindProperty("_transformTracked").isExpanded = value;
		}
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.IsTracking).CamelCaseToHumanReadable(), 
				TypedTarget.IsTracking.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var transformTracked = serializedObject.FindProperty("_transformTracked");
			var trackingLostMode = serializedObject.FindProperty("_trackingLostMode");
			var transformLostTracking = serializedObject.FindProperty("_transformLostTracking");
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerXrBase).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(transformTracked);
			EditorGUILayout.PropertyField(trackingLostMode);

			var trackingLostModeEnum =
				EnumUtility.EnumNameToEnumValue<TrackingLostModes>(
					trackingLostMode.enumNames[trackingLostMode.enumValueIndex]);

			switch (trackingLostModeEnum)
			{
				case TrackingLostModes.DoNothing:
					break;
				case TrackingLostModes.SetToLocalZero:
					break;
				case TrackingLostModes.SetToWorldTransform:
					BasicEditorGuiLayout.PropertyFieldNotNull(transformLostTracking);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var isTrackingChanged = serializedObject.FindProperty("_isTrackingChanged");
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerXrBase).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(isTrackingChanged);
		}
		
	}
}