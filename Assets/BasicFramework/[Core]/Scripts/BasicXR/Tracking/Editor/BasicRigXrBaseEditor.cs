﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.XR.Editor
{
	public abstract class BasicRigXrBaseEditor<TXrRig> : BasicBaseEditor<TXrRig>
		where TXrRig : BasicRigXrBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_nodeTrackerHmd").isExpanded;
			set => serializedObject.FindProperty("_nodeTrackerHmd").isExpanded = value;
		}


		// ****************************************  METHODS  ****************************************\\

		protected override void OnFirstDrawInspectorFields()
		{
			base.OnFirstDrawInspectorFields();
			
			UpdateTransformHeirarchy();
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var transformRig = serializedObject.FindProperty("_transformRig");
			var transformOffset = serializedObject.FindProperty("_transformOffset");

			EditorGUILayout.HelpBox("XR Rig Transform Heirarchy" +
			                        "\n\nparent_rig_xr" +
			                        "\n    > parent_tracked_devices_offset" +
			                        "\n        > tracked_node_hmd" +
			                        "\n        > tracked_node_controller_hand_left" +
			                        "\n        > tracked_node_controller_hand_right" +
			                        "\n        > tracked_node_...", 
				MessageType.Info);
			
			GUI.enabled = false;
			BasicEditorGuiLayout.PropertyFieldNotNull(transformRig);
			BasicEditorGuiLayout.PropertyFieldNotNull(transformOffset);
			GUI.enabled = PrevGuiEnabled;
			
			Space();
			EditorGUILayout.LabelField(nameof(TypedTarget.UserPresent).CamelCaseToHumanReadable(), 
				TypedTarget.UserPresent.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingHmd).CamelCaseToHumanReadable(), 
				TypedTarget.TrackingHmd.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingControllerHandLeft).CamelCaseToHumanReadable(), 
				TypedTarget.TrackingControllerHandLeft.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingControllerHandRight).CamelCaseToHumanReadable(), 
				TypedTarget.TrackingControllerHandRight.ToString().CamelCaseToHumanReadable());
			
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var xrNodeTrackerHmd = serializedObject.FindProperty("_nodeTrackerHmd");
			var xrNodeTrackerControllerHandLeft = serializedObject.FindProperty("_nodeTrackerControllerHandLeft");
			var xrNodeTrackerControllerHandRight = serializedObject.FindProperty("_nodeTrackerControllerHandRight");
			
			var clearHeightOnRecenter = serializedObject.FindProperty("_clearHeightOnRecenter");

			Space();
			SectionHeader($"{nameof(BasicRigXrBase).CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			BasicEditorGuiLayout.PropertyFieldNotNull(xrNodeTrackerHmd);
			if (EditorGUI.EndChangeCheck()) UpdateTransformHeirarchy();

			BasicEditorGuiLayout.PropertyFieldNotNull(xrNodeTrackerControllerHandLeft);
			BasicEditorGuiLayout.PropertyFieldNotNull(xrNodeTrackerControllerHandRight);
			
			Space();
			EditorGUILayout.PropertyField(clearHeightOnRecenter);
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying;
			if (GUILayout.Button("RECENTER")) TypedTarget.Recenter();
			GUI.enabled = PrevGuiEnabled;
		}

		private void UpdateTransformHeirarchy()
		{
			var xrNodeTrackerHmd = serializedObject.FindProperty("_nodeTrackerHmd");
			var transformRig = serializedObject.FindProperty("_transformRig");
			var transformOffset = serializedObject.FindProperty("_transformOffset");

			var nodeTrackerHmd = xrNodeTrackerHmd.objectReferenceValue as BasicNodeTrackerXrBase;

			if (nodeTrackerHmd == null)
			{
				transformOffset.objectReferenceValue = null;
				transformRig.objectReferenceValue = null;
				return;
			}

			var transformOffsetObject = nodeTrackerHmd.transform.parent;
			transformOffset.objectReferenceValue = transformOffsetObject;

			if (transformOffsetObject == null)
			{
				transformRig.objectReferenceValue = null;
				return;
			}

			var transformRigObject = transformOffsetObject.parent;
			transformRig.objectReferenceValue = transformRigObject;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();

			var userPresentChanged = serializedObject.FindProperty("_userPresentChanged");
			
			var trackingHmdChanged = serializedObject.FindProperty("_trackingHmdChanged");
			var trackingControllerHandLeftChanged = serializedObject.FindProperty("_trackingControllerHandLeftChanged");
			var trackingControllerHandRightChanged = serializedObject.FindProperty("_trackingControllerHandRightChanged");
			
			var recentered = serializedObject.FindProperty("_recentered");
			
			Space();
			SectionHeader($"{nameof(BasicRigXrBase).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(userPresentChanged);
			
			Space();
			EditorGUILayout.PropertyField(trackingHmdChanged);
			EditorGUILayout.PropertyField(trackingControllerHandLeftChanged);
			EditorGUILayout.PropertyField(trackingControllerHandRightChanged);
			
			Space();
			EditorGUILayout.PropertyField(recentered);
		}
		
	}
}