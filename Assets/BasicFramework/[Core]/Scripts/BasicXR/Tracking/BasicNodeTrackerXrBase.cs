﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.XR
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_EXTERNAL_TRACKING)]
	public abstract class BasicNodeTrackerXrBase : MonoBehaviour 
	{
		
		// Possibly update OnBeforeRender, register to callback
		// Possibly add PoseFlags (i.e. choose to track position/rotation)
		// TODO: Possibly add position and rotation offset
	
		// **************************************** VARIABLES ****************************************\\
		
		// Node needs to be tracked consistantly for this duration to be considered tracked
		// MUST be greater than 0, i.e. minimum 0.001
		private const float MIN_TRACK_TIME = 0.3f;
		
		
		[Tooltip("The transform that will be moved based on the tracking data")]
		[SerializeField] private Transform _transformTracked = default;

		[Tooltip("How should the node behave when tracking is lost")]
		[SerializeField] private TrackingLostModes _trackingLostMode = TrackingLostModes.DoNothing;

		[Tooltip("The transform to match if tracking is lost")]
		[SerializeField] private Transform _transformLostTracking = default;


		[Tooltip("Is the tracker currently being tracked by the tracking system")]
		[SerializeField] private BoolPlusUnityEvent _isTrackingChanged = default;


		public Transform TransformTracked => _transformTracked;

		public TrackingLostModes TrackingLostMode
		{
			get => _trackingLostMode;
			set => _trackingLostMode = value;
		}

		public BoolUnityEvent IsTrackingChanged => _isTrackingChanged.Value;

		
		private bool _isTracking;
		public bool IsTracking
		{
			get => _isTracking;
			private set
			{
				if (_isTracking == value) return;
				_isTracking = value;
				IsTrackingUpdated(value);
			}
		}

		// The last time that this node was NOT being tracked
		private float _lastTimeNotTracked;
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** abstract/virtual *****\\

		protected abstract bool GetIsTracking();
		protected abstract BasicPose GetLocalPose();
//		protected abstract Vector3 GetLocalPosition();
//		protected abstract Quaternion GetLocalRotation();
		
		
		
		//***** mono *****\\
		
		protected virtual void Reset()
		{
			_transformTracked = transform;
		}

		protected virtual void OnDisable()
		{
			IsTracking = false;
		}

		protected virtual void Start()
		{
			IsTrackingUpdated(_isTracking);
		}

		protected virtual void Update()
		{
			var time = Time.time;
			var isTracking = GetIsTracking();

			if (!isTracking) _lastTimeNotTracked = time;

			IsTracking = time - _lastTimeNotTracked >= MIN_TRACK_TIME;
			
			if (!_isTracking) return;

			var localPose = GetLocalPose();
			
			UpdatePose(localPose.Position, localPose.Rotation);
//			var localPosition = GetLocalPosition();
//			var localRotation = GetLocalRotation();
			
//			UpdatePose(localPosition, localRotation);
		}
		

		//***** property updates *****\\
		
		protected virtual void IsTrackingUpdated(bool value)
		{
			if (!value)
			{
				switch (_trackingLostMode)
				{
					case TrackingLostModes.DoNothing:
						break;
					
					case TrackingLostModes.SetToLocalZero:
						UpdatePose(Vector3.zero, Quaternion.identity);
						break;
					
					case TrackingLostModes.SetToWorldTransform:
						UpdatePose(_transformLostTracking.position, _transformLostTracking.rotation, Space.World);
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			
			_isTrackingChanged.Raise(value);
		}
		
		
		//***** functions ******\\

		private void UpdatePose(Vector3 position, Quaternion rotation, Space space = Space.Self)
		{
			// Note: could check to see if pose chenges and raise event
			
			switch (space)
			{
				case Space.World:
					_transformTracked.position = position;
					_transformTracked.rotation = rotation;
					break;
				
				case Space.Self:
					_transformTracked.localPosition = position;
					_transformTracked.localRotation = rotation;
					break;
				
				default:
					throw new ArgumentOutOfRangeException(nameof(space), space, null);
			}
		}
		
		
		
		//***** reference *****\\
		
		// reference to add offset to position and rotation
		/*private void UpdatePose()
		{
			if (_trackingState != OvrTrackingStates.Tracking) return;

			var ovrPose = OVRPlugin.GetNodePose(_trackedNode, OVRPlugin.Step.Render).ToOVRPose();

			var targetPosition = ovrPose.position;
			var targetRotation = ovrPose.orientation;
			
			var trsMatrix = Matrix4x4.TRS(targetPosition, targetRotation, Vector3.one);
			targetPosition = MathUtility.TransformPoint(trsMatrix, _positionOffset);
			targetRotation = MathUtility.TransformQuaternion(targetRotation, Quaternion.Euler(_rotationOffset));
			
			
			var transform1 = transform;

			if ((_trackedPoseProperties & PoseFlags.Position) != 0) { transform1.localPosition = targetPosition; }
			if ((_trackedPoseProperties & PoseFlags.Rotation) != 0) { transform1.localRotation = targetRotation; }
		}*/
		
	}
}