﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Button.Editor
{
	[CustomEditor(typeof(DistanceButton))]
	public class DistanceButtonEditor : ButtonBaseEditor<DistanceButton>
	{
	
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
		
			Space();
			DrawReferenceFields();
		
			Space();
			DrawSettingFields();
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			EditorGUILayout.LabelField("Travel Distance", $"{Mathf.FloorToInt(TypedTarget.TravelDistance * 1000)}mm");
		}

		private void DrawReferenceFields()
		{
			SectionHeader("References");
		
			var buttonTransform = serializedObject.FindProperty("_buttonTransform");
			var buttonTransformParent = serializedObject.FindProperty("_buttonTransformParent");
		
			EditorGUI.BeginChangeCheck();
			BasicEditorGuiLayout.PropertyFieldNotNull(buttonTransform);
			if (EditorGUI.EndChangeCheck())
			{
				var buttonTransformTyped = buttonTransform.objectReferenceValue as Transform;
				buttonTransformParent.objectReferenceValue =
					buttonTransformTyped == null ? null : buttonTransformTyped.parent;
			}
		
			GUI.enabled = false;
			BasicEditorGuiLayout.PropertyFieldCanBeNull(buttonTransformParent);
			GUI.enabled = PrevGuiEnabled;
		}

		private void DrawSettingFields()
		{
			SectionHeader("Settings");
		
			var activateDistance = serializedObject.FindProperty("_activateDistance");
			var distanceLocalDirection = serializedObject.FindProperty("_distanceLocalDirection");

			EditorGUILayout.PropertyField(activateDistance);
			activateDistance.floatValue = Mathf.Max(0.0001f, activateDistance.floatValue);
		
			BasicEditorGuiLayout.Vector3DirectionDropdown(distanceLocalDirection);
		}

	}
}