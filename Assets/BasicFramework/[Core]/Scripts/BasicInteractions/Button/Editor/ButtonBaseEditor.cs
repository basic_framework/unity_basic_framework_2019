﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Button.Editor
{
	public abstract class ButtonBaseEditor<T> : BasicBaseEditor<T>
		where T : ButtonBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_debounceDelay").isExpanded;
			set => serializedObject.FindProperty("_debounceDelay").isExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			Space();
			DrawButtonBaseFields();
		}

		protected override void DrawDebugFields()
		{
			EditorGUILayout.LabelField("Enabled", TypedTarget.ButtonEnabled.ToString());
			EditorGUILayout.LabelField("Pressed", TypedTarget.ButtonPressed.ToString());
			EditorGUILayout.LabelField("Debounce Timer", MathUtility.RoundToDecimalPlace(TypedTarget.DebounceTimer, 3).ToString());
		}

		protected void DrawButtonBaseFields()
		{
			SectionHeader("Button Base");

			var debounceDelay = serializedObject.FindProperty("_debounceDelay");

			EditorGUILayout.PropertyField(debounceDelay);
			debounceDelay.floatValue = Mathf.Max(0, debounceDelay.floatValue);
		}

		protected override void DrawEventFields()
		{
			SectionHeader("Button Base Events");
		
			var buttonEnabledChanged = serializedObject.FindProperty("_buttonEnabledChanged");
			
			var buttonDown = serializedObject.FindProperty("_buttonDown");
			var buttonHeld = serializedObject.FindProperty("_buttonHeld");
			var buttonUp = serializedObject.FindProperty("_buttonUp");
		
			EditorGUILayout.PropertyField(buttonEnabledChanged);
			
			EditorGUILayout.PropertyField(buttonDown);
			EditorGUILayout.PropertyField(buttonHeld);
			EditorGUILayout.PropertyField(buttonUp);
		}
		
	}
}