﻿using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Button
{
	public abstract class ButtonBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("How long to wait in seconds for the button state to stabilise")]
		[SerializeField] private float _debounceDelay = 0.3f;

		[Tooltip("Raised when the button state changes from enabled to disabled and vice versa")]
		[SerializeField] private BoolPlusUnityEvent _buttonEnabledChanged = default;
		
		[SerializeField] private UnityEvent _buttonDown = default;
		[SerializeField] private UnityEvent _buttonHeld = default;
		[SerializeField] private UnityEvent _buttonUp = default;

		
		public float DebounceDelay
		{
			get => _debounceDelay;
			set => _debounceDelay = Mathf.Max(0, value);
		}

		public BoolPlusUnityEvent ButtonEnabledChanged => _buttonEnabledChanged;

		public UnityEvent ButtonDown => _buttonDown;

		public UnityEvent ButtonHeld => _buttonHeld;

		public UnityEvent ButtonUp => _buttonUp;


		private bool _buttonPressed;
		public bool ButtonPressed => _buttonPressed;

		private float _debounceTimer;
		public float DebounceTimer => _debounceTimer;

		public bool ButtonEnabled
		{
			get => enabled;
			set => enabled = value;
		}


		// ****************************************  METHODS  ****************************************\\

		protected virtual void OnEnable()
		{
			_buttonEnabledChanged.Raise(true);
		}

		protected virtual void OnDisable()
		{
			_buttonEnabledChanged.Raise(false);
		}

		protected virtual void Update()
		{
			bool buttonPressed;
		
			if (_debounceTimer > 0)
			{
				_debounceTimer -= Time.deltaTime;
				_debounceTimer = Mathf.Max(0, _debounceTimer);
				buttonPressed = _buttonPressed;
			}
			else
			{
				buttonPressed = IsButtonPressed();
			}

			if (buttonPressed != _buttonPressed)
			{
				_buttonPressed = buttonPressed;
				_debounceTimer = _debounceDelay;

				if (buttonPressed)
				{
					OnButtonDown();
					_buttonDown.Invoke();
				}
				else
				{
					OnButtonUp();
					_buttonUp.Invoke();
				}
			}
		
			if(!buttonPressed) return;
			OnButtonHeld();
			_buttonHeld.Invoke();
		}

		protected abstract bool IsButtonPressed();

		protected virtual void OnButtonDown(){}
		protected virtual void OnButtonHeld(){}
		protected virtual void OnButtonUp(){}
	
	}
}