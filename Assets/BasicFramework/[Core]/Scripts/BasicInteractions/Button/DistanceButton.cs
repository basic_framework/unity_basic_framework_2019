﻿using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Button
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Button/Distance Button")]
	public class DistanceButton : ButtonBase 
	{

		// Note: this class does not handle the physics of the button,
		// it only looks at the displacement of a transform to determine when a button is pressed
	
		// A button press is determined by the distance a transform has moved in a certain axis relative to its parent
		// Note: the parent can be null (i.e. the button is at the top level of the scene heirarchy)
	
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The transform that needs to move to activate the button")]
		[SerializeField] private Transform _buttonTransform = default;
		[SerializeField] private Transform _buttonTransformParent = default;

		[Tooltip("The distance (in units) from the origin that the button is considered to be pressed")]
		[SerializeField] private float _activateDistance = 0.02f;

		[Tooltip("The direction, relative to the butttons parent, that the travel distance will be calculated from")]
		[SerializeField] private Vector3 _distanceLocalDirection = Vector3.down;

	
		public float ActivateDistance
		{
			get => _activateDistance;
			set => _activateDistance = value;
		}

		public Vector3 DistanceLocalDirection
		{
			get => _distanceLocalDirection;
			set => _distanceLocalDirection = value.normalized;
		}

		
		// The button local position of the button on start 
		private Vector3 _startlocalPosition;
		public Vector3 StartlocalPosition => _startlocalPosition;

		// How far the button has moved in the specified direction
		private float _travelDistance;
		public float TravelDistance => _travelDistance;


		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_buttonTransform = transform;
			_buttonTransformParent = _buttonTransform.parent;
		}

		private void Start()
		{
			_buttonTransformParent = _buttonTransform.parent;
			_startlocalPosition = _buttonTransform.localPosition;
		}
	
	
		protected override bool IsButtonPressed()
		{
			var startToCurrentVectorLocal = _buttonTransform.localPosition - _startlocalPosition;
			_travelDistance = MathUtility.MagOfVectorInAxis(startToCurrentVectorLocal, _distanceLocalDirection);
			return _travelDistance > _activateDistance;
		}
	
	
		private void OnDrawGizmos()
		{
			if (_buttonTransform == null) return;

			if (!Application.isPlaying)
			{
				_startlocalPosition = _buttonTransform.localPosition;
			}
		
			var isParentNull = _buttonTransformParent == null;
		
			var prevGizmosColor = Gizmos.color;
			Gizmos.color = Color.yellow;
		
			var position = isParentNull ? _startlocalPosition : _buttonTransformParent.TransformPoint(_startlocalPosition);
			var rotation = isParentNull ? Quaternion.identity : _buttonTransformParent.rotation;
		
			Gizmos.DrawRay(position, rotation * (_distanceLocalDirection.normalized * _activateDistance));
			Gizmos.color = prevGizmosColor;

			if (!Application.isPlaying) return;
		
			var startToCurrentVectorLocal = _buttonTransform.localPosition - _startlocalPosition;
		
			var travelInDirection = MathUtility.GetAxisFromVector(startToCurrentVectorLocal, _distanceLocalDirection);
			var missingVector = startToCurrentVectorLocal - travelInDirection;
		
			Debug.DrawRay(position, rotation * startToCurrentVectorLocal, Color.magenta);
			Debug.DrawRay(position, rotation * travelInDirection, Color.cyan);
			Debug.DrawRay(position + rotation * travelInDirection, rotation * missingVector, Color.white);
		}
	
	}
}