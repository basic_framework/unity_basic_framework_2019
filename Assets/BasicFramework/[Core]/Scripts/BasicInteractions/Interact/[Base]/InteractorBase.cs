﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions
{
	public abstract class InteractorBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// Seeks an interactable target, initiates and ends interaction with target 

		[Tooltip("Raised when the interactor state changes")]
		[SerializeField] private InteractorStatesUnityEvent _interactorStateChanged = default;
		
		[Tooltip("Raised when the interactor state changes and the condition is met")]
		[SerializeField] private InteractorStatesCompareUnityEvent[] _interactorStateChangedCompare = default;
		
		
		[Tooltip("Raised when the interactable targeting changes")]
		[SerializeField] private InteractableBaseUnityEvent _interactableTargetingChanged = default;

		[Tooltip("Raised when the interactable targeting changes and the compare condition is met")]
		[SerializeField] private InteractableBaseCompareUnityEvent[] _interactableTargetingChangedCompare = default;
		
		
		[Tooltip("Raised when the interactable interacting with changes")]
		[SerializeField] private InteractableBaseUnityEvent _interactableInteractingChanged = default;

		[Tooltip("Raised when the interactable interacting with changes and the condition is met")]
		[SerializeField] private InteractableBaseCompareUnityEvent[] _interactableInteractingChangedCompare = default;
		
		
		private InteractorStates _interactorState;
		public InteractorStates InteractorState
		{
			get => _interactorState;
			private set
			{
				if (_interactorState == value) return;
				_interactorState = value;
				InteractorStateUpdated(value);
			}
		}
		
		private InteractableBase _interactableTargeting;
		public InteractableBase InteractableTargeting
		{
			get => _interactableTargeting;
			private set
			{
				if (_interactableTargeting == value) return;
				_interactableTargeting = value;
				InteractableTargetingUpdated(value);
			}
		}
		
		private InteractableBase _interactableInteracting;
		public InteractableBase InteractableInteracting
		{
			get => _interactableInteracting;
			private set
			{
				if (_interactableInteracting == value) return;
				_interactableInteracting = value;
				InteractableInteractingUpdated(value);
			}
		}

		private InteractableBase _interactableTargetingPending;
		private InteractableBase _interactableInteractionPending;


		// ****************************************  METHODS  ****************************************\\

		//***** virtual *****\\

		protected virtual void UpdateSearching(){}
		protected virtual void UpdateTargeting(InteractableBase targeting){}
		protected virtual void UpdateInteracting(InteractableBase targeting, InteractableBase interacting){}
		
		protected virtual void OnInteractorStateUpdated(InteractorStates interactorState){}
		protected virtual void OnInteractableTargetingUpdated(InteractableBase interactable){}
		protected virtual void OnInteractableInteractingUpdated(InteractableBase interactable){}
		
		
		//***** mono *****\\

		protected virtual void OnEnable()
		{
			InteractorState = InteractorStates.Searching;
		}

		protected virtual void OnDisable()
		{
			StopInteracting();
			StopTargeting();
			InteractorState = InteractorStates.Disabled;
		}

		protected virtual void Start()
		{
			InteractorStateUpdated(_interactorState);
			InteractableTargetingUpdated(_interactableTargeting);
			InteractableInteractingUpdated(_interactableInteracting);
		}

		private void Update()
		{
			switch (InteractorState)
			{
				case InteractorStates.Disabled:
					break;
				
				case InteractorStates.Searching:
					UpdateSearching();
					break;
				
				case InteractorStates.Targeting:
					UpdateTargeting(_interactableTargeting);
					break;
				
				case InteractorStates.Interacting:
					UpdateInteracting(_interactableTargeting, _interactableInteracting);
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		//***** property updates *****\\
		
		private void InteractorStateUpdated(InteractorStates value)
		{
			OnInteractorStateUpdated(value);
			
			_interactorStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactorStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		private void InteractableTargetingUpdated(InteractableBase value)
		{
			OnInteractableTargetingUpdated(value);
			
			_interactableTargetingChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactableTargetingChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		private void InteractableInteractingUpdated(InteractableBase value)
		{
			OnInteractableInteractingUpdated(value);

			_interactableInteractingChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactableInteractingChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** callbacks *****\\
		
		private void OnTargetingStarted(InteractableBase interactable)
		{
			if(interactable == null) return;
			if (!enabled)
			{
				interactable.StopTargeting(this);
				return;
			}
			
			InteractableTargeting = interactable;
			if (InteractorState == InteractorStates.Interacting) return;
			InteractorState = InteractorStates.Targeting;
		}
		
		private void OnTargetingStopped(InteractableBase interactable)
		{
			if(interactable == null) return;
			if(interactable != _interactableTargeting) return;
			
			InteractableTargeting = null;
			if (InteractorState == InteractorStates.Interacting) return;
			InteractorState = InteractorStates.Searching;
		}
		
		
		private void OnInteractionStarted(InteractableBase interactable)
		{
			if(interactable == null) return;
			if (!enabled)
			{
				interactable.StopInteracting(this);
				return;
			}

			InteractableInteracting = interactable;
			InteractorState = InteractorStates.Interacting;
		}
		
		private void OnInteractionStopped(InteractableBase interactable)
		{
			if(interactable == null) return;
			if(interactable != _interactableInteracting) return;
			
			InteractableInteracting = null;
			InteractorState = _interactableTargeting == null ? InteractorStates.Searching : InteractorStates.Targeting;
		}
		
		
		//***** functions *****\\

		//*** Targeting ***\\
		
		protected void StartTargeting(InteractableBase interactable)
		{
			if (!enabled) return;
			if (interactable == null) return;
			if (interactable == _interactableTargeting) return;
			StopTargeting();
			
			interactable.StartTargeting(this, OnTargetingStarted, OnTargetingStopped);
		}
		
		protected void StopTargeting()
		{
			if (_interactableTargeting == null) return;
			_interactableTargeting.StopTargeting(this);
		}

		
		//*** Interacting ***\\
		
		protected void StartInteracting()
		{
			if (!enabled) return;
			if (_interactableTargeting == null) return;
			if (_interactableInteracting == _interactableTargeting) return;
			StopInteracting();
			
			_interactableTargeting.StartInteracting(this, OnInteractionStarted, OnInteractionStopped);
		}
		
		protected void StopInteracting()
		{
			if (_interactableInteracting == null) return;
			_interactableInteracting.StopInteracting(this);
		}
		
	}
	
	[Serializable]
	public class InteractorBaseUnityEvent : UnityEvent<InteractorBase>{}
	
	[Serializable]
	public class InteractorBaseCompareUnityEvent : CompareValueUnityEventBase<InteractorBase>{}
}