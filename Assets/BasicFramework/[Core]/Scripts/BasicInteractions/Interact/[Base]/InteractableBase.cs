﻿using System;
using System.Collections.Generic;
using System.Linq;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions
{
	public abstract class InteractableBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// has the ability to be interacted with by an interactor
		// keeps track of if it is currently being targeted or interacted with

		[Tooltip("Raised when the interactable state changes")]
		[SerializeField] private InteractableStatesUnityEvent _interactableStateChanged = default;
		
		[Tooltip("Raised when the interactable state changes and the condition is met")]
		[SerializeField] private InteractableStatesCompareUnityEvent[] _interactableStateChangedCompare = default;
		
		
		[Tooltip("Raised when the main (newest) targeting interactor changes")]
		[SerializeField] private InteractorBaseUnityEvent _interactorTargetingChanged = default;

		[Tooltip("Raised when the main (newest) targeting interactor changes and the compare condition is met")]
		[SerializeField] private InteractorBaseCompareUnityEvent[] _interactorTargetingChangedCompare = default;
		
		
		[Tooltip("Raised when the main (newest) interacting interactor changes")]
		[SerializeField] private InteractorBaseUnityEvent _interactorInteractingChanged = default;

		[Tooltip("Raised when the main (newest) interacting interactor changes and the compare condition is met")]
		[SerializeField] private InteractorBaseCompareUnityEvent[] _interactorInteractingChangedCompare = default;


		private InteractableStates _interactableState;
		public InteractableStates InteractableState
		{
			get => _interactableState;
			private set
			{
				if (_interactableState == value) return;
				_interactableState = value;
				InteractableStateUpdated(value);
			}
		}

		private InteractorBase _interactorTargeting;
		public InteractorBase InteractorTargeting
		{
			get => _interactorTargeting;
			private set
			{
				if (_interactorTargeting == value) return;
				_interactorTargeting = value;
				InteractorTargetingUpdated(value);
			}
		}

		private InteractorBase _interactorInteracting;
		public InteractorBase InteractorInteracting
		{
			get => _interactorInteracting;
			private set
			{
				if (_interactorInteracting == value) return;
				_interactorInteracting = value;
				InteractorInteractingUpdated(value);
			}
		}
		
		// Action is what is to be called when ending interaction
		private readonly Dictionary<InteractorBase, Action<InteractableBase>> _dictionaryTargeting 
			= new Dictionary<InteractorBase, Action<InteractableBase>>();
		
		private readonly Dictionary<InteractorBase, Action<InteractableBase>> _dictionaryInteracting 
			= new Dictionary<InteractorBase, Action<InteractableBase>>();


		// ****************************************  METHODS  ****************************************\\

		//***** virtual *****\\
		
		protected virtual void OnInteractableStateUpdated(InteractableStates interactableState){}
		protected virtual void OnInteractorTargetingUpdated(InteractorBase interactor){}
		protected virtual void OnInteractorInteractingUpdated(InteractorBase interactor){}

		protected virtual bool CanInteract(InteractorBase interactor) => true;
		
		
		//***** mono *****\\

		protected virtual void OnEnable()
		{
			InteractableState = InteractableStates.Waiting;
		}

		protected virtual void OnDisable()
		{
			StopInteractingAll();
			StopTargetingAll();
			InteractableState = InteractableStates.Disabled;
		}

		protected virtual void Start()
		{
			InteractableStateUpdated(_interactableState);
			InteractorTargetingUpdated(_interactorTargeting);
			InteractorInteractingUpdated(_interactorInteracting);
		}
		

		//***** property updates *****\\
		
		private void InteractableStateUpdated(InteractableStates value)
		{
			OnInteractableStateUpdated(value);
			
			_interactableStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactableStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}

		private void InteractorTargetingUpdated(InteractorBase value)
		{
			OnInteractorTargetingUpdated(value);
			
			_interactorTargetingChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactorTargetingChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		private void InteractorInteractingUpdated(InteractorBase value)
		{
			OnInteractorInteractingUpdated(value);
			
			_interactorInteractingChanged.Invoke(value);

			foreach (var compareUnityEvent in _interactorInteractingChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\
		
		
		//*** Targeting ***\\
		
		public bool IsTargeting(InteractorBase interactor) => _dictionaryTargeting.ContainsKey(interactor);
		
		public void StartTargeting(InteractorBase interactor, Action<InteractableBase> targettingStarted, Action<InteractableBase> targettingStopped)
		{
			if (!enabled) return;
			if (interactor == null) return;
			if (!CanInteract(interactor)) return;
			if (_dictionaryTargeting.ContainsKey(interactor)) return;
			_dictionaryTargeting.Add(interactor, targettingStopped);
			
			InteractorTargeting = interactor;
			
			if (_interactableState != InteractableStates.Interacting)
			{
				InteractableState = InteractableStates.Targeted;
			}
			
			targettingStarted?.Invoke(this);
		}
		
		public void StopTargeting(InteractorBase interactor)
		{
			if (!_dictionaryTargeting.ContainsKey(interactor)) return;
			var actionTargetingStopped = _dictionaryTargeting[interactor];
			_dictionaryTargeting.Remove(interactor);

			var countTargeting = _dictionaryTargeting.Count;
			
			if (_interactorTargeting == interactor)
			{
				var keys = _dictionaryTargeting.Keys.ToArray();
				
				InteractorTargeting = countTargeting != 0 ? keys[countTargeting - 1] : null;
			}
			
			if (_interactableState != InteractableStates.Interacting)
			{
				InteractableState = countTargeting != 0 ? InteractableStates.Targeted : InteractableStates.Waiting;
			}

			actionTargetingStopped?.Invoke(this);
		}
		
		public void StopTargetingAll()
		{
			var keys = _dictionaryTargeting.Keys.ToArray();
			
			// Loop through and remove all interactors
			foreach (var key in keys)
			{
				StopTargeting(key);
			}
		}
		
		
		//*** Interacting ***\\
		
		public bool IsInteracting(InteractorBase interactor) => _dictionaryInteracting.ContainsKey(interactor);
		
		public void StartInteracting(InteractorBase interactor, Action<InteractableBase> interactingStarted, Action<InteractableBase> interactingStopped)
		{
			if (!enabled) return;
			if (interactor == null) return;
			if (_dictionaryInteracting.ContainsKey(interactor)) return;
			_dictionaryInteracting.Add(interactor, interactingStopped);
			
			InteractorInteracting = interactor;
			InteractableState = InteractableStates.Interacting;

			interactingStarted?.Invoke(this);
		}
		
		public void StopInteracting(InteractorBase interactor)
		{
			if (!_dictionaryInteracting.ContainsKey(interactor)) return;
			var actionInteractingStopped = _dictionaryInteracting[interactor];
			_dictionaryInteracting.Remove(interactor);
			
			if (_interactorInteracting == interactor)
			{
				var countInteracting = _dictionaryInteracting.Count;
				
				if (countInteracting != 0)
				{
					var keys = _dictionaryInteracting.Keys.ToArray();
					
					InteractorInteracting = keys[countInteracting - 1];
					InteractableState = InteractableStates.Interacting;
				}
				else
				{
					var countTargeting = _dictionaryTargeting.Count;
					
					InteractorInteracting = null;
					InteractableState = countTargeting == 0 ? InteractableStates.Waiting : InteractableStates.Targeted;
				}
			}

			actionInteractingStopped?.Invoke(this);
		}
		
		public void StopInteractingAll()
		{
			var keys = _dictionaryInteracting.Keys.ToArray();
			
			// Loop through and remove all interactors
			foreach (var key in keys)
			{
				StopInteracting(key);
			}
		}
		
		
	
		//***** Editor *****\\

#if UNITY_EDITOR
		public InteractorBase[] EditorGetInteractorsTargeting() => _dictionaryTargeting.Keys.ToArray();
		public InteractorBase[] EditorGetInteractorsInteracting() => _dictionaryInteracting.Keys.ToArray();
#endif
		
	}
	
	[Serializable]
	public class InteractableBaseUnityEvent : UnityEvent<InteractableBase>{}
	
	[Serializable]
	public class InteractableBaseCompareUnityEvent : CompareValueUnityEventBase<InteractableBase>{}
	
}

//		private readonly List<InteractorBase> _interactorsTargeting = new List<InteractorBase>();
//		private readonly List<InteractorBase> _interactorsInteracting = new List<InteractorBase>();


//		public void StartTargeting(InteractorBase interactor)
//		{
//			if (!enabled) return;
//			if (interactor == null) return;
//			if (_interactorsTargeting.Contains(interactor)) return;
//			_interactorsTargeting.Add(interactor);
//			
//			InteractorTargeting = interactor;
//			
//			if (_interactableState != InteractableStates.Interacting)
//			{
//				InteractableState = InteractableStates.Targeted;
//			}
//			
//			interactor.TargetingStarted(this);
//		}
		
//		public void StopTargeting(InteractorBase interactor)
//		{
//			if (!_interactorsTargeting.Contains(interactor)) return;
//			_interactorsTargeting.Remove(interactor);
//
//			var countTargeting = _interactorsTargeting.Count;
//			
//			if (_interactorTargeting == interactor)
//			{
//				InteractorTargeting = countTargeting != 0 ? _interactorsTargeting[countTargeting - 1] : null;
//			}
//			
//			if (_interactableState != InteractableStates.Interacting)
//			{
//				InteractableState = countTargeting != 0 ? InteractableStates.Targeted : InteractableStates.Waiting;
//			}
//			
//			interactor.TargetingStopped(this);
//		}
		
//		public void StopTargetingAll()
//		{
//			// Loop through and remove all interactors
//			foreach (var interactor in _interactorsTargeting)
//			{
//				StopTargeting(interactor);
//			}
//		}


//		public void StartInteracting(InteractorBase interactor)
//		{
//			if (!enabled) return;
//			if (interactor == null) return;
//			if (_interactorsInteracting.Contains(interactor)) return;
//			_interactorsInteracting.Add(interactor);
//			
//			InteractorInteracting = interactor;
//			InteractableState = InteractableStates.Interacting;
//
//			interactor.InteractionStarted(this);
//		}
//		
//		public void StopInteracting(InteractorBase interactor)
//		{
//			if (!_interactorsInteracting.Contains(interactor)) return;
//			_interactorsInteracting.Remove(interactor);
//			
//			if (_interactorInteracting == interactor)
//			{
//				var countInteracting = _interactorsInteracting.Count;
//				
//				if (countInteracting != 0)
//				{
//					InteractorInteracting = _interactorsInteracting[countInteracting - 1];
//					InteractableState = InteractableStates.Interacting;
//				}
//				else
//				{
//					var countTargeting = _interactorsTargeting.Count;
//					
//					InteractorInteracting = null;
//					InteractableState = countTargeting == 0 ? InteractableStates.Waiting : InteractableStates.Targeted;
//				}
//			}
//
//			interactor.InteractionStopped(this);
//		}

//		public void StopInteractingAll()
//		{
//			// Loop through and remove all interactors
//			foreach (var interactor in _interactorsInteracting)
//			{
//				StopInteracting(interactor);
//			}
//		}