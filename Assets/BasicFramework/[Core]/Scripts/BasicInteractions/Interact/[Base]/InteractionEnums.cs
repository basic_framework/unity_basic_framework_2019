﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions
{
	
	// Make single and events
	
	public enum InteractorStates
	{
		Disabled,
		Searching,
		Targeting,
		Interacting
	}
	
	[Serializable]
	public class InteractorStatesUnityEvent : UnityEvent<InteractorStates>{}

	[Serializable]
	public class InteractorStatesCompareUnityEvent : CompareValueUnityEventBase<InteractorStates>{}

	
	public enum InteractableStates
	{
		Disabled,
		Waiting,
		Targeted,
		Interacting
	}
	
	[Serializable]
	public class InteractableStatesUnityEvent : UnityEvent<InteractableStates>{}

	[Serializable]
	public class InteractableStatesCompareUnityEvent : CompareValueUnityEventBase<InteractableStates>{}
	
}