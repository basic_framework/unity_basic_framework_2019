﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	public abstract class InteractorBaseEditor<T> : BasicBaseEditor<T>
		where T : InteractorBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;
		
		private ReorderableList _reorderableListInteractorStateChanged;
		private ReorderableList _reorderableListTargetingChanged;
		private ReorderableList _reorderableListInteractingChanged;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();

			var interactorStateChangedCompare = serializedObject.FindProperty("_interactorStateChangedCompare");
			var interactableTargetingChangedCompare = serializedObject.FindProperty("_interactableTargetingChangedCompare");
			var interactableInteractingChangedCompare = serializedObject.FindProperty("_interactableInteractingChangedCompare");

			_reorderableListInteractorStateChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactorStateChangedCompare, true);
			
			_reorderableListTargetingChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactableTargetingChangedCompare, true);
			
			_reorderableListInteractingChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactableInteractingChangedCompare, true);
		}
		
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			EditorGUILayout.LabelField(nameof(TypedTarget.InteractorState).CamelCaseToHumanReadable(), 
				TypedTarget.InteractorState.ToString());

			GUI.enabled = false;
			
			EditorGUILayout.ObjectField(nameof(TypedTarget.InteractableTargeting).CamelCaseToHumanReadable(), 
				TypedTarget.InteractableTargeting, typeof(InteractableBase),true);
			
			EditorGUILayout.ObjectField(nameof(TypedTarget.InteractableInteracting).CamelCaseToHumanReadable(), 
				TypedTarget.InteractableInteracting, typeof(InteractableBase),true);
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var interactorStateChanged = serializedObject.FindProperty("_interactorStateChanged");
			var interactableTargetingChanged = serializedObject.FindProperty("_interactableTargetingChanged");
			var interactableInteractingChanged = serializedObject.FindProperty("_interactableInteractingChanged");
			
			Space();
			interactorStateChanged.isExpanded = EditorGUILayout.Foldout(interactorStateChanged.isExpanded, 
				$"{nameof(InteractorBase).CamelCaseToHumanReadable()} Events", BasicEditorStyles.BoldFoldout);

			if (!interactorStateChanged.isExpanded) return;
			
//			Space();
//			SectionHeader($"{nameof(InteractorBase).CamelCaseToHumanReadable()} Events");

			Space();
			SectionHeader("State");
			EditorGUILayout.PropertyField(interactorStateChanged);
			_reorderableListInteractorStateChanged.DoLayoutList();

			Space();
			SectionHeader("Targeting");
			EditorGUILayout.PropertyField(interactableTargetingChanged);
			_reorderableListTargetingChanged.DoLayoutList();
			
			Space();
			SectionHeader("Interacting");
			EditorGUILayout.PropertyField(interactableInteractingChanged);
			_reorderableListInteractingChanged.DoLayoutList();
		}
		
	
	}
}