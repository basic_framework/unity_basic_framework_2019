﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	public abstract class InteractableBaseEditor<T> : BasicBaseEditor<T>
		where T : InteractableBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;
		
		private ReorderableList _reorderableListInteractableStateChanged;
		private ReorderableList _reorderableListTargetingChanged;
		private ReorderableList _reorderableListInteractingChanged;


		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var interactableStateChangedCompare = serializedObject.FindProperty("_interactableStateChangedCompare");
			var interactorTargetingChangedCompare = serializedObject.FindProperty("_interactorTargetingChangedCompare");
			var interactorInteractingChangedCompare = serializedObject.FindProperty("_interactorInteractingChangedCompare");

			_reorderableListInteractableStateChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactableStateChangedCompare, true);
			
			_reorderableListTargetingChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactorTargetingChangedCompare, true);
			
			_reorderableListInteractingChanged =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, interactorInteractingChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			EditorGUILayout.LabelField(nameof(TypedTarget.InteractableState).CamelCaseToHumanReadable(), 
				TypedTarget.InteractableState.ToString());


			GUI.enabled = false;
			
			EditorGUILayout.ObjectField(nameof(TypedTarget.InteractorTargeting).CamelCaseToHumanReadable(), 
				TypedTarget.InteractorTargeting, typeof(InteractorBase),true);
			
			EditorGUILayout.ObjectField(nameof(TypedTarget.InteractorInteracting).CamelCaseToHumanReadable(), 
				TypedTarget.InteractorInteracting, typeof(InteractorBase),true);


			var interactorTargetingChanged = serializedObject.FindProperty("_interactorTargetingChanged");
			
			interactorTargetingChanged.isExpanded = EditorGUILayout.Foldout(interactorTargetingChanged.isExpanded, 
				"Interactors Targeting", BasicEditorStyles.BoldFoldout);

			if (interactorTargetingChanged.isExpanded)
			{
				EditorGUI.indentLevel++;
				
				var interactorsTargeting = TypedTarget.EditorGetInteractorsTargeting();

				for (var i = 0; i < interactorsTargeting.Length; i++)
				{
					var interactor = interactorsTargeting[i];
					EditorGUILayout.ObjectField($"Interactor Targeting {i}", interactor, typeof(InteractorBase),
						true);
				}

				EditorGUI.indentLevel--;
			}
			
			
			var interactorInteractingChanged = serializedObject.FindProperty("_interactorInteractingChanged");
			
			interactorInteractingChanged.isExpanded = EditorGUILayout.Foldout(interactorInteractingChanged.isExpanded, "Interactors Interacting",
				BasicEditorStyles.BoldFoldout);

			if (interactorInteractingChanged.isExpanded)
			{
				EditorGUI.indentLevel++;
				
				var interactorsInteracting = TypedTarget.EditorGetInteractorsInteracting();

				for (var i = 0; i < interactorsInteracting.Length; i++)
				{
					var interactor = interactorsInteracting[i];
					EditorGUILayout.ObjectField($"Interactor Interacting {i}", interactor, typeof(InteractorBase),
						true);
				}

				EditorGUI.indentLevel--;
			}
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var interactableStateChanged = serializedObject.FindProperty("_interactableStateChanged");
			var interactorTargetingChanged = serializedObject.FindProperty("_interactorTargetingChanged");
			var interactorInteractingChanged = serializedObject.FindProperty("_interactorInteractingChanged");
			
			Space();
			interactableStateChanged.isExpanded = EditorGUILayout.Foldout(interactableStateChanged.isExpanded, 
				$"{nameof(InteractableBase).CamelCaseToHumanReadable()} Events", BasicEditorStyles.BoldFoldout);

			if (!interactableStateChanged.isExpanded) return;
			
//			Space();
//			SectionHeader($"{nameof(InteractableBase).CamelCaseToHumanReadable()} Events");

			
			
			Space();
			SectionHeader("State");
			EditorGUILayout.PropertyField(interactableStateChanged);
			_reorderableListInteractableStateChanged.DoLayoutList();

			Space();
			SectionHeader("Targeting");
			EditorGUILayout.PropertyField(interactorTargetingChanged);
			_reorderableListTargetingChanged.DoLayoutList();
			
			Space();
			SectionHeader("Interacting");
			EditorGUILayout.PropertyField(interactorInteractingChanged);
			_reorderableListInteractingChanged.DoLayoutList();
		}
		
	}
}