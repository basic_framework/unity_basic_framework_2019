﻿using UnityEngine;

namespace BasicFramework.Core.Interactions
{
	public interface IGrabber 
	{
	
		// **************************************** VARIABLES ****************************************\\

		Transform TransformGrabber { get; }
		

		// ****************************************  METHODS  ****************************************\\


	}
}