﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	[CustomEditor(typeof(InteractorBasicOverlap))]
	public class InteractorBasicOverlapEditor : InteractorBaseEditor<InteractorBasicOverlap>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var transformOrigin = serializedObject.FindProperty("_transformOrigin");
			var radius = serializedObject.FindProperty("_radius");
			var layerMask = serializedObject.FindProperty("_layerMask");
			var queryTriggerInteraction = serializedObject.FindProperty("_queryTriggerInteraction");
			var inputInteract = serializedObject.FindProperty("_inputInteract");
			
			Space();
			SectionHeader($"{nameof(InteractorBasicOverlap).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(transformOrigin);
			EditorGUILayout.PropertyField(radius);
			EditorGUILayout.PropertyField(layerMask);
			EditorGUILayout.PropertyField(queryTriggerInteraction);
			BasicEditorGuiLayout.PropertyFieldNotNull(inputInteract);
		}
	}
}