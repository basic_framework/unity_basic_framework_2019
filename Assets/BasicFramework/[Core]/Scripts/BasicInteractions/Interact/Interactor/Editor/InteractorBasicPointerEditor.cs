﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	[CustomEditor(typeof(InteractorBasicPointer))]
	public class InteractorBasicPointerEditor : InteractorBaseEditor<InteractorBasicPointer>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var physicsPointer = serializedObject.FindProperty("_physicsPointer");
			var interactOnPoint = serializedObject.FindProperty("_interactOnPoint");
			var inputInteract = serializedObject.FindProperty("_inputInteract");
			
			var transformGrabber = serializedObject.FindProperty("_transformGrabber");
			
			Space();
			SectionHeader($"{nameof(InteractorBasicPointer).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(physicsPointer);

			EditorGUILayout.PropertyField(interactOnPoint);

			if (interactOnPoint.boolValue)
			{
				BasicEditorGuiLayout.PropertyFieldCanBeNull(inputInteract);
			}
			else
			{
				BasicEditorGuiLayout.PropertyFieldNotNull(inputInteract);
			}
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformGrabber);

		}
		
	}
}