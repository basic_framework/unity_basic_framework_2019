﻿using System;
using BasicFramework.Core.Interactions.Pointer;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.Interactions
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Interact/Interactor Pointer Basic")]
	public class InteractorBasicPointer : InteractorBase, IGrabber 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The physics pointer used to target an interactable")]
		[SerializeField] private PhysicsPointerBase _physicsPointer = default;

		[Tooltip("Interacts with the interactable when it is pointing at it, " +
		         "i.e. you don't have to hold a button to interact")]
		[SerializeField] private bool _interactOnPoint = default;

		[Tooltip("The input used to start/stop interaction with a targeted object")]
		[SerializeField] private BoolInput _inputInteract = default;

		[Tooltip("A transform to use as a reference for grabbable interactables")]
		[SerializeField] private Transform _transformGrabber = default;

		
		public PhysicsPointerBase PhysicsPointer => _physicsPointer;

		public bool InteractOnPoint
		{
			get => _interactOnPoint;
			set => _interactOnPoint = value;
		}

		public BoolInput InputInteract => _inputInteract;

		public Transform TransformGrabber => _transformGrabber;


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		private void Reset()
		{
			_physicsPointer = GetComponentInChildren<PhysicsPointerBase>();
			_transformGrabber = transform;
		}
		

		//***** overrides *****\\

		protected override void UpdateSearching()
		{
			base.UpdateSearching();
			
			// StartTargeting does null checking
			StartTargeting(GetInteractablePointing());
		}

		protected override void UpdateTargeting(InteractableBase targeting)
		{
			base.UpdateTargeting(targeting);
			
			var interactablePointing = GetInteractablePointing();
			var interactablePointingIsNull = interactablePointing == null;

			// Pointing at different interactable than previous update, stop targeting previous interactable
			if (interactablePointing != targeting)
			{
				StopTargeting();
				
				if (interactablePointingIsNull) return;
				StartTargeting(interactablePointing);
			}

			if (_interactOnPoint)
			{
				StartInteracting();
				return;
			}

			if (_inputInteract.Value)
			{
				StartInteracting();
			}
		}

		protected override void UpdateInteracting(InteractableBase targeting, InteractableBase interacting)
		{
			base.UpdateInteracting(targeting, interacting);
			
			var interactablePointing = GetInteractablePointing();
			var interactablePointingIsNull = interactablePointing == null;

			if (interactablePointing != targeting)
			{
				StopTargeting();
				
				if(!interactablePointingIsNull) 
					StartTargeting(interactablePointing);
			}

			if (_interactOnPoint)
			{
				if (interactablePointing == interacting) return;
				StopInteracting();
				
				if (interactablePointingIsNull) return;
				StartInteracting();
				
				return;
			}

			if (_inputInteract.Value) return;
			StopInteracting();
		}
		

		//***** functions *****\\

		private InteractableBase GetInteractablePointing()
		{
			return _physicsPointer.HitSomething ? _physicsPointer.GameObjectHit.GetComponent<InteractableBase>() : null;
		}

		
	}
}