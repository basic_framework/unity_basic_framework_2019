﻿using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.Interactions
{
	public class InteractorBasicOverlap : InteractorBase, IGrabber 
	{
		
		// TODO: Add collision masks
		
		// TODO: Keep It Simple. Do a sphere overlap check in update.
		// Can have collision physics sepereate, just make sure overlap sphere is larger than collision geometry 
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The origin transform for the overlap check")]
		[SerializeField] private Transform _transformOrigin = default;

		[Tooltip("The radius of the overlap sphere")]
		[SerializeField] private float _radius = 0.2f;

		[Tooltip("The layers that should be considered")]
		[SerializeField] private LayerMask _layerMask = ~0;
		
		[Tooltip("Should the overlap check consider triggers")]
		[SerializeField] private QueryTriggerInteraction _queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
		
		[Tooltip("The input used to start/stop interaction with a targeted object")]
		[SerializeField] private BoolInput _inputInteract = default;
		
		
		public Transform TransformGrabber => _transformOrigin;
		
		private Collider[] _colliders = new Collider[5];
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		private void Reset()
		{
			_transformOrigin = transform;
		}

		private void OnDrawGizmosSelected()
		{
			if (_transformOrigin == null) return;

			var color = Gizmos.color;
			Gizmos.color = Color.magenta;
			
			Gizmos.DrawWireSphere(_transformOrigin.position, _radius);

			Gizmos.color = color;
		}
		
		
		//***** overrides *****\\

		protected override void UpdateSearching()
		{
			base.UpdateSearching();
			
			// TODO: Perform overlap check, check for interactable script, target if possible 

			var countOverlap = PerformOverlapCheck();
			if (countOverlap == 0) return;

			for (int i = 0; i < countOverlap; i++)
			{
				var colliderRigidbody = _colliders[i].attachedRigidbody;
				if(colliderRigidbody == null) continue;
				
				var interactable = colliderRigidbody.GetComponent<InteractableBase>();
				if (interactable == null) continue;
				StartTargeting(interactable);
				break;
			}
		}

		protected override void UpdateTargeting(InteractableBase targeting)
		{
			base.UpdateTargeting(targeting);
			
			// TODO: Perform overlap check, make sure we are still overlapping the targeted interactable 
			
			var countOverlap = PerformOverlapCheck();
			if (countOverlap == 0)
			{
				StopTargeting();
				return;
			}

			InteractableBase targetInteractable = default;
			
			for (int i = 0; i < countOverlap; i++)
			{
				var colliderRigidbody = _colliders[i].attachedRigidbody;
				if(colliderRigidbody == null) continue;
				
				var interactable = colliderRigidbody.GetComponent<InteractableBase>();
				if (interactable == null) continue;

				if (interactable == InteractableTargeting)
				{
					targetInteractable = interactable;
					break;
				}

				if (targetInteractable == null)
				{
					targetInteractable = interactable;
				}
			}

			if (targetInteractable == null)
			{
				StopTargeting();
				return;
			}
			else if (targetInteractable != InteractableTargeting)
			{
				StopTargeting();
				StartTargeting(targetInteractable);
			}

			if (_inputInteract.Value)
			{
				StartInteracting();
			}
		}

		protected override void UpdateInteracting(InteractableBase targeting, InteractableBase interacting)
		{
			base.UpdateInteracting(targeting, interacting);
			
			var countOverlap = PerformOverlapCheck();

			InteractableBase targetInteractable = default;
			
			for (int i = 0; i < countOverlap; i++)
			{
				var colliderRigidbody = _colliders[i].attachedRigidbody;
				if(colliderRigidbody == null) continue;
				
				var interactable = colliderRigidbody.GetComponent<InteractableBase>();
				if (interactable == null) continue;

				if (interactable == InteractableTargeting)
				{
					targetInteractable = interactable;
					break;
				}

				if (targetInteractable == null)
				{
					targetInteractable = interactable;
				}
			}

			if (targetInteractable != targeting)
			{
				StopTargeting();
				StartTargeting(targetInteractable);
			}
			
			if (_inputInteract.Value) return;
			StopInteracting();
		}

		private int PerformOverlapCheck()
		{
			return Physics.OverlapSphereNonAlloc(_transformOrigin.position, _radius, _colliders, 
				_layerMask, _queryTriggerInteraction);
		}
		
	}
}