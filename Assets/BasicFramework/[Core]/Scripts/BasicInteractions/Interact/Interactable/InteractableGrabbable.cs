﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.Interactions
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Interact/Interactable Grabbable")]
	public class InteractableGrabbable : InteractableBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The rigidbody that will be moved")]
		[SerializeField] private Rigidbody _rigidbody = default;

		[Tooltip("Will this grabbable be moved manually (by another script)")]
		[SerializeField] private bool _moveManually = default;

		[Tooltip("Raised when the interactable's IsHeld state changes")]
		[SerializeField] private BoolPlusUnityEvent _isHeldChanged = default;


		public Rigidbody Rigidbody => _rigidbody;

		public bool MoveManually
		{
			get => _moveManually;
			set => _moveManually = value;
		}

		public BoolPlusUnityEvent IsHeldChanged => _isHeldChanged;
		

		private bool _previouslyKinematic;

		private bool _isHeld;
		public bool IsHeld
		{
			get => _isHeld;
			private set
			{
				if (_isHeld == value) return;
				_isHeld = value;
				IsHeldUpdated(value);
			}
		}

		private Transform _transformGrabber;
		public Transform TransformGrabber => _transformGrabber;
		
		private Vector3 _offsetPosition;
		public Vector3 OffsetPosition => _offsetPosition;
		
		private Quaternion _offsetRotation;
		public Quaternion OffsetRotation => _offsetRotation;


		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		private void Reset()
		{
			_rigidbody = GetComponentInChildren<Rigidbody>();
		}

		private void Awake()
		{
			_previouslyKinematic = _rigidbody.isKinematic;
		}

		protected override void Start()
		{
			base.Start();

			IsHeldUpdated(_isHeld);
		}
		
		private void Update()
		{
			if (!_isHeld) return;
			if (_moveManually) return;

			_rigidbody.MovePosition(_transformGrabber.TransformPoint(_offsetPosition));
			_rigidbody.MoveRotation(MathUtility.TransformQuaternion(_transformGrabber.rotation, _offsetRotation));
		}
		

		//***** overrides *****\\

		protected override bool CanInteract(InteractorBase interactor)
		{
			var iGrabber = interactor as IGrabber;
			return iGrabber != null;
		}

		protected override void OnInteractableStateUpdated(InteractableStates interactableState)
		{
			base.OnInteractableStateUpdated(interactableState);
			
			switch (interactableState)
			{
				case InteractableStates.Disabled:
				case InteractableStates.Waiting:
				case InteractableStates.Targeted:
					IsHeld = false;
					break;
				
				case InteractableStates.Interacting:

					var iGrabber = InteractorInteracting as IGrabber;
					if(iGrabber == null) break;
					
					IsHeld = true;
					
					// Possibly check type of interactor, if grabbing interactor get more information
					_transformGrabber = iGrabber.TransformGrabber;
					_offsetPosition = _transformGrabber.InverseTransformPoint(_rigidbody.position);
					_offsetRotation =
						MathUtility.InverseTransformQuaternion(_transformGrabber.rotation, _rigidbody.rotation);
					
					break;
				
				default:
					throw new ArgumentOutOfRangeException(nameof(interactableState), interactableState, null);
			}
		}
		
		
		//***** property updates *****\\
		
		private void IsHeldUpdated(bool value)
		{
			if (value)
			{
				_previouslyKinematic = _rigidbody.isKinematic;
				_rigidbody.isKinematic = true;
			}
			else
			{
				_rigidbody.isKinematic = _previouslyKinematic;
			}
			
			_isHeldChanged.Raise(value);
		}
		
	}
}