﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
    [CustomEditor(typeof(InteractableGrabbable))]
    public class InteractableGrabbableEditor : InteractableBaseEditor<InteractableGrabbable>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        protected override void DrawDebugFields()
        {
            EditorGUILayout.LabelField(nameof(TypedTarget.IsHeld).CamelCaseToHumanReadable(), 
                TypedTarget.IsHeld.ToString());

            Space();
            base.DrawDebugFields();
        }
	
        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            var rigidbody = serializedObject.FindProperty("_rigidbody");
            var moveManually = serializedObject.FindProperty("_moveManually");
			
            Space();
            SectionHeader($"{nameof(InteractableGrabbable).CamelCaseToHumanReadable()} Properties");

            BasicEditorGuiLayout.PropertyFieldNotNull(rigidbody);
            EditorGUILayout.PropertyField(moveManually);
        }
		
        protected override void DrawEventFields()
        {
            base.DrawEventFields();
			
            var isHeldChanged = serializedObject.FindProperty("_isHeldChanged");

            Space();
            SectionHeader($"{nameof(InteractableGrabbable).CamelCaseToHumanReadable()} Events");
			
            EditorGUILayout.PropertyField(isHeldChanged);
        }
		
    }
}