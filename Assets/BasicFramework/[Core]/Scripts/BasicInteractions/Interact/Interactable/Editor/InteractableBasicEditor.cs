﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	[CustomEditor(typeof(InteractableBasic))]
	public class InteractableBasicEditor : InteractableBaseEditor<InteractableBasic>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}