﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Editor
{
	[CustomEditor(typeof(InteractableTimed))]
	public class InteractableTimedEditor : InteractableBaseEditor<InteractableTimed>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			GUI.enabled = false;
			
			EditorGUILayout.LabelField(nameof(TypedTarget.Timer).CamelCaseToHumanReadable(), 
				TypedTarget.Timer.ToString());

			var normalizedTime = TypedTarget.NormalizedTimer.Value;

			EditorGUILayout.Slider(nameof(TypedTarget.NormalizedTimer).CamelCaseToHumanReadable(), normalizedTime, 0, 1);
			
			EditorGUILayout.LabelField(nameof(TypedTarget.IsActivated).CamelCaseToHumanReadable(), 
				TypedTarget.IsActivated.ToString());
			
			GUI.enabled = PrevGuiEnabled;
			
			Space();
			base.DrawDebugFields();
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var useUnscaledDeltaTime = serializedObject.FindProperty("_useUnscaledDeltaTime");
			var durationActivate = serializedObject.FindProperty("_durationActivate");
			var onStopInteractingResetTimer = serializedObject.FindProperty("_onStopInteractingResetTimer");
			var onStopInteractingResetActive = serializedObject.FindProperty("_onStopInteractingResetActive");
			var onDisableResetAll = serializedObject.FindProperty("_onDisableResetAll");
			
			Space();
			SectionHeader($"{nameof(InteractableTimed).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(useUnscaledDeltaTime);
			EditorGUILayout.PropertyField(durationActivate);
			
			Space();
			EditorGUILayout.PropertyField(onStopInteractingResetTimer);
			EditorGUILayout.PropertyField(onStopInteractingResetActive);
			EditorGUILayout.PropertyField(onDisableResetAll);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var timerChanged = serializedObject.FindProperty("_timerChanged");
			var normalizedTimerChanged = serializedObject.FindProperty("_normalizedTimerChanged");
			var isActivatedChanged = serializedObject.FindProperty("_isActivatedChanged");
			
			Space();
			SectionHeader($"{nameof(InteractableTimed).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(isActivatedChanged);
			EditorGUILayout.PropertyField(normalizedTimerChanged);
			EditorGUILayout.PropertyField(timerChanged);
		}
		
	}
}