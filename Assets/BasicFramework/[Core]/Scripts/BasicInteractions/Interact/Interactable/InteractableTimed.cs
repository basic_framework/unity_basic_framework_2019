﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.Interactions
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Interact/Interactable Timed")]
	public class InteractableTimed : InteractableBase
	{
		
		// Continue counting even if released then reengaged
		// Reset the activation status on release
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Use unscaled delta time instead of delta time")]
		[SerializeField] private bool _useUnscaledDeltaTime = default;
		
		[Tooltip("How long it has to be interacted with before activating")]
		[SerializeField] private float _durationActivate = 1f;

		[Tooltip("Reset the timer when the interaction is removed")]
		[SerializeField] private bool _onStopInteractingResetTimer = true;

		[Tooltip("Reset the active status when the interaction is removed")]
		[SerializeField] private bool _onStopInteractingResetActive = default;

		[Tooltip("Reset the timer to 0 and activated status to false when the component is disabled")]
		[SerializeField] private bool _onDisableResetAll = default;


		[Tooltip("Raised when the timer changes")]
		[SerializeField] private FloatUnityEvent _timerChanged = default;

		[Tooltip("Raised when the normalized state (Timer/Duration Activate) changes")]
		[SerializeField] private NormalizedFloatPlusUnityEvent _normalizedTimerChanged = default;
		
		[Tooltip("Raised when the activated status changes, i.e. the timer is greater than the activate duration")]
		[SerializeField] private BoolPlusUnityEvent _isActivatedChanged = default;
		
		


		public float DurationActivate
		{
			get => _durationActivate;
			set => _durationActivate = Mathf.Max(0, value);
		}

		public bool ResetOnReleaseTimer
		{
			get => _onStopInteractingResetTimer;
			set => _onStopInteractingResetTimer = value;
		}

		public bool ResetOnReleaseActive
		{
			get => _onStopInteractingResetActive;
			set => _onStopInteractingResetActive = value;
		}

		public BoolPlusUnityEvent IsActivatedChanged => _isActivatedChanged;

		public FloatUnityEvent TimerChanged => _timerChanged;

		public NormalizedFloatPlusUnityEvent NormalizedTimerChanged => _normalizedTimerChanged;


		private float _timer;
		public float Timer
		{
			get => _timer;
			private set
			{
				if (Math.Abs(_timer - value) < Mathf.Epsilon) return;
				_timer = value;
				TimerUpdated(value);
			}
		}
		
		private NormalizedFloat _normalizedTimer;
		public NormalizedFloat NormalizedTimer
		{
			get => _normalizedTimer;
			private set
			{
				if (_normalizedTimer == value) return;
				_normalizedTimer = value;
				NormalizedTimerUpdated(value);
			}
		}

		private bool _isActivated;
		public bool IsActivated
		{
			get => _isActivated;
			private set
			{
				if (_isActivated == value) return;
				_isActivated = value;
				IsActivatedUpdated(value);
			}
		}
		
		

		


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		private void OnValidate()
		{
			_durationActivate = Mathf.Max(0, _durationActivate);
		}

		protected override void OnDisable()
		{
			base.OnDisable();

			if (!_onDisableResetAll) return;
			ResetInteractable();
		}

		protected override void Start()
		{
			base.Start();
			
			TimerUpdated(_timer);
			NormalizedTimerUpdated(_normalizedTimer);
			IsActivatedUpdated(_isActivated);
		}

		private void Update()
		{
			if (InteractableState != InteractableStates.Interacting) return;

			Timer += _useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
		}


		//***** property updated *****\\
		
		private void TimerUpdated(float value)
		{
			_timerChanged.Invoke(value);
			
			if (Math.Abs(_durationActivate) < Mathf.Epsilon)
			{
				NormalizedTimer = Math.Abs(value) < Mathf.Epsilon ? NormalizedFloat.Zero : NormalizedFloat.One;
			}
			else
			{
				NormalizedTimer = new NormalizedFloat(value / _durationActivate);
			}
		}
		
		private void NormalizedTimerUpdated(NormalizedFloat value)
		{
			_normalizedTimerChanged.Raise(value);
			
			if (value == NormalizedFloat.One)
			{
				IsActivated = true;
			}
			else if (_isActivated && _onStopInteractingResetActive)
			{
				IsActivated = false;
			}
		}
		
		private void IsActivatedUpdated(bool value)
		{
			_isActivatedChanged.Raise(value);
		}
		

		//***** overrides *****\\

		protected override void OnInteractableStateUpdated(InteractableStates interactableState)
		{
			base.OnInteractableStateUpdated(interactableState);

			switch (interactableState)
			{
				case InteractableStates.Disabled:
				case InteractableStates.Waiting:
				case InteractableStates.Targeted:
					if (_onStopInteractingResetTimer) Timer = 0f;
					break;
				
				case InteractableStates.Interacting:
					break;
				
				default:
					throw new ArgumentOutOfRangeException(nameof(interactableState), interactableState, null);
			}
		}
		
		
		//***** functions *****\\

		public void ResetInteractable()
		{
			Timer = 0;
			IsActivated = false;
		}
		
	}
}