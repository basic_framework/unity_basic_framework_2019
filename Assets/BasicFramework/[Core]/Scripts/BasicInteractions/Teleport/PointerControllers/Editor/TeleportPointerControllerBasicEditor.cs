﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport.Editor
{
    [CustomEditor(typeof(TeleportPointerControllerBasic))]
    public class TeleportPointerControllerBasicEditor : TeleportPointerControllerBaseEditor<TeleportPointerControllerBasic>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            var inputTeleportPointerEnabled = serializedObject.FindProperty("_inputTeleportPointerEnabled");
			
            var inputFaceDirection = serializedObject.FindProperty("_inputFaceDirection");
            var faceForwardOnRelease = serializedObject.FindProperty("_faceForwardOnRelease");
			
            var inputTeleport = serializedObject.FindProperty("_inputTeleport");
			
            Space();
            SectionHeader($"{nameof(TeleportPointerControllerBasic).CamelCaseToHumanReadable()} Properties");
			
            Space();
            BasicEditorGuiLayout.PropertyFieldCanBeNull(inputTeleportPointerEnabled);
			
            Space();
            EditorGUILayout.PropertyField(faceForwardOnRelease);
            BasicEditorGuiLayout.PropertyFieldCanBeNull(inputFaceDirection);
			
            Space();
            BasicEditorGuiLayout.PropertyFieldCanBeNull(inputTeleport);
        }
	
    }
}