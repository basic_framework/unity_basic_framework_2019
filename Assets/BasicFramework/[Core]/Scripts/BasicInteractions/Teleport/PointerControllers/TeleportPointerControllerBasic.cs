﻿using System;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Teleport/Teleport Pointer Controller Basic")]
	public class TeleportPointerControllerBasic : TeleportPointerControllerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The input used to toggle on and off the teleporter")]
		[SerializeField] private BoolInput _inputTeleportPointerEnabled = default;
		
		[Tooltip("The input used to set the face direction of the target")]
		[SerializeField] private Axis2Input _inputFaceDirection = default;
		
		[Tooltip("The input used to teleport the user")] 
		[SerializeField] private ActionInput _inputTeleport = default;
		
		[Tooltip("When the Face Direction Input is released (i.e. magnitude zero) face forward")]
		[SerializeField] private bool _faceForwardOnRelease = default;
		
		private Vector2 _faceDirection = Vector2.up;
		public bool FaceForwardOnRelease
		{
			get => _faceForwardOnRelease;
			set => _faceForwardOnRelease = value;
		}
		
		public new bool TeleportPointerEnabled
		{
			get => base.TeleportPointerEnabled;
			set => base.TeleportPointerEnabled = value;
		}

		private bool _inputTeleportPointerEnabledNotNull = default;
		private bool _inputFaceDirectionNotNull = default;
		private bool _inputTeleportNotNull = default;

	
		// ****************************************  METHODS  ****************************************\\
	
		//***** mono *****\\

		protected override void OnValidate()
		{
			base.OnValidate();

			_inputTeleportNotNull = _inputTeleport != null;
			_inputTeleportPointerEnabledNotNull = _inputTeleportPointerEnabled != null;
			_inputFaceDirectionNotNull = _inputFaceDirection != null;
		}

		protected override void Awake()
		{
			base.Awake();
			
			_inputTeleportNotNull = _inputTeleport != null;
			_inputTeleportPointerEnabledNotNull = _inputTeleportPointerEnabled != null;
			_inputFaceDirectionNotNull = _inputFaceDirection != null;
		}

		private void Update()
		{
			if (_inputTeleportPointerEnabledNotNull)
			{
				TeleportPointerEnabled = _inputTeleportPointerEnabled.Value;
			}

			if (!TeleportPointerEnabled) return;
			
			if (_inputFaceDirectionNotNull)
			{
				var faceDirection = _inputFaceDirection.Value.normalized;

				if (Math.Abs(faceDirection.sqrMagnitude) < Mathf.Epsilon)
				{
					faceDirection = _faceForwardOnRelease ? Vector2.up : _faceDirection;
				}

				_faceDirection = faceDirection;
				TeleportPointer.FaceDirection = _faceDirection;
			}
			
			if (_inputTeleportNotNull && _inputTeleport.Value)
			{
				Teleport();
			}
		}
		
		
		//***** overrides *****\\

		protected override void OnTeleportPointerStateChanged(TeleportPointerStates value)
		{
			if (value == TeleportPointerStates.Disabled)
			{
				_faceDirection = Vector2.up;
				TeleportPointer.FaceDirection = _faceDirection;
			}
		}
		
	}
}