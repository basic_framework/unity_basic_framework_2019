﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport.Editor
{
	public abstract class TeleportPointerControllerBaseEditor<T> : BasicBaseEditor<T>
		where T : TeleportPointerControllerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			EditorGUILayout.LabelField(nameof(TypedTarget.IsTeleporting).CamelCaseToHumanReadable(), 
				TypedTarget.IsTeleporting.ToString());

			GUI.enabled = false;
			
			EditorGUILayout.Slider(nameof(TypedTarget.FadeStatus).CamelCaseToHumanReadable(),
				TypedTarget.FadeStatus.Value, 0, 1);
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var traveller = serializedObject.FindProperty("_traveller");
			var durationTeleport = serializedObject.FindProperty("_durationTeleport");
			
			Space();
			SectionHeader($"{nameof(TeleportPointerControllerBase).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(traveller);
			EditorGUILayout.PropertyField(durationTeleport);
		}
		
		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var isTeleportingChanged = serializedObject.FindProperty("_isTeleportingChanged");
			var fadeStatusChanged = serializedObject.FindProperty("_fadeStatusChanged");
			var teleported = serializedObject.FindProperty("_teleported");
			
			
			Space();
			SectionHeader($"{nameof(TeleportPointerControllerBase).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(isTeleportingChanged);
			EditorGUILayout.PropertyField(fadeStatusChanged);
			EditorGUILayout.PropertyField(teleported);
		}
	
	}
}