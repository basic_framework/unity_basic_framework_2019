﻿using System;
using System.Collections;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Teleport
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_TELEPORT_POINTER_CONTROLLER)]
	[RequireComponent(typeof(TeleportPointer))]
	public abstract class TeleportPointerControllerBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// Teleporting

		[Tooltip("The transform that will be teleported")]
		[SerializeField] private Transform _traveller = default;

		[Tooltip("How long it takes to teleport, fade out > teleport > fade in ")]
		[SerializeField] private float _durationTeleport = 0.25f;

		[Tooltip("Raised just after the traveller has been moved")]
		[SerializeField] private UnityEvent _teleported = default;
		
		[Tooltip("Raised when the status of teleporting changes")]
		[SerializeField] private BoolPlusUnityEvent _isTeleportingChanged = default;
		
		[Tooltip("The amount the screen should be faded, 0 = none, 1 = full")]
		[SerializeField] private NormalizedFloatPlusUnityEvent _fadeStatusChanged = default;


		public TeleportPointer TeleportPointer => _teleportPointer;

		public Transform Traveller => _traveller;

		public float DurationTeleport
		{
			get => _durationTeleport;
			set => _durationTeleport = Mathf.Max(0, value);
		}

		public UnityEvent Teleported => _teleported;

		public BoolPlusUnityEvent IsTeleportingChanged => _isTeleportingChanged;


		public bool TeleportPointerEnabled
		{
			get => _teleportPointer.enabled;
			protected set => _teleportPointer.enabled = value;
		}
		
		private bool _isTeleporting;
		public bool IsTeleporting
		{
			get => _isTeleporting;
			private set
			{
				if (_isTeleporting == value) return;
				_isTeleporting = value;
				IsTeleportingUpdated(value);
			}
		}
		
		private NormalizedFloat _fadeStatus;
		public NormalizedFloat FadeStatus
		{
			get => _fadeStatus;
			private set
			{
				if (_fadeStatus == value) return;
				_fadeStatus = value;
				FadeStatusUpdated(value);
			}
		}
		
		private Vector2 FaceDirection
		{
			get => _teleportPointer.FaceDirection;
			set => _teleportPointer.FaceDirection = value;
		}

		private TeleportPointer _teleportPointer = default;
		
		private Coroutine _teleportCoroutine;
		
		
		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		protected virtual void Reset()
		{
			_traveller = transform;
		}
		
		protected virtual void OnValidate()
		{
			_durationTeleport = Mathf.Max(0, _durationTeleport);
		}

		protected virtual void Awake()
		{
			_teleportPointer = GetComponent<TeleportPointer>();
		}

		protected virtual void OnEnable()
		{
			OnTeleportPointerStateChanged(TeleportPointer.TeleportPointerState);
			
			TeleportPointer.TeleportPointerStateChanged.AddListener(OnTeleportPointerStateChanged);
		}
		
		protected virtual void OnDisable()
		{
			TeleportPointer.TeleportPointerStateChanged.RemoveListener(OnTeleportPointerStateChanged);

			StopTeleporting();
		}

		protected virtual void Start()
		{
			IsTeleportingUpdated(_isTeleporting);
			FadeStatusUpdated(_fadeStatus);
		}

		//***** callbacks *****\\
		
		protected virtual void OnTeleportPointerStateChanged(TeleportPointerStates value){}
		
		
		//***** properties updated *****\\
		
		private void IsTeleportingUpdated(bool value)
		{
			_isTeleportingChanged.Raise(value);
		}
		
		private void FadeStatusUpdated(NormalizedFloat value)
		{
			_fadeStatusChanged.Raise(value);
		}
		
		private void FaceDirectionUpdated(Vector2 value)
		{
			_teleportPointer.BasicPointerEventData.FaceDirection = value;
		}
		
		
		//***** virtual functions *****\\
		
		protected virtual void OnTeleported(){}
		
		
		//***** functions *****\\
		
		public void Teleport()
		{
			if (_isTeleporting) return;
			if (!enabled) return;
			if (!TeleportPointerEnabled) return;
			if (!_teleportPointer.HasValidTarget) return;
			
			_teleportCoroutine = StartCoroutine(TeleportCoroutine(_teleportPointer.PoseTarget));
		}
		
		private void StopTeleporting()
		{
			if (_teleportCoroutine != null)
			{
				StopCoroutine(_teleportCoroutine);
			}
			
			IsTeleporting = false;
			FadeStatus = NormalizedFloat.Zero;
		}
		
		
		//***** coroutines *****\\

		private IEnumerator TeleportCoroutine(BasicPose poseTarget)
		{
			IsTeleporting = true;

			var timer = 0f;
			var halfDuration = _durationTeleport * 0.5f;

			while (timer < halfDuration)
			{
				timer += Time.deltaTime;
				FadeStatus = new NormalizedFloat(timer / halfDuration);
				yield return null;
			}

//			yield return null;
			yield return null;
			
			_traveller.position = poseTarget.Position;
			_traveller.rotation = poseTarget.Rotation;
			
			OnTeleported();
			_teleported.Invoke();
			
//			yield return null;
			yield return null;
			
			while (timer > 0)
			{
				timer -= Time.deltaTime;
				FadeStatus = new NormalizedFloat(timer / halfDuration);
				yield return null;
			}

			IsTeleporting = false;
		}
		
	}
}