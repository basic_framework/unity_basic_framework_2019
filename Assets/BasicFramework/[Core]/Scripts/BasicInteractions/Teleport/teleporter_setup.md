To setup teleport functionality follow the steps below

1. Add a PhysicsPointer to the scene
2. Add a TeleportPointer to the scene
3. Link TeleportPointer to physics pointer