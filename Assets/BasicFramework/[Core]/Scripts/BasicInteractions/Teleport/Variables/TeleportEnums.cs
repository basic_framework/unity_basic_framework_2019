﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Teleport
{
	
	public enum TeleportPointerStates
	{
		Disabled,
		Searching,
		TargetInvalid,
		TargetValid
	}
	
	[Serializable]
	public class TeleportPointerStatesUnityEvent : UnityEvent<TeleportPointerStates>{}

	[Serializable]
	public class TeleportPointerStatesCompareUnityEvent : CompareValueUnityEventBase<TeleportPointerStates>{}
	
}