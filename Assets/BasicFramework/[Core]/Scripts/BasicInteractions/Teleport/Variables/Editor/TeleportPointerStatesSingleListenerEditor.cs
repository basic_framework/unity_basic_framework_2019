using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Teleport.Editor
{
    [CustomEditor(typeof(TeleportPointerStatesSingleListener))][CanEditMultipleObjects]
    public class TeleportPointerStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
        TeleportPointerStates, 
        TeleportPointerStatesSingle, 
        TeleportPointerStatesUnityEvent,
        TeleportPointerStatesCompareUnityEvent,
        TeleportPointerStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}