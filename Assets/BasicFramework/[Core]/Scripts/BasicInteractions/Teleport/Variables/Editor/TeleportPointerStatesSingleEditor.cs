using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Teleport.Editor
{
    [CustomEditor(typeof(TeleportPointerStatesSingle))][CanEditMultipleObjects]
    public class TeleportPointerStatesSingleEditor : SingleStructBaseEditor<TeleportPointerStates, TeleportPointerStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(TeleportPointerStatesSingle))]
    public class TeleportPointerStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(TeleportPointerStatesSingleReference))]
    public class TeleportPointerStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}