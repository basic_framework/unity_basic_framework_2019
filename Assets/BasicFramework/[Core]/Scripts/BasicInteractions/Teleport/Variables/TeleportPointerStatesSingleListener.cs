using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport
{
    [AddComponentMenu("Basic Framework/Core/Interactions/Teleport/TeleportPointer States Single Listener")]
    public class TeleportPointerStatesSingleListener : SingleStructListenerBase
    <
        TeleportPointerStates, 
        TeleportPointerStatesSingle, 
        TeleportPointerStatesUnityEvent, 
        TeleportPointerStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}