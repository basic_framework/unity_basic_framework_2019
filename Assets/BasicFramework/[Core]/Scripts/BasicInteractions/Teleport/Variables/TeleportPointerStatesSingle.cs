using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Teleport
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Interaction/Teleport/Teleport Pointer States Single", 
        fileName = "single_teleport_pointer_states_")
    ]
    public class TeleportPointerStatesSingle : SingleStructBase<TeleportPointerStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(TeleportPointerStatesSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class TeleportPointerStatesSingleReference : SingleStructReferenceBase<TeleportPointerStates, TeleportPointerStatesSingle>
    {
        public TeleportPointerStatesSingleReference()
        {
        }

        public TeleportPointerStatesSingleReference(TeleportPointerStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class TeleportPointerStatesSingleUnityEvent : UnityEvent<TeleportPointerStatesSingle>{}
	
    [Serializable]
    public class TeleportPointerStatesSingleArrayUnityEvent : UnityEvent<TeleportPointerStatesSingle[]>{}
    
}