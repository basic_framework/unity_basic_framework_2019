﻿using BasicFramework.Core.BasicDebug.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Interactions.Pointer;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Teleport/Teleport Pointer")]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_TELEPORT_POINTER)]
	public class TeleportPointer : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// Debug
		
		[SerializeField] private bool _editorDrawTargetPose = true;
		
		
		// Targeting
		
		[SerializeField] private PhysicsPointerBase _physicsPointer = default;
		
		[Tooltip("The direction that will be used as up when checking slope validity")]
		[SerializeField] private Vector3 _directionUp = Vector3.up;

		[Tooltip("The maximum acceptable slope angle")]
		[SerializeField] private float _slopeAngleMax = 15f;
		
		[Tooltip("The layers that are acceptable to teleport to.\n" +
		         "Note: The pointer may still hit other layers, " +
		         "set the Pointer's Collision Mask if you don't want to hit certain layer")]
		[SerializeField] private LayerMask _layerMaskTeleportable = ~0;
		
		[Tooltip("Should the travellers up direction be to the specified up direction")]
		[SerializeField] private bool _alignTargetToDirectionUp = true;
		
		[SerializeField] private TeleportPointerStatesUnityEvent _teleportPointerStateChanged = default;
		[SerializeField] private TeleportPointerStatesCompareUnityEvent[] _teleportPointerStateChangedCompare = default;
		
		
		public PhysicsPointerBase PhysicsPointer => _physicsPointer;

		public Vector3 DirectionUp
		{
			get => _directionUp;
			set => _directionUp = value;
		}

		public float SlopeAngleMax
		{
			get => _slopeAngleMax;
			set => _slopeAngleMax = value;
		}

		public LayerMask LayerMaskTeleportable
		{
			get => _layerMaskTeleportable;
			set => _layerMaskTeleportable = value;
		}

		public bool AlignTargetToDirectionUp
		{
			get => _alignTargetToDirectionUp;
			set => _alignTargetToDirectionUp = value;
		}

		public TeleportPointerStatesUnityEvent TeleportPointerStateChanged => _teleportPointerStateChanged;


		private TeleportPointerStates _teleportPointerState;
		public TeleportPointerStates TeleportPointerState
		{
			get => _teleportPointerState;
			private set
			{
				if (_teleportPointerState == value) return;
				_teleportPointerState = value;
				TeleportPointerStateUpdated(value);
			}
		}
		
		public Vector2 FaceDirection
		{
			get => BasicPointerEventData.FaceDirection;
			set => BasicPointerEventData.FaceDirection = value;
		}
		
		
		public BasicPointerEventData BasicPointerEventData => _physicsPointer.BasicPointerEventData;

		public bool HasTarget => TeleportPointerState == TeleportPointerStates.TargetInvalid ||
		                         TeleportPointerState == TeleportPointerStates.TargetValid;

		public bool HasValidTarget => TeleportPointerState == TeleportPointerStates.TargetValid;

		public BasicPose PoseTarget => BasicPointerEventData.GetTargetPose();
		
		
		
	
		// ****************************************  METHODS  ****************************************\\
	
		//***** mono *****\\
		
		private void Reset()
		{
			_physicsPointer = GetComponentInChildren<PhysicsPointerBase>();
		}
		
		private void OnValidate()
		{
			_slopeAngleMax = Mathf.Clamp(_slopeAngleMax, 0.1f, 80);
		}
		
		
		private void OnEnable()
		{
			TeleportPointerState = TeleportPointerStates.Searching;
		}

		private void OnDisable()
		{
			TeleportPointerState = TeleportPointerStates.Disabled;
		}
		
		private void Start()
		{
			TeleportPointerStateUpdated(_teleportPointerState);
		}
		
		private void Update()
		{
			var basicPointerEventData = _physicsPointer.BasicPointerEventData;
			
			TeleportPointerState = DeterminePointerState(basicPointerEventData);
			
			// Possibly change this to event based
			if (HasValidTarget)
			{
				basicPointerEventData.AlignTargetToDirectionUp = _alignTargetToDirectionUp;
				basicPointerEventData.DirectionUp = _directionUp;
			}
			else
			{
				basicPointerEventData.AlignTargetToDirectionUp = false;
			}
			
#if UNITY_EDITOR
			var poseTarget = PoseTarget;
			
			if (_editorDrawTargetPose)
			{
				DebugUtility.DrawOrientation(poseTarget.Position, poseTarget.Rotation);
			}
#endif
		}
		
		
		//***** properties updated *****\\
		
		private void TeleportPointerStateUpdated(TeleportPointerStates value)
		{
			_physicsPointer.enabled = value != TeleportPointerStates.Disabled;
			
			_teleportPointerStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _teleportPointerStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\

		private TeleportPointerStates DeterminePointerState(BasicPointerEventData basicPointerEventData)
		{
			if (!basicPointerEventData.HitSomething)
			{
//				TeleportPointerState = TeleportPointerStates.Searching;
				return TeleportPointerStates.Searching;
			}

			var hitNormal = basicPointerEventData.HitNormal;
			var hitGo = basicPointerEventData.HitGameObject;
			
			// Do slope check
			var slopeAngle = Vector3.Angle(_directionUp, hitNormal);
			if (slopeAngle > _slopeAngleMax)
			{
//				TeleportPointerState = TeleportPointerStates.TargetInvalid;
				return TeleportPointerStates.TargetInvalid;
			}
			
			// Do layer check
			var layerValid = (_layerMaskTeleportable & (1 << hitGo.layer)) == (1 << hitGo.layer);
			if (!layerValid)
			{
//				TeleportPointerState = TeleportPointerStates.TargetInvalid;
				return TeleportPointerStates.TargetInvalid;
			}
			
//			TeleportPointerState = TeleportPointerStates.TargetValid;
			return TeleportPointerStates.TargetValid;
		}
	
	}
}