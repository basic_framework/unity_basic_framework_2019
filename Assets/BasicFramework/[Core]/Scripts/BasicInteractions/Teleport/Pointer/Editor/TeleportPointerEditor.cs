﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Teleport.Editor
{
	[CustomEditor(typeof(TeleportPointer))]
	public class TeleportPointerEditor : BasicBaseEditor<TeleportPointer>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		private ReorderableList _reorderableListComparePointerState;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();

			var teleportPointerStateChangedCompare =
				serializedObject.FindProperty("_teleportPointerStateChangedCompare");

			_reorderableListComparePointerState =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, teleportPointerStateChangedCompare,
					true);
		}
		
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var editorDrawTargetPose = serializedObject.FindProperty("_editorDrawTargetPose");
			
			EditorGUILayout.PropertyField(editorDrawTargetPose);

			EditorGUILayout.LabelField(nameof(TypedTarget.TeleportPointerState).CamelCaseToHumanReadable(), 
				TypedTarget.TeleportPointerState.ToString());
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var physicsPointer = serializedObject.FindProperty("_physicsPointer");
			
			var directionUp = serializedObject.FindProperty("_directionUp");
			
			var slopeAngleMax = serializedObject.FindProperty("_slopeAngleMax");
			var layerMaskTeleportable = serializedObject.FindProperty("_layerMaskTeleportable");

			var alignTargetToDirectionUp = serializedObject.FindProperty("_alignTargetToDirectionUp");

			Space();
			SectionHeader($"{nameof(TeleportPointer).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(physicsPointer);
			
			BasicEditorGuiLayout.Vector3DirectionDropdown(directionUp);
			EditorGUILayout.PropertyField(alignTargetToDirectionUp);

			EditorGUILayout.PropertyField(slopeAngleMax);
			EditorGUILayout.PropertyField(layerMaskTeleportable);
		}
		
		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var teleportPointerStateChanged = serializedObject.FindProperty("_teleportPointerStateChanged");
			
			Space();
			SectionHeader($"{nameof(TeleportPointer).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(teleportPointerStateChanged);
			_reorderableListComparePointerState.DoLayoutList();
		}
	
	}
}