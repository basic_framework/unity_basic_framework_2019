﻿using System.Linq;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Trigger
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Trigger/Basic Trigger Listener")]
	public class BasicTriggerListener : TriggerListenerBase 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The properties to compare against before raising the events")]
		[SerializeField] private TriggerCompareModeFlags _triggerCompareMode = TriggerCompareModeFlags.NoCompare;

		[SerializeField] private TagField _compareTags = TagField.MultiTag;

		[SerializeField] private LayerMask _compareLayers = default;

		[SerializeField] private Rigidbody[] _compareRigidbodies = default;

		[SerializeField] private Collider[] _compareColliders = default;


		// ****************************************  METHODS  ****************************************\\
		
		protected override bool IsColliderValid(Collider other, bool checkAttachedRigidbody)
		{
			if (_triggerCompareMode == TriggerCompareModeFlags.NoCompare) return true;
			
			if ((_triggerCompareMode & TriggerCompareModeFlags.Collider) != 0)
			{
				if (_compareColliders.Contains(other)) return true;
			}
			
			var attachedRigidbody = other.attachedRigidbody;
			var attachedRigidbodyIsNull = attachedRigidbody == null;
			
			if ((_triggerCompareMode & TriggerCompareModeFlags.Tag) != 0)
			{
				if (_compareTags.ContainsTag(other.tag))
				{
					return true;
				}
				
				if (checkAttachedRigidbody
				    && !attachedRigidbodyIsNull
				    && _compareTags.ContainsTag(attachedRigidbody.tag))
				{
					return true;
				}
			}

			if ((_triggerCompareMode & TriggerCompareModeFlags.Layer) != 0)
			{
				if ((_compareLayers & other.gameObject.layer) != 0)
				{
					return true;
				}
				
				if (checkAttachedRigidbody
				    && !attachedRigidbodyIsNull
				    && (_compareLayers & attachedRigidbody.gameObject.layer) != 0)
				{
					return true;
				}
			}
			
			if ((_triggerCompareMode & TriggerCompareModeFlags.Rigidbody) != 0)
			{
				var rigidBodyOnSameGameObject = !attachedRigidbodyIsNull && attachedRigidbody.gameObject == other.gameObject;
				if (rigidBodyOnSameGameObject && _compareRigidbodies.Contains(attachedRigidbody))
				{
					return true;
				}
				
				if (checkAttachedRigidbody
				    && !attachedRigidbodyIsNull
				    && _compareRigidbodies.Contains(attachedRigidbody))
				{
					return true;
				}
			}
			
			return false;
		}
		
	}
}