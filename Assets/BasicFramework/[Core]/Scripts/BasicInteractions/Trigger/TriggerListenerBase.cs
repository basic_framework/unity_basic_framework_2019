﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Trigger
{
	public abstract class TriggerListenerBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("When determining if collider is valid, also check the rigidbody the collider is attached to. " +
		         "Only set false if you are interested in a specific collider/gameobject")]
		[SerializeField] private bool _checkAttachedRigidbody = true;
		
		[SerializeField] private UnityEvent _triggerEnter = default;
		[SerializeField] private UnityEvent _triggerStay = default;
		[SerializeField] private UnityEvent _triggerExit = default;
		
		
		public UnityEvent TriggerEnter => _triggerEnter;

		public UnityEvent TriggerStay => _triggerStay;

		public UnityEvent TriggerExit => _triggerExit;


		// ****************************************  METHODS  ****************************************\\
	
		protected virtual void OnTriggerEnter(Collider other)
		{
			if (!IsColliderValid(other, _checkAttachedRigidbody)) return;
			_triggerEnter.Invoke();
			//Debug.Log($"{name} => OnTriggerEnter with {other.gameObject.name}");
		}

		protected virtual void OnTriggerStay(Collider other)
		{
			if (!IsColliderValid(other, _checkAttachedRigidbody)) return;
			_triggerStay.Invoke();
			//Debug.Log($"{name} => OnTriggerStay with {other.gameObject.name}");
		}

		protected virtual void OnTriggerExit(Collider other)
		{
			if (!IsColliderValid(other, _checkAttachedRigidbody)) return;
			_triggerExit.Invoke();
			//Debug.Log($"{name} => OnTriggerExit with {other.gameObject.name}");
		}

		protected abstract bool IsColliderValid(Collider other, bool checkAttachedRigidbody);

	}
}