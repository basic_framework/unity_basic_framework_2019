﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Trigger
{
	[Flags]
	public enum TriggerCompareModeFlags
	{
		// Does not compare the trigger to anything
		NoCompare	= 0,
		Collider 	= 1 << 0,
		Tag  		= 1 << 1,
		Layer  		= 1 << 2,
		Rigidbody 	= 1 << 3,
		//Type		= 1 << 3,		// TODO: Maybe allow compare against a component type, Hard to implement
	}
	
}