﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Trigger.Editor
{
	public abstract class TriggerListenerBaseEditor<T> : BasicBaseEditor<T>
		where T : TriggerListenerBase
	{
	
		// **************************************** VARIABLES ****************************************\\

	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawTriggerBaseFields();
		}

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();
			
			var targetGameObject = TypedTarget.gameObject;

			var collider = targetGameObject.GetComponent<Collider>();
			var rigidbody = targetGameObject.GetComponent<Rigidbody>();

			if (collider != null)
			{
				if (!collider.isTrigger)
				{
					EditorGUILayout.HelpBox("Collider is not trigger. Other collider must be trigger. Is this intended?", MessageType.Info);
				}
			}
			else if(rigidbody == null)
			{
				EditorGUILayout.HelpBox("This component only works on GameObjects with a Rigidbody or Collider components attached", MessageType.Warning);
			}
		}

		protected void DrawTriggerBaseFields()
		{
			SectionHeader("Trigger Listener Base");
			
			var checkAttachedRigidbody = serializedObject.FindProperty("_checkAttachedRigidbody");
			
			EditorGUILayout.PropertyField(checkAttachedRigidbody);
		}
		
		protected override void DrawEventFields()
		{
			SectionHeader("Trigger Listener Base Events");

			var triggerEnter = serializedObject.FindProperty("_triggerEnter");
			var triggerStay = serializedObject.FindProperty("_triggerStay");
			var triggerExit = serializedObject.FindProperty("_triggerExit");

			EditorGUILayout.PropertyField(triggerEnter);
			EditorGUILayout.PropertyField(triggerStay);
			EditorGUILayout.PropertyField(triggerExit);
		}
		
	}
}