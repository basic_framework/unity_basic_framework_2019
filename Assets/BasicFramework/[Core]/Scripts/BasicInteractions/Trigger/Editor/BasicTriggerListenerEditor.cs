﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Trigger.Editor
{
	[CustomEditor(typeof(BasicTriggerListener))]
	public class BasicTriggerListenerEditor : TriggerListenerBaseEditor<BasicTriggerListener>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _collidersReorderableList;
		private ReorderableList _rigidbodiesReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
		
			var compareColliders = serializedObject.FindProperty("_compareColliders");
			var compareRigidbodies = serializedObject.FindProperty("_compareRigidbodies");

			_collidersReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, compareColliders);
			
			_rigidbodiesReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, compareRigidbodies);

			_collidersReorderableList.displayAdd = false;
			_rigidbodiesReorderableList.displayAdd = false;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawSettingFields();
		}
		
		private void DrawSettingFields()
		{
			SectionHeader("Basic Trigger Listener Settings");

			var triggerCompareMode = serializedObject.FindProperty("_triggerCompareMode");
		
			var compareTags = serializedObject.FindProperty("_compareTags");
			var compareLayers = serializedObject.FindProperty("_compareLayers");
			var compareColliders = serializedObject.FindProperty("_compareColliders");
			var compareRigidbodies = serializedObject.FindProperty("_compareRigidbodies");
		

			EditorGUILayout.PropertyField(triggerCompareMode);

			var triggerFlag = triggerCompareMode.intValue;
		
			
			// Collider
			Space();
			GUI.enabled = PrevGuiEnabled && (triggerFlag & (int)TriggerCompareModeFlags.Collider) != 0;
			compareColliders.isExpanded =
				EditorGUILayout.Foldout(compareColliders.isExpanded, GetGuiContent(compareColliders));

			if (compareColliders.isExpanded)
			{
				Space();
				
				BasicEditorGuiLayout.DragAndDropAreaIntoList<Collider>(
					compareColliders, $"Drop {nameof(Collider)}'s Here");
		
				Space();
				BasicEditorArrayUtility.RemoveNullReferences(compareColliders);
				BasicEditorArrayUtility.RemoveDuplicateReferences(compareColliders);
				_collidersReorderableList.DoLayoutList();
			}
			
		
			
			// Tag
			Space();
			GUI.enabled = PrevGuiEnabled && (triggerFlag & (int)TriggerCompareModeFlags.Tag) != 0;
			EditorGUILayout.PropertyField(compareTags);
		
			
			// Layer
			Space();
			GUI.enabled = PrevGuiEnabled && (triggerFlag & (int)TriggerCompareModeFlags.Layer) != 0;
			EditorGUILayout.PropertyField(compareLayers);

			
			
			
			// Rigidbody
			Space();
			GUI.enabled = PrevGuiEnabled && (triggerFlag & (int)TriggerCompareModeFlags.Rigidbody) != 0;
			compareRigidbodies.isExpanded =
				EditorGUILayout.Foldout(compareRigidbodies.isExpanded, GetGuiContent(compareRigidbodies));

			if (compareRigidbodies.isExpanded)
			{
				Space();
				BasicEditorGuiLayout.DragAndDropAreaIntoList<Rigidbody>(
					compareRigidbodies, $"Drop {nameof(Rigidbody)}'s Here");
		
				Space();
				BasicEditorArrayUtility.RemoveNullReferences(compareRigidbodies);
				BasicEditorArrayUtility.RemoveDuplicateReferences(compareRigidbodies);
				_rigidbodiesReorderableList.DoLayoutList();
			}
			
			GUI.enabled = PrevGuiEnabled;
		}

	}
}