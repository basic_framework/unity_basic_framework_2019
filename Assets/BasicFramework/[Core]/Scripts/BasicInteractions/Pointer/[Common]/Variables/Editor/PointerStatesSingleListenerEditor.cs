using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
    [CustomEditor(typeof(PointerStatesSingleListener))][CanEditMultipleObjects]
    public class PointerStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
        PointerStates, 
        PointerStatesSingle, 
        PointerStatesUnityEvent,
        PointerStatesCompareUnityEvent,
        PointerStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}