using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
    [AddComponentMenu("Basic Framework/Core/Interactions/Pointer/Pointer States Single Listener")]
    public class PointerStatesSingleListener : SingleStructListenerBase
    <
        PointerStates, 
        PointerStatesSingle, 
        PointerStatesUnityEvent, 
        PointerStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}