using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
    [CustomEditor(typeof(PointerStatesSingle))][CanEditMultipleObjects]
    public class PointerStatesSingleEditor : SingleStructBaseEditor<PointerStates, PointerStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(PointerStatesSingle))]
    public class PointerStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(PointerStatesSingleReference))]
    public class PointerStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}