using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Pointer
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Interaction/Pointer/Pointer States Single", 
        fileName = "single_pointer_states_")
    ]
    public class PointerStatesSingle : SingleStructBase<PointerStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(PointerStatesSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class PointerStatesSingleReference : SingleStructReferenceBase<PointerStates, PointerStatesSingle>
    {
        public PointerStatesSingleReference()
        {
        }

        public PointerStatesSingleReference(PointerStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class PointerStatesSingleUnityEvent : UnityEvent<PointerStatesSingle>{}
	
    [Serializable]
    public class PointerStatesSingleArrayUnityEvent : UnityEvent<PointerStatesSingle[]>{}
    
}