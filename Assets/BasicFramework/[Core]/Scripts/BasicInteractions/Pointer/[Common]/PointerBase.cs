﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_POINTER_CALCULATION)]
	public abstract class PointerBase : MonoBehaviour
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// TODO: add methods or variables to get the pointer path, hit/end point, hit normal, etc. Basic data needed for visuals
		
		[Tooltip("Should the pointer path be drawn in the editor")]
		[SerializeField] private bool _editorDrawPointerPath = true;
		
		[Tooltip("The layers the pointer should interact with")]
		[SerializeField] private LayerMask _layerMaskInteraction = ~0;
		
		[Tooltip("Raised when the pointer goes from enable to disabled or vice versa")]
		[SerializeField] private BoolPlusUnityEvent _pointerEnabledChanged = default;
		
		[Tooltip("Raised when the poiner state changes")]
		[SerializeField] private PointerStatesUnityEvent _pointerStateChanged = new PointerStatesUnityEvent();
		
		[Tooltip("Raised when the poiner state changes and the condition is met")]
		[SerializeField] private PointerStatesCompareUnityEvent[] _pointerStateChangedCompare = new PointerStatesCompareUnityEvent[0];


		public bool EditorDrawPointerPath
		{
			get => _editorDrawPointerPath;
			set => _editorDrawPointerPath = value;
		}

		public LayerMask LayerMaskInteraction
		{
			get => _layerMaskInteraction;
			set => _layerMaskInteraction = value;
		}

		public BoolPlusUnityEvent PointerEnabledChanged => _pointerEnabledChanged;

		public PointerStatesUnityEvent PointerStateChanged => _pointerStateChanged;


		private bool _pointerEnabled = true;
		public bool PointerEnabled
		{
			get => _pointerEnabled;
			private set
			{
				if (!enabled || !_allowPointerEnabled) value = false;
				if (_pointerEnabled == value) return;
				_pointerEnabled = value;
				PointerEnabledUpdated(value);
			}
		}
		
		private bool _allowPointerEnabled = true;
		protected bool AllowPointerEnabled
		{
			get => _allowPointerEnabled;
			set
			{
				if (_allowPointerEnabled == value) return;
				_allowPointerEnabled = value;
				PointerEnabled = _allowPointerEnabled;
			}
		}
		
		private PointerStates _pointerState = PointerStates.Disabled;
		public PointerStates PointerState
		{
			get => _pointerState;
			private set
			{
				if (_pointerState == value) return;
				_pointerState = value;
				PointerStateUpdated(value);
			}
		}

		private readonly BasicPointerEventData _basicPointerEventData = new BasicPointerEventData();
		public BasicPointerEventData BasicPointerEventData => _basicPointerEventData;


		// ****************************************  METHODS  ****************************************\\
	
		//***** abstract ******\\
		
		protected abstract BasicPose GetPointerPose();
		protected abstract void UpdatePointerEventData(BasicPointerEventData basicPointerEventData);
		protected abstract void DrawPointerPathInEditor();
		
		
		//***** mono *****\\

		protected virtual void Awake()
		{
			_basicPointerEventData.ClearData();
		}

		protected virtual void OnEnable()
		{
			PointerEnabled = true;
		}

		protected virtual void OnDisable()
		{
			PointerEnabled = false;
		}
		
		protected virtual void Start()
		{
			PointerEnabledUpdated(_pointerEnabled);
			PointerStateUpdated(_pointerState);
		}
		
		protected virtual void Update()
		{
			if (!_pointerEnabled) return;

			_basicPointerEventData.ResetData();
			
			UpdatePointerEventData(_basicPointerEventData);

			PointerState = _basicPointerEventData.HitSomething ? PointerStates.HitSomething : PointerStates.Searching;

#if UNITY_EDITOR
			if (_editorDrawPointerPath) DrawPointerPathInEditor();
#endif
		}


		//***** property updated *****\\
		
		protected virtual void PointerEnabledUpdated(bool value)
		{
			if (!value)
			{
				PointerState = PointerStates.Disabled;
			}

			_pointerEnabledChanged.Raise(value);
		}
		
		protected virtual void PointerStateUpdated(PointerStates value)
		{
			switch (value)
			{
				case PointerStates.Disabled:
					_basicPointerEventData.ClearData();
					break;
				
				case PointerStates.Searching:
					break;
				
				case PointerStates.HitSomething:
					break;
				
				default:
					throw new ArgumentOutOfRangeException(nameof(value), value, null);
			}
			
			_pointerStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _pointerStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
	
	}
}