﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
    [Serializable]
    public class BasicPointerPoseSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [Tooltip("The mode to use for this pointer")]
        [SerializeField] private BasicPointerPoseSourceTypes _sourceType = default;
		
        [Tooltip("The transform to use as the pointer")]
        [SerializeField] private Transform _transform = default;
		
        [Tooltip("The camera creating the view that the cursor position is relative to")]
        [SerializeField] private Camera _cameraCursor = default;

        [Tooltip("The basic pose used as the pointer source")]
        [SerializeField] private BasicPoseSingle _basicPose = default;


        public BasicPointerPoseSourceTypes SourceType
        {
            get => _sourceType;
            set => _sourceType = value;
        }

        public Transform Transform
        {
            get => _transform;
            set => _transform = value;
        }

        public Camera CameraCursor
        {
            get => _cameraCursor;
            set => _cameraCursor = value;
        }

        public BasicPoseSingle BasicPose
        {
            get => _basicPose;
            set => _basicPose = value;
        }


        // ****************************************  METHODS  ****************************************\\
	
        public BasicPose GetPointerPose()
        {
            switch (_sourceType)
            {
                case BasicPointerPoseSourceTypes.Transform:
                    return new BasicPose(_transform);

                case BasicPointerPoseSourceTypes.Cursor:
                    var rayCursor = _cameraCursor.ScreenPointToRay(Input.mousePosition);
                    return new BasicPose(rayCursor, _cameraCursor.transform.up);

                case BasicPointerPoseSourceTypes.BasicPose:
                    return _basicPose.Value;
				
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}