﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
    [CustomPropertyDrawer(typeof(BasicPointerPoseSource))]
    public class BasicPointerPoseSourcePropertyDrawer : BasicBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        private const float SOURCE_WIDTH = 80f;
		
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position = EditorGUI.PrefixLabel(position, label);
            TempRect = position;
            TempRect.width = SOURCE_WIDTH;
			
            var sourceType = property.FindPropertyRelative("_sourceType");

            EditorGUI.PropertyField(TempRect, sourceType, GUIContent.none);
            TempRect.x += TempRect.width + BUFFER_WIDTH;
            TempRect.width = position.width - TempRect.width - BUFFER_WIDTH;
			
            var sourceTypeEnum = 
                EnumUtility.EnumNameToEnumValue<BasicPointerPoseSourceTypes>(sourceType.enumNames[sourceType.enumValueIndex]);

            SerializedProperty sourceProperty;
			
            switch (sourceTypeEnum)
            {
                case BasicPointerPoseSourceTypes.Transform:
                    sourceProperty = property.FindPropertyRelative("_transform");
                    break;
				
                case BasicPointerPoseSourceTypes.Cursor:
                    sourceProperty = property.FindPropertyRelative("_cameraCursor");
                    break;

                case BasicPointerPoseSourceTypes.BasicPose:
                    sourceProperty = property.FindPropertyRelative("_basicPose");
                    break;
				
                default:
                    throw new ArgumentOutOfRangeException();
            }
			
            BasicEditorGui.PropertyFieldNotNull(TempRect, sourceProperty, GUIContent.none);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return SingleLineHeight;
        }
		
    }
}