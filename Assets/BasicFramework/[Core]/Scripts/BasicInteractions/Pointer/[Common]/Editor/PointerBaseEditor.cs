﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public abstract class PointerBaseEditor<TPointerBase> : BasicBaseEditor<TPointerBase>
		where TPointerBase : PointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded 
		{
			get => serializedObject.FindProperty("_editorDrawPointerPath").isExpanded;
			set => serializedObject.FindProperty("_editorDrawPointerPath").isExpanded = value;
		}

		private ReorderableList _reorderableListPointerStateCompare;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var pointerStateChangedCompare = serializedObject.FindProperty("_pointerStateChangedCompare");

			_reorderableListPointerStateCompare =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, pointerStateChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var editorDrawPointerPath = serializedObject.FindProperty("_editorDrawPointerPath");
			var pointerEventData = TypedTarget.BasicPointerEventData;
			
			EditorGUILayout.PropertyField(editorDrawPointerPath);

			EditorGUILayout.LabelField(nameof(TypedTarget.PointerEnabled).CamelCaseToHumanReadable(), 
				TypedTarget.PointerEnabled.ToString());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.PointerState).CamelCaseToHumanReadable(), 
				TypedTarget.PointerState.ToString());
			
			EditorGUILayout.LabelField(nameof(pointerEventData.HitGameObject).CamelCaseToHumanReadable(), 
				pointerEventData.HitSomething ? pointerEventData.HitGameObject.name : "NULL");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var layerMaskInteraction = serializedObject.FindProperty("_layerMaskInteraction");
			
			Space();
			SectionHeader($"{nameof(PointerBase).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(layerMaskInteraction);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var pointerEnabledChanged = serializedObject.FindProperty("_pointerEnabledChanged");
			var pointerStateChanged = serializedObject.FindProperty("_pointerStateChanged");
			
			Space();
			SectionHeader($"{nameof(PointerBase).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(pointerEnabledChanged);
			EditorGUILayout.PropertyField(pointerStateChanged);
			_reorderableListPointerStateCompare.DoLayoutList();
		}
		
	}
}