﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public class BasicPointerEventData : ICloneable
	{
	
		// **************************************** VARIABLES ****************************************\\

		private BasicPose _posePointer;
		public BasicPose PosePointer
		{
			get => _posePointer;
			set => _posePointer = value;
		}

		private bool _hitSomething;
		public bool HitSomething => _hitSomething;
		
		private GameObject _hitGameObject;
		public GameObject HitGameObject
		{
			get => _hitGameObject;
			set
			{
				_hitGameObject = value;
				_hitSomething = _hitGameObject != null;
			}
		}

		private Vector3 _hitPoint;
		public Vector3 HitPoint
		{
			get => _hitPoint;
			set => _hitPoint = value;
		}
		
		private Vector3 _hitNormal;
		public Vector3 HitNormal
		{
			get => _hitNormal;
			set => _hitNormal = value;
		}
		
		private readonly List<Vector3> _pathPoints = new List<Vector3>(2);
		public List<Vector3> PathPoints => _pathPoints;
		
		
		// The forward direction desired relative to the forward of the pointer pose and the hit normal
		private Vector2 _faceDirection = Vector2.up;
		public Vector2 FaceDirection
		{
			get => _faceDirection;
			set
			{
				if(Math.Abs(value.sqrMagnitude) < Mathf.Epsilon) value = Vector2.up; 
				_faceDirection = value.normalized;
			}
		}
		
		// Should the travellers up direction be to the specified up direction
		private bool _alignTargetToDirectionUp = true;
		public bool AlignTargetToDirectionUp
		{
			get => _alignTargetToDirectionUp;
			set => _alignTargetToDirectionUp = value;
		}
		
		private Vector3 _directionUp = Vector3.up;
		public Vector3 DirectionUp
		{
			get => _directionUp;
			set
			{
				if(Math.Abs(value.sqrMagnitude) < Mathf.Epsilon) _directionUp = Vector3.up;
				_directionUp = value.normalized;
			}
		}

		// ****************************************  METHODS  ****************************************\\

		//***** implementations *****\\
		
		public virtual object Clone()
		{
			var clone = new BasicPointerEventData();
			
			clone._posePointer = _posePointer;

			clone._hitSomething = _hitSomething;
			clone._hitGameObject = _hitGameObject;

			clone._hitPoint = _hitPoint;
			clone._hitNormal = _hitNormal;
			
			clone._pathPoints.Resize(_pathPoints.Count);
			
			for (int i = 0; i < clone._pathPoints.Count; i++)
			{
				clone._pathPoints[i] = _pathPoints[i];
			}

			clone._faceDirection = _faceDirection;
			clone._alignTargetToDirectionUp = _alignTargetToDirectionUp;
			clone._directionUp = _directionUp;

			return clone;
		}
		
		
		//***** functions *****\\
		
		public virtual void ResetData()
		{
			_hitSomething = default;
			_hitGameObject = default;
		}

		public virtual void ClearData()
		{
			_posePointer = default;

			_hitSomething = default;
			_hitGameObject = default;

			_hitPoint = default;
			_hitNormal = default;
			
			_pathPoints.Resize(2);
			_pathPoints[0] = Vector3.zero;
			_pathPoints[1] = Vector3.zero;
			
			_faceDirection = Vector2.up;
			_alignTargetToDirectionUp = false;
			_directionUp = Vector3.up;
		}
		
		public void SetPathPoints(Vector3 start, Vector3 end)
		{
			_pathPoints.Resize(2);
			
			_pathPoints[0] = start;
			_pathPoints[1] = end;
		}
		
		public void SetPathPoints(Vector3[] pathPoints)
		{
			SetPathPoints(pathPoints, pathPoints.Length);
		}
		
		public void SetPathPoints(Vector3[] pathPoints, int pathPointsCount)
		{
			_pathPoints.Resize(pathPointsCount);

			for (int i = 0; i < pathPointsCount; i++)
			{
				_pathPoints[i] = pathPoints[i];
			}
		}
		
		public BasicPose GetTargetPose()
		{
			var pointerForward = _posePointer.Rotation * Vector3.forward;

			var targetUp = _alignTargetToDirectionUp ? _directionUp : _hitNormal;
			
			var targetForward = MathUtility.RemoveAxisFromVector(pointerForward, targetUp);

			var faceAngle = Vector2.SignedAngle(Vector2.up, _faceDirection) * -1f;
			
			targetForward = MathUtility.RotateDirection(targetForward, faceAngle, targetUp);

			return new BasicPose
			{
				Position = _hitPoint, 
				Rotation = MathUtility.Direction2Quaternion(targetForward, targetUp)
			};
		}
		
		
		//***** static *****\\
		
		public static void Copy(BasicPointerEventData from, BasicPointerEventData to)
		{
			to.PosePointer = from.PosePointer;
			
			to.HitGameObject = from.HitGameObject; 
			
			to.HitPoint= from.HitPoint;
			to.HitNormal = from.HitNormal;
			
			to._pathPoints.Resize(from._pathPoints.Count);
			
			for (int i = 0; i < to._pathPoints.Count; i++)
			{
				to._pathPoints[i] = from._pathPoints[i];
			}

			to._faceDirection = from._faceDirection;
			to._alignTargetToDirectionUp = from._alignTargetToDirectionUp;
			to._directionUp = from._directionUp;
		}
		
	}
}