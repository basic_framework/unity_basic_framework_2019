﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Pointer
{
    public enum PointerStates
    {
        Disabled,
        Searching,
        HitSomething
    }
	
    [Serializable]
    public class PointerStatesUnityEvent : UnityEvent<PointerStates>{}

    [Serializable]
    public class PointerStatesCompareUnityEvent : CompareValueUnityEventBase<PointerStates>{}
	
	
    public enum BasicPointerPoseSourceTypes
    {
        Transform,
        Cursor,
        BasicPose
    }
	
}