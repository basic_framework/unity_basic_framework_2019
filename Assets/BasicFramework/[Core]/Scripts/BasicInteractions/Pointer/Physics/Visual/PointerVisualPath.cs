﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Pointers/Physics/Physics Pointer Visual Path")]
	public class PointerVisualPath : PointerVisualBase<PointerBase> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The line renderer that will represent the pointer path")]
		[SerializeField] private LineRenderer _lineRenderer = default;

		[Tooltip("Use the physics path of the pointer rather than the visual path")]
		[SerializeField] private bool _usePhysicsPath = default;


		public LineRenderer LineRenderer => _lineRenderer;

		public bool UsePhysicsPath
		{
			get => _usePhysicsPath;
			set => _usePhysicsPath = value;
		}
		
		private Vector3[] _arrayPathPoints = new Vector3[0];


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		protected override void Reset()
		{
			base.Reset();
			
			_lineRenderer = GetComponentInChildren<LineRenderer>();
		}
		
		
		//***** overrides *****\\

		protected override void UpdatePointerVisual(BasicPointerEventData basicPointerEventData)
		{
			base.UpdatePointerVisual(basicPointerEventData);

			var pathPoints = basicPointerEventData.PathPoints;
			var pathCount = pathPoints.Count;
			
			if (_arrayPathPoints.Length < pathCount)
			{
				_arrayPathPoints = _arrayPathPoints.Resize((uint) pathCount);
			}

			for (int i = 0; i < pathCount; i++)
			{
				_arrayPathPoints[i] = pathPoints[i];
			}
			
			_lineRenderer.positionCount = pathCount;
			_lineRenderer.SetPositions(_arrayPathPoints);
		}

	}
}