﻿using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_POINTER_VISUALS)]
	public abstract class PointerVisualBase<TPointer> : MonoBehaviour
		where TPointer : PointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The pointer this visual is representing")]
		[SerializeField] private TPointer _pointer = default;

		[Tooltip("Raised when the pointer state changes")]
		[SerializeField] private PointerStatesUnityEvent _pointerStateChanged = default;
		
		[Tooltip("Raised when the pointer state changes and the condition is met")]
		[SerializeField] private PointerStatesCompareUnityEvent[] _pointerStateChangedCompare = default;

		
		public TPointer Pointer => _pointer;

		public PointerStatesUnityEvent PointerStateChanged => _pointerStateChanged;

		public PointerStatesCompareUnityEvent[] PointerStateChangedCompare => _pointerStateChangedCompare;


		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		protected virtual void Reset()
		{
			_pointer = GetComponentInParent<TPointer>();
		}

		protected virtual void OnEnable()
		{
			_pointer.PointerStateChanged.AddListener(OnPointerStateChanged);
		}

		protected virtual void OnDisable()
		{
			_pointer.PointerStateChanged.RemoveListener(OnPointerStateChanged);
		}

		protected virtual void Update()
		{
			UpdatePointerVisual(_pointer.BasicPointerEventData);
		}
		
		
		//***** virtual *****\\
		
		protected virtual void UpdatePointerVisual(BasicPointerEventData basicPointerEventData){}

		protected virtual void OnPointerStateChanged(PointerStates pointerState)
		{
			_pointerStateChanged.Invoke(pointerState);

			foreach (var compareUnityEvent in _pointerStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(pointerState);
			}
		}
		
	}
}