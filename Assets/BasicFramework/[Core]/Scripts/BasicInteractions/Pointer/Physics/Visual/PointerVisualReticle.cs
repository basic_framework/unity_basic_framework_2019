﻿using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Pointers/Physics/Physics Pointer Visual Reticle")]
	public class PointerVisualReticle : PointerVisualBase<PointerBase> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The transform to position at the gui hit position, the up direction will be aligned with the gui normal")]
		[SerializeField] private Transform _transformReticle = default;


		// ****************************************  METHODS  ****************************************\\
		
		
		//***** mono *****\\
		
		protected override void Reset()
		{
			base.Reset();
			
			_transformReticle = transform;
		}
		

		//***** overrides *****\\

		protected override void UpdatePointerVisual(BasicPointerEventData basicPointerEventData)
		{
			base.UpdatePointerVisual(basicPointerEventData);

			var poseTarget = basicPointerEventData.GetTargetPose();

			_transformReticle.position = poseTarget.Position;
			_transformReticle.rotation = poseTarget.Rotation;
			
//			_transformReticle.position = basicPointerEventData.HitPoint;
//			_transformReticle.rotation = Quaternion.FromToRotation(_transformReticle.up, basicPointerEventData.HitNormal) * _transformReticle.rotation;
		}
		
	}
}