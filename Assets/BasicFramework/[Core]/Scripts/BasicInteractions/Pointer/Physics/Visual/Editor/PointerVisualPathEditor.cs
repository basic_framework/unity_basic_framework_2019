﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	[CustomEditor(typeof(PointerVisualPath))]
	public class PointerVisualPathEditor : PointerVisualBaseEditor<PointerBase, PointerVisualPath>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var lineRenderer = serializedObject.FindProperty("_lineRenderer");
			var usePhysicsPath = serializedObject.FindProperty("_usePhysicsPath");
			
			Space();
			SectionHeader($"{nameof(PointerVisualPath).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(lineRenderer);
			EditorGUILayout.PropertyField(usePhysicsPath);
		}
		
	}
}