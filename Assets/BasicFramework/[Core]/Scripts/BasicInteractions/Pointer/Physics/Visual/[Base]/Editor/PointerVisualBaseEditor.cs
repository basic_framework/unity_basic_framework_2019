﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	public abstract class PointerVisualBaseEditor<TPointer, TVisualBase> : BasicBaseEditor<TVisualBase>
		where TPointer : PointerBase
		where TVisualBase : PointerVisualBase<TPointer>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		private ReorderableList _reorderableListPointerStateCompare;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var pointerStateChangedCompare = serializedObject.FindProperty("_pointerStateChangedCompare");

			_reorderableListPointerStateCompare =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, pointerStateChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var pointer = TypedTarget.Pointer;
			var pointerNotNull = pointer != null;
			
			EditorGUILayout.LabelField(nameof(pointer.PointerState).CamelCaseToHumanReadable(), 
				pointerNotNull ? pointer.PointerState.ToString() : "NULL");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var pointer = serializedObject.FindProperty("_pointer");
			
			Space();
			SectionHeader($"{nameof(PointerVisualBase<TPointer>).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(pointer);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var pointerStateChanged = serializedObject.FindProperty("_pointerStateChanged");

			Space();
			SectionHeader($"{nameof(PointerVisualBase<TPointer>).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(pointerStateChanged);
			_reorderableListPointerStateCompare.DoLayoutList();
		}
		
	}
}