﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
    [CustomEditor(typeof(PointerVisualReticle))]
    public class PointerVisualReticleEditor : PointerVisualBaseEditor<PointerBase, PointerVisualReticle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
			
            var transformReticle = serializedObject.FindProperty("_transformReticle");

            Space();
            SectionHeader($"{nameof(PointerVisualReticle).CamelCaseToHumanReadable()} Properties");
			
            BasicEditorGuiLayout.PropertyFieldNotNull(transformReticle);
        }
		
    }
}