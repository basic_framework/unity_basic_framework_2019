﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	[CustomEditor(typeof(PhysicsPointerLineBasic))]
	public class PhysicsPointerLineBasicEditor : PhysicsPointerLineBaseEditor<PhysicsPointerLineBasic>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var pointerPoseSource = serializedObject.FindProperty("_pointerPoseSource");

			Space();
			SectionHeader($"{nameof(PhysicsPointerLineBasic).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(pointerPoseSource);
		}
		
	}
}