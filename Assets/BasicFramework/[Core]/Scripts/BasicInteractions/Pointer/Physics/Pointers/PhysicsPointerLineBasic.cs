﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Pointer/Physics/Physics Pointer Line Basic")]
	public class PhysicsPointerLineBasic : PhysicsPointerLineBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The source of the pointer pose")]
		[SerializeField] private BasicPointerPoseSource _pointerPoseSource = new BasicPointerPoseSource();


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		private void Reset()
		{
			_pointerPoseSource.Transform = transform;
		}

		
		//***** overrides *****\\
		
		protected override BasicPose GetPointerPose()
		{
			return _pointerPoseSource.GetPointerPose();
		}


		//***** functions *****\\

		// Set the transform to use for the pointer and switches to the appropriate mode
		public void SetTransformPointerPose(Transform transformPointerPose)
		{
			_pointerPoseSource.Transform = transformPointerPose;
			_pointerPoseSource.SourceType = BasicPointerPoseSourceTypes.Transform;
		}

		// Set the camera to use for the cursor pointer and switches to the appropriate mode
		public void SetCameraCursor(Camera cameraCursor)
		{
			_pointerPoseSource.CameraCursor = cameraCursor;
			_pointerPoseSource.SourceType = BasicPointerPoseSourceTypes.Cursor;
		}

		
	}
}