﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	[CustomEditor(typeof(PhysicsPointerArcBasic))]
	public class PhysicsPointerArcBasicEditor : PhysicsPointerArcBaseEditor<PhysicsPointerArcBasic>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var pointerPoseSource = serializedObject.FindProperty("_pointerPoseSource");
			
			Space();
			SectionHeader($"{nameof(PhysicsPointerArcBasic).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(pointerPoseSource);
		}

	}
}