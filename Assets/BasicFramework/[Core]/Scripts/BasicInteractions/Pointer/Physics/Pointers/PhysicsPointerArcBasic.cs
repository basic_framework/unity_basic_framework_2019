﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Interactions/Pointer/Physics/Physics Pointer Arc Basic")]
	public class PhysicsPointerArcBasic : PhysicsPointerArcBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private BasicPointerPoseSource _pointerPoseSource = default;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		private void Reset()
		{
			_pointerPoseSource.Transform = transform;
		}

		
		//***** overrides *****\\
		
		protected override BasicPose GetPointerPose()
		{
			return _pointerPoseSource.GetPointerPose();
		}

	}
}