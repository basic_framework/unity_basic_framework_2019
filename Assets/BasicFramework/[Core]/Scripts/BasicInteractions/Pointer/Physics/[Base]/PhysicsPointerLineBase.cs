﻿using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public abstract class PhysicsPointerLineBase : PhysicsPointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The maximum distance of the pointer")]
		[SerializeField] private float _maxDistance = 5f;
		
		public float MaxDistance
		{
			get => _maxDistance;
			set => _maxDistance = value;
		}
		
		
		private RaycastHit _raycastHit;
		public sealed override RaycastHit RaycastHit => _raycastHit;

		private readonly Vector3[] _pathPointsPhysics = new Vector3[2];
		public sealed override Vector3[] PathPointsPhysics => _pathPointsPhysics;

		private readonly int _pathPointsPhysicsCount = 2;
		public sealed override int PathPointsPhysicsCount => _pathPointsPhysicsCount;

		
		// ****************************************  METHODS  ****************************************\\
		

		//***** overrides *****\\

		protected sealed override void UpdatePointerEventData(BasicPointerEventData basicPointerEventData)
		{
			var posePointer = GetPointerPose();
			
			basicPointerEventData.PosePointer = posePointer;
			
			var start = posePointer.Position;
			var end = start + posePointer.Rotation * Vector3.forward * MaxDistance;
			
			if (Physics.Linecast(start, end, out var hit, LayerMaskInteraction, QueryTriggerInteraction))
			{
				_raycastHit = hit;
				basicPointerEventData.HitPoint = hit.point;
				basicPointerEventData.HitNormal = hit.normal;
				basicPointerEventData.HitGameObject = hit.transform.gameObject;
				
				end = hit.point;
			}
			
			basicPointerEventData.SetPathPoints(start, end);

			_pathPointsPhysics[0] = start;
			_pathPointsPhysics[1] = end;
		}

	}
}