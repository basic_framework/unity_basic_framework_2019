﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public abstract class PhysicsPointerArcBase : PhysicsPointerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private const float GRAVITY_MAGNITUDE = 9.81f;
		public const float MINIMUM_TIME_STEP = 0.01f;

		[Tooltip("The direction of gravity")]
		[SerializeField] private Vector3 _gravityDirection = Vector3.down;
		
		[Tooltip("The time between trajectory points. Lower value means more rays need to be used")]
		[SerializeField] private float _physicsTimeStep = 0.3f;
		
		[Tooltip("The time step used for the visual path calculation.\n" +
		         "Note: It may be less so that the path end at the required flight time.")]
		[SerializeField] private float _maxVisualTimeStep = 0.1f;
		
		[Tooltip("The distance when the arc has zero vertical displacement from the origin")]
		[SerializeField] private float _maxDistance = 20f;
		
		
		// The minimum number of rays required to fire straight up and return to the same point
		[HideInInspector]
		[SerializeField] private int _minPhysicsRays;
		
		[HideInInspector]
		[SerializeField] private int _maxPhysicsRays;
		
		[Tooltip("The number of extra rays to cast. Add more for greater distance")]
		[SerializeField] private int _extraPhysicsRays = 1;
		
		// TODO: make variables accessible, update data as necessary
		public Vector3 GravityDirection
		{
			get => _gravityDirection;
			set
			{
				value = value.normalized;
				if (_gravityDirection == value) return;
				_gravityDirection = value;
				UpdateTrajectoryData();
			}
		}

		public float PhysicsTimeStep
		{
			get => _physicsTimeStep;
			set
			{
				value = Mathf.Max(MINIMUM_TIME_STEP, value);
				if (Math.Abs(_physicsTimeStep - value) < Mathf.Epsilon) return;
				_physicsTimeStep = value;
				UpdateTrajectoryData();
			}
		}

		public float MaxVisualTimeStep
		{
			get => _maxVisualTimeStep;
			set => _maxVisualTimeStep = Mathf.Max(MINIMUM_TIME_STEP, value);
		}

		public float MaxDistance
		{
			get => _maxDistance;
			set
			{
				if (Math.Abs(_maxDistance - value) < Mathf.Epsilon) return;
				_maxDistance = value;
				UpdateTrajectoryData();
			}
		}

		public int ExtraRays
		{
			get => _extraPhysicsRays;
			set
			{
				_extraPhysicsRays = Mathf.Max(0, value);
				_maxPhysicsRays = Mathf.Max(0, _minPhysicsRays + _extraPhysicsRays);
			}
		}

		
		private readonly TrajectoryData _trajectoryData = new TrajectoryData(Vector3.down, GRAVITY_MAGNITUDE);
		public TrajectoryData TrajectoryData => _trajectoryData;

		
		private Vector3[] _physicsPathPoints = new Vector3[25];
		private Vector3[] _visualPathPoints = new Vector3[50];
		
		private readonly List<Vector3> _workingList = new List<Vector3>(25);
		
		
		private RaycastHit _raycastHit;
		public sealed override RaycastHit RaycastHit => _raycastHit;

		public sealed override Vector3[] PathPointsPhysics => _physicsPathPoints;

		private int _pathPointsPhysicsCount = 2;
		public sealed override int PathPointsPhysicsCount => _pathPointsPhysicsCount;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		private void OnValidate()
		{
			UpdateTrajectoryData();
		}

		protected override void Awake()
		{
			UpdateTrajectoryData();
		}
		
		
		//***** overrides ******\\

		protected override void UpdatePointerEventData(BasicPointerEventData basicPointerEventData)
		{
			var posePointer = GetPointerPose();

			basicPointerEventData.PosePointer = posePointer;
			
			_trajectoryData.SetLaunchParameters(posePointer.Position,posePointer.Rotation * Vector3.forward);
			
			_workingList.Clear();

			var launchHorizontalSpeed = _trajectoryData.LaunchHorizontalSpeed;
			var launchVerticalSpeed = _trajectoryData.LaunchVerticalSpeed;
			
			var launchHorizontalDirection = _trajectoryData.LaunchHorizontalDirection;
			var launchVerticalDirection = _trajectoryData.LaunchVerticalDirection;

			var launchPosition = _trajectoryData.Origin;
			var startPosition = launchPosition;
			Vector3 endPosition;

			var flightTime = 0f;
			
			_workingList.Add(launchPosition);

			for (int i = 1; i <= _maxPhysicsRays; i++)
			{
				//***** Find the next point along the arc *****\\
				
				flightTime = _physicsTimeStep * i;
				var horizontalDisplacement = launchHorizontalSpeed * flightTime;
				var verticalDisplacement = launchVerticalSpeed * flightTime - 0.5f * GRAVITY_MAGNITUDE * flightTime * flightTime;
				var displacement = horizontalDisplacement *  launchHorizontalDirection + verticalDisplacement * launchVerticalDirection;
				endPosition = launchPosition + displacement;

				
				//***** Line Cast *****\\
				
				if (Physics.Linecast(startPosition, endPosition, out var hit, LayerMaskInteraction, QueryTriggerInteraction))
				{
					// Work out time of flight
					var launchToHit = hit.point - launchPosition;

					var deltaHorizontal =
						MathUtility.MagOfVectorInAxis(launchToHit, _trajectoryData.LaunchHorizontalDirection);

					if (Math.Abs(deltaHorizontal) > Mathf.Epsilon)
					{
						flightTime = ArcTrajectory.FlightTime(_trajectoryData, deltaHorizontal, false);
					}
					else
					{
						var deltaVertical = MathUtility.MagOfVectorInAxis(launchToHit, _trajectoryData.LaunchVerticalDirection);
						flightTime = ArcTrajectory.FlightTime(_trajectoryData, deltaVertical, true);
					}
					
					_raycastHit = hit;
					basicPointerEventData.HitPoint = hit.point;
					basicPointerEventData.HitNormal = hit.normal;
					basicPointerEventData.HitGameObject = hit.transform.gameObject;
					
					_workingList.Add(hit.point);
					break;
				}
				
				_workingList.Add(endPosition);
				startPosition = endPosition;
			}
			
			
			// Store the Physics Path Points

			var physicsPathPointsCount = _workingList.Count;
			var physicsPathPointsCountLessOne = physicsPathPointsCount - 1;
			
			if (_physicsPathPoints.Length < physicsPathPointsCount)
			{
				_physicsPathPoints = new Vector3[physicsPathPointsCount];
			}

			for (int i = 0; i < _physicsPathPoints.Length; i++)
			{
				_physicsPathPoints[i] = _workingList[Mathf.Min(i, physicsPathPointsCountLessOne)];
			}

			_pathPointsPhysicsCount = physicsPathPointsCount;
			

			// Store the Visual Path Points
			
			var visualPathLineCount = Mathf.CeilToInt(flightTime / _maxVisualTimeStep);
			var visualTimeStep = flightTime / visualPathLineCount;

			var visualPathPointsCount = visualPathLineCount + 1;
			var visualPathPointsCountLessOne = visualPathLineCount - 1;
			
			if (_visualPathPoints.Length < visualPathPointsCount)
			{
				_visualPathPoints = new Vector3[visualPathPointsCount];
			}

			for (int i = 0; i < _visualPathPoints.Length; i++)
			{
				if (i >= visualPathPointsCount)
				{
					_visualPathPoints[i] = _visualPathPoints[visualPathPointsCountLessOne];
					continue;
				}
				
				flightTime = visualTimeStep * i;
				var horizontalDisplacement = launchHorizontalSpeed * flightTime;
				var verticalDisplacement = launchVerticalSpeed * flightTime - 0.5f * GRAVITY_MAGNITUDE * flightTime * flightTime;
				var displacement = horizontalDisplacement *  launchHorizontalDirection + verticalDisplacement * launchVerticalDirection;
				_visualPathPoints[i] = launchPosition + displacement;
			}
			
			BasicPointerEventData.SetPathPoints(_visualPathPoints, visualPathPointsCount);
		}


		//***** functions ******\\
		
		private void UpdateTrajectoryData()
		{
			_physicsTimeStep = Mathf.Max(MINIMUM_TIME_STEP, _physicsTimeStep);
			_maxVisualTimeStep = Mathf.Max(MINIMUM_TIME_STEP, _maxVisualTimeStep);
			
			// The velocity required to reach max distance when angle is 45 degree
			var launchSpeed = Mathf.Sqrt(_maxDistance * GRAVITY_MAGNITUDE);
			
			// The time of flight if the ray is fired directly against gravity
			var flightTime = ArcTrajectory.FlightTime(
				_gravityDirection * -1, launchSpeed,
				_gravityDirection, GRAVITY_MAGNITUDE, 
				0, true);
			
			_minPhysicsRays 	= Mathf.CeilToInt(flightTime / _physicsTimeStep);
			_extraPhysicsRays 	= Mathf.Max(0, _extraPhysicsRays);
			_maxPhysicsRays 	= Mathf.Max(0, _minPhysicsRays + _extraPhysicsRays);
			
			_trajectoryData.LaunchSpeed = launchSpeed;
			_trajectoryData.GravityDirection = _gravityDirection;
			_trajectoryData.GravityMagnitude = GRAVITY_MAGNITUDE;
		}

	}
}