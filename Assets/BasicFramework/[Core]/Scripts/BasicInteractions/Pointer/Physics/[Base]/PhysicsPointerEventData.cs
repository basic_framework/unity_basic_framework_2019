﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Interactions.Pointer
{
	public class PhysicsPointerEventData : BasicPointerEventData 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private RaycastHit _raycastHit;
		public RaycastHit RaycastHit
		{
			get => _raycastHit;
			set => _raycastHit = value;
		}

		// The path points used in the physics calculation, this may be different to base.PathPoints
		private readonly List<Vector3> _pathPointsPhysics = new List<Vector3>(2);
		public List<Vector3> PathPointsPhysics => _pathPointsPhysics;
		

		// ****************************************  METHODS  ****************************************\\

		//***** implementations *****\\
		
		public override object Clone()
		{
			var clone = base.Clone() as PhysicsPointerEventData;
			
			clone._raycastHit = _raycastHit;
			
			clone._pathPointsPhysics.Resize(_pathPointsPhysics.Count);
			
			for (int i = 0; i < clone._pathPointsPhysics.Count; i++)
			{
				clone._pathPointsPhysics[i] = _pathPointsPhysics[i];
			}

			return clone;
		}
		
		
		//***** functions *****\\
		
		public override void ResetData()
		{
			base.ResetData();
			
			_raycastHit = default;
		}
		
		public void SetPathPointsPhysics(Vector3 start, Vector3 end)
		{
			_pathPointsPhysics.Resize(2);
			
			_pathPointsPhysics[0] = start;
			_pathPointsPhysics[1] = end;
		}
		
		public void SetPathPointsPhysics(Vector3[] pathPoints, int pathPointsCount)
		{
			_pathPointsPhysics.Resize(pathPointsCount);

			for (int i = 0; i < pathPointsCount; i++)
			{
				_pathPointsPhysics[i] = pathPoints[i];
			}
		}
		
		
		//***** static *****\\
		
		public static void Copy(PhysicsPointerEventData from, PhysicsPointerEventData to)
		{
			BasicPointerEventData.Copy(from, to);
			
			to._raycastHit = from._raycastHit;
			
			to._pathPointsPhysics.Resize(from._pathPointsPhysics.Count);
			
			for (int i = 0; i < to._pathPointsPhysics.Count; i++)
			{
				to._pathPointsPhysics[i] = from._pathPointsPhysics[i];
			}
		}
		
	}
	
	[Serializable]
	public class PhysicsPointerEventDataUnityEvent : UnityEvent<PhysicsPointerEventData>{}

	[Serializable]
	public class PhysicsPointerEventDataArrayUnityEvent : UnityEvent<PhysicsPointerEventData[]>{}
}