using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
    [CustomEditor(typeof(PhysicsBasicPointerEventDataEventListener))][CanEditMultipleObjects]
    public class PhysicsPointerEventDataEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        PhysicsPointerEventData, 
        PhysicsPointerEventDataEvent, 
        PhysicsPointerEventDataUnityEvent,
        PhysicsBasicPointerEventDataEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}