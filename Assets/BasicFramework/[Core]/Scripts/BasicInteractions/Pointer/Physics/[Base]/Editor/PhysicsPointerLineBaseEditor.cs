﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	public abstract class PhysicsPointerLineBaseEditor<TLinePointer> : PhysicsPointerBaseEditor<TLinePointer>
		where TLinePointer : PhysicsPointerLineBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var maxDistance = serializedObject.FindProperty("_maxDistance");
			
			Space();
			SectionHeader($"{nameof(PhysicsPointerLineBase).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(maxDistance);
		}
		
	}
}