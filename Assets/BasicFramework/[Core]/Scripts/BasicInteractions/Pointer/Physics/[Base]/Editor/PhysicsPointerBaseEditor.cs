﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public abstract class PhysicsPointerBaseEditor<TPointerBase> : PointerBaseEditor<TPointerBase>
		where TPointerBase : PhysicsPointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var queryTriggerInteraction = serializedObject.FindProperty("_queryTriggerInteraction");
			
			Space();
			SectionHeader($"{nameof(PhysicsPointerBase).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(queryTriggerInteraction);
		}

	}
}