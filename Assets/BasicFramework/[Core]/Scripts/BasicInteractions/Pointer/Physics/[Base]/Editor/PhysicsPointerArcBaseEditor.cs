﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
	public abstract class PhysicsPointerArcBaseEditor<TPointerArc> : PhysicsPointerBaseEditor<TPointerArc>
		where TPointerArc : PhysicsPointerArcBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var trajectoryData = TypedTarget.TrajectoryData;
			var maxPhysicsRays = serializedObject.FindProperty("_maxPhysicsRays");
			
			Space();
			SectionHeader($"{nameof(PhysicsPointerArcBase).CamelCaseToHumanReadable()} Debug");
			
			EditorGUILayout.LabelField("Max Physics Rays", maxPhysicsRays.intValue.ToString());

			maxPhysicsRays.isExpanded = EditorGUILayout.Foldout(maxPhysicsRays.isExpanded, "Verbose Data");
			if (!maxPhysicsRays.isExpanded) return;

			EditorGUI.indentLevel++;
			
			Space();
			EditorGUILayout.LabelField("Launch Direction", trajectoryData.LaunchDirection.ToString());
			EditorGUILayout.LabelField("Launch Speed", trajectoryData.LaunchSpeed.ToString());
			EditorGUILayout.LabelField("Launch Velocity", trajectoryData.LaunchVelocity.ToString());
			
			Space();
			EditorGUILayout.LabelField("Gravity Direction", trajectoryData.GravityDirection.ToString());
			EditorGUILayout.LabelField("Gravity Magnitude", trajectoryData.GravityMagnitude.ToString());
			
			Space();
			EditorGUILayout.LabelField("Launch Horizontal Direction", trajectoryData.LaunchHorizontalDirection.ToString());
			EditorGUILayout.LabelField("Launch Horizontal Speed", trajectoryData.LaunchHorizontalSpeed.ToString());
			EditorGUILayout.LabelField("Launch Vertical Direction", trajectoryData.LaunchVerticalDirection.ToString());
			EditorGUILayout.LabelField("Launch Vertical Speed", trajectoryData.LaunchVerticalSpeed.ToString());
			
			EditorGUI.indentLevel--;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var gravityDirection = serializedObject.FindProperty("_gravityDirection");
			
			var physicsTimeStep = serializedObject.FindProperty("_physicsTimeStep");
			var maxVisualTimeStep = serializedObject.FindProperty("_maxVisualTimeStep");
			
			var maxDistance = serializedObject.FindProperty("_maxDistance");
			var minPhysicsRays = serializedObject.FindProperty("_minPhysicsRays");
			var maxPhysicsRays = serializedObject.FindProperty("_maxPhysicsRays");
			var extraPhysicsRays = serializedObject.FindProperty("_extraPhysicsRays");

			
			Space();
			SectionHeader($"{nameof(PhysicsPointerArcBase).CamelCaseToHumanReadable()} Properties");
			
			
			BasicEditorGuiLayout.Vector3DirectionDropdown(gravityDirection);
			
			DisplayTimeStepSlider(physicsTimeStep);
			DisplayTimeStepSlider(maxVisualTimeStep);
			
			EditorGUILayout.PropertyField(maxDistance);
			maxDistance.floatValue = Mathf.Max(0.1f, maxDistance.floatValue);
			
			GUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("Max Physics Rays", maxPhysicsRays.intValue.ToString());
			EditorGUILayout.LabelField(" = ", GUILayout.Width(20));
			EditorGUILayout.LabelField(minPhysicsRays.intValue.ToString(), GUILayout.Width(40));
			EditorGUILayout.LabelField(" + ", GUILayout.Width(20));
			EditorGUILayout.PropertyField(extraPhysicsRays, GUIContent.none);

			GUILayout.EndHorizontal();
		}
		
		private void DisplayTimeStepSlider(SerializedProperty serializedProperty)
		{
			GUILayout.BeginHorizontal();

			if (!serializedProperty.isExpanded)
			{
				serializedProperty.floatValue = EditorGUILayout.Slider(GetGuiContent(serializedProperty), serializedProperty.floatValue, PhysicsPointerArcBase.MINIMUM_TIME_STEP, 1);
			}
			else
			{
				EditorGUILayout.PropertyField(serializedProperty);
			}

			if (GUILayout.Button(serializedProperty.isExpanded ? "1" : "∞", GUILayout.Width(25)))
			{
				serializedProperty.isExpanded = !serializedProperty.isExpanded;
			}
			
			serializedProperty.floatValue = Mathf.Max(PhysicsPointerArcBase.MINIMUM_TIME_STEP, serializedProperty.floatValue);
			
			GUILayout.EndHorizontal();
		}
	
	}
}