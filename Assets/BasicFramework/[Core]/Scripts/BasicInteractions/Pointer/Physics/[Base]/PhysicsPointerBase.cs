﻿using BasicFramework.Core.BasicDebug.Utility;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
	public abstract class PhysicsPointerBase : PointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		// The path points used in the physics calculation, this may be different to base.PathPoints
		public abstract Vector3[] PathPointsPhysics { get; }
		public abstract int PathPointsPhysicsCount { get; }
		
		public abstract RaycastHit RaycastHit { get; }
		
		
		[Tooltip("Should the pointer hit colliders that are set as triggers")]
		[SerializeField] private QueryTriggerInteraction _queryTriggerInteraction = QueryTriggerInteraction.Ignore;
		
		
		public QueryTriggerInteraction QueryTriggerInteraction
		{
			get => _queryTriggerInteraction;
			set => _queryTriggerInteraction = value;
		}

		public bool HitSomething => PointerState == PointerStates.HitSomething;
		public GameObject GameObjectHit => HitSomething ? RaycastHit.transform.gameObject : null;


		// ****************************************  METHODS  ****************************************\\

		//***** overrides ******\\

		protected sealed override void DrawPointerPathInEditor()
		{
			var pointerEventData = BasicPointerEventData;
			
			DebugUtility.DrawPath(PathPointsPhysics, PathPointsPhysicsCount, Color.green);
			DebugUtility.DrawPath(pointerEventData.PathPoints, Color.blue);
		}

	}
}