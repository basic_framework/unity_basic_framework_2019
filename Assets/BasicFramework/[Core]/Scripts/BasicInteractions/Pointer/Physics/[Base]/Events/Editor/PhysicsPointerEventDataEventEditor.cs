using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.Interactions.Pointer.Editor
{
    [CustomEditor(typeof(PhysicsPointerEventDataEvent))][CanEditMultipleObjects]
    public class PhysicsPointerEventDataEventEditor : ScriptableEventBaseEditor<PhysicsPointerEventData, PhysicsPointerEventDataEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

        public override bool TypeIsSerializable => false;
        

        // ****************************************  METHODS  ****************************************\\


    }

    [CustomPropertyDrawer(typeof(PhysicsPointerEventDataEvent))]
    public class PhysicsPointerEventDataEventPropertyDrawer : ScriptableEventBasePropertyDrawer<PhysicsPointerEventData, PhysicsPointerEventDataEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}