using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Interaction/Pointer/Physics Pointer Event Data Event", 
        fileName = "event_physics_pointer_event_data_")
    ]
    public class PhysicsPointerEventDataEvent : ScriptableEventBase<PhysicsPointerEventData>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}