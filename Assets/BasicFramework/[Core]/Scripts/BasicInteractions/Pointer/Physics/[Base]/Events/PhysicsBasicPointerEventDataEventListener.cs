using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.Interactions.Pointer
{
    [AddComponentMenu("Basic Framework/Core/Interactions/Pointer/Physics Pointer Event Data Event Listener")]
    public class PhysicsBasicPointerEventDataEventListener : ScriptableEventListenerBase
    <
        PhysicsPointerEventData, 
        PhysicsPointerEventDataEvent, 
        PhysicsPointerEventDataUnityEvent
    >
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}