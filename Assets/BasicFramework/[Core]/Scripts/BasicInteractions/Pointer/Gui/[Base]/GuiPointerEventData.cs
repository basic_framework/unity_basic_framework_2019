﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BasicFramework.Core.BasicGui.Pointer
{
	
	public class GuiPointerEventData : PointerEventData 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// Keeps track if the button is held
		private bool _buttonHeld;
		public bool ButtonHeld
		{
			get => _buttonHeld;
			set => _buttonHeld = value;
		}

		// Represents the ray that was last used to update pointer event data
		private Ray _ray;
		public Ray Ray
		{
			get => _ray;
			set => _ray = value;
		}

		// The angle in degrees between the previous update and this one
		private float _deltaAngle;
		public float DeltaAngle
		{
			get => _deltaAngle;
			set => _deltaAngle = value;
		}


		// The distance in unity between the previous ray hit position
		// and this one along the plane of the last hit canvas
		private Vector3 _deltaWorldPosition;
		public Vector3 DeltaWorldPosition
		{
			get => _deltaWorldPosition;
			set => _deltaWorldPosition = value;
		}

		// Used to calculate DeltaWorldPosition when no longer pointing at a canvas
		// The position in world space on the plane of the canvas
		private Vector3 _projectedWorldPosition;
		public Vector3 ProjectedWorldPosition
		{
			get => _projectedWorldPosition;
			set => _projectedWorldPosition = value;
		}

		// Used to calculate DeltaWorldPosition when no longer pointing at a canvas
		private Vector3 _projectedCanvasNormal;
		public Vector3 ProjectedCanvasNormal
		{
			get => _projectedCanvasNormal;
			set => _projectedCanvasNormal = value;
		}

		// Used to calculate DeltaWorldPosition when no longer pointing at a canvas
		private bool _hitProjectedCanvas;
		public bool HitProjectedCanvas
		{
			get => _hitProjectedCanvas;
			set => _hitProjectedCanvas = value;
		}

		private float _projectedDistance;
		public float ProjectedDistance
		{
			get => _projectedDistance;
			set => _projectedDistance = value;
		}


		// ****************************************  METHODS  ****************************************\\

		// Constructor
		public GuiPointerEventData(EventSystem eventSystem) : base(eventSystem){}

		public static void Copy(GuiPointerEventData from, GuiPointerEventData to)
		{

			//***** GuiPointerEventData *****\\

			to._buttonHeld = from._buttonHeld;
			to._ray = from._ray;
			to._deltaAngle = from._deltaAngle;
			to._deltaWorldPosition = from._deltaWorldPosition;
			to._projectedWorldPosition = from._projectedWorldPosition;
			to._projectedCanvasNormal = from._projectedCanvasNormal;
			to._hitProjectedCanvas = from._hitProjectedCanvas;
			to._projectedDistance = from._projectedDistance;
			
			
			//***** PointerEventData *****\\
			
			to.pointerEnter = from.pointerEnter;
			to.pointerPress = from.pointerPress;
			to.rawPointerPress = from.rawPointerPress;
			to.pointerDrag = from.pointerDrag;
			to.hovered = from.hovered;
			
			to.pointerCurrentRaycast = from.pointerCurrentRaycast;
			to.pointerPressRaycast = from.pointerPressRaycast;
			
			to.eligibleForClick = from.eligibleForClick;
			to.pointerId = from.pointerId;
			
			to.position = from.position;
			to.pressPosition = from.pressPosition;
			
			to.delta = from.delta;
			to.scrollDelta = from.scrollDelta;
			
			to.clickTime = from.clickTime;
			to.clickCount = from.clickCount;

			to.useDragThreshold = from.useDragThreshold;
			to.dragging = from.dragging;
			to.button = from.button;
			
			
			//***** BaseEventData *****\\

			to.selectedObject = from.selectedObject;
			
		}
		
	}
	
	[Serializable]
	public class GuiPointerEventDataUnityEvent : UnityEvent<GuiPointerEventData>{}
	
	[Serializable]
	public class GuiPointerEventDataArrayUnityEvent : UnityEvent<GuiPointerEventData[]>{}
	
}