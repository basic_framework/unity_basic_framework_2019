﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicGui.Pointer
{
	public enum GuiPointerStates
	{
		NoTarget,
		HoverTarget,
		PressedTarget,
		DraggingTarget,
	}
	
	[Serializable]
	public class GuiPointerStatesUnityEvent : UnityEvent<GuiPointerStates>{}

	[Serializable]
	public class GuiPointerStatesCompareUnityEvent : CompareValueUnityEventBase<GuiPointerStates>{}
	
	
	public enum RayVisualModes
	{
		Transform,
		LineRenderer
	}
	
	[Serializable]
	public class RayVisualModesUnityEvent : UnityEvent<RayVisualModes>{}

	[Serializable]
	public class RayVisualModesCompareUnityEvent : CompareValueUnityEventBase<RayVisualModes>{}

}