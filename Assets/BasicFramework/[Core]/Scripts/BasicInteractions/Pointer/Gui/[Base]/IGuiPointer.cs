﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	public interface IGuiPointer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		string PointerName { get; }
		
		bool PointerEnabled { get; }
		
		LayerMask LayerMask { get; }
		float MaxRaycastDistance { get; }

		bool ButtonHeld { get; }

		BasicPose PointerPose { get; }
		
		BasicEvent<GuiPointerEventData> PointerEventDataChanged { get; }
		

		// ****************************************  METHODS  ****************************************\\
		
	}
}