﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[RequireComponent(typeof(EventSystem))] 
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Gui Pointer Input Module")]
	public class GuiPointerInputModule : PointerInputModule 
	{
		
		// TODO: Check if GuiPointer is enabled before doing raycast, if it is keep default values

		// **************************************** VARIABLES ****************************************\\

		private const int CURRENT_POINTER_EVENT_DATA_INDEX = 0;
		private const int PREVIOUS_POINTER_EVENT_DATA_INDEX = 1;
		
		private const float GUI_CAMERA_NEAR_CLIP_PLANE = 0.01f;
		private const float GUI_CAMERA_FIELD_OF_VIEW = 1;
		private const float MULTI_CLICK_WAIT_TIME = 0.3f;
		
		private static GuiPointerInputModule _instance;
		public static GuiPointerInputModule Instance => _instance;

		[Tooltip("The distance in millimetres that the pointer canvas position needs to move for a drag to begin")]
		[SerializeField] private float _dragThresholdMillimetres = 30f;

		[Tooltip("Reference to the event system. There should only be one in the scene and it should be on the same gameobject as this")]
		[SerializeField] private EventSystem _eventSystem = default;
		
		
		public float DragThresholdMillimetres
		{
			get => _dragThresholdMillimetres;
			set => _dragThresholdMillimetres = value;
		}

		private Camera _guiCamera;
		public Camera GuiCamera => _guiCamera;
		
		private Transform _guiCameraTransform;

		public readonly Dictionary<IGuiPointer, GuiPointerEventData[]> GuiPointerDatas = 
			new Dictionary<IGuiPointer, GuiPointerEventData[]>();
		

		// ****************************************  METHODS  ****************************************\\

		public static GuiPointerInputModule GetOrCreateInstance()
		{
			// TODO: Test this may cause issues
			
			if (_instance != null) return _instance;
			
			_instance = FindObjectOfType<GuiPointerInputModule>();
			if (_instance != null) return _instance;
			
			var go = new GameObject("event_system_basic_pointer");
			_instance = go.AddComponent<GuiPointerInputModule>();

			var eventSystem = _instance.GetComponent<EventSystem>();
			if (eventSystem == null)
			{
				_instance.gameObject.AddComponent<EventSystem>();
			}
			
			EventSystem.current.UpdateModules();

			return _instance;
		}

		
		//***** mono *****\\
		
#if UNITY_EDITOR
		private new void Reset()
		{
			_eventSystem = GetComponent<EventSystem>();
		}

		private new void OnValidate()
		{
			if (_eventSystem == null)
			{
				_eventSystem = GetComponent<EventSystem>();
			}
		}
#endif

		private new void Awake()
		{
			//base.Awake();

			// Need to check if instance is this as it could be set before getting here
			if (_instance != null && _instance != this)
			{
				Destroy(gameObject);
				return;
			}

			_instance = this;

			if (_eventSystem == null)
			{
				_eventSystem = GetComponent<EventSystem>();				
			}
			
			_guiCamera = new GameObject("gui_pointer_event_camera").AddComponent<Camera>();
			_guiCameraTransform = _guiCamera.transform;
			
			_guiCameraTransform.SetParent(transform);

			_guiCamera.enabled = false;
			_guiCamera.stereoTargetEye = StereoTargetEyeMask.None;
			_guiCamera.clearFlags = CameraClearFlags.Nothing;
			_guiCamera.cullingMask = 0;
			_guiCamera.fieldOfView = GUI_CAMERA_FIELD_OF_VIEW; // WHY
			_guiCamera.nearClipPlane = GUI_CAMERA_NEAR_CLIP_PLANE;
		}
		

		//***** Miscellaneous *****\\

		public void AddGuiPointer(IGuiPointer guiPointer)
		{
			if (_eventSystem == null)
			{
				_eventSystem = GetComponent<EventSystem>();
			}

			if (GuiPointerDatas.ContainsKey(guiPointer)) return;
			GuiPointerDatas.Add(guiPointer, new[]{new GuiPointerEventData(_eventSystem), new GuiPointerEventData(_eventSystem)});
		}

		public void RemoveGuiPointer(IGuiPointer guiPointer)
		{
			if(!GuiPointerDatas.ContainsKey(guiPointer)) return;
			
			var currentPointerEventData = GuiPointerDatas[guiPointer][CURRENT_POINTER_EVENT_DATA_INDEX];

			if (currentPointerEventData.pointerPress != null)
			{
				ProcessButtonUp(guiPointer, currentPointerEventData);
			}
			
			GuiPointerDatas.Remove(guiPointer);
		}
		
		public override void DeactivateModule()
		{
			base.DeactivateModule();
			ClearSelection();
		}
		
		
		//***** Process *****\\
		
		// Required => BaseInputModule
		public override void Process()
		{
			// Loop through each pointer object and update the canvas elements
			foreach (var guiPointerData in GuiPointerDatas)
			{
				//ProcessGuiPointerData(guiPointerData.Key, guiPointerData.Value);

				var guiPointer = guiPointerData.Key;
				var guiPointerEventDatas = guiPointerData.Value;
				
				UpdatePointerEventData(guiPointer, guiPointerEventDatas);

				var previousPointerEventData = guiPointerEventDatas[PREVIOUS_POINTER_EVENT_DATA_INDEX];
				var currentPointerEventData = guiPointerEventDatas[CURRENT_POINTER_EVENT_DATA_INDEX];
			
				// This will set pointerEventData.pointerEnter (and hovered???), don't need to worry about that
				HandlePointerExitAndEnter(currentPointerEventData, currentPointerEventData.pointerCurrentRaycast.gameObject);

				if (!previousPointerEventData.ButtonHeld && currentPointerEventData.ButtonHeld)
				{
					ProcessButtonDown(guiPointer, currentPointerEventData);
				}

				if (previousPointerEventData.ButtonHeld && !currentPointerEventData.ButtonHeld)
				{
					ProcessButtonUp(guiPointer, currentPointerEventData);
				}
			
				BasicProcessDrag(guiPointer, currentPointerEventData);

				guiPointer.PointerEventDataChanged.Raise(currentPointerEventData);
			}
		}

		private void UpdatePointerEventData(IGuiPointer guiPointer, GuiPointerEventData[] guiPointerEventDatas)
		{
			
			var currentPointerEventData = guiPointerEventDatas[CURRENT_POINTER_EVENT_DATA_INDEX];
			var previousPointerEventData = guiPointerEventDatas[PREVIOUS_POINTER_EVENT_DATA_INDEX];
			
			// Update our previous data to the current data
			GuiPointerEventData.Copy(currentPointerEventData, previousPointerEventData);
			
			
			//***** Raycast *****\\
			
			// Move the camera to the pointer position and rotation
			var pointerPose = guiPointer.PointerPose;
			_guiCameraTransform.position = pointerPose.Position;
			_guiCameraTransform.rotation = pointerPose.Rotation;

			currentPointerEventData.Reset();
			
			// Place the pointer (cursor) screen position in the center of the camera so the raycast goes directly forward
			currentPointerEventData.position = new Vector2(_guiCamera.pixelWidth * 0.5f, _guiCamera.pixelHeight * 0.5f);

			var guiPointerEnabled = guiPointer.PointerEnabled;
			currentPointerEventData.ButtonHeld = guiPointerEnabled && guiPointer.ButtonHeld;

			if (guiPointerEnabled)
			{
				_eventSystem.RaycastAll(currentPointerEventData, m_RaycastResultCache);
				currentPointerEventData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
				m_RaycastResultCache.Clear();
			}
			else
			{
				currentPointerEventData.pointerCurrentRaycast = new RaycastResult();
			}
			
			// Make sure the canvas is in distance of our pointer
			if (currentPointerEventData.pointerCurrentRaycast.distance > guiPointer.MaxRaycastDistance)
			{
				currentPointerEventData.pointerCurrentRaycast = new RaycastResult();
			}
			
			// If we hit something do more checks
			if (currentPointerEventData.pointerCurrentRaycast.distance > 0.0f)
			{
				// Make sure the object we hit is contained in the layers we are looking for
				var hitLayer = 1 << currentPointerEventData.pointerCurrentRaycast.gameObject.layer;
				if ((hitLayer & guiPointer.LayerMask) != hitLayer)
				{
					currentPointerEventData.pointerCurrentRaycast = new RaycastResult();
				}
			}

			
			//***** Store Extra Data *****\\
			
			// Store the ray we used
			var forward = _guiCameraTransform.forward;
			var position = _guiCameraTransform.position + forward * GUI_CAMERA_NEAR_CLIP_PLANE;
			
			var currentRay = new Ray(position, forward);
			
			currentPointerEventData.Ray = currentRay;
			
			
			// Work out out deltas
			currentPointerEventData.DeltaAngle = Vector3.Angle(forward, previousPointerEventData.Ray.direction);
			
			// Reset values, they will be set below
			currentPointerEventData.HitProjectedCanvas = false;
			currentPointerEventData.DeltaWorldPosition = Vector3.zero;
			currentPointerEventData.ProjectedWorldPosition = Vector3.zero;
			currentPointerEventData.ProjectedCanvasNormal = Vector3.zero;
			currentPointerEventData.ProjectedDistance = 0f;
			
			var currentRaycast = currentPointerEventData.pointerCurrentRaycast;
			
			if (currentRaycast.distance > 0.0f)
			{
				// We hit a canvas this process so get the difference in hit position between now and previous hit
				currentPointerEventData.HitProjectedCanvas = true;
				currentPointerEventData.ProjectedWorldPosition = currentRaycast.worldPosition;
				currentPointerEventData.ProjectedCanvasNormal = currentRaycast.worldNormal;
				currentPointerEventData.ProjectedDistance = (currentRay.origin - currentRaycast.worldPosition).magnitude;

				if (!previousPointerEventData.HitProjectedCanvas) return;
				currentPointerEventData.DeltaWorldPosition = currentRaycast.worldPosition - previousPointerEventData.ProjectedWorldPosition;
			}
			else if(previousPointerEventData.HitProjectedCanvas)
			{
				// We didn't hit a canvas this process so check where we would intersect with one
				
				// Check that the ray is actually facing the plane
				var planeInFront =
					Vector3.Angle(currentRay.direction, previousPointerEventData.ProjectedCanvasNormal) > 90;

				if (!planeInFront) return;

				// Check that we will actually intersect the plane
				var willIntersectPlane = MathUtility.LinePlaneIntersection(
					out var projectedIntersectPosition,
					currentRay.origin,currentRay.direction,
					previousPointerEventData.ProjectedCanvasNormal,previousPointerEventData.ProjectedWorldPosition);

				if (!willIntersectPlane) return;

				currentPointerEventData.HitProjectedCanvas = true;
				currentPointerEventData.ProjectedWorldPosition = projectedIntersectPosition;
				currentPointerEventData.ProjectedCanvasNormal = previousPointerEventData.ProjectedCanvasNormal;
				currentPointerEventData.ProjectedDistance = (currentRay.origin - projectedIntersectPosition).magnitude;
				
				currentPointerEventData.DeltaWorldPosition = projectedIntersectPosition - previousPointerEventData.ProjectedWorldPosition;
			}
		}

		private void ProcessButtonDown(IGuiPointer guiPointer, GuiPointerEventData currentPointerEventData)
		{
			// The gameobject that was hit by the raycast
			var currentOverGameObject = currentPointerEventData.pointerCurrentRaycast.gameObject;
			
			currentPointerEventData.eligibleForClick = true;
			currentPointerEventData.delta = Vector2.zero;
			currentPointerEventData.dragging = false;
			currentPointerEventData.useDragThreshold = true;
			currentPointerEventData.pressPosition = currentPointerEventData.position;
			currentPointerEventData.pointerPressRaycast = currentPointerEventData.pointerCurrentRaycast;
			
			// Raises Select and Deselect handlers
			DeselectIfSelectionChanged(currentOverGameObject, currentPointerEventData);
			
			// search for the control that will receive the press
			// if we can't find a press handler set the press
			// handler to be what would receive a click.
			var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGameObject, currentPointerEventData, ExecuteEvents.pointerDownHandler);
			
			// didnt find a press handler... search for a click handler
			if (newPressed == null)
			{
				newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGameObject);
			}
				
			// Update the click count
			var time = Time.unscaledTime;

			if (newPressed == currentPointerEventData.lastPress)
			{
				var diffTime = time - currentPointerEventData.clickTime;
				if (diffTime < MULTI_CLICK_WAIT_TIME)
				{
					++currentPointerEventData.clickCount;
				}
				else
				{
					currentPointerEventData.clickCount = 1;
				}
				
				currentPointerEventData.clickTime = time;
			}
			else
			{
				currentPointerEventData.clickCount = 1;
			}

			currentPointerEventData.clickTime = time;
			
			// The gameobject that handled either pointerDown or can handle pointerclick
			currentPointerEventData.pointerPress = newPressed;
			
			// The gameobject out raycast originally hit
			currentPointerEventData.rawPointerPress = currentOverGameObject;

			
			// Save the drag handler as well
			currentPointerEventData.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGameObject);

			if (currentPointerEventData.pointerDrag != null)
			{
				ExecuteEvents.Execute(currentPointerEventData.pointerDrag, currentPointerEventData, ExecuteEvents.initializePotentialDrag);
			}
		}
		
		private void ProcessButtonUp(IGuiPointer guiPointer, GuiPointerEventData currentPointerEventData)
		{
			var currentOverGameObject = currentPointerEventData.pointerCurrentRaycast.gameObject;
			
			ExecuteEvents.Execute(currentPointerEventData.pointerPress, currentPointerEventData, ExecuteEvents.pointerUpHandler);

			var pointerClickHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGameObject);
			
			//***** Handle Click and Drop *****\\
			
			if (currentPointerEventData.pointerPress == pointerClickHandler && currentPointerEventData.eligibleForClick)
			{
				ExecuteEvents.Execute(currentPointerEventData.pointerPress, currentPointerEventData, ExecuteEvents.pointerClickHandler);
			}
			else if (currentPointerEventData.pointerDrag != null && currentPointerEventData.dragging)
			{
				ExecuteEvents.ExecuteHierarchy(currentOverGameObject, currentPointerEventData, ExecuteEvents.dropHandler);
			}
			
			currentPointerEventData.eligibleForClick = false;
			currentPointerEventData.pointerPress = null;
			currentPointerEventData.rawPointerPress = null;

			
			//***** Handle Drag *****\\

			if (currentPointerEventData.pointerDrag != null && currentPointerEventData.dragging)
			{
				ExecuteEvents.Execute(currentPointerEventData.pointerDrag, currentPointerEventData, ExecuteEvents.endDragHandler);
			}
			
			currentPointerEventData.dragging = false;
			currentPointerEventData.pointerDrag = null;
			
			// redo pointer enter / exit to refresh state, so that if we moused over something that ignored it before
			// due to having pressed on something else it now gets it.
			if (currentOverGameObject == currentPointerEventData.pointerEnter) return;
			HandlePointerExitAndEnter(currentPointerEventData, null);
			HandlePointerExitAndEnter(currentPointerEventData, currentOverGameObject);
		}
		
		private void BasicProcessDrag(IGuiPointer guiPointer, GuiPointerEventData currentPointerEventData)
		{
			// Make sure we have a drag target and the hit position moved this process
			if (currentPointerEventData.pointerDrag == null) return;
			if (Math.Abs(currentPointerEventData.DeltaWorldPosition.sqrMagnitude) < Mathf.Epsilon) return;
			
			if (!currentPointerEventData.dragging && ShouldStartDrag(guiPointer, currentPointerEventData))
			{
				currentPointerEventData.dragging = true;
				ExecuteEvents.Execute(currentPointerEventData.pointerDrag, currentPointerEventData, ExecuteEvents.beginDragHandler);
			}

			if (!currentPointerEventData.dragging) return;
			
			// Before doing drag we should cancel any pointer down state and clear selection!
			if (currentPointerEventData.pointerPress != currentPointerEventData.pointerDrag)
			{
				ExecuteEvents.Execute(currentPointerEventData.pointerPress, currentPointerEventData, ExecuteEvents.pointerUpHandler);

				currentPointerEventData.eligibleForClick = false;
				currentPointerEventData.pointerPress = null;
				currentPointerEventData.rawPointerPress = null;
			}
			
			ExecuteEvents.Execute(currentPointerEventData.pointerDrag, currentPointerEventData, ExecuteEvents.dragHandler);
		}
		
		private bool ShouldStartDrag(IGuiPointer guiPointer, GuiPointerEventData currentPointerEventData)
		{
			// Make sure the distance between where we started pressing and now is greater than our threshold
			var pressToCurrentPosition = currentPointerEventData.ProjectedWorldPosition -
			                             currentPointerEventData.pointerPressRaycast.worldPosition;
			
			return pressToCurrentPosition.magnitude * 1000 > _dragThresholdMillimetres;
		}
		
	}
}