﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerInputModule))]
	public class GuiPointerInputModuleEditor : BasicBaseEditor<GuiPointerInputModule>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{

			base.DrawPropertyFields();
			
			Space();
			
			var eventSystem = serializedObject.FindProperty("_eventSystem");
			var dragThresholdMillimetres = serializedObject.FindProperty("_dragThresholdMillimetres");
			
			if (eventSystem.objectReferenceValue == null)
			{
				EditorGUILayout.HelpBox("Please add an EventSystem component to this gameobject", MessageType.Warning);
			}
			
			GUI.enabled = false;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(eventSystem);

			GUI.enabled = PrevGuiEnabled;

			Space();
			EditorGUILayout.PropertyField(dragThresholdMillimetres);

			if (!Application.isPlaying) return;
			
			Space();
			SectionHeader("Managed Pointers");
			foreach (var key in TypedTarget.GuiPointerDatas.Keys)
			{
				EditorGUILayout.LabelField(key.PointerName, key.PointerEnabled ? "Enabled" : "Disabled");
			}
		}
		
	}
}