﻿using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Gui Pointer Canvas")]
	public class GuiPointerCanvas : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		public static readonly HashSet<GuiPointerCanvas> GuiPointerCanvases = new HashSet<GuiPointerCanvas>();
		
		[Tooltip("The canvas to make a touch screen")]
		[SerializeField] private Canvas _canvas = default;
		
		[Tooltip("The rect transform of the canvas")]
		[SerializeField] private RectTransform _rectTransformCanvas = default;

		[Tooltip("Use this to scale the screen size if it is incorrect. " +
		         "For example, because ui elements are outside canvas rect")]
		[SerializeField] private float _canvasSizeMultiplier = 1f;


		public Canvas Canvas => _canvas;

		public RectTransform RectTransformCanvas => _rectTransformCanvas;

		public BasicPose CanvasPose => new BasicPose(_rectTransformCanvas.position, _rectTransformCanvas.rotation);
		public Vector2 CanvasSize => _rectTransformCanvas.rect.size * _rectTransformCanvas.lossyScale * _canvasSizeMultiplier;
		public Vector3 CanvasNormal => _rectTransformCanvas.forward * -1f;
		

		// ****************************************  METHODS  ****************************************\\

		protected virtual void Reset()
		{
			_canvas = GetComponentInChildren<Canvas>();
			if (_canvas == null) return;
			_rectTransformCanvas = _canvas.transform as RectTransform;
		}

		protected virtual void OnValidate()
		{
			if (_canvas == null) return;
			_rectTransformCanvas = _canvas.transform as RectTransform;
		}
		
		protected virtual void Awake()
		{
			_rectTransformCanvas = _canvas.transform as RectTransform;
		}

		protected virtual void OnEnable()
		{
			if (_canvas.renderMode != RenderMode.WorldSpace)
			{
				Debug.LogError($"{name}/{GetType().Name.CamelCaseToHumanReadable()} => " +
				               $"Disabling \"{GetType().Name.CamelCaseToHumanReadable()}\", " +
				               $"referenced canvas's render mode is not set to World Space");
				enabled = false;
				return;
			}
			
			GuiPointerCanvases.Add(this);
		}

		protected virtual void OnDisable()
		{
			GuiPointerCanvases.Remove(this);
		}

		private void Start()
		{
			var guiPointerInputModule = GuiPointerInputModule.Instance;
			
			if (guiPointerInputModule == null)
			{
				Debug.LogError($"{name}/{GetType().Name} => " +
				               $"Please add a Gui Pointer Input module to the scene");
				enabled = false;
				return;
			}
			
			_canvas.worldCamera = guiPointerInputModule.GuiCamera;
		}

		protected virtual void OnDrawGizmosSelected()
		{
			if (_canvas == null) return;
			if (_rectTransformCanvas == null) return;
			
			var previousColor = Gizmos.color;
			Gizmos.color = Color.blue;

			var screenPose = CanvasPose;

			// Draw normal
			Gizmos.DrawRay(screenPose.Position, CanvasNormal);
			
			
			// Draw boundary
			
			Gizmos.color = Color.magenta;
			
			var screenSize = CanvasSize;
			var halfScreenSize = screenSize * 0.5f;

			var up = screenPose.Rotation * Vector3.up;
			var right = screenPose.Rotation * Vector3.right;
			
			var bottomLeft 	= screenPose.Position - screenPose.Rotation * halfScreenSize;
			var topRight 	= screenPose.Position + screenPose.Rotation * halfScreenSize;
			
			Gizmos.DrawRay(bottomLeft, screenSize.x * right);
			Gizmos.DrawRay(bottomLeft, screenSize.y * up);
			
			Gizmos.DrawRay(topRight, screenSize.x * -1 * right);
			Gizmos.DrawRay(topRight, screenSize.y * -1 * up);
			
			
			Gizmos.color = previousColor;
		}
		
	}
}