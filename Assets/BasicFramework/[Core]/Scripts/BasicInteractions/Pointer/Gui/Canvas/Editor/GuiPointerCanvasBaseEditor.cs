﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerCanvas))]
	public class GuiPointerCanvasBaseEditor : BasicBaseEditor<GuiPointerCanvas>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();

			var canvas = TypedTarget.Canvas;

			string message;
			MessageType messageType;
			
			if (canvas == null)
			{
				message = "Please assign a Canvas";
				messageType = MessageType.Error;
			}
			else if (canvas.renderMode != RenderMode.WorldSpace)
			{
				message = "The referenced Canvas's render mode needs to be changed to \"World Space\"" +
				          "\nThe GuiPointer system only works with World Space Canvas's";
				messageType = MessageType.Error;
			}
			else
			{
				message = "The Canvas's world camera will be set at runtime";
				messageType = MessageType.Info;
			}
			
			EditorGUILayout.HelpBox(message, messageType);
			
			GuiPointerEditorUtility.DrawAddGuiPointerInputModuleButton();
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var rectTransformCanvas = serializedObject.FindProperty("_rectTransformCanvas");

			GUI.enabled = false;

			BasicEditorGuiLayout.PropertyFieldNotNull(rectTransformCanvas);
			
			var canvasRect = rectTransformCanvas.objectReferenceValue as RectTransform; 
			var canvasNotNull = canvasRect != null;
			EditorGUILayout.Vector2Field(nameof(TypedTarget.CanvasSize).CamelCaseToHumanReadable(), canvasNotNull ? TypedTarget.CanvasSize : Vector2.zero);

			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			
			var canvas = serializedObject.FindProperty("_canvas");
			var rectTransformCanvas = serializedObject.FindProperty("_rectTransformCanvas");
			var canvasSizeMultiplier = serializedObject.FindProperty("_canvasSizeMultiplier");
			
			
			SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			BasicEditorGuiLayout.PropertyFieldNotNull(canvas);
			if (EditorGUI.EndChangeCheck())
			{
				var canvasObject = canvas.objectReferenceValue as Canvas;
				rectTransformCanvas.objectReferenceValue =
					canvasObject != null ? canvasObject.transform as RectTransform : null;
			}

			EditorGUILayout.PropertyField(canvasSizeMultiplier);

		}
		
	}
}