﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Pointers/Gui Pointer Touch")]
	public class GuiPointerTouch : GuiPointerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private const float TOUCH_DISTANCE_ENTER = 0.0f;
		private const float TOUCH_DISTANCE_EXIT = 0.02f;
		
		// How far behind the screen will we consider it to still be seleceted
		private const float DISTANCE_POINTER_ENABLE = 0.2f;
		private const float DISTANCE_POINTER_DISABLE = -0.1f;
		
		// How far away from the screen to position the guipointer
		private const float GUI_POINTER_OFFSET = 0.02f;
		private const float MAX_RAYCAST_DISTANCE = GUI_POINTER_OFFSET * 2f;
		
		
		//***** hiding *****\\
		
		public new float MaxRaycastDistance => MAX_RAYCAST_DISTANCE;
		
		
		//***** touch properties *****\\

		[SerializeField] private GuiPointerTouchVolume _touchVolume = default;

		[SerializeField] private NormalizedFloatPlusUnityEvent _touchDistanceChanged = default;
		
		
		public GuiPointerTouchVolume TouchVolume => _touchVolume;
		
		public NormalizedFloatPlusUnityEvent TouchDistanceChanged => _touchDistanceChanged;
		
		
		private bool _pointingAtCanvas;
		public bool PointingAtCanvas
		{
			get => _pointingAtCanvas;
			private set
			{
				if (_pointingAtCanvas == value) return;
				_pointingAtCanvas = value;
				PointingAtCanvasUpdated(_pointingAtCanvas);
			}
		}
		
		private float _distanceToCanvas;
		public float DistanceToCanvas
		{
			get => _distanceToCanvas;
			private set
			{
				if (Math.Abs(_distanceToCanvas - value) < Mathf.Epsilon) return;
				var prevValue = _distanceToCanvas;
				_distanceToCanvas = value;
				DistanceToCanvasUpdated(prevValue, value);
			}
		}

		private NormalizedFloat _touchDistance;
		public NormalizedFloat TouchDistance
		{
			get => _touchDistance;
			private set
			{
				if (_touchDistance == value) return;
				_touchDistance = value;
				TouchDistanceUpdated(_touchDistance);
			}
		}

		
		private bool _touchingCanvas;
		public bool TouchingCanvas => _touchingCanvas;

		private BasicPose _pointerPose;


		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\
		
		private void OnDrawGizmosSelected()
		{
			if (_touchVolume == null) return;
			_touchVolume.OnDrawGizmosSelected();
		}

		protected override void Start()
		{
			PointingAtCanvasUpdated(_pointingAtCanvas);
			TouchDistanceUpdated(_touchDistance);
		}

		protected override void Update()
		{
			var touchPosition =  _touchVolume.Position;
			var touchRadius = _touchVolume.Radius;
			
			var pointingAtCanvas = false;
			var closestDistance = DISTANCE_POINTER_ENABLE;

			foreach (var guiPointerCanvas in GuiPointerCanvas.GuiPointerCanvases)
			{
				var canvasPose = guiPointerCanvas.CanvasPose;
				
				var canvasPosition = canvasPose.Position;
				var canvasRotation = canvasPose.Rotation;
				var canvasNormal = guiPointerCanvas.CanvasNormal;
				
				// Find distance to screen plane
				
				var touchToCanvas = canvasPosition - touchPosition;
				
				var distanceToCanvas = 
					MathUtility.MagOfVectorInAxis(touchToCanvas, canvasNormal * -1f) - touchRadius;
				
				if(distanceToCanvas > closestDistance) continue;
				if(distanceToCanvas < DISTANCE_POINTER_DISABLE) continue;
				
				// Find if intersects with canvas
				var halfCanvasSize = guiPointerCanvas.CanvasSize * 0.5f; 
				
				var canvasPlaneIntersectPoint = touchPosition + (distanceToCanvas + touchRadius) * -1 * canvasNormal;
				
				var canvasPlaneIntersectOffset = 
					MathUtility.InverseTransformPoint(
					canvasPosition, canvasRotation,
					Vector3.one, canvasPlaneIntersectPoint);

				var intersectsCanvas = Math.Abs(canvasPlaneIntersectOffset.x) <= halfCanvasSize.x &&
				                       Math.Abs(canvasPlaneIntersectOffset.y) <= halfCanvasSize.y;

				
//				// TODO: Comment out
//				var color = intersectsCanvas ? Color.green : Color.red;
//				Debug.DrawRay(canvasPlaneIntersectPoint, canvasNormal * distanceToCanvas, Color.blue);
//				Debug.DrawRay(canvasPlaneIntersectPoint, canvasRotation * Vector3.up * 0.025f, color);
//				Debug.DrawRay(canvasPlaneIntersectPoint, canvasRotation * Vector3.down * 0.025f, color);
//				Debug.DrawRay(canvasPlaneIntersectPoint, canvasRotation * Vector3.right * 0.025f, color);
//				Debug.DrawRay(canvasPlaneIntersectPoint, canvasRotation * Vector3.left * 0.025f, color);
//				Debug.DrawRay(canvasPosition, canvasRotation * canvasPlaneIntersectOffset, Color.yellow);
				
				
				if(!intersectsCanvas) continue;

				
				// This is the information we want to use for the point

				pointingAtCanvas = true;
				closestDistance = distanceToCanvas;
				
				_pointerPose.Position = canvasPlaneIntersectPoint + canvasNormal * GUI_POINTER_OFFSET;
				_pointerPose.Rotation = MathUtility.Direction2Quaternion(canvasNormal * -1);
				
//				// TODO: Comment out
//				Debug.DrawRay(
//					_pointerPose.Position, 
//					_pointerPose.Rotation * (Vector3.forward * GUI_POINTER_OFFSET), Color.cyan);
				
			}
			
			PointingAtCanvas = pointingAtCanvas;
			if(!pointingAtCanvas) return;
			DistanceToCanvas = closestDistance;
		}

		
		//***** overrides *****\\

		protected override bool GetButtonHeld() => _touchingCanvas;

		protected override BasicPose GetPointerPose() => _pointerPose;


		//***** property updates *****\\

		private void PointingAtCanvasUpdated(bool pointingAtScreen)
		{
			AllowPointerEnabled = pointingAtScreen;
			DistanceToCanvas = DISTANCE_POINTER_DISABLE;
		}

		private void DistanceToCanvasUpdated(float previousDistance, float currentDistance)
		{
			if (!PointerEnabled)
			{
				_touchingCanvas = false;
				TouchDistance = NormalizedFloat.Zero;
			}
			else
			{
				_touchingCanvas = _touchingCanvas
					? currentDistance < TOUCH_DISTANCE_EXIT
					: currentDistance <= TOUCH_DISTANCE_ENTER && previousDistance > TOUCH_DISTANCE_ENTER;
				
				TouchDistance = 
					new NormalizedFloat(currentDistance, DISTANCE_POINTER_ENABLE, TOUCH_DISTANCE_ENTER);
			}
		}
		
		private void TouchDistanceUpdated(NormalizedFloat touchDistance)
		{
			_touchDistanceChanged.Raise(TouchDistance);
		}

	}
}