﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Pointers/Gui Pointer Camera")]
	public class GuiPointerCamera : GuiPointerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Camera _camera = default;
		
		[SerializeField] private BoolInputReference _selectInput = new BoolInputReference(BoolInputReferenceTypes.Keycode);
		
		
		public Camera Camera => _camera;


		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_camera = GetComponent<Camera>();
		}

		
		//***** overrides *****\\

		protected override BasicPose GetPointerPose()
		{
			var ray = _camera.ScreenPointToRay(Input.mousePosition);
			
			return new BasicPose(ray, Vector3.up);
		}

		protected override bool GetButtonHeld() => _selectInput.Value;
		
	}
}