﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerTouch))][CanEditMultipleObjects]
	public class GuiPointerTouchEditor : GuiPointerBaseEditor<GuiPointerTouch>
	{
	
		// **************************************** VARIABLES ****************************************\\

	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			// Touching canvas, touch distance, pointing at canvas
			
			Space();
			EditorGUILayout.LabelField("Max Raycast Distance", TypedTarget.MaxRaycastDistance.ToString());
			EditorGUILayout.LabelField("Pointing At Canvas", TypedTarget.PointingAtCanvas.ToString());
			EditorGUILayout.LabelField("Touching Canvas", TypedTarget.TouchingCanvas.ToString());
			
			GUI.enabled = false;
			EditorGUILayout.Slider("Touch Distance", TypedTarget.TouchDistance.Value, 0f, 1f);
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			//base.DrawPropertyFields();
			
			var touchVolume = serializedObject.FindProperty("_touchVolume");
			var layerMask = serializedObject.FindProperty("_layerMask");

			Space();
			SectionHeader($"{nameof(GuiPointerBase).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(layerMask);
			
			Space();
			SectionHeader($"{nameof(GuiPointerTouch).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(touchVolume);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			Space();
			SectionHeader($"{nameof(GuiPointerTouch).CamelCaseToHumanReadable()} Events");
			
			var touchDistanceChanged = serializedObject.FindProperty("_touchDistanceChanged");
			
			EditorGUILayout.PropertyField(touchDistanceChanged);
		}
	}
}