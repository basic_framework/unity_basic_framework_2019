﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
    [AddComponentMenu("Basic Framework/Core/Gui/Pointer/Pointers/Gui Pointer Basic")]
    public class GuiPointerBasic : GuiPointerBase 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BoolInputReference _selectInput = new BoolInputReference(BoolInputReferenceTypes.Keycode);
        
        [Tooltip("The pose source that will be used to cast the ray from")]
        [SerializeField] private PoseSource _pointerPoseSource = new PoseSource();

        [SerializeField] private Vector3 _offsetPosition = Vector3.zero;
        [SerializeField] private Vector3 _offsetRotation = Vector3.zero;
		
		[SerializeField] private NormalizedFloat _lerpRatePosition = new NormalizedFloat(1f);
		[SerializeField] private NormalizedFloat _lerpRateRotation = new NormalizedFloat(1f);
		
		
        public PoseSource PointerPoseSource => _pointerPoseSource;

        public Vector3 OffsetPosition => _offsetPosition;

        public Vector3 OffsetRotation => _offsetRotation;
		
		public NormalizedFloat LerpRatePosition => _lerpRatePosition;

		public NormalizedFloat LerpRateRotation => _lerpRateRotation;

		private BasicPose _lastPose;


		// ****************************************  METHODS  ****************************************\\
		
        
        //***** mono *****\\
		
        protected virtual void Reset()
        {
            _pointerPoseSource.Transform = transform;
            _pointerPoseSource.Rigidbody = GetComponentInParent<Rigidbody>();
        }
        
        protected override void Awake()
		{
			base.Awake();
			
			_lastPose = GetTargetPose();
		}


        //***** overrides *****\\
        
        protected override BasicPose GetPointerPose()
        {
	        var targetPose = GetTargetPose();

	        targetPose.Position = Vector3.Lerp(_lastPose.Position, targetPose.Position, _lerpRatePosition.Value);
	        targetPose.Rotation = Quaternion.Lerp(_lastPose.Rotation, targetPose.Rotation, _lerpRateRotation.Value);
            
	        _lastPose = targetPose;

	        return targetPose;
        }

        protected override bool GetButtonHeld() => _selectInput.Value;
        
        
        //***** functions *****\\

        private BasicPose GetTargetPose()
        {
            var targetPose = _pointerPoseSource.Pose;
			
            var trsMatrix = Matrix4x4.TRS(targetPose.Position, targetPose.Rotation, Vector3.one);
            targetPose.Position = MathUtility.TransformPoint(trsMatrix, _offsetPosition);
            targetPose.Rotation = MathUtility.TransformQuaternion(targetPose.Rotation, Quaternion.Euler(_offsetRotation));

            return targetPose;
        }

    }
}