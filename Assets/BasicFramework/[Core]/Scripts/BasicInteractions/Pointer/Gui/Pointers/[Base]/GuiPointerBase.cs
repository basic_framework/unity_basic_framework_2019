﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_POINTER_CALCULATION)]
	public abstract class GuiPointerBase : MonoBehaviour, IGuiPointer
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private LayerMask _layerMask = ~0;

		[SerializeField] private float _maxRaycastDistance = 5f;

		
		[Tooltip("Raised when the pointer goes from enable to disabled or vice versa")]
		[SerializeField] private BoolPlusUnityEvent _pointerEnabledChanged = default;
		
		[Tooltip("Raised when the Gui Pointer State changes")]
		[SerializeField] private GuiPointerStatesUnityEvent _guiPointerStateChanged = default;
		
		[Tooltip("Raised when the Gui Pointer State changes and the conditions are met")]
		[SerializeField] private GuiPointerStatesCompareUnityEvent[] _guiPointerStateChangedCompare = default;


		public BoolPlusUnityEvent PointerEnabledChanged => _pointerEnabledChanged;

		public GuiPointerStatesUnityEvent GuiPointerStateChanged => _guiPointerStateChanged;


		public string PointerName => name;
		
		public LayerMask LayerMask => _layerMask;

		public float MaxRaycastDistance => _maxRaycastDistance;

		public bool ButtonHeld => GetButtonHeld();

		public BasicPose PointerPose => GetPointerPose();

		private readonly BasicEvent<GuiPointerEventData> _pointerEventDataChanged = new BasicEvent<GuiPointerEventData>();
		public BasicEvent<GuiPointerEventData> PointerEventDataChanged => _pointerEventDataChanged;
		

		private bool _pointerEnabled;
		public bool PointerEnabled
		{
			get => _pointerEnabled;
			private set
			{
				if (!enabled || !_allowPointerEnabled) value = false;
				if (_pointerEnabled == value) return;
				_pointerEnabled = value;
				PointerEnabledUpdated(_pointerEnabled);
			}
		}
		
		private bool _allowPointerEnabled = true;
		protected bool AllowPointerEnabled
		{
			get => _allowPointerEnabled;
			set
			{
				if (_allowPointerEnabled == value) return;
				_allowPointerEnabled = value;
				PointerEnabled = _allowPointerEnabled;
			}
		}

		
		private GuiPointerStates _guiPointerState;
		public GuiPointerStates GuiPointerState
		{
			get => _guiPointerState;
			private set
			{
				if (_guiPointerState == value) return;
				_guiPointerState = value;
				GuiPointerStateUpdated(_guiPointerState);
			}
		}
		
		private GuiPointerEventData _pointerEventData;
		public GuiPointerEventData PointerEventData => _pointerEventData;
		
		private bool _previousButtonHeld;
		

		// ****************************************  METHODS  ****************************************\\
		
		
		//***** abstract *****\\

		protected abstract bool GetButtonHeld();
		protected abstract BasicPose GetPointerPose();
		
		
		//***** mono *****\\
		
		protected virtual void OnValidate()
		{
			_maxRaycastDistance = Mathf.Max(0, _maxRaycastDistance);
		}
		
		protected virtual void Awake()
		{
			GuiPointerInputModule.GetOrCreateInstance().AddGuiPointer(this);
		}
		
		protected virtual void OnEnable()
		{
			PointerEnabled = true;
			
			_pointerEventDataChanged.Subscribe(OnPointerEventDataChanged);
		}

		protected virtual void OnDisable()
		{
			PointerEnabled = false;
			
			_pointerEventDataChanged.Unsubscribe(OnPointerEventDataChanged);
		}
		
		protected virtual void Start()
		{
			PointerEnabledUpdated(_pointerEnabled);
			GuiPointerStateUpdated(_guiPointerState);
		}
		

		protected virtual void Update()
		{
#if UNITY_EDITOR
			if (_pointerEventData == null) return;

			var ray = _pointerEventData.Ray;
			var rayDistance = _pointerEventData.ProjectedDistance;
			var distance = rayDistance > 0 ? rayDistance : _maxRaycastDistance;

			Debug.DrawRay(ray.origin, ray.direction * distance, Color.blue);
			Debug.DrawRay(_pointerEventData.ProjectedWorldPosition, _pointerEventData.DeltaWorldPosition * -1f, Color.cyan);
#endif
		}
		
		protected virtual void OnDestroy()
		{
			if (GuiPointerInputModule.Instance == null) return;
			GuiPointerInputModule.Instance.RemoveGuiPointer(this);
		}
		

		//***** callbacks *****\\
		
		protected virtual void OnPointerEventDataChanged(GuiPointerEventData pointerEventData)
		{
			_pointerEventData = pointerEventData;

			// Change the pointer state
			if (pointerEventData.pointerDrag != null)
			{
				GuiPointerState = GuiPointerStates.DraggingTarget;
			}
			else if (pointerEventData.pointerPress != null)
			{
				GuiPointerState = GuiPointerStates.PressedTarget;
			}
			else if (pointerEventData.pointerEnter != null)
			{
				GuiPointerState = GuiPointerStates.HoverTarget;
			}
			else
			{
				GuiPointerState = GuiPointerStates.NoTarget;
			}
		}
		

		//***** property updated *****\\
		
		protected virtual void PointerEnabledUpdated(bool pointerEnabled)
		{
			if (!pointerEnabled)
			{
				GuiPointerState = GuiPointerStates.NoTarget;
			}

			_pointerEnabledChanged.Raise(pointerEnabled);
		}
		
		protected virtual void GuiPointerStateUpdated(GuiPointerStates guiPointerState)
		{
			_guiPointerStateChanged.Invoke(guiPointerState);

			foreach (var changedUnityEvent in _guiPointerStateChangedCompare)
			{
				changedUnityEvent.SetCurrentValue(guiPointerState);
			}
		}

	}
}