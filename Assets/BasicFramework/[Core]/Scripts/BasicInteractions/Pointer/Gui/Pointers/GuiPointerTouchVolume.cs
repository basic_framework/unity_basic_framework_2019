﻿using System;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	public class GuiPointerTouchVolume : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The color to use to draw the wire volume sphere")]
		[SerializeField] private Color _editorGizmoColor = Color.magenta;
		
		[Tooltip("The origin position of the touch and scale factor if radius is relative")]
		[SerializeField] private Transform _transformOrigin = default;

		[Tooltip("Should the radius be relative to the transform origin scale")]
		[SerializeField] private bool _radiusIsRelative = default;

		[Tooltip("The absolute value of the radius, i.e. radius before scaling is applied")]
		[SerializeField] private float _absoluteRadius = 0.01f;
		
		
		public Vector3 Position => _transformOrigin.position;
		
		public float Radius
		{
			get
			{
				if (!_radiusIsRelative) return _absoluteRadius;

				var lossyScale = _transformOrigin.lossyScale;
				var scaler = lossyScale.x;
				scaler = Math.Max(scaler, lossyScale.y);
				scaler = Math.Max(scaler, lossyScale.z);
				return _absoluteRadius * scaler;
			}
		}
		

		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_transformOrigin = transform;
		}

		private void OnValidate()
		{
			_absoluteRadius = Math.Max(0, _absoluteRadius);
		}
		
		public void OnDrawGizmosSelected()
		{
			if (_transformOrigin == null) return;

			var previousColor = Gizmos.color;
			Gizmos.color = _editorGizmoColor;
			
			Gizmos.DrawWireSphere(Position, Radius);
			
			Gizmos.color = previousColor;
		}
		
	}
}