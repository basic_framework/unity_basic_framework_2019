﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
    [CustomEditor(typeof(GuiPointerBasic))][CanEditMultipleObjects]
    public class GuiPointerBasicEditor : GuiPointerBaseEditor<GuiPointerBasic>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override void DrawPropertyFields()
        {
	        base.DrawPropertyFields();
	        
	        var selectInput = serializedObject.FindProperty("_selectInput");
	        
            var pointerPoseSource = serializedObject.FindProperty("_pointerPoseSource");
            var offsetPosition = serializedObject.FindProperty("_offsetPosition");
            var offsetRotation = serializedObject.FindProperty("_offsetRotation");
            
            var lerpRatePosition = serializedObject.FindProperty("_lerpRatePosition");
            var lerpRateRotation = serializedObject.FindProperty("_lerpRateRotation");
			
            Space();
            SectionHeader($"{nameof(GuiPointerBasic).CamelCaseToHumanReadable()} Properties");
			
            EditorGUILayout.PropertyField(selectInput);
            
            EditorGUILayout.PropertyField(pointerPoseSource);
            
            Space();
            EditorGUILayout.PropertyField(offsetPosition);
            EditorGUILayout.PropertyField(offsetRotation);
            
            Space();
            EditorGUILayout.PropertyField(lerpRatePosition);
            EditorGUILayout.PropertyField(lerpRateRotation);
	    }

    }
	
}