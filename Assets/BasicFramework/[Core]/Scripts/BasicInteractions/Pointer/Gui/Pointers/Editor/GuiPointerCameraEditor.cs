﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerCamera))][CanEditMultipleObjects]
	public class GuiPointerCameraEditor : GuiPointerBaseEditor<GuiPointerCamera> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var camera = serializedObject.FindProperty("_camera");
			var selectInput = serializedObject.FindProperty("_selectInput");
			
			Space();
			SectionHeader($"{nameof(GuiPointerCamera).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(camera);
			EditorGUILayout.PropertyField(selectInput);
			
		}
		
	
	}
}