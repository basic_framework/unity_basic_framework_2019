﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	public abstract class GuiPointerBaseEditor<T> : BasicBaseEditor<T>
		where T : GuiPointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _pointerStateCompareReorderableList;
		
		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_maxRaycastDistance").isExpanded;
			set => serializedObject.FindProperty("_maxRaycastDistance").isExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void OnEnable()
		{
			base.OnEnable();
			
			var guiPointerStateChangedCompare = serializedObject.FindProperty("_guiPointerStateChangedCompare");

			_pointerStateCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, guiPointerStateChangedCompare, true);
		}
		
		protected override void DrawTopOfInspectorMessage()
		{
			GuiPointerEditorUtility.DrawAddGuiPointerInputModuleButton();
//			var guiPointerInputModule = FindObjectOfType<GuiPointerInputModule>();
//
//			if (guiPointerInputModule != null) return;
//
//			var inputModuleName = nameof(GuiPointerInputModule).CamelCaseToHumanReadable();
//			
//			EditorGUILayout.HelpBox($"Could not find \"{inputModuleName}\" component in scene (it may be on an inactive GameObject).\n" +
//			                        $"\"{TypedTarget.GetType().Name}\" will not work without this component.", MessageType.Warning);
//			
//			if(!GUILayout.Button($"Create \"{inputModuleName}\" GameObject")) return;
//			
//			var inputModule = new GameObject($"event_system_gui_pointer").AddComponent<GuiPointerInputModule>();
//			Undo.RegisterCreatedObjectUndo(inputModule.gameObject, $"created {inputModuleName}");
		}

		protected override void DrawDebugFields()
		{
			var pointerEventData = TypedTarget.PointerEventData;

			var hoverGameObjectName = pointerEventData == null
				? "NULL"
				: pointerEventData.pointerEnter == null ? "Nothing" : pointerEventData.pointerEnter.name;
			
			var pressGameObjectName = pointerEventData == null
				? "NULL"
				: pointerEventData.pointerPress == null ? "Nothing" : pointerEventData.pointerPress.name;
			
			var dragGameObjectName = pointerEventData == null
				? "NULL"
				: pointerEventData.pointerDrag == null ? "Nothing" : pointerEventData.pointerDrag.name;
			
			EditorGUILayout.LabelField("Pointer Enabled", TypedTarget.PointerEnabled.ToString());
			EditorGUILayout.LabelField("Pointer State", TypedTarget.GuiPointerState.ToString());
			EditorGUILayout.LabelField("Pointer Hover", hoverGameObjectName);
			EditorGUILayout.LabelField("Pointer Press", pressGameObjectName);
			EditorGUILayout.LabelField("Pointer Drag", dragGameObjectName);
		}

		protected override void DrawPropertyFields()
		{
			//base.DrawPropertyFields();
			
			var layerMask = serializedObject.FindProperty("_layerMask");
			var maxRaycastDistance = serializedObject.FindProperty("_maxRaycastDistance");

			Space();
			SectionHeader($"{nameof(GuiPointerBase).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(layerMask);
			EditorGUILayout.PropertyField(maxRaycastDistance);
		}
		
		protected override void DrawEventFields()
		{
			SectionHeader($"{nameof(GuiPointerBase).CamelCaseToHumanReadable()} Events");
			
			var pointerEnabledChanged = serializedObject.FindProperty("_pointerEnabledChanged");
//			var pointerEventDataChanged = serializedObject.FindProperty("_pointerEventDataChanged");
			var guiPointerStateChanged = serializedObject.FindProperty("_guiPointerStateChanged");
			
			EditorGUILayout.PropertyField(pointerEnabledChanged);
			Space();
//			EditorGUILayout.PropertyField(pointerEventDataChanged);
			EditorGUILayout.PropertyField(guiPointerStateChanged);
			
			_pointerStateCompareReorderableList.DoLayoutList();
		}
		
	}
}