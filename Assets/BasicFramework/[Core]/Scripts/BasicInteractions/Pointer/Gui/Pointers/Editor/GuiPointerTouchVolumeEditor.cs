﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[CustomEditor(typeof(GuiPointerTouchVolume))]
	public class GuiPointerTouchVolumeEditor : BasicBaseEditor<GuiPointerTouchVolume>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var editorGizmoColor = serializedObject.FindProperty("_editorGizmoColor");
			var absoluteRadius  = serializedObject.FindProperty("_absoluteRadius");
			
			EditorGUILayout.PropertyField(editorGizmoColor);
			absoluteRadius.isExpanded = EditorGUILayout.Toggle("Hide Radius Handle", absoluteRadius.isExpanded);
			EditorGUILayout.LabelField("Scaled Radius", MathUtility.RoundToDecimalPlace(TypedTarget.Radius, 3).ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var transformOrigin  = serializedObject.FindProperty("_transformOrigin");
			var absoluteRadius  = serializedObject.FindProperty("_absoluteRadius");
			var radiusIsRelative  = serializedObject.FindProperty("_radiusIsRelative");
			
			Space();
			SectionHeader($"{nameof(GuiPointerTouchVolume).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformOrigin);
			EditorGUILayout.PropertyField(radiusIsRelative);
			EditorGUILayout.PropertyField(absoluteRadius);
		}

		protected override void DrawSceneGUI(SerializedObject sceneGuiSerializedObject)
		{
			base.DrawSceneGUI(sceneGuiSerializedObject);

			if(sceneGuiSerializedObject.FindProperty("_transformOrigin").objectReferenceValue == null) return;
			
			var absoluteRadius  = sceneGuiSerializedObject.FindProperty("_absoluteRadius");
			if(absoluteRadius.isExpanded) return;
			
			var position = TypedTarget.Position;

			var scale = absoluteRadius.floatValue;
			var scaleHandle = Handles.ScaleValueHandle
				(
				scale, 
				position, 
				Quaternion.identity, 
				HandleUtility.GetHandleSize(position),
				Handles.DotHandleCap, 
				0.01f
				);

			var scaleDelta = Math.Abs(scale - scaleHandle);
			if (scaleDelta > Mathf.Epsilon)
			{
				absoluteRadius.floatValue = scaleHandle;
			}
		}
	}
}