﻿using BasicFramework.Core.Utility.ExtensionMethods;

using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	public static class GuiPointerEditorUtility
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		
	
		// ****************************************  METHODS  ****************************************\\

		public static void DrawAddGuiPointerInputModuleButton()
		{
			var guiPointerInputModule = Object.FindObjectOfType<GuiPointerInputModule>();

			if (guiPointerInputModule != null) return;

			var inputModuleName = nameof(GuiPointerInputModule).CamelCaseToHumanReadable();
			
			EditorGUILayout.HelpBox(
				$"Could not find \"{inputModuleName}\" component in scene (it may be on an inactive GameObject).", 
				MessageType.Warning);
			
			if(!GUILayout.Button($"Create \"{inputModuleName}\" GameObject")) return;
			
			var inputModule = new GameObject($"event_system_gui_pointer").AddComponent<GuiPointerInputModule>();
			Undo.RegisterCreatedObjectUndo(inputModule.gameObject, $"created {inputModuleName}");
		}
	
	}
}