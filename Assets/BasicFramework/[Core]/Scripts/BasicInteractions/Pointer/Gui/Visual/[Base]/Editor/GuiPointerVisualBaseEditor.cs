﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	public abstract class GuiPointerVisualBaseEditor<TGuiPointer, TGuiPointerUtility> : BasicBaseEditor<TGuiPointerUtility>
		where TGuiPointer : GuiPointerBase
		where TGuiPointerUtility : GuiPointerVisualBase<TGuiPointer>
	
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private ReorderableList _pointerStateCompareReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var guiPointerStateChangedCompare = serializedObject.FindProperty("_guiPointerStateChangedCompare");

			_pointerStateCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, guiPointerStateChangedCompare, true);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var guiPointer = serializedObject.FindProperty("_guiPointer");
			
			Space();
			SectionHeader($"{nameof(GuiPointerVisualBase<TGuiPointer>).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(guiPointer);
		}

		 protected override void DrawEventFields()
		{
			var guiPointerEnabledChanged = serializedObject.FindProperty("_guiPointerEnabledChanged");
			
			Space();
			SectionHeader($"{nameof(GuiPointerVisualBase<TGuiPointer>).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(guiPointerEnabledChanged);
			
			Space(8);
			_pointerStateCompareReorderableList.DoLayoutList();
		}
		
	}
}