﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
    [CustomEditor(typeof(GuiPointerVisualReticleTouch))]
    public class GuiPointerVisualReticleTouchEditor : GuiPointerVisualBaseEditor<GuiPointerTouch, GuiPointerVisualReticleTouch>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            var transformReticle = serializedObject.FindProperty("_transformReticle");
            var rangeReticleScale = serializedObject.FindProperty("_rangeReticleScale");
            var reverseScaleDirection = serializedObject.FindProperty("_reverseScaleDirection");
			
            Space();
            SectionHeader($"{GetType().Name.CamelCaseToHumanReadable()} Properties");
			
            BasicEditorGuiLayout.PropertyFieldNotNull(transformReticle);
            EditorGUILayout.PropertyField(rangeReticleScale);
            EditorGUILayout.PropertyField(reverseScaleDirection);
        }
		
    }
}