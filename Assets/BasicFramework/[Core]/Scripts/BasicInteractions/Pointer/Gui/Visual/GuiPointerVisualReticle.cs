﻿using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Visual/Gui Pointer Visual Reticle")]
	public class GuiPointerVisualReticle : GuiPointerVisualBase<GuiPointerBase>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The transform to position at the gui hit position, the up direction will be aligned with the gui normal")]
		[SerializeField] private Transform _transformReticle = default;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void Reset()
		{
			base.Reset();
			
			_transformReticle = transform;
		}

		// Places reticle at raycast hit position with up in line with normal
		protected override void OnPointerEventDataChanged(GuiPointerEventData pointerEventData)
		{
			base.OnPointerEventDataChanged(pointerEventData);
			
			if (pointerEventData == null) return;
			//if(GuiPointer.GuiPointerState == GuiPointerStates.NoTarget) return;
			
			var raycastResult = pointerEventData.pointerCurrentRaycast;

			_transformReticle.position = pointerEventData.ProjectedWorldPosition;
			_transformReticle.rotation = Quaternion.FromToRotation(_transformReticle.up, raycastResult.worldNormal) * _transformReticle.rotation;
		}
		
		
	}
}