﻿using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_POINTER_VISUALS)]
	public abstract class GuiPointerVisualBase<TGuiPointer> : MonoBehaviour
		where TGuiPointer : GuiPointerBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The gui pointer that this is referencing")]
		[SerializeField] private TGuiPointer _guiPointer = default;
		
		[Tooltip("Raised when the gui pointer is enabled/disabled")]
		[SerializeField] private BoolPlusUnityEvent _guiPointerEnabledChanged = default;
		
		[Tooltip("Raised when the gui pointer state changes and the condition is met")]
		[SerializeField] private GuiPointerStatesCompareUnityEvent[] _guiPointerStateChangedCompare = default;

		
		public TGuiPointer GuiPointerBehaviour
		{
			get => _guiPointer;
			private set
			{
				if (_guiPointer == value) return;
				var prevGuiPointer = _guiPointer;
				_guiPointer = value;
				
				if (!enabled) return;
				SetGuiPointerEventListeners(prevGuiPointer, false);
				SetGuiPointerEventListeners(_guiPointer, true);
			}
		}

		
		public BoolPlusUnityEvent GuiPointerEnabledChanged => _guiPointerEnabledChanged;

		public GuiPointerStatesCompareUnityEvent[] GuiPointerStateChangedCompare => _guiPointerStateChangedCompare;

		public GuiPointerStates GuiPointerState => _guiPointer.GuiPointerState;
		public bool GuiPointerEnabled => _guiPointer.PointerEnabled;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected virtual void Reset()
		{
			_guiPointer = GetComponentInParent<TGuiPointer>();
		}

		protected virtual void OnEnable()
		{
			SetGuiPointerEventListeners(_guiPointer, true);
		}
		
		protected virtual void OnDisable()
		{
			SetGuiPointerEventListeners(_guiPointer, false);
		}

		protected virtual void Start()
		{
			OnGuiPointerEnabledChanged(_guiPointer.PointerEnabled);
			OnGuiPointerStateChanged(_guiPointer.GuiPointerState);
		}

		protected virtual void SetGuiPointerEventListeners(TGuiPointer guiPointer, bool listen)
		{
			if (guiPointer == null) return;
			
			if (listen)
			{
				OnPointerEventDataChanged(guiPointer.PointerEventData);
				OnGuiPointerEnabledChanged(guiPointer.PointerEnabled);
				OnGuiPointerStateChanged(guiPointer.GuiPointerState);
				
				guiPointer.PointerEventDataChanged.Subscribe(OnPointerEventDataChanged);
				guiPointer.PointerEnabledChanged.Value.AddListener(OnGuiPointerEnabledChanged);
				guiPointer.GuiPointerStateChanged.AddListener(OnGuiPointerStateChanged);
			}
			else
			{
				_guiPointer.PointerEventDataChanged.Unsubscribe(OnPointerEventDataChanged);
				_guiPointer.PointerEnabledChanged.Value.RemoveListener(OnGuiPointerEnabledChanged);
				_guiPointer.GuiPointerStateChanged.RemoveListener(OnGuiPointerStateChanged);
			}
		}
		
		protected virtual void OnGuiPointerEnabledChanged(bool guiPointerEnabled)
		{
			_guiPointerEnabledChanged.Raise(guiPointerEnabled);
		}
		
		protected virtual  void OnGuiPointerStateChanged(GuiPointerStates guiPointerState)
		{
			foreach (var changedUnityEvent in _guiPointerStateChangedCompare)
			{
				changedUnityEvent.SetCurrentValue(guiPointerState);
			}
		}
		
		protected virtual void OnPointerEventDataChanged(GuiPointerEventData pointerEventData){}
		
	}
}