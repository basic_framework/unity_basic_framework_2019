﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	// TODO: Make a base class for touch pointer similar to GuiPointerUtilityBase
	
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Visual/Gui Pointer Visual Reticle Touch")]
	public class GuiPointerVisualReticleTouch : GuiPointerVisualBase<GuiPointerTouch>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		// TODO: Add events similar to tween transitioner for Color and Vector 3 where user can specify min/max and tween

		[SerializeField] private Transform _transformReticle = default;
		
		[Tooltip("The range to scale the reticle transform, if you want to change direction make Min > Max")]
		[SerializeField] private FloatRange _rangeReticleScale = new FloatRange(0f, 1f);

		[Tooltip("Scale from Max to Min (Far to Close), rether than Min to Max")]
		[SerializeField] private bool _reverseScaleDirection = default;
		

		// ****************************************  METHODS  ****************************************\\
		
		protected override void Reset()
		{
			base.Reset();
			
			_transformReticle = transform;
		}
		
		
		//***** override *****\\

		protected override void SetGuiPointerEventListeners(GuiPointerTouch guiPointerTouch, bool listen)
		{
			base.SetGuiPointerEventListeners(guiPointerTouch, listen);
			
			if (guiPointerTouch == null) return;
			
			if (listen)
			{
				guiPointerTouch.TouchDistanceChanged.Value.AddListener(OnTouchDistanceChanged);
			}
			else
			{
				guiPointerTouch.TouchDistanceChanged.Value.RemoveListener(OnTouchDistanceChanged);
			}
		}

		// Places reticle at raycast hit position with up in line with normal
		protected override void OnPointerEventDataChanged(GuiPointerEventData pointerEventData)
		{
			base.OnPointerEventDataChanged(pointerEventData);
			
			if(pointerEventData == null) return;
			
			_transformReticle.position = pointerEventData.ProjectedWorldPosition;
			
			_transformReticle.rotation = 
				Quaternion.FromToRotation(
					_transformReticle.up, 
					pointerEventData.pointerCurrentRaycast.worldNormal) * _transformReticle.rotation;
		}
		
//		protected override void OnGuiPointerEnabledChanged(bool guiPointerEnabled)
//		{
//			base.OnGuiPointerEnabledChanged(guiPointerEnabled);
//			
//			_transformReticle.gameObject.SetActive(guiPointerEnabled);
//		}

		private void OnTouchDistanceChanged(NormalizedFloat distanceNormalized)
		{
			var min = _reverseScaleDirection ? _rangeReticleScale.Min : _rangeReticleScale.Max;
			var max = _reverseScaleDirection ? _rangeReticleScale.Max : _rangeReticleScale.Min;
			_transformReticle.localScale = Vector3.one * distanceNormalized.MapToRange(min, max);
		}
		
	}
}