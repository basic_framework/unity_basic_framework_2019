﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer
{
	[AddComponentMenu("Basic Framework/Core/Gui/Pointer/Visual/Gui Pointer Visual Ray")]
	public class GuiPointerVisualRay : GuiPointerVisualBase<GuiPointerBase>
	{

		// **************************************** VARIABLES ****************************************\\

		//***** Mode *****\\
		
		[Tooltip("The method to use to display the ray")]
		[SerializeField] private RayVisualModes _rayVisualMode = default;

		[Tooltip("Raised when the ray visual mode changes")]
		[SerializeField] private RayVisualModesUnityEvent _rayVisualModeChanged = default;

		[Tooltip("Raised when the ray visual mode changes and the condition is met")]
		[SerializeField] private RayVisualModesCompareUnityEvent[] _rayVisualModeChangedCompare = default;
		
		
		//***** Transform *****\\
		
		[Tooltip("The transform that will be positioned at the ray start and pointed in the ray direction")]
		[SerializeField] private Transform _rayVisualTransform = default;
		
		[Tooltip("The up direction that will be used when setting the rotation of the transform")]
		[SerializeField] private Vector3 _rayVisualTransformWorldUp = Vector3.up;
		
		[Tooltip("Move the ray start position forward an amount. Useful if the start position is inside a controller or hand model")]
		[SerializeField] private float _rayStartPositionOffsetTransform = 0f;
		
		
		//***** Line Renderer *****\\
		
		[Tooltip("The line renderer that will be used to display the ray")]
		[SerializeField] private LineRenderer _lineRenderer = default;
		
		[Tooltip("The maximum length of the line renderer. " +
		         "Note: if the pointer raycast distance is less than this the distance will be clamped to that value")]
		[SerializeField] private float _maxRayDistance = 1f;
		
		[Tooltip("Move the ray start position forward an amount. Useful if the start position is inside a controller or hand model")]
		[SerializeField] private float _rayStartPositionOffsetLineRenderer = 0f;
		
		
		public RayVisualModes RayVisualMode
		{
			get => _rayVisualMode;
			set
			{
				if (_rayVisualMode == value) return;
				_rayVisualMode = value;
				RayVisualModeUpdated(_rayVisualMode);
			}
		}
		
		
		public Transform RayVisualTransform
		{
			get => _rayVisualTransform;
			set => _rayVisualTransform = value;
		}

		public Vector3 RayVisualTransformWorldUp
		{
			get => _rayVisualTransformWorldUp;
			set => _rayVisualTransformWorldUp = value;
		}

		public float RayStartPositionOffsetTransform
		{
			get => _rayStartPositionOffsetTransform;
			set
			{
				value = Mathf.Max(0, value);
				_rayStartPositionOffsetTransform = value;
			}
		}
		
		
		public LineRenderer LineRenderer
		{
			get => _lineRenderer;
			set => _lineRenderer = value;
		}

		public float MaxRayDistance
		{
			get => _maxRayDistance;
			set
			{
				value = Mathf.Max(0, value);
				_maxRayDistance = value;
				RayStartPositionOffsetLineRenderer = _rayStartPositionOffsetLineRenderer;
			}
		}

		public float RayStartPositionOffsetLineRenderer
		{
			get => _rayStartPositionOffsetLineRenderer;
			set
			{
				value = Mathf.Clamp(value, 0, _maxRayDistance);
				_rayStartPositionOffsetLineRenderer = value;
			}
		}

		
		private readonly Vector3[] _linePoints = new Vector3[2];
		

		// ****************************************  METHODS  ****************************************\\

		protected override void Reset()
		{
			base.Reset();
			
			_rayVisualTransform = transform;
			_lineRenderer = GetComponentInChildren<LineRenderer>();
		}

		private void OnValidate()
		{
			MaxRayDistance = _maxRayDistance;
			RayStartPositionOffsetTransform = _rayStartPositionOffsetTransform;
			RayStartPositionOffsetLineRenderer = _rayStartPositionOffsetLineRenderer;
		}

		protected override void Start()
		{
			base.Start();
			
			RayVisualModeUpdated(_rayVisualMode);
		}
		
		protected override void OnPointerEventDataChanged(GuiPointerEventData pointerEventData)
		{
			base.OnPointerEventDataChanged(pointerEventData);

			if (pointerEventData == null) return;

			var ray = pointerEventData.Ray;
			
			switch (_rayVisualMode)
			{
				case RayVisualModes.Transform:
					
					_rayVisualTransform.position = ray.origin + ray.direction * _rayStartPositionOffsetTransform;
					_rayVisualTransform.rotation =
						MathUtility.Direction2Quaternion(ray.direction, _rayVisualTransformWorldUp);
					
					break;
				
				case RayVisualModes.LineRenderer:
					
					var raycastResultDistance = pointerEventData.pointerCurrentRaycast.distance;  
					var rayDistance = raycastResultDistance > 0.0f
						? Mathf.Min(_maxRayDistance, raycastResultDistance)
						: _maxRayDistance;

					rayDistance = Mathf.Min(rayDistance, GuiPointerBehaviour.MaxRaycastDistance);
					rayDistance = Mathf.Max(0, rayDistance - _rayStartPositionOffsetLineRenderer);
					
					_linePoints[0] = ray.origin + ray.direction * _rayStartPositionOffsetLineRenderer;
					_linePoints[1] = _linePoints[0] + ray.direction * rayDistance;
					
					_lineRenderer.SetPositions(_linePoints);
					
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		
		private void RayVisualModeUpdated(RayVisualModes rayVisualMode)
		{
			_rayVisualModeChanged.Invoke(rayVisualMode);

			foreach (var changedUnityEvent in _rayVisualModeChangedCompare)
			{
				changedUnityEvent.SetCurrentValue(rayVisualMode);
			}
		}

		public void EditorRaise()
		{
			if (!Application.isPlaying) return;
			if (!Application.isEditor) return;
			RayVisualModeUpdated(_rayVisualMode);
		}
		
	}
}