﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerVisualRay))][CanEditMultipleObjects]
	public class GuiPointerVisualRayEditor : GuiPointerVisualBaseEditor<GuiPointerBase, GuiPointerVisualRay>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private ReorderableList _rayModeCompareReorderableList;

		private bool _raiseVisualModeChangedEvent;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var rayVisualModeChangedCompare = serializedObject.FindProperty("_rayVisualModeChangedCompare");

			_rayModeCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, rayVisualModeChangedCompare, true);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var rayVisualMode = serializedObject.FindProperty("_rayVisualMode");
			
			Space();
			SectionHeader($"{nameof(GuiPointerVisualRay)} Properties");
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(rayVisualMode);
			if (EditorGUI.EndChangeCheck())
			{
				_raiseVisualModeChangedEvent = true;
			}
			
			Space();

			var rayVisualModeEnum =
				EnumUtility.EnumNameToEnumValue<RayVisualModes>(
					rayVisualMode.enumNames[rayVisualMode.enumValueIndex]);

			switch (rayVisualModeEnum)
			{
				case RayVisualModes.Transform:
					
					var rayVisualTransform = serializedObject.FindProperty("_rayVisualTransform");
					var rayVisualTransformWorldUp = serializedObject.FindProperty("_rayVisualTransformWorldUp");
					var rayStartPositionOffsetTransform = serializedObject.FindProperty("_rayStartPositionOffsetTransform");
					
					BasicEditorGuiLayout.PropertyFieldNotNull(rayVisualTransform);
					BasicEditorGuiLayout.Vector3DirectionDropdown(rayVisualTransformWorldUp);
					EditorGUILayout.PropertyField(rayStartPositionOffsetTransform);
					
					break;
				
				case RayVisualModes.LineRenderer:
					
					var rayStartPositionOffsetLineRenderer = serializedObject.FindProperty("_rayStartPositionOffsetLineRenderer");
					var lineRenderer = serializedObject.FindProperty("_lineRenderer");
					var maxRayDistance = serializedObject.FindProperty("_maxRayDistance");
					
					BasicEditorGuiLayout.PropertyFieldNotNull(lineRenderer);
					
					EditorGUILayout.PropertyField(maxRayDistance);
					maxRayDistance.floatValue = Mathf.Max(0, maxRayDistance.floatValue);
					
					rayStartPositionOffsetLineRenderer.floatValue = EditorGUILayout.Slider(GetGuiContent(rayStartPositionOffsetLineRenderer),
						rayStartPositionOffsetLineRenderer.floatValue, 0, maxRayDistance.floatValue);
			
					rayStartPositionOffsetLineRenderer.floatValue =
						Mathf.Clamp(rayStartPositionOffsetLineRenderer.floatValue, 0, maxRayDistance.floatValue);
					
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var rayVisualModeChanged = serializedObject.FindProperty("_rayVisualModeChanged");

			Space();
			SectionHeader($"{nameof(GuiPointerVisualRay)} Events");
			

			EditorGUILayout.PropertyField(rayVisualModeChanged);
			_rayModeCompareReorderableList.DoLayoutList();
		}

		protected override void AfterApplyingModifiedProperties()
		{
			base.AfterApplyingModifiedProperties();

			if (!Application.isPlaying) return;
			if (!_raiseVisualModeChangedEvent) return;
			_raiseVisualModeChangedEvent = false;

			foreach (var typedTarget in TypedTargets)
			{
				typedTarget.EditorRaise();
			}
		}
		
	}
}