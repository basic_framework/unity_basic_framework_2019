﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.BasicGui.Pointer.Editor
{
	[CustomEditor(typeof(GuiPointerVisualReticle))][CanEditMultipleObjects]
	public class GuiPointerVisualReticleEditor : GuiPointerVisualBaseEditor<GuiPointerBase, GuiPointerVisualReticle>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			SectionHeader($"{nameof(GuiPointerVisualReticle).CamelCaseToHumanReadable()} Properties");
			
			var transformReticle = serializedObject.FindProperty("_transformReticle");

			BasicEditorGuiLayout.PropertyFieldNotNull(transformReticle);
		}
		
	}
}