﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(RangedFloat))]
	public class RangedFloatPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const float MIN_MAX_FIELD_WIDTH_PERCENT = 0.2f;
		
		private const float FIX_RANGE_BUTTON_WIDTH = 20f;
		
		private readonly GUIContent _swapGuiContent = new GUIContent("⇄", "Fix the Range by swapping the min and max");
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var min =  property.FindPropertyRelative("_min");
			var max = property.FindPropertyRelative("_max");
			var range = property.FindPropertyRelative("_range");
		    
			var value = property.FindPropertyRelative("_value");
			var normalizedValue = property.FindPropertyRelative("_normalizedValue");
			
		    position = EditorGUI.PrefixLabel(position, label);
		    
		    var tempRect 	= new Rect(position);
		    var expandableWidth = position.width - FIX_RANGE_BUTTON_WIDTH - BUFFER_WIDTH * 3;

		    var valueFieldWidth = MIN_MAX_FIELD_WIDTH_PERCENT * expandableWidth;
		    var sliderWidth = (1 - MIN_MAX_FIELD_WIDTH_PERCENT * 2) * expandableWidth;
		    
		    tempRect.width = valueFieldWidth;
		    min.floatValue = EditorGUI.FloatField(tempRect, min.floatValue);
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    
		    tempRect.width = sliderWidth;
		    var prevFieldWidth = EditorGUIUtility.fieldWidth;
		    EditorGUIUtility.fieldWidth = valueFieldWidth;
		    value.floatValue = EditorGUI.Slider(tempRect, value.floatValue, min.floatValue, max.floatValue);
		    EditorGUIUtility.fieldWidth = prevFieldWidth;
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    
		    tempRect.width = valueFieldWidth;
		    max.floatValue = EditorGUI.FloatField(tempRect, max.floatValue);
		    tempRect.x += tempRect.width + BUFFER_WIDTH;

		    GUI.enabled = PrevGuiEnabled && range.floatValue < 0;
		    tempRect.width = FIX_RANGE_BUTTON_WIDTH;
		    if (GUI.Button(tempRect, _swapGuiContent))
		    {
			    GUI.FocusControl("");
			    var tempMin = min.floatValue;
			    min.floatValue = max.floatValue;
			    max.floatValue = tempMin;
			    value.floatValue = Mathf.Clamp(value.floatValue, min.floatValue, max.floatValue);
		    }
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    GUI.enabled = PrevGuiEnabled;
		    
		    range.floatValue = max.floatValue - min.floatValue;
		    
		    normalizedValue.floatValue = 
			    Math.Abs(range.floatValue) < float.Epsilon
			    ? 1f 
			    : MathUtility.Map(value.floatValue, min.floatValue, max.floatValue, 0, 1);
		}
		
	}
}