﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(RangedInt))]
	public class RangedIntPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const float MIN_MAX_FIELD_WIDTH_PERCENT = 0.2f;
		
		private const float FIX_RANGE_BUTTON_WIDTH = 20f;
		
		private readonly GUIContent _swapGuiContent = new GUIContent("⇄", "Fix the Range by swapping the min and max");
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var min =  property.FindPropertyRelative("_min");
			var max = property.FindPropertyRelative("_max");
			var range = property.FindPropertyRelative("_range");
		    
			var value = property.FindPropertyRelative("_value");
			var normalizedValue = property.FindPropertyRelative("_normalizedValue");
			
		    position = EditorGUI.PrefixLabel(position, label);
		    
		    var tempRect 	= new Rect(position);
		    var expandableWidth = position.width - FIX_RANGE_BUTTON_WIDTH - BUFFER_WIDTH * 3;

		    var valueFieldWidth = MIN_MAX_FIELD_WIDTH_PERCENT * expandableWidth;
		    var sliderWidth = (1 - MIN_MAX_FIELD_WIDTH_PERCENT * 2) * expandableWidth;
		    
		    tempRect.width = valueFieldWidth;
		    min.intValue = EditorGUI.IntField(tempRect, min.intValue);
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    
		    tempRect.width = sliderWidth;
		    var prevFieldWidth = EditorGUIUtility.fieldWidth;
		    EditorGUIUtility.fieldWidth = valueFieldWidth;
		    value.intValue = EditorGUI.IntSlider(tempRect, value.intValue, min.intValue, max.intValue);
		    EditorGUIUtility.fieldWidth = prevFieldWidth;
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    
		    tempRect.width = valueFieldWidth;
		    max.intValue = EditorGUI.IntField(tempRect, max.intValue);
		    tempRect.x += tempRect.width + BUFFER_WIDTH;

		    GUI.enabled = PrevGuiEnabled && range.intValue < 0;
		    tempRect.width = FIX_RANGE_BUTTON_WIDTH;
		    if (GUI.Button(tempRect, _swapGuiContent))
		    {
			    GUI.FocusControl("");
			    var tempMin = min.intValue;
			    min.intValue = max.intValue;
			    max.intValue = tempMin;
			    value.intValue = Mathf.Clamp(value.intValue, min.intValue, max.intValue);
		    }
		    tempRect.x += tempRect.width + BUFFER_WIDTH;
		    GUI.enabled = PrevGuiEnabled;
		    
		    range.intValue = max.intValue - min.intValue;
		    
		    normalizedValue.floatValue = 
			   range.intValue == 0
			    ? 1f 
			    : MathUtility.Map(value.intValue, min.intValue, max.intValue, 0f, 1f);
		}
		
	}
}