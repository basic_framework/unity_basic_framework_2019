﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(IntRange))]
	public class IntRangePropertyDrawer : BasicBasePropertyDrawer
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const float MIN_MAX_LABEL_WIDTH = 35f;

		private const float FIX_RANGE_BUTTON_WIDTH = 20f;

		private readonly GUIContent _swapGuiContent = new GUIContent("⇄", "Fix the Range by swapping the min and max");

		
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position = EditorGUI.PrefixLabel(position, label);
			
			var min = property.FindPropertyRelative("_min");
			var max = property.FindPropertyRelative("_max");
			var range = property.FindPropertyRelative("_range");

			var tempRect = position;
			var valueWidth = (position.width - FIX_RANGE_BUTTON_WIDTH) * 0.5f - BUFFER_WIDTH;

			tempRect.width = valueWidth;
			
			var prevLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = MIN_MAX_LABEL_WIDTH;

			EditorGUI.PropertyField(tempRect, min);
			tempRect.x += tempRect.width + BUFFER_WIDTH;
			
			EditorGUI.PropertyField(tempRect, max);
			tempRect.x += tempRect.width + BUFFER_WIDTH;
			
			GUI.enabled = PrevGuiEnabled && range.intValue < 0;
			tempRect.width = FIX_RANGE_BUTTON_WIDTH;
			if (GUI.Button(tempRect, _swapGuiContent))
			{
				GUI.FocusControl("");
				var tempMin = min.intValue;
				min.intValue = max.intValue;
				max.intValue = tempMin;
			}
			GUI.enabled = PrevGuiEnabled;
			
			range.intValue = max.intValue - min.intValue;
			
			EditorGUIUtility.labelWidth = prevLabelWidth;
		}
		
	}
}