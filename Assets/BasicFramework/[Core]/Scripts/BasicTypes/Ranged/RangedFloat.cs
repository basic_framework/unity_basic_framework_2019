﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct RangedFloat : IRangedValue<float>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		public static readonly RangedFloat Default = new RangedFloat(-1, 0, 1);
		
		//***** FloatRange *****\\
		
		[SerializeField] private float _min;
		[SerializeField] private float _max;
		[SerializeField] private float _range;
		
		public float Min
		{
			get => _min;
			set
			{
				_min = value;
				if (_min > _max) { _max = _min; }
				
				_range = _max - _min;
				_value = Mathf.Clamp(_value, _min, _max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}
		
		public float Max
		{
			get => _max;
			set
			{
				_max = value;
				if (_max < _min) { _min = _max; }
				
				_range = _max - _min;
				_value = Mathf.Clamp(_value, _min, _max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}

		
		public float Range => _range;


		//***** RangedFloat *****\\
		
		[SerializeField] private float _value;
		[SerializeField] private float _normalizedValue;
		
		public float Value
		{
			get => _value;
			set
			{
				_value = Mathf.Clamp(value, Min, Max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}

		public float NormalizedValue
		{
			get => _normalizedValue;
			set
			{
				_normalizedValue = Mathf.Clamp01(value);
				_value = NormalizedToRangedValue(_normalizedValue);
			}
		}

	
		// ****************************************  METHODS  ****************************************\\
		
		public RangedFloat(float min, float value, float max)
		{
			if (min > max) { max = min; }
			
			_min = min;
			_max = max;
			_range = max - min;
			
			_value = Mathf.Clamp(value, _min, _max);
			
			_normalizedValue = 
				Math.Abs(_range) < float.Epsilon
				? 1f
				: MathUtility.Map(_value, _min, _max, 0, 1);
		}
		
		/// <summary>Determines whether the passed value is within the range</summary>
		/// <param name="value">The value to test</param>
		/// <returns>If value is within range</returns>
		public bool WithinRange(float value)
		{
			return value >= _min && value <= _max;
		}
		
		/// <summary>Clamps the passed value to the range</summary>
		/// <param name="value">The value to clamp</param>
		/// <returns>The value clamped to the range</returns>
		public float ClampToRange(float value)
		{
			return Mathf.Clamp(value, _min, _max);
		}

		/// <summary>
		/// Calculates the ranged value given the normalized value (0 to 1).
		/// Example. Min = -5, Max = 5, NormalizedValue = 0.75, returns 2.5 
		/// </summary>
		/// <param name="normalizedValue">Value between 0 to 1</param>
		/// <returns>The ranged value given the normalized value</returns>
		public float NormalizedToRangedValue(float normalizedValue)
		{
			normalizedValue = Mathf.Clamp01(normalizedValue);
			return Math.Abs(_range) < Mathf.Epsilon ? Max : MathUtility.Map(normalizedValue, 0, 1, Min, Max);
		}

		/// <summary>
		/// Calculates the normalized value given the ranged value (Min to Max)
		/// Example. Min = -5, Max = 5, RangedValue = 2.5, returns 0.75 
		/// </summary>
		/// <param name="rangedValue">The ranged value, will be clamped from Min to Max</param>
		/// <returns>The normalized value given the ranged value</returns>
		public float RangedToNormalizedValue(float rangedValue)
		{
			rangedValue = ClampToRange(rangedValue);
			return Math.Abs(_range) < Mathf.Epsilon ? 1 : MathUtility.Map(rangedValue, Min, Max, 0, 1);
		}
		
		/// <summary>
		/// Returns a random float between the Min (inclusive) and Max (inclusive) value
		/// </summary>
		/// <returns></returns>
		public float GetRandomInRange()
		{
			return Random.Range(Min, Max);
		}
		
		public override string ToString()
		{
			return "Min: " + Min + ", Value: " + _value + ", Max: " + Max
			       + ", Norm: " + NormalizedValue + ", Range: " + Range;
			//return "Value: " + _value + ", Min: " + Min + ", Max: " + Max;
		}
	
	}
	
	[Serializable]
	public class RangedFloatUnityEvent : UnityEvent<RangedFloat>{}
	
	[Serializable]
	public class RangedFloatCompareUnityEvent : CompareValueUnityEventBase<RangedFloat>{}
	
	[Serializable]
	public class RangedFloatArrayUnityEvent : UnityEvent<RangedFloat[]>{}

}