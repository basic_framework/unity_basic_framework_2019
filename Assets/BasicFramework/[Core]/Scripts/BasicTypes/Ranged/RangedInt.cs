﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct RangedInt : IRangedValue<int>
	{
	
		// **************************************** VARIABLES ****************************************\\
		public static readonly RangedInt Default = new RangedInt(-100, 0, 100);
		
		//***** FloatRange *****\\
		
		[SerializeField] private int _min;
		[SerializeField] private int _max;
		[SerializeField] private int _range;
		
		public int Min
		{
			get => _min;
			set
			{
				_min = value;
				if (_min > _max) { _max = _min; }
				
				_range = _max - _min;
				_value = Mathf.Clamp(value, Min, Max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}
		
		public int Max
		{
			get => _max;
			set
			{
				_max = value;
				if (_max < _min) { _min = _max; }
				
				_range = _max - _min;
				_value = Mathf.Clamp(value, Min, Max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}
		
		public int Range => _range;
		
		//***** RangedInt *****\\
		
		[SerializeField] private int _value;
		[SerializeField] private float _normalizedValue;
		
		public int Value
		{
			get => _value;
			set
			{
				_value = Mathf.Clamp(value, Min, Max);
				_normalizedValue = RangedToNormalizedValue(_value);
			}
		}

		public float NormalizedValue
		{
			get => _normalizedValue;
			set
			{
				_normalizedValue = Mathf.Clamp01(value);
				_value = NormalizedToRangedValue(_normalizedValue);
			}
		}
		
	
		// ****************************************  METHODS  ****************************************\\
		
		public RangedInt(int min, int value, int max)
		{
			if (min > max) { max = min; }
			
			_min = min;
			_max = max;
			_range = _max - _min;
			
			_value = Mathf.Clamp(value, _min, _max);
			
			_normalizedValue = 
				_range == 0
				? 1f
				: MathUtility.Map(_value, _min, _max, 0, 1);
		}
		
		/// <summary>Determines whether the passed value is within the range</summary>
		/// <param name="value">The value to test</param>
		/// <returns>If value is within range</returns>
		public int ClampToRange(int value)
		{
			return Mathf.Clamp(value, _min, _max);
		}
		
		/// <summary>Clamps the passed value to the range</summary>
		/// <param name="value">The value to clamp</param>
		/// <returns>The value clamped to the range</returns>
		public bool WithinRange(int value)
		{
			return value >= _min && value <= _max;
		}
		
		/// <summary>
		/// Calculates the ranged value given the normalized value (0 to 1) and return the nearest int.
		/// Example. Max = 5, Min = -5, NormalizedValue = 0.725, returns 2 
		/// </summary>
		/// <param name="normalizedValue">Value between 0 to 1</param>
		/// <returns>The ranged value given the normalized value</returns>
		public int NormalizedToRangedValue(float normalizedValue)
		{
			normalizedValue = Mathf.Clamp01(normalizedValue);
			return _range == 0 ? Max : Mathf.RoundToInt(MathUtility.Map(normalizedValue, 0, 1, Min, Max));
		}
		
		/// <summary>
		/// Calculates the normalized value given the ranged value (Min to Max)
		/// Example. Min = -5, Max = 5, RangedValue = 2, returns 0.7 
		/// </summary>
		/// <param name="rangedValue">The ranged value, will be clamped from Min to Max</param>
		/// <returns>The normalized value given the ranged value</returns>
		public float RangedToNormalizedValue(int rangedValue)
		{
			rangedValue = ClampToRange(rangedValue);
			return _range == 0 ? 1f : MathUtility.Map(rangedValue, Min, Max, 0, 1);
		}
		
		/// <summary>
		/// Returns a random int between the Min (inclusive) and Max (exclusive) value
		/// </summary>
		/// <returns></returns>
		public int GetRandomInRange()
		{
			return Random.Range(Min, Max);
		}
		
		public override string ToString()
		{
			return "Min: " + Min + ", Value: " + _value + ", Max: " + Max
			       + ", Norm: " + NormalizedValue + ", Range: " + Range;
			//return "Value: " + _value + ", Min: " + Min + ", Max: " + Max;
		}

	}
	
	[Serializable]
	public class RangedIntUnityEvent : UnityEvent<RangedInt>{}
	
	[Serializable]
	public class RangedIntCompareUnityEvent : CompareValueUnityEventBase<RangedInt>{}
	
	[Serializable]
	public class RangedIntArrayUnityEvent : UnityEvent<RangedInt[]>{}

}