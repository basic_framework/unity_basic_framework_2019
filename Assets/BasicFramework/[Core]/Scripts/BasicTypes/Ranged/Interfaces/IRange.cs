﻿namespace BasicFramework.Core.BasicTypes
{
	public interface IRange<T> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		T Min { get; set; }
		T Max { get; set; }
		T Range { get; }
	
		
		// ****************************************  METHODS  ****************************************\\
		
		/// <summary>Determines whether the passed value is within the range</summary>
		/// <param name="value">The value to test</param>
		/// <returns>If value is within range</returns>
		T ClampToRange(T value);
		
		/// <summary>Clamps the passed value to the range</summary>
		/// <param name="value">The value to clamp</param>
		/// <returns>The value clamped to the range</returns>
		bool WithinRange(T value);
		
		/// <summary>
		/// Calculates the ranged value given the normalized value (0 to 1).
		/// </summary>
		/// <param name="normalizedValue">Value between 0 to 1</param>
		/// <returns>The ranged value given the normalized value</returns>
		T NormalizedToRangedValue(float normalizedValue);
		
		/// <summary>
		/// Calculates the normalized value given the ranged value (Min to Max)
		/// </summary>
		/// <param name="rangedValue">The ranged value, will be clamped from Min to Max</param>
		/// <returns>The normalized value given the ranged value</returns>
		float RangedToNormalizedValue(T rangedValue);
		
		/// <summary>
		/// Returns a random value between the Min and Max values
		/// </summary>
		/// <returns></returns>
		T GetRandomInRange();

	}
}