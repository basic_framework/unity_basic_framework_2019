﻿namespace BasicFramework.Core.BasicTypes
{
	public interface IRangedValue<T> : IRange<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		T Value { get; set; }
		float NormalizedValue { get; set; }
		
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}