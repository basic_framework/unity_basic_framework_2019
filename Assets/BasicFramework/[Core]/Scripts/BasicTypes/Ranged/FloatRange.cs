﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct FloatRange : IRange<float>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		public static readonly FloatRange Default = new FloatRange(-1, 1);
		
		[SerializeField] private float _min;
		[SerializeField] private float _max;
		[SerializeField] private float _range;
		
		public float Min
		{
			get => _min;
			set
			{
				_min = value;
				if (_min > _max) { _max = _min; }
				_range = _max - _min;
			}
		}
		
		public float Max
		{
			get => _max;
			set
			{
				_max = value;
				if (_max < _min) { _min = _max; }
				_range = _max - _min;
			}
		}
		
		public float Range => _range;

		
		// ****************************************  METHODS  ****************************************\\
		
		public FloatRange(float min, float max)
		{
			if (min > max) { max = min; }
			
			_min = min;
			_max = max;
			_range = max - min;
		}

		public bool WithinRange(float value)
		{
			return value >= _min && value <= _max;
		}

		public float ClampToRange(float value)
		{
			return Mathf.Clamp(value, _min, _max);
		}

		/// <summary>
		/// Calculates the ranged value given the normalized value (0 to 1).
		/// Example. Min = -5, Max = 5, NormalizedValue = 0.75, returns 2.5 
		/// </summary>
		/// <param name="normalizedValue">Value between 0 to 1</param>
		/// <returns>The ranged value given the normalized value</returns>
		public float NormalizedToRangedValue(float normalizedValue)
		{
			normalizedValue = Mathf.Clamp01(normalizedValue);
			return Math.Abs(_range) < Mathf.Epsilon ? Max : MathUtility.Map(normalizedValue, 0, 1, Min, Max);
		}

		/// <summary>
		/// Calculates the normalized value given the ranged value (Min to Max)
		/// Example. Min = -5, Max = 5, RangedValue = 2.5, returns 0.75 
		/// </summary>
		/// <param name="rangedValue">The ranged value, will be clamped from Min to Max</param>
		/// <returns>The normalized value given the ranged value</returns>
		public float RangedToNormalizedValue(float rangedValue)
		{
			rangedValue = ClampToRange(rangedValue);
			return Math.Abs(_range) < Mathf.Epsilon ? 1 : MathUtility.Map(rangedValue, Min, Max, 0, 1);
		}

		/// <summary>
		/// Returns a random float between the Min (inclusive) and Max (inclusive) value
		/// </summary>
		/// <returns></returns>
		public float GetRandomInRange()
		{
			return Random.Range(Min, Max);
		}
		
		public override string ToString()
		{
			return "Min: " + Min + ", Max: " + Max + ", Range: " + Range;
			//return "Min: " + _min + ", Max: " + _max;
		}
		
	}
	
	[Serializable]
	public class FloatRangeUnityEvent : UnityEvent<FloatRange>{}
	
	[Serializable]
	public class FloatRangeCompareUnityEvent : CompareValueUnityEventBase<FloatRange>{}
	
	[Serializable]
	public class FloatRangeArrayUnityEvent : UnityEvent<FloatRange[]>{}

}