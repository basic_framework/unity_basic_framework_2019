﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(CompareValueUnityEventConcreteBase), true)]
	public class CompareValueUnityEventBasePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded) return;

			var compareValue = property.FindPropertyRelative("_compareValue");


			TempRect.height = GetPropertyHeight(compareValue, true);
			EditorGUI.PropertyField(TempRect, compareValue, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			TempRect.height = SingleLineHeight;

			
			var equalToCompareValue = property.FindPropertyRelative("_equalToCompareValue");
			
			equalToCompareValue.isExpanded = EditorGUI.Foldout(TempRect, equalToCompareValue.isExpanded, GetGuiContent("Events"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			if (!equalToCompareValue.isExpanded) return;
			
			//var equalToCompareValue = property.FindPropertyRelative("_equalToCompareValue");
			var value = equalToCompareValue.FindPropertyRelative("_value");
			var invertedValue = equalToCompareValue.FindPropertyRelative("_invertedValue");
			var isTrue = equalToCompareValue.FindPropertyRelative("_isTrue");
			var isFalse = equalToCompareValue.FindPropertyRelative("_isFalse");
			
			var prevEqualToCompareValue = property.FindPropertyRelative("_prevEqualToCompareValue");
			
			
			TempRect.height = GetPropertyHeight(value, true);
			EditorGUI.PropertyField(TempRect, value, GetGuiContent("Is Compare Value"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			TempRect.height = GetPropertyHeight(invertedValue, true);
			EditorGUI.PropertyField(TempRect, invertedValue, GetGuiContent("(Inverted) Is Compare Value"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			TempRect.height = GetPropertyHeight(isTrue, true);
			EditorGUI.PropertyField(TempRect, isTrue, GetGuiContent("Equal to Compare Value"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			TempRect.height = GetPropertyHeight(isFalse, true);
			EditorGUI.PropertyField(TempRect, isFalse, GetGuiContent("Not Equal to Compare Value"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			TempRect.height = GetPropertyHeight(prevEqualToCompareValue, true);
			EditorGUI.PropertyField(TempRect, prevEqualToCompareValue, GetGuiContent("Previously Equal to Compare Value"));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;

			var compareValue = property.FindPropertyRelative("_compareValue");
			
			var equalToCompareValue = property.FindPropertyRelative("_equalToCompareValue");
			var value = equalToCompareValue.FindPropertyRelative("_value");
			var invertedValue = equalToCompareValue.FindPropertyRelative("_invertedValue");
			var isTrue = equalToCompareValue.FindPropertyRelative("_isTrue");
			var isFalse = equalToCompareValue.FindPropertyRelative("_isFalse");
			
			var prevEqualToCompareValue = property.FindPropertyRelative("_prevEqualToCompareValue");

			//height += BUFFER_HEIGHT + GetPropertyHeight(compareValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(compareValue, true);
			height += BUFFER_HEIGHT + SingleLineHeight;

			if (!equalToCompareValue.isExpanded) return height;
			
			height += BUFFER_HEIGHT + GetPropertyHeight(value, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(invertedValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(isTrue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(isFalse, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(prevEqualToCompareValue, true);
			
			return height;
		}
		
	}
}