﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.Events
{
    [Serializable]
    public class BoolPlusUnityEvent 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Unity Event for the Normal state of the bool value")]
        [SerializeField] private BoolUnityEvent _value = new BoolUnityEvent();
		
        [Tooltip("Unity Event for the Normal state of the bool value")]
        [SerializeField] private BoolUnityEvent _invertedValue = new BoolUnityEvent();

        [Tooltip("Unity Event raised when the bool value is true")]
        [SerializeField] private UnityEvent _isTrue = new UnityEvent();
		
        [Tooltip("Unity Event raised when the bool value is true")]
        [SerializeField] private UnityEvent _isFalse = new UnityEvent();

        public BoolUnityEvent Value => _value;

        public BoolUnityEvent InvertedValue => _invertedValue;

        public UnityEvent IsTrue => _isTrue;

        public UnityEvent IsFalse => _isFalse;


        // ****************************************  METHODS  ****************************************\\

        public void Raise(bool value)
        {
            _value.Invoke(value);
            _invertedValue.Invoke(!value);
			
            if(value) _isTrue.Invoke();
            else      _isFalse.Invoke();
        }
		
    }
}