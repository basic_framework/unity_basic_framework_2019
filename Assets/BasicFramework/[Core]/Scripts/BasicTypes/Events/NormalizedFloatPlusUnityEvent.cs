﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.Events
{
	[Serializable]
	public class NormalizedFloatPlusUnityEvent 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("Unity Event for the value of the NormalizedFloat")]
		[SerializeField] private NormalizedFloatUnityEvent _value = new NormalizedFloatUnityEvent();
		
		[Tooltip("Unity Event for the float value of the NormalizedFloat")]
		[SerializeField] private FloatUnityEvent _floatValue = new FloatUnityEvent();
		
		[Tooltip("The inverted float value (i.e. 1 - value) of the NormalizedFloat")]
		[SerializeField] private FloatUnityEvent _invertedFloatValue = new FloatUnityEvent();

		[Tooltip("Unity Event raised when the value is Zero")]
		[SerializeField] private UnityEvent _isZero = new UnityEvent();
		
		[Tooltip("Unity Event raised when the value is in between Zero and One")]
		[SerializeField] private UnityEvent _isInBetween = new UnityEvent();
		
		[Tooltip("Unity Event raised when the value is One")]
		[SerializeField] private UnityEvent _isOne = new UnityEvent();

		
		public NormalizedFloatUnityEvent Value => _value;

		public FloatUnityEvent FloatValue => _floatValue;

		public UnityEvent IsZero => _isZero;
		
		public UnityEvent IsInBetween => _isZero;

		public UnityEvent IsOne => _isOne;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		public void Raise(NormalizedFloat value)
		{
			_value.Invoke(value);
			_floatValue.Invoke(value.Value);
			_invertedFloatValue.Invoke(1 - value.Value);
			
			if(value == NormalizedFloat.Zero) 		_isZero.Invoke();
			else if(value == NormalizedFloat.One) 	_isOne.Invoke();
			else 									_isInBetween.Invoke();
		}
	
	}
}