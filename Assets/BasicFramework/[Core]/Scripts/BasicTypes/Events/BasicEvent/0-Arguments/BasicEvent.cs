﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Events
{
	public class BasicEvent 
	{

		// **************************************** VARIABLES ****************************************\\

		public static bool SkipActionNullCheck = default;
		
		private readonly List<Action> _subscribers = new List<Action>();
		public IEnumerable<Action> Subscribers => _subscribers;
		

		// ****************************************  METHODS  ****************************************\\
		
		public void Subscribe(Action action)
		{
			if (action == null){return;}
			if (_subscribers.Contains(action)) return;
			_subscribers.Add(action);
		}

		public void Unsubscribe(Action action)
		{
			if(action == null){return;}
			if (!_subscribers.Contains(action)) return;
			_subscribers.Remove(action);
		}
		
		public void Raise()
		{
			for (int i = _subscribers.Count - 1; i >= 0; i--)
			{
				var action = _subscribers[i];
			
				// If the action or its target is null remove it from the list (May be expensive)
				// Is the action reference null
				// Is the action.Target a System.Object and is null, UnityEngine.Object may pass this test
				// Is the action.Target a UnityEngine.Object and is null
				if (!SkipActionNullCheck && IsActionNull(action))
				{
					Debug.LogWarning("Action " + i + " is null. Removing action from \"" + ToString() + "\" subscriber list.");
					_subscribers.RemoveAt(i);
					continue;
				}

				//Debug.Log("Calling target: " + action.Target.ToString() + ", Method: " + action.Method.ToString());
				
				// Call the action
				action();
			}
		}

		private static bool IsActionNull(Action action)
		{
			return action?.Target == null || action.Target is UnityEngine.Object && action.Target.Equals(null);
		}
		
	}
}