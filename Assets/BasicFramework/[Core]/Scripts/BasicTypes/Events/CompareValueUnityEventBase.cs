﻿using System;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
    [Serializable]
    public class CompareValueUnityEventConcreteBase{}
    
    [Serializable]
    public abstract class CompareValueUnityEventBase<T> : CompareValueUnityEventConcreteBase
    {
    
        // **************************************** VARIABLES ****************************************\\

        // Note: Only raises the event when the value is changed

        [Tooltip("The value to be compared against")]
        [SerializeField] private T _compareValue = default;

        [Tooltip("Raised when the value changes")]
        [SerializeField] private BoolPlusUnityEvent _equalToCompareValue = default;

        [Tooltip("Raised when the value changes from the compare value")]
        [SerializeField] private UnityEvent _prevEqualToCompareValue = default;

        private bool _initialised;
        
        public T CompareValue => _compareValue;
        
        private T _currentValue;
        public T CurrentValue => _currentValue;

        private bool _currentValueEqualToCompareValue;
        public bool CurrentValueEqualToCompareValue => _currentValueEqualToCompareValue;


        // ****************************************  METHODS  ****************************************\\
        
        protected virtual bool Compare(T left, T right)
        {
            return Equals(left, right);
        }

        private void SetCompareValue(T compareValue)
        {
            if (Compare(compareValue, _compareValue)) return;
            _compareValue = compareValue;
            _initialised = false;
            SetCurrentValue(_currentValue);
        }
        
        public void SetCurrentValue(T newValue, bool forceRaise = false)
        {
            if (!_initialised)
            {
                _initialised = true;
                _currentValue = newValue;
                _currentValueEqualToCompareValue = Compare(_currentValue, _compareValue);
                _equalToCompareValue.Raise(_currentValueEqualToCompareValue);
                return;
            }

            if (forceRaise)
            {
                _currentValue = newValue;
                
                var prevValueEqualToCompareValue = _currentValueEqualToCompareValue;
                _currentValueEqualToCompareValue = Compare(_currentValue, _compareValue);
                
                _equalToCompareValue.Raise(_currentValueEqualToCompareValue);
                
                if (_currentValueEqualToCompareValue)  return;
                if (!prevValueEqualToCompareValue)     return;
                _prevEqualToCompareValue.Invoke();
            }
            
            // Check that the new value is different to our current value
            if (Compare(newValue, _currentValue)) return;
            _currentValue = newValue;

            var newValueEqualToCompare = Compare(newValue, _compareValue);
            
            // Only raise events if switching between true/false
            if (newValueEqualToCompare == _currentValueEqualToCompareValue) return;
            _currentValueEqualToCompareValue = newValueEqualToCompare;
            
            _equalToCompareValue.Raise(_currentValueEqualToCompareValue);
            
            if (_currentValueEqualToCompareValue) return;
            _prevEqualToCompareValue.Invoke();
        }
        
    }
}