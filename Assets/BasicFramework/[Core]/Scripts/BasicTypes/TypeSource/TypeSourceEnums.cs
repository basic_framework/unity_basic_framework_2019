﻿using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	public enum BoolInputReferenceTypes
	{
//		Property,
		Keycode,
		UnityInput,
		BoolInput,
		BoolSingle,
	}
	
}