﻿using System;
using BasicFramework.Core.ScriptableInputs;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public class BoolInputReference 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The source type for the select button input")]
		[SerializeField] private BoolInputReferenceTypes _inputType = BoolInputReferenceTypes.Keycode;

		[SerializeField] private KeyCode _keyCode = KeyCode.Mouse0;
		[SerializeField] private string _unityInput = "Fire1";
		[SerializeField] private BoolInput _boolInput = default;
		[SerializeField] private BoolSingle _boolSingle = default;
		
		
		public bool Value
		{
			get
			{
				switch (_inputType)
				{
					case BoolInputReferenceTypes.Keycode:		return Input.GetKey(_keyCode);
					case BoolInputReferenceTypes.UnityInput:	return Input.GetButton(_unityInput);
					case BoolInputReferenceTypes.BoolInput:		return _boolInput.Value;
					case BoolInputReferenceTypes.BoolSingle:	return _boolSingle.Value;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}


		// ****************************************  METHODS  ****************************************\\

		public BoolInputReference(){}

		public BoolInputReference(BoolInputReferenceTypes inputType)
		{
			_inputType = inputType;
		}
		
	}
}