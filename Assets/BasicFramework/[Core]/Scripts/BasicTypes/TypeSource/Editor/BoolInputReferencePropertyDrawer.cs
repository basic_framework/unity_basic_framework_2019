﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.UnityInput.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(BoolInputReference))]
	public class BoolInputReferencePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private const float SOURCE_WIDTH = 80f;
		

		// ****************************************  METHODS  ****************************************\\


		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position = EditorGUI.PrefixLabel(position, label);
			TempRect = position;
			TempRect.width = SOURCE_WIDTH;
			
			var inputType = property.FindPropertyRelative("_inputType");

			EditorGUI.PropertyField(TempRect, inputType, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			TempRect.width = position.width - TempRect.width - BUFFER_WIDTH;
			
			var sourceTypeEnum =
				EnumUtility.EnumNameToEnumValue<BoolInputReferenceTypes>(inputType.enumNames[inputType.enumValueIndex]);

			SerializedProperty sourceProperty;
			
			switch (sourceTypeEnum)
			{
				case BoolInputReferenceTypes.Keycode:
					sourceProperty = property.FindPropertyRelative("_keyCode");
					break;
				
				case BoolInputReferenceTypes.UnityInput:
					sourceProperty = property.FindPropertyRelative("_unityInput");
					break;
				
				case BoolInputReferenceTypes.BoolInput:
					sourceProperty = property.FindPropertyRelative("_boolInput");
					break;
				
				case BoolInputReferenceTypes.BoolSingle:
					sourceProperty = property.FindPropertyRelative("_boolSingle");
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			if (sourceTypeEnum == BoolInputReferenceTypes.UnityInput)
			{
				UnityInputEditorUtility.UnityAxisNameField(TempRect, sourceProperty);
			}
			else
			{
				BasicEditorGui.PropertyFieldNotNull(TempRect, sourceProperty, GUIContent.none);
			}
			
		}
		
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return SingleLineHeight;
		}
		
	}
}