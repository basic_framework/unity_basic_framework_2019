using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
    public abstract class ScriptableAssetBase : ScriptableObject 
    {
        // Note: Inherit from this class to make a generic ScriptableObject Data Asset, can also be used as an enum 

        // **************************************** VARIABLES ****************************************\\
	
        [Tooltip("Will not unload object if there are no references to it. " +
                 "This will maintain its value across the entire session")]
        [SerializeField] private bool _dontUnloadUnusedAsset = true;
        
        [SerializeField] private string _assetName = default;

        [SerializeField] private Sprite _assetIcon = default;

        public string AssetName
        {
            get => string.IsNullOrEmpty(_assetName) ? name : _assetName;
            protected set => _assetName = value;
        }

        public Sprite AssetIcon
        {
            get => _assetIcon;
            protected set => _assetIcon = value;
        }


        // ****************************************  METHODS  ****************************************\\

        protected virtual void OnEnable()
        {
            if (!_dontUnloadUnusedAsset) return;
            hideFlags = HideFlags.DontUnloadUnusedAsset;
        }
        
        public override string ToString()
        {
            return AssetName;
        }
        
    }
}