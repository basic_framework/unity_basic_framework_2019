using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Editor
{
    public abstract class ScriptableAssetBaseEditor<T> : BasicBaseEditor<T>
        where T : ScriptableAssetBase
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            Space();
            SectionHeader($"{nameof(ScriptableAssetBase).CamelCaseToHumanReadable()} Properties");
            
            var assetName = serializedObject.FindProperty("_assetName");
            var assetIcon = serializedObject.FindProperty("_assetIcon");

            EditorGUILayout.PropertyField(assetName);
            EditorGUILayout.PropertyField(assetIcon);
        }
        
    }
}