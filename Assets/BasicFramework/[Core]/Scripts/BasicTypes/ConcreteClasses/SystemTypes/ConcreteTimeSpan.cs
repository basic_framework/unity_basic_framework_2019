﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
    [Serializable]
    public class TimeSpanUnityEvent : UnityEvent<TimeSpan>{}
    
    [Serializable]
    public class TimeSpanCompareValueUnityEvent : CompareValueUnityEventBase<TimeSpan>{}
	
    [Serializable]
    public class TimeSpanArrayUnityEvent : UnityEvent<TimeSpan[]>{}

    [Serializable]
    public class TimeSpanBuffer : BasicBuffer<TimeSpan>{}
}