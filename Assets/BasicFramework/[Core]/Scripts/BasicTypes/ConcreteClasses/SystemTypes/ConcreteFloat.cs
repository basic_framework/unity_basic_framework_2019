﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class FloatUnityEvent : UnityEvent<float>{}
	
	[Serializable]
	public class FloatCompareUnityEvent : CompareValueUnityEventBase<float>{}
	
	[Serializable]
	public class FloatArrayUnityEvent : UnityEvent<float[]>{}

	[Serializable]
	public class FloatBuffer : BasicBuffer<float>{}
}