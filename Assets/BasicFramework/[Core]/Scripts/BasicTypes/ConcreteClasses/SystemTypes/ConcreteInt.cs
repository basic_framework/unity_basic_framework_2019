﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class IntUnityEvent : UnityEvent<int>{}
	
	[Serializable]
	public class IntCompareUnityEvent : CompareValueUnityEventBase<int>{}
	
	[Serializable]
	public class IntArrayUnityEvent : UnityEvent<int[]>{}

	[Serializable]
	public class IntBuffer : BasicBuffer<int>{}
}