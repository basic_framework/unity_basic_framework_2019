﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class SystemObjectUnityEvent : UnityEvent<object>{}
	
	[Serializable]
	public class ObjectCompareValueUnityEvent : CompareValueUnityEventBase<object>{}
	
	[Serializable]
	public class SystemObjectArrayUnityEvent : UnityEvent<object[]>{}

	[Serializable]
	public class SystemObjectBuffer : BasicBuffer<object>{}
}