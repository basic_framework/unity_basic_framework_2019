﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class UriUnityEvent : UnityEvent<Uri>{}
	
	[Serializable]
	public class UriCompareUnityEvent : CompareValueUnityEventBase<Uri>{}
	
	[Serializable]
	public class UriArrayUnityEvent : UnityEvent<Uri[]>{}

	[Serializable]
	public class UriBuffer : BasicBuffer<Uri>{}
}