﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class StringUnityEvent : UnityEvent<string>{}
	
	[Serializable]
	public class StringCompareUnityEvent : CompareValueUnityEventBase<string>{}
	
	[Serializable]
	public class StringArrayUnityEvent : UnityEvent<string[]>{}

	[Serializable]
	public class StringBuffer : BasicBuffer<string>{}
}