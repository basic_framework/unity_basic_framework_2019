﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
    [Serializable]
    public class DateTimeUnityEvent : UnityEvent<DateTime>{}
    
    [Serializable]
    public class DateTimeCompareValueUnityEvent : CompareValueUnityEventBase<DateTime>{}
	
    [Serializable]
    public class DateTimeArrayUnityEvent : UnityEvent<DateTime[]>{}

    [Serializable]
    public class DateTimeBuffer : BasicBuffer<DateTime>{}
}