﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class BoolUnityEvent : UnityEvent<bool>{}
	
	[Serializable]
	public class BoolCompareValueUnityEvent : CompareValueUnityEventBase<bool>{}
	
	[Serializable]
	public class BoolArrayUnityEvent : UnityEvent<bool[]>{}

	[Serializable]
	public class BoolBuffer : BasicBuffer<bool>{}
}