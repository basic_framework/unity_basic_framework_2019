﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class RigidbodyUnityEvent : UnityEvent<Rigidbody>{}
	
	[Serializable]
	public class RigidbodyCompareValueUnityEvent : CompareValueUnityEventBase<Rigidbody>{}
	
	[Serializable]
	public class RigidbodyArrayUnityEvent : UnityEvent<Rigidbody[]>{}
}