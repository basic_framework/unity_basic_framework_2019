﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class RectTransformUnityEvent : UnityEvent<RectTransform>{}
	
	[Serializable]
	public class RectTransformCompareUnityEvent : CompareValueUnityEventBase<RectTransform>{}
	
	[Serializable]
	public class RectTransformArrayUnityEvent : UnityEvent<RectTransform[]>{}
}