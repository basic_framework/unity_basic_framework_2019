﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class Texture2DUnityEvent : UnityEvent<Texture2D>{}
	
	[Serializable]
	public class Texture2DCompareUnityEvent : CompareValueUnityEventBase<Texture2D>{}
	
	[Serializable]
	public class Texture2DArrayUnityEvent : UnityEvent<Texture2D[]>{}
}