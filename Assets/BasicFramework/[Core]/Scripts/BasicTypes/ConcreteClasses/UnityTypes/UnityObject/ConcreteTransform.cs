﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class TransformUnityEvent : UnityEvent<Transform>{}
	
	[Serializable]
	public class TransformCompareUnityEvent : CompareValueUnityEventBase<Transform>{}
	
	[Serializable]
	public class TransformArrayUnityEvent : UnityEvent<Transform[]>{}
}