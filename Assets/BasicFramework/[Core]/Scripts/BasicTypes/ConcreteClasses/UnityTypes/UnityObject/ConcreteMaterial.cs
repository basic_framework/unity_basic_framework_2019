﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
    [Serializable]
    public class MaterialUnityEvent : UnityEvent<Material>{}
    
    [Serializable]
    public class MaterialCompareUnityEvent : CompareValueUnityEventBase<Material>{}

    [Serializable]
    public class MaterialArrayUnityEvent : UnityEvent<Material[]>{}
}