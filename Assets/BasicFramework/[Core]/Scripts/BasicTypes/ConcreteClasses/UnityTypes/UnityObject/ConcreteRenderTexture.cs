﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class RenderTextureUnityEvent : UnityEvent<RenderTexture>{}
	
	[Serializable]
	public class RenderTextureCompareUnityEvent : CompareValueUnityEventBase<RenderTexture>{}
	
	[Serializable]
	public class RenderTextureArrayUnityEvent : UnityEvent<RenderTexture[]>{}
}