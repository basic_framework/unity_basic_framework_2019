﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
    [Serializable]
    public class SpriteUnityEvent : UnityEvent<Sprite>{}
	
    [Serializable]
    public class SpriteCompareUnityEvent : CompareValueUnityEventBase<Sprite>{}
	
    [Serializable]
    public class SpriteArrayUnityEvent : UnityEvent<Sprite[]>{}
}