﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class GameObjectUnityEvent : UnityEvent<GameObject>{}
	
	[Serializable]
	public class GameObjectCompareUnityEvent : CompareValueUnityEventBase<GameObject>{}
	
	[Serializable]
	public class GameObjectArrayUnityEvent : UnityEvent<GameObject[]>{}
}