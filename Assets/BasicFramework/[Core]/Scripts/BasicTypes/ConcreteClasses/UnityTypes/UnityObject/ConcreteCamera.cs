﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class CameraUnityEvent : UnityEvent<Camera>{}
	
	[Serializable]
	public class CameraCompareUnityEvent : CompareValueUnityEventBase<Camera>{}
	
	[Serializable]
	public class CameraArrayUnityEvent : UnityEvent<Camera[]>{}
}