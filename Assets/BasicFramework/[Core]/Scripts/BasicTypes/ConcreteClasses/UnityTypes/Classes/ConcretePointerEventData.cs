﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class PointerEventDataUnityEvent : UnityEvent<PointerEventData>{}
	
	[Serializable]
	public class PointerEventDataCompareUnityEvent : CompareValueUnityEventBase<PointerEventData>{}
	
	[Serializable]
	public class PointerEventDataArrayUnityEvent : UnityEvent<PointerEventData[]>{}
}