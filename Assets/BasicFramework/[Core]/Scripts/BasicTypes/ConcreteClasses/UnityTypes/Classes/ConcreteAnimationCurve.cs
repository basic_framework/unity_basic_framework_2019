﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class AnimationCurveUnityEvent : UnityEvent<AnimationCurve>{}
	
	[Serializable]
	public class AnimationCurveCompareUnityEvent : CompareValueUnityEventBase<AnimationCurve>{}
	
	[Serializable]
	public class AnimationCurveArrayUnityEvent : UnityEvent<AnimationCurve[]>{}
	
}