﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class Vector3UnityEvent : UnityEvent<Vector3>{}
	
	[Serializable]
	public class Vector3CompareUnityEvent : CompareValueUnityEventBase<Vector3>{}
	
	[Serializable]
	public class Vector3ArrayUnityEvent : UnityEvent<Vector3[]>{}
}