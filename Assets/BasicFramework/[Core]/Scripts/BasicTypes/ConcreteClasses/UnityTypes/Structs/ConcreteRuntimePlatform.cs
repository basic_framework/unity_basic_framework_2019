﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	
	[Serializable]
	public class RuntimePlatformUnityEvent : UnityEvent<RuntimePlatform>{}
    
	[Serializable]
	public class RuntimePlatformCompareUnityEvent : CompareValueUnityEventBase<RuntimePlatform>{}
	
	[Serializable]
	public class RuntimePlatformArrayUnityEvent : UnityEvent<RuntimePlatform[]>{}

}