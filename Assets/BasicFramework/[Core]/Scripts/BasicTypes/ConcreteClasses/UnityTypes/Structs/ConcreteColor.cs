﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class ColorUnityEvent : UnityEvent<Color>{}
	
	[Serializable]
	public class ColorCompareUnityEvent : CompareValueUnityEventBase<Color>{}
	
	[Serializable]
	public class ColorArrayUnityEvent : UnityEvent<Color[]>{}
}