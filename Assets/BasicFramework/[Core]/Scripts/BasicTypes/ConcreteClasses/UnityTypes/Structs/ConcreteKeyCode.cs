﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
    [Serializable]
    public class KeyCodeUnityEvent : UnityEvent<KeyCode>{}
    
    [Serializable]
    public class KeyCodeCompareUnityEvent : CompareValueUnityEventBase<KeyCode>{}
	
    [Serializable]
    public class KeyCodeArrayUnityEvent : UnityEvent<KeyCode[]>{}
}