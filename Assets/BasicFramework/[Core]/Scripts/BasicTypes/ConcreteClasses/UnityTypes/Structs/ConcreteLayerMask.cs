﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class LayerMaskUnityEvent : UnityEvent<LayerMask>{}
	
	[Serializable]
	public class LayerMaskCompareUnityEvent : CompareValueUnityEventBase<LayerMask>{}
	
	[Serializable]
	public class LayerMaskArrayUnityEvent : UnityEvent<LayerMask[]>{}
}