﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class QuaternionUnityEvent : UnityEvent<Quaternion>{}
	
	[Serializable]
	public class QuaternionCompareUnityEvent : CompareValueUnityEventBase<Quaternion>{}
	
	[Serializable]
	public class QuaternionArrayUnityEvent : UnityEvent<Quaternion[]>{}
}