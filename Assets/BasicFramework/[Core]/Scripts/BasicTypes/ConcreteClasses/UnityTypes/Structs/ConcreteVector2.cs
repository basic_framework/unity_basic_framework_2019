﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.ConcreteClasses
{
	[Serializable]
	public class Vector2UnityEvent : UnityEvent<Vector2>{}
	
	[Serializable]
	public class Vector2CompareUnityEvent : CompareValueUnityEventBase<Vector2>{}
	
	[Serializable]
	public class Vector2ArrayUnityEvent : UnityEvent<Vector2[]>{}
}