﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public class BasicBuffer<T> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const int MIN_BUFFER_LENGTH = 1;
		private const int MAX_BUFFER_LENGTH = 1024;
		
		[Range(MIN_BUFFER_LENGTH, MAX_BUFFER_LENGTH)]
		[SerializeField] private int _maxBufferLength = 10;
		
		[Tooltip("If the max buffer length is exceeded should the new value be dropped?" +
		         " If false the oldest value will be dropped.")]
		[SerializeField] private bool _keepOldest = false;
	
		private readonly Queue<T> _bufferQueue = new Queue<T>();
		
		private T _newestValue;
		//public T LastValue => _lastValue;

		/// <summary>
		/// The maximum number of values that can be stored in the buffer
		/// </summary>
		public int MaxBufferLength
		{
			get => _maxBufferLength;
			set
			{
				value = Mathf.Clamp(value, MIN_BUFFER_LENGTH, MAX_BUFFER_LENGTH);
				_maxBufferLength = value;
			}
		}
		
		/// <summary>
		/// If the max buffer length is exceeded should the new value be dropped?
		///	If false the oldest value will be dropped.
		/// </summary>
		public bool KeepOldest
		{
			get => _keepOldest;
			set => _keepOldest = value;
		}

		/// <summary>
		/// Number of values stored in the buffer
		/// </summary>
		public int BufferCount => _bufferQueue.Count;

		/// <summary>
		/// Return true if there is something stored in the buffer, you should always check this before reading from buffer
		/// </summary>
		public bool Available => _bufferQueue.Count != 0;
		
		
		// ****************************************  METHODS  ****************************************\\

		/// <summary>
		/// Writes the value to the end of the buffer.
		/// If the limit is reached the newest or oldest input is dropped depending on the state of KeepOldest
		/// </summary>
		/// <param name="value"></param>
		/// <param name="logDebugMessage">Set true if you want a log message to be printed when a byte is dropped</param>
		public void WriteToBuffer(T value, bool logDebugMessage = false)
		{		
			if (_bufferQueue.Count == _maxBufferLength)
			{
				if (logDebugMessage)
				{
					var message = $"Buffer Full. Dropping {(_keepOldest ? "NEWEST" : "OLDEST")} value \"{(_keepOldest ? value : _bufferQueue.Peek())}\"";
					Debug.LogWarning(message);
				}
				
				if (_keepOldest) return;
				_bufferQueue.Dequeue();
			}
				
			_bufferQueue.Enqueue(value);
			_newestValue = value;
		}
		
		/// <summary>
		/// <para>Returns the oldest buffer input and removes it from the buffer.</para> 
		/// <para>You should check to see if there is something available otherwise an error will be thrown</para>
		/// </summary>
		/// <returns></returns>
		public T ReadOldest()
		{
			if (!Available)
			{
				throw new ArgumentOutOfRangeException(GetType().Name, "Cannot read from buffer as it is empty");
			}

			return _bufferQueue.Dequeue();
			
			//return _bufferQueue.Count == 0 ? default(T) : _bufferQueue.Dequeue(); 
		}

		/// <summary>
		/// Returns the newest buffer input.
		/// You should check to see if there is something available otherwise an error will be thrown
		/// </summary>
		/// <param name="clearBuffer">Clear the buffer</param>
		/// <returns></returns>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public T ReadNewest(bool clearBuffer = true)
		{
			if (!Available)
			{
				throw new ArgumentOutOfRangeException(GetType().Name, "Cannot read from buffer as it is empty");
			}
				
			var value = _newestValue;
			if (clearBuffer) { ClearBuffer(); }
			return value;
		}

		/// <summary>
		/// <para>Returns the oldest buffer input without removing it from the buffer.</para> 
		/// <para>You should check to see if there is something available otherwise an error will be thrown</para>
		/// </summary>
		/// <returns></returns>
		public T PeekFirst()
		{
			if (_bufferQueue.Count == 0)
			{
				throw new ArgumentOutOfRangeException(GetType().Name, "Cannot read from buffer as it is empty");
			}
			
			return _bufferQueue.Peek();
		}
		

		/// <summary>
		/// Clears the buffer
		/// </summary>
		public void ClearBuffer()
		{
			_bufferQueue.Clear();
			_newestValue = default;
		}
		
	}
}