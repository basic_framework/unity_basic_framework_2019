﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct PowerOfTwo 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		public static PowerOfTwo Default = new PowerOfTwo(10);

		public const int BASE = 2;
		public const int MINIMUM_EXPONENT = 1;
		public const int MAXIMUM_EXPONENT = 30;

		[SerializeField] private int _exponent;
		
		[SerializeField] private int _value;
		[SerializeField] private int _minimumExponent;
		[SerializeField] private int _maximumExponent;
		
		public int Exponent
		{
			get => _exponent;
			set
			{
				_exponent = Mathf.Clamp(value, _minimumExponent, _maximumExponent);
				_value = (int) Mathf.Pow(BASE, _exponent);
			}
		}

		/// <summary>
		/// Sets the value to the nearest PowerOfTwo greater than the value passed
		/// </summary>
		public int Value
		{
			get => _value;
			set
			{
				_exponent = Mathf.CeilToInt(Mathf.Log(value, BASE));
				_exponent = Mathf.Clamp(value, _minimumExponent, _maximumExponent);
				_value = (int) Mathf.Pow(BASE, _exponent);
			}
		}
		
		
		// ****************************************  METHODS  ****************************************\\
		
		public PowerOfTwo(int exponent)
		{
			_minimumExponent = MINIMUM_EXPONENT;
			_maximumExponent = MAXIMUM_EXPONENT;
			
			_exponent = Mathf.Clamp(exponent, _minimumExponent, _maximumExponent);
			_value = (int) Mathf.Pow(BASE, _exponent);
		}
		
		public PowerOfTwo(int exponent, int minimum, int maximum)
		{
			_minimumExponent = Mathf.Clamp(minimum, MINIMUM_EXPONENT, minimum);
			_maximumExponent = Mathf.Clamp(maximum, minimum, MAXIMUM_EXPONENT);

			_exponent = Mathf.Clamp(exponent, _minimumExponent, _maximumExponent);
			_value = (int) Mathf.Pow(BASE, _exponent);
		}

		public static implicit operator int(PowerOfTwo powerOfTwo)
		{
			return powerOfTwo._value;
		}
		
	}
	
	[Serializable]
	public class PowerOfTwoUnityEvent : UnityEvent<PowerOfTwo>{}
	
	[Serializable]
	public class PowerOfTwoArrayUnityEvent : UnityEvent<PowerOfTwo[]>{}

}