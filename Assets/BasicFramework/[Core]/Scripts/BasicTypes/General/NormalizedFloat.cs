﻿using System;
using System.Globalization;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct NormalizedFloat
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		public static readonly NormalizedFloat Zero = new NormalizedFloat(0f);
		public static readonly NormalizedFloat Half = new NormalizedFloat(0.5f);
		public static readonly NormalizedFloat One = new NormalizedFloat(1f);
	
		[Range(0f, 1f)]
		[SerializeField] private float _value;
		
		public float Value
		{
			get => _value;
			set => _value = Mathf.Clamp01(value);
		}

		public bool IsZero => Mathf.Abs(_value) < Mathf.Epsilon;
		public bool IsOne => Mathf.Abs(_value - 1f) < Mathf.Epsilon;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		public NormalizedFloat(float value)
		{
			_value = Mathf.Clamp01(value);
		}

		public NormalizedFloat(float value, float min, float max)
		{
			_value = Mathf.Clamp01(MathUtility.Map(value, min, max, 0, 1));
		}
		
		public NormalizedFloat(double value, double min, double max)
		{
			_value = Mathf.Clamp01((float) MathUtility.Map(value, min, max, 0, 1));
		}
		
		public void SetToRangedValue(float rangedValue, float min, float max)
		{
			rangedValue = Mathf.Clamp(rangedValue, min, max);
			_value = MathUtility.Map(rangedValue, min, max, 0, 1);
		}

		public void SetToRangedValue(float rangedValue, FloatRange floatRange)
		{
			_value = floatRange.RangedToNormalizedValue(rangedValue);
		}
		
		public void SetToRangedValue(RangedFloat rangedFloat)
		{
			_value = rangedFloat.NormalizedValue;
		}
		
		public void SetToRangedValue(int rangedValue, IntRange intRange)
		{
			_value = intRange.RangedToNormalizedValue(rangedValue);
		}
		
		public void SetToRangedValue(RangedInt rangedInt)
		{
			_value = rangedInt.NormalizedValue;
		}

		
		public float MapToRange(RangedInt range)
		{
			return range.NormalizedToRangedValue(_value);
		}

		public float MapToRange(RangedFloat range)
		{
			return range.NormalizedToRangedValue(_value);
		}
		
		public float MapToRange(float min, float max)
		{
			return MathUtility.Map(_value, 0, 1, min, max);
		}

		public bool DoRandomTest()
		{
			return Math.Abs(_value) > Mathf.Epsilon && Random.Range(0f, 1f) <= _value;
		}
		
		
		//***** Override Operators *****\\
		
//		public static implicit operator float(NormalizedFloat normalizedFloat)
//		{
//			return normalizedFloat.Value;
//		}

		public static bool operator ==(NormalizedFloat left, NormalizedFloat right)
		{
			return Math.Abs(left._value - right._value) < Mathf.Epsilon;
		}

		public static bool operator !=(NormalizedFloat left, NormalizedFloat right)
		{
			return !(left == right);
		}

		public override string ToString()
		{
			return MathUtility.RoundToDecimalPlace(_value, 2).ToString(CultureInfo.InvariantCulture);
		}

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			var normalizedFloat = (NormalizedFloat) obj;
			return normalizedFloat == this;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
	
	[Serializable]
	public class NormalizedFloatUnityEvent : UnityEvent<NormalizedFloat>{}
	
	[Serializable]
	public class NormalizedFloatCompareUnityEvent : CompareValueUnityEventBase<NormalizedFloat>{}
	
	[Serializable]
	public class NormalizedFloatArrayUnityEvent : UnityEvent<NormalizedFloat[]>{}
	
}