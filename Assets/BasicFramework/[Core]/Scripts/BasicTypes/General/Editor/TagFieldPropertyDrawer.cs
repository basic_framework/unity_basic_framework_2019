﻿using System.Collections.Generic;
using System.Linq;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(TagField))]
	public class TagFieldPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private readonly List<string> _preUpdateList = new List<string>();
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var tagsProperty = property.FindPropertyRelative("_tags");
		
			var unityTagsList = UnityEditorInternal.InternalEditorUtility.tags;
		
			_preUpdateList.Clear();
		
			// Get a list of the currently selected tags
			// Also clear out any tags that don't exist anymore
			for (int i = tagsProperty.arraySize - 1; i >= 0; i--)
			{
				var tag = tagsProperty.GetArrayElementAtIndex(i).stringValue;
			
				if (!unityTagsList.Contains(tag))
				{
					BasicEditorArrayUtility.RemoveArrayElement(tagsProperty, i);
					continue;
				}
			
				_preUpdateList.Add(tag);
			}
		
			// Setup tag mask
			var preTagMask = 0;
		
			// Setup the mask
			for (int i = 0; i < unityTagsList.Length; i++)
			{
				if (i >= 32)
				{
					Debug.LogError($"{nameof(TagField)} => Number of tags defined is >= 32. Later tags skipped");
					break;
				}
			
				if(!_preUpdateList.Contains(unityTagsList[i])) continue;
				preTagMask |= 1 << i;
			}
		
			// Update the mask
			var postTagMask = EditorGUI.MaskField(position, label, preTagMask, unityTagsList);
			
			if (preTagMask == postTagMask) return;
		
			var allowMultipleProperty = property.FindPropertyRelative("_allowMultiple");
		
			for(int i = 0; i < 32; i++)
			{
				if (i >= unityTagsList.Length) break;
			
				var preBitSet = (preTagMask & 1 << i) != 0;
				var postBitSet = (postTagMask & 1 << i) != 0;
			
				if(preBitSet == postBitSet) continue;

				var tag = unityTagsList[i];
			
				// Tag deselected
				if (!postBitSet)
				{
					// Option deselected, remove it from our property
					for (int j = tagsProperty.arraySize - 1; j >= 0; j--)
					{
						if(tagsProperty.GetArrayElementAtIndex(j).stringValue != tag) continue;
						BasicEditorArrayUtility.RemoveArrayElement(tagsProperty, j);
					}
					continue;
				}
			
				// Tag selected
				if (!allowMultipleProperty.boolValue)
				{
					BasicEditorArrayUtility.RemoveAllArrayElements(tagsProperty);
				}
			
				var newTagProperty = BasicEditorArrayUtility.AddArrayElement(tagsProperty);
				newTagProperty.stringValue = tag;
			}
		}
		
	}
}