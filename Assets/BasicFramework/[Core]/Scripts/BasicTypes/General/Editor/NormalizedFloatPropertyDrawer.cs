﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(NormalizedFloat))]
	public class NormalizedFloatPropertyDrawer : PropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var value = property.FindPropertyRelative("_value");
			EditorGUI.PropertyField(position, value, label);
		}

	}
}