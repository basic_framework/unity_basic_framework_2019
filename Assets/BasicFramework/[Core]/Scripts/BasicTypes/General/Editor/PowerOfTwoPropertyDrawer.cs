﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(PowerOfTwo))]
	public class PowerOfTwoPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const float VALUE_WIDTH = 88;

		private GUIContent _labelContent;
		private GUIStyle _valueLabelStyle;
	
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (_labelContent == null)
			{
				_labelContent = new GUIContent(property.displayName);
			}

			if (_valueLabelStyle == null)
			{
				_valueLabelStyle = new GUIStyle(EditorStyles.boldLabel);
				_valueLabelStyle.alignment = TextAnchor.MiddleCenter;
			}
			
			var exponent = property.FindPropertyRelative("_exponent");
			var value = property.FindPropertyRelative("_value");
			var minimum = property.FindPropertyRelative("_minimumExponent");
			var maximum = property.FindPropertyRelative("_maximumExponent");

			if (minimum.intValue < PowerOfTwo.MINIMUM_EXPONENT)
			{
				minimum.intValue = PowerOfTwo.MINIMUM_EXPONENT;
			}

			if (maximum.intValue > PowerOfTwo.MAXIMUM_EXPONENT || maximum.intValue < minimum.intValue)
			{
				maximum.intValue = PowerOfTwo.MAXIMUM_EXPONENT;
			}
			
			var exponentRect = new Rect(position.x, position.y, position.width - VALUE_WIDTH, position.height);
			var valueRect	 = new Rect(exponentRect.max.x, position.y, VALUE_WIDTH, position.height);
			
			exponent.intValue = EditorGUI.IntSlider(exponentRect, _labelContent, exponent.intValue, minimum.intValue,maximum.intValue);
			exponent.intValue = Mathf.Clamp(exponent.intValue, minimum.intValue, maximum.intValue);
			
			value.intValue = (int) Mathf.Pow(2, exponent.intValue);
			
			EditorGUI.LabelField(valueRect, value.intValue.ToString(), _valueLabelStyle);
		}

	}
}