﻿using System;
using System.Linq;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct TagField 
	{
		// Note: This is mainly meant for editor use, shouldn't modify at runtime
		// Note: Only works if the number of tags defined is <= 32
		// Note: If a tag name changes it will not be updated, need to reselect the new tag name
	
		// **************************************** VARIABLES ****************************************\\

		public static TagField SingleTag = new TagField(false);
		public static TagField MultiTag = new TagField(true);
	
		[SerializeField] private bool _allowMultiple;
		[SerializeField] private string[] _tags;

		public string[] Tags => _tags;
		

		// ****************************************  METHODS  ****************************************\\

		public TagField(bool allowMultiple = false)
		{
			_allowMultiple = allowMultiple;
			_tags = new string[0];
		}
	
	
		public bool ContainsTag(string tag)
		{
			return _tags.Contains(tag);
		}
		
	}
}