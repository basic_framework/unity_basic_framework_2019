﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	public class TrajectoryData 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private Vector3 _origin;
		public Vector3 Origin
		{
			get => _origin;
			set => _origin = value;
		}

		private Vector3 _launchDirection;
		public Vector3 LaunchDirection
		{
			get => _launchDirection;
			set
			{
				value.Normalize();
				if (_launchDirection == value) return;
				_launchDirection = value;
				UpdateData();
			}
		}

		private float _launchSpeed;
		public float LaunchSpeed
		{
			get => _launchSpeed;
			set
			{
				value = Mathf.Max(0, value);
				if (Math.Abs(_launchSpeed - value) < Mathf.Epsilon) return;
				_launchSpeed = value;
				UpdateData();
			}
		}
		
		private Vector3 _gravityDirection;
		public Vector3 GravityDirection
		{
			get => _gravityDirection;
			set
			{
				value.Normalize();
				if (_gravityDirection == value) return;
				_gravityDirection = value;
				UpdateData();
			}
		}

		private float _gravityMagnitude;
		public float GravityMagnitude
		{
			get => _gravityMagnitude;
			set
			{
				value = Mathf.Max(0, value);
				_gravityMagnitude = value;
			}
		}


		private Vector3 _launchVelocity;
		public Vector3 LaunchVelocity => _launchVelocity;
		
		private Vector3 _launchHorizontalDirection;
		public Vector3 LaunchHorizontalDirection => _launchHorizontalDirection;
		
		private float _launchHorizontalSpeed;
		public float LaunchHorizontalSpeed => _launchHorizontalSpeed;

		private Vector3 _launchVerticalDirection;
		public Vector3 LaunchVerticalDirection => _launchVerticalDirection;
		
		private float _launchVerticalSpeed;
		public float LaunchVerticalSpeed => _launchVerticalSpeed;
		
		public bool IsValid => _launchVelocity.sqrMagnitude > 0;
		

		// ****************************************  METHODS  ****************************************\\

		public TrajectoryData()
		{
			_gravityDirection = Vector3.down;
			UpdateData();
		}

		public TrajectoryData(Vector3 origin, Vector3 launchDirection, float launchSpeed)
		{
			_origin = origin;
			_launchDirection = launchDirection.normalized;
			_launchSpeed = Mathf.Max(0, launchSpeed);
			_gravityDirection = Vector3.down;
			UpdateData();
		}
		
		public TrajectoryData(Vector3 gravityDirection, float gravityMagnitude)
		{
			_gravityDirection = gravityDirection.normalized;
			_gravityMagnitude = Mathf.Max(0, gravityMagnitude);
			UpdateData();
		}
		
		public TrajectoryData(Vector3 origin, Vector3 launchDirection, float launchSpeed, Vector3 gravityDirection, float gravityMagnitude)
		{
			_origin = origin;
			_launchDirection = launchDirection.normalized;
			_launchSpeed = Mathf.Max(0, launchSpeed);
			_gravityDirection = Vector3.down;
			_gravityMagnitude = Mathf.Max(0, gravityMagnitude);
			UpdateData();
		}
		
		
		public void UpdateData()
		{
			_launchVelocity = _launchDirection * _launchSpeed;
			
			_launchVerticalDirection = (_gravityDirection * -1f).normalized;
			_launchVerticalSpeed = MathUtility.MagOfVectorInAxis(_launchVelocity, _launchVerticalDirection);

			_launchHorizontalDirection = MathUtility.RemoveAxisFromVector(_launchDirection, _gravityDirection).normalized;
			_launchHorizontalSpeed = MathUtility.MagOfVectorInAxis(_launchVelocity, _launchHorizontalDirection);
		}
		
		
		
		public void SetAllParamters(Vector3 origin, Vector3 launchDirection, float launchSpeed, Vector3 gravityDirection, float gravityMagnitude)
		{
			_origin = origin;
			_gravityMagnitude = Mathf.Max(0, gravityMagnitude);
			
			if (_launchDirection == launchDirection && 
			    Math.Abs(_launchSpeed - launchSpeed) < Mathf.Epsilon &&
			    _gravityDirection == gravityDirection) return;

			_launchDirection = launchDirection.normalized;
			_launchSpeed = Mathf.Max(0, launchSpeed);
			_gravityDirection = gravityDirection.normalized;
		}
		
		
		
		public void SetLaunchParameters(Vector3 origin)
		{
			_origin = origin;
		}
		
		public void SetLaunchParameters(Vector3 origin, Vector3 launchDirection)
		{
			SetLaunchParameters(origin, launchDirection, _launchSpeed);
		}
		
		public void SetLaunchParameters(Vector3 launchDirection, float launchSpeed)
		{
			SetLaunchParameters(_origin, launchDirection, launchSpeed);
		}
		
		public void SetLaunchParameters(Vector3 origin, Vector3 launchDirection, float launchSpeed)
		{
			_origin = origin;
			
			launchDirection.Normalize();
			
			if (_launchDirection == launchDirection && Math.Abs(_launchSpeed - launchSpeed) < Mathf.Epsilon) return;
			_launchDirection = launchDirection.normalized;
			_launchSpeed = Mathf.Max(0, launchSpeed);
			UpdateData();
		}
		
		
		public void SetGravityParameters(Vector3 gravityDirection)
		{
			SetGravityParameters(gravityDirection, _gravityMagnitude);
		}
		
		public void SetGravityParameters(Vector3 gravityDirection, float gravityMagnitude)
		{
			_gravityMagnitude = Mathf.Max(0, gravityMagnitude);
			
			gravityDirection.Normalize();
			if (_gravityDirection == gravityDirection) return;
			_gravityDirection = gravityDirection;
			UpdateData();
		}

	}
}