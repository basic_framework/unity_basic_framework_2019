﻿using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	public static class ArcTrajectory
	{
		public static float FlightTime(TrajectoryData trajectoryData, float displacement, bool isVerticalDisplacement)
		{
			if (!isVerticalDisplacement)
			{
				var launchHorizontalSpeed = trajectoryData.LaunchHorizontalSpeed;
				return launchHorizontalSpeed > 0 ? displacement / launchHorizontalSpeed : -1f;
			}
			
			if (MathUtility.QuadraticFormula(
				-trajectoryData.GravityMagnitude * 0.5f, 
				trajectoryData.LaunchVerticalSpeed, 
				-displacement,
				out var lower, out var upper))
			{
				return upper;
			}
			
			// Could not find a real solution
			return -1f;
		}
		
		public static float FlightTime(
			Vector3 initialDirection, float initialPower, 
			Vector3 gravityDirection, float gravityMagnitude, 
			float displacement, bool isVerticalDisplacement)
		{
			initialDirection = initialDirection.normalized;
			var initialVelocity = initialDirection * initialPower;

			
			if (!isVerticalDisplacement)
			{
				var horizontalDirection = MathUtility.RemoveAxisFromVector(initialDirection, gravityDirection).normalized;
				var initialHorizontalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, horizontalDirection);
				return initialHorizontalSpeed > 0 ? displacement / initialHorizontalSpeed : -1f;
			}
			
			var initialVerticalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, gravityDirection) * -1;
			
			if (MathUtility.QuadraticFormula(
				-gravityMagnitude * 0.5f, 
				initialVerticalSpeed, 
				-displacement,
				out var lower, out var upper))
			{
				return upper;
			}
			
			// Could not find a real solution
			return -1f;
		}
		
		public static Vector3 Solve(TrajectoryData trajectoryData, float time)
		{
			var horizontalDisplacement = trajectoryData.LaunchHorizontalSpeed * time;
			var verticalDisplacement = trajectoryData.LaunchVerticalSpeed * time - 0.5f * trajectoryData.GravityMagnitude * time * time;

			var displacement = horizontalDisplacement *  trajectoryData.LaunchHorizontalDirection + verticalDisplacement * trajectoryData.LaunchVerticalDirection;
			
			return trajectoryData.Origin + displacement;
		}
		
		public static void Solve(TrajectoryData trajectoryData, float timeStep, Vector3[] pathPositions)
		{
			if (pathPositions == null || pathPositions.Length == 0) return;

			var launchPosition = trajectoryData.Origin;

			var gravityMagnitude = trajectoryData.GravityMagnitude;

			var launchHorizontalSpeed = trajectoryData.LaunchHorizontalSpeed;
			var launchVerticalSpeed = trajectoryData.LaunchVerticalSpeed;
			
			var launchHorizontalDirection = trajectoryData.LaunchHorizontalDirection;
			var launchVerticalDirection = trajectoryData.LaunchVerticalDirection;
			
			for (int i = 0; i < pathPositions.Length; i++)
			{
				var time = timeStep * i;
				var horizontalDisplacement = launchHorizontalSpeed * time;
				var verticalDisplacement = launchVerticalSpeed * time - 0.5f * gravityMagnitude * time * time;
				var displacement = horizontalDisplacement *  launchHorizontalDirection + verticalDisplacement * launchVerticalDirection;
				
				pathPositions[i] = launchPosition + displacement;
			}
		}

	}
	
	
//	[Serializable]
//	public struct ArcTrajectorySettings
//	{
//		public Vector3 Origin;
//		public Vector3 Direction;
//		public float Speed;
//		public Vector3 GravityDirection;
//		public float GravityMagnitude;
//	}
//	
//	[Serializable]
//	public class ArcTrajectory : ITrajectory
//	{
//	
//		// **************************************** VARIABLES ****************************************\\
//
//		[SerializeField] private float _power = 5f;
//		[SerializeField] private Vector3 _gravityDirection = Vector3.down;
//		[SerializeField] private float _gravityMagnitude = 9.81f;
//		
//
//		public float Power
//		{
//			get => _power;
//			set => _power = value;
//		}
//
//		public Vector3 GravityDirection
//		{
//			get => _gravityDirection;
//			set => _gravityDirection = value;
//		}
//
//		public float GravityMagnitude
//		{
//			get => _gravityMagnitude;
//			set => _gravityMagnitude = value;
//		}
//		
//
//		// ****************************************  METHODS  ****************************************\\
//		
//		// Returns -1 if cannot calculate time
//		public float TimeOfFlight(Vector3 initialDirection, float displacement, bool isVerticalDisplacement)
//		{
//			return TimeOfFlight(
//				initialDirection,
//				_power, 
//				_gravityDirection, 
//				_gravityMagnitude, 
//				displacement, 
//				isVerticalDisplacement);
//		}
//
//		public float TimeOfFlight(
//			Vector3 initialDirection, float initialPower, 
//			Vector3 gravityDirection, float gravityMagnitude, 
//			float displacement, bool isVerticalDisplacement)
//		{
//			initialDirection = initialDirection.normalized;
//			var initialVelocity = initialDirection * initialPower;
//
//			
//			if (!isVerticalDisplacement)
//			{
//				var horizontalDirection = MathUtility.RemoveAxisFromVector(initialDirection, gravityDirection).normalized;
//				var initialHorizontalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, horizontalDirection);
//				return initialHorizontalSpeed > 0 ? displacement / initialHorizontalSpeed : -1f;
//			}
//			
//			var initialVerticalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, gravityDirection) * -1;
//			
//			if (MathUtility.QuadraticFormula(
//				-gravityMagnitude * 0.5f, 
//				initialVerticalSpeed, 
//				-displacement,
//				out var lower, out var upper))
//			{
//				return upper;
//			}
//			
//			// Could not find a real solution
//			return -1f;
//		}
//		
//
//		public BasicPose Solve(BasicPose originPose, float time)
//		{
//			// is .normalized required?
//			var launchDirection = (originPose.Rotation * Vector3.forward).normalized;
//
//			var horizontalDirection = MathUtility.RemoveAxisFromVector(launchDirection, _gravityDirection).normalized;
//			var verticalDirection = (_gravityDirection * -1f).normalized;
//			
//			var initialVelocity = launchDirection * _power;
//
//			var initialVerticalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, verticalDirection);
//			var initialHorizontalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, horizontalDirection);
//			
//			var horizontalDisplacement = initialHorizontalSpeed * time;
//			var verticalDisplacement = initialVerticalSpeed * time - 0.5f * _gravityMagnitude * time * time;
//
//			var displacement = horizontalDisplacement * horizontalDirection + verticalDisplacement * verticalDirection;
//
//			return new BasicPose(originPose.Position + displacement, originPose.Rotation);
//		}
//
//		public void Solve(BasicPose originPose, float timeStep, ref BasicPose[] poseCache)
//		{
//			if (poseCache == null || poseCache.Length == 0) return;
//			
//			var launchDirection = (originPose.Rotation * Vector3.forward).normalized;
//
//			var horizontalDirection = MathUtility.RemoveAxisFromVector(launchDirection, _gravityDirection).normalized;
//			var verticalDirection = (_gravityDirection * -1f).normalized;
//			
//			var initialVelocity = launchDirection * _power;
//
//			var initialVerticalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, verticalDirection);
//			var initialHorizontalSpeed = MathUtility.MagOfVectorInAxis(initialVelocity, horizontalDirection);
//
//			for (int i = 0; i < poseCache.Length; i++)
//			{
//				var time = timeStep * i;
//				var horizontalDisplacement = initialHorizontalSpeed * time;
//				var verticalDisplacement = initialVerticalSpeed * time - 0.5f * _gravityMagnitude * time * time;
//				var displacement = horizontalDisplacement * horizontalDirection + verticalDisplacement * verticalDirection;
//
//				var pose = originPose;
//				pose.Position += displacement;
//				poseCache[i] = pose;
//			}
//		}

//	}
}