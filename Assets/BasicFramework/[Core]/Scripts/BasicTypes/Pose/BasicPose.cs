﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct BasicPose 
	{

		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private Vector3 _position;

		[SerializeField] private Quaternion _rotation;
		
		// TODO: Check that the #if UNITY_EDITOR doesn't break stuff
#if UNITY_EDITOR
		[SerializeField] private Vector3 _editorEulerAngles;
#endif

		public Vector3 Position
		{
			get => _position;
			set => _position = value;
		}

		public Quaternion Rotation
		{
			get => _rotation;
			set
			{
				_rotation = value;
				
#if UNITY_EDITOR
				_editorEulerAngles = _rotation.eulerAngles;
#endif
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		
		//***** constructor *****\\

		public BasicPose(Transform transform)
		{
			_position = transform.position;
			_rotation = transform.rotation;
			
			if (!_rotation.IsValid()) 
				_rotation = Quaternion.identity;

#if UNITY_EDITOR
			_editorEulerAngles = _rotation.eulerAngles;
#endif
		}
		
		public BasicPose(Ray ray, Vector3 upDirection)
		{
			_position = ray.origin;
			_rotation = MathUtility.Direction2Quaternion(ray.direction, upDirection);
			
			if (!_rotation.IsValid()) 
				_rotation = Quaternion.identity;

#if UNITY_EDITOR
			_editorEulerAngles = _rotation.eulerAngles;
#endif
		}
		
		public BasicPose(Vector3 position, Quaternion rotation)
		{
			_position = position;
			_rotation = rotation;
			
			if (!_rotation.IsValid()) 
				_rotation = Quaternion.identity;
			
#if UNITY_EDITOR
			_editorEulerAngles = _rotation.eulerAngles;
#endif
		}
		
		
		//***** overloads *****\\

		public static bool operator ==(BasicPose left, BasicPose right)
		{
			return left._position == right._position 
			       && left._rotation == right._rotation;
		}

		public static bool operator !=(BasicPose left, BasicPose right)
		{
			return !(left == right);
		}
		
		public bool Equals(BasicPose other)
		{
			return _position.Equals(other._position) 
			       && _rotation.Equals(other._rotation) 
			       //&& _editorEulerAngles.Equals(other._editorEulerAngles)
				;
		}

		public override bool Equals(object obj)
		{
			return obj is BasicPose other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = _position.GetHashCode();
				hashCode = (hashCode * 397) ^ _rotation.GetHashCode();
				//hashCode = (hashCode * 397) ^ _editorEulerAngles.GetHashCode();
				return hashCode;
			}
		}


		//***** functions *****\\

		public static BasicPose Lerp(BasicPose start, BasicPose end, float time)
		{
			start.Position = Vector3.Lerp(start.Position, end.Position, time);
			start.Rotation = Quaternion.Lerp(start.Rotation, end.Rotation, time);

			return start;
		}
		
		public static BasicPose Slerp(BasicPose start, BasicPose end, float time)
		{
			start.Position = Vector3.Slerp(start.Position, end.Position, time);
			start.Rotation = Quaternion.Slerp(start.Rotation, end.Rotation, time);

			return start;
		}

	}
	
	[Serializable]
	public class BasicPoseUnityEvent : UnityEvent<BasicPose>{}
	
	[Serializable]
	public class BasicPoseCompareUnityEvent : CompareValueUnityEventBase<BasicPose>{}
	
	[Serializable]
	public class BasicPoseArrayUnityEvent : UnityEvent<BasicPose[]>{}
}