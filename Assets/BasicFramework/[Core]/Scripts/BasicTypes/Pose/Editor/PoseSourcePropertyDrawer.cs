﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(PoseSource))]
	public class PoseSourcePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float SOURCE_WIDTH = 80f;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position = EditorGUI.PrefixLabel(position, label);
			TempRect = position;
			TempRect.width = SOURCE_WIDTH;
			
			var sourceType = property.FindPropertyRelative("_sourceType");

			EditorGUI.PropertyField(TempRect, sourceType, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			TempRect.width = position.width - TempRect.width - BUFFER_WIDTH;
			
			var sourceTypeEnum =
				EnumUtility.EnumNameToEnumValue<PoseSourceTypes>(sourceType.enumNames[sourceType.enumValueIndex]);

			SerializedProperty sourceProperty;
			
			switch (sourceTypeEnum)
			{
				case PoseSourceTypes.Transform:
					sourceProperty = property.FindPropertyRelative("_transform");
					break;
				case PoseSourceTypes.Rigidbody:
					sourceProperty = property.FindPropertyRelative("_rigidbody");
					break;
				case PoseSourceTypes.BasicPose:
					sourceProperty = property.FindPropertyRelative("_basicPoseSingle");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			BasicEditorGui.PropertyFieldNotNull(TempRect, sourceProperty, GUIContent.none);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return SingleLineHeight;
		}
		
	}
}