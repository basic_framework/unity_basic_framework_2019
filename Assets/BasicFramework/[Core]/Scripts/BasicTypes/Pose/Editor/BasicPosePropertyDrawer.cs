﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(BasicPose))]
	public class BasicPosePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float EULER_SCALER = 3;
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded) return;

			var posePosition = property.FindPropertyRelative("_position");
			var poseRotation = property.FindPropertyRelative("_rotation");
			var editorEulerAngles = property.FindPropertyRelative("_editorEulerAngles");

			EditorGUI.PropertyField(TempRect, posePosition);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			EditorGUI.PropertyField(TempRect, editorEulerAngles, GetGuiContent("Rotation Euler"));
			poseRotation.quaternionValue = Quaternion.Euler(editorEulerAngles.vector3Value);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			GUI.enabled = false;
			EditorGUI.PropertyField(TempRect, poseRotation, GetGuiContent("Rotation"), true);
			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;

			height += SingleLineHeight + BUFFER_HEIGHT;
			height += SingleLineHeight + BUFFER_HEIGHT;
			
			var poseRotation = property.FindPropertyRelative("_rotation");
			
			height += GetPropertyHeight(poseRotation, true) + BUFFER_HEIGHT;
			
			return height;
		}
	}
}