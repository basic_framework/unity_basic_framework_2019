﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public class PoseSource 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private PoseSourceTypes _sourceType = PoseSourceTypes.Transform; 
		
		[SerializeField] private Transform _transform = default;
		[SerializeField] private Rigidbody _rigidbody = default;
		[SerializeField] private BasicPoseSingle _basicPoseSingle = default;

		
		public PoseSourceTypes SourceType
		{
			get => _sourceType;
			set => _sourceType = value;
		}

		public Transform Transform
		{
			get => _transform;
			set => _transform = value;
		}

		public Rigidbody Rigidbody
		{
			get => _rigidbody;
			set => _rigidbody = value;
		}

		public BasicPoseSingle BasicPoseSingle
		{
			get => _basicPoseSingle;
			set => _basicPoseSingle = value;
		}


		public Vector3 Position
		{
			get
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:	
						return _transform.position;
					
					case PoseSourceTypes.Rigidbody:
						return Application.isPlaying ? _rigidbody.position : _rigidbody.transform.position;
					
					case PoseSourceTypes.BasicPose:	
						return _basicPoseSingle.Value.Position;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			set
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:
						_transform.position = value;
						break;
					
					case PoseSourceTypes.Rigidbody:
						if (Application.isPlaying)
							_rigidbody.MovePosition(value);
						else
							_rigidbody.transform.position = value;
						break;
					
					case PoseSourceTypes.BasicPose:
						var basicPose = _basicPoseSingle.Value;
						basicPose.Position = value;
						_basicPoseSingle.Value = basicPose;
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		
		public Quaternion Rotation
		{
			get
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:	
						return _transform.rotation;
					
					case PoseSourceTypes.Rigidbody:
						return Application.isPlaying ? _rigidbody.rotation : _rigidbody.transform.rotation;
					
					case PoseSourceTypes.BasicPose:	
						return _basicPoseSingle.Value.Rotation;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			set
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:
						_transform.rotation = value;
						break;
					
					case PoseSourceTypes.Rigidbody:
						if (Application.isPlaying)
							_rigidbody.MoveRotation(value);
						else
							_rigidbody.transform.rotation = value;
						break;
					
					case PoseSourceTypes.BasicPose:
						var basicPose = _basicPoseSingle.Value;
						basicPose.Rotation = value;
						_basicPoseSingle.Value = basicPose;
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public BasicPose Pose
		{
			get
			{
				BasicPose basicPose;
				
				switch (_sourceType)
				{
						
					case PoseSourceTypes.Transform:
						basicPose= new BasicPose(_transform.position, _transform.rotation);
						break;
					case PoseSourceTypes.Rigidbody:
						if (Application.isPlaying)
						{
							basicPose = new BasicPose(_rigidbody.position, _rigidbody.rotation);
						}
						else
						{
							var transform = _rigidbody.transform;
							basicPose = new BasicPose(transform.position, transform.rotation);
						}
						break;
					case PoseSourceTypes.BasicPose:
						basicPose = _basicPoseSingle.Value;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				return basicPose;
			}

			set
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:
						_transform.position = value.Position;
						_transform.rotation = value.Rotation;
						break;
					case PoseSourceTypes.Rigidbody:
						if (Application.isPlaying)
						{
							_rigidbody.position = value.Position;
							_rigidbody.rotation = value.Rotation;
						}
						else
						{
							var transform = _rigidbody.transform;
							transform.position = value.Position;
							transform.rotation = value.Rotation;
						}
						break;
					case PoseSourceTypes.BasicPose:
						_basicPoseSingle.Value = value;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}


		public bool IsValid
		{
			get
			{
				switch (_sourceType)
				{
					case PoseSourceTypes.Transform:
						return _transform != null;
						
					case PoseSourceTypes.Rigidbody:
						return _rigidbody != null;
						
					case PoseSourceTypes.BasicPose:
						return _basicPoseSingle != null;
						
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		
		// ****************************************  METHODS  ****************************************\\
		
	
	}
}