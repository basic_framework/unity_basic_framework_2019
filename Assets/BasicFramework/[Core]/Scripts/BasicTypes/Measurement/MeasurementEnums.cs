﻿using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	// Note: ISO only goes up to power 24
	public enum MetricPrefixes
	{
		octillion_10EP27,
		yotta_10EP24,
		zetta_10EP21,
		exa_10EP18,		
		peta_10EP15,		
		tera_10EP12,		
		giga_10EP09,		
		mega_10EP06,		
		kilo_10EP03,		
		unit_10E00,		
		milli_10EN03,		
		micro_10EN06,		
		nano_10EN09,		
		pico_10EN12,		
		femto_10EN15,		
		atto_10EN18,
		zepto_10EN21,
		yocto_10EN24,
		octillionth_10EN27,
	}
	
	
}