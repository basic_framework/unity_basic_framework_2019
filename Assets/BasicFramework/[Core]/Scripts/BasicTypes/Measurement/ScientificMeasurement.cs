﻿using System;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct ScientificMeasurement 
	{
	
		// TODO: Move to basic framework
		
		// **************************************** VARIABLES ****************************************\\

		public static ScientificMeasurement Default = new ScientificMeasurement(0f, MetricPrefixes.unit_10E00);
		
		[SerializeField] private float _value;
		[SerializeField] private MetricPrefixes _metricPrefix;

		public float Value
		{
			get => _value;
			set => _value = value;
		}

		public MetricPrefixes MetricPrefix
		{
			get => _metricPrefix;
			set => _metricPrefix = value;
		}


		// ****************************************  METHODS  ****************************************\\

		public ScientificMeasurement(float value, MetricPrefixes metricPrefix)
		{
			_value = value;
			_metricPrefix = metricPrefix;
		}

		public static bool operator ==(ScientificMeasurement left, ScientificMeasurement right)
		{
			return Math.Abs(left._value - right._value) < Mathf.Epsilon && left._metricPrefix == right._metricPrefix;
		}

		public static bool operator !=(ScientificMeasurement left, ScientificMeasurement right)
		{
			return !(left == right);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override string ToString()
		{
			string power;
			
			switch (_metricPrefix)
			{
				case MetricPrefixes.octillion_10EP27:
					power = "27";
					break;
				case MetricPrefixes.yotta_10EP24:
					power = "24";
					break;
				case MetricPrefixes.zetta_10EP21:
					power = "21";
					break;
				case MetricPrefixes.exa_10EP18:
					power = "18";
					break;
				case MetricPrefixes.peta_10EP15:
					power = "15";
					break;
				case MetricPrefixes.tera_10EP12:
					power = "12";
					break;
				case MetricPrefixes.giga_10EP09:
					power = "09";
					break;
				case MetricPrefixes.mega_10EP06:
					power = "06";
					break;
				case MetricPrefixes.kilo_10EP03:
					power = "03";
					break;
				case MetricPrefixes.unit_10E00:
					power = "00";
					break;
				case MetricPrefixes.milli_10EN03:
					power = "-03";
					break;
				case MetricPrefixes.micro_10EN06:
					power = "-06";
					break;
				case MetricPrefixes.nano_10EN09:
					power = "-09";
					break;
				case MetricPrefixes.pico_10EN12:
					power = "-12";
					break;
				case MetricPrefixes.femto_10EN15:
					power = "-15";
					break;
				case MetricPrefixes.atto_10EN18:
					power = "-18";
					break;
				case MetricPrefixes.zepto_10EN21:
					power = "-21";
					break;
				case MetricPrefixes.yocto_10EN24:
					power = "-24";
					break;
				case MetricPrefixes.octillionth_10EN27:
					power = "-27";
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return $"{_value} * 10E{power}";
//			return base.ToString();
		}

		// The actual unit value, i.e. value without MetricPrefix
		public decimal GetUnitValue()
		{
			switch (_metricPrefix)
			{
				case MetricPrefixes.octillion_10EP27:
					return ((decimal) _value) * 10E27M; 
				
				case MetricPrefixes.yotta_10EP24:
					return ((decimal) _value) * 10E24M; 
				
				case MetricPrefixes.zetta_10EP21:
					return ((decimal) _value) * 10E21M; 
				
				case MetricPrefixes.exa_10EP18:
					return ((decimal) _value) * 10E18M; 
				
				case MetricPrefixes.peta_10EP15:
					return ((decimal) _value) * 10E15M; 
				
				case MetricPrefixes.tera_10EP12:
					return ((decimal) _value) * 10E12M; 
				
				case MetricPrefixes.giga_10EP09:
					return ((decimal) _value) * 10E09M; 
				
				case MetricPrefixes.mega_10EP06:
					return ((decimal) _value) * 10E06M; 
				
				case MetricPrefixes.kilo_10EP03:
					return ((decimal) _value) * 10E03M; 
				
				case MetricPrefixes.unit_10E00:
					return ((decimal) _value) * 10E00M; 
				
				case MetricPrefixes.milli_10EN03:
					return ((decimal) _value) * 10E-03M; 
				
				case MetricPrefixes.micro_10EN06:
					return ((decimal) _value) * 10E-06M; 
				
				case MetricPrefixes.nano_10EN09:
					return ((decimal) _value) * 10E-09M; 
				
				case MetricPrefixes.pico_10EN12:
					return ((decimal) _value) * 10E-12M; 
				
				case MetricPrefixes.femto_10EN15:
					return ((decimal) _value) * 10E-15M; 
				
				case MetricPrefixes.atto_10EN18:
					return ((decimal) _value) * 10E-18M; 
				
				case MetricPrefixes.zepto_10EN21:
					return ((decimal) _value) * 10E-21M; 
				
				case MetricPrefixes.yocto_10EN24:
					return ((decimal) _value) * 10E-24M; 
				
				case MetricPrefixes.octillionth_10EN27:
					return ((decimal) _value) * 10E-27M; 
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}