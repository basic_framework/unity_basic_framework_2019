﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(ScientificMeasurement))]
	public class ScientificMeasurementPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = EditorGUI.PrefixLabel(position, label);
			var width = TempRect.width * 0.5f - BUFFER_WIDTH;
			TempRect.width = width;

			var value = property.FindPropertyRelative("_value");
			var metricPrefix = property.FindPropertyRelative("_metricPrefix");

			EditorGUI.PropertyField(TempRect, value, GUIContent.none);

			TempRect.x += width + BUFFER_WIDTH;
			EditorGUI.PropertyField(TempRect, metricPrefix, GUIContent.none);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return SingleLineHeight;
//			return base.GetPropertyHeight(property, label);
		}
		
	}
}