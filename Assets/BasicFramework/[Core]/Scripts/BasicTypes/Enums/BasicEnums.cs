﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
	[Flags]
	public enum PoseFlags
	{
		Position = 1 << 0,
		Rotation = 1 << 1
	}
	
	public enum PoseSourceTypes
	{
		Transform,
		Rigidbody,
		BasicPose
	}

	[Flags]
	public enum SetupCallbackFlags
	{
		Awake = 1 << 0,
		Enable = 1 << 1,
		Start = 1 << 2
	}

	[Flags]
	public enum UpdateCallbackFlags
	{
		Update 		= 1 << 0,
		LateUpdate	= 1 << 1,
		FixedUpdate = 1 << 2
	}

	public enum UpdateCallbacks
	{
		Nothing,
		Update,
		LateUpdate,
		FixedUpdate
	}
	
	public enum RaiseConditions
	{
		Changed,
		EqualTo,
		NotEqualTo,
		PreviouslyEqualTo
	}

}