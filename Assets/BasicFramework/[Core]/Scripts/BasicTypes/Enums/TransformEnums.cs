﻿using System;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
    public enum AxisAlignedDirections
    {
        Right,
        Left,
        Up,
        Down,
        Forward,
        Back
    }
	
    [Flags]
    public enum AxisAlignedDirectionFlags
    {
        Right 	= 1 << 0,
        Left	= 1 << 1,
        Up		= 1 << 2,
        Down	= 1 << 3,
        Forward	= 1 << 4,
        Back	= 1 << 5
    }

    public enum LinearAxis
    {
        X,
        Y,
        Z
    }

    [Flags]
    public enum LinearAxisFlags
    {
        X 	= 1 << 0,
        Y	= 1 << 1,
        Z	= 1 << 2,
    }
	
    public enum RotationAxis
    {
        Pitch,
        Yaw,
        Roll
    }

    [Flags]
    public enum RaotationAxisFlags
    {
        Pitch 	= 1 << 0,
        Yaw		= 1 << 1,
        Roll	= 1 << 2,
    }
    
    public enum LinearAndRotationAxis
    {
        X,
        Y,
        Z,
        Pitch,
        Yaw,
        Roll
    }
    
    
    [Flags]
    public enum LinearAndRotationAxisFlags
    {
        X 	    = 1 << 0,
        Y	    = 1 << 1,
        Z		= 1 << 2,
        Pitch	= 1 << 3,
        Yaw	    = 1 << 4,
        Roll	= 1 << 5
    }
    
}