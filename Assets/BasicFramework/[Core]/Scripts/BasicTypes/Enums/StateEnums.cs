﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
	public enum TriBool
	{
		Indetermined,
		False,
		True
	}
	
	[Serializable]
	public class TriBoolUnityEvent : UnityEvent<TriBool>{}

	[Serializable]
	public class TriBoolCompareUnityEvent : CompareValueUnityEventBase<TriBool>{}
}