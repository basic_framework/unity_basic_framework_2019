﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public class BasicDateTimePlusUnityEvent 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private StringUnityEvent _stringDateTime = new StringUnityEvent();
		
		[SerializeField] private StringUnityEvent _stringDate = new StringUnityEvent();
		
		[SerializeField] private StringUnityEvent _stringTimeOfDay = new StringUnityEvent();
		
		
		[SerializeField] private BasicDateTimeUnityEvent _basicDateTime = new BasicDateTimeUnityEvent();
		
		[SerializeField] private DateTimeUnityEvent _dateTime = new DateTimeUnityEvent();
	
		[SerializeField] private DateTimeUnityEvent _date = new DateTimeUnityEvent();
		
		[SerializeField] private TimeSpanUnityEvent _timeOfDay = new TimeSpanUnityEvent();


		public StringUnityEvent StringDateTime => _stringDateTime;

		public StringUnityEvent StringDate => _stringDate;

		public StringUnityEvent StringTimeOfDay => _stringTimeOfDay;

		public BasicDateTimeUnityEvent BasicDateTime => _basicDateTime;

		public DateTimeUnityEvent DateTime => _dateTime;

		public DateTimeUnityEvent Date => _date;

		public TimeSpanUnityEvent TimeOfDay => _timeOfDay;


		// ****************************************  METHODS  ****************************************\\

		public void Raise(BasicDateTime basicDateTime)
		{
			Raise(basicDateTime.DateTime);
		}
		
		public void Raise(DateTime dateTime)
		{
			_stringDateTime.Invoke(dateTime.ToString());
			
			_stringDate.Invoke(dateTime.ToShortDateString());
			
			_stringTimeOfDay.Invoke(dateTime.ToShortTimeString());
			
			_basicDateTime.Invoke(new BasicDateTime(dateTime));
			
			_dateTime.Invoke(dateTime);
			
			_date.Invoke(dateTime.Date);
			
			_timeOfDay.Invoke(dateTime.TimeOfDay);
		}
		
	
	}
}