﻿using System;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Editor
{
	[CustomPropertyDrawer(typeof(BasicDateTime))]
	public class BasicDateTimePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// TODO: Maybe toggle between dropdowns
		
		// **************************************** VARIABLES ****************************************\\
		
		
	
		// ****************************************  METHODS  ****************************************\\


		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded) return;

			
			
			var minDateTimeValue = DateTime.MinValue;
			var maxDateTimeValue = DateTime.MaxValue;
			
			
			//***** Date *****\\
			
			var year = property.FindPropertyRelative("_year");
			var month = property.FindPropertyRelative("_month");
			var day = property.FindPropertyRelative("_day");
			
			var hour = property.FindPropertyRelative("_hour");
			var minute = property.FindPropertyRelative("_minute");
			var second = property.FindPropertyRelative("_second");
			var millisecond = property.FindPropertyRelative("_millisecond");

			TempRect.width -= INDENT;
			TempRect.x += INDENT;
			
			TempRect = EditorGUI.PrefixLabel(TempRect, GetGuiContent("Date YYYY/MM/DD"));
			var singleCharWidth = (TempRect.width - 3 * BUFFER_WIDTH) / 8;
			
			TempRect.width = singleCharWidth * 2;
			if(GUI.Button(TempRect, GetGuiContent("Now", "Set the DateTime to now")))
			{
				var now = DateTime.Now;
				
				year.intValue = now.Year;
				month.intValue = now.Month;
				day.intValue = now.Day;

				hour.intValue = now.Hour;
				minute.intValue = now.Minute;
				second.intValue = now.Second;
				millisecond.intValue = now.Millisecond;
			}
			TempRect.x += TempRect.width + BUFFER_WIDTH;

			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, year, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			year.intValue = Mathf.Clamp(year.intValue, minDateTimeValue.Year, maxDateTimeValue.Year);
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, month, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			month.intValue = Mathf.Clamp(month.intValue, 1, 12);
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, day, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			day.intValue = Mathf.Clamp(day.intValue, 1, DateTime.DaysInMonth(year.intValue, month.intValue));
			
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			TempRect.width = position.width;
			TempRect.x = position.x;
			
			
			//***** TIME *****\\
			
			TempRect.width -= INDENT;
			TempRect.x += INDENT;
			
			TempRect = EditorGUI.PrefixLabel(TempRect, GetGuiContent("Time HH/MM/SS/mm"));
			singleCharWidth = (TempRect.width - 3 * BUFFER_WIDTH) / 8;
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, hour, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			hour.intValue = Mathf.Clamp(hour.intValue, 0, 23);
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, minute, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			minute.intValue = Mathf.Clamp(minute.intValue, 0, 59);
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, second, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			second.intValue = Mathf.Clamp(second.intValue, 0, 59);
			
			TempRect.width = singleCharWidth * 2;
			EditorGUI.PropertyField(TempRect, millisecond, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			millisecond.intValue = Mathf.Clamp(millisecond.intValue, 0, 99);
			
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			TempRect.width = position.width;

		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;

			height += BUFFER_HEIGHT + SingleLineHeight;
			height += BUFFER_HEIGHT + SingleLineHeight;
			
			return height;
		}
	}
}