﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes
{
	[Serializable]
	public struct BasicDateTime
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		public static BasicDateTime MinValue = new BasicDateTime(DateTime.MinValue);
		public static BasicDateTime MaxValue = new BasicDateTime(DateTime.MaxValue);
		public static BasicDateTime Today = new BasicDateTime(DateTime.Today);
		public static BasicDateTime Now = new BasicDateTime(DateTime.Now);
		
		[SerializeField] private int _year;
		[SerializeField] private int _month;
		[SerializeField] private int _day;

		[SerializeField] private int _hour;
		[SerializeField] private int _second;
		[SerializeField] private int _minute;
		[SerializeField] private int _millisecond;
		
		public DateTime DateTime
		{
			get => new DateTime(_year, _month, _day, _hour, _minute, _second, _millisecond);
			set
			{
				_year = value.Year;
				_month = value.Month;
				_day = value.Day;
				
				_hour = value.Hour;
				_minute = value.Minute;
				_second = value.Second;
				_millisecond = value.Millisecond;
			}
		}
		
		
		// ****************************************  METHODS  ****************************************\\
	
		public BasicDateTime(DateTime dateTime)
		{
			_year = dateTime.Year;
			_month = dateTime.Month;
			_day = dateTime.Day;
			
			_hour = dateTime.Hour;
			_minute = dateTime.Minute;
			_second = dateTime.Second;
			_millisecond = dateTime.Millisecond;
		}

		public override string ToString()
		{
			return DateTime.ToString();
		}
	}
	
	[Serializable]
	public class BasicDateTimeUnityEvent : UnityEvent<BasicDateTime>{}
	
	[Serializable]
	public class BasicDateTimeCompareUnityEvent : CompareValueUnityEventBase<BasicDateTime>{}
	
	[Serializable]
	public class BasicDateTimeArrayUnityEvent : UnityEvent<BasicDateTime[]>{}
}