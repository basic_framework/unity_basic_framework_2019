﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	[CustomPropertyDrawer(typeof(ScriptableSingleCondition))]
	public class ScriptableSingleConditionPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			if (!property.isExpanded) return;

			var conditionType = property.FindPropertyRelative("_conditionType");
			var invert = property.FindPropertyRelative("_invert");

			var conditionTypeEnum =
				EnumUtility.EnumNameToEnumValue<ScriptableSingleConditionTypes>(
					conditionType.enumNames[conditionType.enumValueIndex]);
			
			
			TempRect.height = GetPropertyHeight(conditionType, true);
			EditorGUI.PropertyField(TempRect, conditionType);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			TempRect.height = GetPropertyHeight(invert, true);
			EditorGUI.PropertyField(TempRect, invert);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			switch (conditionTypeEnum)
			{
				case ScriptableSingleConditionTypes.BoolSingle:
					
					var singleBool = property.FindPropertyRelative("_singleBool");
					var compareValueBool = property.FindPropertyRelative("_compareValueBool");
					
					TempRect.height = GetPropertyHeight(singleBool, true);
					BasicEditorGui.PropertyFieldNotNull(TempRect, singleBool, true);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					
					TempRect.height = GetPropertyHeight(compareValueBool, true);
					EditorGUI.PropertyField(TempRect, compareValueBool);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					
					break;
				
				case ScriptableSingleConditionTypes.IntSingle:
					
					var singleInt = property.FindPropertyRelative("_singleInt");
					var compareValueInt = property.FindPropertyRelative("_compareValueInt");
					
					TempRect.height = GetPropertyHeight(singleInt, true);
					BasicEditorGui.PropertyFieldNotNull(TempRect, singleInt, true);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					
					TempRect.height = GetPropertyHeight(compareValueInt, true);
					EditorGUI.PropertyField(TempRect, compareValueInt);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
				
					break;
				
				case ScriptableSingleConditionTypes.StringSingle:
					
					var singleString = property.FindPropertyRelative("_singleString");
					var compareValueString = property.FindPropertyRelative("_compareValueString");
					
					TempRect.height = GetPropertyHeight(singleString, true);
					BasicEditorGui.PropertyFieldNotNull(TempRect, singleString, true);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					
					TempRect.height = GetPropertyHeight(compareValueString, true);
					EditorGUI.PropertyField(TempRect, compareValueString);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;

					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;
			
			var conditionType = property.FindPropertyRelative("_conditionType");
			var invert = property.FindPropertyRelative("_invert");

			var conditionTypeEnum =
				EnumUtility.EnumNameToEnumValue<ScriptableSingleConditionTypes>(
					conditionType.enumNames[conditionType.enumValueIndex]);
			
			var singleBool = property.FindPropertyRelative("_singleBool");
			var compareValueBool = property.FindPropertyRelative("_compareValueBool");
			
			var singleInt = property.FindPropertyRelative("_singleInt");
			var compareValueInt = property.FindPropertyRelative("_compareValueInt");
			
			var singleString = property.FindPropertyRelative("_singleString");
			var compareValueString = property.FindPropertyRelative("_compareValueString");

			height += BUFFER_HEIGHT + GetPropertyHeight(conditionType, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(invert, true);
			
			switch (conditionTypeEnum)
			{
				case ScriptableSingleConditionTypes.BoolSingle:
					height += BUFFER_HEIGHT + GetPropertyHeight(singleBool, true);
					height += BUFFER_HEIGHT + GetPropertyHeight(compareValueBool, true);
					break;
				
				case ScriptableSingleConditionTypes.IntSingle:
					height += BUFFER_HEIGHT + GetPropertyHeight(singleInt, true);
					height += BUFFER_HEIGHT + GetPropertyHeight(compareValueInt, true);
					break;
				
				case ScriptableSingleConditionTypes.StringSingle:
					height += BUFFER_HEIGHT + GetPropertyHeight(singleString, true);
					height += BUFFER_HEIGHT + GetPropertyHeight(compareValueString, true);
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			return height;
		}
	
	}
}