﻿using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
	public enum ScriptableSingleConditionTypes
	{
		BoolSingle,
		IntSingle,
		StringSingle,
	}
	
	[Serializable]
	public class ScriptableSingleCondition 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private ScriptableSingleConditionTypes _conditionType = ScriptableSingleConditionTypes.BoolSingle;
		[SerializeField] private bool _invert = default;
		
		[SerializeField] private BoolSingle _singleBool = default;
		[SerializeField] private bool _compareValueBool = default;
		
		[SerializeField] private IntSingle _singleInt = default;
		[SerializeField] private int _compareValueInt = default;
		
		[SerializeField] private StringSingle _singleString = default;
		[SerializeField] private string _compareValueString = default;
		
		public bool IsValid
		{
			get
			{
				switch (_conditionType)
				{
					case ScriptableSingleConditionTypes.BoolSingle:
						return _singleBool != null;
					case ScriptableSingleConditionTypes.IntSingle:
						return _singleInt != null;
					case ScriptableSingleConditionTypes.StringSingle:
						return _singleString != null;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		public bool CheckCondition()
		{
			bool value;
			
			switch (_conditionType)
			{
				case ScriptableSingleConditionTypes.BoolSingle:
					value = _singleBool.Value == _compareValueBool;
					break;
				
				case ScriptableSingleConditionTypes.IntSingle:
					value = _singleInt.Value == _compareValueInt;
					break;
				
				case ScriptableSingleConditionTypes.StringSingle:
					value = _singleString.Value == _compareValueString;
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			return !_invert ? value : !value;
		}
	}
}