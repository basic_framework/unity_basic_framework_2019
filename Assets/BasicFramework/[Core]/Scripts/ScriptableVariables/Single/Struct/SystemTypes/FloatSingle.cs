using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Float", 
        fileName = "single_float_", 
        order = ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatSingle : SingleStructBase<float> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(FloatSingle value)
        {
            Value = value.Value;
        }

        public override bool Compare(float left, float right)
        {
            return Math.Abs(left - right) < Mathf.Epsilon;
        }
        
    }

    [Serializable]
    public class FloatSingleReference : SingleStructReferenceBase<float, FloatSingle>
    {
        public FloatSingleReference()
        {
        }

        public FloatSingleReference(float value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class FloatSingleUnityEvent : UnityEvent<FloatSingle>{}
	
    [Serializable]
    public class FloatSingleArrayUnityEvent : UnityEvent<FloatSingle[]>{}
    
}