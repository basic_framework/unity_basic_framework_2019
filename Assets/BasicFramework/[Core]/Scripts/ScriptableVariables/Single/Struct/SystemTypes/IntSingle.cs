using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Int", 
        fileName = "single_int_", 
        order = ScriptableVariablesMenuOrder.INT)
    ]
    public class IntSingle : SingleStructBase<int> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(IntSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class IntSingleReference : SingleStructReferenceBase<int, IntSingle>
    {
        public IntSingleReference()
        {
        }

        public IntSingleReference(int value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class IntSingleUnityEvent : UnityEvent<IntSingle>{}
	
    [Serializable]
    public class IntSingleArrayUnityEvent : UnityEvent<IntSingle[]>{}
    
}