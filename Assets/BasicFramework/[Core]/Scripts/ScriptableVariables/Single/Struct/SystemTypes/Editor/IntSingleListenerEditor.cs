using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(IntSingleListener))][CanEditMultipleObjects]
    public class IntSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    int, 
	    IntSingle, 
	    IntUnityEvent,
	    IntCompareUnityEvent,
	    IntSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}