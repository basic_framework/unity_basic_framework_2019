using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Float Single Listener",
        ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatSingleListener : SingleStructListenerBase
    <
        float, 
        FloatSingle, 
        FloatUnityEvent, 
        FloatCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        [Range(0, 10)]
        [Tooltip("The number of decimal places to round the string value to")]
        [SerializeField] private int _stringDecimalPlaces = 2;
        

        // ****************************************  METHODS  ****************************************\\
        
        protected override string ValueToString(float value)
        {
            value = MathUtility.RoundToDecimalPlace(value, _stringDecimalPlaces);
            return base.ValueToString(value);
        }
        
    }
}