using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(BoolSingleListener))][CanEditMultipleObjects]
    public class BoolSingleListenerEditor : SingleStructListenerBaseEditor
    <
        bool, 
        BoolSingle, 
        BoolUnityEvent, 
        BoolCompareValueUnityEvent, 
        BoolSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawEventFields()
        {
            SectionHeader("Single Listener Events");
            
            var valueChangedString = serializedObject.FindProperty("_valueChangedString");

            var valueChangedPlus = serializedObject.FindProperty("_valueChangedPlus");

            var value = valueChangedPlus.FindPropertyRelative("_value");
            var invertedValue = valueChangedPlus.FindPropertyRelative("_invertedValue");
            var isTrue = valueChangedPlus.FindPropertyRelative("_isTrue");
            var isFalse = valueChangedPlus.FindPropertyRelative("_isFalse");
            
            EditorGUILayout.PropertyField(valueChangedString);

            EditorGUILayout.PropertyField(value);
            EditorGUILayout.PropertyField(invertedValue);
            EditorGUILayout.PropertyField(isTrue);
            EditorGUILayout.PropertyField(isFalse);
        }
        
    }
}