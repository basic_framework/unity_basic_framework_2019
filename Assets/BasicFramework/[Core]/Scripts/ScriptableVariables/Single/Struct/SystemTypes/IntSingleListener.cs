using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Int Single Listener",
        ScriptableVariablesMenuOrder.INT)
    ]
    public class IntSingleListener : SingleStructListenerBase
    <
        int, 
        IntSingle, 
        IntUnityEvent, 
        IntCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}