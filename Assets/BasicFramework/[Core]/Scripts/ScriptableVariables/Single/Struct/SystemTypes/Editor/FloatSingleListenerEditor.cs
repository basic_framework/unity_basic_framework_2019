using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(FloatSingleListener))][CanEditMultipleObjects]
    public class FloatSingleListenerEditor : SingleStructListenerBaseEditor
    <
        float, 
        FloatSingle, 
        FloatUnityEvent,
        FloatCompareUnityEvent,
        FloatSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            var stringDecimalPlaces = serializedObject.FindProperty("_stringDecimalPlaces");
	        
            Space();
            SectionHeader($"{nameof(FloatSingleListener).CamelCaseToHumanReadable()} Properties");

            EditorGUILayout.PropertyField(stringDecimalPlaces);
        }
    }
}