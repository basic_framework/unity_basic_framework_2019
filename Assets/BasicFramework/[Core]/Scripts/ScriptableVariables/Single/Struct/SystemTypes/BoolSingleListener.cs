using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/Single/Bool Single Listener",
	    ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolSingleListener : SingleStructListenerBase
    <
	    bool, 
	    BoolSingle, 
	    BoolUnityEvent, 
	    BoolCompareValueUnityEvent
    >
    {
	    
	    // TODO: Inherit from Monobehaviour and use BoolPlusUnityEvent
	
        // **************************************** VARIABLES ****************************************\\
        
        [Tooltip("Response to invoke when value is changed")]
        [SerializeField] protected BoolPlusUnityEvent _valueChangedPlus = default;
		
        
        // ****************************************  METHODS  ****************************************\\

        protected override void OnRaiseValueChanged(ScriptableSingleBase<bool> scriptableSingleBase)
        {
	        base.OnRaiseValueChanged(scriptableSingleBase);
	        
	        _valueChangedPlus.Raise(scriptableSingleBase.Value);
        }
        
    }
}