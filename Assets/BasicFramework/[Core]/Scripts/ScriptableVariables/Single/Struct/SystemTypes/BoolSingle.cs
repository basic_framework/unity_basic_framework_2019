using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/bool", 
        fileName = "single_bool_", 
        order = ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolSingle : SingleStructBase<bool> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(BoolSingle value)
        {
            Value = value.Value;
        }
		
        public void Toggle()
        {
            Value = !Value;
        }
		
    }

    [Serializable]
    public class BoolSingleReference : SingleStructReferenceBase<bool, BoolSingle>
    {
        public BoolSingleReference()
        {
        }

        public BoolSingleReference(bool value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class BoolSingleUnityEvent : UnityEvent<BoolSingle>{}
	
    [Serializable]
    public class BoolSingleArrayUnityEvent : UnityEvent<BoolSingle[]>{}
    
}