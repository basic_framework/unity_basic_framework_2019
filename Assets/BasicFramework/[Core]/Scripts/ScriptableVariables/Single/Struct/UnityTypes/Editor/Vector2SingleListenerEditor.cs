using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(Vector2SingleListener))][CanEditMultipleObjects]
    public class Vector2SingleListenerEditor : SingleStructListenerBaseEditor
    <
	    Vector2, 
	    Vector2Single, 
	    Vector2UnityEvent,
	    Vector2CompareUnityEvent,
	    Vector2SingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}