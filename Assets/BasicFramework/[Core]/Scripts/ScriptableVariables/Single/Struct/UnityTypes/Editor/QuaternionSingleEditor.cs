using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(QuaternionSingle))][CanEditMultipleObjects]
    public class QuaternionSingleEditor : SingleStructBaseEditor<Quaternion, QuaternionSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(QuaternionSingle))]
    public class QuaternionSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(QuaternionSingleReference))]
    public class QuaternionSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}