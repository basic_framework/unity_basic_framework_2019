using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(LayerMaskSingleListener))][CanEditMultipleObjects]
    public class LayerMaskSingleListenerEditor : SingleStructListenerBaseEditor
    <
        LayerMask, 
        LayerMaskSingle, 
        LayerMaskUnityEvent,
        LayerMaskCompareUnityEvent,
        LayerMaskSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}