using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Vector3 Single Listener",
        ScriptableVariablesMenuOrder.VECTOR3)
    ]
    public class Vector3SingleListener : SingleStructListenerBase
    <
        Vector3, 
        Vector3Single, 
        Vector3UnityEvent, 
        Vector3CompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}