using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Vector2 Single Listener",
        ScriptableVariablesMenuOrder.VECTOR2)
    ]
    public class Vector2SingleListener : SingleStructListenerBase
    <
        Vector2, 
        Vector2Single, 
        Vector2UnityEvent, 
        Vector2CompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}