using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Vector3", 
        fileName = "single_vector3_", 
        order = ScriptableVariablesMenuOrder.VECTOR3)
    ]
    public class Vector3Single : SingleStructBase<Vector3> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(Vector3Single value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class Vector3SingleReference : SingleStructReferenceBase<Vector3, Vector3Single>
    {
        public Vector3SingleReference()
        {
        }

        public Vector3SingleReference(Vector3 value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class Vector3SingleUnityEvent : UnityEvent<Vector3Single>{}
	
    [Serializable]
    public class Vector3SingleArrayUnityEvent : UnityEvent<Vector3Single[]>{}
    
}