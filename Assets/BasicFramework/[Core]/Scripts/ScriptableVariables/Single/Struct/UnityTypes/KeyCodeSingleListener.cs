using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Key Code Single Listener",
        ScriptableVariablesMenuOrder.KEY_CODE)
    ]
    public class KeyCodeSingleListener : SingleStructListenerBase
    <
        KeyCode, 
        KeyCodeSingle, 
        KeyCodeUnityEvent, 
        KeyCodeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}