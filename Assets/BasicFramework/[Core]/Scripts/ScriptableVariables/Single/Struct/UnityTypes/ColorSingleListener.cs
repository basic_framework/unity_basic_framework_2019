using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Color Single Listener",
        ScriptableVariablesMenuOrder.COLOR)
    ]
    public class ColorSingleListener : SingleStructListenerBase
    <
        Color, 
        ColorSingle, 
        ColorUnityEvent, 
        ColorCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}