using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Layer Mask Single Listener",
        ScriptableVariablesMenuOrder.LAYER_MASK)
    ]
    public class LayerMaskSingleListener : SingleStructListenerBase
    <
        LayerMask, 
        LayerMaskSingle, 
        LayerMaskUnityEvent, 
        LayerMaskCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}