using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(ColorSingleListener))][CanEditMultipleObjects]
    public class ColorSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    Color, 
	    ColorSingle, 
	    ColorUnityEvent,
	    ColorCompareUnityEvent,
	    ColorSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}