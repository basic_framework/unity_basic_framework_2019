using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Key Code", 
        fileName = "single_key_code_", 
        order = ScriptableVariablesMenuOrder.KEY_CODE)
    ]
    public class KeyCodeSingle : SingleStructBase<KeyCode> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(KeyCodeSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class KeyCodeSingleReference : SingleStructReferenceBase<KeyCode, KeyCodeSingle>
    {
        public KeyCodeSingleReference()
        {
        }

        public KeyCodeSingleReference(KeyCode value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class KeyCodeSingleUnityEvent : UnityEvent<KeyCodeSingle>{}
	
    [Serializable]
    public class KeyCodeSingleArrayUnityEvent : UnityEvent<KeyCodeSingle[]>{}
    
}