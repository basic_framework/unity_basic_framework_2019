using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Quaternion", 
        fileName = "single_quaternion_", 
        order = ScriptableVariablesMenuOrder.QUATERNION)
    ]
    public class QuaternionSingle : SingleStructBase<Quaternion> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(QuaternionSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class QuaternionSingleReference : SingleStructReferenceBase<Quaternion, QuaternionSingle>
    {
        public QuaternionSingleReference()
        {
        }

        public QuaternionSingleReference(Quaternion value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class QuaternionSingleUnityEvent : UnityEvent<QuaternionSingle>{}
	
    [Serializable]
    public class QuaternionSingleArrayUnityEvent : UnityEvent<QuaternionSingle[]>{}
    
}