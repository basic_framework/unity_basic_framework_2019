using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Quaternion Single Listener",
        ScriptableVariablesMenuOrder.QUATERNION)
    ]
    public class QuaternionSingleListener : SingleStructListenerBase
    <
        Quaternion, 
        QuaternionSingle, 
        QuaternionUnityEvent, 
        QuaternionCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}