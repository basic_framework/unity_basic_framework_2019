using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(QuaternionSingleListener))][CanEditMultipleObjects]
    public class QuaternionSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    Quaternion, 
	    QuaternionSingle, 
	    QuaternionUnityEvent,
	    QuaternionCompareUnityEvent,
	    QuaternionSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}