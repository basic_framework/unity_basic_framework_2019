using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Layer Mask", 
        fileName = "single_layer_mask_", 
        order = ScriptableVariablesMenuOrder.LAYER_MASK)
    ]
    public class LayerMaskSingle : SingleStructBase<LayerMask> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(LayerMaskSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class LayerMaskSingleReference : SingleStructReferenceBase<LayerMask, LayerMaskSingle>
    {
        public LayerMaskSingleReference()
        {
        }

        public LayerMaskSingleReference(LayerMask value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class LayerMaskSingleUnityEvent : UnityEvent<LayerMaskSingle>{}
	
    [Serializable]
    public class LayerMaskSingleArrayUnityEvent : UnityEvent<LayerMaskSingle[]>{}
    
}