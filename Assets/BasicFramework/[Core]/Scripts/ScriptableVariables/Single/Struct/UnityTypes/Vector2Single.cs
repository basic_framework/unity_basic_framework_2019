using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Vector2", 
        fileName = "single_vector2_", 
        order = ScriptableVariablesMenuOrder.VECTOR2)
    ]
    public class Vector2Single : SingleStructBase<Vector2> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(Vector2Single value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class Vector2SingleReference : SingleStructReferenceBase<Vector2, Vector2Single>
    {
        public Vector2SingleReference()
        {
        }

        public Vector2SingleReference(Vector2 value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class Vector2SingleUnityEvent : UnityEvent<Vector2Single>{}
	
    [Serializable]
    public class Vector2SingleArrayUnityEvent : UnityEvent<Vector2Single[]>{}
    
}