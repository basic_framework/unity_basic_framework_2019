using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(KeyCodeSingleListener))][CanEditMultipleObjects]
    public class KeyCodeSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    KeyCode, 
	    KeyCodeSingle, 
	    KeyCodeUnityEvent,
	    KeyCodeCompareUnityEvent,
	    KeyCodeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}