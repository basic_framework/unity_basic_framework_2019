using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Color", 
        fileName = "single_color_", 
        order = ScriptableVariablesMenuOrder.COLOR)
    ]
    public class ColorSingle : SingleStructBase<Color> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(ColorSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class ColorSingleReference : SingleStructReferenceBase<Color, ColorSingle>
    {
        public ColorSingleReference()
        {
        }

        public ColorSingleReference(Color value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class ColorSingleUnityEvent : UnityEvent<ColorSingle>{}
	
    [Serializable]
    public class ColorSingleArrayUnityEvent : UnityEvent<ColorSingle[]>{}
    
}