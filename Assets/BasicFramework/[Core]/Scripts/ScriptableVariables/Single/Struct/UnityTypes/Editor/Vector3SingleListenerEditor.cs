using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(Vector3SingleListener))][CanEditMultipleObjects]
    public class Vector3SingleListenerEditor : SingleStructListenerBaseEditor
    <
	    Vector3, 
	    Vector3Single, 
	    Vector3UnityEvent,
	    Vector3CompareUnityEvent,
	    Vector3SingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}