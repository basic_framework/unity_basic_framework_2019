using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    public abstract class SingleStructListenerBase<T, TScriptableSingleStruct, TUnityEvent, TCompareUnityEvent> 
        : ScriptableSingleListenerBase<T, TScriptableSingleStruct, TUnityEvent, TCompareUnityEvent>
        where T : struct
        where TScriptableSingleStruct : SingleStructBase<T>
        where TUnityEvent : UnityEvent<T>
        where TCompareUnityEvent : CompareValueUnityEventBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\
		
		
    }
}