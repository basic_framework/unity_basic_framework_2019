using System;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [Serializable]
    public abstract class SingleStructReferenceBase<T, TScriptableSingleStruct> 
        : ScriptableSingleReferenceBase<T, TScriptableSingleStruct>
        where T : struct
        where TScriptableSingleStruct : SingleStructBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        
    }
}