namespace BasicFramework.Core.ScriptableVariables.Single
{
    public abstract class SingleStructBase<T> : ScriptableSingleBase<T>
        where T : struct
    {
	
        // **************************************** VARIABLES ****************************************\\
		
		
        // ****************************************  METHODS  ****************************************\\
		
        protected sealed override T Clone(T reference)
        {
            return reference;
        }
        
        public override bool Compare(T left, T right)
        {
            return Equals(left, right);
        }

    }
}