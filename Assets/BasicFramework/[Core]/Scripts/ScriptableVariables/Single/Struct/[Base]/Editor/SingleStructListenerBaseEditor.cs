using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    public abstract class SingleStructListenerBaseEditor<T, TSingleStruct, TUnityEvent, TCompareUnityEvent, TScriptableSingleStructListenerBase> 
        : ScriptableSingleListenerBaseEditor<T, TSingleStruct, TUnityEvent, TCompareUnityEvent, TScriptableSingleStructListenerBase>
        where T : struct
        where TSingleStruct : SingleStructBase<T>
        where TUnityEvent : UnityEvent<T>
        where TCompareUnityEvent : CompareValueUnityEventBase<T>
        where TScriptableSingleStructListenerBase : SingleStructListenerBase<T, TSingleStruct, TUnityEvent, TCompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\

        
    }
}