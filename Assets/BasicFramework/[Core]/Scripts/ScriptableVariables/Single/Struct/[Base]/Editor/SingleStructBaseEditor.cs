namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    public abstract class SingleStructBaseEditor<T, TScriptableSingleStruct> 
        : ScriptableSingleBaseEditor<T, TScriptableSingleStruct>
        where T : struct
        where TScriptableSingleStruct : SingleStructBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\
		

        // ****************************************  METHODS  ****************************************\\

		
    }
}