using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(NormalizedFloatSingleListener))][CanEditMultipleObjects]
    public class NormalizedFloatSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    NormalizedFloat, 
	    NormalizedFloatSingle, 
	    NormalizedFloatUnityEvent,
	    NormalizedFloatCompareUnityEvent,
	    NormalizedFloatSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawEventFields()
        {
	        SectionHeader("Single Listener Events");

	        var valueChangedPlus = serializedObject.FindProperty("_valueChangedPlus");

	        EditorGUILayout.PropertyField(valueChangedPlus);

	        if (!valueChangedPlus.isExpanded)
	        {
		        Space();
	        }
	        
	        ValueChangedCompareReorderableList.DoLayoutList();
	        
	        //base.DrawEventFields();
        }
        
    }
}