﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Basic Pose", 
        fileName = "single_pose_", 
        order = ScriptableVariablesMenuOrder.BASIC_POSE)
    ]
    public class BasicPoseSingle : SingleStructBase<BasicPose> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(BasicPoseSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class BasicPoseSingleReference : SingleStructReferenceBase<BasicPose, BasicPoseSingle>
    {
        public BasicPoseSingleReference()
        {
        }

        public BasicPoseSingleReference(BasicPose value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class BasicPoseSingleUnityEvent : UnityEvent<BasicPoseSingle>{}
    
    [Serializable]
    public class BasicPoseSingleCompareUnityEvent : CompareValueUnityEventBase<BasicPoseSingle>{}
	
    [Serializable]
    public class BasicPoseSingleArrayUnityEvent : UnityEvent<BasicPoseSingle[]>{}
    
}