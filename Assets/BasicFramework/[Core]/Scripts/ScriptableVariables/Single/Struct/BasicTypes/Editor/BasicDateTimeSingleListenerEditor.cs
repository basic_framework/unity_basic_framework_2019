﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(BasicDateTimeSingleListener))][CanEditMultipleObjects]
    public class BasicDateTimeSingleListenerEditor : SingleStructListenerBaseEditor
    <
        BasicDateTime, 
        BasicDateTimeSingle, 
        BasicDateTimeUnityEvent,
        BasicDateTimeCompareUnityEvent,
        BasicDateTimeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawEventFields()
        {
            var valueChangedPlus = serializedObject.FindProperty("_valueChangedPlus");
            
            Space();
            SectionHeader("Basic Date Time Single Listener Events");

            EditorGUILayout.PropertyField(valueChangedPlus);
            
            base.DrawEventFields();
        }
        
    }
}