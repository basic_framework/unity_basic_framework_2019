﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Basic Date Time", 
        fileName = "single_basic_date_time_", 
        order = ScriptableVariablesMenuOrder.BASIC_DATE_TIME)
    ]
    public class BasicDateTimeSingle : SingleStructBase<BasicDateTime> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(BasicDateTimeSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class BasicDateTimeSingleReference : SingleStructReferenceBase<BasicDateTime, BasicDateTimeSingle>
    {
        public BasicDateTimeSingleReference()
        {
        }

        public BasicDateTimeSingleReference(BasicDateTime value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class BasicDateTimeSingleUnityEvent : UnityEvent<BasicDateTimeSingle>{}
	
    [Serializable]
    public class BasicDateTimeSingleArrayUnityEvent : UnityEvent<BasicDateTimeSingle[]>{}
    
}