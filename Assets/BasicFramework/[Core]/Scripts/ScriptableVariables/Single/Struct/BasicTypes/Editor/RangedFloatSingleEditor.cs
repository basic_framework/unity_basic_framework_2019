using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RangedFloatSingle))][CanEditMultipleObjects]
    public class RangedFloatSingleEditor : SingleStructBaseEditor<RangedFloat, RangedFloatSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(RangedFloatSingle))]
    public class RangedFloatSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(RangedFloatSingleReference))]
    public class RangedFloatSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}