using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Ranged Float Single Listener",
        ScriptableVariablesMenuOrder.RANGED_FLOAT)
    ]
    public class RangedFloatSingleListener : SingleStructListenerBase
    <
        RangedFloat, 
        RangedFloatSingle, 
        RangedFloatUnityEvent, 
        RangedFloatCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private FloatUnityEvent _rangedValueChanged = default;


        // ****************************************  METHODS  ****************************************\\

        protected override void OnRaiseValueChanged(ScriptableSingleBase<RangedFloat> single)
        {
            base.OnRaiseValueChanged(single);
            
            _rangedValueChanged.Invoke(single.Value.Value);
        }

    }
}