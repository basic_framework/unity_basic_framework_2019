using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Normalized Float Single Listener",
        ScriptableVariablesMenuOrder.NORMALIZED_FLOAT)
    ]
    public class NormalizedFloatSingleListener : SingleStructListenerBase
    <
        NormalizedFloat, 
        NormalizedFloatSingle, 
        NormalizedFloatUnityEvent, 
        NormalizedFloatCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private NormalizedFloatPlusUnityEvent _valueChangedPlus = default;
        

        // ****************************************  METHODS  ****************************************\\

        protected override void OnRaiseValueChanged(ScriptableSingleBase<NormalizedFloat> scriptableSingleBase)
        {
            base.OnRaiseValueChanged(scriptableSingleBase);
            
            _valueChangedPlus.Raise(scriptableSingleBase.Value);
        }
        
    }
}