﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Runtime Platform Single Listener", 
        ScriptableVariablesMenuOrder.RUNTIME_PLATFORM)]
    public class RuntimePlatformSingleListener : SingleStructListenerBase
    <
        RuntimePlatform, 
        RuntimePlatformSingle, 
        RuntimePlatformUnityEvent, 
        RuntimePlatformCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}