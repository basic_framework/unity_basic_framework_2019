using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Int Range Single Listener",
        ScriptableVariablesMenuOrder.INT_RANGE)
    ]
    public class IntRangeSingleListener : SingleStructListenerBase
    <
        IntRange, 
        IntRangeSingle, 
        IntRangeUnityEvent, 
        IntRangeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}