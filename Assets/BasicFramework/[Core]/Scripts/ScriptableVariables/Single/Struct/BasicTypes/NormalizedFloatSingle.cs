using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Normalized Float", 
        fileName = "single_normalized_float_", 
        order = ScriptableVariablesMenuOrder.NORMALIZED_FLOAT)
    ]
    public class NormalizedFloatSingle : SingleStructBase<NormalizedFloat> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(NormalizedFloatSingle value)
        {
            Value = value.Value;
        }
        
        public void SetValue(float value)
        {
            Value = new NormalizedFloat(value);
        }

    }

    [Serializable]
    public class NormalizedFloatSingleReference : SingleStructReferenceBase<NormalizedFloat, NormalizedFloatSingle>
    {
        public NormalizedFloatSingleReference()
        {
        }

        public NormalizedFloatSingleReference(NormalizedFloat value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class NormalizedFloatSingleUnityEvent : UnityEvent<NormalizedFloatSingle>{}
	
    [Serializable]
    public class NormalizedFloatSingleArrayUnityEvent : UnityEvent<NormalizedFloatSingle[]>{}
    
}