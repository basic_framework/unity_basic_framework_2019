using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Float Range", 
        fileName = "single_float_range_", 
        order = ScriptableVariablesMenuOrder.FLOAT_RANGE)
    ]
    public class FloatRangeSingle : SingleStructBase<FloatRange> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
            Value = FloatRange.Default;
            EditorSetDefaultValueToValue();
        }
        
        public void SetValue(FloatRangeSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class FloatRangeSingleReference : SingleStructReferenceBase<FloatRange, FloatRangeSingle>
    {
        public FloatRangeSingleReference()
        {
        }

        public FloatRangeSingleReference(FloatRange value)
        {
            _constantValue = value;
        }
        
        public FloatRangeSingleReference(float min, float max)
        {
            _constantValue.Min = min;
            _constantValue.Max = max;
        }
    }
	
    [Serializable]
    public class FloatRangeSingleUnityEvent : UnityEvent<FloatRangeSingle>{}
	
    [Serializable]
    public class FloatRangeSingleArrayUnityEvent : UnityEvent<FloatRangeSingle[]>{}
    
}