using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(IntRangeSingleListener))][CanEditMultipleObjects]
    public class IntRangeSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    IntRange, 
	    IntRangeSingle, 
	    IntRangeUnityEvent,
	    IntRangeCompareUnityEvent,
	    IntRangeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}