using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Ranged Int", 
        fileName = "single_ranged_int_", 
        order = ScriptableVariablesMenuOrder.RANGED_INT)
    ]
    public class RangedIntSingle : SingleStructBase<RangedInt> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
            Value = RangedInt.Default;
            EditorSetDefaultValueToValue();
        }

        public void SetValue(RangedIntSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class RangedIntSingleReference : SingleStructReferenceBase<RangedInt, RangedIntSingle>
    {
        public RangedIntSingleReference()
        {
        }

        public RangedIntSingleReference(RangedInt value)
        {
            _constantValue = value;
        }
        
        public RangedIntSingleReference(int min, int value, int max)
        {
            _constantValue.Min = min;
            _constantValue.Max = max;
            _constantValue.Value = value;
        }
    }
	
    [Serializable]
    public class RangedIntSingleUnityEvent : UnityEvent<RangedIntSingle>{}
	
    [Serializable]
    public class RangedIntSingleArrayUnityEvent : UnityEvent<RangedIntSingle[]>{}
    
}