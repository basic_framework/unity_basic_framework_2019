﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RuntimePlatformSingle))][CanEditMultipleObjects]
    public class RuntimePlatformSingleEditor : SingleStructBaseEditor<RuntimePlatform, RuntimePlatformSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(RuntimePlatformSingle))]
    public class RuntimePlatformSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(RuntimePlatformSingleReference))]
    public class RuntimePlatformSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    	    
            // **************************************** VARIABLES ****************************************\\
    	    
    	    
            // ****************************************  METHODS  ****************************************\\
    
    }
}