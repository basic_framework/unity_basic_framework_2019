using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RangedFloatSingleListener))][CanEditMultipleObjects]
    public class RangedFloatSingleListenerEditor : SingleStructListenerBaseEditor
    <
        RangedFloat, 
        RangedFloatSingle, 
        RangedFloatUnityEvent,
        RangedFloatCompareUnityEvent,
        RangedFloatSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawEventFields()
        {
            base.DrawEventFields();

            var rangedValueChanged = serializedObject.FindProperty("_rangedValueChanged");
	        
            Space();
            SectionHeader($"{nameof(RangedFloatSingleListener).CamelCaseToHumanReadable()} Events");

            EditorGUILayout.PropertyField(rangedValueChanged);
        }
        
    }
}