using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(BasicPoseSingleListener))][CanEditMultipleObjects]
    public class BasicPoseSingleListenerEditor : SingleStructListenerBaseEditor
    <
        BasicPose, 
        BasicPoseSingle, 
        BasicPoseUnityEvent,
        BasicPoseCompareUnityEvent,
        BasicPoseSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}