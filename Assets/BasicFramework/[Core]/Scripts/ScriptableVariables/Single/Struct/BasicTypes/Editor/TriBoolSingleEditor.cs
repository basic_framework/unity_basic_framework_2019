﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(TriBoolSingle))][CanEditMultipleObjects]
    public class TriBoolSingleEditor : SingleStructBaseEditor<TriBool, TriBoolSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(TriBoolSingle))]
    public class TriBoolSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(TriBoolSingleReference))]
    public class TriBoolSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    	    
            // **************************************** VARIABLES ****************************************\\
    	    
    	    
            // ****************************************  METHODS  ****************************************\\
    
    }
}