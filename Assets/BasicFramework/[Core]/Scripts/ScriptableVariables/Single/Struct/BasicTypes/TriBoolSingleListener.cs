﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu("Basic Framework/Core/Scriptable Variables/Single/Tri Bool Single Listener",
        ScriptableVariablesMenuOrder.TRI_BOOL)]
    public class TriBoolSingleListener : SingleStructListenerBase
    <
        TriBool, 
        TriBoolSingle, 
        TriBoolUnityEvent, 
        TriBoolCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}