using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(BasicPoseSingle))][CanEditMultipleObjects]
    public class BasicPoseSingleEditor : SingleStructBaseEditor<BasicPose, BasicPoseSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(BasicPoseSingle))]
    public class BasicPoseSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(BasicPoseSingleReference))]
    public class BasicPoseSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}