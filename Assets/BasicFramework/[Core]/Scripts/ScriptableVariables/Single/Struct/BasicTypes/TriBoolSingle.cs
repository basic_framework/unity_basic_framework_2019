﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Tri Bool Single", 
        fileName = "single_tri_bool_", 
        order = ScriptableVariablesMenuOrder.TRI_BOOL)]
    public class TriBoolSingle : SingleStructBase<TriBool> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(TriBoolSingle value)
        {
            Value = value.Value;
        }

        public void SetFalse()
        {
            Value = TriBool.False;
        }
        
        public void SetIndetermined()
        {
            Value = TriBool.Indetermined;
        }
        
        public void SetTrue()
        {
            Value = TriBool.True;
        }

    }

    [Serializable]
    public class TriBoolSingleReference : SingleStructReferenceBase<TriBool, TriBoolSingle>
    {
        public TriBoolSingleReference()
        {
        }

        public TriBoolSingleReference(TriBool value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class TriBoolSingleUnityEvent : UnityEvent<TriBoolSingle>{}
	
    [Serializable]
    public class TriBoolSingleArrayUnityEvent : UnityEvent<TriBoolSingle[]>{}
    
}