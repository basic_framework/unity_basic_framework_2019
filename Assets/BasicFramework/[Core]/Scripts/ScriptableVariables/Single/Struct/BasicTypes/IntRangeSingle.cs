using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Int Range", 
        fileName = "single_int_range_", 
        order = ScriptableVariablesMenuOrder.INT_RANGE)
    ]
    public class IntRangeSingle : SingleStructBase<IntRange> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
            Value = IntRange.Default;
            EditorSetDefaultValueToValue();
        }
        
        public void SetValue(IntRangeSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class IntRangeSingleReference : SingleStructReferenceBase<IntRange, IntRangeSingle>
    {
        public IntRangeSingleReference()
        {
        }

        public IntRangeSingleReference(IntRange value)
        {
            _constantValue = value;
        }
        
        public IntRangeSingleReference(int min, int max)
        {
            _constantValue.Min = min;
            _constantValue.Max = max;
        }
    }
	
    [Serializable]
    public class IntRangeSingleUnityEvent : UnityEvent<IntRangeSingle>{}
	
    [Serializable]
    public class IntRangeSingleArrayUnityEvent : UnityEvent<IntRangeSingle[]>{}
    
}