using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(FloatRangeSingleListener))][CanEditMultipleObjects]
    public class FloatRangeSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    FloatRange, 
	    FloatRangeSingle, 
	    FloatRangeUnityEvent,
	    FloatRangeCompareUnityEvent,
	    FloatRangeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}