﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Basic Date Time Single Listener",
        ScriptableVariablesMenuOrder.BASIC_DATE_TIME)
    ]
    public class BasicDateTimeSingleListener : SingleStructListenerBase
    <
        BasicDateTime, 
        BasicDateTimeSingle, 
        BasicDateTimeUnityEvent, 
        BasicDateTimeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BasicDateTimePlusUnityEvent _valueChangedPlus = new BasicDateTimePlusUnityEvent();
        

        // ****************************************  METHODS  ****************************************\\

        protected override void OnRaiseValueChanged(ScriptableSingleBase<BasicDateTime> scriptableSingleBase)
        {
            base.OnRaiseValueChanged(scriptableSingleBase);
            
            _valueChangedPlus.Raise(scriptableSingleBase.Value);
        }
    }
}