﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(BasicDateTimeSingle))][CanEditMultipleObjects]
    public class BasicDateTimeSingleEditor : SingleStructBaseEditor<BasicDateTime, BasicDateTimeSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
    }
	
    [CustomPropertyDrawer(typeof(BasicDateTimeSingle))]
    public class BasicDateTimeSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(BasicDateTimeSingleReference))]
    public class BasicDateTimeSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}