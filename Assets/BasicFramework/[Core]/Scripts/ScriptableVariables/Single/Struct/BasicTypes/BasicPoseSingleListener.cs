using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Basic Pose Single Listener",
        ScriptableVariablesMenuOrder.BASIC_POSE)
    ]
    public class BasicPoseSingleListener : SingleStructListenerBase
    <
        BasicPose, 
        BasicPoseSingle, 
        BasicPoseUnityEvent, 
        BasicPoseCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}