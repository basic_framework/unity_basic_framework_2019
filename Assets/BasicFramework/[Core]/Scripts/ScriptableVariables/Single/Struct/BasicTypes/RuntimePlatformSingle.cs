﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Runtime Platform Single", 
        fileName = "single_runtime_platform_",
        order = ScriptableVariablesMenuOrder.RUNTIME_PLATFORM)]
    public class RuntimePlatformSingle : SingleStructBase<RuntimePlatform> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(RuntimePlatformSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class RuntimePlatformSingleReference : SingleStructReferenceBase<RuntimePlatform, RuntimePlatformSingle>
    {
        public RuntimePlatformSingleReference()
        {
        }

        public RuntimePlatformSingleReference(RuntimePlatform value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class RuntimePlatformSingleUnityEvent : UnityEvent<RuntimePlatformSingle>{}
	
    [Serializable]
    public class RuntimePlatformSingleArrayUnityEvent : UnityEvent<RuntimePlatformSingle[]>{}
    
}