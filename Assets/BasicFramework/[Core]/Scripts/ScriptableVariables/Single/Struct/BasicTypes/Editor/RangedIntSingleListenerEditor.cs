using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RangedIntSingleListener))][CanEditMultipleObjects]
    public class RangedIntSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    RangedInt, 
	    RangedIntSingle, 
	    RangedIntUnityEvent,
	    RangedIntCompareUnityEvent,
	    RangedIntSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}