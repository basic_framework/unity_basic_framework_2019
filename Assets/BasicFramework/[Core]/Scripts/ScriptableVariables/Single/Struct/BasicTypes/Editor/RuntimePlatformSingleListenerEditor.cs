﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RuntimePlatformSingleListener))][CanEditMultipleObjects]
    public class RuntimePlatformSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    RuntimePlatform, 
	    RuntimePlatformSingle, 
	    RuntimePlatformUnityEvent,
	    RuntimePlatformCompareUnityEvent,
	    RuntimePlatformSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}