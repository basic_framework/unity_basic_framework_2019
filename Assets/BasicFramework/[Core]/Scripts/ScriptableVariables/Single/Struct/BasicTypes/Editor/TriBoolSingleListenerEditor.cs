﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(TriBoolSingleListener))][CanEditMultipleObjects]
    public class TriBoolSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    TriBool, 
	    TriBoolSingle, 
	    TriBoolUnityEvent,
	    TriBoolCompareUnityEvent,
	    TriBoolSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}