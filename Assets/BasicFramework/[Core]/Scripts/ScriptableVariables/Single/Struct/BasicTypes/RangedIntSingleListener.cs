using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Ranged Int Single Listener",
        ScriptableVariablesMenuOrder.RANGED_INT)
    ]
    public class RangedIntSingleListener : SingleStructListenerBase
    <
        RangedInt, 
        RangedIntSingle, 
        RangedIntUnityEvent, 
        RangedIntCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}