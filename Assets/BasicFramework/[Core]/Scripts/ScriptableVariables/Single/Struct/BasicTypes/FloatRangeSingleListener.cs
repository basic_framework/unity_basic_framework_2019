using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Float Range Single Listener",
        ScriptableVariablesMenuOrder.FLOAT_RANGE)
    ]
    public class FloatRangeSingleListener : SingleStructListenerBase
    <
        FloatRange, 
        FloatRangeSingle, 
        FloatRangeUnityEvent, 
        FloatRangeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}