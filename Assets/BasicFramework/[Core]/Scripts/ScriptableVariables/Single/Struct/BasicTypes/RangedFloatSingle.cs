using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Ranged Float", 
        fileName = "single_ranged_float_", 
        order = ScriptableVariablesMenuOrder.RANGED_FLOAT)
    ]
    public class RangedFloatSingle : SingleStructBase<RangedFloat> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
            Value = RangedFloat.Default;
            EditorSetDefaultValueToValue();
        }
        
        public void SetValue(RangedFloatSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class RangedFloatSingleReference : SingleStructReferenceBase<RangedFloat, RangedFloatSingle>
    {
        public RangedFloatSingleReference()
        {
        }

        public RangedFloatSingleReference(RangedFloat value)
        {
            _constantValue = value;
        }
        
        public RangedFloatSingleReference(float min, float value, float max)
        {
            _constantValue.Min = min;
            _constantValue.Max = max;
            _constantValue.Value = value;
        }
    }
	
    [Serializable]
    public class RangedFloatSingleUnityEvent : UnityEvent<RangedFloatSingle>{}
	
    [Serializable]
    public class RangedFloatSingleArrayUnityEvent : UnityEvent<RangedFloatSingle[]>{}
    
}