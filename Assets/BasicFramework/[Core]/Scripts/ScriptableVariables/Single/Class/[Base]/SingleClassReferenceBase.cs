using System;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [Serializable]
    public abstract class SingleClassReferenceBase<T, TScriptableSingleClass> 
        : ScriptableSingleReferenceBase<T, TScriptableSingleClass>
        where T : class
        where TScriptableSingleClass : SingleClassBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        
    }
}