namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	public abstract class SingleClassBaseEditor<T, TScriptableSingleClass> 
		: ScriptableSingleBaseEditor<T, TScriptableSingleClass>
		where T : class
		where TScriptableSingleClass : SingleClassBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		

		// ****************************************  METHODS  ****************************************\\

		
	}
}