using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    public abstract class SingleClassListenerBaseEditor<T, TSingleClass, TUnityEvent, TCompareUnityEvent, TScriptableSingleClassListenerBase> 
        : ScriptableSingleListenerBaseEditor<T, TSingleClass, TUnityEvent, TCompareUnityEvent, TScriptableSingleClassListenerBase>
        where T : class
        where TSingleClass : SingleClassBase<T>
        where TUnityEvent : UnityEvent<T>
        where TCompareUnityEvent : CompareValueUnityEventBase<T>
        where TScriptableSingleClassListenerBase : SingleClassListenerBase<T, TSingleClass, TUnityEvent, TCompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\

        
    }
}