

namespace BasicFramework.Core.ScriptableVariables.Single
{
	public abstract class SingleClassBase<T> : ScriptableSingleBase<T>
		where T : class
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		
		// ****************************************  METHODS  ****************************************\\
		
		public override bool Compare(T left, T right)
		{
			return ReferenceEquals(left, right);
		}

	}
}