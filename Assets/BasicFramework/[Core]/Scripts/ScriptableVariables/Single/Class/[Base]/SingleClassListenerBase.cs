using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
	public abstract class SingleClassListenerBase<T, TScriptableSingleClass, TUnityEvent, TCompareUnityEvent> 
		: ScriptableSingleListenerBase<T, TScriptableSingleClass, TUnityEvent, TCompareUnityEvent>
		where T : class
		where TScriptableSingleClass : SingleClassBase<T>
		where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		
		// ****************************************  METHODS  ****************************************\\
		
		
	}
}