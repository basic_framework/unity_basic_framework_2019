using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/String Single Listener",
        ScriptableVariablesMenuOrder.STRING)
    ]
    public class StringSingleListener : SingleClassListenerBase
    <
        string, 
        StringSingle, 
        StringUnityEvent, 
        StringCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}