using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/string", 
        fileName = "single_string_", 
        order = ScriptableVariablesMenuOrder.STRING)]
    public class StringSingle : SingleClassBase<string>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(StringSingle value)
        {
            Value = Clone(value.Value);
        }
        
        protected override string Clone(string reference)
        {
            return reference == null ? "" : string.Copy(reference);
        }

        public override bool Compare(string left, string right)
        {
            return left == right;
        }

        public void Append(string value)
        {
            Value += value;
        }

        public void Remove(int count)
        {
            Value = Value.Remove(Value.Length - count, count);
        }

    }

    [Serializable]
    public class StringSingleReference : SingleClassReferenceBase<string, StringSingle>
    {
        public StringSingleReference()
        {
        }

        public StringSingleReference(string value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class StringSingleUnityEvent : UnityEvent<StringSingle>{}
	
    [Serializable]
    public class StringSingleArrayUnityEvent : UnityEvent<StringSingle[]>{}

}