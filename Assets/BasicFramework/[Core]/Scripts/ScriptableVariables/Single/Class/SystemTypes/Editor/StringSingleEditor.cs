using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(StringSingle))][CanEditMultipleObjects]
    public class StringSingleEditor : SingleClassBaseEditor<string, StringSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        

    }
    
    [CustomPropertyDrawer(typeof(StringSingle))]
    public class StringSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(StringSingleReference))]
    public class StringSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
}