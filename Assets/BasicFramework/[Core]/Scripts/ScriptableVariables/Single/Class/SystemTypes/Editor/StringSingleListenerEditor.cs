using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;


namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	[CustomEditor(typeof(StringSingleListener))][CanEditMultipleObjects]
	public class StringSingleListenerEditor : SingleClassListenerBaseEditor
	<
		string, 
		StringSingle, 
		StringUnityEvent,
		StringCompareUnityEvent,
		StringSingleListener
	>
	{
	
		// **************************************** VARIABLES ****************************************\\

		
		// ****************************************  METHODS  ****************************************\\

		
	}
}