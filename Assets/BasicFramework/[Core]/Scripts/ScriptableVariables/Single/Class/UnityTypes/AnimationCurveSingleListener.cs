using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/Single/Animation Curve Single Listener",
	    ScriptableVariablesMenuOrder.ANIMATION_CURVE)
    ]
    public class AnimationCurveSingleListener : SingleClassListenerBase
    <
        AnimationCurve, 
        AnimationCurveSingle, 
        AnimationCurveUnityEvent,
        AnimationCurveCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}