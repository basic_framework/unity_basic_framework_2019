using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(AnimationCurveSingle))][CanEditMultipleObjects]
    public class AnimationCurveSingleEditor : SingleClassBaseEditor<AnimationCurve, AnimationCurveSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
    }
    
    [CustomPropertyDrawer(typeof(AnimationCurveSingle))]
    public class AnimationCurveSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(AnimationCurveSingleReference))]
    public class AnimationCurveSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
    
}