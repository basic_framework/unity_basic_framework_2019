using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(AnimationCurveSingleListener))][CanEditMultipleObjects]
    public class AnimationCurveSingleListenerEditor : SingleClassListenerBaseEditor
    <
        AnimationCurve, 
        AnimationCurveSingle, 
        AnimationCurveUnityEvent,
        AnimationCurveCompareUnityEvent,
        AnimationCurveSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}