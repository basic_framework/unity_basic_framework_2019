using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Animation Curve", 
        fileName = "single_animation_curve_", 
        order = ScriptableVariablesMenuOrder.ANIMATION_CURVE)
    ]
    public class AnimationCurveSingle : SingleClassBase<AnimationCurve>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        private void Reset()
        {
            Value = AnimationCurve.Linear(0, 0, 1, 1);
            EditorSetDefaultValueToValue();
        }
        
        public void SetValue(AnimationCurveSingle value)
        {
            Value = Clone(value.Value);
        }
        
        protected override AnimationCurve Clone(AnimationCurve reference)
        {
            return new AnimationCurve(reference.keys);
        }
    }


    [Serializable]
    public class AnimationCurveSingleReference : SingleClassReferenceBase<AnimationCurve, AnimationCurveSingle>
    {
        public AnimationCurveSingleReference()
        {
            _constantValue = AnimationCurve.Linear(0, 0, 1, 1);
        }

        public AnimationCurveSingleReference(AnimationCurve value)
        {
            _constantValue = new AnimationCurve(value.keys);
        }
    }
    
    [Serializable]
    public class AnimationCurveSingleUnityEvent : UnityEvent<AnimationCurveSingle>{}
	
    [Serializable]
    public class AnimationCurveSingleArrayUnityEvent : UnityEvent<AnimationCurveSingle[]>{}
    
}