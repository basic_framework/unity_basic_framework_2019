﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Rect Transform Single Listener",
        ScriptableVariablesMenuOrder.RECT_TRANSFORM)
    ]
    public class RectTransformSingleListener : SingleObjectListenerBase
    <
        RectTransform, 
        RectTransformSingle, 
        RectTransformUnityEvent, 
        RectTransformCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}