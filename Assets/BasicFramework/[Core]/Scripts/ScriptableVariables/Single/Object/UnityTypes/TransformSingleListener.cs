using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Transform Single Listener",
        ScriptableVariablesMenuOrder.TRANSFORM)
    ]
    public class TransformSingleListener : SingleObjectListenerBase
    <
	    Transform, 
	    TransformSingle, 
	    TransformUnityEvent, 
	    TransformCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}