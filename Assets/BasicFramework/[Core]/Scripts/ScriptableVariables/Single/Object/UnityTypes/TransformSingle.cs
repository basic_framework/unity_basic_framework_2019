using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Transform", 
        fileName = "single_transform_", 
        order = ScriptableVariablesMenuOrder.TRANSFORM)
    ]
    public class TransformSingle : SingleObjectBase<Transform>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(TransformSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class TransformSingleReference : SingleObjectReferenceBase<Transform, TransformSingle>
    {
        public TransformSingleReference()
        {
        }

        public TransformSingleReference(Transform value)
        {
            _constantValue = value;
        }
    }
}