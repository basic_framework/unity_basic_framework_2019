using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(GameObjectSingleListener))][CanEditMultipleObjects]
    public class GameObjectSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        GameObject, 
        GameObjectSingle, 
        GameObjectUnityEvent,
        GameObjectCompareUnityEvent,
        GameObjectSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}