using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/Single/Material Single Listener",
	    ScriptableVariablesMenuOrder.MATERIAL)
    ]
    public class MaterialSingleListener : SingleObjectListenerBase
    <
	    Material, 
	    MaterialSingle, 
	    MaterialUnityEvent,
	    MaterialCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}