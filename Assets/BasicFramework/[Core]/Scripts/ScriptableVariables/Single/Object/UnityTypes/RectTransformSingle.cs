﻿using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Rect Transform", 
        fileName = "single_rect_transform_", 
        order = ScriptableVariablesMenuOrder.RECT_TRANSFORM)
    ]
    public class RectTransformSingle : SingleObjectBase<RectTransform>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(RectTransformSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class RectTransformSingleReference : SingleObjectReferenceBase<RectTransform, RectTransformSingle>
    {
        public RectTransformSingleReference()
        {
        }

        public RectTransformSingleReference(RectTransform value)
        {
            _constantValue = value;
        }
    }
}