using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(TransformSingleListener))][CanEditMultipleObjects]
    public class TransformSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        Transform, 
        TransformSingle, 
        TransformUnityEvent,
        TransformCompareUnityEvent,
        TransformSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}