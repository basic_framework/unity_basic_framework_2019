using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Render Texture Single Listener",
        ScriptableVariablesMenuOrder.RENDER_TEXTURE)
    ]
    public class RenderTextureSingleListener : SingleObjectListenerBase
    <
	    RenderTexture, 
	    RenderTextureSingle, 
	    RenderTextureUnityEvent, 
	    RenderTextureCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}