﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(CameraSingleListener))][CanEditMultipleObjects]
    public class CameraSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        Camera, 
        CameraSingle, 
        CameraUnityEvent,
        CameraCompareUnityEvent,
        CameraSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}