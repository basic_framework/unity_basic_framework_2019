using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Game Object Single Listener",
        ScriptableVariablesMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectSingleListener : SingleObjectListenerBase
    <
	    GameObject, 
	    GameObjectSingle, 
	    GameObjectUnityEvent, 
	    GameObjectCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}