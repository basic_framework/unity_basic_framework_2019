using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Game Object", 
        fileName = "single_game_object_", 
        order = ScriptableVariablesMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectSingle : SingleObjectBase<GameObject>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(GameObjectSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class GameObjectSingleReference : SingleObjectReferenceBase<GameObject, GameObjectSingle>
    {
        public GameObjectSingleReference()
        {
        }

        public GameObjectSingleReference(GameObject value)
        {
            _constantValue = value;
        }
    }
}