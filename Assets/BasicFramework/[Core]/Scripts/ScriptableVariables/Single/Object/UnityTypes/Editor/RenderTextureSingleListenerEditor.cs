using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RenderTextureSingleListener))][CanEditMultipleObjects]
    public class RenderTextureSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        RenderTexture, 
        RenderTextureSingle, 
        RenderTextureUnityEvent,
        RenderTextureCompareUnityEvent,
        RenderTextureSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}