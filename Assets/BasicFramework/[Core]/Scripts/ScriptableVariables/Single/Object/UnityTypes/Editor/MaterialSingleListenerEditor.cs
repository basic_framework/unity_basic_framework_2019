using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(MaterialSingleListener))][CanEditMultipleObjects]
    public class MaterialSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        Material, 
        MaterialSingle, 
        MaterialUnityEvent,
        MaterialCompareUnityEvent,
        MaterialSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}