using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Material", 
        fileName = "single_material_", 
        order = ScriptableVariablesMenuOrder.MATERIAL)
    ]
    public class MaterialSingle : SingleObjectBase<Material>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(MaterialSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class MaterialSingleReference : SingleObjectReferenceBase<Material, MaterialSingle>
    {
        public MaterialSingleReference()
        {
        }

        public MaterialSingleReference(Material value)
        {
            _constantValue = value;
        }
    }
}