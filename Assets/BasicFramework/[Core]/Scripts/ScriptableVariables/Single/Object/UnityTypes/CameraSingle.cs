﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Camera", 
        fileName = "single_camera_", 
        order = ScriptableVariablesMenuOrder.CAMERA)
    ]
    public class CameraSingle : SingleObjectBase<Camera>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(CameraSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class CameraSingleReference : SingleObjectReferenceBase<Camera, CameraSingle>
    {
        public CameraSingleReference()
        {
        }

        public CameraSingleReference(Camera value)
        {
            _constantValue = value;
        }
    }
    
    [Serializable]
    public class CameraSingleUnityEvent : UnityEvent<CameraSingle>{}
	
    [Serializable]
    public class CameraSingleArrayUnityEvent : UnityEvent<CameraSingle[]>{}
    
}