﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Camera Single Listener",
        ScriptableVariablesMenuOrder.CAMERA)
    ]
    public class CameraSingleListener : SingleObjectListenerBase
    <
        Camera, 
        CameraSingle, 
        CameraUnityEvent, 
        CameraCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}