﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RectTransformSingleListener))][CanEditMultipleObjects]
    public class RectTransformSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        RectTransform, 
        RectTransformSingle, 
        RectTransformUnityEvent,
        RectTransformCompareUnityEvent,
        RectTransformSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}