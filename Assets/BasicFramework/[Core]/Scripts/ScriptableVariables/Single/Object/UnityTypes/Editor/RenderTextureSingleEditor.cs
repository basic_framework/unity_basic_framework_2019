using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(RenderTextureSingle))][CanEditMultipleObjects]
    public class RenderTextureSingleEditor : SingleObjectBaseEditor<RenderTexture, RenderTextureSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(RenderTextureSingle))]
    public class RenderTextureSinglePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(RenderTextureSingleReference))]
    public class RenderTextureSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
}