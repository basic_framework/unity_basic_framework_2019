using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Render Texture", 
        fileName = "single_render_texture_", 
        order = ScriptableVariablesMenuOrder.RENDER_TEXTURE)
    ]
    public class RenderTextureSingle : SingleObjectBase<RenderTexture>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(RenderTextureSingle value)
        {
            Value = value.Value;
        }
	
    }
    
    [Serializable]
    public class RenderTextureSingleReference : SingleObjectReferenceBase<RenderTexture, RenderTextureSingle>
    {
        public RenderTextureSingleReference()
        {
        }

        public RenderTextureSingleReference(RenderTexture value)
        {
            _constantValue = value;
        }
    }
}