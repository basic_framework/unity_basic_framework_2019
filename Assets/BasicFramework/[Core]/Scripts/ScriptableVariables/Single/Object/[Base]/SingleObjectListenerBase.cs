using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    public abstract class SingleObjectListenerBase<T, TScriptableSingleObject, TUnityEvent, TCompareUnityEvent> 
        : ScriptableSingleListenerBase<T, TScriptableSingleObject, TUnityEvent, TCompareUnityEvent>
        where T : Object
        where TScriptableSingleObject : SingleObjectBase<T>
        where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\
		
		
    }
}