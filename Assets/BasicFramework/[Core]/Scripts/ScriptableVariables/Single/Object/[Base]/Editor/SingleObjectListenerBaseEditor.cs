using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    public abstract class SingleObjectListenerBaseEditor<T, TSingleObject, TUnityEvent, TCompareUnityEvent, TScriptableSingleObjectListenerBase> 
        : ScriptableSingleListenerBaseEditor<T, TSingleObject, TUnityEvent, TCompareUnityEvent, TScriptableSingleObjectListenerBase>
        where T : Object
        where TSingleObject : SingleObjectBase<T>
        where TUnityEvent : UnityEvent<T>
        where TCompareUnityEvent : CompareValueUnityEventBase<T>
        where TScriptableSingleObjectListenerBase : SingleObjectListenerBase<T, TSingleObject, TUnityEvent, TCompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\

        
    }
}