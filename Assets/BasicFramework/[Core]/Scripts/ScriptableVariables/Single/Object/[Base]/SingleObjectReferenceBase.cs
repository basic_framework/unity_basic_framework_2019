using System;
using Object = UnityEngine.Object;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [Serializable]
    public abstract class SingleObjectReferenceBase<T, TScriptableSingleObject> 
        : ScriptableSingleReferenceBase<T, TScriptableSingleObject>
        where T : Object
        where TScriptableSingleObject : SingleObjectBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        
        
    }
}