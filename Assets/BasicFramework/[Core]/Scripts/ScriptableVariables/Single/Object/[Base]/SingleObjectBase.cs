using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    public abstract class SingleObjectBase<T> : ScriptableSingleBase<T>
        where T : Object
    {
	
        // **************************************** VARIABLES ****************************************\\
		
		
        // ****************************************  METHODS  ****************************************\\

        protected sealed override T Clone(T reference)
        {
            return reference;
        }

        public sealed override bool Compare(T left, T right)
        {
            return left == right;
        }

    }
}