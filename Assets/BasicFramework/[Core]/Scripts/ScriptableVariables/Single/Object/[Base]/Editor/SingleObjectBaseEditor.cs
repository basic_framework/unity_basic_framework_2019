using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    public abstract class SingleObjectBaseEditor<T, TScriptableSingleObject> 
        : ScriptableSingleBaseEditor<T, TScriptableSingleObject>
        where T : Object
        where TScriptableSingleObject : SingleObjectBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\
		

        // ****************************************  METHODS  ****************************************\\

		
    }
}