using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Scriptable Event Single Listener",
        ScriptableVariablesMenuOrder.SCRIPTABLE_EVENT)
    ]
    public class ScriptableEventSingleListener : SingleObjectListenerBase
    <
        ScriptableEvent, 
        ScriptableEventSingle, 
        ScriptableEventUnityEvent, 
        ScriptableEventCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}