using BasicFramework.Core.ScriptableEvents;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(ScriptableEventSingleListener))][CanEditMultipleObjects]
    public class ScriptableEventSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        ScriptableEvent, 
        ScriptableEventSingle, 
        ScriptableEventUnityEvent,
        ScriptableEventCompareUnityEvent,
        ScriptableEventSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}