﻿using System;
using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Single/Scriptable Event", 
        fileName = "single_scriptable_event_", 
        order = ScriptableVariablesMenuOrder.SCRIPTABLE_EVENT)
    ]
    public class ScriptableEventSingle : SingleObjectBase<ScriptableEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public void SetValue(ScriptableEventSingle value)
        {
            Value = value.Value;
        }

        public void RaiseScriptableEvent()
        {
            if (Value == null) return;
            Value.Raise();
        }
	
    }
    
    [Serializable]
    public class ScriptableEventSingleReference : SingleObjectReferenceBase<ScriptableEvent, ScriptableEventSingle>
    {
        public ScriptableEventSingleReference()
        {
        }

        public ScriptableEventSingleReference(ScriptableEvent value)
        {
            _constantValue = value;
        }
    }
}