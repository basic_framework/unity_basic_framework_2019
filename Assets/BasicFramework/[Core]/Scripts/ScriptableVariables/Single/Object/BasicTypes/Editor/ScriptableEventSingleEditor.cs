using BasicFramework.Core.ScriptableEvents;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
    [CustomEditor(typeof(ScriptableEventSingle))][CanEditMultipleObjects]
    public class ScriptableEventSingleEditor : SingleObjectBaseEditor<ScriptableEvent, ScriptableEventSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(ScriptableEventSingle))]
    public class ScriptableEventSinglePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(ScriptableEventSingleReference))]
    public class ScriptableEventSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
}