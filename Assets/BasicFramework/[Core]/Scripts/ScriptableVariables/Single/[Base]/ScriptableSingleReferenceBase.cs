using System;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    [Serializable]
    public abstract class ScriptableSingleReferenceBase<T, TScriptableSingle>
        where TScriptableSingle : ScriptableSingleBase<T>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [SerializeField] protected bool _useConstant = true;

        [SerializeField] protected T _constantValue = default;

        [SerializeField] protected TScriptableSingle _scriptableValue = default;

        public T Value => _useConstant ? _constantValue : _scriptableValue.Value;
		
        public bool IsValueValid => _useConstant || _scriptableValue;
		
        /// <summary>Note: Setting the constant value will force the constant value to be used</summary>
        public T ConstantValue
        {
            set
            {
                _constantValue = value;
                _useConstant = true;
            }
        }


        // ****************************************  METHODS  ****************************************\\
        
        
    }
}