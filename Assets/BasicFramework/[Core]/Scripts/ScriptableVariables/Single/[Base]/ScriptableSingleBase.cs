using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single
{
    public abstract class ScriptableSingleBase<T> : ScriptableVariableBase
    {
        
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Reset Value to Default Value on enable")]
        [SerializeField] private bool _resetValueOnEnable = true;
		
        [Tooltip("The Value is set to this value on enable if ResetValueOnEnable is true")]
        [SerializeField] private T _defaultValue = default;

        [SerializeField] private T _value = default;
        
        public bool ResetValueOnEnable => _resetValueOnEnable;

        public T DefaultValue => _defaultValue;
        
        /// <summary>
        /// Reference to the class.
        /// Note: if you change data within the class make sure you call RaiseValueChanged().
        /// Otherwise the ValueChanged event will not be raised
        /// </summary>
        public T Value
        {
            get => _value;
            set
            {
                if (Compare(_value, value)) return;
                _value = value;
                RaiseValueChanged();
            }
        }
        
        public readonly BasicEvent<ScriptableSingleBase<T>> ValueChanged = new BasicEvent<ScriptableSingleBase<T>>();

        
        // ****************************************  METHODS  ****************************************\\
        
        protected abstract T Clone(T reference);

        public abstract bool Compare(T left, T right);

        public bool CompareToValue(T other)
        {
            return Compare(_value, other);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
			
            if (!_resetValueOnEnable) return;
            SetToDefaultValue();
        }
        
        public override void RaiseValueChanged()
        {
            ValueChanged.Raise(this);
        }

        public void SetToDefaultValue()
        {
            Value = Clone(_defaultValue);
        }

        public void EditorSetDefaultValueToValue()
        {
            if (!Application.isEditor) return;
            _defaultValue = Clone(_value);
        }

        public override string ToString()
        {
            return Value == null ? base.ToString() : Value.ToString();
        }
    }
}