using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	public abstract class ScriptableSingleReferenceBasePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private readonly string[] popupOptions = { "Use Constant", "Use Variable" };
		
		private GUIStyle _popupStyle;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (_popupStyle == null)
			{
				_popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"))
				{
					imagePosition = ImagePosition.ImageOnly
				};
			}
			
			var topLineRect = new Rect(
				position.x, 
				position.y, 
				position.width, 
				EditorGUIUtility.singleLineHeight);

			topLineRect = EditorGUI.PrefixLabel(topLineRect, label);
			
			var useConstant = property.FindPropertyRelative("_useConstant");
			var scriptableValue = property.FindPropertyRelative("_scriptableValue");
			var constantValue = property.FindPropertyRelative("_constantValue");

			var buttonRect = new Rect(topLineRect);
			buttonRect.yMin += _popupStyle.margin.top;
			buttonRect.width = _popupStyle.fixedWidth + _popupStyle.margin.right;
			topLineRect.xMin = buttonRect.xMax;
			
			var result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, popupOptions, _popupStyle);
			useConstant.boolValue = result == 0;

			
			// Check if it will all fit on one line, if so do that
			var singleLineHeight = EditorGUIUtility.singleLineHeight;
			
			var useSingleLine =
				Mathf.Abs(BasicEditorUtility.GetPropertyExpandedHeight(constantValue) - singleLineHeight) < Mathf.Epsilon;
			
			if (useSingleLine)
			{
				if (useConstant.boolValue)
				{
					EditorGUI.PropertyField(topLineRect, constantValue, GUIContent.none, true);
				}
				else
				{
					BasicEditorGui.PropertyFieldNotNull(topLineRect, scriptableValue, GUIContent.none, true);
				}
				
				return;
			}
			
			if (useConstant.boolValue) { GUI.enabled = false; }

			if (useConstant.boolValue)
			{
				BasicEditorGui.PropertyFieldCanBeNull(topLineRect, scriptableValue, GUIContent.none, true);
			}
			else
			{
				BasicEditorGui.PropertyFieldNotNull(topLineRect, scriptableValue, GUIContent.none, true);
			}
			
			GUI.enabled = PrevGuiEnabled;
			
			if (!useConstant.boolValue) return;
			
			var constantRect = new Rect(
				position.x, 
				position.y + EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT,
				position.width,
				position.height - EditorGUIUtility.singleLineHeight - BUFFER_HEIGHT
			);
			
			EditorGUI.indentLevel++;
			EditorGUI.PropertyField(constantRect, constantValue, true);
			EditorGUI.indentLevel--;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var useConstant = property.FindPropertyRelative("_useConstant");
			
			var singleLineHeight = EditorGUIUtility.singleLineHeight;
			
			if (!useConstant.boolValue)
			{
				return singleLineHeight;
			}
			
			// Single line height + constant array height
			
			var constantValue = property.FindPropertyRelative("_constantValue");

			var constantHeight = EditorGUI.GetPropertyHeight(constantValue);

			var useSingleLine =
				Mathf.Abs(BasicEditorUtility.GetPropertyExpandedHeight(constantValue) - singleLineHeight) < Mathf.Epsilon;
			
			return useSingleLine ? singleLineHeight : singleLineHeight + constantHeight;
		}
	
	}
}