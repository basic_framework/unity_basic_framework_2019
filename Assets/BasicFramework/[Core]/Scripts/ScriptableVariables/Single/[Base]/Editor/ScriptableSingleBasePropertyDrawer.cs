using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	public abstract class ScriptableSingleBasePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// Note: Leave value as read only, if we allow it to change here it will be very difficult to 
		// 		 make the on value changed event fire
		
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			
			if (string.IsNullOrEmpty(label.text) || property.objectReferenceValue == null)
			{
				EditorGUI.PropertyField(position, property, label);
				return;
			}

			//var foldoutRect = new Rect(position.position, new Vector2(EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight));

			
//			var obj = property.objectReferenceValue;
//			if (obj == null)
//			{
//				EditorGUI.PropertyField(position, property, label);
//				return;
//			}

			
			
			

			var scriptableVariableSerializedObject = new SerializedObject(property.objectReferenceValue);
			var value = scriptableVariableSerializedObject.FindProperty("_value");

			var labelWidth = EditorGUIUtility.labelWidth - GetIndentPixels();
			var foldoutRect = new Rect(position.position, new Vector2(labelWidth, EditorGUIUtility.singleLineHeight));
			
			var propertyRect = 
				new Rect(position.x + labelWidth, position.y,
					position.width - labelWidth, EditorGUIUtility.singleLineHeight);

			property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
			EditorGUI.PropertyField(propertyRect, property, GUIContent.none);
			
			if (!property.isExpanded) return;
			
			GUI.enabled = false;

			var valueRect = 
				new Rect(
					position.x, 
					position.y + EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT,
					position.width,
					position.height - EditorGUIUtility.singleLineHeight
				);
			
			EditorGUI.indentLevel++;
			EditorGUI.PropertyField(valueRect, value, true);
			EditorGUI.indentLevel--;

			//scriptableVariableSerializedObject.ApplyModifiedProperties();
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;
			
			var single = property.objectReferenceValue;
			if (single == null) return height;
			
			var singleSerializedObject = new SerializedObject(single);
			var value = singleSerializedObject.FindProperty("_value");

			height += BUFFER_HEIGHT + GetPropertyHeight(value, true);

			return height;

//			return EditorGUI.GetPropertyHeight(value, true) + defaultHeight + BUFFER_HEIGHT;


//			var defaultHeight = base.GetPropertyHeight(property, label);
//			
//			var obj = property.objectReferenceValue;
//			if (obj == null) return defaultHeight;

//			if (!property.isExpanded) { return defaultHeight; }
//			
//			var scriptableVariableSerializedObject = new SerializedObject(single);
//			var value = scriptableVariableSerializedObject.FindProperty("_value");
//
//			return EditorGUI.GetPropertyHeight(value, true) + defaultHeight + BUFFER_HEIGHT;
		}
		
	}
}