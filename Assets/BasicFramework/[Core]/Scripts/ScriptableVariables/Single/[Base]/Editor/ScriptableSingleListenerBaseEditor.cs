using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	public abstract class ScriptableSingleListenerBaseEditor
		<T, TScriptableSingle, TUnityEvent, TCompareUnityEvent, TScriptableSingleListener> : BasicBaseEditor<TScriptableSingleListener>
		where TScriptableSingle : ScriptableSingleBase<T>
		where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
		where TScriptableSingleListener : ScriptableSingleListenerBase<T, TScriptableSingle, TUnityEvent, TCompareUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		private ReorderableList _valueChangedCompareReorderableList;

		protected ReorderableList ValueChangedCompareReorderableList => _valueChangedCompareReorderableList;

		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var valueChangedCompare = serializedObject.FindProperty("_valueChangedCompare");
			
			_valueChangedCompareReorderableList = BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, valueChangedCompare, true);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawListenerBaseFields();
		}
		
		protected override void DrawDebugFields()
		{
			var listenValue = serializedObject.FindProperty("_listenValue");
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying && listenValue.objectReferenceValue;
			if (GUILayout.Button(GetGuiContent("Raise", "Note: Does not raise compare events")))
			{
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.EditorRaise();
				}
			}
			GUI.enabled = PrevGuiEnabled;
		}
		
		protected void DrawListenerBaseFields()
		{
			SectionHeader("Single Listener Base");
			
			var listenValue = serializedObject.FindProperty("_listenValue");
			var raiseOnStates = serializedObject.FindProperty("_raiseOnStates");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(listenValue);
			EditorGUILayout.PropertyField(raiseOnStates);
		}
		
		protected override void DrawEventFields()
		{
			SectionHeader("Single Listener Base Events");
			
			var valueChanged = serializedObject.FindProperty("_valueChanged");
			var valueChangedString = serializedObject.FindProperty("_valueChangedString");

			EditorGUILayout.PropertyField(valueChangedString);
			EditorGUILayout.PropertyField(valueChanged);
			_valueChangedCompareReorderableList.DoLayoutList();
		}

	}
}