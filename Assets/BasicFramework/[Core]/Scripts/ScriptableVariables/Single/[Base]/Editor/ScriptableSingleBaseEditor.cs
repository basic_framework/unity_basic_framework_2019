using BasicFramework.Core.ScriptableVariables.Editor;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Editor
{
	
	public abstract class ScriptableSingleBaseEditor<T, TScriptableSingle> : ScriptableVariableBaseEditor<TScriptableSingle>
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private bool _setValueToDefault;
		private bool _setDefaultValueToValue;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var value = serializedObject.FindProperty("_value");
			var resetOnEnable = serializedObject.FindProperty("_resetValueOnEnable");
			var defaultValue = serializedObject.FindProperty("_defaultValue");

			//SectionHeader($"{TypedTarget.GetType().Name.CamelCaseToHumanReadable()} Properties");
			SectionHeader($"{nameof(ScriptableSingleBase<T>).CamelCaseToHumanReadable()} Properties");
			
			Space();
			EditorGUILayout.PropertyField(resetOnEnable);
			
			Space();
			EditorGUILayout.PropertyField(defaultValue, true);
			
			Space();
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(value, true);
			if (EditorGUI.EndChangeCheck()) 
			{
				RaiseValueChangedAfter = true;
			}
			
			Space();
			if (GUILayout.Button("Set Value to Default Value"))
			{
				_setValueToDefault = true;
			}
			
			if (GUILayout.Button("Set Default Value to Value"))
			{
				_setDefaultValueToValue = true;
			}
		}

		protected override void AfterApplyingModifiedProperties()
		{
			base.AfterApplyingModifiedProperties();

			if (_setValueToDefault)
			{
				_setValueToDefault = false;
				
				Undo.RecordObjects(TypedTargets, "Set Value to Default Value");
				
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.SetToDefaultValue();
				}
			}
			
			if (_setDefaultValueToValue)
			{
				_setDefaultValueToValue = false;
				
				Undo.RecordObjects(TypedTargets, "Set Default Value to Value");
				
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.EditorSetDefaultValueToValue();
				}
			}
		}

		protected sealed override void DrawValueChangedSubscribers()
		{
			if (!Application.isPlaying) return;
			if (TargetingMultiple) return;

			BasicEditorGuiLayout.ActionList(TypedTarget.ValueChanged.Subscribers);
		}
		
	}
}