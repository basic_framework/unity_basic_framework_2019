using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single
{
	public abstract class ScriptableSingleListenerBase<T, TScriptableSingle, TUnityEvent, TCompareUnityEvent> : MonoBehaviour
		where TScriptableSingle : ScriptableSingleBase<T>
		where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
	{

		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Variable to register value changed listener to")]
		[SerializeField] private TScriptableSingle _listenValue = default;
		
		[HideInInspector]
		[EnumFlag("The ValueChanged event will be raised on the selected monobehaviour callbacks, regardless of whether it has changed")]
		[SerializeField] private SetupCallbackFlags _raiseOnStates = SetupCallbackFlags.Enable;

		[Tooltip("Raised when the value changes with the ToString value")]
		[SerializeField] protected StringUnityEvent _valueChangedString = default;
		
		[Tooltip("Raised when value is changed")]
		[SerializeField] protected TUnityEvent _valueChanged = default;
		
		[Tooltip("Raised when value is changed and the condition is met")]
		[SerializeField] protected TCompareUnityEvent[] _valueChangedCompare = default;
		
		
		private bool RaiseOnAwake => (_raiseOnStates & SetupCallbackFlags.Awake) != 0;
		private bool RaiseOnEnable => (_raiseOnStates & SetupCallbackFlags.Enable) != 0;
		private bool RaiseOnStart => (_raiseOnStates & SetupCallbackFlags.Start) != 0;
		

		public TScriptableSingle ListenValue
		{
			get => _listenValue;
			set
			{
				if (_listenValue == value) return;

				// Unsubscribe from the current event if we are subscribed
				if (_listenValue && enabled)
				{
					_listenValue.ValueChanged.Unsubscribe(RaiseValueChanged);
				}
				
				_listenValue = value;
				
				if (!enabled) return;
				
				// Subscribe to the current event if we are enabled
				if (_listenValue)
				{
					RaiseValueChanged(_listenValue);
					_listenValue.ValueChanged.Subscribe(RaiseValueChanged);
				}
				else
				{
					RaiseValueChanged(default);
				}
			}
		}

		
		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		protected virtual void Awake()
		{
			if (!RaiseOnAwake) return;
			RaiseValueChanged(_listenValue);
		}

		protected virtual void OnEnable()
		{
			if (RaiseOnEnable)
			{
				RaiseValueChanged(_listenValue);
			}
			
			_listenValue.ValueChanged.Subscribe(RaiseValueChanged);
		}

		protected virtual void OnDisable()
		{
			_listenValue.ValueChanged.Unsubscribe(RaiseValueChanged);
		}

		protected virtual void Start()
		{
			if (!RaiseOnStart) return;
			RaiseValueChanged(_listenValue);
		}

		
		//***** functions *****\\
		
		public void RaiseValueChanged()
		{
			RaiseValueChanged(_listenValue);
		}

		private void RaiseValueChanged(ScriptableSingleBase<T> scriptableSingleBase)
		{
			if (!enabled)
			{
				Debug.LogWarning($"{name} => ({GetType().Name}) cannot raise Value Changed as the Listener component is disabled");
				return;
			}

			OnRaiseValueChanged(scriptableSingleBase);
			
			var toStringValue = ValueToString(scriptableSingleBase.Value);
			
			_valueChangedString.Invoke(toStringValue);
			
			_valueChanged.Invoke(scriptableSingleBase.Value);

			foreach (var compareUnityEvent in _valueChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(scriptableSingleBase.Value);
			}
		}
		
		protected virtual void OnRaiseValueChanged(ScriptableSingleBase<T> single){}

		
		//***** functions (virtual) *****\\

		protected virtual string ValueToString(T value)
		{
			return value == null ? "NULL" : value.ToString();
		}
		
		
		//***** Editor *****\\
		
		/// <summary>
		/// Call this from an editor script to raise the value changed event at any time.
		/// </summary>
		public void EditorRaise()
		{
			if (!Application.isEditor) return;
			if (_listenValue == null) return;
			
			RaiseValueChanged(_listenValue);
		}
		
	}
}
