using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/List/Float List Listener",
        ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatListListener : ScriptableListListenerBase
    <
        float, 
        FloatList, 
        FloatListUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}