using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [CreateAssetMenu(
	    menuName = "Basic Framework/Core/Scriptable Variables/Lists/bool", 
	    fileName = "list_bool_", 
	    order = ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolList : ScriptableListBase<bool>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class BoolListUnityEvent : UnityEvent<BoolList>{}
    
}