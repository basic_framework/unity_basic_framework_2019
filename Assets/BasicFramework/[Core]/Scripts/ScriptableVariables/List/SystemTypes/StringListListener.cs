using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/List/String List Listener",
        ScriptableVariablesMenuOrder.STRING)
    ]
    public class StringListListener : ScriptableListListenerBase
    <
        string, 
        StringList, 
        StringListUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}