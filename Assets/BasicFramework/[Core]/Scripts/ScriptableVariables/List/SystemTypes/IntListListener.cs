using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/List/Int List Listener",
        ScriptableVariablesMenuOrder.INT)
    ]
    public class IntListListener : ScriptableListListenerBase
    <
        int, 
        IntList, 
        IntListUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}