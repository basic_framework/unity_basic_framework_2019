using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/List/Bool List Listener",
		ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolListListener : ScriptableListListenerBase
	    <
		    bool, 
		    BoolList, 
		    BoolListUnityEvent
		>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}