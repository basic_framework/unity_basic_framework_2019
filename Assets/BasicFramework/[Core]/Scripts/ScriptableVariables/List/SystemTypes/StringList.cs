using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Lists/String", 
        fileName = "list_string_", 
        order = ScriptableVariablesMenuOrder.STRING)
    ]
    public class StringList : ScriptableListBase<string>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class StringListUnityEvent : UnityEvent<StringList>{}
    
}