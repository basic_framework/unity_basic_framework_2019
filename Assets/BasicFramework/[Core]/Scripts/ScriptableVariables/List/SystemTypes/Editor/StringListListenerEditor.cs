using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(StringListListener))][CanEditMultipleObjects]
    public class StringListListenerEditor : ScriptableListListenerBaseEditor
    <
        string, 
        StringList, 
        StringListUnityEvent, 
        StringListListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}