using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(IntListListener))][CanEditMultipleObjects]
    public class IntListListenerEditor : ScriptableListListenerBaseEditor
    <
        int, 
        IntList, 
        IntListUnityEvent, 
        IntListListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}