using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(FloatList))][CanEditMultipleObjects]
    public class FloatListEditor : ScriptableListBaseEditor<float, FloatList>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
       
        
    }
    
    [CustomPropertyDrawer(typeof(FloatList))]
    public class FloatListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }

}