using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(BoolList))][CanEditMultipleObjects]
    public class BoolListEditor : ScriptableListBaseEditor<bool, BoolList>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
       
        
    }
    
    [CustomPropertyDrawer(typeof(BoolList))]
    public class BoolListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }

}