using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(BoolListListener))][CanEditMultipleObjects]
    public class BoolListListenerEditor : ScriptableListListenerBaseEditor
	    <
		    bool, 
		    BoolList, 
		    BoolListUnityEvent, 
		    BoolListListener
		>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}