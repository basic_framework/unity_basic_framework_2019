using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(FloatListListener))][CanEditMultipleObjects]
    public class FloatListListenerEditor : ScriptableListListenerBaseEditor
    <
        float, 
        FloatList, 
        FloatListUnityEvent, 
        FloatListListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}