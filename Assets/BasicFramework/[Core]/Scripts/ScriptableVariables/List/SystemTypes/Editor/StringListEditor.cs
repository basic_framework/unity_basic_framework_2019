using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(StringList))][CanEditMultipleObjects]
    public class StringListEditor : ScriptableListBaseEditor<string, StringList>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
       
        
    }
    
    [CustomPropertyDrawer(typeof(StringList))]
    public class StringListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }

}