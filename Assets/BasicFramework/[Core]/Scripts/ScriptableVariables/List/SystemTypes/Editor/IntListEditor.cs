using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(IntList))][CanEditMultipleObjects]
    public class IntListEditor : ScriptableListBaseEditor<int, IntList>
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
       
        
    }
    
    [CustomPropertyDrawer(typeof(IntList))]
    public class IntListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
}