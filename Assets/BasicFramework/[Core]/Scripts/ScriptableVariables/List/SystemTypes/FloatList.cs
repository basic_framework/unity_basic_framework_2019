using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Lists/Float", 
        fileName = "list_float_", 
        order = ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatList : ScriptableListBase<float>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class FloatListUnityEvent : UnityEvent<FloatList>{}
    
}