using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Lists/int", 
        fileName = "list_int_", 
        order = ScriptableVariablesMenuOrder.INT)
    ]
    public class IntList : ScriptableListBase<int>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class IntListUnityEvent : UnityEvent<IntList>{}
    
}