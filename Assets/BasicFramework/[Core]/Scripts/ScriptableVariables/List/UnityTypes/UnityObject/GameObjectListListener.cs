using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/List/Game Object List Listener",
	    ScriptableVariablesMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectListListener 
        : ScriptableListListenerBase<GameObject, GameObjectList, GameObjectListUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}