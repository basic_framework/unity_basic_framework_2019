using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(GameObjectListListener))][CanEditMultipleObjects]
    public class GameObjectListListenerEditor : ScriptableListListenerBaseEditor
    <
        GameObject, 
        GameObjectList, 
        GameObjectListUnityEvent, 
        GameObjectListListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}