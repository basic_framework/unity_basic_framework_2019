using UnityEditor;
using UnityEngine;


namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    [CustomEditor(typeof(GameObjectList))][CanEditMultipleObjects]
    public class GameObjectListEditor : ScriptableListBaseEditor<GameObject, GameObjectList>
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        
        // ****************************************  METHODS  ****************************************\\
        
        
    }
    
    [CustomPropertyDrawer(typeof(GameObjectList))]
    public class GameObjectListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        
    }
}