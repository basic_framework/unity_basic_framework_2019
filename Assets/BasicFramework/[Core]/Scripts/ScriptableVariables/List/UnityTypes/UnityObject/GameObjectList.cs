using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
    [CreateAssetMenu(
	    menuName = "Basic Framework/Core/Scriptable Variables/Lists/Game Object", 
	    fileName = "list_game_object_", 
	    order = ScriptableVariablesMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectList : ScriptableListBase<GameObject>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class GameObjectListUnityEvent : UnityEvent<GameObjectList>{}
    
}