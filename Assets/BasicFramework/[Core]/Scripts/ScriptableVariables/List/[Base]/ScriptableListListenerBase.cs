using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List
{
	public class ScriptableListListenerBase<T, TScriptableList, TUnityEvent> : MonoBehaviour 
		where TScriptableList : ScriptableListBase<T>
		where TUnityEvent : UnityEvent<TScriptableList>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[HideInInspector]
		[EnumFlag("The ValueChanged event will be raised on the selected monobehaviour callbacks, regardless of whether it has changed")]
		[SerializeField] private SetupCallbackFlags _raiseOnStates = SetupCallbackFlags.Enable;

		[Tooltip("Variable to register value changed listener to")]
		[SerializeField] private TScriptableList _scriptableList = default;
		
		[Tooltip("Response to invoke when value is changed")]
		[SerializeField] protected TUnityEvent _valueChanged = default;
	
		private bool RaiseOnAwake => (_raiseOnStates & SetupCallbackFlags.Awake) != 0;
		private bool RaiseOnEnable => (_raiseOnStates & SetupCallbackFlags.Enable) != 0;
		private bool RaiseOnStart => (_raiseOnStates & SetupCallbackFlags.Start) != 0;
		
		public TScriptableList ScriptableList
		{
			get => _scriptableList;
			set
			{
				if (_scriptableList == value) return;

				// Unsubscribe from the current event if we are subscribed
				if (_scriptableList && enabled)
				{
					_scriptableList.ValueChanged.Unsubscribe(ValueChanged);
				}
				
				_scriptableList = value;
				
				if (!enabled) return;
				
				// Subscribe to the current event if we are enabled
				if (_scriptableList)
				{
					RaiseValueChanged(_scriptableList);
					_scriptableList.ValueChanged.Subscribe(ValueChanged);
				}
				else
				{
					RaiseValueChanged(default);
				}
			}
		}
		

		// ****************************************  METHODS  ****************************************\\

		protected virtual void Awake()
		{
			if (!RaiseOnAwake) return;
			RaiseValueChanged(_scriptableList);
		}

		protected virtual void OnEnable()
		{
			if (RaiseOnEnable)
			{
				RaiseValueChanged(_scriptableList);
			}

			_scriptableList.ValueChanged.Subscribe(ValueChanged);
		}

		protected virtual void OnDisable()
		{
			_scriptableList.ValueChanged.Unsubscribe(ValueChanged);
		}

		protected virtual void Start()
		{
			if (!RaiseOnStart) return;
			RaiseValueChanged(_scriptableList);
		}

		
		private void ValueChanged(ScriptableListBase<T> list)
		{
			RaiseValueChanged((TScriptableList) list);
		}

		protected void RaiseValueChanged(TScriptableList list)
		{
			if (!enabled)
			{
				Debug.LogWarning($"{name} => ({GetType().Name}) cannot raise Value Changed as it is disabled");
				return;
			}
			_valueChanged.Invoke(list);
		}
		
		
		/// <summary>
		/// Call this from an editor script to raise the value changed event at any time.
		/// </summary>
		public void EditorRaise()
		{
			if (!Application.isEditor) return;
			if (_scriptableList == null) return;
			RaiseValueChanged(_scriptableList);
		}
		
	}
}