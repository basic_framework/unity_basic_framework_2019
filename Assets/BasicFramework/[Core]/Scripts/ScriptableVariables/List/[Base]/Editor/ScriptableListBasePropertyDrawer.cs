using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
	public abstract class ScriptableListBasePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// Note: Leave value as read only, if we allow it to change here it will be very difficult to 
		// 		 make the on value changed event fire
		
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (label == GUIContent.none)
			{
				EditorGUI.PropertyField(position, property, label);
				return;
			}

			var foldoutRect = new Rect(position.position, new Vector2(EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight));
			
			var obj = property.objectReferenceValue;

			if (obj == null)
			{
				EditorGUI.PropertyField(position, property, label);
				return;
			}
			
			var propertyRect = 
				new Rect(
					position.x + EditorGUIUtility.labelWidth,
					position.y,
					position.width - EditorGUIUtility.labelWidth, 
					EditorGUIUtility.singleLineHeight
				);

			property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
			EditorGUI.PropertyField(propertyRect, property, GUIContent.none);
			
			if (!property.isExpanded) return;
			
			GUI.enabled = false;
			
			var valueRect = 
				new Rect(
					position.x, 
					position.y + EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT,
					position.width,
					position.height - EditorGUIUtility.singleLineHeight
				);

			var scriptableListSerializedObject = new SerializedObject(obj);
			var value = scriptableListSerializedObject.FindProperty("_values");
			
			EditorGUI.indentLevel++;
			EditorGUI.PropertyField(valueRect, value, true);
			EditorGUI.indentLevel--;

			scriptableListSerializedObject.ApplyModifiedProperties();
			
			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var defaultHeight = base.GetPropertyHeight(property, label);
			
			var obj = property.objectReferenceValue;
			if (obj == null) return defaultHeight;

			if (!property.isExpanded) { return defaultHeight; }
			
			var scriptableVariableSerializedObject = new SerializedObject(obj);
			var value = scriptableVariableSerializedObject.FindProperty("_values");

			return EditorGUI.GetPropertyHeight(value, true) + defaultHeight + BUFFER_HEIGHT;
		}
		
	}
}