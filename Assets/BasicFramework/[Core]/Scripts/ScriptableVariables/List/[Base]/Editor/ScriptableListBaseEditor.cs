using BasicFramework.Core.ScriptableVariables.Editor;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
	
	public abstract class ScriptableListBaseEditor<T, TScriptableList> : ScriptableVariableBaseEditor<TScriptableList>
		where TScriptableList : ScriptableListBase<T>
	{

		// **************************************** VARIABLES ****************************************\\
		
		private ReorderableList _listReorderableList;

		private bool _isUnityObjectType;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			_isUnityObjectType = typeof(T).IsSubclassOf(typeof(Object));
			
			var values = serializedObject.FindProperty("_values");
			
			_listReorderableList = BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, values);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawScriptableListBaseFields();
		}

		protected void DrawScriptableListBaseFields()
		{
			SectionHeader("Scriptable List Fields");
			
			Space();
			DrawSettingFields();
			
			Space();
			DrawListFields();
		}
		
		private void DrawSettingFields()
		{
			SectionHeader("Settings");
			
			var clearOnEnable = serializedObject.FindProperty("_clearListOnEnable");
			
			var allowNullValues = serializedObject.FindProperty("_allowNullValues");
			var allowDuplicates = serializedObject.FindProperty("_allowDuplicates");
			
			EditorGUILayout.PropertyField(clearOnEnable);

			EditorGUILayout.PropertyField(allowNullValues);

			EditorGUILayout.PropertyField(allowDuplicates);
		}

		private void DrawListFields()
		{
			SectionHeader("List");

			if (GUILayout.Button("Validate List"))
			{
				ValidateList();
			}
			
			EditorGUI.BeginChangeCheck();
			
			DrawList();
			
			if (EditorGUI.EndChangeCheck())
			{
				RaiseValueChangedAfter = true;
			}
		}

		protected sealed override void DrawValueChangedSubscribers()
		{
			BasicEditorGuiLayout.ActionList(TypedTarget.ValueChanged.Subscribers);
		}

		private void DrawList()
		{
			if (_isUnityObjectType)
			{
				var values = serializedObject.FindProperty("_values");

				Space();
				BasicEditorGuiLayout.DragAndDropAreaIntoList<T>(values, $"Drop {typeof(T).Name}'s Here");
			}
			
			Space();
			_listReorderableList.DoLayoutList();
		}

		private void ValidateList()
		{
			// TODO: Check and make sure values are being saved after validating list
			
//			if (!_isUnityObjectType)
//			{
				foreach (var typedTarget in TypedTargets)
				{
					Undo.RecordObject(typedTarget, "Validate list");
					typedTarget.Validate();
				}

//				return;
//			}
//			
//			var values = serializedObject.FindProperty("_values");
//			
//			var allowNullValues = serializedObject.FindProperty("_allowNullValues");
//			var allowDuplicates = serializedObject.FindProperty("_allowDuplicates");
//			
//			if (!allowNullValues.boolValue && _isUnityObjectType)
//			{
//				BasicEditorArrayUtility.RemoveNullReferences(values);
//			}
//
//			if (!allowDuplicates.boolValue)
//			{
//				BasicEditorArrayUtility.RemoveDuplicateReferences(values);
//			}
		}
		
	}
}