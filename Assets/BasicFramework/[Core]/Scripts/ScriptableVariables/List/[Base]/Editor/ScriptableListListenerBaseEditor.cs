using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.List.Editor
{
    public abstract class ScriptableListListenerBaseEditor<T, TScriptableList, TUnityEvent, TListenerBase> : BasicBaseEditor<TListenerBase>
        where TScriptableList 	: ScriptableListBase<T>
        where TUnityEvent 	    : UnityEvent<TScriptableList>
        where TListenerBase     : ScriptableListListenerBase<T, TScriptableList, TUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
        

        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            Space();
            DrawSetListenerBaseEditorFields();
        }

        protected void DrawSetListenerBaseEditorFields()
        {
            var raiseOnStates = serializedObject.FindProperty("_raiseOnStates");
            var scriptableList = serializedObject.FindProperty("_scriptableList");
            var valueChanged = serializedObject.FindProperty("_valueChanged");
			
            Space();
            EditorGUILayout.PropertyField(raiseOnStates);
			
            Space();
            BasicEditorGuiLayout.PropertyFieldNotNull(scriptableList);
			
            Space();
            GUI.enabled = PrevGuiEnabled && scriptableList.objectReferenceValue && !TargetingMultiple;
            if (GUILayout.Button("Raise"))
            {
                TypedTarget.EditorRaise();
            }
            GUI.enabled = PrevGuiEnabled;
			
            Space();
            EditorGUILayout.PropertyField(valueChanged);
        }
		
    }
}