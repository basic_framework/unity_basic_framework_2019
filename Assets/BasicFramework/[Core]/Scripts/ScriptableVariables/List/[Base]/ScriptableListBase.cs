﻿using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.List
{
	public abstract class ScriptableListBase<T> : ScriptableVariableBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] protected bool _clearListOnEnable = default;

		[Tooltip("Allow null values to be added to the list")]
		[SerializeField] protected bool _allowNullValues = false;
		
		[Tooltip("Allow duplicates to be added to the list")]
		[SerializeField] protected bool _allowDuplicates = true;
		
		[SerializeField] private List<T> _values = new List<T>();

		public IEnumerable<T> Values => _values;

		public int Count => _values.Count;
		
		/// <summary>
		/// Raised when the list changes in any way.
		/// If a specific index value was changed the index value is passed, otherwise -1 is passed
		/// </summary>
		public readonly BasicEvent<ScriptableListBase<T>> ValueChanged = new BasicEvent<ScriptableListBase<T>>();
		
		// The index that was changed on ValueChanged event, -1 if ValueChanghed for diferent reason
		private int _valueChangedIndex;
		
		public int ValueChangedIndex => _valueChangedIndex;
		

		// ****************************************  METHODS  ****************************************\\
		
		public override void RaiseValueChanged()
		{
			_valueChangedIndex = -1;
			ValueChanged.Raise(this);
		}

		private void RaiseValueChanged(int index)
		{
			_valueChangedIndex = index;
			ValueChanged.Raise(this);
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			
			if (_clearListOnEnable)
			{
				_values.Clear();
				return;
			}

			Validate();
		}
		

		//********** List Functions ***********\\

		//***** Add *****\\
		
		public void Add(T value)
		{
			if (!_allowNullValues && ReferenceEquals(value, null)) return;
			if (!_allowDuplicates && _values.Contains(value)) return;
			_values.Add(value);
			RaiseValueChanged(_values.Count - 1);
		} 
		
		public void AddRange(IEnumerable<T> values)
		{
			var startCount = _values.Count;
			
			foreach (var value in values)
			{
				if (!_allowNullValues && ReferenceEquals(value, null)) continue;
				if (!_allowDuplicates && _values.Contains(value)) continue;
				_values.Add(value);
			}

			if (startCount == _values.Count) return;
			RaiseValueChanged();
		}
		
		
		//***** Remove *****\\

		public void Remove(T value)
		{
			if (!_values.Contains(value)) return;
			_values.Remove(value);
			RaiseValueChanged();
		}
		
		public void RemoveAt(int index)
		{
			_values.RemoveAt(index);
			RaiseValueChanged();
		}
		
		public void RemoveRange(int startIndex, int count)
		{
			var startCount = _values.Count;
			_values.RemoveRange(startIndex, count);
			if (startCount == _values.Count) return;
			RaiseValueChanged();
		}
		
		
		//***** Getter/Setter *****\\
		
		public T this[int index]
		{
			get => _values[index];
			set
			{
				if (!_allowNullValues && ReferenceEquals(value, null))
				{
					Debug.LogError($"Trying to set {name}[{index}] to null. This is not allowed");
					return;
				}
				if (_values[index].Equals(value)) return;
				_values[index] = value;
				RaiseValueChanged(index);
			}
		}
		
		public void SetValues(IEnumerable<T> values)
		{
			Clear();
			AddRange(values);
		}
		
		
		//***** Utility *****\\
		
		public int IndexOf(T value)
		{
			return _values.IndexOf(value);
		}
		
		public bool Contains(T value)
		{
			return _values.Contains(value);
		}
		
		public void Clear()
		{
			if (_values.Count == 0) return;
			_values.Clear();
			RaiseValueChanged();
		}
		
		public void Validate()
		{
			var isUnityObject = typeof(T).IsSubclassOf(typeof(Object));
			
			var validatedList = new List<T>(_values.Count);

			if (_allowNullValues && _allowDuplicates) return;
			
			foreach (var value in _values)
			{
				if (!_allowNullValues)
				{
					if (isUnityObject)
					{
						if (value as Object == null) continue;
					}
					else
					{
						if(ReferenceEquals(value, null)) continue;
					}
				}

				if(!_allowDuplicates && validatedList.Contains(value)) continue;
				validatedList.Add(value);
			}
			
			if(validatedList.ListEquivalent(_values)) return;
			_values = validatedList;
			RaiseValueChanged();
		}
		
	}
}