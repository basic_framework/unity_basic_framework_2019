using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    public abstract class ScriptableArrayListenerBaseEditor<T, TScriptableArray, TUnityEvent, TListenerBase> : BasicBaseEditor<TListenerBase>
        where TScriptableArray 	: ScriptableArrayBase<T>
        where TUnityEvent 	    : UnityEvent<TScriptableArray>
        where TListenerBase     : ScriptableArrayListenerBase<T, TScriptableArray, TUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            Space();
            DrawSetListenerBaseEditorFields();
        }

        protected void DrawSetListenerBaseEditorFields()
        {
            var raiseOnStates = serializedObject.FindProperty("_raiseOnStates");
            var scriptableArray = serializedObject.FindProperty("_scriptableArray");
            var valueChanged = serializedObject.FindProperty("_valueChanged");
			
            Space();
            EditorGUILayout.PropertyField(raiseOnStates);

            Space();
            BasicEditorGuiLayout.PropertyFieldNotNull(scriptableArray);

            Space();
            GUI.enabled = PrevGuiEnabled && scriptableArray.objectReferenceValue && !TargetingMultiple;
            if (GUILayout.Button("Raise"))
            {
                TypedTarget.EditorRaise();
            }
            GUI.enabled = PrevGuiEnabled;
			
            Space();
            EditorGUILayout.PropertyField(valueChanged);
        }
	
    }
}