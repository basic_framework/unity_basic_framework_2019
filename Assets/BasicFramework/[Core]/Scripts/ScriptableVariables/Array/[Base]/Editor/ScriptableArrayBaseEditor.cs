using BasicFramework.Core.ScriptableVariables.Editor;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
	public class ScriptableArrayBaseEditor<T, TScriptableArray> : ScriptableVariableBaseEditor<TScriptableArray>
		where TScriptableArray 	: ScriptableArrayBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			Space();
			DrawScriptableArrayFields();
		}

		protected void DrawScriptableArrayFields()
		{
			var resetValuesOnEnable = serializedObject.FindProperty("_resetValuesOnEnable");
			
			var values = serializedObject.FindProperty("_values");
			
			SectionHeader("Scriptable Array Fields");
			
			EditorGUILayout.PropertyField(resetValuesOnEnable);
			
			EditorGUILayout.HelpBox("Keep the values dropdown closed for better performance in editor", MessageType.Info);
			
			EditorGUI.BeginChangeCheck();
			
			EditorGUILayout.PropertyField(values, GetGuiContent($"Values ({values.arraySize})"), true);

			if (EditorGUI.EndChangeCheck())
			{
				RaiseValueChangedAfter = true;
			}
		}

		protected sealed override void DrawValueChangedSubscribers()
		{
			BasicEditorGuiLayout.ActionList(TypedTarget.ValueChanged.Subscribers);
		}

	}
}