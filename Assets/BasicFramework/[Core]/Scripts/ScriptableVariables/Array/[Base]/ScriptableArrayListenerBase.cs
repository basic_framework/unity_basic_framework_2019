using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array
{
	public class ScriptableArrayListenerBase<T, TScriptableArray, TUnityEvent> : MonoBehaviour 
		where TScriptableArray : ScriptableArrayBase<T>
		where TUnityEvent : UnityEvent<TScriptableArray>
	{
		
		// **************************************** VARIABLES ****************************************\\

		[HideInInspector]
		[EnumFlag("The ValueChanged event will be raised on the selected monobehaviour callbacks, regardless of whether it has changed")]
		[SerializeField] private SetupCallbackFlags _raiseOnStates = SetupCallbackFlags.Enable;
		
		[Tooltip("Variable to register value changed listener to")]
		[SerializeField] private TScriptableArray _scriptableArray = default;
		
		[Tooltip("Response to invoke when value is changed")]
		[SerializeField] protected TUnityEvent _valueChanged = default;

		
		private bool RaiseOnAwake => (_raiseOnStates & SetupCallbackFlags.Awake) != 0;
		private bool RaiseOnEnable => (_raiseOnStates & SetupCallbackFlags.Enable) != 0;
		private bool RaiseOnStart => (_raiseOnStates & SetupCallbackFlags.Start) != 0;
		
		public TScriptableArray ScriptableArray
		{
			get => _scriptableArray;
			set
			{
				if (_scriptableArray == value) return;

				// Unsubscribe from the current event if we are subscribed
				if (_scriptableArray && enabled)
				{
					_scriptableArray.ValueChanged.Unsubscribe(ValueChanged);
				}
				
				_scriptableArray = value;

				if (!enabled) return;
				
				// Subscribe to the current event if we are enabled
				if (_scriptableArray)
				{
					RaiseValueChanged(_scriptableArray);
					_scriptableArray.ValueChanged.Subscribe(ValueChanged);
				}
				else
				{
					RaiseValueChanged(default);
				}
			}
		}
	
		
		// ****************************************  METHODS  ****************************************\\

		protected virtual void Awake()
		{
			if (!RaiseOnAwake) return;
			RaiseValueChanged(_scriptableArray);
		}

		protected virtual void OnEnable()
		{
			if (RaiseOnEnable)
			{
				RaiseValueChanged(_scriptableArray);
			}
			
			_scriptableArray.ValueChanged.Subscribe(ValueChanged);
		}

		protected virtual void OnDisable()
		{
			_scriptableArray.ValueChanged.Unsubscribe(ValueChanged);
		}

		protected virtual void Start()
		{
			if (!RaiseOnStart) return;
			RaiseValueChanged(_scriptableArray);
		}

		private void ValueChanged(ScriptableArrayBase<T> array)
		{
			RaiseValueChanged((TScriptableArray) array);
		}

		protected void RaiseValueChanged(TScriptableArray value)
		{
			if (!enabled)
			{
				Debug.LogWarning($"{name} => ({GetType().Name}) cannot raise Value Changed as it is disabled");
				return;
			}
			
			_valueChanged.Invoke(value);
		}
		
		/// <summary>
		/// Call this from an editor script to raise the value changed event at any time.
		/// </summary>
		public void EditorRaise()
		{
			if (!Application.isEditor) return;
			if (_scriptableArray == null) return;
			RaiseValueChanged(_scriptableArray);
		}
	
	}
}