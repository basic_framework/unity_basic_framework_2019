using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    public abstract class ScriptableArrayBase<T> : ScriptableVariableBase
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        [Tooltip("Reset the array element to the default value on enable")]
        [SerializeField] protected bool _resetValuesOnEnable = default;
		
        [Tooltip("The values in the array")]
        [SerializeField] private T[] _values = new T[0];

        public IEnumerable<T> Values => _values;

        public int Length => _values.Length;

        /// <summary>
        /// Raised when the array changes in any way.
        /// If a specific index value was changed the index value is passed, otherwise -1 is passed
        /// </summary>
        public readonly BasicEvent<ScriptableArrayBase<T>> ValueChanged = new BasicEvent<ScriptableArrayBase<T>>();
        
        // The index that was changed on ValueChanged event, -1 if ValueChanghed for diferent reason
        private int _valueChangedIndex;
		
        public int ValueChangedIndex => _valueChangedIndex;
        
	
        // ****************************************  METHODS  ****************************************\\
		
        public override void RaiseValueChanged()
        {
            _valueChangedIndex = -1;
            ValueChanged.Raise(this);
        }

        private void RaiseValueChanged(int index)
        {
            _valueChangedIndex = index;
            ValueChanged.Raise(this);
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();

            if (!_resetValuesOnEnable) return;
            ResetValues();
        }
        
        //********** Array Functions **********\\
        
        /// <summary>
        /// Set the value at the index specified
        /// </summary>
        /// <param name="index"></param>
        public virtual T this[int index]
        {
            get => _values[index];
            set
            {
                if (_values[index].Equals(value)) return;
                _values[index] = value;
                RaiseValueChanged(index);
            }
        }
        
        /// <summary>
        /// Sets all the values to the type default value
        /// </summary>
        public void ResetValues()
        {
            for (int i = 0; i < _values.Length; i++)
            {
                _values[i] = default;
            }

            RaiseValueChanged();
        }

        public void SetValues(ICollection<T> values)
        {
            if (_values.Length != values.Count)
            {
                _values = new T[values.Count];
            }

            var index = 0;

            foreach (var value in values)
            {
                _values[index] = value;
                index++;
            }
            
            RaiseValueChanged();
        }
        
    }
}