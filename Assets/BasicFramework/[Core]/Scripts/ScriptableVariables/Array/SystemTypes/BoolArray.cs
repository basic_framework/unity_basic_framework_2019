using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Arrays/Bool", 
        fileName = "array_bool_", 
        order = ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolArray : ScriptableArrayBase<bool>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class BoolArrayUnityEvent : UnityEvent<BoolArray>{}
    
}