using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Array/Int Array Listener",
        ScriptableVariablesMenuOrder.INT)
    ]
    public class IntArrayListener : ScriptableArrayListenerBase<int, IntArray, IntArrayUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}