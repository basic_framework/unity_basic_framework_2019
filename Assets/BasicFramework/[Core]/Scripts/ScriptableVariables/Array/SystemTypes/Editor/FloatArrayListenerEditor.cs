using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(FloatArrayListener))][CanEditMultipleObjects]
    public class FloatArrayListenerEditor : ScriptableArrayListenerBaseEditor
    <
        float, 
        FloatArray,
        FloatArrayUnityEvent, 
        FloatArrayListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}