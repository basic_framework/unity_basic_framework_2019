using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(IntArray))][CanEditMultipleObjects]
    public class IntArrayEditor : ScriptableArrayBaseEditor<int, IntArray>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
	
    [CustomPropertyDrawer(typeof(IntArray))]
    public class IntArrayPropertyDrawer : ScriptableArrayBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        
    }
}