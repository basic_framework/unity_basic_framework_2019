using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(BoolArrayListener))][CanEditMultipleObjects]
    public class BoolArrayListenerEditor : ScriptableArrayListenerBaseEditor
	    <
		    bool, 
		    BoolArray,
		    BoolArrayUnityEvent, 
		    BoolArrayListener
		>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}