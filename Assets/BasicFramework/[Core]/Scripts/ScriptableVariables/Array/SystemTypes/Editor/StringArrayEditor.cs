using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(StringArray))][CanEditMultipleObjects]
    public class StringArrayEditor : ScriptableArrayBaseEditor<string, StringArray>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
	
    [CustomPropertyDrawer(typeof(StringArray))]
    public class StringArrayPropertyDrawer : ScriptableArrayBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        
    }
}