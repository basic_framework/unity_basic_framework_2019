using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(BoolArray))][CanEditMultipleObjects]
    public class BoolArrayEditor : ScriptableArrayBaseEditor<bool, BoolArray>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
	
    [CustomPropertyDrawer(typeof(BoolArray))]
    public class BoolArrayPropertyDrawer : ScriptableArrayBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        
    }
}