using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(IntArrayListener))][CanEditMultipleObjects]
    public class IntArrayListenerEditor : ScriptableArrayListenerBaseEditor
    <
        int, 
        IntArray,
        IntArrayUnityEvent, 
        IntArrayListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}