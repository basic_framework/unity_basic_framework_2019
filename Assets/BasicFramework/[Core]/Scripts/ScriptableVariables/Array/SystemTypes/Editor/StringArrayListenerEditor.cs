using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(StringArrayListener))][CanEditMultipleObjects]
    public class StringArrayListenerEditor : ScriptableArrayListenerBaseEditor
    <
        string, 
        StringArray,
        StringArrayUnityEvent, 
        StringArrayListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}