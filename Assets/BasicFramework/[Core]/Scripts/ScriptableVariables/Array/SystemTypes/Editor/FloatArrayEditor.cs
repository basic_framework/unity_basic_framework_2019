using UnityEditor;

namespace BasicFramework.Core.ScriptableVariables.Array.Editor
{
    [CustomEditor(typeof(FloatArray))][CanEditMultipleObjects]
    public class FloatArrayEditor : ScriptableArrayBaseEditor<float, FloatArray>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
	
    [CustomPropertyDrawer(typeof(FloatArray))]
    public class FloatArrayPropertyDrawer : ScriptableArrayBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        
    }
}