using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Array/String Array Listener",
        ScriptableVariablesMenuOrder.STRING)
    ]
    public class StringArrayListener : ScriptableArrayListenerBase<string, StringArray, StringArrayUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}