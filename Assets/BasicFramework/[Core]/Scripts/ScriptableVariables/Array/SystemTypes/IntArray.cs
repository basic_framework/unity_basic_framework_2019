using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Arrays/Int", 
        fileName = "array_int_", 
        order = ScriptableVariablesMenuOrder.INT)
    ]
    public class IntArray : ScriptableArrayBase<int>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class IntArrayUnityEvent : UnityEvent<IntArray>{}
    
}