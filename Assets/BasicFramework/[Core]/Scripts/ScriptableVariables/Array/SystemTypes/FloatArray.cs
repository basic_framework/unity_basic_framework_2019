using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Arrays/Float", 
        fileName = "array_float_", 
        order = ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatArray : ScriptableArrayBase<float>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class FloatArrayUnityEvent : UnityEvent<FloatArray>{}
    
}