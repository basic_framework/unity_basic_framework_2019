using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Array/Float Array Listener",
        ScriptableVariablesMenuOrder.FLOAT)
    ]
    public class FloatArrayListener : ScriptableArrayListenerBase<float, FloatArray, FloatArrayUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}