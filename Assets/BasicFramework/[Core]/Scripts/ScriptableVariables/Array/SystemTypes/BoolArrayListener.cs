using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [AddComponentMenu(
	    "Basic Framework/Core/Scriptable Variables/Array/Bool Array Listener",
	    ScriptableVariablesMenuOrder.BOOL)
    ]
    public class BoolArrayListener : ScriptableArrayListenerBase<bool, BoolArray, BoolArrayUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}