using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Array
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Variables/Arrays/String", 
        fileName = "array_string_", 
        order = ScriptableVariablesMenuOrder.STRING)
    ]
    public class StringArray : ScriptableArrayBase<string>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class StringArrayUnityEvent : UnityEvent<StringArray>{}
    
}