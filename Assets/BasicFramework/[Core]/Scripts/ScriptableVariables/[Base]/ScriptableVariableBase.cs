﻿using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables
{
    public abstract class ScriptableVariableBase : ScriptableObject 
    {

        // **************************************** VARIABLES ****************************************\\


        [TextArea(4, 8)]
        [SerializeField] private string _developerNote = default;

#if UNITY_EDITOR
        /// <summary>DO NOT USE! only here to stop compiler warning</summary>
        // ReSharper disable once InconsistentNaming
        protected string ___developer_note___ => Application.isEditor ? _developerNote : "";
#endif
        
        [Tooltip("Will not unload object if there are no references to it. " +
                 "This will maintain its value across the entire session")]
        [SerializeField] private bool _dontUnloadUnusedAsset = true;
        
        
        // ****************************************  METHODS  ****************************************\\

        protected virtual void OnEnable()
        {
            if (!_dontUnloadUnusedAsset) return;
            hideFlags = HideFlags.DontUnloadUnusedAsset;
        }

        // Call this to raise value changed event when overwriting the values by deserialing onto a scriptablevariable (i.e. FromJsonOverwrite) 
        public abstract void RaiseValueChanged();
    }
}