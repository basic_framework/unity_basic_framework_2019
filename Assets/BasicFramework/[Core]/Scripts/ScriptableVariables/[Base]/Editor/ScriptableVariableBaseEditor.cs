using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Editor
{
    public abstract class ScriptableVariableBaseEditor<T> : BasicBaseEditor<T>
        where T : ScriptableVariableBase
    {
	
        // **************************************** VARIABLES ****************************************\\

        protected bool RaiseValueChangedAfter;
        protected override bool HasDebugSection => true;

        // ****************************************  METHODS  ****************************************\\

        protected override void AfterApplyingModifiedProperties()
        {
            base.AfterApplyingModifiedProperties();
            
            if (!RaiseValueChangedAfter) return;
            RaiseValueChangedAfter = false;
            EditorRaiseValueChanged();
        }

        protected override void DrawDebugFields()
        {
            base.DrawDebugFields();
            
            var developerNote = serializedObject.FindProperty("_developerNote");
            
            EditorGUILayout.PropertyField(developerNote);
        }

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            var dontUnload = serializedObject.FindProperty("_dontUnloadUnusedAsset");
			
            Space();
            SectionHeader($"{nameof(ScriptableVariableBase).CamelCaseToHumanReadable()} Properties");
            
            EditorGUILayout.PropertyField(dontUnload);
			
            dontUnload.isExpanded = EditorGUILayout.Foldout(dontUnload.isExpanded, "Value Changed Subscribers");

            if (dontUnload.isExpanded)
            {
                DrawValueChangedSubscribers();
                EditorGUILayout.Space();
            }

            GUI.enabled = PrevGuiEnabled && Application.isPlaying;

            if (GUILayout.Button("Raise Value Changed"))
            {
                EditorRaiseValueChanged();
            }

            GUI.enabled = PrevGuiEnabled;
			
            EditorGUILayout.Space();
        }

        private void DrawScriptableVariableBaseFields()
        {
            
        }

        private void EditorRaiseValueChanged()
        {
            foreach (var typedTarget in TypedTargets)
            {
                typedTarget.RaiseValueChanged();
            }
        }
        
        protected abstract void DrawValueChangedSubscribers();
		
    }
}