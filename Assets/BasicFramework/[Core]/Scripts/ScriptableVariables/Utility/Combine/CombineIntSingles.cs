﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Utility/Combine Int Singles",
        ScriptableVariablesMenuOrder.INT)]
    public class CombineIntSingles : CombineSinglesBase<int, IntSingle, IntUnityEvent, IntCompareUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override int Min(IntSingle[] singles)
        {
            if (singles.Length == 0) return default;

            var value = singles[0].Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value = Math.Min(value, singles[i].Value);
            }

            return value;
        }

        protected override int Max(IntSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value = Math.Max(value, singles[i].Value);
            }

            return value;
        }

        protected override int Multiply(IntSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value *= singles[i].Value;
            }

            return value;
        }

        protected override int Average(IntSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value += singles[i].Value;
            }

            return Mathf.FloorToInt(value / (float) singles.Length);
        }
    }
}