﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
	[AddComponentMenu(
		"Basic Framework/Core/Scriptable Variables/Single/Utility/Combine Float Singles",
		ScriptableVariablesMenuOrder.FLOAT)]
	public class CombineFloatSingles : CombineSinglesBase<float, FloatSingle, FloatUnityEvent, FloatCompareUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override float Min(FloatSingle[] singles)
		{
			if (singles.Length == 0) return default;

			var value = singles[0].Value;

			for (int i = 1; i < singles.Length; i++)
			{
				value = Math.Min(value, singles[i].Value);
			}

			return value;
		}

		protected override float Max(FloatSingle[] singles)
		{
			if (singles.Length == 0) return default;
			
			var value = singles[0].Value;

			for (int i = 1; i < singles.Length; i++)
			{
				value = Math.Max(value, singles[i].Value);
			}

			return value;
		}

		protected override float Multiply(FloatSingle[] singles)
		{
			if (singles.Length == 0) return default;
			
			var value = singles[0].Value;

			for (int i = 1; i < singles.Length; i++)
			{
				value *= singles[i].Value;
			}

			return value;
		}

		protected override float Average(FloatSingle[] singles)
		{
			if (singles.Length == 0) return default;
			
			var value = singles[0].Value;

			for (int i = 1; i < singles.Length; i++)
			{
				value += singles[i].Value;
			}

			return value / singles.Length;
		}
	}
}