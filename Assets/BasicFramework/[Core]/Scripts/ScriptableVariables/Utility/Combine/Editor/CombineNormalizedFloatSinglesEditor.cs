﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
    [CustomEditor(typeof(CombineNormalizedFloatSingles))]
    public class CombineNormalizedFloatSinglesEditor : CombineSinglesBaseEditor
    <
        NormalizedFloat, 
        NormalizedFloatSingle, 
        NormalizedFloatUnityEvent, 
        NormalizedFloatCompareUnityEvent,
        CombineNormalizedFloatSingles
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawEventFields()
        {
            var combinedValueChangedPlus = serializedObject.FindProperty("_combinedValueChangedPlus");
			
            Space();
            SectionHeader(
                $"{nameof(CombineNormalizedFloatSingles).CamelCaseToHumanReadable()} Events");
			
            EditorGUILayout.PropertyField(combinedValueChangedPlus);
            
            base.DrawEventFields();
        }
        
    }
}