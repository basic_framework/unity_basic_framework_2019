﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
    [CustomEditor(typeof(CombineIntSingles))]
    public class CombineIntSinglesEditor : CombineSinglesBaseEditor
    <
        int, 
        IntSingle, 
        IntUnityEvent, 
        IntCompareUnityEvent,
        CombineIntSingles
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}