﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
	[CustomEditor(typeof(CombineFloatSingles))]
	public class CombineFloatSinglesEditor : CombineSinglesBaseEditor
	<
		float, 
		FloatSingle, 
		FloatUnityEvent, 
		FloatCompareUnityEvent,
		CombineFloatSingles
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}