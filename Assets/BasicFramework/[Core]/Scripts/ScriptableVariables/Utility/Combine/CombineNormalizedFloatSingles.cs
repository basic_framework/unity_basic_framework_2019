﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Utility/Combine Normalized Float Singles",
        ScriptableVariablesMenuOrder.NORMALIZED_FLOAT)]
    public class CombineNormalizedFloatSingles : CombineSinglesBase
    <
        NormalizedFloat, 
        NormalizedFloatSingle, 
        NormalizedFloatUnityEvent, 
        NormalizedFloatCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private NormalizedFloatPlusUnityEvent _combinedValueChangedPlus = default;
        
        
        // ****************************************  METHODS  ****************************************\\
		
        protected override NormalizedFloat Min(NormalizedFloatSingle[] singles)
        {
            if (singles.Length == 0) return default;

            var value = singles[0].Value.Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value = Math.Min(value, singles[i].Value.Value);
            }

            return new NormalizedFloat(value);
        }

        protected override NormalizedFloat Max(NormalizedFloatSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value.Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value = Math.Max(value, singles[i].Value.Value);
            }

            return new NormalizedFloat(value);
        }

        protected override NormalizedFloat Multiply(NormalizedFloatSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value.Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value *= singles[i].Value.Value;
            }

            return new NormalizedFloat(value);
        }

        protected override NormalizedFloat Average(NormalizedFloatSingle[] singles)
        {
            if (singles.Length == 0) return default;
			
            var value = singles[0].Value.Value;

            for (int i = 1; i < singles.Length; i++)
            {
                value += singles[i].Value.Value;
            }

            return new NormalizedFloat(value / singles.Length);
        }

        protected override void CombinedValueUpdated(NormalizedFloat value)
        {
            base.CombinedValueUpdated(value);
            
            _combinedValueChangedPlus.Raise(value);
        }
        
    }
}