﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
	public class CombineSinglesBaseEditor<T, TSingle, TUnityEvent, TCompareUnityEvent, TCombine> : BasicBaseEditor<TCombine>
		where TSingle : ScriptableSingleBase<T>
		where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
		where TCombine : CombineSinglesBase<T, TSingle, TUnityEvent, TCompareUnityEvent> 
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		private ReorderableList _reorderableListCompareCombined;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var combinedValueChangedCompare = serializedObject.FindProperty("_combinedValueChangedCompare");

			_reorderableListCompareCombined =
				BasicEditorGuiLayout.CreateBasicReorderableList(
					serializedObject, combinedValueChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.CombinedValue).CamelCaseToHumanReadable(), 
				TypedTarget.CombinedValue.ToString());

			RaiseEditorUpdate = GUILayout.Button("UPDATE VALUE") || RaiseEditorUpdate;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var singles = serializedObject.FindProperty("_singles");
			var combineMode = serializedObject.FindProperty("_combineMode");
			
			Space();
			SectionHeader(
				$"{nameof(CombineSinglesBase<T, TSingle, TUnityEvent, TCompareUnityEvent>).CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(combineMode);
			RaiseEditorUpdate = EditorGUI.EndChangeCheck() || RaiseEditorUpdate;
			
			GUI.enabled = GUI.enabled && !Application.isPlaying;
			
			Space();

			if (BasicEditorGuiLayout.DragAndDropAreaIntoList<TSingle>(singles,
				$"Drop {typeof(TSingle).Name.CamelCaseToHumanReadable()}'s Here"))
			{
				GUI.enabled = PrevGuiEnabled;
				return;
			}
			
			BasicEditorArrayUtility.RemoveNullReferences(singles);
			BasicEditorArrayUtility.RemoveDuplicateReferences(singles);
			
			EditorGUI.BeginChangeCheck();
			
			var enableGui = GUI.enabled && !Application.isPlaying;
			
			for (int i = singles.arraySize - 1; i >= 0; i--)
			{
				EditorGUILayout.BeginHorizontal();
				
				GUI.enabled = false;
				BasicEditorGuiLayout.PropertyFieldNotNull(singles.GetArrayElementAtIndex(i));
				GUI.enabled = enableGui;
				
				if (GUILayout.Button("-", GUILayout.Width(20)))
				{
					BasicEditorArrayUtility.RemoveArrayElement(singles, i);
				}
				
				EditorGUILayout.EndHorizontal();
			}
			
			GUI.enabled = PrevGuiEnabled;
			
			RaiseEditorUpdate = EditorGUI.EndChangeCheck() || RaiseEditorUpdate;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var combinedValueChanged = serializedObject.FindProperty("_combinedValueChanged");
			
			Space();
			SectionHeader(
				$"{nameof(CombineSinglesBase<T, TSingle, TUnityEvent, TCompareUnityEvent>).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(combinedValueChanged);
			_reorderableListCompareCombined.DoLayoutList();
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			
			TypedTarget.EditorUpdate();
		}
	}
}