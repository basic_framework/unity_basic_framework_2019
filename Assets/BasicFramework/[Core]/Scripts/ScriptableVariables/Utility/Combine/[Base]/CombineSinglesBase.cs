﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
	public enum CombineModes
	{
		Multiply,
		Min,
		Max,
		Average
	}
	
	public abstract class CombineSinglesBase<T, TSingle, TUnityEvent, TCompareUnityEvent> : MonoBehaviour
		where TSingle : ScriptableSingleBase<T>
		where TUnityEvent : UnityEvent<T>
		where TCompareUnityEvent : CompareValueUnityEventBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Multiply: Values will be multiplied")]
		[SerializeField] private CombineModes _combineMode = default;
		
		[SerializeField] private TSingle[] _singles = default;
		
		[SerializeField] private TUnityEvent _combinedValueChanged = default;

		[SerializeField] private TCompareUnityEvent[] _combinedValueChangedCompare = default;
		

		public CombineModes CombineMode
		{
			get => _combineMode;
			set
			{
				if (_combineMode == value) return;
				_combineMode = value;
				UpdatedCombinedValue();
			}
		}

		public TUnityEvent CombinedValueChanged => _combinedValueChanged;

		private T _combinedValue;
		public T CombinedValue
		{
			get => _combinedValue;
			private set
			{
				if (Equals(_combinedValue, value)) return;
				_combinedValue = value;
				CombinedValueUpdated(value);
			}
		}

		


		// ****************************************  METHODS  ****************************************\\

		
		//***** abstract *****\\

		protected abstract T Min(TSingle[] singles);
		protected abstract T Max(TSingle[] singles);
		protected abstract T Multiply(TSingle[] singles);
		protected abstract T Average(TSingle[] singles);
		

		//***** mono *****\\
		
		private void Awake()
		{
			_singles = _singles.RemoveDuplicatesAndNulls();
		}

		protected virtual void OnEnable()
		{
			UpdatedCombinedValue();

			foreach (var single in _singles)
			{
				single.ValueChanged.Subscribe(OnValueChanged);
			}
		}
		
		protected virtual void OnDisable()
		{
			foreach (var single in _singles)
			{
				single.ValueChanged.Unsubscribe(OnValueChanged);
			}
		}

		private void Start()
		{
			CombinedValueUpdated(_combinedValue);
		}


		//***** callbacks *****\\
		
		private void OnValueChanged(ScriptableSingleBase<T> obj)
		{
			UpdatedCombinedValue();
		}
		

		//***** property updated *****\\
		
		protected virtual void CombinedValueUpdated(T value)
		{
			_combinedValueChanged.Invoke(value);

			foreach (var compareUnityEvent in _combinedValueChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\

		public void UpdatedCombinedValue()
		{
			T combinedValue;
			
			switch (_combineMode)
			{
				case CombineModes.Multiply:
					combinedValue = Multiply(_singles);
					break;
					
				case CombineModes.Min:
					combinedValue = Min(_singles);
					break;
					
				case CombineModes.Max:
					combinedValue = Max(_singles);
					break;
					
				case CombineModes.Average:
					combinedValue = Average(_singles);
					break;
					
				default:
					throw new ArgumentOutOfRangeException();
			}

			CombinedValue = combinedValue;
		}
		

		#if UNITY_EDITOR

		public void EditorUpdate()
		{
			UpdatedCombinedValue();
		}
		
		#endif
		
	}
}