namespace BasicFramework.Core.ScriptableVariables
{
    public static class ScriptableVariablesMenuOrder 
    {
	
        // **************************************** VARIABLES ****************************************\\

        //public const int NONE = 0;

        public const int SYSTEM_TYPE_START = 0;
		
        public const int BOOL 	= SYSTEM_TYPE_START;
        public const int INT 	= SYSTEM_TYPE_START + 1;
        public const int FLOAT 	= SYSTEM_TYPE_START + 2;
        public const int STRING = SYSTEM_TYPE_START + 3;

		
		
        public const int UNITY_TYPE_START = 20;
		
        public const int VECTOR2 			= UNITY_TYPE_START;
        public const int VECTOR3 			= UNITY_TYPE_START + 1;
        public const int QUATERNION 		= UNITY_TYPE_START + 2;
        public const int COLOR 				= UNITY_TYPE_START + 3;
        public const int ANIMATION_CURVE 	= UNITY_TYPE_START + 4;
        public const int KEY_CODE 			= UNITY_TYPE_START + 5;
        public const int LAYER_MASK			= UNITY_TYPE_START + 6;
        public const int RUNTIME_PLATFORM	= UNITY_TYPE_START + 7;
        
        
        
		
        public const int UNITY_OBJECT_START = 40;
		
        public const int GAME_OBJECT 		= UNITY_OBJECT_START;
        public const int TRANSFORM 			= UNITY_OBJECT_START + 1;
        public const int RECT_TRANSFORM 	= UNITY_OBJECT_START + 2;
        public const int RIGIDBODY 			= UNITY_OBJECT_START + 3;
        public const int RENDER_TEXTURE 	= UNITY_OBJECT_START + 5;
        public const int MATERIAL 			= UNITY_OBJECT_START + 6;
        public const int CAMERA 			= UNITY_OBJECT_START + 7;
		
		
		
		
        public const int BASIC_TYPE_START = 60;
		
        public const int NORMALIZED_FLOAT 	= BASIC_TYPE_START;
        public const int INT_RANGE 			= BASIC_TYPE_START + 1;
        public const int FLOAT_RANGE 		= BASIC_TYPE_START + 2;
        public const int RANGED_INT 		= BASIC_TYPE_START + 3;
        public const int RANGED_FLOAT 		= BASIC_TYPE_START + 4;
        public const int BASIC_POSE 		= BASIC_TYPE_START + 5;
        public const int BASIC_RAY 			= BASIC_TYPE_START + 6;
        public const int BASIC_DATE_TIME	= BASIC_TYPE_START + 7;
        public const int TRI_BOOL			= BASIC_TYPE_START + 8;
        
		
		
        public const int BASIC_OBJECT_START = 80;
        public const int SCRIPTABLE_EVENT 		= BASIC_OBJECT_START;
		
		

        // ****************************************  METHODS  ****************************************\\


    }
}