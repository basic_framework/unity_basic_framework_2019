﻿using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Utility/Vector 2 Single Move To",
        ScriptableVariablesMenuOrder.VECTOR2)
    ]
    public class Vector2SingleMoveTo : SingleMoveToBase<Vector2, Vector2Single>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override Vector2 LerpUnclamped(Vector2 start, Vector2 end, float time)
        {
            return Vector2.LerpUnclamped(start, end, time);
        }

        protected override Vector2 MoveTo(Vector2 start, Vector2 end, float units)
        {
            return MathUtility.MoveTo(start, end, units);
        }
		
    }
}