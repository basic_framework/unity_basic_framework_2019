﻿using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
	[AddComponentMenu(
		"Basic Framework/Core/Scriptable Variables/Single/Utility/Float Single Move To",
		ScriptableVariablesMenuOrder.FLOAT)
	]
	public class FloatSingleMoveTo : SingleMoveToBase<float, FloatSingle>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override float LerpUnclamped(float start, float end, float time)
		{
			return Mathf.LerpUnclamped(start, end, time);
		}

		protected override float MoveTo(float start, float end, float units)
		{
			return MathUtility.MoveTo(start, end, units);
		}
		
	}
}