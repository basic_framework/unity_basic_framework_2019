﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Utility/Normalized Float Single Move To",
        ScriptableVariablesMenuOrder.NORMALIZED_FLOAT)
    ]
    public class NormalizedFloatSingleMoveTo : SingleMoveToBase<NormalizedFloat, NormalizedFloatSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override NormalizedFloat LerpUnclamped(NormalizedFloat start, NormalizedFloat end, float time)
        {
            return new NormalizedFloat(Mathf.LerpUnclamped(start.Value, end.Value, time));
        }

        protected override NormalizedFloat MoveTo(NormalizedFloat start, NormalizedFloat end, float units)
        {
            return new NormalizedFloat(MathUtility.MoveTo(start.Value, end.Value, units));
        }
		
    }
}