﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
	[CustomEditor(typeof(FloatSingleMoveTo))]
	public class FloatSingleMoveToEditor : SingleMoveToBaseEditor
	<
		float, 
		FloatSingle, 
		FloatSingleMoveTo
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}