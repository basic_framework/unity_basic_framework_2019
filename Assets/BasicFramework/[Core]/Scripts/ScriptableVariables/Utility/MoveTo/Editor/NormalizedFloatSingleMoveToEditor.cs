﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
    [CustomEditor(typeof(NormalizedFloatSingleMoveTo))]
    public class NormalizedFloatSingleMoveToEditor : SingleMoveToBaseEditor
    <
        NormalizedFloat, 
        NormalizedFloatSingle, 
        NormalizedFloatSingleMoveTo
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}