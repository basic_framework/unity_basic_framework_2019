﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
    [CustomEditor(typeof(Vector2SingleMoveTo))]
    public class Vector2SingleMoveToEditor : SingleMoveToBaseEditor
    <
	    Vector2, 
	    Vector2Single, 
	    Vector2SingleMoveTo
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}