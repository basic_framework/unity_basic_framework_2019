﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
    [CustomEditor(typeof(Vector3SingleMoveTo))]
    public class Vector3SingleMoveToEditor : SingleMoveToBaseEditor
    <
        Vector3, 
        Vector3Single, 
        Vector3SingleMoveTo
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}