﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility.Editor
{
	public class SingleMoveToBaseEditor<T, TSingle, TMoveTo> : BasicBaseEditor<TMoveTo>
		where TSingle : ScriptableSingleBase<T>
		where TMoveTo : SingleMoveToBase<T, TSingle>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TargetCurrent).CamelCaseToHumanReadable(), 
				TypedTarget.TargetCurrent.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var single = serializedObject.FindProperty("_single");
			var targetDefault = serializedObject.FindProperty("_targetDefault");
			
			var setupFlagsMoveOn = serializedObject.FindProperty("_setupFlagsMoveOn");
			var useUnscaledDeltaTime = serializedObject.FindProperty("_useUnscaledDeltaTime");
			var stopMovingIfChangedExternally = serializedObject.FindProperty("_stopMovingIfChangedExternally");
			
			var moveToTimingMode = serializedObject.FindProperty("_moveToTimingMode");
			var speed = serializedObject.FindProperty("_speed");
			var duration = serializedObject.FindProperty("_duration");
			
			var easingType = serializedObject.FindProperty("_easingType");
			
			var timingMode =
				EnumUtility.EnumNameToEnumValue<MoveToTimingModes>(
					moveToTimingMode.enumNames[moveToTimingMode.enumValueIndex]);
			
			
			Space();
			SectionHeader($"{nameof(SingleMoveToBase<T, TSingle>).CamelCaseToHumanReadable()} Properties");
			
			
			BasicEditorGuiLayout.PropertyFieldNotNull(single);
			EditorGUILayout.PropertyField(targetDefault);
			
			Space();
			EditorGUILayout.PropertyField(useUnscaledDeltaTime);
			EditorGUILayout.PropertyField(setupFlagsMoveOn);
			EditorGUILayout.PropertyField(stopMovingIfChangedExternally);

			Space();
			EditorGUILayout.PropertyField(moveToTimingMode);

			switch (timingMode)
			{
				case MoveToTimingModes.Speed:
					EditorGUILayout.PropertyField(speed);
					break;
				case MoveToTimingModes.Duration:
					EditorGUILayout.PropertyField(duration);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			GUI.enabled = PrevGuiEnabled && timingMode == MoveToTimingModes.Duration;
			
			EditorGUILayout.PropertyField(easingType);
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();

			var isMovingChanged = serializedObject.FindProperty("_isMovingChanged");
			
			
			Space();
			SectionHeader($"{nameof(SingleMoveToBase<T, TSingle>).CamelCaseToHumanReadable()} Events");
			
			
			EditorGUILayout.PropertyField(isMovingChanged);
		}
	}
}