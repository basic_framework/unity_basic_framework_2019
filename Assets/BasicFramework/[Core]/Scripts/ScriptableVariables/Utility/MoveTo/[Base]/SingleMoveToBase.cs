﻿using System;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
	public enum MoveToTimingModes
	{
		Speed,
		Duration
	}
	
	public abstract class SingleMoveToBase<T, TSingle> : MonoBehaviour
		where TSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		public const float MIN_MOVE_SPEED = 0.001f;
		
		[Tooltip("The scriptable single that will be moved to the target value")]
		[SerializeField] private TSingle _single = default;
		
		[Tooltip("The target value that will be used if no other value is specified")]
		[SerializeField] private T _targetDefault = default;

		[EnumFlag("Start moving to the target value on these setup callbacks")]
		[SerializeField] private SetupCallbackFlags _setupFlagsMoveOn = 0;

		[Tooltip("Use unscaled (real) delta time when moving value")]
		[SerializeField] private bool _useUnscaledDeltaTime = default;

		[Tooltip("Stop moving to the desired value if we detect the value has been changed somewhere else")]
		[SerializeField] private bool _stopMovingIfChangedExternally = default;

		[Tooltip("" +
		         "Speed => Moves to the target at a constant unit speed\n" +
		         "Duration => Takes a specified amount of time to move to the target. You can use easing in duration mode")]
		[SerializeField] private MoveToTimingModes _moveToTimingMode = MoveToTimingModes.Duration;

		[Tooltip("How fast to move (in units) toward the target")]
		[SerializeField] private float _speed = MIN_MOVE_SPEED;
		
		[Tooltip("How long it takes to move to the target")]
		[SerializeField] private float _duration = 1f;
		
		[Tooltip("The easing type to use when moving to the target over a duration of time")]
		[SerializeField] private EasingTypes _easingType = EasingTypes.Linear;
		
		[Tooltip("Raised when IsMoving changes")]
		[SerializeField] private BoolPlusUnityEvent _isMovingChanged = default;

		
		public T TargetDefault
		{
			get => _targetDefault;
			set => _targetDefault = value;
		}

		public bool UseUnscaledDeltaTime
		{
			get => _useUnscaledDeltaTime;
			set => _useUnscaledDeltaTime = value;
		}

		public bool StopMovingIfChangedExternally
		{
			get => _stopMovingIfChangedExternally;
			set => _stopMovingIfChangedExternally = value;
		}

		public float Speed
		{
			get => _speed;
			set =>_speed = Math.Max(MIN_MOVE_SPEED, value);
		}

		public float Duration
		{
			get => _duration;
			set => _duration = Math.Max(0, value);
		}

		public EasingTypes EasingType
		{
			get => _easingType;
			set => _easingType = value;
		}


		private bool _isMoving;
		public bool IsMoving
		{
			get => _isMoving;
			private set
			{
				if (_isMoving == value) return;
				_isMoving = value;
				IsMovingUpdated(value);
			}
		}


		private T _targetCurrent;
		public T TargetCurrent => _targetCurrent;

		private T _valuePrevious;
		
		private T _startCurrent;
		private float _timer;
		private float _durationCurrent;

		


		// ****************************************  METHODS  ****************************************\\

		
		//***** abstract/virtual *****\\

		protected abstract T LerpUnclamped(T start, T end, float time);

		protected abstract T MoveTo(T start, T end, float units);
		
		
		//***** mono *****\\

		protected virtual void OnValidate()
		{
			_speed = Math.Max(MIN_MOVE_SPEED, _speed);
			_duration = Math.Max(0, _duration);
		}

		protected virtual void Awake()
		{
			if (!_isMoving && (_setupFlagsMoveOn & SetupCallbackFlags.Awake) == SetupCallbackFlags.Awake)
			{
				StartMoving(_targetDefault);
			}
		}
		
		protected virtual void OnEnable()
		{
			if (!_isMoving && (_setupFlagsMoveOn & SetupCallbackFlags.Enable) == SetupCallbackFlags.Enable)
			{
				StartMoving(_targetDefault);
			}
		}

		protected virtual void OnDisable()
		{
			IsMoving = false;
		}

		protected virtual void Start()
		{
			if (!_isMoving && (_setupFlagsMoveOn & SetupCallbackFlags.Start) == SetupCallbackFlags.Start)
			{
				StartMoving(_targetDefault);
			}
			
			IsMovingUpdated(_isMoving);
		}

		protected virtual void Update()
		{
			var valueCurrent = _single.Value;
			
			if (Equals(_targetCurrent, valueCurrent) || 
			    _stopMovingIfChangedExternally && !Equals(_valuePrevious, valueCurrent))
			{
//				if (!Equals(_valuePrevious, valueCurrent))
//				{
//					Debug.Log($"{_single.name} changed externally, Previous: {_valuePrevious}, Current: {valueCurrent}");
//				}
				
				IsMoving = false;
				enabled = false;
				return;
			}

			var deltaTime = _useUnscaledDeltaTime ? Time.fixedUnscaledDeltaTime : Time.deltaTime;
			
			switch (_moveToTimingMode)
			{
				case MoveToTimingModes.Speed:
					valueCurrent = MoveTo(valueCurrent, _targetCurrent, _speed * deltaTime);
					break;
				
				case MoveToTimingModes.Duration:
					_timer += deltaTime;
					var progress = Mathf.Clamp01(_timer / _durationCurrent);
					var progressEased = Easing.Ease(_easingType, progress);
					valueCurrent = LerpUnclamped(_startCurrent,_targetCurrent, progressEased);
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			_single.Value = valueCurrent;
			_valuePrevious = valueCurrent;
		}
		

		//***** property updated *****\\
		
		private void IsMovingUpdated(bool value)
		{
			enabled = value;
			_isMovingChanged.Raise(value);
		}
		
		
		//***** functions *****\\

		public void StartMoving() => StartMoving(_targetDefault);

		public void StartMoving(TSingle target) => StartMoving(target.Value);

		public void StartMoving(T target)
		{
			_targetCurrent = target;
			_startCurrent = _single.Value;
			_valuePrevious = _startCurrent;
			
			if (Equals(_targetCurrent, _startCurrent))
			{
				IsMoving = false;
				return;
			}

			_timer = 0; 
			_durationCurrent = Math.Max(0.0001f, _duration);
			
			IsMoving = true;
		}
		
		public void StopMoving() => IsMoving = false;
	}
}