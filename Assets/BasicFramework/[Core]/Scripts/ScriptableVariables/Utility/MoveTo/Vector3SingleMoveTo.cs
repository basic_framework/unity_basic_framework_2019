﻿using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableVariables.Single.Utility
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Variables/Single/Utility/Vector 3 Single Move To",
        ScriptableVariablesMenuOrder.VECTOR3)
    ]
    public class Vector3SingleMoveTo : SingleMoveToBase<Vector3, Vector3Single>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override Vector3 LerpUnclamped(Vector3 start, Vector3 end, float time)
        {
            return Vector3.LerpUnclamped(start, end, time);
        }

        protected override Vector3 MoveTo(Vector3 start, Vector3 end, float units)
        {
            return MathUtility.MoveTo(start, end, units);
        }
		
    }
}