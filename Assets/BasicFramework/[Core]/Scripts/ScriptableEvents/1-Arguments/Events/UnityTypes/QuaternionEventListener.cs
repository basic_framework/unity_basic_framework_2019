using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Quaternion Event Listener", 
        ScriptableEventsAssetMenuOrder.QUATERNION)
    ]
    public class QuaternionEventListener : ScriptableEventListenerBase<Quaternion, QuaternionEvent, QuaternionUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}