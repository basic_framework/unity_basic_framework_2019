using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Vector2 Event Listener", 
        ScriptableEventsAssetMenuOrder.VECTOR2)
    ]
    public class Vector2EventListener : ScriptableEventListenerBase<Vector2, Vector2Event, Vector2UnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}