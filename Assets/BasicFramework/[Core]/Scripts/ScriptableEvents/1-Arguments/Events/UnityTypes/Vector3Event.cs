using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Vector3", 
        fileName = "event_vector3_", 
        order = ScriptableEventsAssetMenuOrder.VECTOR3)
    ]
    public class Vector3Event : ScriptableEventBase<Vector3>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}