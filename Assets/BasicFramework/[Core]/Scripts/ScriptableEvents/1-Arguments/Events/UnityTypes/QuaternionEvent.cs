using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Quaternion", 
        fileName = "event_quaternion_", 
        order = ScriptableEventsAssetMenuOrder.QUATERNION)
    ]
    public class QuaternionEvent : ScriptableEventBase<Quaternion>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}