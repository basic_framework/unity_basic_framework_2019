using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(Vector2EventListener))][CanEditMultipleObjects]
    public class Vector2EventListenerEditor : ScriptableEventListenerBaseEditor
    <
        Vector2, 
        Vector2Event, 
        Vector2UnityEvent,
        Vector2EventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}