using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Vector2", 
        fileName = "event_vector2_", 
        order = ScriptableEventsAssetMenuOrder.VECTOR2)
    ]
    public class Vector2Event : ScriptableEventBase<Vector2>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}