using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Color", 
        fileName = "event_color_", 
        order = ScriptableEventsAssetMenuOrder.COLOR)
    ]
    public class ColorEvent : ScriptableEventBase<Color>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}