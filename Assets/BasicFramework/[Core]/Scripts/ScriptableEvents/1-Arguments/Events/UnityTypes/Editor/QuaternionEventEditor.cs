using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(QuaternionEvent))][CanEditMultipleObjects]
    public class QuaternionEventEditor : ScriptableEventBaseEditor<Quaternion, QuaternionEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(QuaternionEvent))]
    public class QuaternionEventPropertyDrawer : ScriptableEventBasePropertyDrawer<Quaternion, QuaternionEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}