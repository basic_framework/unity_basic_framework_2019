using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Vector3 Event Listener", 
        ScriptableEventsAssetMenuOrder.VECTOR3)
    ]
    public class Vector3EventListener : ScriptableEventListenerBase<Vector3, Vector3Event, Vector3UnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}