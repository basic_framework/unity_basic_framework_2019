using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ColorEvent))][CanEditMultipleObjects]
    public class ColorEventEditor : ScriptableEventBaseEditor<Color, ColorEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(ColorEvent))]
    public class ColorEventPropertyDrawer : ScriptableEventBasePropertyDrawer<Color, ColorEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}