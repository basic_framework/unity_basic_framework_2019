using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(QuaternionEventListener))][CanEditMultipleObjects]
    public class QuaternionEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        Quaternion, 
        QuaternionEvent, 
        QuaternionUnityEvent,
        QuaternionEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}