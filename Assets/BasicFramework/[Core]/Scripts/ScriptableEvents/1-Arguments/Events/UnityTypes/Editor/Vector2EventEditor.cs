using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(Vector2Event))][CanEditMultipleObjects]
    public class Vector2EventEditor : ScriptableEventBaseEditor<Vector2, Vector2Event>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(Vector2Event))]
    public class Vector2EventPropertyDrawer : ScriptableEventBasePropertyDrawer<Vector2, Vector2Event>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}