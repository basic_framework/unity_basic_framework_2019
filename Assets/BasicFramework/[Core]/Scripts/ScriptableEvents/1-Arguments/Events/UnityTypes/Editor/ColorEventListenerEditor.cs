using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ColorEventListener))][CanEditMultipleObjects]
    public class ColorEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        Color, 
        ColorEvent, 
        ColorUnityEvent,
        ColorEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}