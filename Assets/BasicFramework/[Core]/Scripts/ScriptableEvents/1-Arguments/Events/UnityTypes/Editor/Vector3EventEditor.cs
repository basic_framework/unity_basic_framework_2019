using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(Vector3Event))][CanEditMultipleObjects]
    public class Vector3EventEditor : ScriptableEventBaseEditor<Vector3, Vector3Event>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(Vector3Event))]
    public class Vector3EventPropertyDrawer : ScriptableEventBasePropertyDrawer<Vector3, Vector3Event>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}