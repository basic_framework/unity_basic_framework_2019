using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(Vector3EventListener))][CanEditMultipleObjects]
    public class Vector3EventListenerEditor : ScriptableEventListenerBaseEditor
    <
        Vector3, 
        Vector3Event, 
        Vector3UnityEvent,
        Vector3EventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}