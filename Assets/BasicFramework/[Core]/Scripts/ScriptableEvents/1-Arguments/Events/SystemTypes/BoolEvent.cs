using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
	    menuName = "Basic Framework/Core/Scriptable Events/One Arg/Bool", 
	    fileName = "event_bool_", 
	    order = ScriptableEventsAssetMenuOrder.BOOL)
    ]
    public class BoolEvent : ScriptableEventBase<bool>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}