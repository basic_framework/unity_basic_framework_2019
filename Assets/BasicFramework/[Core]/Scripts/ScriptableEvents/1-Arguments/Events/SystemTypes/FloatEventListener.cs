using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Float Event Listener", 
        ScriptableEventsAssetMenuOrder.FLOAT)
    ]
    public class FloatEventListener : ScriptableEventListenerBase<float, FloatEvent, FloatUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}