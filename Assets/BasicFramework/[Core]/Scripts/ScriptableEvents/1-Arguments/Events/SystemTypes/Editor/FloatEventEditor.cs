using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(FloatEvent))][CanEditMultipleObjects]
    public class FloatEventEditor : ScriptableEventBaseEditor<float, FloatEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(FloatEvent))]
    public class FloatEventPropertyDrawer : ScriptableEventBasePropertyDrawer<float, FloatEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}