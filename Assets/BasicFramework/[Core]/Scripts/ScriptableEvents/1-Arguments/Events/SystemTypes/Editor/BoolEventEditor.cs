using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BoolEvent))][CanEditMultipleObjects]
    public class BoolEventEditor : ScriptableEventBaseEditor<bool, BoolEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(BoolEvent))]
    public class BoolEventPropertyDrawer : ScriptableEventBasePropertyDrawer<bool, BoolEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}