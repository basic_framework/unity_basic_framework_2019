using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Int", 
        fileName = "event_int_", 
        order = ScriptableEventsAssetMenuOrder.INT)
    ]
    public class IntEvent : ScriptableEventBase<int>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}