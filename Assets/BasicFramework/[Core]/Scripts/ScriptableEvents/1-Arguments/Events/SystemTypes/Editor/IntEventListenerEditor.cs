using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(IntEventListener))][CanEditMultipleObjects]
    public class IntEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        int, 
        IntEvent, 
        IntUnityEvent,
        IntEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}