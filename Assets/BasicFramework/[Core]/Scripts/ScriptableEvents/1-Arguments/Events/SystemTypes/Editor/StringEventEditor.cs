using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(StringEvent))][CanEditMultipleObjects]
    public class StringEventEditor : ScriptableEventBaseEditor<string, StringEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(StringEvent))]
    public class StringEventPropertyDrawer : ScriptableEventBasePropertyDrawer<string, StringEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}