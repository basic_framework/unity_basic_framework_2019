using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Float", 
        fileName = "event_float_", 
        order = ScriptableEventsAssetMenuOrder.FLOAT)
    ]
    public class FloatEvent : ScriptableEventBase<float>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}