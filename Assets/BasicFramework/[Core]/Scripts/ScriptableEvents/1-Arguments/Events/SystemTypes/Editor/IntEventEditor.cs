using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(IntEvent))][CanEditMultipleObjects]
    public class IntEventEditor : ScriptableEventBaseEditor<int, IntEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(IntEvent))]
    public class IntEventPropertyDrawer : ScriptableEventBasePropertyDrawer<int, IntEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}