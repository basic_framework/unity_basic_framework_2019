using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/String", 
        fileName = "event_string_", 
        order = ScriptableEventsAssetMenuOrder.STRING)
    ]
    public class StringEvent : ScriptableEventBase<string>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}