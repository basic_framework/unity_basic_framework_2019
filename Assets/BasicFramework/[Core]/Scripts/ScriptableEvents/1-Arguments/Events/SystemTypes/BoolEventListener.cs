using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Bool Event Listener", 
        ScriptableEventsAssetMenuOrder.BOOL)
    ]
    public class BoolEventListener : ScriptableEventListenerBase<bool, BoolEvent, BoolUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BoolPlusUnityEvent _responsePlus = default;


        // ****************************************  METHODS  ****************************************\\

        protected override void OnEventRaised(bool arg0)
        {
            base.OnEventRaised(arg0);
			
            _responsePlus.Raise(arg0);
        }
        
    }
}