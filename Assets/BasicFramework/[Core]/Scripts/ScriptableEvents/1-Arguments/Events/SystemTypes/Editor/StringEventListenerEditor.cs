using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(StringEventListener))][CanEditMultipleObjects]
    public class StringEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        string, 
        StringEvent, 
        StringUnityEvent,
        StringEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}