using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BoolEventListener))][CanEditMultipleObjects]
    public class BoolEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        bool, 
        BoolEvent, 
        BoolUnityEvent,
        BoolEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            Space();
            DrawBoolEventListenerFields();
        }

        private void DrawBoolEventListenerFields()
        {
            var responsePlus = serializedObject.FindProperty("_responsePlus");
	        
            var invertedValue = responsePlus.FindPropertyRelative("_invertedValue");
            var isTrue = responsePlus.FindPropertyRelative("_isTrue");
            var isFalse = responsePlus.FindPropertyRelative("_isFalse");

            EditorGUILayout.PropertyField(invertedValue, true);
            EditorGUILayout.PropertyField(isTrue, true);
            EditorGUILayout.PropertyField(isFalse, true);
        }
        
    }
}