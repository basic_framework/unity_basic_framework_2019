using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Int Event Listener", 
        ScriptableEventsAssetMenuOrder.INT)
    ]
    public class IntEventListener : ScriptableEventListenerBase<int, IntEvent, IntUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}