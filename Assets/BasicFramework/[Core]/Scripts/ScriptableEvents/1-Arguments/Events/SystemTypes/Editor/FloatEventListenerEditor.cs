using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(FloatEventListener))][CanEditMultipleObjects]
    public class FloatEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        float, 
        FloatEvent, 
        FloatUnityEvent,
        FloatEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}