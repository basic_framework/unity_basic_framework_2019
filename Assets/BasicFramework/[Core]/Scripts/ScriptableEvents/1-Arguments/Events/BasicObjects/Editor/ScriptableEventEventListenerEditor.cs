using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ScriptableEventEventListener))][CanEditMultipleObjects]
    public class ScriptableEventEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        ScriptableEvent, 
        ScriptableEventEvent, 
        ScriptableEventUnityEvent,
        ScriptableEventEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}