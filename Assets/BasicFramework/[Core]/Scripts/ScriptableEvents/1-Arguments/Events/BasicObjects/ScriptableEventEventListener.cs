using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Scriptable Event Event Listener", 
        ScriptableEventsAssetMenuOrder.SCRIPTABLE_EVENT)
    ]
    public class ScriptableEventEventListener : ScriptableEventListenerBase<ScriptableEvent, ScriptableEventEvent, ScriptableEventUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}