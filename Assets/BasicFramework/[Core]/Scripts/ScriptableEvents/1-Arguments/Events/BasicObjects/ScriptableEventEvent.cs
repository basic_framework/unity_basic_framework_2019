﻿using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Scriptable Event", 
        fileName = "event_scriptable_event_", 
        order = ScriptableEventsAssetMenuOrder.SCRIPTABLE_EVENT)
    ]
    public class ScriptableEventEvent : ScriptableEventBase<ScriptableEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}