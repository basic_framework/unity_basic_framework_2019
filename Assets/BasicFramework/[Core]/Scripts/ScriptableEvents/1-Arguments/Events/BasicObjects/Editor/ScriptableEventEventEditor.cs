using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ScriptableEventEvent))][CanEditMultipleObjects]
    public class ScriptableEventEventEditor : ScriptableEventBaseEditor<ScriptableEvent, ScriptableEventEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(ScriptableEventEvent))]
    public class ScriptableEventEventPropertyDrawer : ScriptableEventBasePropertyDrawer<ScriptableEvent, ScriptableEventEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}