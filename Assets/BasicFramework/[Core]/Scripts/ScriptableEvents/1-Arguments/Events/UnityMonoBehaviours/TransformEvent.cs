using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Transform", 
        fileName = "event_transform_", 
        order = ScriptableEventsAssetMenuOrder.TRANSFORM)
    ]
    public class TransformEvent : ScriptableEventBase<Transform>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}