using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Rect Transform Event Listener", 
        ScriptableEventsAssetMenuOrder.RECT_TRANSFORM)
    ]
    public class RectTransformEventListener : ScriptableEventListenerBase<RectTransform, RectTransformEvent, RectTransformUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}