using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Rect Transform", 
        fileName = "event_rect_transform_", 
        order = ScriptableEventsAssetMenuOrder.RECT_TRANSFORM)
    ]
    public class RectTransformEvent : ScriptableEventBase<RectTransform>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}