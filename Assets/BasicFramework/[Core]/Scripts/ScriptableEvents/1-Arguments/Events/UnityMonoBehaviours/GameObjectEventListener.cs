using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Game Object Event Listener", 
        ScriptableEventsAssetMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectEventListener : ScriptableEventListenerBase<GameObject, GameObjectEvent, GameObjectUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}