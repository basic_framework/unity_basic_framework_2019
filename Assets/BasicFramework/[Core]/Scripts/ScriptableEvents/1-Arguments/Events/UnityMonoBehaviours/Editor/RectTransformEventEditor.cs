using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(RectTransformEvent))][CanEditMultipleObjects]
    public class RectTransformEventEditor : ScriptableEventBaseEditor<RectTransform, RectTransformEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(RectTransformEvent))]
    public class RectTransformEventPropertyDrawer : ScriptableEventBasePropertyDrawer<RectTransform, RectTransformEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}