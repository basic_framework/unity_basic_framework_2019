using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Game Object", 
        fileName = "event_game_object_", 
        order = ScriptableEventsAssetMenuOrder.GAME_OBJECT)
    ]
    public class GameObjectEvent : ScriptableEventBase<GameObject>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}