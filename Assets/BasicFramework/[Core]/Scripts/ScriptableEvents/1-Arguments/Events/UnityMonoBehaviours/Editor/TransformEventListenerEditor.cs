using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(TransformEventListener))][CanEditMultipleObjects]
    public class TransformEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        Transform, 
        TransformEvent, 
        TransformUnityEvent,
        TransformEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}