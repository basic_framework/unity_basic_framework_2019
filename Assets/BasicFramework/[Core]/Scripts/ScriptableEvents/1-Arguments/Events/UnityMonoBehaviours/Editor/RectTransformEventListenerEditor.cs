using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(RectTransformEventListener))][CanEditMultipleObjects]
    public class RectTransformEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        RectTransform, 
        RectTransformEvent, 
        RectTransformUnityEvent,
        RectTransformEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}