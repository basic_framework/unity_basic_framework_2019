using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(GameObjectEventListener))][CanEditMultipleObjects]
    public class GameObjectEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        GameObject, 
        GameObjectEvent, 
        GameObjectUnityEvent,
        GameObjectEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}