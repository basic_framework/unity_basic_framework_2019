using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(TransformEvent))][CanEditMultipleObjects]
    public class TransformEventEditor : ScriptableEventBaseEditor<Transform, TransformEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(TransformEvent))]
    public class TransformEventPropertyDrawer : ScriptableEventBasePropertyDrawer<Transform, TransformEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}