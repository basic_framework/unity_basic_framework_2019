using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(GameObjectEvent))][CanEditMultipleObjects]
    public class GameObjectEventEditor : ScriptableEventBaseEditor<GameObject, GameObjectEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(GameObjectEvent))]
    public class GameObjectEventPropertyDrawer : ScriptableEventBasePropertyDrawer<GameObject, GameObjectEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}