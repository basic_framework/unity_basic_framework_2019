﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Basic Pose", 
        fileName = "event_pose_", 
        order = ScriptableEventsAssetMenuOrder.BASIC_POSE)
    ]
    public class BasicPoseEvent : ScriptableEventBase<BasicPose>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void Raise(BasicPoseSingle basicPoseSingle)
        {
            base.Raise(basicPoseSingle.Value);
        }
        
        public void Raise(Transform transformDesired)
        {
            base.Raise(new BasicPose(transformDesired));
        }
        
    }
}