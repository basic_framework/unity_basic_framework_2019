using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicPoseEvent))][CanEditMultipleObjects]
    public class BasicPoseEventEditor : ScriptableEventBaseEditor<BasicPose, BasicPoseEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(BasicPoseEvent))]
    public class BasicPoseEventPropertyDrawer : ScriptableEventBasePropertyDrawer<BasicPose, BasicPoseEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}