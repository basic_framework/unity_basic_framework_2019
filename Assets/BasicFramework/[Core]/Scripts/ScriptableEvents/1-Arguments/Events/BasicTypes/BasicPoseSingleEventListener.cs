﻿using BasicFramework.Core.ScriptableEvents.Utility;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Basic Pose Single Event Listener", 
        ScriptableEventsAssetMenuOrder.BASIC_POSE_SINGLE)
    ]
    public class BasicPoseSingleEventListener : ScriptableEventListenerBase
    <
        BasicPoseSingle, 
        BasicPoseSingleEvent, 
        BasicPoseSingleUnityEvent
    >
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}