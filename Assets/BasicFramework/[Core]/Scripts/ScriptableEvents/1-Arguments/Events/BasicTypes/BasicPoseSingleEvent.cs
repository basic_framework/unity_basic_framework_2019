﻿using BasicFramework.Core.ScriptableEvents.Utility;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Basic Pose Single", 
        fileName = "event_pose_single_", 
        order = ScriptableEventsAssetMenuOrder.BASIC_POSE_SINGLE)
    ]
    public class BasicPoseSingleEvent : ScriptableEventBase<BasicPoseSingle>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        

    }
}