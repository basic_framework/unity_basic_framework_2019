﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicDateTimeEvent))][CanEditMultipleObjects]
    public class BasicDateTimeEventEditor : ScriptableEventBaseEditor<BasicDateTime, BasicDateTimeEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(BasicDateTimeEvent))]
    public class BasicDateTimeEventPropertyDrawer : ScriptableEventBasePropertyDrawer<BasicDateTime, BasicDateTimeEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}