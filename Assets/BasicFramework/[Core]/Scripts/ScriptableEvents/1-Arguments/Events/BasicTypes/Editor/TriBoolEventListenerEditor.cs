﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(TriBoolEventListener))][CanEditMultipleObjects]
    public class TriBoolEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        TriBool, 
        TriBoolEvent, 
        TriBoolUnityEvent,
        TriBoolEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}