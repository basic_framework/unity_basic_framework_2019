﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Tri Bool", 
        fileName = "event_tri_bool_", 
        order = ScriptableEventsAssetMenuOrder.TRI_BOOL)
    ]
    public class TriBoolEvent : ScriptableEventBase<TriBool>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void Raise(TriBoolSingle basicPoseSingle)
        {
            base.Raise(basicPoseSingle.Value);
        }
        
    }
}