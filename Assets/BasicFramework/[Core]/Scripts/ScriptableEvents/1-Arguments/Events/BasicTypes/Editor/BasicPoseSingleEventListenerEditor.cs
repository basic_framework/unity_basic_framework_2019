﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicPoseSingleEventListener))][CanEditMultipleObjects]
    public class BasicPoseSingleEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        BasicPoseSingle, 
        BasicPoseSingleEvent, 
        BasicPoseSingleUnityEvent,
        BasicPoseSingleEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}