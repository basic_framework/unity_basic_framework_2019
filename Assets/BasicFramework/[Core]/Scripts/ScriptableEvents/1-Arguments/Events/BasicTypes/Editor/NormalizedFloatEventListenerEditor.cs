using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(NormalizedFloatEventListener))][CanEditMultipleObjects]
    public class NormalizedFloatEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        NormalizedFloat, 
        NormalizedFloatEvent, 
        NormalizedFloatUnityEvent,
        NormalizedFloatEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}