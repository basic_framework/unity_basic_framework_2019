﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Basic Date Time Event Listener", 
        ScriptableEventsAssetMenuOrder.BASIC_DATE_TIME)
    ]
    public class BasicDateTimeEventListener : ScriptableEventListenerBase<BasicDateTime, BasicDateTimeEvent, BasicDateTimeUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BasicDateTimePlusUnityEvent _responsePlus = default;

        
        public BasicDateTimePlusUnityEvent ResponsePlus => _responsePlus;
        

        // ****************************************  METHODS  ****************************************\\

        protected override void OnEventRaised(BasicDateTime arg0)
        {
            base.OnEventRaised(arg0);
            
            _responsePlus.Raise(arg0);
        }
    }
}