﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Basic Date Time", 
        fileName = "event_DateTime_", 
        order = ScriptableEventsAssetMenuOrder.BASIC_DATE_TIME)
    ]
    public class BasicDateTimeEvent : ScriptableEventBase<BasicDateTime>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void Raise(BasicDateTimeSingle basicDateTimeSingle)
        {
            base.Raise(basicDateTimeSingle.Value);
        }
        
    }
}