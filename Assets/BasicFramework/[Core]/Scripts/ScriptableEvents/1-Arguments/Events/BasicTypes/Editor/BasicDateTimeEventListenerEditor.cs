﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicDateTimeEventListener))][CanEditMultipleObjects]
    public class BasicDateTimeEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        BasicDateTime, 
        BasicDateTimeEvent, 
        BasicDateTimeUnityEvent,
        BasicDateTimeEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawEventFields()
        {
            var responsePlus = serializedObject.FindProperty("_responsePlus");
            
            Space();
            SectionHeader("Basic Date Time Event Listener Events");
            
            EditorGUILayout.PropertyField(responsePlus);
            
            base.DrawEventFields();
            
//            var stringDateTime = responsePlus.FindPropertyRelative("_stringDateTime");
//            var stringDate = responsePlus.FindPropertyRelative("_stringDate");
//            var stringTimeOfDay = responsePlus.FindPropertyRelative("_stringTimeOfDay");
//            
//            var basicDateTime = responsePlus.FindPropertyRelative("_basicDateTime");
//            var dateTime = responsePlus.FindPropertyRelative("_dateTime");
//            var date = responsePlus.FindPropertyRelative("_date");
//            var timeOfDay = responsePlus.FindPropertyRelative("_timeOfDay");
//            
//            //TODO possibly remove
//            EditorGUILayout.PropertyField(stringDateTime);
//            EditorGUILayout.PropertyField(stringDate);
//            EditorGUILayout.PropertyField(stringTimeOfDay);
//            
//            EditorGUILayout.PropertyField(basicDateTime);
//            EditorGUILayout.PropertyField(dateTime);
//            EditorGUILayout.PropertyField(date);
//            EditorGUILayout.PropertyField(timeOfDay);
        }
        
    }
}