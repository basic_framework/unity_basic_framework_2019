using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(NormalizedFloatEvent))][CanEditMultipleObjects]
    public class NormalizedFloatEventEditor : ScriptableEventBaseEditor<NormalizedFloat, NormalizedFloatEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(NormalizedFloatEvent))]
    public class NormalizedFloatEventPropertyDrawer : ScriptableEventBasePropertyDrawer<NormalizedFloat, NormalizedFloatEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}