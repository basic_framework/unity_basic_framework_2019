using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/One Arg/Normalized Float", 
        fileName = "event_normalized_float_", 
        order = ScriptableEventsAssetMenuOrder.NORMALIZED_FLOAT)
    ]
    public class NormalizedFloatEvent : ScriptableEventBase<NormalizedFloat>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}