﻿using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(TriBoolEvent))][CanEditMultipleObjects]
    public class TriBoolEventEditor : ScriptableEventBaseEditor<TriBool, TriBoolEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(TriBoolEvent))]
    public class TriBoolEventPropertyDrawer : ScriptableEventBasePropertyDrawer<TriBool, TriBoolEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}