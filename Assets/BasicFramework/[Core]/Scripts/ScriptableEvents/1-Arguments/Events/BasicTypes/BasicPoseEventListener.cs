using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Basic Pose Event Listener", 
        ScriptableEventsAssetMenuOrder.BASIC_POSE)
    ]
    public class BasicPoseEventListener : ScriptableEventListenerBase<BasicPose, BasicPoseEvent, BasicPoseUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}