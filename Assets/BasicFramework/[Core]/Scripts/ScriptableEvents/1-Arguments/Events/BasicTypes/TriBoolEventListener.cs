﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Tri Bool Event Listener", 
        ScriptableEventsAssetMenuOrder.TRI_BOOL)
    ]
    public class TriBoolEventListener : ScriptableEventListenerBase<TriBool, TriBoolEvent, TriBoolUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}