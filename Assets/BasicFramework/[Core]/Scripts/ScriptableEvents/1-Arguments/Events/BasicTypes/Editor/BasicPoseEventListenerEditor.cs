using BasicFramework.Core.BasicTypes;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicPoseEventListener))][CanEditMultipleObjects]
    public class BasicPoseEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        BasicPose, 
        BasicPoseEvent, 
        BasicPoseUnityEvent,
        BasicPoseEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}