﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(BasicPoseSingleEvent))][CanEditMultipleObjects]
    public class BasicPoseSingleEventEditor : ScriptableEventBaseEditor<BasicPoseSingle, BasicPoseSingleEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }

    [CustomPropertyDrawer(typeof(BasicPoseSingleEvent))]
    public class BasicPoseSingleEventPropertyDrawer : ScriptableEventBasePropertyDrawer<BasicPoseSingle, BasicPoseSingleEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

		
    }
}