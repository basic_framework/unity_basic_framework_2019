using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/One Arg/Normalized Float Event Listener", 
        ScriptableEventsAssetMenuOrder.NORMALIZED_FLOAT)
    ]
    public class NormalizedFloatEventListener : ScriptableEventListenerBase<NormalizedFloat, NormalizedFloatEvent, NormalizedFloatUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}