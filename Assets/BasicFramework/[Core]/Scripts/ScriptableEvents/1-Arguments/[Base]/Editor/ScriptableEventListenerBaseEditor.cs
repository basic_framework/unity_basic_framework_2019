using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    public abstract class ScriptableEventListenerBaseEditor<T0, TScriptableEventBase, TUnityEvent, TScriptableEventListenerBase> : BasicBaseEditor<TScriptableEventListenerBase>
        where TScriptableEventBase : ScriptableEventBase<T0>
        where TUnityEvent : UnityEvent<T0>
        where TScriptableEventListenerBase : ScriptableEventListenerBase<T0, TScriptableEventBase, TUnityEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            //base.DrawPropertyFields();
            
            var scriptableEvent = serializedObject.FindProperty("_scriptableEvent");
            var editorLogReminder = serializedObject.FindProperty("_editorLogReminder");
            var prevSciptableEvent = scriptableEvent.objectReferenceValue;
            
            Space();
            SectionHeader("Scriptable Event Listener Properties");
            
            EditorGUI.BeginChangeCheck();
            BasicEditorGuiLayout.PropertyFieldNotNull(scriptableEvent);
            if (EditorGUI.EndChangeCheck())
            {
                TypedTarget.EditorScriptableEventChanged(
                    prevSciptableEvent as TScriptableEventBase, 
                    scriptableEvent.objectReferenceValue as TScriptableEventBase);
            }

            EditorGUILayout.PropertyField(editorLogReminder);
        }

        protected override void DrawEventFields()
        {
            base.DrawEventFields();
            
            var response = serializedObject.FindProperty("_response");
            
            Space();
            SectionHeader("Scriptable Event Listener Events");
            
            EditorGUILayout.PropertyField(response);
        }
        
    }
}