using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    public abstract class ScriptableEventBaseEditor<T0, TScriptableEventBase> : BasicBaseEditor<TScriptableEventBase>
        where TScriptableEventBase : ScriptableEventBase<T0>
    {

        // **************************************** VARIABLES ****************************************\\

        protected override bool HasDebugSection => true;
        
        public virtual bool TypeIsSerializable => true;
        
        
        
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawDebugFields()
        {
            base.DrawDebugFields();
            
            var developerNote = serializedObject.FindProperty("_developerNote");

            EditorGUILayout.PropertyField(developerNote);
            Space();
            
            if (TypeIsSerializable)
            {
                var testArg0 = serializedObject.FindProperty("_testArg0");
                EditorGUILayout.PropertyField(testArg0, true);
            }
            else
            {
                EditorGUILayout.HelpBox($"{typeof(T0).Name} is not serializeable. " +
                                        $"Cannot raise a test argument", MessageType.Info);
            }
            
            GUI.enabled = PrevGuiEnabled && Application.isPlaying && TypeIsSerializable;
	        
            if (GUILayout.Button("Raise"))
            {
                foreach (var typedTarget in TypedTargets)
                {
                    typedTarget.EditorRaise();
                }
            }

            GUI.enabled = PrevGuiEnabled;
            
            Space();
            SectionHeader("Subscribers");
            BasicEditorGuiLayout.ActionList(TypedTarget.Subscribers);
        }
        
    }
}