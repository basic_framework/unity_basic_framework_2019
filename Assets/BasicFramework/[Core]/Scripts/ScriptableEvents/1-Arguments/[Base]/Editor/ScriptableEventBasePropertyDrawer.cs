using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    public abstract class ScriptableEventBasePropertyDrawer<T0, TScriptableEventBase> : BasicBasePropertyDrawer
        where TScriptableEventBase : ScriptableEventBase<T0>
    {
	
        // **************************************** VARIABLES ****************************************\\

        private static readonly GUIContent RaiseGuiContent = new GUIContent("Raise", "Raises the event");
        private const float BUTTON_WIDTH = 50f;
        
		
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            TempRect.width -= BUTTON_WIDTH + BUFFER_WIDTH;
	        
            EditorGUI.PropertyField(TempRect, property);

            GUI.enabled = PrevGuiEnabled && property.objectReferenceValue != null;

            TempRect.x += TempRect.width + BUFFER_WIDTH;
            TempRect.width = BUTTON_WIDTH;

            if (GUI.Button(TempRect, RaiseGuiContent) && Selection.objects.Length == 1)
            {
                var scriptableEvent = property.objectReferenceValue as TScriptableEventBase;
                if (scriptableEvent != null)
                {
                    scriptableEvent.EditorRaise();
                }
            }

            GUI.enabled = PrevGuiEnabled;
        }

    }
}