namespace BasicFramework.Core.ScriptableEvents.Utility
{
    public static class ScriptableEventsAssetMenuOrder 
    {
	
        // **************************************** VARIABLES ****************************************\\

        public const int NONE = 0;

        private const int SECTION_ONE_START 	= 11;
        public const int BOOL 					= SECTION_ONE_START;
        public const int INT 					= SECTION_ONE_START + 1;
        public const int FLOAT 					= SECTION_ONE_START + 2;
        public const int STRING 				= SECTION_ONE_START + 3;
		
        private const int SECTION_TWO_START 	= 30;
        public const int VECTOR2 				= SECTION_TWO_START;
        public const int VECTOR3 				= SECTION_TWO_START + 1;
        public const int QUATERNION 			= SECTION_TWO_START + 2;
        public const int COLOR 					= SECTION_TWO_START + 3;
        public const int ANIMATION_CURVE 		= SECTION_TWO_START + 4;
        public const int RENDER_TEXTURE 		= SECTION_TWO_START + 5;
		
        private const int SECTION_THREE_START 	= 50;
        public const int GAME_OBJECT 			= SECTION_THREE_START;
        public const int TRANSFORM 				= SECTION_THREE_START + 1;
        public const int RECT_TRANSFORM 		= SECTION_THREE_START + 2;
		
        private const int SECTION_FOUR_START 	= 80;
        public const int NORMALIZED_FLOAT 		= SECTION_FOUR_START;
        public const int INT_RANGE 				= SECTION_FOUR_START + 1;
        public const int FLOAT_RANGE 			= SECTION_FOUR_START + 2;
        public const int RANGED_INT 			= SECTION_FOUR_START + 3;
        public const int RANGED_FLOAT 			= SECTION_FOUR_START + 4;
        public const int BASIC_POSE		 		= SECTION_FOUR_START + 6;
        public const int BASIC_POSE_SINGLE 		= SECTION_FOUR_START + 7;
        public const int BASIC_TRANSFORM 		= SECTION_FOUR_START + 8;
        public const int BASIC_DATE_TIME 		= SECTION_FOUR_START + 9;
        public const int TRI_BOOL 				= SECTION_FOUR_START + 10;
		
        
        private const int SECTION_FIVE_START 	= 100;
        public const int SCRIPTABLE_EVENT 		= SECTION_FIVE_START;
        

        // ****************************************  METHODS  ****************************************\\


    }
}