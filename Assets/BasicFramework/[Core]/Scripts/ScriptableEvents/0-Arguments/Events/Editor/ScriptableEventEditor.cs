using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ScriptableEvent))][CanEditMultipleObjects]
    public class ScriptableEventEditor : BasicBaseEditor<ScriptableEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        protected override bool HasDebugSection => true;


        // ****************************************  METHODS  ****************************************\\

        protected override void DrawDebugFields()
        {
	        base.DrawDebugFields();
	        
	        var developerNote = serializedObject.FindProperty("_developerNote");

	        EditorGUILayout.PropertyField(developerNote);
	        Space();

	        GUI.enabled = PrevGuiEnabled && Application.isPlaying;
	        
	        if (GUILayout.Button("Raise"))
	        {
		        foreach (var typedTarget in TypedTargets)
		        {
			        typedTarget.EditorRaise();
		        }
	        }

	        GUI.enabled = PrevGuiEnabled;
	        
	        Space();
	        SectionHeader("Subscribers");
	        BasicEditorGuiLayout.ActionList(TypedTarget.Subscribers);
        }
        
    }
}