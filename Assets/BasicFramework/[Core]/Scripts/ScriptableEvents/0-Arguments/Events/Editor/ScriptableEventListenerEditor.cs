using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.ScriptableEvents.Editor
{
    [CustomEditor(typeof(ScriptableEventListener))][CanEditMultipleObjects]
    public class ScriptableEventListenerEditor : BasicBaseEditor<ScriptableEventListener> 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawPropertyFields()
        {
            //base.DrawPropertyFields();
	        
            var scriptableEvent = serializedObject.FindProperty("_scriptableEvent");
            var editorLogReminder = serializedObject.FindProperty("_editorLogReminder");
            var prevSciptableEvent = scriptableEvent.objectReferenceValue;
	        
            Space();
            SectionHeader("Scriptable Event Listener Properties");
	        
            EditorGUI.BeginChangeCheck();
            BasicEditorGuiLayout.PropertyFieldNotNull(scriptableEvent);
            if (EditorGUI.EndChangeCheck())
            {
                TypedTarget.EditorScriptableEventChanged(
                    prevSciptableEvent as ScriptableEvent, 
                    scriptableEvent.objectReferenceValue as ScriptableEvent);
            }
	        
            EditorGUILayout.PropertyField(editorLogReminder);
        }

        protected override void DrawEventFields()
        {
            base.DrawDebugFields();
	        
            var response = serializedObject.FindProperty("_response");

            Space();
            SectionHeader("Scriptable Event Listener Events");
	        
            EditorGUILayout.PropertyField(response);
        }
        
    }
}