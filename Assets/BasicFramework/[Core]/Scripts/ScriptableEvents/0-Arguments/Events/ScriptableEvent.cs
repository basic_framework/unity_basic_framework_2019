using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableEvents
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core/Scriptable Events/No Arg Event", 
        fileName = "event_", 
        order = ScriptableEventsAssetMenuOrder.NONE)
    ]
    public class ScriptableEvent : ScriptableObject 
    {
		
        // **************************************** VARIABLES ****************************************\\
        

        [TextArea(4, 8)]
        [SerializeField] private string _developerNote = default;

#if UNITY_EDITOR
        /// <summary>DO NOT USE! only here to stop compiler warning</summary>
        // ReSharper disable once InconsistentNaming
        protected string ___developer_note___ => Application.isEditor ? _developerNote : "";
#endif
        
        private readonly BasicEvent _basicEvent = new BasicEvent();
        public IEnumerable<Action> Subscribers => _basicEvent.Subscribers;
        
        
        // ****************************************  METHODS  ****************************************\\
        
        public void Subscribe(Action action)
        {
            _basicEvent.Subscribe(action);
        }

        public void Unsubscribe(Action action)
        {
            _basicEvent.Unsubscribe(action);
        }
		
        public void Raise()
        {
            _basicEvent.Raise();
        }

#if UNITY_EDITOR
        public void EditorRaise()
        {
            if (!Application.isEditor) return;
            _basicEvent.Raise();
        }
#endif
        
    }
    
    [Serializable]
    public class ScriptableEventUnityEvent : UnityEvent<ScriptableEvent>{}
    
    [Serializable]
    public class ScriptableEventCompareUnityEvent : CompareValueUnityEventBase<ScriptableEvent>{}
}