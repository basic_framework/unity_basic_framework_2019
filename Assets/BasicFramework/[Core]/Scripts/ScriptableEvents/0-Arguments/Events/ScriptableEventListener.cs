using BasicFramework.Core.ScriptableEvents.Utility;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableEvents
{
	//[ExecuteAlways]
    [AddComponentMenu(
        "Basic Framework/Core/Scriptable Events/Scriptable Event Listener",
        ScriptableEventsAssetMenuOrder.NONE)
    ]
    public class ScriptableEventListener : MonoBehaviour 
    {
		
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Event for which to listen")]
        [SerializeField] private ScriptableEvent _scriptableEvent = default;

        [Tooltip("Log a reminder to add responses in the editor when the event is raised")]
        [SerializeField] private bool _editorLogReminder = default;
        
        [Tooltip("Response to invoke when event is raised")]
        [SerializeField] private UnityEvent _response = default;
        
        
        public ScriptableEvent ScriptableEvent
        {
	        get => _scriptableEvent;
	        set
	        {
		        if (_scriptableEvent == value) return;
		        var prevScriptableEvent = _scriptableEvent;
		        _scriptableEvent = value;
		        ScriptableEventUpdated(prevScriptableEvent, _scriptableEvent);
	        }
        }
        
	
        // ****************************************  METHODS  ****************************************\\

        protected virtual void OnEnable()
        {
	        //if (!Application.isPlaying && _scriptableEvent == null) return;
	        _scriptableEvent.Subscribe(EventRaised);
        }

        protected virtual void OnDisable()
        {
	        //if (!Application.isPlaying && _scriptableEvent == null) return;
	        _scriptableEvent.Unsubscribe(EventRaised);
        }
        
        private void ScriptableEventUpdated(ScriptableEvent prevScriptableEvent, ScriptableEvent scriptableEvent)
        {
	        if (prevScriptableEvent != null)
	        {
		        prevScriptableEvent.Unsubscribe(EventRaised);
	        }

	        if (!isActiveAndEnabled) return;
            
	        if (scriptableEvent != null)
	        {
		        scriptableEvent.Subscribe(EventRaised);
	        }
        }

        private void EventRaised()
        {
	        if (_editorLogReminder && Application.isEditor)
	        {
		        Debug.LogWarning($"{name}/{GetType().Name} => " +
		                         $"Reminder to add response for \"{_scriptableEvent.name}\" event");
	        }
	        
	        _response.Invoke();
        }
		
        public void EditorScriptableEventChanged(ScriptableEvent eventPrevious, ScriptableEvent eventCurrent)
        {
	        if (!Application.isPlaying) return;
	        if (!Application.isEditor) return;
	        ScriptableEventUpdated(eventPrevious, eventCurrent);
        }
    }
}