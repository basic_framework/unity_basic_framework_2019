using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.ScriptableEvents
{
    //[ExecuteAlways]
    public abstract class ScriptableEventListenerBase<T0, T1, TScriptableEventBase, TUnityEvent> : MonoBehaviour
        where TScriptableEventBase : ScriptableEventBase<T0, T1>
        where TUnityEvent : UnityEvent<T0, T1>
    {

        // **************************************** VARIABLES ****************************************\\
		
        [Tooltip("Event for which to listen")]
        [SerializeField] private TScriptableEventBase _scriptableEvent = default;
        
        [Tooltip("Log a reminder to add responses in the editor when the event is raised")]
        [SerializeField] private bool _editorLogReminder = default;
        
        [Tooltip("Response to invoke when event is raised")]
        [SerializeField] private TUnityEvent _response = default;
        
        
        public TScriptableEventBase ScriptableEvent
        {
            get => _scriptableEvent;
            set
            {
                if (_scriptableEvent == value) return;
                var prevScriptableEvent = _scriptableEvent;
                _scriptableEvent = value;
                ScriptableEventUpdated(prevScriptableEvent, _scriptableEvent);
            }
        }
		
		
        // ****************************************  METHODS  ****************************************\\
		
        protected virtual void OnEnable()
        {
            //if (!Application.isPlaying && _scriptableEvent == null) return;
            _scriptableEvent.Subscribe(EventRaised);
        }

        protected virtual void OnDisable()
        {
            //if (!Application.isPlaying && _scriptableEvent == null) return;
            _scriptableEvent.Unsubscribe(EventRaised);
        }
        
        private void ScriptableEventUpdated(TScriptableEventBase prevScriptableEvent, TScriptableEventBase scriptableEvent)
        {
            if (prevScriptableEvent != null)
            {
                prevScriptableEvent.Unsubscribe(EventRaised);
            }

            if (!isActiveAndEnabled) return;
            
            if (scriptableEvent != null)
            {
                scriptableEvent.Subscribe(EventRaised);
            }
        }

        private void EventRaised(T0 arg0, T1 arg1)
        {
            if (_editorLogReminder && Application.isEditor)
            {
                Debug.LogWarning($"{name}/{GetType().Name} => " +
                                 $"Reminder to add response for \"{_scriptableEvent.name}\" event");
            }
            
            _response.Invoke(arg0, arg1);
            OnEventRaised(arg0, arg1);
        }
		
        protected virtual void OnEventRaised(T0 arg0, T1 arg1){}
        
        public void EditorScriptableEventChanged(TScriptableEventBase eventPrevious, TScriptableEventBase eventCurrent)
        {
            if (!Application.isPlaying) return;
            if (!Application.isEditor) return;
            ScriptableEventUpdated(eventPrevious, eventCurrent);
        }
        
    }
}