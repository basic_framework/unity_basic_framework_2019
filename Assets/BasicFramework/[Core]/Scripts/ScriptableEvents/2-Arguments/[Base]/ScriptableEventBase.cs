using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.ScriptableEvents
{
    public abstract class ScriptableEventBase<T0, T1> : ScriptableObject 
    {

        // **************************************** VARIABLES ****************************************\\
        

        [SerializeField] private T0 _testArg0 = default;
        [SerializeField] private T1 _testArg1 = default;
        
        [TextArea(4, 8)]
        [SerializeField] private string _developerNote = default;
        
#if UNITY_EDITOR
        /// <summary>DO NOT USE! only here to stop compiler warning</summary>
        // ReSharper disable once InconsistentNaming
        protected string ___developer_note___ => Application.isEditor ? _developerNote : "";
#endif
        
        private readonly BasicEvent<T0, T1> _basicEvent = new BasicEvent<T0, T1>();
        public IEnumerable<Action<T0, T1>> Subscribers => _basicEvent.Subscribers;
        
        
        // ****************************************  METHODS  ****************************************\\
		
        public void Subscribe(Action<T0, T1> action)
        {
            _basicEvent.Subscribe(action);
        }

        public void Unsubscribe(Action<T0, T1> action)
        {
            _basicEvent.Unsubscribe(action);
        }
		
        public void Raise(T0 arg0, T1 arg1)
        {
            _basicEvent.Raise(arg0, arg1);
        }

#if UNITY_EDITOR
        public void EditorRaise()
        {
            if (!Application.isEditor) return;
            _basicEvent.Raise(_testArg0, _testArg1);
        }
#endif
		
    }	
}