﻿namespace BasicFramework.Core.BasicMath.UnitConversion
{
	public static class SpeedConversion 
	{

		// **************************************** VARIABLES ****************************************\\
	
		
		// ************************ LINEAR ************************ \\
		
		// ************ Meter Per Hour ************ \\
		
		//public const float METER_PER_HOUR_TO_METER_PER_HOUR 		= 1f;
		public const float METER_PER_HOUR_TO_METER_PER_MINUTE		= 0.0166666667f;
		public const float METER_PER_HOUR_TO_METER_PER_SECOND		= 0.0002777778f;
		
		public const float METER_PER_HOUR_TO_KILOMETER_PER_HOUR 	= 0.001f;
		public const float METER_PER_HOUR_TO_KILOMETER_PER_MINUTE	= 0.0000166667f;
		public const float METER_PER_HOUR_TO_KILOMETER_PER_SECOND	= 2.777777777E-7f;
		
		public const float METER_PER_HOUR_TO_MILE_PER_HOUR 			= 0.0006213712f;
		public const float METER_PER_HOUR_TO_MILE_PER_MINUTE 		= 0.0000103562f;
		public const float METER_PER_HOUR_TO_MILE_PER_SECOND 		= 1.726031089E-7f;
		
		
		// ************ Meter Per Minute ************ \\
		
		public const float METER_PER_MINUTE_TO_METER_PER_HOUR 		= 60f;
		//public const float METER_PER_MINUTE_TO_METER_PER_MINUTE		= 1f;
		public const float METER_PER_MINUTE_TO_METER_PER_SECOND		= 0.0166666667f;
		
		public const float METER_PER_MINUTE_TO_KILOMETER_PER_HOUR 	= 0.06f;
		public const float METER_PER_MINUTE_TO_KILOMETER_PER_MINUTE	= 0.001f;
		public const float METER_PER_MINUTE_TO_KILOMETER_PER_SECOND	= 0.0000166667f;
		
		public const float METER_PER_MINUTE_TO_MILE_PER_HOUR 		= 0.0372822715f;
		public const float METER_PER_MINUTE_TO_MILE_PER_MINUTE 		= 0.0006213712f;
		public const float METER_PER_MINUTE_TO_MILE_PER_SECOND 		= 0.0000103562f;
		
		
		// ************ Meter Per Second ************ \\
		
		public const float METER_PER_SECOND_TO_METER_PER_HOUR 		= 3600f;
		public const float METER_PER_SECOND_TO_METER_PER_MINUTE		= 60f;
		//public const float METER_PER_SECOND_TO_METER_PER_SECOND		= 1f;
		
		public const float METER_PER_SECOND_TO_KILOMETER_PER_HOUR 	= 3.6f;
		public const float METER_PER_SECOND_TO_KILOMETER_PER_MINUTE	= 0.06f;
		public const float METER_PER_SECOND_TO_KILOMETER_PER_SECOND	= 0.001f;
		
		public const float METER_PER_SECOND_TO_MILE_PER_HOUR 		= 2.2369362921f;
		public const float METER_PER_SECOND_TO_MILE_PER_MINUTE 		= 0.0372822715f;
		public const float METER_PER_SECOND_TO_MILE_PER_SECOND 		= 0.0006213712f;
		

		// ************ Kilometre Per Hour ************ \\
		
		public const float KILOMETER_PER_HOUR_TO_METER_PER_HOUR 		= 1000f;
		public const float KILOMETER_PER_HOUR_TO_METER_PER_MINUTE		= 16.666666667f;
		public const float KILOMETER_PER_HOUR_TO_METER_PER_SECOND 		= 0.2777777778f;

		//public const float KILOMETER_PER_HOUR_TO_KILOMETER_PER_HOUR 	= 1f;
		public const float KILOMETER_PER_HOUR_TO_KILOMETER_PER_MINUTE	= 0.0166666667f;
		public const float KILOMETER_PER_HOUR_TO_KILOMETER_PER_SECOND	= 0.0002777778f;
		
		public const float KILOMETER_PER_HOUR_TO_MILE_PER_HOUR 			= 0.6213711922f;
		public const float KILOMETER_PER_HOUR_TO_MILE_PER_MINUTE 		= 0.0103561865f;
		public const float KILOMETER_PER_HOUR_TO_MILE_PER_SECOND 		= 0.0001726031f;
		
		
		// ************ Kilometre Per Minute ************ \\
		
		public const float KILOMETER_PER_MINUTE_TO_METER_PER_HOUR 		= 60000f;
		public const float KILOMETER_PER_MINUTE_TO_METER_PER_MINUTE		= 1000f;
		public const float KILOMETER_PER_MINUTE_TO_METER_PER_SECOND 	= 16.666666667f;

		public const float KILOMETER_PER_MINUTE_TO_KILOMETER_PER_HOUR 	= 60f;
		//public const float KILOMETER_PER_MINUTE_TO_KILOMETER_PER_MINUTE	= 1f;
		public const float KILOMETER_PER_MINUTE_TO_KILOMETER_PER_SECOND	= 0.0166666667f;
		
		public const float KILOMETER_PER_MINUTE_TO_MILE_PER_HOUR 		= 37.282271534f;
		public const float KILOMETER_PER_MINUTE_TO_MILE_PER_MINUTE 		= 0.6213711922f;
		public const float KILOMETER_PER_MINUTE_TO_MILE_PER_SECOND 		= 0.0103561865f;
		
		
		// ************ Kilometre Per Second ************ \\
		
		public const float KILOMETER_PER_SECOND_TO_METER_PER_HOUR 		= 3600000f;
		public const float KILOMETER_PER_SECOND_TO_METER_PER_MINUTE		= 60000f;
		public const float KILOMETER_PER_SECOND_TO_METER_PER_SECOND 	= 1000f;

		public const float KILOMETER_PER_SECOND_TO_KILOMETER_PER_HOUR 	= 3600f;
		public const float KILOMETER_PER_SECOND_TO_KILOMETER_PER_MINUTE	= 60f;
		//public const float KILOMETER_PER_SECOND_TO_KILOMETER_PER_SECOND	= 1f;
		
		public const float KILOMETER_PER_SECOND_TO_MILE_PER_HOUR 		= 2236.9362921f;
		public const float KILOMETER_PER_SECOND_TO_MILE_PER_MINUTE 		= 37.282271534f;
		public const float KILOMETER_PER_SECOND_TO_MILE_PER_SECOND 		= 0.6213711922f;
		
		
		// ************ Miles Per Hour ************ \\
		
		public const float MILE_PER_HOUR_TO_METER_PER_HOUR 			= 1609.344f;
		public const float MILE_PER_HOUR_TO_METER_PER_MINUTE		= 26.8224f;
		public const float MILE_PER_HOUR_TO_METER_PER_SECOND 		= 0.44704f;

		public const float MILE_PER_HOUR_TO_KILOMETER_PER_HOUR 		= 1.609344f;
		public const float MILE_PER_HOUR_TO_KILOMETER_PER_MINUTE	= 0.0268224f;
		public const float MILE_PER_HOUR_TO_KILOMETER_PER_SECOND	= 0.00044704f;
		
		//public const float MILE_PER_HOUR_TO_MILE_PER_HOUR 			= 1f;
		public const float MILE_PER_HOUR_TO_MILE_PER_MINUTE 		= 0.0166666667f;
		public const float MILE_PER_HOUR_TO_MILE_PER_SECOND 		= 0.0002777778f;
		
		
		// ************ Miles Per Minute ************ \\
		
		public const float MILE_PER_MINUTE_TO_METER_PER_HOUR 		= 96560.64f;
		public const float MILE_PER_MINUTE_TO_METER_PER_MINUTE		= 1609.344f;
		public const float MILE_PER_MINUTE_TO_METER_PER_SECOND 		= 26.8224f;

		public const float MILE_PER_MINUTE_TO_KILOMETER_PER_HOUR 	= 96.56064f;
		public const float MILE_PER_MINUTE_TO_KILOMETER_PER_MINUTE	= 1.609344f;
		public const float MILE_PER_MINUTE_TO_KILOMETER_PER_SECOND	= 0.0268224f;
		
		public const float MILE_PER_MINUTE_TO_MILE_PER_HOUR 		= 60f;
		//public const float MILE_PER_MINUTE_TO_MILE_PER_MINUTE 		= 1f;
		public const float MILE_PER_MINUTE_TO_MILE_PER_SECOND 		= 0.0166666667f;
		
		
		// ************ Miles Per Second ************ \\
		
		public const float MILE_PER_SECOND_TO_METER_PER_HOUR 		= 5793638.4f;
		public const float MILE_PER_SECOND_TO_METER_PER_MINUTE		= 96560.64f;
		public const float MILE_PER_SECOND_TO_METER_PER_SECOND 		= 1609.344f;

		public const float MILE_PER_SECOND_TO_KILOMETER_PER_HOUR 	= 5793.6384f;
		public const float MILE_PER_SECOND_TO_KILOMETER_PER_MINUTE	= 96.56064f;
		public const float MILE_PER_SECOND_TO_KILOMETER_PER_SECOND	= 1.609344f;
		
		public const float MILE_PER_SECOND_TO_MILE_PER_HOUR 		= 3600f;
		public const float MILE_PER_SECOND_TO_MILE_PER_MINUTE 		= 60f;
		//public const float MILE_PER_SECOND_TO_MILE_PER_SECOND 		= 1f;
		
		
		// ************************ ANGULAR ************************ \\
		
		// ************ Revolutions Per Minute ************ \\

		public const float REVOLUTIONS_PER_MINUTE_TO_DEGRESS_PER_SECOND = 6;


		// ****************************************  METHODS  ****************************************\\

	}
}