﻿using System.Text;
using UnityEngine;

namespace BasicFramework.Core.BasicMath.UnitConversion
{
	public static class TimeConversion 
	{

		// **************************************** VARIABLES ****************************************\\
	
		
		// ************ Second ************ \\
		
		public const float SECOND_TO_MILLISECOND 	= 1000f;
		public const float SECOND_TO_MICROSECOND 	= 1000000f;
		public const float SECOND_TO_NANOSECOND  	= 1000000000f;
		public const float SECOND_TO_PICOSECOND  	= 1000000000000f;
		public const float SECOND_TO_MINUTE 		= 0.016666666666667f;
		public const float SECOND_TO_HOUR 			= 0.0002777777777778f;
		public const float SECOND_TO_DAY 			= 0.0000115741f;
		public const float SECOND_TO_WEEK 			= 0.0000016534f;
		public const float SECOND_TO_MONTH 			= 3.802570537E-7f;
		public const float SECOND_TO_YEAR 			= 3.168808781E-8f;
		
		
		// ************ MilliSecond ************ \\
		
		public const float MILLISECOND_TO_SECOND 		= 0.001f;
		public const float MILLISECOND_TO_MICROSECOND 	= 1000f;
		public const float MILLISECOND_TO_NANOSECOND  	= 1000000f;
		public const float MILLISECOND_TO_PICOSECOND  	= 1000000000f;
		public const float MILLISECOND_TO_MINUTE 		= 0.0000166667f;
		public const float MILLISECOND_TO_HOUR 			= 2.777777777E-7f;
		public const float MILLISECOND_TO_DAY 			= 1.157407407E-8f;
		public const float MILLISECOND_TO_WEEK 			= 1.653439153E-9f;
		public const float MILLISECOND_TO_MONTH 		= 3.802570537E-10f;
		public const float MILLISECOND_TO_YEAR 			= 3.168808781E-11f;
		
		
		// ************ MicroSecond ************ \\
		
		public const float MICROSECOND_TO_SECOND 		= 0.000001f;
		public const float MICROSECOND_TO_MILLISECOND 	= 0.001f;
		public const float MICROSECOND_TO_NANOSECOND  	= 1000f;
		public const float MICROSECOND_TO_PICOSECOND  	= 1000000f;
		public const float MICROSECOND_TO_MINUTE 		= 1.666666666E-8f;
		public const float MICROSECOND_TO_HOUR 			= 2.777777777E-10f;
		public const float MICROSECOND_TO_DAY 			= 1.157407407E-11f;
		public const float MICROSECOND_TO_WEEK 			= 1.653439153E-12f;
		public const float MICROSECOND_TO_MONTH 		= 3.802570537E-13f;
		public const float MICROSECOND_TO_YEAR 			= 3.168808781E-14f;
		
		
		// ************ NanoSecond ************ \\
		
		public const float NANOSECOND_TO_SECOND 		= 1.0E-9f;
		public const float NANOSECOND_TO_MILLISECOND 	= 0.000001f;
		public const float NANOSECOND_TO_MICROSECOND  	= 0.001f;
		public const float NANOSECOND_TO_PICOSECOND  	= 1000f;
		public const float NANOSECOND_TO_MINUTE 		= 1.666666666E-11f;
		public const float NANOSECOND_TO_HOUR 			= 2.777777777E-13f;
		public const float NANOSECOND_TO_DAY 			= 1.157407407E-14f;
		public const float NANOSECOND_TO_WEEK 			= 1.653439153E-15f;
		public const float NANOSECOND_TO_MONTH 			= 3.802570537E-16f;
		public const float NANOSECOND_TO_YEAR 			= 3.168808781E-17f;
		
		
		// ************ PicoSecond ************ \\
		
		public const float PICOSECOND_TO_SECOND 		= 1.0E-12f;
		public const float PICOSECOND_TO_MILLISECOND 	= 1.0E-9f;
		public const float PICOSECOND_TO_MICROSECOND  	= 0.000001f;
		public const float PICOSECOND_TO_NANOSECOND  	= 0.001f;
		public const float PICOSECOND_TO_MINUTE 		= 1.666666666E-14f;
		public const float PICOSECOND_TO_HOUR 			= 2.777777777E-16f;
		public const float PICOSECOND_TO_DAY 			= 1.157407407E-17f;
		public const float PICOSECOND_TO_WEEK 			= 1.653439153E-18f;
		public const float PICOSECOND_TO_MONTH 			= 3.802570537E-19f;
		public const float PICOSECOND_TO_YEAR 			= 3.168808781E-20f;
		
		
		// ************ Minute ************ \\
		
		public const float MINUTE_TO_SECOND 		= 6.0E1f;
		public const float MINUTE_TO_MILLISECOND 	= 6.0E4f;
		public const float MINUTE_TO_MICROSECOND  	= 6.0E7f;
		public const float MINUTE_TO_NANOSECOND  	= 6.0E10f;
		public const float MINUTE_TO_PICOSECOND 	= 6.0E13f;
		public const float MINUTE_TO_HOUR 			= 0.0166666667f;
		public const float MINUTE_TO_DAY 			= 0.0006944444f;
		public const float MINUTE_TO_WEEK 			= 0.0000992063f;
		public const float MINUTE_TO_MONTH 			= 0.0000228154f;
		public const float MINUTE_TO_YEAR 			= 0.0000019013f;
		
		
		// ************ Hour ************ \\
		
		public const float HOUR_TO_SECOND 		= 3.6E3f;
		public const float HOUR_TO_MILLISECOND 	= 3.6E6f;
		public const float HOUR_TO_MICROSECOND  = 3.6E9f;
		public const float HOUR_TO_NANOSECOND  	= 3.6E12f;
		public const float HOUR_TO_PICOSECOND 	= 3.6E15f;
		public const float HOUR_TO_MINUTE 		= 60f;
		public const float HOUR_TO_DAY 			= 0.0416666667f;
		public const float HOUR_TO_WEEK 		= 0.005952381f;
		public const float HOUR_TO_MONTH 		= 0.0013689254f;
		public const float HOUR_TO_YEAR 		= 0.0001140771f;
		
		
		// ************ Day ************ \\
		
		public const float DAY_TO_SECOND 		= 8.64E4f;
		public const float DAY_TO_MILLISECOND 	= 8.64E7f;
		public const float DAY_TO_MICROSECOND  	= 8.64E10f;
		public const float DAY_TO_NANOSECOND  	= 8.64E13f;
		public const float DAY_TO_PICOSECOND 	= 8.64E16f;
		public const float DAY_TO_MINUTE 		= 1440f;
		public const float DAY_TO_HOUR 			= 24f;
		public const float DAY_TO_WEEK 			= 0.1428571429f;
		public const float DAY_TO_MONTH 		= 0.0328542094f;
		public const float DAY_TO_YEAR 			= 0.0027378508f;
		
		
		// ************ Week ************ \\
		
		public const float WEEK_TO_SECOND 		= 6.048E5f;
		public const float WEEK_TO_MILLISECOND 	= 6.048E8f;
		public const float WEEK_TO_MICROSECOND  = 6.048E11f;
		public const float WEEK_TO_NANOSECOND  	= 6.048E14f;
		public const float WEEK_TO_PICOSECOND 	= 6.048E17f;
		public const float WEEK_TO_MINUTE 		= 10080f;
		public const float WEEK_TO_HOUR 		= 168f;
		public const float WEEK_TO_DAY 			= 7f;
		public const float WEEK_TO_MONTH 		= 0.2299794661f;
		public const float WEEK_TO_YEAR 		= 0.0191649555f;
		
		
		// ************ Month ************ \\
		
		public const float MONTH_TO_SECOND 		= 2.6298E6f;
		public const float MONTH_TO_MILLISECOND = 2.6298E9f;
		public const float MONTH_TO_MICROSECOND = 2.6298E12f;
		public const float MONTH_TO_NANOSECOND  = 2.6298E15f;
		public const float MONTH_TO_PICOSECOND 	= 2.6298E18f;
		public const float MONTH_TO_MINUTE 		= 43830f;
		public const float MONTH_TO_HOUR 		= 730.5f;
		public const float MONTH_TO_DAY 		= 30.4375f;
		public const float MONTH_TO_WEEK 		= 4.3482142857f;
		public const float MONTH_TO_YEAR 		= 0.0833333333f;
		
		
		// ************ Year ************ \\
		
		public const float YEAR_TO_SECOND 		= 3.15576E7f;
		public const float YEAR_TO_MILLISECOND 	= 3.15576E10f;
		public const float YEAR_TO_MICROSECOND 	= 3.15576E13f;
		public const float YEAR_TO_NANOSECOND 	= 3.15576E16f;
		public const float YEAR_TO_PICOSECOND 	= 3.15576E19f;
		public const float YEAR_TO_MINUTE 		= 525960f;
		public const float YEAR_TO_HOUR 		= 8766f;
		public const float YEAR_TO_DAY 			= 365.25f;
		public const float YEAR_TO_WEEK 		= 52.178571429f;
		public const float YEAR_TO_MONTH 		= 12f;


		// ****************************************  METHODS  ****************************************\\
		
		public static string SecondsToDisplay(float timeSeconds)
		{
			var hour 	= Mathf.FloorToInt(timeSeconds * SECOND_TO_HOUR);
			var minute 	= Mathf.FloorToInt(timeSeconds * SECOND_TO_MINUTE % 60);
			var second 	= Mathf.FloorToInt(timeSeconds % 60);
	
			var sb = new StringBuilder();

			sb.AppendFormat(
				"{0}{1}:{2}{3}:{4}{5}", 
				hour < 10 ? "0" : "", 	hour, 
				minute < 10 ? "0" : "",	minute,
				second < 10 ? "0" : "", second
				);

			return sb.ToString();
		}  
		
	}
}