﻿namespace BasicFramework.Core.BasicMath.UnitConversion
{
	public static class TemperatureConversion 
	{

		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\
		
		
		// ************ Celsius ************ \\
		
		public static float CelsiusToFahrenheit(float celsius)
		{
			return celsius * 1.8f + 32f;
		}
		
		public static float CelsiusToKelvin(float celsius)
		{
			return celsius + 273.15f;
		}
		

		// ************ Fahrenheit ************ \\
		
		public static float FahrenheitToCelsius(float fahrenheit)
		{
			return (fahrenheit - 32) * 0.555555555555556f;
		}
		
		public static float FahrenheitToKelvin(float fahrenheit)
		{
			return (fahrenheit - 32) * 0.555555555555556f + 273.15f;
		}
		
		
		// ************ Kelvin ************ \\
		
		public static float KelvinToCelsius(float kelvin)
		{
			return kelvin - 273.15f;
		}
		
		public static float KelvinToFahrenheit(float kelvin)
		{
			return (kelvin - 273.15f) * 1.8f + 32;
		}
		
	}

}