﻿namespace BasicFramework.Core.BasicMath.UnitConversion
{
	public static class LengthConversion 
	{

		// **************************************** VARIABLES ****************************************\\
		
		
		// ************ KiloMeter ************ \\
		
		public const float KILOMETER_TO_METER = 1000f;
		public const float KILOMETER_TO_CENTIMETER = 100000f;
		public const float KILOMETER_TO_MILLIMETER = 1000000f;
		public const float KILOMETER_TO_MICROMETER = 1000000000f;
		public const float KILOMETER_TO_NANOMETER = 1000000000000f;
		public const float KILOMETER_TO_MILE = 0.6213688756f;
		public const float KILOMETER_TO_YARD = 1093.6132983f;
		public const float KILOMETER_TO_FOOT = 3280.839895f;
		public const float KILOMETER_TO_INCH = 39370.07874f;
		public const float KILOMETER_TO_LIGHTYEAR = 1.057008707E-13f;
		
		
		// ************ Meter ************ \\
		
		public const float METER_TO_KILOMETER = 0.001f;
		public const float METER_TO_CENTIMETER = 100f;
		public const float METER_TO_MILLIMETER = 1000f;
		public const float METER_TO_MICROMETER = 1000000f;
		public const float METER_TO_NANOMETER = 1000000000f;
		public const float METER_TO_MILE = 0.0006213689f;
		public const float METER_TO_YARD = 1.0936132983f;
		public const float METER_TO_FOOT = 3.280839895f;
		public const float METER_TO_INCH = 39.37007874f;
		public const float METER_TO_LIGHTYEAR = 1.057008707E-16f;

		
		// ************ CentiMeter ************ \\
		
		public const float CENTIMETER_TO_KILOMETER = 0.00001f;
		public const float CENTIMETER_TO_METER = 0.01f;
		public const float CENTIMETER_TO_MILLIMETER = 10f;
		public const float CENTIMETER_TO_MICROMETER = 10000f;
		public const float CENTIMETER_TO_NANOMETER = 10000000f;
		public const float CENTIMETER_TO_MILE = 0.0000062137f;
		public const float CENTIMETER_TO_YARD = 0.010936133f;
		public const float CENTIMETER_TO_FOOT = 0.032808399f;
		public const float CENTIMETER_TO_INCH = 0.3937007874f;
		public const float CENTIMETER_TO_LIGHTYEAR = 1.057008707E-18f;
		
		public const float CENTIMETER_TO_POINT 	= 28.346456693f;
		
		
		// ************ MilliMeter ************ \\
		
		public const float MILLIMETER_TO_KILOMETER = 0.000001f;
		public const float MILLIMETER_TO_METER = 0.001f;
		public const float MILLIMETER_TO_CENTIMETER = 0.1f;
		public const float MILLIMETER_TO_MICROMETER = 1000f;
		public const float MILLIMETER_TO_NANOMETER = 1000000f;
		public const float MILLIMETER_TO_MILE = 6.213688756E-7f;
		public const float MILLIMETER_TO_YARD = 0.0010936133f;
		public const float MILLIMETER_TO_FOOT = 0.0032808399f;
		public const float MILLIMETER_TO_INCH = 0.0393700787f;
		public const float MILLIMETER_TO_LIGHTYEAR = 1.057008707E-19f;
		
		public const float MILLIMETER_TO_POINT 	= 2.8346456693f;
		
		
		// ************ MicroMeter ************ \\
		
		public const float MICROMETER_TO_KILOMETER = 0.000000001f;
		public const float MICROMETER_TO_METER = 0.000001f;
		public const float MICROMETER_TO_CENTIMETER = 0.0001f;
		public const float MICROMETER_TO_MILLIMETER = 0.001f;
		public const float MICROMETER_TO_NANOMETER = 1000f;
		public const float MICROMETER_TO_MILE = 6.213688756E-10f;
		public const float MICROMETER_TO_YARD = 0.0000010936f;
		public const float MICROMETER_TO_FOOT = 0.0000032808f;
		public const float MICROMETER_TO_INCH = 0.0000393701f;
		public const float MICROMETER_TO_LIGHTYEAR = 1.057008707E-22f;
		
		
		// ************ NanoMeter ************ \\
		
		public const float NANOMETER_TO_KILOMETER = 0.000000000001f;
		public const float NANOMETER_TO_METER = 0.000000001f;
		public const float NANOMETER_TO_CENTIMETER = 0.0000001f;
		public const float NANOMETER_TO_MILLIMETER = 0.000001f;
		public const float NANOMETER_TO_MICROMETER = 0.001f;
		public const float NANOMETER_TO_MILE = 6.213688756E-13f;
		public const float NANOMETER_TO_YARD = 1.093613298E-9f;
		public const float NANOMETER_TO_FOOT = 3.280839895E-9f;
		public const float NANOMETER_TO_INCH = 3.937007874E-8f;
		public const float NANOMETER_TO_LIGHTYEAR = 1.057008707E-25f;
		
		
		// ************ Mile ************ \\
		
		public const float MILE_TO_KILOMETER = 1.60935f;
		public const float MILE_TO_METER = 1609.35f;
		public const float MILE_TO_CENTIMETER = 160935f;
		public const float MILE_TO_MILLIMETER = 1609350f;
		public const float MILE_TO_MICROMETER = 1609350000f;
		public const float MILE_TO_NANOMETER = 1609350000000f;
		public const float MILE_TO_YARD = 1760.0065617f;
		public const float MILE_TO_FOOT = 5280.019685f;
		public const float MILE_TO_INCH = 63360.23622f;
		public const float MILE_TO_LIGHTYEAR = 1.701096963E-13f;
		
		
		// ************ Yard ************ \\
		
		public const float YARD_TO_METER = 0.9144f;
		public const float YARD_TO_KILOMETER = 0.0009144f;
		public const float YARD_TO_CENTIMETER = 91.44f;
		public const float YARD_TO_MILLIMETER = 914.4f;
		public const float YARD_TO_MICROMETER = 914400f;
		public const float YARD_TO_NANOMETER = 914400000f;
		public const float YARD_TO_MILE = 0.0005681797f;
		public const float YARD_TO_FOOT = 3f;
		public const float YARD_TO_INCH = 36f;
		public const float YARD_TO_LIGHTYEAR = 9.665287622E-17f;
		
		
		// ************ Foot ************ \\
		
		public const float FOOT_TO_KILOMETER = 0.0003048f;
		public const float FOOT_TO_METER = 0.3048f;
		public const float FOOT_TO_CENTIMETER = 30.48f;
		public const float FOOT_TO_MILLIMETER = 304.8f;
		public const float FOOT_TO_MICROMETER = 304800f;
		public const float FOOT_TO_NANOMETER = 304800000f;
		public const float FOOT_TO_MILE = 0.0001893932f;
		public const float FOOT_TO_YARD = 0.3333333333f;
		public const float FOOT_TO_INCH = 12f;
		public const float FOOT_TO_LIGHTYEAR = 3.22176254E-17f;
		
		public const float FOOT_TO_POINT = 864;
		

		// ************ Inch ************ \\
		
		public const float INCH_TO_KILOMETER = 0.0000254f;
		public const float INCH_TO_METER = 0.0254f;
		public const float INCH_TO_CENTIMETER = 2.54f;
		public const float INCH_TO_MILLIMETER = 25.4f;
		public const float INCH_TO_MICROMETER = 25400f;
		public const float INCH_TO_NANOMETER = 25400000f;
		public const float INCH_TO_MILE = 0.0000157828f;
		public const float INCH_TO_YARD = 0.0277777778f;
		public const float INCH_TO_FOOT = 0.0833333333f;
		public const float INCH_TO_LIGHTYEAR = 2.684802117E-18f;
		
		public const float INCH_TO_POINT = 72;
		
		// ************ LightYear ************ \\
		
		public const float LIGHTYEAR_TO_KILOMETER = 9460660000000f;
		public const float LIGHTYEAR_TO_METER = 9460660000000000f;
		public const float LIGHTYEAR_TO_CENTIMETER = 946066000000000000f;
		public const float LIGHTYEAR_TO_MILLIMETER = 9460660000000000000f;
		public const float LIGHTYEAR_TO_MICROMETER = 9.46066E+21f;
		public const float LIGHTYEAR_TO_NANOMETER = 9.460659999E+24f;
		public const float LIGHTYEAR_TO_MILE = 5878559666946f;
		public const float LIGHTYEAR_TO_YARD = 10346303587051618f;
		public const float LIGHTYEAR_TO_FOOT = 31038910761154856f;
		public const float LIGHTYEAR_TO_INCH = 372466929133858300f;
		
		
		// ************ Point ************ \\
		
		public const float POINT_TO_CENTIMETER 	= 0.0352777778f;
		public const float POINT_TO_MILLIMETER 	= 0.3527777778f;
		
		public const float POINT_TO_FOOT = 0.0011574074f;
		public const float POINT_TO_INCH = 0.0138888889f;
		
		
		// ****************************************  METHODS  ****************************************\\
		
	}

}