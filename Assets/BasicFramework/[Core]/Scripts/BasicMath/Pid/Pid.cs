using System;
using UnityEngine;

namespace BasicFramework.Core.BasicMath
{
    [Serializable]
    public class Pid : ICloneable
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private float _setPoint = default;
		
        [SerializeField] private float _kP = default;
        [SerializeField] private float _kI = default;
        [SerializeField] private float _kD = default;
		
        private float _lastUpdateTime;

        public float SetPoint
        {
            get => _setPoint;
            set => _setPoint = value;
        }

        public float KP
        {
            get => _kP;
            set => _kP = Mathf.Clamp(value, 0, float.MaxValue);
        }

        public float KI
        {
            get => _kI;
            set => _kI = Mathf.Clamp(value, 0, float.MaxValue);
        }

        public float KD
        {
            get => _kD;
            set => _kD = Mathf.Clamp(value, 0, float.MaxValue);
        }

        private float _integral;
        private float _lastError;
        private float _controlValue;

        public float ControlValue => _controlValue;


        // ****************************************  METHODS  ****************************************\\
		
        public Pid(float kP, float kI, float kD)
        {
            _kP = kP;
            _kI = kI;
            _kD = kD;
        }
		
        public void Reset()
        {
            _lastUpdateTime = Time.time;
            _integral = 0;
            _lastError = 0;
        }

        public float Update(float currentValue)
        {
            return Update(currentValue, Time.time - _lastUpdateTime);
        }

        public float Update(float currentValue, float timeInterval)
        {
            if (timeInterval <= 0) return _controlValue;
			
            var delta = _setPoint - currentValue;
            _integral += delta * timeInterval;

            var derivative = (delta - _lastError) / timeInterval;
            _lastError = delta;

            _controlValue = delta * _kP + _integral * _kI + derivative * _kD;
            return _controlValue;
        }

        public object Clone()
        {
            var clone = new Pid(_kP, _kI, _kD);

            clone._setPoint = this._setPoint;

            return clone;
        }
        
    }
}