using System;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicMath.Editor
{
	[CustomPropertyDrawer(typeof(Pid))]
	public class PidPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private GUIContent[] _pidModes = new GUIContent[]
		{
			new GUIContent("P"),
			new GUIContent("PI"),
			new GUIContent("PD"),
			new GUIContent("PID")
		};
		
		private const float CONSTANTS_LABEL_WIDTH = 14f;

		private float _prevKi, _prevKd;
		
		
		// ****************************************  METHODS  ****************************************\\


		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			
			TempRect = new Rect(position);

			TempRect.height = SingleLineHeight;
			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			TempRect.x += INDENT;
			TempRect.width -= INDENT;
			
			if (!property.isExpanded)
			{
				return;
			}
			
			var kp = property.FindPropertyRelative("_kP");
			var ki = property.FindPropertyRelative("_kI");
			var kd = property.FindPropertyRelative("_kD");
			
			DrawPidModeField(TempRect, ki, kd);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;


			TempRect = EditorGUI.PrefixLabel(TempRect, GetGuiContent("Gain"));

			var constantFieldWidth = (TempRect.width - BUFFER_WIDTH * 2 - CONSTANTS_LABEL_WIDTH * 3) / 3;

			TempRect.width = CONSTANTS_LABEL_WIDTH;
			EditorGUI.LabelField(TempRect, "P");
			TempRect.x += TempRect.width;
			
			TempRect.width = constantFieldWidth;
			EditorGUI.PropertyField(TempRect, kp, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			
			TempRect.width = CONSTANTS_LABEL_WIDTH;
			EditorGUI.LabelField(TempRect, "I");
			TempRect.x += TempRect.width;
			
			TempRect.width = constantFieldWidth;
			EditorGUI.PropertyField(TempRect, ki, GUIContent.none);
			TempRect.x += TempRect.width + BUFFER_WIDTH;
			
			TempRect.width = CONSTANTS_LABEL_WIDTH;
			EditorGUI.LabelField(TempRect, "D");
			TempRect.x += TempRect.width;
			
			TempRect.width = constantFieldWidth;
			EditorGUI.PropertyField(TempRect, kd, GUIContent.none);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			TempRect.x = position.x + INDENT;
			TempRect.width = position.width - INDENT;

			kp.floatValue = Mathf.Clamp(kp.floatValue, 0, float.MaxValue);
			ki.floatValue = Mathf.Clamp(ki.floatValue, 0, float.MaxValue);
			kd.floatValue = Mathf.Clamp(kd.floatValue, 0, float.MaxValue);
			
			
			var setPoint = property.FindPropertyRelative("_setPoint");

			EditorGUI.PropertyField(TempRect, setPoint);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
		}

		private void DrawPidModeField(Rect rect, SerializedProperty kiProp, SerializedProperty kdProp)
		{

			var kiZero = Math.Abs(kiProp.floatValue) < Mathf.Epsilon;
			var kdZero = Math.Abs(kdProp.floatValue) < Mathf.Epsilon;

			var selectedIndex = 4;
			
			if (kiZero && kdZero)
			{
				selectedIndex = 0;
			}
			else if (!kiZero && kdZero)
			{
				selectedIndex = 1;
			}
			else if (kiZero)
			{
				selectedIndex = 2;
			}
			else
			{
				selectedIndex = 3;
			}


			
			var newIndex = EditorGUI.Popup(rect, GetGuiContent("PID Mode"), selectedIndex, _pidModes);

			if (newIndex == selectedIndex) return;

			switch (newIndex)
			{
				case 0:
					_prevKi = Math.Abs(_prevKi) > Mathf.Epsilon ? _prevKi : kiProp.floatValue;
					_prevKd = Math.Abs(_prevKd) > Mathf.Epsilon ? _prevKd : kdProp.floatValue;
					kiProp.floatValue = 0;
					kdProp.floatValue = 0;
					break;
				case 1:
					if(kiZero) kiProp.floatValue = Math.Abs(_prevKi) > Mathf.Epsilon ? _prevKi : 1;
					_prevKd = Math.Abs(_prevKd) > Mathf.Epsilon ? _prevKd : kdProp.floatValue;
					kdProp.floatValue = 0;
					break;
				case 2:
					_prevKi = Math.Abs(_prevKi) > Mathf.Epsilon ? _prevKi : kiProp.floatValue;
					kiProp.floatValue = 0;
					if(kdZero) kdProp.floatValue = Math.Abs(_prevKd) > Mathf.Epsilon ? _prevKd : 1;
					break;
				case 3:
					if(kiZero) kiProp.floatValue = Math.Abs(_prevKi) > Mathf.Epsilon ? _prevKi : 1;
					if(kdZero) kdProp.floatValue = Math.Abs(_prevKd) > Mathf.Epsilon ? _prevKd : 1;
					break;
			}
		}
		
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded)
			{
				return height;
			}

			height += BUFFER_HEIGHT + SingleLineHeight;
			height += BUFFER_HEIGHT + SingleLineHeight;
			height += BUFFER_HEIGHT + SingleLineHeight;

			return height;
		}
		
	}
}