﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicMath
{
	
	
	public enum DataIntervalModes
	{
		Continuous,
		Discrete
	}
	
	[Serializable]
	public class DataIntervalModesUnityEvent : UnityEvent<DataIntervalModes>{}

	[Serializable]
	public class DataIntervalModesCompareValueUnityEvent : CompareValueUnityEventBase<DataIntervalModes>{}

	
	
//	public enum NumberCompareConditions
//	{
//		EqualTo,
//		NotEqualTo,
//		LessThan,
//		LessThanOrEqualTo,
//		GreaterThan,
//		GreaterThanOrEqualTo
//	}
//	
//	public static class MathEnumsUtility
//	{
//		public static bool NumberCompare(NumberCompareConditions numberCompareCondition, int left, int right)
//		{
//			switch (numberCompareCondition)
//			{
//				case NumberCompareConditions.EqualTo:				return left == right;
//
//				case NumberCompareConditions.NotEqualTo:			return left != right;
//				
//				case NumberCompareConditions.LessThan: 				return left < right;
//
//				case NumberCompareConditions.LessThanOrEqualTo:		return left <= right;
//
//				case NumberCompareConditions.GreaterThan:			return left > right;
//		    
//				case NumberCompareConditions.GreaterThanOrEqualTo:	return left >= right;
//
//				default:
//					throw new ArgumentOutOfRangeException();
//			}
//		}
//		
//		public static bool NumberCompare(NumberCompareConditions numberCompareCondition, float left, float right)
//		{
//			switch (numberCompareCondition)
//			{
//				case NumberCompareConditions.EqualTo:				return Math.Abs(left - right) < Mathf.Epsilon;
//
//				case NumberCompareConditions.NotEqualTo:			return Math.Abs(left - right) > Mathf.Epsilon;
//				
//				case NumberCompareConditions.LessThan: 				return left < right;
//
//				case NumberCompareConditions.LessThanOrEqualTo:		return left <= right;
//
//				case NumberCompareConditions.GreaterThan:			return left > right;
//		    
//				case NumberCompareConditions.GreaterThanOrEqualTo:	return left >= right;
//
//				default:
//					throw new ArgumentOutOfRangeException();
//			}
//		}
//	}
	
}