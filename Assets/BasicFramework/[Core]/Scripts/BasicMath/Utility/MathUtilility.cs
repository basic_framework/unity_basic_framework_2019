﻿using System;
using UnityEngine;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using Random = UnityEngine.Random;

namespace BasicFramework.Core.BasicMath.Utility
{

	// TODO: Split into seperate scripts
	public static class MathUtility
	{
		// Quaternion identity direction (in unity) is (0,0,1), i.e. forward in the z-axis or Vector3.forward

		// Quaternions can be used to represent both orientations and rotations
		// i.e. one quaternion may store the orientation of an object, whilst the other stores the rotation you wish to apply
		// unity uses transform.rotation to store an objects orientation
		// In this class when an orientation is required the variable will be labelled orienation, when a rotation is required the variable will be labelled rotation

		// NOTE: Quaternions can represent an orientation and a rotation. To rotate an orientation you multiply two quaternions. The order is important.
		//		 The internet says to put the base first, however unity seems to do things the opposite way around. i.e. newOrientation = currentOrientation * rotation.

		// NOTE: Some of these functions will be containers for inbuilt unity functions, this is done to supply more detail about what unity function actually does.
		//		 Once familiar with the applications it would be more efficient to call the unity functions directly in code

		// If the quaternion represents an orientation the parameter is labelled orientation, if it represents a rotation the parameter is labelled rotation, if it doesn't matter it is labelled quat
		
		
		// **************************************** 3D ****************************************\\



		/// <summary>
		/// Converts the given quaternion orientation into a direction vector. (i.e. the quaternions forward axis)
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="orientation">Orientation.</param>
		public static Vector3 Quaternion2Direction(Quaternion orientation){
			return orientation * Vector3.forward;
		}

		/// <summary>
		/// <para> Returns a vector whose direction is the localDirection of the orientation.</para>
		/// <para> i.e. if you ask for localDirection = Vector3.up (0,1,0), you will receive the orientation's up direction. </para>
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="orientation">Orientation.</param>
		/// <param name="localDirection">Orientation local direction.</param>
		public static Vector3 Quaternion2Direction(Quaternion orientation, Vector3 localDirection){
			return orientation * localDirection;		
		}

		/// <summary>
		/// <para>Converts a direction into a Quaternion orientation.</para>
		/// <para>The quaternions up direction (i.e. rotation about forward axis) will be aligned with the component of world up normal to direction.</para>
		/// <para>Note: direction does not have to be normalized.</para>
		/// </summary>
		/// <returns>Quaternion Orientation with Forward as Direction.</returns>
		/// <param name="direction">Quaternion Orientation Forward Direction.</param>
		public static Quaternion Direction2Quaternion(Vector3 direction){
			return Quaternion.LookRotation(direction);
		}

		/// <summary>
		/// <para>Converts a direction into a Quaternion orientation.</para>
		/// <para>The quaternions up direction (i.e. rotation about forward axis) will be aligned with the component of upDirection normal to direction.</para>
		/// <para>Note: direction and upDirection does not have to be normalized.</para>
		/// </summary>
		/// <returns>Quaternion Orientation with Forward as Direction.</returns>
		/// <param name="direction">Quaternion Orientation Forward Direction.</param>
		/// <param name="upDirection">Quaternion Orientations Up Direction (i.e. rotation about forward axis).</param>
		public static Quaternion Direction2Quaternion(Vector3 direction, Vector3 upDirection){
			return Quaternion.LookRotation(direction, upDirection);
		}

		/// <summary>
		/// Rotates a quatrnion (orientation) by another quaternion (rotation)
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="orientation">Orientation.</param>
		/// <param name="rotation">Rotation.</param>
		public static Quaternion RotateQuaternion(Quaternion orientation, Quaternion rotation){
			//return orientation * rotation;		// newOrientation = currentOrientation * rotation. (Generally it is the other way around)
			return rotation * orientation;			// Updated in 2017.4.25f
		}

		/// <summary>
		/// Rotates a quaternion (orientation) by a eulerAngle rotation.
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="orientation">Orientation.</param>
		/// <param name="eulerAngle">Euler angle.</param>
		public static Quaternion RotateQuaternion(Quaternion orientation, Vector3 eulerAngle){
			return RotateQuaternion(orientation, Quaternion.Euler(eulerAngle));
		}

		/// <summary>
		/// Rotates a quaternion (orientation) by an angle around the axisOfRotation.
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="orientation">Orientation.</param>
		/// <param name="angle">Angle.</param>
		/// <param name="axisOfRotation">Axis of rotation.</param>
		public static Quaternion RotateQuaternion(Quaternion orientation, float angle, Vector3 axisOfRotation){
			return RotateQuaternion(orientation, Quaternion.AngleAxis(angle, axisOfRotation));
		}

		/// <summary>
		/// Create a quaternion rotation angle around the axis of rotation. 
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="angle">Angle.</param>
		/// <param name="axisOfRotation">Axis of rotation.</param>
		public static Quaternion CreateQuaternionRotation(float angle, Vector3 axisOfRotation){
			return Quaternion.AngleAxis(angle, axisOfRotation);
		}

		
		/// <summary>
		/// Rotates the world rotation by the local rotation.
		/// Effectively creates a new world rotation from a local rotation
		/// </summary>
		/// <param name="referenceWorldRotation">The world reference rotation, usually the transform rotation</param>
		/// <param name="localRotation">The local based rotation</param>
		/// <returns></returns>
		public static Quaternion TransformQuaternion(Quaternion referenceWorldRotation, Quaternion localRotation)
		{
			return referenceWorldRotation * localRotation;
		}
		
		/// <summary>
		/// Gets the rotation needed to rotate from referenceWorldRotation to world rotation.
		/// Effectively creates the local rotation equivalent of world rotation in referenceWorldRotation space 
		/// </summary>
		/// <param name="referenceWorldRotation">The world reference rotation, usually the transform rotation</param>
		/// <param name="worldRotation">The world based rotation</param>
		/// <returns></returns>
		public static Quaternion InverseTransformQuaternion(Quaternion referenceWorldRotation, Quaternion worldRotation)
		{
			return Quaternion.Inverse(referenceWorldRotation) * worldRotation;
		}


		/// <summary>
		/// Rotate a direction by the rotation.
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="rotation">Rotation.</param>
		public static Vector3 RotateDirection(Vector3 direction, Quaternion rotation){
			return rotation * direction;		// Rotation (Quaternion) * Direction (V3)
		}

		/// <summary>
		/// Rotates a direction around world axis using eulerAngles
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="eulerAngles">Euler angles.</param>
		public static Vector3 RotateDirection(Vector3 direction, Vector3 eulerAngles){
			return RotateDirection(direction, Quaternion.Euler(eulerAngles));
		}

		/// <summary>
		/// Rotates a direction by angle around the axisOfRotation
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="angle">Angle.</param>
		/// <param name="axisOfRotation">Axis of rotation.</param>
		public static Vector3 RotateDirection(Vector3 direction, float angle, Vector3 axisOfRotation){
			return RotateDirection(direction, Quaternion.AngleAxis(angle, axisOfRotation));
		}


		

		/// <summary>
		/// Transform a point from local space to world space
		/// </summary>
		/// <param name="worldPosition">The World Position of the transform the point is local to</param>
		/// <param name="worldRotation">The World Rotation of the transform the point is local to</param>
		/// <param name="lossyScale">The World Scale of the transform the point is local to</param>
		/// <param name="localPoint">The point in local space</param>
		/// <returns></returns>
		public static Vector3 TransformPoint(Vector3 worldPosition, Quaternion worldRotation, Vector3 lossyScale,
			Vector3 localPoint)
		{
			return TransformPoint(Matrix4x4.TRS(worldPosition, worldRotation, lossyScale), localPoint);
		}
		
//		public static Vector3 TransformPoint(BasicTransform basicTransform, Vector3 localPoint)
//		{
//			return TransformPoint(basicTransform.MatrixTRS, localPoint);
//		}
		
		public static Vector3 TransformPoint(Matrix4x4 transformMatrix, Vector3 localPoint)
		{
			return transformMatrix.MultiplyPoint3x4(localPoint);
		}

		
		/// <summary>
		/// Transform a point from local space to local space
		/// </summary>
		/// <param name="worldPosition">The World Position of the transform you want the point to be local to</param>
		/// <param name="worldRotation">The World Rotation of the transform you want the point to be local to</param>
		/// <param name="lossyScale">The World Scale of the transform you want the point to be local to</param>
		/// <param name="worldPoint">The point in world space</param>
		/// <returns></returns>
		public static Vector3 InverseTransformPoint(Vector3 worldPosition, Quaternion worldRotation, Vector3 lossyScale,
			Vector3 worldPoint)
		{
			return InverseTransformPoint(Matrix4x4.TRS(worldPosition, worldRotation, lossyScale), worldPoint);
		}
		
//		public static Vector3 InverseTransformPoint(BasicTransform basicTransform, Vector3 worldPoint)
//		{
//			return InverseTransformPoint(basicTransform.MatrixTRS, worldPoint);
//		}

		public static Vector3 InverseTransformPoint(Matrix4x4 transformMatrix, Vector3 worldPoint)
		{
			return transformMatrix.inverse.MultiplyPoint3x4(worldPoint);
		}
		
		
		
		/// <summary>
		/// Gets the axis from vector.
		/// <para> i.e. if you pass in an axis of (0,1,0) the component of vector in the y axis will be returned.</para>
		/// </summary>
		/// <returns>The axis from vector.</returns>
		/// <param name="vector">Vector.</param>
		/// <param name="axis">Axis.</param>
		public static Vector3 GetAxisFromVector(Vector3 vector, Vector3 axis){
			axis.Normalize();
			return MagOfVectorInAxis(vector, axis) * axis;
		}

		/// <summary>
		/// Zeroes the component of vector in the given axis.
		/// <para> i.e. if you pass in an axis of (0,1,0) the component of vector in the y axis will be removed.</para>
		/// </summary>
		/// <returns>The axis from vector.</returns>
		/// <param name="vector">Vector.</param>
		/// <param name="axis">Axis.</param>
		public static Vector3 RemoveAxisFromVector(Vector3 vector, Vector3 axis){
			axis.Normalize();
			return vector - MagOfVectorInAxis(vector, axis) * axis;
		}

		/// <summary>
		/// Returns the magnitude that vector is in the given axis.
		/// Useful for removing an axis from a vector or determining a magnitude to apply in the given axis.
		/// </summary>
		/// <returns>The of vector in axis.</returns>
		public static float MagOfVectorInAxis(Vector3 vector, Vector3 axis){
			return Vector3.Dot(vector, axis.normalized);
		}

		// Quaternion rotation representing change between two quaternion, i.e. delta quaternion
		// Angle representing change between two quaternion: a = start, b = final
		// Quaternion delta = Quaternion.Inverse(a) * b;	// **** For earlier version of Unity it is backwards so is instead, delta = Quaternion.Inverse(b) * a
		/// <summary>
		/// <para>Returns a quaternion that represents the change in rotation from initial to final.</para>
		/// <para>i.e. the rotation you need to apply to initial to end up at final</para>
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="initialOrientation">Initial orientation.</param>
		/// <param name="finalOrientation">Final orientation.</param>
		public static Quaternion DeltaQuaternion(Quaternion initialOrientation, Quaternion finalOrientation){
			return Quaternion.Inverse(initialOrientation) * finalOrientation;		 
		}

		/// <summary>
		/// <para>Returns direction vector normal to a and b.</para>
		/// <para>The direction of the normal vector can be determined using "left hand rule", where a = Thumb, b = pointer, result = Middle Finger.</para>
		/// </summary>
		/// <returns>The to vectors.</returns>
		/// <param name="a">Vector a.</param>
		/// <param name="b">Vector b.</param>
		public static Vector3 NormalToVectors(Vector3 a, Vector3 b){
			return Vector3.Cross(a, b).normalized;		// see unity api Vector3.Cross
		}

		/// <summary>
		/// <para>Returns the signed angle between two vectors in degrees (between -180 and 180). The angle is relative to vector a using Left Hand Rotation.</para>
		/// <para>Note: This assumes you are using (0,1,0) or Vector3.up as up direction. If not specify an up direction</para>
		/// </summary>
		/// <returns>The signed angle between vectors.</returns>
		/// <param name="a">Vector a.</param>
		/// <param name="b">Vector b.</param>
		public static float AngleBetweenVectorsSigned(Vector3 a, Vector3 b){
			return AngleBetweenVectorsSigned(a, b, Vector3.up);	// Default is up
		}

		/// <summary>
		/// <para>Returns the signed angle between two vectors in degrees (between -180 and 180). The angle is relative to vector a using Left Hand Rotation.</para>
		/// </summary>
		/// <returns>The signed angle between vectors.</returns>
		/// <param name="a">Vector a.</param>
		/// <param name="b">Vector b.</param>
		/// <param name="gameWorldUp">up direction.</param>
		public static float AngleBetweenVectorsSigned(Vector3 a, Vector3 b, Vector3 upDirection){
			float angle 	= Vector3.Angle(a, b);		// Get the unsigned angle between vectors
			Vector3 normal 	= Vector3.Cross(a, b);	// Get the normal between the two vectors
			float mag 		= Vector3.Dot(normal, upDirection.normalized);	// Determine how much of the normal is in the up axis

			return angle * Mathf.Sign(mag);			// Multiply by sign of mag in normal to get the signed angle
			//if(mag < 0){return angle * -1;}		// If normal in up less than zero, sign is negative
			//else{return angle;}					// If normal in up
		}

		//public static Vector3 RemovePlaneFromVector(Vector3 vector, Vector3 dirInPlane){
		// TODO: NEEDS TESTING, should be alright though
		//	Vector3 dirInPlane2 = Vector3.Cross(vector, dirInPlane);	// possibly need to use normalized values here
		//	Vector3 result = RemoveAxisFromVector(vector, dirInPlane);
		//	result = RemoveAxisFromVector(vector, dirInPlane2);
		//	return result;
		//}

		public static Vector3 GetVectorInPlane(Vector3 vector, Vector3 planeDir1, Vector3 planeDir2){
			// TODO: Needs testing
			return GetAxisFromVector(vector, planeDir1) + GetAxisFromVector(vector, planeDir2);
		}

		
		//Get the intersection between a line and a plane. 
		//If the line and plane are not parallel, the function outputs true, otherwise false.
		public static bool LinePlaneIntersection(out Vector3 pointOfIntersection, Vector3 pointOnLine, Vector3 lineDirection, Vector3 planeNormal, Vector3 pointOnPlane)
		{
			pointOfIntersection = Vector3.zero;
 
			//calculate the distance between the linePoint and the line-plane intersection point
			var dotNumerator = Vector3.Dot(pointOnPlane - pointOnLine, planeNormal);
			var dotDenominator = Vector3.Dot(lineDirection, planeNormal);
			
			// Check if line and plane are parrallel
			if (!(Math.Abs(dotDenominator) > Mathf.Epsilon)) return false;
			
			//line and plane are not parallel
			var length = dotNumerator / dotDenominator;
 
			//create a vector from the linePoint to the intersection point
			//Vector3 vector = SetVectorLength(lineVec, length);
			var vector = lineDirection.normalized * length;
 
			//get the coordinates of the line-plane intersection point
			pointOfIntersection = pointOnLine + vector;	
 
			return true;
		}

		
		
		
		
		
		// **************************************** 2D ****************************************\\
		/// <summary>
		/// Gets the axis from vector.
		/// <para> i.e. if you pass in an axis of (0,1) the component of vector in the y axis will be returned.</para>
		/// </summary>
		/// <returns>The axis from vector.</returns>
		/// <param name="vector">Vector.</param>
		/// <param name="axis">Axis.</param>
		public static Vector2 GetAxisFromVector(Vector2 vector, Vector2 axis){
			axis.Normalize();
			return MagOfVectorInAxis(vector, axis) * axis;
		}

		/// <summary>
		/// Zeroes the component of vector in the given axis.
		/// <para> i.e. if you pass in an axis of (0,1) the component of vector in the y axis will be removed.</para>
		/// </summary>
		/// <returns>The axis from vector.</returns>
		/// <param name="vector">Vector.</param>
		/// <param name="axis">Axis.</param>
		public static Vector2 RemoveAxisFromVector(Vector2 vector, Vector2 axis){
			axis.Normalize();
			return vector - MagOfVectorInAxis(vector, axis) * axis;
		}

		/// <summary>
		/// Returns the magnitude that vector is in the given axis.
		/// Useful for removing an axis from a vector or determining a magnitude to apply in the given axis.
		/// </summary>
		/// <returns>The of vector in axis.</returns>
		/// <param name="vector">Vector.</param>
		/// <param name="axis">Axis.</param>
		public static float MagOfVectorInAxis(Vector2 vector, Vector2 axis){
			return Vector2.Dot(vector, axis.normalized);
		}

		/// <summary>
		/// Returns the direction rotate angle about Z. The length of the vector is maintained
		/// </summary>
		/// <returns>The direction.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="angle">Angle.</param>
		public static Vector2 RotateDirection(Vector2 direction, float angle){
			return (Vector2) RotateDirection(direction, angle, Vector3.forward);
		}

		public static float Direction2RotationAboutZ(Vector2 direction){
			return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; 
		}

		public static Quaternion Direction2Quaternion(Vector2 direction){
			return Direction2Quaternion((Vector3) direction);
		}

		public static Vector2 V2multV3(Vector2 v2, Vector3 v3){return new Vector2(v2.x * v3.x, v2.y * v3.y);}
		public static Vector2 V2divV3 (Vector2 v2, Vector3 v3){return new Vector2(v2.x / v3.x, v2.y / v3.y);}

		
		
		
		
		// **************************************** Visual Formulas ****************************************\\
		
		/// <summary>Calculates the Visual Angle (in degrees) given a Distance and Object Size.</summary>
		/// <returns>The Visual Angle in degrees.</returns>
		/// <param name="dist">The distance from visual point to object.</param>
		/// <param name="size">The size of the object in the desired plane.</param>
		public static float VisualAngle(float dist, float objectSize)			 {return 2 * Mathf.Atan2(objectSize, 2 * dist) * Mathf.Rad2Deg;}	// Check using correct tan formula

		/// <summary>Calculates the Object Size for a given Visual Angle and Distance</summary>
		/// <returns>The object size.</returns>
		/// <param name="visualAngle">The Visual Angle in degrees.</param>
		/// <param name="dist">The distance from visual point to object.</param>
		public static float VisualObjectSize(float visualAngle, float dist)	 {return 2 * dist * Mathf.Tan(visualAngle / 2 * Mathf.Deg2Rad);}

		/// <summary>Calculates the Distance for a given Visual Angle and Object Size.</summary>
		/// <returns>The distance from visual source to object.</returns>
		/// <param name="visualAngle">The Visual Angle in degrees.</param>
		/// <param name="size">The size of the object in the desired plane.</param>
		public static float VisualDistance(float visualAngle, float objectSize){return objectSize / (2 * Mathf.Tan(visualAngle / 2 * Mathf.Deg2Rad));}

		
		
		
		
		// **************************************** Arc Formulas ****************************************\\
		
		/// <summary>Calculates the Arc Length given an Arc Angle and Radius.</summary>
		/// <returns>The Arc Length.</returns>
		/// <param name="arcAngle">The Arc Angle in degrees.</param>
		/// <param name="arcRadius">The Arc Radius.</param>
		public static float ArcLength(float arcAngle, float arcRadius){return 2 * Mathf.PI * arcRadius * (arcAngle / 360);}

		/// <summary>Calculates the Arc Angle (in degrees) given an Arc Length and Radius</summary>	/// </summary>
		/// <returns>The Arc Angle.</returns>
		/// <param name="arcRadius">The Arc Radius.</param>
		/// <param name="arcLength">The Arc Length.</param>
		public static float ArcAngle(float arcRadius, float arcLength){return (360 * arcLength) / (2 * Mathf.PI * arcRadius);}

		/// <summary>Calculates the Arc Radius given an Arc Angle and Arc Length.</summary>
		/// <returns>The Arc Radius.</returns>
		/// <param name="arcAngle">The Arc Angle in degrees.</param>
		/// <param name="arcLength">The Arc Length.</param>
		public static float ArcRadius(float arcAngle, float arcLength){return (360 * arcLength) / (2 * Mathf.PI * arcAngle);}

		public static float ArcChordLength(float arcAngle, float arcRadius){return 2 * arcRadius * Mathf.Sin(arcAngle / 2 * Mathf.Deg2Rad);}

		

		
		
		// **************************************** Ratio Formulas ****************************************\\
		/// <summary>Calculates the Unit Value given the components ratio values and the unit value of a known component.</summary>
		/// <returns>The required components unit value.</returns>
		/// <param name="ratioValueDesired">The Ratio Value for the desired component.</param>
		/// <param name="ratioValueKnown">The Ratio Value of the known component.</param>
		/// <param name="unitValueKnown">The Unit Value of the known component.</param>
		public static float RatioUnitValueGivenOtherValue(float ratioValueDesired, float ratioValueKnown, float unitValueKnown){return (unitValueKnown / ratioValueKnown) * ratioValueDesired;}

		/// <summary>Calculates the Unit Value given the Total Unit Value and the components Ratio Values.</summary>
		/// <returns>The required components unit value.</returns>
		/// <param name="ratioValueDesired">The Ratio Value for the Desired Component.</param>
		/// <param name="ratioValueOther">The Ratio Value for the Other Component.</param>
		/// <param name="unitValueTotal">The Total Unit Value.</param>
		public static float RatioUnitValueGivenTotalValue(float ratioValueDesired, float ratioValueOther, float unitValueTotal){return (unitValueTotal * ratioValueDesired) / (ratioValueDesired + ratioValueOther);}

		/// <summary>Calculates the Total Unit Value given a ratio and a know unit value of that ratio.</summary>
		/// <returns>The Total Unit Value.</returns>
		/// <param name="ratioValueKnown">The Ratio Value of the known component.</param>
		/// <param name="ratioValueOther">The Ratio Value of the other component.</param>
		/// <param name="unitValueKnown">The Unit Value of the known component.</param>
		public static float RatioUnitTotalValue(float ratioValueKnown, float ratioValueOther, float unitValueKnown){return unitValueKnown + RatioUnitValueGivenOtherValue(ratioValueOther, ratioValueKnown, unitValueKnown);}

		
		


		
		
		
		// **************************************** Other ****************************************\\

		/// <summary>
		/// Returns a random float between the floatRange Min (inclusive) and Max (inclusive) value
		/// </summary>
		/// <param name="floatRange"></param>
		/// <returns></returns>
		public static float GetRandomInRange(FloatRange floatRange)
		{
			return Random.Range(floatRange.Min, floatRange.Max);
		}
		
		/// <summary>
		/// Returns a random int between the intRange Min (inclusive) and Max (exclusive) value
		/// </summary>
		/// <param name="intRange"></param>
		/// <returns></returns>
		public static int GetRandomInRange(IntRange intRange)
		{
			return Random.Range(intRange.Min, intRange.Max);
		}
		
		/// <summary>
		/// Returns a random float between the rangedFloat Min (inclusive) and Max (inclusive) value
		/// </summary>
		/// <param name="rangedFloat"></param>
		/// <returns></returns>
		public static float GetRandomInRange(RangedFloat rangedFloat)
		{
			return Random.Range(rangedFloat.Min, rangedFloat.Max);
		}
		
		/// <summary>
		/// Returns a random int between the rangedInt Min (inclusive) and Max (exclusive) value
		/// </summary>
		/// <param name="rangedInt"></param>
		/// <returns></returns>
		public static float GetRandomInRange(RangedInt rangedInt)
		{
			return Random.Range(rangedInt.Min, rangedInt.Max);
		}
		
		
		
		
		
		/// <summary> The Quadratic Formula. Calculates the two possible solutions for the quadratic equation.
		/// <para>If there is no real solution the method will return false.</para>
		/// <para>Quadratic Equation: 0 = ax^2 + bx + c </para>
		/// <para>Quadratic Formula: (x0,x1) = (-b +/- Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a) </para></summary>
		/// <returns><c>true</c>, if two real solutions were found, <c>false</c> otherwise.</returns>
		/// <param name="a">The a constant of the quadratic equation.</param>
		/// <param name="b">The b constant of the quadratic equation.</param>
		/// <param name="c">The c constant of the quadratic equation.</param>
		/// <param name="x1">The first x value.</param>
		/// <param name="x2">The second x value.</param>
		public static bool QuadraticFormula(float a, float b, float c, out float lowerSolution, out float upperSolution){

			// Quadratic Formula
			// ax^2 + bx + c = 0
			// x = (-b +/- Sqrt(b^2 - 4ac)) / (2 * a)

			var d = b * b - 4 * a * c;

			// Cannot square root negative number, there is no solution
			if (d < 0)
			{
				lowerSolution = upperSolution = 0;
				return false;
			}		

			var sqrRootD = Mathf.Sqrt(d);

			var solutionOne = (-1 * b - sqrRootD) / (2 * a);
			var solutionTwo = (-1 * b + sqrRootD) / (2 * a);

			if (solutionOne < solutionTwo)
			{
				lowerSolution = solutionOne;
				upperSolution = solutionTwo;
			}
			else
			{
				lowerSolution = solutionTwo;
				upperSolution = solutionOne;
			}
			
			return true;
		}

		/// <summary>
		/// Rounds a value to the desired amount of decimal places.
		/// This is better for comparing float/double values
		/// </summary>
		/// <returns>The to decimal place.</returns>
		/// <param name="value">Value.</param>
		/// <param name="decimalPlaces">Decimal places.</param>
		public static float RoundToDecimalPlace(float value, int decimalPlaces){

			int multiplier = (int) Mathf.Pow(10, decimalPlaces);

			value *= multiplier;
			int temp = (int) value;
			value = (float) temp;
			value /= multiplier;

			return value;
		}

		/// <summary>
		/// Maps a value from one range to another. 
		/// Will map outside of the range so you may need to clamp the value if necessary.
		/// </summary>
		public static float Map(float inValue, float inMin, float inMax, float outMin, float outMax)
		{
			return (inValue - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
		}
		
		/// <summary>
		/// Maps a value from one range to another. 
		/// Will map outside of the range so you may need to clamp the value if necessary.
		/// </summary>
		public static double Map(double inValue, double inMin, double inMax, double outMin, double outMax)
		{
			return (inValue - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
		}

		public static double Clamp(double value, double min, double max)
		{
			value = Math.Max(value, min);
			value = Math.Min(value, max);
			return value;
		}

		public static double Clamp01(double value)
		{
			value = Math.Max(value, 0);
			value = Math.Min(value, 1);
			return value;
		}
		

		/// <summary>
		/// <para>Wraps a value to the specified lower[inclusive] and upper[inclusive] bound.  Inclusive</para>
		/// <para>Example => Wrap value from -3 to 3. value = -4, lower bound = -3, upper bound = 3, return value = 3</para>
		/// </summary>
		/// <param name="value">The value to wrap</param>
		/// <param name="lowerBound">The lower bound to wrap to</param>
		/// <param name="upperBound">The Upper bound to wrap to</param>
		/// <returns>The value wrapped to the bounds</returns>
		public static int WrapValue(int value, int lowerBound, int upperBound)
		{
			if (lowerBound > upperBound)
			{
				int temp = lowerBound;
				lowerBound = upperBound;
				upperBound = temp;
			}

			int range = upperBound - lowerBound + 1;
			value = ((value - lowerBound) % range) + range;
			value = (value % range) + lowerBound;
			return value;
		}

		public static Vector3 WrapValue(Vector3 value, float lowerBound, float upperBound)
		{
			value.x = WrapValue(value.x, lowerBound, upperBound);
			value.y = WrapValue(value.y, lowerBound, upperBound);
			value.z = WrapValue(value.z, lowerBound, upperBound);

			return value;
		}

		/// <summary>
		/// <para>Wraps a value to the specified lower[inclusive] and upper[exclusive] bound</para>
		/// <para>Example => Wrap value from -3 to 3. value = -4, lower bound = -3, upper bound = 3, return value = 2</para>
		/// </summary>
		/// <param name="value">The value to wrap</param>
		/// <param name="lowerBound">The lower bound to wrap to</param>
		/// <param name="upperBound">The Upper bound to wrap to</param>
		/// <returns>The value wrapped to the bounds</returns>
		public static float WrapValue(float value, float lowerBound, float upperBound)
		{

			if (lowerBound > upperBound)
			{
				var temp = lowerBound;
				lowerBound = upperBound;
				upperBound = temp;
			}

			var range = upperBound - lowerBound;
			value = ((value - lowerBound) % range) + range;
			value = (value % range) + lowerBound;
			return value;
		}

		public static Vector2 RadianToVector2(float radian){return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));}
		public static Vector2 RadianToVector2(float radian, float length){return RadianToVector2(radian) * length;}
		public static Vector2 DegreeToVector2(float degree){return RadianToVector2(degree * Mathf.Deg2Rad);}
		public static Vector2 DegreeToVector2(float degree, float length){return RadianToVector2(degree * Mathf.Deg2Rad) * length;}

		public static Vector3 V2toV3(Vector2 v2){return new Vector3(v2.x, v2.y, 0);}	// Possibly better just to cast

		/// <summary> Move current toward the desired value by rate. Stops at the value. Return the value after applying rate.	/// </summary>
		/// <param name="current">Current Value.</param>
		/// <param name="desired">Desired Value.</param>
		/// <param name="maxAmount">Max Amount to Move. The absolute of this value will be used</param>
		public static float MoveTo(float current, float desired, float maxAmount)
		{
			maxAmount = Mathf.Abs(maxAmount);

			var delta = desired - current;

			if (Mathf.Abs(delta) <= maxAmount) return desired;

			return delta > 0 ? current + maxAmount : current - maxAmount;
		}

		public static Vector2 MoveTo(Vector2 current, Vector2 desired, float maxAmount)
		{
			var vector = desired - current;

			maxAmount = Mathf.Abs(maxAmount);
			
			var delta = vector.magnitude;

			if (delta <= maxAmount) return desired;
			
			return current + vector.normalized * maxAmount;
		}
		
		public static Vector3 MoveTo(Vector3 current, Vector3 desired, float maxAmount)
		{
			var vector = desired - current;

			maxAmount = Mathf.Abs(maxAmount);
			
			var delta = vector.magnitude;

			if (delta <= maxAmount) return desired;
			
			return current + vector.normalized * maxAmount;
		}

		public static Quaternion MoveTo(Quaternion current, Quaternion desired, float maxDegrees)
		{
			return Quaternion.RotateTowards(current, desired, maxDegrees);
		}

		/// <summary> Randomises the input list and returns the randomised array.</summary>
		/// <returns>The randomised list.</returns>
		/// <param name="list">The list to randomise.</param>
		/// <typeparam name="T">The type of elements in the list.</typeparam>
		public static T[] RandomiseList<T>(List<T> list){return RandomiseList<T>(list);}

		/// <summary> Randomises the input list and returns the randomised array.</summary>
		/// <returns>The randomised list.</returns>
		/// <param name="list">The list to randomise.</param>
		/// <typeparam name="T">The type of elements in the list.</typeparam>
		public static T[] RandomiseList<T>(T[] list){

			List<T> remainingList = new List<T>();
			foreach(T item in list){remainingList.Add(item);}

			List<T> randomList = new List<T>();
			for(int i = 0; i < list.Length; i++){
				int index = Random.Range(0, remainingList.Count);
				randomList.Add(remainingList[index]);
				remainingList.RemoveAt(index);
			}

			return randomList.ToArray();
		}


		public static int GetClosest(int reference, IEnumerable<int> values)
		{
			var minDelta = int.MaxValue;
			var closest = 0;

			foreach (var value in values)
			{
				var delta = Mathf.Abs(value - reference);
				
				if(delta > minDelta) continue;
				if(delta == 0) return value;
				minDelta = delta;
				closest = value;
			}

			return closest;
		}
		
		public static float GetClosest(float reference, IEnumerable<float> values)
		{
			var minDelta = float.MaxValue;
			var closest = 0f;

			foreach (var value in values)
			{
				var delta = Mathf.Abs(value - reference);
				
				if(delta > minDelta) continue;
				if(Mathf.Abs(delta) < Mathf.Epsilon) return value;
				minDelta = delta;
				closest = value;
			}

			return closest;
		}
		
		// Power Of (All of these need to be tested)
		
		// Int
		public static int CeilToPowerOf(int value, int baseValue = 2)
		{
			var exponent = Mathf.CeilToInt(Mathf.Log(value, baseValue));
			return (int) Mathf.Pow(baseValue, exponent);
		}

		public static int FloorToPowerOf(int value, int baseValue = 2)
		{
			var exponent = Mathf.FloorToInt(Mathf.Log(value, baseValue));
			return (int) Mathf.Pow(baseValue, exponent);
		}
		
		public static int RoundToNearestPowerOf(int value, int baseValue = 2)
		{
			var upper = CeilToPowerOf(value, baseValue);
			var lower = FloorToPowerOf(value, baseValue);

			var difUpper = Mathf.Abs(upper - value);
			var difLower = Mathf.Abs(lower - value);

			return difUpper < difLower ? upper : lower;
		}
		
		// Float
		public static float CeilToPowerOf(float value, float baseValue = 2)
		{
			var exponent = Mathf.CeilToInt(Mathf.Log(value, baseValue));
			return Mathf.Pow(baseValue, exponent);
		}
		
		public static float FloorToPowerOf(float value, float baseValue = 2)
		{
			var exponent = Mathf.FloorToInt(Mathf.Log(value, baseValue));
			return Mathf.Pow(baseValue, exponent);
		}
		
		public static float RoundToNearestPowerOf(float value, float baseValue = 2)
		{
			var upper = CeilToPowerOf(value, baseValue);
			var lower = FloorToPowerOf(value, baseValue);

			var difUpper = Mathf.Abs(upper - value);
			var difLower = Mathf.Abs(lower - value);

			return difUpper < difLower ? upper : lower;
		}


		// Acceleration Rate

		public static float AccelerationRateTime(float finalSpeed, float initialSpeed, float time)
		{
			time = Mathf.Clamp(time, 0.0001f, Mathf.Infinity);
			return (finalSpeed - initialSpeed) / time;
		}
		
		public static float AccelerationRateDistance(float finalSpeed, float initialSpeed, float distance)
		{
			distance = Mathf.Clamp(distance, 0.0001f, Mathf.Infinity);
			return (finalSpeed * finalSpeed - initialSpeed * initialSpeed) / (2 * distance);
		}

		public static float AccelerationTime(float finalSpeed, float initialSpeed, float acceleration)
		{
			acceleration = Mathf.Clamp(acceleration, 0.0001f, Mathf.Infinity);
			return (finalSpeed - initialSpeed) / acceleration;
		}
		
		public static float AccelerationDistance(float finalSpeed, float initialSpeed, float acceleration)
		{
			acceleration = Mathf.Clamp(acceleration, 0.0001f, Mathf.Infinity);
			return (finalSpeed * finalSpeed - initialSpeed * initialSpeed) / (2 * acceleration);
		}
		
	}
}