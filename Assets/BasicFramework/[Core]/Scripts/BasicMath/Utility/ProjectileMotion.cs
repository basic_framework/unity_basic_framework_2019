﻿using UnityEngine;

namespace BasicFramework.Core.BasicMath.Utility
{
	public static class ProjectileMotion 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
			
		// Y-axis needs to be parallel to gravity
		// X-axis needs to be perpendicular to gravity

		// v0 		= initial velocity in forward-up plane
		// theta	=  angle of projectile from xz plane, positive is up = Atan(vy0/vx0) 

		// dx		= displacement in x (at time t)
		// vx 		= velocity in x (at time t)
		// vx0		= initial velocity in x (t = 0) = v0 * cos(theta)

		// dy		= displacement in y (at time t)
		// vy 		= velocity in y (at time t)
		// vy0		= initial velocity in y (t = 0) = v0 * sin(theta)

		// t 		= time into projection
		// g		= acceleration due to gravity

		public static float InitialVelY(float theta, float v0){return v0 * Mathf.Sin(theta * Mathf.Deg2Rad);}
		public static float InitialVelX(float theta, float v0){return v0 * Mathf.Cos(theta * Mathf.Deg2Rad);}
		public static float InitialAngle(float vx0, float vy0){return Mathf.Atan2(vy0, vx0);}

		public static float DisplacementX(float vx0, float t) 			{return vx0 * t;}
		public static float DisplacementY(float vy0, float t, float g)	{g = Mathf.Abs(g); return (vy0 * t) - ((g * (t * t)) * 0.5f);}
		public static float DisplacementY(float vy0, float vx0, float dx, float g){

			g = Mathf.Abs(g);

			float t = TimeOfFlightDx(vx0, dx); 
			return DisplacementY(vy0, t, g);
		}

		//public static float PM_VelocityX(float vx0, float t)		 {return vx0;}			// If we had drag that would factor in here
		public static float VelocityY(float vy0, float t, float g){g = Mathf.Abs(g); return vy0 - g*t;}

		//public static float PM_TimeOfFlightVX(float vx0, float dx){return dx / vx0;}	// Doesn
		public static float TimeOfFlightDx(float vx0, float dx){return dx / vx0;}

		public static float TimeOfFlightVy(float vy0, float vy, float g){g = Mathf.Abs(g); return ( (vy - vy0) / g) * -1;}
		public static float TimeOfFlightDy(float vy0, float dy, float g){

			// Returns the first time the projectile will be at given height dy, if that is possible, if not return 0

			g = Mathf.Abs(g);

			if (!MathUtility.QuadraticFormula(g * 0.5f, vy0 * -1, dy, out var t1, out var t2)) return 0;
			
			if(t1 > 0) {return t1;}
			if(t2 > 0) {return t2;}
			return 0;

		}

		public static float MaximumHeight(float v0, float theta, float g){g = Mathf.Abs(g); return ((v0 * v0) * Mathf.Pow(Mathf.Sin(theta * Mathf.Deg2Rad), 2)) / (2 * g);}
		public static float InitialVelForHeight(float h, float theta, float g){

			g = Mathf.Abs(g);
			if(h < 0){return 0;}

			return Mathf.Sqrt( (2*g*h) / Mathf.Pow( Mathf.Sin(theta * Mathf.Deg2Rad), 2));

		}

		/// <summary> Returns the horizontal range for level plane of the projectile motion. </summary>
		/// <returns>The horizontal range.</returns>
		/// <param name="v0">Initial velocity in forward-up plane (m/s).</param>
		/// <param name="theta">The angle for projection from the xz plane (deg), where xz plane is perpendicular to gravity.</param>
		/// <param name="g">The value of gravity (m/s^2).</param>
		public static float HorizontalRange(float v0, float theta, float g){g = Mathf.Abs(g); return ((v0 * v0) * Mathf.Sin((2 * theta) * Mathf.Deg2Rad)) / g;}
		public static float InitialVelForRange(float r, float theta, float g){

			g = Mathf.Abs(g);
			if(r < 0){return 0;}

			return Mathf.Sqrt( (g * r) / Mathf.Sin((theta * 2) * Mathf.Deg2Rad) );

		}
		
	}
}