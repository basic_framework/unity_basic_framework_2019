﻿using UnityEngine;

namespace BasicFramework.Core.BasicMath
{
	public static class Easing
	{
		public static float Ease(SymmetricalEasingTypes easeType, float t, float exponent = EXPONENT, 
			float amplitude = AMPLITUDE, float period = PERIOD, float overshoot = OVERSHOOT)
		{
			switch (easeType)
			{
				case SymmetricalEasingTypes.Linear: 
					break;
				
				case SymmetricalEasingTypes.EaseInOutQuad:
					return Ease(EasingTypes.EaseInOutQuad, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutCubic:
					return Ease(EasingTypes.EaseInOutCubic, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutSine:
					return Ease(EasingTypes.EaseInOutSine, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutExponent:
					return Ease(EasingTypes.EaseInOutExponent, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutCircle:
					return Ease(EasingTypes.EaseInOutCircle, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutBounce:
					return Ease(EasingTypes.EaseInOutBounce, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutPolynomial:
					return Ease(EasingTypes.EaseInOutPolynomial, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutElastic:
					return Ease(EasingTypes.EaseInOutElastic, t, exponent, amplitude, period, overshoot);
				
				case SymmetricalEasingTypes.EaseInOutBack:
					return Ease(EasingTypes.EaseInOutBack, t, exponent, amplitude, period, overshoot);
			}
			
			return Ease(EasingTypes.Linear, t, exponent, amplitude, period, overshoot);
		}

		public static float Ease(EasingTypes easeType, float t, float exponent = EXPONENT, 
			float amplitude = AMPLITUDE, float period = PERIOD, float overshoot = OVERSHOOT)
		{
			switch (easeType)
			{
				case EasingTypes.Linear: return EaseLinear(t);

				case EasingTypes.EaseInQuad: return EaseInQuad(t);
				case EasingTypes.EaseOutQuad: return EaseOutQuad(t);
				case EasingTypes.EaseInOutQuad: return EaseInOutQuad(t);

				case EasingTypes.EaseInCubic: return EaseInCubic(t);
				case EasingTypes.EaseOutCubic: return EaseOutCubic(t);
				case EasingTypes.EaseInOutCubic: return EaseInOutCubic(t);

				case EasingTypes.EaseInSine: return EaseInSine(t);
				case EasingTypes.EaseOutSine: return EaseOutSine(t);
				case EasingTypes.EaseInOutSine: return EaseInOutSine(t);

				case EasingTypes.EaseInExponent: return EaseInExponent(t);
				case EasingTypes.EaseOutExponent: return EaseOutExponent(t);
				case EasingTypes.EaseInOutExponent: return EaseInOutExponent(t);

				case EasingTypes.EaseInCircle: return EaseInCircle(t);
				case EasingTypes.EaseOutCircle: return EaseOutCircle(t);
				case EasingTypes.EaseInOutCircle: return EaseInOutCircle(t);

				case EasingTypes.EaseInBounce: return EaseInBounce(t);
				case EasingTypes.EaseOutBounce: return EaseOutBounce(t);
				case EasingTypes.EaseInOutBounce: return EaseInOutBounce(t);

				case EasingTypes.EaseInPolynomial: return EaseInPolynomial(t, exponent);
				case EasingTypes.EaseOutPolynomial: return EaseOutPolynomial(t, exponent);
				case EasingTypes.EaseInOutPolynomial: return EaseInOutPolynomial(t, exponent);

				case EasingTypes.EaseInElastic: return EaseInElastic(t, amplitude, period);
				case EasingTypes.EaseOutElastic: return EaseOutElastic(t, amplitude, period);
				case EasingTypes.EaseInOutElastic: return EaseInOutElastic(t, amplitude, period);

				case EasingTypes.EaseInBack: return EaseInBack(t, overshoot);
				case EasingTypes.EaseOutBack: return EaseOutBack(t, overshoot);
				case EasingTypes.EaseInOutBack: return EaseInOutBack(t, overshoot);

				default: return t;
			}
		}

		//***** Linear *****\\
		
		public static float EaseLinear(float t)
		{
			t = Mathf.Clamp01(t); return t;
		}

		
		//***** Quad *****\\
		
		public static float EaseInQuad(float t)
		{
			t = Mathf.Clamp01(t); 
			return t * t;
		}

		public static float EaseOutQuad(float t)
		{
			t = Mathf.Clamp01(t); 
			return t * (2 - t);
		}

		public static float EaseInOutQuad(float t)
		{
			t = Mathf.Clamp01(t); 
			return (((t *= 2) <= 1) ? t * t : --t * (2 - t) + 1) / 2;
		}

		
		//***** Cubic *****\\

		public static float EaseInCubic(float t)
		{
			t = Mathf.Clamp01(t); 
			return t * t * t;
		}

		public static float EaseOutCubic(float t)
		{
			t = Mathf.Clamp01(t); 
			return --t * t * t + 1;
		}

		public static float EaseInOutCubic(float t)
		{
			t = Mathf.Clamp01(t); 
			return ((t *= 2) <= 1 
				       ? t * t * t 
				       : (t -= 2) * t * t + 2) / 2;
		}
		
		
		//***** Sine *****\\

		private const float 
			PI = Mathf.PI, 
			HALF_PI = PI / 2;

		public static float EaseInSine(float t)
		{
			t = Mathf.Clamp01(t); 
			return 1 - Mathf.Cos(t * HALF_PI);
		}

		public static float EaseOutSine(float t)
		{
			t = Mathf.Clamp01(t); 
			return Mathf.Sin(t * HALF_PI);
		}

		public static float EaseInOutSine(float t)
		{
			t = Mathf.Clamp01(t); 
			return (1 - Mathf.Cos(t * PI)) / 2f;
		}

		
		//***** Exponent *****\\
		
		public static float EaseInExponent(float t)
		{
			t = Mathf.Clamp01(t); 
			return Mathf.Pow(2, 10 * t - 10);
		}

		public static float EaseOutExponent(float t)
		{
			t = Mathf.Clamp01(t); 
			return 1 - Mathf.Pow(2, -10 * t);
		}

		public static float EaseInOutExponent(float t)
		{
			t = Mathf.Clamp01(t); 
			return ((t *= 2) <= 1 
				       ? Mathf.Pow(2, 10 * t - 10) 
				       : 2 - Mathf.Pow(2, 10 - 10 * t)) / 2;
		}

		//***** Circle *****\\

		public static float EaseInCircle(float t)
		{
			t = Mathf.Clamp01(t); 
			return 1 - Mathf.Sqrt(1 - t * t);
		}

		public static float EaseOutCircle(float t)
		{
			t = Mathf.Clamp01(t); 
			return Mathf.Sqrt(1 - --t * t);
		}

		public static float EaseInOutCircle(float t)
		{
			t = Mathf.Clamp01(t); 
			return ((t *= 2) <= 1 
				       ? 1 - Mathf.Sqrt(1 - t * t) 
				       : Mathf.Sqrt(1 - (t -= 2) * t) + 1) / 2;
		}
		
		
		//***** Bounce *****\\

		private const float 
			B1 = 4f / 11f, 
			B2 = 6f / 11f, 
			B3 = 8f / 11f, 
			B4 = 3f / 4f, 
			B5 = 9f / 11f, 
			B6 = 10f / 11f, 
			B7 = 15f / 16f, 
			B8 = 21f / 22f, 
			B9 = 63f / 64f, 
			B0 = 1f / B1 / B1;

		public static float EaseInBounce(float t)
		{
			t = Mathf.Clamp01(t); 
			return 1 - EaseOutBounce(1 - t);
		}

		public static float EaseOutBounce(float t)
		{
			t = Mathf.Clamp01(t); 
			return t < B1 
				? B0 * t * t 
				: t < B3 
					? B0 * (t -= B2) * t + B4 
					: (t < B6) ? B0 * (t -= B5) * t + B7 : B0 * (t -= B8) * t + B9;
		}

		public static float EaseInOutBounce(float t)
		{
			t = Mathf.Clamp01(t); 
			return ((t *= 2) <= 1 
				       ? 1 - EaseOutBounce(1 - t) 
				       : EaseOutBounce(t - 1) + 1) / 2;
		}
		
		
		//***** Polynomial *****\\

		private const float EXPONENT = 3;

		public static float EaseInPolynomial(float t, float e = EXPONENT)
		{
			t = Mathf.Clamp01(t); 
			return Mathf.Pow(t, e);
		}

		public static float EaseOutPolynomial(float t, float e = EXPONENT)
		{
			t = Mathf.Clamp01(t); 
			return 1 - Mathf.Pow(1 - t, e);
		}

		public static float EaseInOutPolynomial(float t, float e = EXPONENT)
		{
			t = Mathf.Clamp01(t); 
			return ((t *= 2) <= 1 
				       ? Mathf.Pow(t, e) 
				       : 2 - Mathf.Pow(2 - t, e)) / 2;
		}
		
		
		//***** Back *****\\

		private const float OVERSHOOT = 1.70158f;

		public static float EaseInBack(float t, float o = OVERSHOOT)
		{
			t = Mathf.Clamp01(t); 
			o = Mathf.Abs(o); 
			return t * t * ((o + 1) * t - o);
		}

		public static float EaseOutBack(float t, float o = OVERSHOOT)
		{
			t = Mathf.Clamp01(t); 
			o = Mathf.Abs(o); 
			return --t * t * ((o + 1) * t + o) + 1;
		}

		public static float EaseInOutBack(float t, float o = OVERSHOOT)
		{
			t = Mathf.Clamp01(t); 
			o = Mathf.Abs(o); 
			return ((t *= 2) < 1 
				       ? t * t * ((o + 1) * t - o) 
				       : (t -= 2) * t * ((o + 1) * t + o) + 2) / 2;
		}
		
		
		//***** Elastic *****\\

		private const float 
			AMPLITUDE = 1, 
			PERIOD = 0.3f, 
			TAU = 2 * Mathf.PI;
		
		public static float EaseInElastic(float t, float a = AMPLITUDE, float p = PERIOD)
		{
			t = Mathf.Clamp01(t);
			var s = Mathf.Asin(1 / (a = Mathf.Max(1, a))) * (p /= TAU);
			return a * Mathf.Pow(2, 10 * --t) * Mathf.Sin((s - t) / p);
		}

		public static float EaseOutElastic(float t, float a = AMPLITUDE, float p = PERIOD)
		{
			t = Mathf.Clamp01(t);
			var s = Mathf.Asin(1 / (a = Mathf.Max(1, a))) * (p /= TAU);
			return 1 - a * Mathf.Pow(2, -10 * (t)) * Mathf.Sin((t + s) / p);
		}

		public static float EaseInOutElastic(float t, float a = AMPLITUDE, float p = PERIOD)
		{
			t = Mathf.Clamp01(t);
			var s = Mathf.Asin(1 / (a = Mathf.Max(1, a))) * (p /= TAU);
			return (((t = t * 2 - 1) < 0) ? a * Mathf.Pow(2, 10 * t) * Mathf.Sin((s - t) / p) : 2 - a * Mathf.Pow(2, -10 * t) * Mathf.Sin((t + s) / p)) / 2;
		}
		
	}
}