﻿using System;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicMath
{
	public enum EasingTypes
	{
		Linear,
		EaseInQuad, EaseOutQuad, EaseInOutQuad,
		EaseInCubic, EaseOutCubic, EaseInOutCubic,
		EaseInSine, EaseOutSine, EaseInOutSine,
		EaseInExponent, EaseOutExponent, EaseInOutExponent,
		EaseInCircle, EaseOutCircle, EaseInOutCircle,
		EaseInBounce, EaseOutBounce, EaseInOutBounce,
		EaseInPolynomial, EaseOutPolynomial, EaseInOutPolynomial,
		EaseInElastic, EaseOutElastic, EaseInOutElastic,
		EaseInBack, EaseOutBack, EaseInOutBack
	}
	
	[Serializable]
	public class EasingTypesUnityEvent : UnityEvent<EasingTypes>{}
	
//	[Serializable]
//	public class EasingTypesValueChangedUnityEvent : ValueChangeCompareUnityEventBase<EasingTypes, EasingTypesUnityEvent>{}

	
	public enum SymmetricalEasingTypes
	{
		Linear,
		EaseInOutQuad,
		EaseInOutCubic,
		EaseInOutSine,
		EaseInOutExponent,
		EaseInOutCircle,
		EaseInOutBounce,
		EaseInOutPolynomial,
		EaseInOutElastic,
		EaseInOutBack
	}
	
	[Serializable]
	public class SymmetricalEasingTypesUnityEvent : UnityEvent<SymmetricalEasingTypes>{}
	
//	[Serializable]
//	public class SymmetricalEasingTypesValueChangedUnityEvent : ValueChangeCompareUnityEventBase<SymmetricalEasingTypes, SymmetricalEasingTypesUnityEvent>{}
}