﻿using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicMath
{
	public abstract class MovingAverageBase<T> 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private T _average;
		public T Average => _average;

		private T _total;
		public T Total => _total;
		
		private uint _sampleCount;
		public uint SampleCount => _sampleCount;
		
		private uint _sampleIndex;
		
		private readonly T[] _samples;
		

		// ****************************************  METHODS  ****************************************\\

		protected abstract T Add(T left, T right);
		protected abstract T Remove(T left, T right);
		protected abstract T Divide(T numerator, uint denominator);
		
		public MovingAverageBase(uint maxSamples)
		{
			if (maxSamples < 1) 
				maxSamples = 1;
			
			_samples = new T[maxSamples];
		}

		public T AddSample(T sampleValue)
		{
			_total = Remove(_total, _samples[_sampleIndex]);

			_total = Add(_total, sampleValue);

			if (_sampleCount < _samples.Length)
			{
				_sampleCount++;
			}

			_average = Divide(_total, _sampleCount);

			_sampleIndex++;
			if (_sampleIndex >= _samples.Length)
			{
				_sampleIndex = 0;
			}

			return _average;
		}
		

	}
}