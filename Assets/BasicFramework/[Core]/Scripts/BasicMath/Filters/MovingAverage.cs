﻿using System.Collections.Generic;

namespace BasicFramework.Core.BasicMath
{
	public class MovingAverage
	{
		
		// **************************************** VARIABLES ****************************************\\ 
		
		private readonly Queue<float> _samples = new Queue<float>();

		private uint _maxSamples;

		public uint MaxSamples
		{
			get => _maxSamples;
			set => ChangeMaxSampleSize(value);
		}

		private float _total;

		private float _average;
		public float Average => _average;

		
		// ****************************************  METHODS  ****************************************\\		
		
		public MovingAverage(uint maxSamples = 10)
		{
			ChangeMaxSampleSize(maxSamples);
		}
		
		public void AddSample(float value)
		{
			if (_samples.Count == _maxSamples)
			{
				_total -= _samples.Dequeue();
			}

			_samples.Enqueue(value);
			_total += value;
			
			_average = _total / _samples.Count;
		}

		private void ChangeMaxSampleSize(uint value)
		{
			if (_maxSamples == value) return;

			if (value < 2)
			{
				value = 2;
			}

			_maxSamples = value;

			while (_maxSamples < _samples.Count)
			{
				_total -= _samples.Dequeue();
			}

			var count = _samples.Count;
			
			_average = count == 0 ? 0 : _total / _samples.Count;
		}
		
	}
}

