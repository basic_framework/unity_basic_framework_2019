using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
	[AddComponentMenu("Basic Framework/Core/Utility/Events/Delayed Unity Event")]
	public class DelayedUnityEvent : MonoBehaviour 
	{

		// **************************************** VARIABLES ****************************************\\
		
		public enum Modes
		{
			OneShot,
			ResetOnEnable,
			Loop
		}

		public enum DelayModes
		{
			Custom,
			Random,
			FirstFrame,
			SecondFrame
		}

		[Tooltip("OneShot => Destroys component after time out\n" +
						"ResetOnEnable => Disables component on time out, the timer is reset on enable" +
						"Loop => Resets timer on time out")]
		[SerializeField] private Modes _mode = Modes.OneShot;
		
		[SerializeField] private DelayModes _delayMode = DelayModes.Custom;
		
		[Tooltip("How long to wait (in seconds) before raising the event")]
		[SerializeField] private FloatSingleReference _customDelay = default;
		
		[Tooltip("The Range that the delay will be chosen between")]
		[SerializeField] private FloatRangeSingleReference _randomDelayRange = new FloatRangeSingleReference(0f, 1f);
		
		[Tooltip("Unscaled delta time can have inconsistant results, try to avoid using unscaled time")]
		[SerializeField] private bool _useUnscaledDeltaTime = default;


		[Tooltip("Raised after the specified delay")]
		[SerializeField] private UnityEvent _timedOut = default;

		[Tooltip("Raised when the duration of the timer is updated")]
		[SerializeField] private FloatUnityEvent _timerDurationUpdated = default;

		[Tooltip("Raised every update with the remaining time left before the Delayed Unity Event will be raised")]
		[SerializeField] private FloatUnityEvent _remainingTimeEvent = default;
		
		[Tooltip("Raised every update with the normalized time (0-1) left before the Delayed Unity Event will be raised")]
		[SerializeField] private FloatUnityEvent _remainingNormalizedTimeEvent = default;
		
		
		private float _timerDuration;
		private float _remainingTime;
		private float _remainingTimeNormalized;

		public float TimerDuration => _timerDuration;

		public float RemainingTime => _remainingTime;

		public float RemainingTimeNormalized => _remainingTimeNormalized;
		

		// ****************************************  METHODS  ****************************************\\

		private void OnEnable()
		{
			if (_mode != Modes.ResetOnEnable) return;
			ResetTimer();
		}

		private void Start()
		{
			ResetTimer();
		}

		private void Update()
		{
			if (_delayMode == DelayModes.SecondFrame && _remainingTime > 0f)
			{
				_remainingTime = 0;
				return;
			}

			_remainingTime -= _useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
			_remainingTime = Mathf.Max(_remainingTime, 0f);

			_remainingTimeNormalized = _timerDuration <= Mathf.Epsilon 
					? 0f 
					: Mathf.Clamp01(_remainingTime / _timerDuration);

			_remainingTimeEvent.Invoke(_remainingTime);
			_remainingNormalizedTimeEvent.Invoke(_remainingTimeNormalized);

			if (_remainingTime > 0) return;

			// Disable before invoke in case want to re enable script on time out
			if (_mode == Modes.ResetOnEnable)
			{
				enabled = false;
			}
			
			_timedOut.Invoke();
			
			switch (_mode)
			{
				case Modes.OneShot:			Destroy(this);	break;
				case Modes.ResetOnEnable:	break;
				case Modes.Loop:			ResetTimer();		break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		public void ResetTimer()
		{
			switch (_delayMode)
			{
				case DelayModes.Custom:
					_timerDuration = _customDelay.Value;
					break;
				
				case DelayModes.Random:
					_timerDuration = _randomDelayRange.Value.GetRandomInRange(); 
					break;
				
				case DelayModes.FirstFrame:
					_timerDuration = 0f;
					break;
				
				case DelayModes.SecondFrame:
					_timerDuration = 0.001f;
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			_timerDuration = Mathf.Clamp(_timerDuration, 0, float.MaxValue);
			_remainingTime = _timerDuration;
			_remainingTimeNormalized = 1f;
			
			_timerDurationUpdated.Invoke(_timerDuration);
			_remainingTimeEvent.Invoke(_remainingTime);
			_remainingNormalizedTimeEvent.Invoke(_remainingTimeNormalized);
		}
		
	}
}