using System;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Events.Editor
{
    [CustomEditor(typeof(DelayedUnityEvent)), CanEditMultipleObjects]
    public class DelayedUnityEventEditor : BasicBaseEditor<DelayedUnityEvent>
    {
        
        // **************************************** VARIABLES ****************************************\\

        protected override bool UpdateConstantlyInPlayMode => true;
		
        protected override bool HasDebugSection => true;


        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            Space();
            DrawSettingFields();
        }

        protected override void DrawDebugFields()
        {
            EditorGUILayout.LabelField("Remaining Time", TypedTarget.RemainingTime.ToString());
            EditorGUILayout.LabelField("Remaining Time Normalized", TypedTarget.RemainingTimeNormalized.ToString());
        }

        private void DrawSettingFields()
        {
            SectionHeader("Settings");
            
            var mode = serializedObject.FindProperty("_mode");
            var delayMode = serializedObject.FindProperty("_delayMode");
            var customDelay = serializedObject.FindProperty("_customDelay");
            var randomDelayRange = serializedObject.FindProperty("_randomDelayRange");
            var useUnscaledDeltaTime = serializedObject.FindProperty("_useUnscaledDeltaTime");
            
            EditorGUILayout.PropertyField(useUnscaledDeltaTime);
            
            EditorGUILayout.PropertyField(mode);
            
            EditorGUILayout.PropertyField(delayMode);

            var delayModeEnum =
                EnumUtility.EnumNameToEnumValue<DelayedUnityEvent.DelayModes>(delayMode.enumNames[delayMode.enumValueIndex]);
            
            switch (delayModeEnum)
            {
                case DelayedUnityEvent.DelayModes.Custom:
                    EditorGUILayout.PropertyField(customDelay);
                    break;
                case DelayedUnityEvent.DelayModes.Random:
                    EditorGUILayout.PropertyField(randomDelayRange);
                    break;
                case DelayedUnityEvent.DelayModes.FirstFrame:
                    EditorGUILayout.LabelField("The event will be raised on the First Update Frame", EditorStyles.boldLabel);
                    break;
                case DelayedUnityEvent.DelayModes.SecondFrame:
                    EditorGUILayout.LabelField("The event will be raised on the Second Update Frame", EditorStyles.boldLabel);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected override void DrawEventFields()
        {
            var timedOut = serializedObject.FindProperty("_timedOut");
            var timerDurationUpdated = serializedObject.FindProperty("_timerDurationUpdated");
            var remainingTimeEvent = serializedObject.FindProperty("_remainingTimeEvent");
            var remainingNormalizedTimeEvent = serializedObject.FindProperty("_remainingNormalizedTimeEvent");
            
            timedOut.isExpanded = EditorGUILayout.Foldout(timedOut.isExpanded, "Events", BasicEditorStyles.BoldFoldout);
            if(!timedOut.isExpanded) return;
            
            EditorGUILayout.PropertyField(timedOut);
            EditorGUILayout.PropertyField(timerDurationUpdated);
            EditorGUILayout.PropertyField(remainingTimeEvent);
            EditorGUILayout.PropertyField(remainingNormalizedTimeEvent);
        }
        
    }
}