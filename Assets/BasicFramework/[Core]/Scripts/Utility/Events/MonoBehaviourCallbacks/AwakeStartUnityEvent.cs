﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
	[AddComponentMenu("Basic Framework/Core/Utility/Events/Awake | Start", 0)]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
	public class AwakeStartUnityEvent : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private UnityEvent _awake = default;
		[SerializeField] private UnityEvent _start = default;


		// ****************************************  METHODS  ****************************************\\

		private void Awake()
		{
			_awake.Invoke();
		}
		
		private void Start()
		{
			_start.Invoke();
			Destroy(this);
		}
		
	}
}