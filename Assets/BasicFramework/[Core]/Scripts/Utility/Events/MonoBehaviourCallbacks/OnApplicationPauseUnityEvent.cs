﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/On Application Pause", 6)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class OnApplicationPauseUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityEvent _onApplicationPause = default;


        // ****************************************  METHODS  ****************************************\\

        private void OnApplicationPause()
        {
            _onApplicationPause.Invoke();
        }
		
    }
}