﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/On Destroy", 9)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class OnDestroyUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityEvent _onDestroy = default;


        // ****************************************  METHODS  ****************************************\\

        private void OnDestroy()
        {
            _onDestroy.Invoke();
        }
		
    }
}