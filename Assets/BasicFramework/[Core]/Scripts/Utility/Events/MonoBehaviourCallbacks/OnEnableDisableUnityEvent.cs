﻿using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/On Enable|Disable", 1)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class OnEnableDisableUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BoolPlusUnityEvent _enabledChanged = default;

        
        public BoolPlusUnityEvent EnabledChanged => _enabledChanged;


        // ****************************************  METHODS  ****************************************\\

        private void OnEnable()
        {
            _enabledChanged.Raise(true);
        }

        private void OnDisable()
        {
            _enabledChanged.Raise(false);
        }
        
    }
}