﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/On Application Quit", 7)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class OnApplicationQuitUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityEvent _onApplicationQuit = default;


        // ****************************************  METHODS  ****************************************\\

        private void OnApplicationQuit()
        {
            _onApplicationQuit.Invoke();
        }
		
    }
}