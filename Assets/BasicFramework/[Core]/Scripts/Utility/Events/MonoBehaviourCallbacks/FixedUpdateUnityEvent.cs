﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/Fixed Update", 3)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class FixedUpdateUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityEvent _fixedUpdate = default;


        // ****************************************  METHODS  ****************************************\\

        private void FixedUpdate()
        {
            _fixedUpdate.Invoke();
        }
		
    }
}