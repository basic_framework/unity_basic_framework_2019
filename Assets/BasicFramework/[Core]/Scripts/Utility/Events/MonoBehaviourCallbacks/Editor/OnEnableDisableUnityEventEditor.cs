﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Events.Editor
{
	[CustomEditor(typeof(OnEnableDisableUnityEvent))]
	public class OnEnableDisableUnityEventEditor : BasicBaseEditor<OnEnableDisableUnityEvent>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var enabledChanged = serializedObject.FindProperty("_enabledChanged");

			var value = enabledChanged.FindPropertyRelative("_value");
			var invertedValue = enabledChanged.FindPropertyRelative("_invertedValue");
			var isTrue = enabledChanged.FindPropertyRelative("_isTrue");
			var isFalse = enabledChanged.FindPropertyRelative("_isFalse");

			Space();
			SectionHeader(GetGuiContent(enabledChanged));
			
			EditorGUILayout.PropertyField(value, GetGuiContent("Enabled"));
			EditorGUILayout.PropertyField(invertedValue, GetGuiContent("Enabled Inverted"));
			
			EditorGUILayout.PropertyField(isTrue, GetGuiContent("On Enable"));
			EditorGUILayout.PropertyField(isFalse, GetGuiContent("On Disable"));
		}

	}
}