﻿using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Events
{
    [AddComponentMenu("Basic Framework/Core/Utility/Events/Update", 4)]
    [DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_UNITY_CALLBACK_EVENTS)]
    public class UpdateUnityEvent : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private UnityEvent _update = default;


        // ****************************************  METHODS  ****************************************\\

        private void Update()
        {
            _update.Invoke();
        }
		
    }
}