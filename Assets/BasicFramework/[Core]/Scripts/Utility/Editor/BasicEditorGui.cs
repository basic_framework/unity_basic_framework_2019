using System;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	public static class BasicEditorGui 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private static readonly GUIContent _tempGuiContent = new GUIContent();


		// ****************************************  METHODS  ****************************************\\
	
		//***** Not Null *****\\
		
		public static void PropertyFieldNotNull(Rect position, SerializedProperty property, bool includeChildren = false)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldNotNull(position, property, _tempGuiContent, includeChildren);
		}
		
		public static void PropertyFieldNotNull(Rect position, SerializedProperty property, GUIContent label, bool includeChildren = false)
		{
			if (property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUI.PropertyField(position, property, label, includeChildren);
				return;
			}
			
			var prevGuiBgColor = GUI.backgroundColor;
			
			GUI.backgroundColor = property.objectReferenceValue == null
				? BasicEditorStyles.IsNullFieldColor
				: BasicEditorStyles.NotNullFieldColor;
			
			EditorGUI.PropertyField(position, property, label, includeChildren);

			GUI.backgroundColor = prevGuiBgColor;
		}
		
		
		//***** Can Be Null *****\\
	
		public static void PropertyFieldCanBeNull(Rect position, SerializedProperty property, bool includeChildren = false)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldCanBeNull(position, property, _tempGuiContent, includeChildren);
		}
		
		public static void PropertyFieldCanBeNull(Rect position, SerializedProperty property, GUIContent label, bool includeChildren = false)
		{
			if (property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUI.PropertyField(position, property, label, includeChildren);
				return;
			}
			
			var prevGuiBgColor = GUI.backgroundColor;
			
			GUI.backgroundColor = property.objectReferenceValue == null
				? BasicEditorStyles.IsAndCanBeNullFieldColor
				: BasicEditorStyles.NotNullFieldColor;
			
			EditorGUI.PropertyField(position, property, label, includeChildren);

			GUI.backgroundColor = prevGuiBgColor;
		}
		
		
		//***** String *****\\
		
		public static string StringPopupField(Rect rect, GUIContent label, string selected, string[] options)
		{
			var selectedIndex = -1;

			for (int i = 0; i < options.Length; i++)
			{
				if (options[i] != selected) continue;
				selectedIndex = i;
				break;
			}
			
			selectedIndex = EditorGUI.Popup(rect, label.text, selectedIndex, options);

			// Selected an option in range
			if (selectedIndex >= 0 && selectedIndex < options.Length)
			{
				return options[selectedIndex];
			}
			
			// if value is null return the first element, otherwise the current value 
			return String.IsNullOrEmpty(selected) && options.Length > 0 ? options[0] : selected;
		}
		
	}
}