﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BasicFramework.Core.Utility.Editor
{
	public static class BasicEditorGuiLayout 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float DEFAULT_DROP_AREA_HEIGHT = 32f;
		private static readonly GUIContent _tempGuiContent = new GUIContent();
		
		private static string[] Vector2DirectionNames = new[]
		{
			"Custom",
			"Right",
			"Left",
			"Up",
			"Down"
		};
		
		private static Vector2[] Vector2DirectionValues = new[]
		{
			Vector2.zero,
			Vector2.right,
			Vector2.left,
			Vector2.up,
			Vector2.down
		};
		
		private static string[] Vector3DirectionNames = new[]
		{
			"Custom",
			"Right",
			"Left",
			"Up",
			"Down",
			"Forward",
			"Back"
		};

		private static Vector3[] Vector3DirectionValues = new[]
		{
			Vector3.zero,
			Vector3.right,
			Vector3.left,
			Vector3.up,
			Vector3.down,
			Vector3.forward,
			Vector3.back
		};


		// ****************************************  METHODS  ****************************************\\

		//***** General *****\\
		
		public static void DrawScriptField(SerializedObject serializedObject)
		{
			var prevGuiEnabled = GUI.enabled;
			GUI.enabled = false;
			//serializedObject.Update();
			var prop = serializedObject.FindProperty("m_Script");

			if (prop != null)
			{
				EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);
			}
			
			//serializedObject.ApplyModifiedProperties();
			GUI.enabled = prevGuiEnabled;
		}

		//***** String *****\\
		
		public static string StringPopupField(GUIContent label, string selected, string[] options)
		{
			var selectedIndex = -1;

			for (int i = 0; i < options.Length; i++)
			{
				if (options[i] != selected) continue;
				selectedIndex = i;
				break;
			}
			
			selectedIndex = EditorGUILayout.Popup(label, selectedIndex, options);

			// Selected an option in range
			if (selectedIndex >= 0 && selectedIndex < options.Length)
			{
				return options[selectedIndex];
			}
			
			// if value is null return the first element, otherwise the current value 
			return String.IsNullOrEmpty(selected) && options.Length > 0 ? options[0] : selected;
		}
		
		
		//***** Not Null *****\\
		
		public static void PropertyFieldNotNull(SerializedProperty property, params GUILayoutOption[] options)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldNotNull(property, _tempGuiContent, false, options);
		}
		
		public static void PropertyFieldNotNull(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
		{
			PropertyFieldNotNull(property, label, false, options);
		}
		
		public static void PropertyFieldNotNull(SerializedProperty property, bool includeChildren, params GUILayoutOption[] options)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldNotNull(property, _tempGuiContent, includeChildren, options);
		}
		
		public static void PropertyFieldNotNull(SerializedProperty property, GUIContent label, bool includeChildren, params GUILayoutOption[] options)
		{
			if (property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUILayout.PropertyField(property, label, includeChildren, options);
				return;
			}
			
			var prevGuiBgColor = GUI.backgroundColor;
			
			GUI.backgroundColor = property.objectReferenceValue == null
				? BasicEditorStyles.IsNullFieldColor
				: BasicEditorStyles.NotNullFieldColor;
			
			EditorGUILayout.PropertyField(property, label, includeChildren, options);

			GUI.backgroundColor = prevGuiBgColor;
		}
	
		
		//***** Can Be Null *****\\
		
		public static void PropertyFieldCanBeNull(SerializedProperty property, params GUILayoutOption[] options)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldCanBeNull(property, _tempGuiContent, false, options);
		}
		
		public static void PropertyFieldCanBeNull(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
		{
			PropertyFieldCanBeNull(property, label, false, options);
		}
		
		public static void PropertyFieldCanBeNull(SerializedProperty property, bool includeChildren, params GUILayoutOption[] options)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.image = null;
			_tempGuiContent.tooltip = "";
			
			PropertyFieldCanBeNull(property, _tempGuiContent, includeChildren, options);
		}
		
		public static void PropertyFieldCanBeNull(SerializedProperty property, GUIContent label, bool includeChildren, params GUILayoutOption[] options)
		{
			if (property.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUILayout.PropertyField(property, label, includeChildren, options);
				return;
			}
			
			var prevGuiBgColor = GUI.backgroundColor;
			
			GUI.backgroundColor = property.objectReferenceValue == null
				? BasicEditorStyles.IsAndCanBeNullFieldColor
				: BasicEditorStyles.NotNullFieldColor;
			
			EditorGUILayout.PropertyField(property, label, includeChildren, options);

			GUI.backgroundColor = prevGuiBgColor;
		}
		
		
		//***** ReorderableList *****\\

		/// <summary>
		/// Call this in editor script OnEnable. Store the result.
		/// In OnInspectorGUI call DoLayoutList on stored ReorderableList.
		/// Note: Does not work for array of arrays.
		/// </summary>
		/// <param name="serializedObject">SerializedObject of the arraySerializedProperty</param>
		/// <param name="arraySerializedProperty">SerializedProperty of an Array or List</param>
		/// <param name="drawElementLabel">If true, display Element I next to each property (where I is the index of the property in the array|list) </param>
		/// <returns></returns>
		public static ReorderableList CreateBasicReorderableList(
			SerializedObject serializedObject, SerializedProperty arraySerializedProperty, bool drawElementLabel = false)
		{
			// Create reorderable list
			var reorderableList = new ReorderableList(
				serializedObject, 
				arraySerializedProperty, 
				true, 
				true, 
				true, 
				true
			);

			reorderableList.drawHeaderCallback =
				rect =>
				{
					EditorGUI.LabelField(rect, arraySerializedProperty.displayName);
				};
			
			// Get the elements height
			reorderableList.elementHeightCallback =
				index =>
				{
					var element = arraySerializedProperty.GetArrayElementAtIndex(index);
					return EditorGUI.GetPropertyHeight(element, true) + 4;
				};
			
			// Draw the element
			reorderableList.drawElementCallback =
				(rect, index, active, focused) =>
				{
					rect.y += 2;
					rect.height -= 4;
					rect.width -= 10f;
					
					var tempRect = new Rect(rect);
					tempRect.x += 8;
					
					var element = arraySerializedProperty.GetArrayElementAtIndex(index);

					if (drawElementLabel)
					{
						EditorGUI.PropertyField(tempRect, element, true);
					}
					else
					{
						EditorGUI.PropertyField(rect, element, GUIContent.none, true);
					}
				};

			return reorderableList;
		}
		
		public static LayerMask DrawUnityLayerMask(string label, int mask)
		{
			return DrawUnityLayerMask(label, mask, EditorStyles.layerMaskField);
		}

		public static LayerMask DrawUnityLayerMask(string label, int mask, GUIStyle style)
		{
			LayerMask tempMask = EditorGUILayout.MaskField(
				label,
				InternalEditorUtility.LayerMaskToConcatenatedLayersMask(mask), 
				InternalEditorUtility.layers
			);
			
			return InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(tempMask);
		}
		
		
		//***** Drag & Drop *****\\

		public static bool DragAndDropAreaIntoList<T>(SerializedProperty list, string dropAreaMessage,
			float dropAreaHeight = DEFAULT_DROP_AREA_HEIGHT)
		{
			var droppedObjects = DragAndDropArea<T>(dropAreaMessage, dropAreaHeight);

			if (droppedObjects == null) return false;

			if (!list.isArray)
			{
				Debug.LogError($"Cannot add {typeof(T).Name}(s) to {list.name}. {list.name} is not an array");
				return false;
			}

			foreach (var droppedObject in droppedObjects)
			{
				BasicEditorArrayUtility.AddArrayElement(list, droppedObject);
			}

			return true;
		}
		
		public static Object[] DragAndDropArea<T>(string dropAreaMessage, float dropAreaHeight = DEFAULT_DROP_AREA_HEIGHT)
		{
			// Draw the Drop Area
			var currentEvent = Event.current;
			var dropAreaRect = GUILayoutUtility.GetRect(0f, dropAreaHeight, GUILayout.ExpandWidth(true));

			var prevAlignment = GUI.skin.box.alignment;
			GUI.skin.box.alignment = TextAnchor.MiddleCenter;
			GUI.Box(dropAreaRect, dropAreaMessage, GUI.skin.box);
			GUI.skin.box.alignment = prevAlignment;

			EditorGUILayout.Space();
			
			if (!dropAreaRect.Contains(currentEvent.mousePosition)) return null;

			// Ensure this is a drag event
			if (currentEvent.type != EventType.DragUpdated && currentEvent.type != EventType.DragPerform) return null;
			
			var draggedObjects = DragAndDrop.objectReferences;

			var requiredType = typeof(T);
			
			// Make sure all dragged objects can be cast to the desired type
			foreach (var draggedObject in draggedObjects)
			{
				var draggedObjectType = draggedObject.GetType();
				
				if (draggedObjectType == requiredType || draggedObject.GetType().IsSubclassOf(typeof(T))) continue;

				if (requiredType.IsSubclassOf(typeof(Component)))
				{
					var draggedGameObject = draggedObject as GameObject;
					var component = draggedGameObject == null ? null : draggedGameObject.GetComponent(requiredType);
					if(component != null) continue;
				}
				
				DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
				return null;
			}
			
			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

			if (currentEvent.type == EventType.DragUpdated) return null;
			
			// Accept the drag and store the 
			DragAndDrop.AcceptDrag();

			return draggedObjects;
		}


		//***** Actions *****\\
		
		public static void ActionList(IEnumerable<Action> actions)
		{
			var prevGuiEnabled = GUI.enabled;
			
			foreach (var action in actions)
			{
				var targetObject = action.Target as Object;

				GUI.enabled = false;
				EditorGUILayout.ObjectField("Target", targetObject, typeof(Object), true);
				GUI.enabled = prevGuiEnabled;
				EditorGUILayout.LabelField("Method", action.Method.ToString());
				EditorGUILayout.Space();
			}
		}
		
		public static void ActionList<T0>(IEnumerable<Action<T0>> actions)
		{
			var prevGuiEnabled = GUI.enabled;
			
			foreach (var action in actions)
			{
				var targetObject = action.Target as Object;

				GUI.enabled = false;
				EditorGUILayout.ObjectField("Target", targetObject, typeof(Object), true);
				GUI.enabled = prevGuiEnabled;
				EditorGUILayout.LabelField("Method", action.Method.ToString());
				EditorGUILayout.Space();
			}
		}
		
		public static void ActionList<T0, T1>(IEnumerable<Action<T0, T1>> actions)
		{
			var prevGuiEnabled = GUI.enabled;
			
			foreach (var action in actions)
			{
				var targetObject = action.Target as Object;

				GUI.enabled = false;
				EditorGUILayout.ObjectField("Target", targetObject, typeof(Object), true);
				GUI.enabled = prevGuiEnabled;
				EditorGUILayout.LabelField("Method", action.Method.ToString());
				EditorGUILayout.Space();
			}
		}
		
		public static void ActionList<T0, T1, T2>(IEnumerable<Action<T0, T1, T2>> actions)
		{
			var prevGuiEnabled = GUI.enabled;
			
			foreach (var action in actions)
			{
				var targetObject = action.Target as Object;

				GUI.enabled = false;
				EditorGUILayout.ObjectField("Target", targetObject, typeof(Object), true);
				GUI.enabled = prevGuiEnabled;
				EditorGUILayout.LabelField("Method", action.Method.ToString());
				EditorGUILayout.Space();
			}
		}
		
		public static void ActionList<T0, T1, T2, T3>(IEnumerable<Action<T0, T1, T2, T3>> actions)
		{
			var prevGuiEnabled = GUI.enabled;
			
			foreach (var action in actions)
			{
				var targetObject = action.Target as Object;

				GUI.enabled = false;
				EditorGUILayout.ObjectField("Target", targetObject, typeof(Object), true);
				GUI.enabled = prevGuiEnabled;
				EditorGUILayout.LabelField("Method", action.Method.ToString());
				EditorGUILayout.Space();
			}
		}
		
		
		//***** Vector *****\\

		public static void Vector2DirectionDropdown(SerializedProperty vector2Property)
		{
			var index = 0;
			
			for (int i = 0; i < Vector2DirectionValues.Length; i++)
			{
				if(Vector2DirectionValues[i] != vector2Property.vector2Value) continue;
				index = i;
				break;
			}

			EditorGUILayout.BeginHorizontal();
			
			var newIndex = EditorGUILayout.Popup(vector2Property.displayName, index, Vector2DirectionNames, GUILayout.ExpandWidth(false));
			EditorGUILayout.PropertyField(vector2Property, GUIContent.none);

			if (GUILayout.Button(BasicEditorStyles.GetGuiContent("1", "Normalize"), EditorStyles.miniButton,GUILayout.Width(25f)))
			{
				vector2Property.vector2Value = vector2Property.vector2Value.normalized;
				GUI.FocusControl("");
			}
			
			EditorGUILayout.EndHorizontal();
			
			if (index == newIndex) return;

			vector2Property.vector2Value = Vector2DirectionValues[newIndex];
		}

		public static void Vector3DirectionDropdown(SerializedProperty vector3Property)
		{
			_tempGuiContent.text = vector3Property.displayName;
			_tempGuiContent.tooltip = vector3Property.tooltip;
			Vector3DirectionDropdown(vector3Property, _tempGuiContent);

		}

		public static void Vector3DirectionDropdown(SerializedProperty vector3Property, GUIContent label)
		{
			var index = 0;
			
			for (int i = 0; i < Vector3DirectionValues.Length; i++)
			{
				if(Vector3DirectionValues[i] != vector3Property.vector3Value) continue;
				index = i;
				break;
			}

			EditorGUILayout.BeginHorizontal();

			
			var prevFieldWidth = EditorGUIUtility.fieldWidth;

			EditorGUIUtility.fieldWidth = 54f;
			var newIndex = EditorGUILayout.Popup(label, index, Vector3DirectionNames);
			EditorGUIUtility.fieldWidth = prevFieldWidth;
			
			
			EditorGUILayout.PropertyField(vector3Property, GUIContent.none);

			
			if (GUILayout.Button(BasicEditorStyles.GetGuiContent("1", "Normalize"), EditorStyles.miniButton,GUILayout.Width(24f)))
			{
				vector3Property.vector3Value = vector3Property.vector3Value.normalized;
				GUI.FocusControl("");
			}
			
			
			EditorGUILayout.EndHorizontal();
			

			if (index == newIndex) return;

			vector3Property.vector3Value = Vector3DirectionValues[newIndex];
		}
		
		//***** OBJECT *****\\

		public static void ObjectFieldWithAddComponentButton<TComponent>(SerializedProperty targetProperty,
			MonoBehaviour target, bool fieldNotNull = true)
			where TComponent : Component
		{
			if (targetProperty.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUILayout.PropertyField(targetProperty);
				return;
			}
			
			EditorGUILayout.BeginHorizontal();

			if (fieldNotNull) { PropertyFieldNotNull(targetProperty); }
			else              { PropertyFieldCanBeNull(targetProperty); }

			var prevGuiEnabled = GUI.enabled;

			var component = target.GetComponent<TComponent>();
			
			var buttonLabel = component ? "GET" : "ADD";
			
			GUI.enabled = prevGuiEnabled && !targetProperty.objectReferenceValue;
			if (GUILayout.Button(buttonLabel, GUILayout.Width(40f)))
			{
				SetObjectReferenceValue(targetProperty, target,
					component 
						? component 
						: Undo.AddComponent(target.gameObject, typeof(TComponent)));
			}

			GUI.enabled = prevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();
		}


		/// <summary>
		/// Draws a component field with an Add button to the right of it.
		/// The add button brings up a list of components that derive from componenetTypeBase
		/// The selected component is added to the gameobject and assigned to the component field
		/// </summary>
		/// <param name="targetProperty"></param>
		/// <param name="target"></param>
		/// <param name="fieldNotNull"></param>
		/// <typeparam name="TComponent"></typeparam>
		public static void ObjectFieldWithAddDerivedComponentButton<TComponent>(SerializedProperty targetProperty, MonoBehaviour target, bool fieldNotNull = true)
			where TComponent : Component
		{
			if (targetProperty.propertyType != SerializedPropertyType.ObjectReference)
			{
				EditorGUILayout.PropertyField(targetProperty);
				return;
			}
			
			EditorGUILayout.BeginHorizontal();

			if (fieldNotNull) { PropertyFieldNotNull(targetProperty); }
			else              { PropertyFieldCanBeNull(targetProperty); }

			var prevGuiEnabled = GUI.enabled;

			var component = target.GetComponent<TComponent>();
			
			var buttonLabel = component ? "GET" : "ADD";
			
			GUI.enabled = prevGuiEnabled && !targetProperty.objectReferenceValue;
			if (GUILayout.Button(buttonLabel, GUILayout.Width(40f)))
			{
				if (component)
				{
					SetObjectReferenceValue(targetProperty, target, component);
				}
				else
				{
					BasicEditorContextMenus.DerivedTypeSelectorContextMenu(
						typeof(TComponent),
						type =>
						{
							SetObjectReferenceValue(targetProperty, target, Undo.AddComponent(target.gameObject, type));
						}
					);
				}
			}

			GUI.enabled = prevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();
		}

		private static void SetObjectReferenceValue(SerializedProperty property, Object target, Object objectReferenceValue)
		{
			var serializedObject = new SerializedObject(target);
			var targetComponent = serializedObject.FindProperty(property.name);
					
			serializedObject.Update();
			targetComponent.objectReferenceValue = objectReferenceValue;
			serializedObject.ApplyModifiedProperties();
		}
		
	}
}