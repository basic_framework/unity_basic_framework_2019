﻿using System;
using System.Reflection;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
    public static class BasicEditorContextMenus 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        public static void DerivedTypeSelectorContextMenu(Type baseType, Action<Type> typeSelectedCallback)
        {
            var derivedTypes = ReflectionUtility.GetDerivedTypes(baseType, Assembly.GetAssembly(baseType));

            TypeSelectorContextMenu(derivedTypes, typeSelectedCallback);
        }

        public static void TypeSelectorContextMenu(Type[] types, Action<Type> typeSelectedCallback)
        {
            var genericMenu = new GenericMenu();

            foreach (var type in types)
            {
                var t = type;
					
                genericMenu.AddItem(
                    new GUIContent(t.Name.CamelCaseToHumanReadable()),
                    false,
                    () =>
                    {
                        if (typeSelectedCallback == null) return;
                        typeSelectedCallback(t);
                    }
                );
            }
				
            genericMenu.ShowAsContext();
        }
        
    }
}