using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace BasicFramework.Core.Utility.Editor
{
	public static class BasicEditorUtility 
	{
		// TODO: Make array methods for different value types
		
		// **************************************** VARIABLES ****************************************\\
		
		
		public const string MENU_ITEM_ROOT = "Tools/Basic Framework/";

	
		// ****************************************  METHODS  ****************************************\\

		public static SerializedProperty GetChildProperty(SerializedProperty parent, string name)
		{
			var child = parent.Copy();
			child.Next(true);
			do
			{
				if (child.name == name) return child;
			}
			while (child.Next(false));
			return null;
		}

		/// <summary>Checks if the object is reference somewhere in the scene</summary>
		/// <param name="obj">The object to look for in the scene</param>
		/// <returns>If the object is referenced in the scene</returns>
		public static bool ReferencedInScene(Object obj)
		{
			return FindReferenceInScene(obj) != null;
		}

		/// <summary>
		/// Gets the first reference to the object in a scene.
		/// If there is no reference returns null 
		/// </summary>
		/// <param name="obj">The object to look for in the scene</param>
		/// <param name="printDebug">Should debug messages be printed</param>
		/// <returns>The first reference to the object if a reference exists, otherwise null</returns>
		public static Object FindReferenceInScene(Object obj, bool includeInactive = true)
		{
			var allComponents = FindReferencesInScene(obj, includeInactive);
			return allComponents.Length == 0 ? null : allComponents[0];
		}
		
		/// <summary>
		/// Gets all references to the object in a scene.
		/// If there are no references an empty array is returned
		/// </summary>
		/// <param name="obj">The object to look for in the scene</param>
		/// <param name="printDebug">Should debug messages be printed</param>
		/// <returns>An array of references to the object. If there are no references the array will be empty</returns>
		public static Object[] FindReferencesInScene(Object obj, bool includeInactive = true)
		{
			var referencedBy = new List<Object>();
			var loadedGameObjects = GetAllLoadedGameObjects(includeInactive);

			foreach (var go in loadedGameObjects)
			{
				
				// TODO: Check removing this does not create an issue 
//				if (PrefabUtility.GetPrefabType(go) == PrefabType.PrefabInstance)
//				{
//					if (PrefabUtility.GetPrefabParent(go) == obj)
//					{
////					Debug.Log(string.Format("referenced by {0}, {1}", go.name, go.GetType()), go);
//						referencedBy.Add(go);
//					}
//				}
 
				var components = go.GetComponents<Component>();
				foreach (var c in components)
				{
					if (!c) continue;
 
					var so = new SerializedObject(c);
					var sp = so.GetIterator();

					while (sp.NextVisible(true))
					{
						if(sp.propertyType != SerializedPropertyType.ObjectReference) continue;
						if(sp.objectReferenceValue != obj) continue;
						
						//Debug.Log(String.Format("referenced by {0}, {1}", c.name, c.GetType()), c);
						referencedBy.Add(c);
					}
				}
			}

//			if (referencedBy.Count == 0)
//			{
//				Debug.Log(String.Format("No references in scene to {0}", obj.ToString()));
//			}

			return referencedBy.ToArray();
		}

		public static GameObject[] GetAllLoadedGameObjects(bool includeInactive = true)
		{
			if (!includeInactive)
			{
				return Object.FindObjectsOfType<GameObject>();
			}

			
			var transforms = new List<Transform>();
			
			for(int i = 0; i < SceneManager.sceneCount; i++)
			{
				var scene = SceneManager.GetSceneAt(i);
				if (!scene.isLoaded) continue;
				
				foreach (var gameObject in scene.GetRootGameObjects())
				{
					transforms.AddRange(gameObject.GetComponentsInChildren<Transform>(true));
				}
			}

			var loadedGameObjects = new GameObject[transforms.Count];

			for (int i = 0; i < loadedGameObjects.Length; i++)
			{
				loadedGameObjects[i] = transforms[i].gameObject;
			}
			
//			Debug.Log(loadedGameObjects.Length);
			
			return loadedGameObjects;
		}
		

		/// Use this method to get all loaded objects of some type, including inactive objects. 
		/// This is an alternative to Resources.FindObjectsOfTypeAll (returns project assets, including prefabs), and GameObject.FindObjectsOfTypeAll (deprecated).
		public static T[] FindObjectsOfTypeAll<T>(bool includeInactive = true)
		{
			if (typeof(T) == typeof(GameObject))
			{
				return GetAllLoadedGameObjects(includeInactive) as T[];
			}
			
			var results = new List<T>();
			for(int i = 0; i< SceneManager.sceneCount; i++)
			{
				var s = SceneManager.GetSceneAt(i);
				if (!s.isLoaded) continue;
				
				var allGameObjects = s.GetRootGameObjects();

				foreach (var go in allGameObjects)
				{
					results.AddRange(go.GetComponentsInChildren<T>(includeInactive));
				}
			}
			return results.ToArray();
		}


		//***** SerializedProperty *****\\

		public static bool SerializedPropertyValuesEqual(SerializedProperty leftProperty,
			SerializedProperty rightProperty)
		{

			if (leftProperty.propertyType != rightProperty.propertyType)
			{
				return false;
			}
			
			switch (leftProperty.propertyType)
			{
				case SerializedPropertyType.Integer: 	
					return leftProperty.intValue == rightProperty.intValue;
				
				case SerializedPropertyType.Float: 	
					return Mathf.Abs(leftProperty.floatValue - rightProperty.floatValue) < Mathf.Epsilon;

				case SerializedPropertyType.Boolean: 	
					return leftProperty.boolValue == rightProperty.boolValue;
				
				case SerializedPropertyType.String: 	
					return leftProperty.stringValue == rightProperty.stringValue;
				
				case SerializedPropertyType.Enum: 		
					return leftProperty.enumValueIndex == rightProperty.enumValueIndex;
				
				case SerializedPropertyType.ObjectReference:
					return leftProperty.objectReferenceValue == rightProperty.objectReferenceValue;
				
				case SerializedPropertyType.Vector2: 	
					return leftProperty.vector2Value == rightProperty.vector2Value;
				
				case SerializedPropertyType.Vector3: 	
					return leftProperty.vector3Value == rightProperty.vector3Value;
				
				case SerializedPropertyType.Quaternion: 	
					return leftProperty.quaternionValue == rightProperty.quaternionValue;
				
				case SerializedPropertyType.Color: 	
					return leftProperty.colorValue == rightProperty.colorValue;
				
				case SerializedPropertyType.Bounds: 	
					return leftProperty.boundsValue == rightProperty.boundsValue;
				
				case SerializedPropertyType.Rect: 	
					return leftProperty.rectValue == rightProperty.rectValue;
			}
			
			return false;
		}

		public static bool CopySerializedPropertyValue(SerializedProperty source, SerializedProperty destination)
		{
			if (source.propertyType != destination.propertyType)
			{
				return false;
			}
			
			switch (source.propertyType)
			{
				case SerializedPropertyType.Integer: 	
					destination.intValue = source.intValue;
					return true;
					
				
				case SerializedPropertyType.Float: 	
					destination.floatValue = source.floatValue;
					return true;

				case SerializedPropertyType.Boolean: 	
					destination.boolValue = source.boolValue;
					return true;
				
				case SerializedPropertyType.String: 	
					destination.stringValue = source.stringValue;
					return true;
				
				case SerializedPropertyType.Enum: 		
					destination.enumValueIndex = source.enumValueIndex;
					return true;
				
				case SerializedPropertyType.ObjectReference:
					destination.objectReferenceValue = source.objectReferenceValue;
					return true;
				
				case SerializedPropertyType.Vector2: 	
					destination.vector2Value = source.vector2Value;
					return true;
				
				case SerializedPropertyType.Vector3: 	
					destination.vector3Value = source.vector3Value;
					return true;
				
				case SerializedPropertyType.Quaternion: 	
					destination.quaternionValue = source.quaternionValue;
					return true;
				
				case SerializedPropertyType.Color: 	
					destination.colorValue = source.colorValue;
					return true;
				
				case SerializedPropertyType.Bounds: 	
					destination.boundsValue = source.boundsValue;
					return true;

				case SerializedPropertyType.Rect: 	
					destination.rectValue = source.rectValue;
					return true;
			}
			
			return false;
		}

		public static float GetPropertyExpandedHeight(SerializedProperty property)
		{
			var prevExpanded = property.isExpanded;
			property.isExpanded = true;
			var propExpandedHeight = EditorGUI.GetPropertyHeight(property);
			property.isExpanded = prevExpanded;
			return propExpandedHeight;
		}
		
	}

}