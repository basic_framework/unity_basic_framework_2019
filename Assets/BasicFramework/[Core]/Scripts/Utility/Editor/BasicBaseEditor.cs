﻿using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BasicFramework.Core.Utility.Editor
{
	public abstract class BasicBaseEditor<T> : UnityEditor.Editor 
		where T : Object
	{
	
		// **************************************** VARIABLES ****************************************\\

		//public const float MIN_MOVE_DISTANCE = 0.00001f;
		//public const float MIN_ROTATE_ANGLE = 0.1f;
		
		private bool _prevGuiEnabled;
		protected bool PrevGuiEnabled => _prevGuiEnabled;
		
		protected T TypedTarget => _typedTargets[0];

		private T[] _typedTargets;
		protected T[] TypedTargets => _typedTargets;

		private bool _targetingMultiple;
		protected bool TargetingMultiple => _targetingMultiple;

		private bool _firstDraw;

		private readonly GUIContent _tempGuiContent = new GUIContent();
		
		private readonly GUIContent[] _guiContentBuffer = 
		{
			new GUIContent(),
			new GUIContent(),
			new GUIContent(), 
		};

		private int _guiContentBufferIndex;
		
		protected virtual bool UpdateConstantlyInPlayMode => false;
		protected virtual bool HasDebugSection => false;
		
		protected virtual bool DebugSectionIsExpandable => false;
		protected virtual bool DebugSectionIsExpanded{get; set;}

		private SerializedObject _sceneGuiSerializedObject;

		protected bool RaiseEditorUpdate;
		

		// ****************************************  METHODS  ****************************************\\
	
		protected virtual void OnEnable()
		{
			_typedTargets = new T[targets.Length];

			for (var i = 0; i < targets.Length; i++)
			{
				var objectTarget = targets[i];
				_typedTargets[i] = objectTarget as T;
			}

			_targetingMultiple = targets.Length != 1;

			_firstDraw = false;
			
			//_sceneGuiSerializedObject = new SerializedObject(target);
		}
		

		public sealed override void OnInspectorGUI()
		{
			BasicEditorGuiLayout.DrawScriptField(serializedObject);

			_prevGuiEnabled = GUI.enabled;
			
			serializedObject.Update();

			if (!_firstDraw)
			{
				_firstDraw = true;
				OnFirstDrawInspectorFields();
			}
			
			DrawInspectorFields();
			
			serializedObject.ApplyModifiedProperties();

			AfterApplyingModifiedProperties();
			
			if (RaiseEditorUpdate)
			{
				RaiseEditorUpdate = false;
				OnRaiseEditorUpdate();
			}

			_sceneGuiSerializedObject = serializedObject;

			GUI.enabled = _prevGuiEnabled;
		}
		
		// Raised after modified properties have been applied
		protected virtual void AfterApplyingModifiedProperties()
		{
			
			
			if (!Application.isPlaying || !UpdateConstantlyInPlayMode) return;
			EditorUtility.SetDirty(target);
		}
		
		
		//***** Draw *****\\
		
		// Raised the first time OnInspectorGui is called
		protected virtual void OnFirstDrawInspectorFields(){}

		//protected virtual void DrawInspectorFields()
		private void DrawInspectorFields()
		{
			DrawTopOfInspectorMessage();
			
			if (HasDebugSection)
			{
				DrawDebugFieldsBase();
				Space();
			}
			
			DrawPropertyFields();
			
			Space();
			DrawEventFields();
			
			Space();
			DrawBottomOfInspectorFields();
		}

		

		private void DrawDebugFieldsBase()
		{
			if (DebugSectionIsExpandable)
			{
				DebugSectionIsExpanded =
					EditorGUILayout.Foldout(DebugSectionIsExpanded, "Debug", BasicEditorStyles.BoldFoldout);

				if (!DebugSectionIsExpanded) return;
			}
			else
			{
				SectionHeader("Debug");
			}
			
			DrawDebugFields();
		}

		/// <summary>Use this if you want to display a message at the top of the editor</summary>
		protected virtual void DrawTopOfInspectorMessage(){}
		
		/// <summary>Use this to draw any debug fields. You need to override HasDebugSection and return true to display it</summary>
		protected virtual void DrawDebugFields(){}

		/// <summary>This is the main method to ovveride, if you don't want anything else just ovveride this</summary>
		protected virtual void DrawPropertyFields(){}

		/// <summary>Use this callback to draw event fields at the bottom of the editor</summary>
		protected virtual void DrawEventFields(){}
		
		protected virtual void DrawBottomOfInspectorFields(){}
		
		protected new void DrawDefaultInspector()
		{
			var targetSerializedObject = new SerializedObject(target);
			
			targetSerializedObject.Update();
			
			var iterator = targetSerializedObject.GetIterator();
			
			// Skip displaying the script field
			iterator.NextVisible(true);
			
			for (var enterChildren = true; iterator.NextVisible(enterChildren); enterChildren = false)
			{
				EditorGUILayout.PropertyField(iterator, true);
			}
			
			targetSerializedObject.ApplyModifiedProperties();
		}
		
		
		//***** Helpers *****\\

		protected void SectionHeader(string heading)
		{
			EditorGUILayout.LabelField(heading, EditorStyles.boldLabel);
		}
		
		protected void SectionHeader(GUIContent heading)
		{
			EditorGUILayout.LabelField(heading, EditorStyles.boldLabel);
		}

		protected void Space(float pixelHeight = 4f)
		{
			GUILayout.Space(pixelHeight);
		}
		
		protected GUIContent GetGuiContent(SerializedProperty property)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.tooltip = property.tooltip;
			_tempGuiContent.image = null;

			return _tempGuiContent;
		}
		
		protected GUIContent GetGuiContent(string text, string tooltip = "", Texture image = null)
		{
			var guiContent = _guiContentBuffer[_guiContentBufferIndex];
			_guiContentBufferIndex++;
			if (_guiContentBufferIndex >= _guiContentBuffer.Length) _guiContentBufferIndex = 0;
			
			guiContent.text = text;
			guiContent.tooltip = tooltip;
			guiContent.image = image;

			return guiContent;
		}

		// Gets called after applying modified properties
		protected virtual void OnRaiseEditorUpdate(){}

		
		protected virtual void OnSceneGUI()
		{
			if(_sceneGuiSerializedObject == null) return;
			
			_prevGuiEnabled = GUI.enabled;

//			if (_sceneGuiSerializedObject == null)
//			{
//				_sceneGuiSerializedObject = new SerializedObject(target);
//			}
			
			//var mySerializedObject = new SerializedObject(target);
			//mySerializedObject.Update();
			//serializedObject.Update();
			
			
			_sceneGuiSerializedObject.Update();
			
			DrawSceneGUI(_sceneGuiSerializedObject);
			
			//mySerializedObject.ApplyModifiedProperties();
			//serializedObject.ApplyModifiedProperties();
			_sceneGuiSerializedObject.ApplyModifiedProperties();
			
			GUI.enabled = _prevGuiEnabled;
		}

		protected virtual void DrawSceneGUI(SerializedObject sceneGuiSerializedObject){}
		
		
	}
}