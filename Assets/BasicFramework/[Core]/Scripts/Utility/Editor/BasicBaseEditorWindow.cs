﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	public abstract class BasicBaseEditorWindow : EditorWindow
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private SerializedObject _serializedObject;
		public SerializedObject SerializedObject => _serializedObject;

		private bool _prevGuiEnabled;
		public bool PrevGuiEnabled => _prevGuiEnabled;
		
		private bool _firstDraw;

		private readonly GUIContent _tempGuiContent = new GUIContent();
		
	
		// ****************************************  METHODS  ****************************************\\

		// Raised the first time OnGUI is called
		protected virtual void OnFirstDrawCall(){}
		
		// Raised after modified properties have been applied
		protected virtual void AfterApplyingModifiedProperties(){}
		
		// Use this instead of OnGUI
		protected abstract void DrawGUI();
		
		
		protected virtual void OnEnable()
		{
			_serializedObject = new SerializedObject(this);

			_firstDraw = false;
		}

		private void OnGUI()
		{
			_prevGuiEnabled = GUI.enabled;
			
			_serializedObject.Update();

			if (!_firstDraw)
			{
				_firstDraw = true;
				OnFirstDrawCall();
			}
			
			DrawGUI();
			
			_serializedObject.ApplyModifiedProperties();

			AfterApplyingModifiedProperties();

			GUI.enabled = _prevGuiEnabled;
		}
		
		protected void SectionHeader(string heading)
		{
			EditorGUILayout.LabelField(heading, EditorStyles.boldLabel);
		}

		protected void SectionHeader(GUIContent heading)
		{
			EditorGUILayout.LabelField(heading, EditorStyles.boldLabel);
		}

		protected void Space(float pixelHeight = 4f)
		{
			GUILayout.Space(pixelHeight);
		}
		
		protected GUIContent GetGuiContent(SerializedProperty property)
		{
			_tempGuiContent.text = property.displayName;
			_tempGuiContent.tooltip = property.tooltip;
			_tempGuiContent.image = null;

			return _tempGuiContent;
		}
		
		protected GUIContent GetGuiContent(string text, string tooltip = "", Texture image = null)
		{
			_tempGuiContent.text = text;
			_tempGuiContent.tooltip = tooltip;
			_tempGuiContent.image = image;

			return _tempGuiContent;
		}
		
	}
}