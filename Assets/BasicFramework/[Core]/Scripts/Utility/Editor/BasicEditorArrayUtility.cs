﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	public static class BasicEditorArrayUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		public static bool ArrayContains(SerializedProperty list, Object obj)
		{
			if (!list.isArray || list.arraySize == 0) return false;
			if (list.GetArrayElementAtIndex(0).propertyType != SerializedPropertyType.ObjectReference) return false;
			
			for (var i = 0; i < list.arraySize; i++)
			{
				if (list.GetArrayElementAtIndex(i).objectReferenceValue == obj)
				{
					return true;
				}
			}

			return false;
		}

		public static SerializedProperty AddArrayElement(SerializedProperty list)
		{
			if (!list.isArray) return null;
			
			list.InsertArrayElementAtIndex(list.arraySize);

			var element = list.GetArrayElementAtIndex(list.arraySize - 1);

			if (element.propertyType == SerializedPropertyType.ObjectReference)
			{
				element.objectReferenceValue = null;
			}

			return element;
		}

		public static SerializedProperty AddArrayElement(SerializedProperty list, Object objectReferenceValue)
		{
			if (!list.isArray) return null;
			
			list.InsertArrayElementAtIndex(list.arraySize);

			var element = list.GetArrayElementAtIndex(list.arraySize - 1);

			//Debug.Log(element.propertyType);
			
			if (element.propertyType == SerializedPropertyType.ObjectReference)
			{
				element.objectReferenceValue = objectReferenceValue;
			}

			return element;
		}

		public static void SetArraySize(SerializedProperty list, int arraySize)
		{
			if (!list.isArray) return;

			var difference = arraySize - list.arraySize;

			var add = difference > 0;
			
			for (int i = 0; i < Mathf.Abs(difference); i++)
			{
				if (add)
				{
					AddArrayElement(list);
				}
				else
				{
					RemoveArrayElement(list, list.arraySize - 1);
				}
			}		
		}

		public static void RemoveDuplicateReferences(SerializedProperty list)
		{
			if (!list.isArray || list.arraySize == 0) return;
			if (list.GetArrayElementAtIndex(0).propertyType != SerializedPropertyType.ObjectReference) return;

			var indexToRemove = new List<int>();

			// Find all the duplicate indexes
			for (int i = 0; i < list.arraySize; i++)
			{
				if(indexToRemove.Contains(i)) continue;
				
				for (int j = i + 1; j < list.arraySize; j++)
				{
					if (list.GetArrayElementAtIndex(i).objectReferenceValue 
					    == list.GetArrayElementAtIndex(j).objectReferenceValue)
					{
						indexToRemove.Add(j);
					}
				}
			}

			// Remove all the duplicate indexes
			for (int i = list.arraySize - 1; i >= 0; i--)
			{
				if (indexToRemove.Contains(i))
				{
					RemoveArrayElement(list, i);
				}
			}
			
		}

		public static void RemoveNullReferences(SerializedProperty list)
		{
			if (!list.isArray) return;

			for (int i = list.arraySize - 1; i >= 0; i--)
			{
				if (list.GetArrayElementAtIndex(i).objectReferenceValue == null)
				{
					RemoveArrayElement(list, i);
				}
			}
		}

		public static void RemoveArrayElement(SerializedProperty list, int index)
		{
			if (!list.isArray) return;
			
			if (list.arraySize == 0 || index < 0 || index >= list.arraySize) return;
			
			var oldSize = list.arraySize;
			
			list.DeleteArrayElementAtIndex(index);

			if (list.arraySize == oldSize)
			{
				list.DeleteArrayElementAtIndex(index);
			}
		}
		
		public static void RemoveAllArrayElements(SerializedProperty list)
		{
			if (!list.isArray) return;

			for (int i = list.arraySize - 1; i >= 0 ; i--)
			{
				RemoveArrayElement(list, i);
			}
		}
		
	}
}