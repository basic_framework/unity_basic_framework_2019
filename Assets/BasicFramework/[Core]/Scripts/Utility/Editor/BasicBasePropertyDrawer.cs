using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
    public abstract class BasicBasePropertyDrawer : PropertyDrawer
    {
	
        // **************************************** VARIABLES ****************************************\\

        public const float INDENT = 14f;
        public const float BUFFER_WIDTH = 4f;
        public const float BUFFER_HEIGHT = 2f;
        public static float SingleLineHeight => EditorGUIUtility.singleLineHeight;
		
        protected readonly GUIContent TempGUIContent = new GUIContent();
        protected Rect TempRect;
		
        private bool _prevGuiEnabled;
        public bool PrevGuiEnabled => _prevGuiEnabled;


        // ****************************************  METHODS  ****************************************\\

        public sealed override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            _prevGuiEnabled = GUI.enabled;
            TempRect = new Rect(position);
            DrawGUI(position, property, label);
        }
		
        protected abstract void DrawGUI(Rect position, SerializedProperty property, GUIContent label);

        protected void DrawBaseGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            base.OnGUI(position, property, label);
        }
        
        protected GUIContent GetGuiContent(SerializedProperty property)
        {
            TempGUIContent.text = property.displayName;
            TempGUIContent.tooltip = property.tooltip;
            TempGUIContent.image = null;

            return TempGUIContent;
        }
		
        protected GUIContent GetGuiContent(string text, string tooltip = "", Texture image = null)
        {
            TempGUIContent.text = text;
            TempGUIContent.tooltip = tooltip;
            TempGUIContent.image = image;

            return TempGUIContent;
        }

        protected float GetPropertyHeight(SerializedProperty property, bool includeChildren)
        {
            return EditorGUI.GetPropertyHeight(property, includeChildren);
        }

        public float GetIndentPixels()
        {
            // This may not be accurate
            return EditorGUI.indentLevel * INDENT;
        }
    }
}