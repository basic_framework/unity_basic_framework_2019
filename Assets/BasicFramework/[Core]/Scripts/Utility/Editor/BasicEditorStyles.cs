﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	public static class BasicEditorStyles 
	{
	
		// **************************************** VARIABLES ****************************************\\

		public static Color IsNullFieldColor 			=> Colors.LightRed;
		public static Color IsAndCanBeNullFieldColor 	=> Colors.LightYellow;
		public static Color NotNullFieldColor 			=> Colors.LightGreen;

		private static GUIStyle _boldFoldout;
		public static GUIStyle BoldFoldout
		{
			get
			{
				if (_boldFoldout == null)
				{
					_boldFoldout = new GUIStyle(EditorStyles.foldout)
					{
						fontStyle = FontStyle.Bold
					};
				}
				return _boldFoldout;
			}
		}

		public static readonly GUIContent TempGuiContent = new GUIContent();
		

		// ****************************************  METHODS  ****************************************\\

		public static void ChangeGuiStyleTextColor(GUIStyle style, Color color)
		{
			style.normal.textColor = color;
			style.focused.textColor = color;
			style.active.textColor = color;
			style.hover.textColor = color;
			
			style.onNormal.textColor = color;
			style.onFocused.textColor = color;
			style.onActive.textColor = color;
			style.onHover.textColor = color;
		}
		
		
		// ****************************************  CLASSES  ****************************************\\
		
		public static class Colors
		{
			public static readonly Color LightBlue = new Color(0f, 0f, 0.85f);
			public static readonly Color MediumBlue = new Color(0f, 0f, 0.6f);
			public static readonly Color DarkBlue = new Color(0f, 0f, 0.4f);
			
			public static readonly Color LightGreen = new Color(0f, 0.85f, 0f);
			public static readonly Color MediumGreen = new Color(0f, 0.6f, 0f);
			public static readonly Color DarkGreen = new Color(0f, 0.4f, 0f);
			
			public static readonly Color LightRed = new Color(0.85f, 0.0f, 0.0f);
			public static readonly Color MediumRed = new Color(0.6f, 0.0f, 0.0f);
			public static readonly Color DarkRed = new Color(0.4f, 0.0f, 0.0f);
			
			public static readonly Color LightYellow = new Color(0.85f, 0.85f, 0f);
			public static readonly Color MediumYellow = new Color(0.6f, 0.6f, 0f);
			public static readonly Color DarkYellow = new Color(0.4f, 0.4f, 0f);
		}

		public static GUIContent GetGuiContent(string text, string tooltip = "", Texture image = null)
		{
			TempGuiContent.text = text;
			TempGuiContent.tooltip = tooltip;
			TempGuiContent.image = image;

			return TempGuiContent;
		}
		
	}
}