using System.Collections.Generic;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
    public static class MonobehaviourUtility 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        public static T[] FindObjectsOfTypeInScene<T>(bool includeInactive = true)
            where T : MonoBehaviour
        {
            var objects = includeInactive 
                ? Object.FindObjectsOfType<T>() 
                : Resources.FindObjectsOfTypeAll<T>();

            var objectsInScene = new List<T>();

            foreach (var o in objects)
            {
                if(!o.gameObject.ExistsInActiveScenes()) continue;
                objectsInScene.Add(o);
            }

            return objectsInScene.ToArray();
        }
	
    }
}