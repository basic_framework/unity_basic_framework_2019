using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
    public abstract class BasicBaseDecoratorDrawer : DecoratorDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        public const float BUFFER_WIDTH = 4f;
        public const float BUFFER_HEIGHT = 2f;
        public static float SingleLineHeight => EditorGUIUtility.singleLineHeight;
		
        protected readonly GUIContent TempGUIContent = new GUIContent();
        protected Rect TempRect;
		
        private bool _prevGuiEnabled;
        public bool PrevGuiEnabled => _prevGuiEnabled;
		
	
        // ****************************************  METHODS  ****************************************\\
	
        public sealed override void OnGUI(Rect position)
        {
            _prevGuiEnabled = GUI.enabled;
            DrawGUI(position);
        }

        protected abstract void DrawGUI(Rect position);
	
    }
}