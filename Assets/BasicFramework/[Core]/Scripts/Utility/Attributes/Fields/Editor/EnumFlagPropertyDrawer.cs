﻿using System;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(EnumFlagAttribute))]
    public class EnumFlagPropertyDrawer : BasicBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
	        var flagSettings = (EnumFlagAttribute)attribute;
	        var targetEnum = (Enum)Enum.ToObject(fieldInfo.FieldType, property.intValue);

	        label.text = property.displayName;
	        label.tooltip = flagSettings.tooltip;

	        EditorGUI.BeginProperty(position, label, property);
	        var enumNew = EditorGUI.EnumFlagsField(position, label, targetEnum);

	        if (flagSettings.IsShortEnum)
	        {
		        property.intValue = (short)Convert.ChangeType(enumNew, targetEnum.GetType());
	        }
	        else
	        {
		        property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());   
	        }
	        
			
	        EditorGUI.EndProperty();
        }
        
    }
}