﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes.Editor
{
	[CustomPropertyDrawer(typeof(EditTimeOnlyAttribute))]
	public class EditTimeOnlyPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;

			EditorGUI.PropertyField(position, property, label);
			
			GUI.enabled = PrevGuiEnabled;
		}
		
	}
}