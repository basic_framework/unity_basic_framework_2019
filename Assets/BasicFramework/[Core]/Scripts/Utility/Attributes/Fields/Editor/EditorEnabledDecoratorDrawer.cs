using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes.Editor
{
    [CustomPropertyDrawer(typeof(EditorEnabledAttribute))]
    public class EditorEnabledDecoratorDrawer : BasicBaseDecoratorDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        private EditorEnabledAttribute _editorEnabledAttribute => (EditorEnabledAttribute) attribute;

	
        // ****************************************  METHODS  ****************************************\\
		
        protected override void DrawGUI(Rect position)
        {
            GUI.enabled = _editorEnabledAttribute.EnableEditor;
        }

        public override float GetHeight()
        {
            return 0f;
        }
		
    }
}