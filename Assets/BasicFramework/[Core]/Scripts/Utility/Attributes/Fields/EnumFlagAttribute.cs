﻿using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes
{
    public class EnumFlagAttribute : PropertyAttribute 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        public string tooltip;
        public bool IsShortEnum;
	
	
        // ****************************************  METHODS  ****************************************\\
	
        public EnumFlagAttribute() { }

        public EnumFlagAttribute(bool isShortEnum)
        {
	        this.IsShortEnum = isShortEnum;
        }
        
        public EnumFlagAttribute(string tooltip)
        {
            this.tooltip = tooltip;
        }
	
        public EnumFlagAttribute(string tooltip, bool isShortEnum)
        {
	        this.tooltip = tooltip;
	        this.IsShortEnum = isShortEnum;
        }
        
    }
}