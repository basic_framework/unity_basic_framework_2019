using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class EditorEnabledAttribute : PropertyAttribute 
    {
	
        // **************************************** VARIABLES ****************************************\\

        public readonly bool EnableEditor;
		

        // ****************************************  METHODS  ****************************************\\

        public EditorEnabledAttribute(bool enableEditor)
        {
            EnableEditor = enableEditor;
        }

    }
}