﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.Attributes
{
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public class EditTimeOnlyAttribute : PropertyAttribute 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		public EditTimeOnlyAttribute()
		{
			
		}
	
	}
}