﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Utility.Tracking
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Utility/Tracking/Basic Observer")]
	public class BasicObserver : TrackingBase 
	{
		
		// TODO: Allow users to select the local direction that is pointed at the object 
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The world space direction that will be used as up for the observer")]
		[SerializeField] private Vector3 _upDirection = Vector3.up;
		
		[Tooltip("Reverse the direction of the observer")]
		[SerializeField] private bool _reverseObserveDirection = default;

		[SerializeField] private NormalizedFloat _slerpSmoothRate = new NormalizedFloat(0.15f);
		

		public Vector3 UpDirection
		{
			get => _upDirection;
			set => _upDirection = value;
		}

		public bool ReverseObserveDirection
		{
			get => _reverseObserveDirection;
			set => _reverseObserveDirection = value;
		}

		public NormalizedFloat SlerpSmoothRate
		{
			get => _slerpSmoothRate;
			set => _slerpSmoothRate = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		protected override void UpdateTracker(PoseSource tracker, BasicPose targetOffsetPose)
		{
			var poseTracker = tracker.Pose;
//			var poseTargetOffset = GetOffsetTargetPose();

			var desiredLookDirection = (targetOffsetPose.Position - poseTracker.Position).normalized * (_reverseObserveDirection ? -1 : 1);
			var desiredLookRotation = MathUtility.Direction2Quaternion(desiredLookDirection, _upDirection);
			
			var smoothedRotation = Application.isPlaying
				? Quaternion.Slerp(poseTracker.Rotation, desiredLookRotation, _slerpSmoothRate.Value)
				: desiredLookRotation;

			tracker.Rotation = smoothedRotation;
		}

	}
}