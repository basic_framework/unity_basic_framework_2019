﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.Utility.Tracking.Editor
{
	[CustomEditor(typeof(BasicObserver))]
	public class BasicObserverEditor : TrackingBaseEditor<BasicObserver>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasOffsetRotation => false;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var upDirection = serializedObject.FindProperty("_upDirection");
			var reverseObserveDirection = serializedObject.FindProperty("_reverseObserveDirection");
			var slerpSmoothRate = serializedObject.FindProperty("_slerpSmoothRate");
			
			Space();
			SectionHeader($"{nameof(BasicObserver).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(reverseObserveDirection);
			BasicEditorGuiLayout.Vector3DirectionDropdown(upDirection);
			EditorGUILayout.PropertyField(slerpSmoothRate);
		}

	}
}