﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.Core.Utility.Tracking.Editor
{
	[CustomEditor(typeof(BasicFollower))]
	public class BasicFollowerEditor : TrackingBaseEditor<BasicFollower>
	{
	
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var followProperties = serializedObject.FindProperty("_followProperties");
			
			Space();
			SectionHeader($"{nameof(BasicFollower).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(followProperties);
		}

	}
}