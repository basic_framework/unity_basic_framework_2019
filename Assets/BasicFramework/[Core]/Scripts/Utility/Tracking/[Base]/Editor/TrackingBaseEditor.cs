﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Tracking.Editor
{
	public abstract class TrackingBaseEditor<TTrackingBase> : BasicBaseEditor<TTrackingBase>
		where TTrackingBase : TrackingBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		protected virtual bool HasOffsetPosition => true;
		protected virtual bool HasOffsetRotation => true;

		private bool _editorUpdate;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var editorTimeUpdate = serializedObject.FindProperty("_editorTimeUpdate");
			var editorDrawGizmoTarget = serializedObject.FindProperty("_editorDrawGizmoTarget");
			var editorGizmoSize = serializedObject.FindProperty("_editorGizmoSize");
			
			EditorGUILayout.PropertyField(editorTimeUpdate);
			EditorGUILayout.PropertyField(editorDrawGizmoTarget);
			EditorGUILayout.PropertyField(editorGizmoSize);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var setupCallbackFlags = serializedObject.FindProperty("_setupCallbackFlags");
			var updateCallbackFlags = serializedObject.FindProperty("_updateCallbackFlags");
			
			var trackerSource = serializedObject.FindProperty("_tracker");
			var targetSource = serializedObject.FindProperty("_target");
			
			var targetOffsetPositionWorldSpace = serializedObject.FindProperty("_targetOffsetPositionWorldSpace");
			var targetOffsetPosition = serializedObject.FindProperty("_targetOffsetPosition");
			var targetOffsetRotation = serializedObject.FindProperty("_targetOffsetRotation");
			
			Space();
			SectionHeader($"{nameof(TrackingBase).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(setupCallbackFlags);
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(updateCallbackFlags);
			if (EditorGUI.EndChangeCheck()) { _editorUpdate = true; }
			
			Space();
			EditorGUILayout.PropertyField(trackerSource);
			EditorGUILayout.PropertyField(targetSource);
			
			Space();
			GUI.enabled = PrevGuiEnabled && HasOffsetPosition;
			EditorGUILayout.PropertyField(targetOffsetPositionWorldSpace);
			EditorGUILayout.PropertyField(targetOffsetPosition);

			GUI.enabled = PrevGuiEnabled && HasOffsetRotation;
			EditorGUILayout.PropertyField(targetOffsetRotation);
			
			GUI.enabled = PrevGuiEnabled;

			var trackerPoseSource = TypedTarget.Tracker;
			var trackerTransform = trackerPoseSource.Transform;
			var trackerName = trackerTransform == null ? "NULL" : trackerTransform.name;

//			GUI.enabled = PrevGuiEnabled && !Application.isPlaying && trackerTransform != null;
//
//			if (GUILayout.Button($"Save Tracker \"{trackerName}\" Pose"))
//			{
//				Undo.RecordObject(trackerTransform, $"Updated \"{trackerName}\" position");
//				TypedTarget.EditTimeUpdate();
//			}
//			
//			GUI.enabled = PrevGuiEnabled;
		}

		protected override void AfterApplyingModifiedProperties()
		{
			base.AfterApplyingModifiedProperties();

			if (!_editorUpdate) return;
			_editorUpdate = false;
			TypedTarget.EditorUpdate();
		}
	}
}