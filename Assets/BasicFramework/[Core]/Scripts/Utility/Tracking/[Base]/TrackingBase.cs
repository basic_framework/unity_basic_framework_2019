﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.Attributes;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Tracking
{
	[ExecuteAlways]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_BASIC_TRACKING)]
	public abstract class TrackingBase : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		//***** Editor *****\\

//#if UNITY_EDITOR
		[Tooltip("Should the tracker run at edit time")]
		[SerializeField] private bool _editorTimeUpdate = default;
		
		[Tooltip("Draw the target on gizmo selected")]
		[SerializeField] private bool _editorDrawGizmoTarget = true;
		
		[Tooltip("The size of the target gizmo")]
		[SerializeField] private float _editorGizmoSize = 0.1f;
//#endif		
		
		//***** Sources *****\\
		
		[Tooltip("The pose source that is tracking the target")]
		[SerializeField] private PoseSource _tracker = new PoseSource();
		
		[Tooltip("The pose source used for the target (i.e. the one being tracked)")]
		[SerializeField] private PoseSource _target = new PoseSource();
		
		
		//***** Settings *****\\

		[EnumFlag("The setup callbacks to update tracking on")] 
		[SerializeField] private SetupCallbackFlags _setupCallbackFlags = default;

		[EnumFlag("The update callbacks to update tracking on")] 
		[SerializeField] private UpdateCallbackFlags _updateCallbackFlags = UpdateCallbackFlags.Update;
		
		[Tooltip("Should the offset position be applied in world space")]
		[SerializeField] private bool _targetOffsetPositionWorldSpace = default;
		[SerializeField] private Vector3 _targetOffsetPosition = default;
		[SerializeField] private Vector3 _targetOffsetRotation = default;


		public PoseSource Tracker => _tracker;

		public PoseSource Target => _target;

		public UpdateCallbackFlags UpdateCallbackFlags
		{
			get => _updateCallbackFlags;
			set
			{
				if (_updateCallbackFlags == value) return;
				_updateCallbackFlags = value;
				UpdateCallbackFlagsUpdated(value);
			}
		}
		

		public bool TargetOffsetPositionWorldSpace
		{
			get => _targetOffsetPositionWorldSpace;
			set => _targetOffsetPositionWorldSpace = value;
		}

		public Vector3 TargetOffsetPosition
		{
			get => _targetOffsetPosition;
			set => _targetOffsetPosition = value;
		}

		public Vector3 TargetOffsetRotation
		{
			get => _targetOffsetRotation;
			set => _targetOffsetRotation = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
	
		
		//***** abstract/virtual *****\\

		protected abstract void UpdateTracker(PoseSource tracker, BasicPose targetOffsetPose);
		
		
		//***** mono *****\\
		
		protected virtual void Reset()
		{
			_tracker.Transform = transform;
			_tracker.Rigidbody = GetComponent<Rigidbody>();
		}

		protected virtual void Awake()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if ((_setupCallbackFlags & SetupCallbackFlags.Awake) != SetupCallbackFlags.Awake) return;
			UpdateTracker(_tracker, GetOffsetTargetPose());
		}

		protected virtual void OnEnable()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if ((_setupCallbackFlags & SetupCallbackFlags.Enable) != SetupCallbackFlags.Enable) return;
			UpdateTracker(_tracker, GetOffsetTargetPose());
		}
		
		protected virtual void Start()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if ((_setupCallbackFlags & SetupCallbackFlags.Start) == SetupCallbackFlags.Start)
			{
				UpdateTracker(_tracker, GetOffsetTargetPose());
			}

			UpdateCallbackFlagsUpdated(_updateCallbackFlags);
		}

		protected virtual void Update()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				EditTimeUpdate();
				return;
			}
#endif
			
			if ((_updateCallbackFlags & UpdateCallbackFlags.Update) != UpdateCallbackFlags.Update) return;
			UpdateTracker(_tracker, GetOffsetTargetPose());
		}

		protected virtual void LateUpdate()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			
			if ((_updateCallbackFlags & UpdateCallbackFlags.LateUpdate) != UpdateCallbackFlags.LateUpdate) return;
			UpdateTracker(_tracker, GetOffsetTargetPose());
		}
		
		protected virtual void FixedUpdate()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif			
			if ((_updateCallbackFlags & UpdateCallbackFlags.FixedUpdate) != UpdateCallbackFlags.FixedUpdate) return;
			UpdateTracker(_tracker, GetOffsetTargetPose());
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmosSelected()
		{
			if (!_editorDrawGizmoTarget) return;
			if (!_target.IsValid) return;
			
			var poseTargetOffset = GetOffsetTargetPose();

			var colorPrev = Gizmos.color;
			

			var position = poseTargetOffset.Position;
			var rotation = poseTargetOffset.Rotation;
			
			Gizmos.color = new Color(1f, 0f, 1f, 0.5f);
			//Gizmos.DrawWireSphere(position, _editorGizmoSize * 0.5f);
			Gizmos.DrawSphere(position, _editorGizmoSize * 0.5f);
			
			Gizmos.color = new Color(1f, 0f, 0f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.right * _editorGizmoSize);
			
			Gizmos.color = new Color(0f, 1f, 0f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.up * _editorGizmoSize);
			
			Gizmos.color = new Color(0f, 0f, 1f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.forward * _editorGizmoSize);
			
			Gizmos.color = colorPrev;
		}
#endif

		
		//***** property updates *****\\
		
		private void UpdateCallbackFlagsUpdated(UpdateCallbackFlags value)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying) return;
#endif
			enabled = value != 0;
		}
		
		
		//***** functions *****\\
		
		protected BasicPose GetOffsetTargetPose()
		{
			var targetPose = _target.Pose;
			
			targetPose.Position += _targetOffsetPositionWorldSpace
				? _targetOffsetPosition
				: targetPose.Rotation * _targetOffsetPosition;
			
//			if (_targetOffsetPositionWorldSpace)
//			{
//				targetPose.Position += _targetOffsetPosition;
//			}
//			else
//			{
//				var trsMatrix = Matrix4x4.TRS(targetPose.Position, targetPose.Rotation, Vector3.one);
//				targetPose.Position = MathUtility.TransformPoint(trsMatrix, _targetOffsetPosition);
//			}

			targetPose.Rotation *= Quaternion.Euler(_targetOffsetRotation);
			//targetPose.Rotation = MathUtility.TransformQuaternion(targetPose.Rotation, Quaternion.Euler(_targetOffsetRotation));

			return targetPose;
		}
		

		public void SetFollowerSource(Transform followerTransform)
		{
			_tracker.Transform = followerTransform;
			_tracker.SourceType = PoseSourceTypes.Transform;
		}
		
		public void SetFollowerSource(Rigidbody followerRigidbody)
		{
			_tracker.Rigidbody = followerRigidbody;
			_tracker.SourceType = PoseSourceTypes.Rigidbody;
		}
		
		public void SetFollowerSource(BasicPoseSingle followerBasicPoseSingle)
		{
			_tracker.BasicPoseSingle = followerBasicPoseSingle;
			_tracker.SourceType = PoseSourceTypes.BasicPose;
		}
		
		
		public void SetTargetSource(Transform targetTransform)
		{
			_target.Transform = targetTransform;
			_target.SourceType = PoseSourceTypes.Transform;
		}
		
		public void SetTargetSource(Rigidbody targetRigidbody)
		{
			_target.Rigidbody = targetRigidbody;
			_target.SourceType = PoseSourceTypes.Rigidbody;
		}
		
		public void SetTargetSource(BasicPoseSingle targetBasicPoseSingle)
		{
			_target.BasicPoseSingle = targetBasicPoseSingle;
			_target.SourceType = PoseSourceTypes.BasicPose;
		}
		
#if UNITY_EDITOR		
		//***** editor *****\\
		
		public void EditTimeUpdate()
		{
			if (Application.isPlaying) return;
			if (!_editorTimeUpdate) return;

			if (!_tracker.IsValid) return;
			if (!_target.IsValid) return;

			// Not sure if this is actually doing anything
			if (_tracker.SourceType == PoseSourceTypes.Transform)
			{
				Undo.RecordObject(_tracker.Transform, $"Updated \"{_tracker.Transform.name}\" position");
			}

			UpdateTracker(_tracker, GetOffsetTargetPose());
		}
		
		public void EditorUpdate()
		{
			UpdateCallbackFlagsUpdated(_updateCallbackFlags);
		}
#endif
		
	}
}