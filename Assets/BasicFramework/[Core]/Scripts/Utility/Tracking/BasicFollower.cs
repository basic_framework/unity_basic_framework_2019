﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.Core.Utility.Tracking
{
	[AddComponentMenu("Basic Framework/Core/Utility/Tracking/Basic Follower")]
	public class BasicFollower : TrackingBase 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		[EnumFlag("The transform properties to follow")]
		[SerializeField] private PoseFlags _followProperties = (PoseFlags) (~0);
		
		
		public PoseFlags FollowProperties
		{
			get => _followProperties;
			set => _followProperties = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		protected override void UpdateTracker(PoseSource tracker, BasicPose targetOffsetPose)
		{
			if (_followProperties == 0) return;

//			var poseTargetOffset = GetOffsetTargetPose();
			
			var newPose = tracker.Pose;
			
			// follow position?
			if ((_followProperties & PoseFlags.Position) == PoseFlags.Position)
			{
				newPose.Position = targetOffsetPose.Position;
			}
			
			// follow rotation?
			if ((_followProperties & PoseFlags.Rotation) == PoseFlags.Rotation)
			{
				newPose.Rotation = targetOffsetPose.Rotation;
			}

			tracker.Pose = newPose;
		}
		
	}
}