using UnityEngine;

namespace BasicFramework.Core.Utility.Screen
{
	public static class ScreenUtility 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		private static float _fallbackDpi = 97;
		
		/// <summary>The screen dots per inch that will be used if actual dpi is unavailable</summary>
		public static float FallbackDpi
		{
			get => _fallbackDpi;
			set
			{
				value = Mathf.Clamp(value, 0.01f, Mathf.Infinity);
				_fallbackDpi = value;
			}
		}
	
		/// <summary>Screen dots per inch. If unavailable, FallbackDpi will be used.</summary>
		public static float Dpi
		{
			get
			{
				var dpi = UnityEngine.Screen.dpi;

				if (!(Mathf.Abs(UnityEngine.Screen.dpi) < Mathf.Epsilon)) return dpi;
				
				Debug.LogWarning("Could not get screen dpi, falling back to FallbackDpi");
				dpi = FallbackDpi;

				return dpi;
			}
		}
		
		/// <summary>Screen resolution in pixels</summary>
		public static Vector2 Resolution => Application.isEditor ? GetMainGameViewSize() : new Vector2(UnityEngine.Screen.width, UnityEngine.Screen.height);

		/// <summary>Width in pixels</summary>
		public static float WidthPx => Application.isEditor ? Resolution.x : UnityEngine.Screen.width;

		/// <summary>Height in pixels</summary>
		public static float HeightPx => Application.isEditor ? Resolution.y : UnityEngine.Screen.height;

		/// <summary>Screen size in inches</summary>
		public static Vector2 SizeInch => Resolution / Dpi;

		/// <summary>Screen width in inches</summary>
		public static float WidthInch => WidthPx / Dpi;

		/// <summary>Screen height in inches</summary>
		public static float HeightInch => HeightPx / Dpi;

		/// <summary>If the screen is landscape, i.e. width > height</summary>
		public static bool Landscape => Aspect > 1;

		/// <summary>The screen aspect, i.e. width / height</summary>
		public static float Aspect
		{
			get
			{
				var res = Resolution;
				return res.x / res.y;
			}
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		/// <summary>
		/// <para>Calculates the actual size of the game view in pixels.</para>
		/// Note: Only necessary in editor. Expensive to calculate so use sparingly.
		/// </summary>
		/// <returns></returns>
		public static Vector2 GetMainGameViewSize()
		{
			var T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
			if (T == null) return Vector2.one;
			
			var getSizeOfMainGameView = T.GetMethod(
				"GetSizeOfMainGameView",
				System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static
				);
			
			if (getSizeOfMainGameView == null) return Vector2.one;
			
			var res = getSizeOfMainGameView.Invoke(null,null);
			
			return (Vector2)res;
		}
		
		/// <summary>Calculates Dpi given a pixel and inch length</summary>
		/// <param name="pixels">pixel length</param>
		/// <param name="inches">length in inches</param>
		/// <returns></returns>
		public static float GetDpi(float pixels, float inches)
		{
			return pixels / inches;
		}
	
		/// <summary>Calculates pixel length given dpi and inch length</summary>
		/// <param name="dpi">dots (pixels) per inch</param>
		/// <param name="inches">length in inches</param>
		/// <returns></returns>
		public static float GetPixels(float dpi, float inches)
		{
			return dpi * inches;
		}
	
		/// <summary>Calculates inch length given dpi and pixel length</summary>
		/// <param name="pixels">pixel length</param>
		/// <param name="dpi">dots (pixels) per inch</param>
		/// <returns></returns>
		public static float GetInches(float pixels, float dpi)
		{
			return pixels / dpi;
		}

	}
}