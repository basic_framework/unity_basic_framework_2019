using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.Utility.Screen
{
	[AddComponentMenu("")]
	public class ScreenChangeMonitor : MonoBehaviour 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private bool _dontDestroyOnLoad = true;

		[Tooltip("Raised any time the screen changes")]
		[SerializeField] private ScriptableEvent _screenChanged = default;
		
		[Tooltip("Raised when the app window orientation changes. True = Landscape, False = Portrait")]
		[SerializeField] private BoolEvent _orientationChanged = default;
		
		[Tooltip("Raised when the app window resolution changes. Passes the pixel size of the window")]
		[SerializeField] private Vector2Event _resolutionChanged = default;
		
		[Tooltip("Raised when the app window aspect changes. Passes the new aspect ration (Width/Height)")]
		[SerializeField] private FloatEvent _aspectChanged = default;

		private static ScreenChangeMonitor _instance;
		
		private bool _prevLandscape;
		private Vector2 _prevResolution;
		private float _prevAspect;
		
		
		// ****************************************  METHODS  ****************************************\\

		private void Awake()
		{
			if (!Application.isPlaying) return;
			
			if (_instance != null && _instance != this)
			{
				enabled = false;
				Destroy(gameObject);
				return;
			}

			_instance = this;

			if (_dontDestroyOnLoad)
			{
				DontDestroyOnLoad(this);
			}
		}
		
		private void OnEnable()
		{
			_prevLandscape 	= ScreenUtility.Landscape;
			_prevResolution = ScreenUtility.Resolution;
			_prevAspect 	= ScreenUtility.Aspect;
		}
		
		private void Update()
		{
			var screenChanged = false;
			
			if (_prevLandscape != ScreenUtility.Landscape)
			{
				_prevLandscape = !_prevLandscape;
				_orientationChanged.Raise(_prevLandscape);
				screenChanged = true;
			}

			if (_prevResolution != ScreenUtility.Resolution)
			{
				_prevResolution = ScreenUtility.Resolution;
				_resolutionChanged.Raise(_prevResolution);
				screenChanged = true;
			}

			if (Mathf.Abs(_prevAspect - ScreenUtility.Aspect) > Mathf.Epsilon)
			{
				_prevAspect = ScreenUtility.Aspect;
				_aspectChanged.Raise(_prevAspect);
				screenChanged = true;
			}

			if (!screenChanged) return;
			_screenChanged.Raise();
		}
		
	}
}