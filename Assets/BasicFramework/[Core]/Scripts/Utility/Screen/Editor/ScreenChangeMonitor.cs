using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Screen.Editor
{
	[CustomEditor(typeof(ScreenChangeMonitor))]
	public class ScreenChangeMonitorEditor : UnityEditor.Editor 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		public override void OnInspectorGUI()
		{
			BasicEditorGuiLayout.DrawScriptField(serializedObject);
			
			serializedObject.Update();

			DrawScreenChangeMonitorFields();
			
			serializedObject.ApplyModifiedProperties();
		}

		private void DrawScreenChangeMonitorFields()
		{
			DrawSettingFields();
			
			EditorGUILayout.Space();
			DrawEventFields();
		}

		private void DrawSettingFields()
		{
			var dontDestroy = serializedObject.FindProperty("_dontDestroyOnLoad");
			
			EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(dontDestroy);
		}

		private void DrawEventFields()
		{
			var prevGuiEnabled = GUI.enabled;
			
			var screenChanged = serializedObject.FindProperty("_screenChanged");
			var orientationChanged = serializedObject.FindProperty("_orientationChanged");
			var resolutionChanged = serializedObject.FindProperty("_resolutionChanged");
			var aspectChanged = serializedObject.FindProperty("_aspectChanged");
			
			EditorGUILayout.LabelField("Events", EditorStyles.boldLabel);

			GUI.enabled = screenChanged.objectReferenceValue == null  && prevGuiEnabled;
			BasicEditorGuiLayout.PropertyFieldNotNull(screenChanged);
			
			GUI.enabled = orientationChanged.objectReferenceValue == null && prevGuiEnabled;
			BasicEditorGuiLayout.PropertyFieldNotNull(orientationChanged);
			
			GUI.enabled = resolutionChanged.objectReferenceValue == null && prevGuiEnabled;
			BasicEditorGuiLayout.PropertyFieldNotNull(resolutionChanged);
			
			GUI.enabled = aspectChanged.objectReferenceValue == null && prevGuiEnabled;
			BasicEditorGuiLayout.PropertyFieldNotNull(aspectChanged);

			GUI.enabled = prevGuiEnabled;
		}
		
	}
}