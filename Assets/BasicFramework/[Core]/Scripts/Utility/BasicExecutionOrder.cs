﻿using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public static class BasicExecutionOrder 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const int BUFFER = 100;
		
		public const int EXECUTION_ORDER_EXTERNAL_TRACKING = -10000;
		public const int EXECUTION_ORDER_BASIC_TRACKING = EXECUTION_ORDER_EXTERNAL_TRACKING + BUFFER;
		public const int EXECUTION_ORDER_POINTER_CALCULATION = EXECUTION_ORDER_BASIC_TRACKING + BUFFER;
		
		public const int EXECUTION_ORDER_TELEPORT_POINTER = EXECUTION_ORDER_POINTER_CALCULATION + BUFFER;
		public const int EXECUTION_ORDER_TELEPORT_POINTER_CONTROLLER = EXECUTION_ORDER_TELEPORT_POINTER + BUFFER;

		public const int EXECUTION_ORDER_INPUT_MANAGER = -8000;
		public const int EXECUTION_ORDER_UNITY_CALLBACK_EVENTS = -5000;

		public const int EXECUTION_ORDER_DEFAULT = 0;
		
		public const int EXECUTION_ORDER_POINTER_VISUALS = 8500;
		

		// ****************************************  METHODS  ****************************************\\


	}
}