﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	public class SaveLoadDataBaseEditor<T, TManager> : BasicBaseEditor<TManager>
	where T : class
	where TManager : SaveLoadDataBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.FileDirectory).CamelCaseToHumanReadable(), TypedTarget.FileDirectory);
			EditorGUILayout.LabelField(nameof(TypedTarget.FileName).CamelCaseToHumanReadable(), TypedTarget.FileName);
			EditorGUILayout.LabelField(nameof(TypedTarget.FilePath).CamelCaseToHumanReadable(), TypedTarget.FilePath);
			
			EditorGUILayout.LabelField(nameof(TypedTarget.ExistsDataFileDirectory).CamelCaseToHumanReadable(), TypedTarget.ExistsDataFileDirectory.ToString());
			EditorGUILayout.LabelField(nameof(TypedTarget.ExistsDataFile).CamelCaseToHumanReadable(), TypedTarget.ExistsDataFile.ToString());

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Save"))
			{
				TypedTarget.Save();
			}
			
			if (GUILayout.Button("Load"))
			{
				TypedTarget.Load();
			}
			
			EditorGUILayout.EndHorizontal();

			GUI.enabled = PrevGuiEnabled && TypedTarget.ExistsDataFileDirectory;
			
			if (GUILayout.Button("Open Directory"))
			{
				EditorUtility.RevealInFinder(TypedTarget.FileDirectory);
			}

			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var autoSave = serializedObject.FindProperty("_autoSave");
			var createFileOnStart = serializedObject.FindProperty("_createFileOnStart");
			var setupCallbacksLoad = serializedObject.FindProperty("_setupCallbacksLoad");
			
			Space();
			SectionHeader($"{nameof(SaveLoadDataBase<T>).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(autoSave);
			EditorGUILayout.PropertyField(createFileOnStart);
			EditorGUILayout.PropertyField(setupCallbacksLoad);
		}
	}
}