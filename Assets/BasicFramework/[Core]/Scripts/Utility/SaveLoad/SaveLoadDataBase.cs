﻿using System.IO;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public abstract class SaveLoadDataBase<T> : MonoBehaviour
		where T : class
	{
	
		// **************************************** VARIABLES ****************************************\\

		public abstract string FileDirectory { get; }
		public abstract string FileName { get; }
		public string FilePath => $"{FileDirectory}/{FileName}";
		
		
		[SerializeField] private bool _autoSave = default;

		[Tooltip("Create the data file on start if none exists")]
		[SerializeField] private bool _createFileOnStart = true;

		[EnumFlag("Which setup callbacks should the data be loaded during")] 
		[SerializeField] private SetupCallbackFlags _setupCallbacksLoad = default;
		
		
		public bool AutoSave
		{
			get => _autoSave;
			set => _autoSave = value;
		}

		
		public bool ExistsDataFileDirectory => Directory.Exists(FileDirectory);
		public bool ExistsDataFile => ExistsDataFileDirectory && File.Exists(FilePath);
		 

		// ****************************************  METHODS  ****************************************\\
		
		
		//***** mono *****\\

		protected virtual void Awake()
		{
			if ((_setupCallbacksLoad & SetupCallbackFlags.Awake) == SetupCallbackFlags.Awake)
			{
				Load();
			}
		}

		protected virtual void OnEnable()
		{
			if ((_setupCallbacksLoad & SetupCallbackFlags.Enable) == SetupCallbackFlags.Enable)
			{
				Load();
			}
		}

		protected virtual void Start()
		{
			if ((_setupCallbacksLoad & SetupCallbackFlags.Start) == SetupCallbackFlags.Start)
			{
				Load();
			}

			if (_createFileOnStart && !ExistsDataFile)
			{
				Save();
			}
			
			Debug.Log($"Path: {FilePath}");
		}

		
		//***** functions *****\\

		public void LoadWithoutResult()
		{
			Load();
		}
		
		public bool Load()
		{
			if (!ExistsDataFile) return false;
			
//			if (!Directory.Exists(FileDirectory))
//			{
//				return false;
//			}
//
//			if (!File.Exists(FilePath))
//			{
//				return false;
//			}

			var content = File.ReadAllText(FilePath);
			
			var data = JsonUtility.FromJson<T>(content);

			return OnDataLoaded(data);
		}

		public void Save()
		{
			if (!Directory.Exists(FileDirectory))
			{
				Directory.CreateDirectory(FileDirectory);
			}
		
			if (!File.Exists(FilePath))
			{
				var fs = new FileStream(FilePath, FileMode.Create);
				fs.Dispose();
			}

			var data = GetSaveData();

			var content = JsonUtility.ToJson(data);

//			Debug.Log($"Content: {content}");
			
			File.WriteAllText(FilePath, content);
		}


		//***** functions (abstract) *****\\
		
		/// <summary>
		/// Called just before saving to get a class containing the data that should be saved
		/// </summary>
		/// <returns></returns>
		protected abstract T GetSaveData();
		
		/// <summary>
		/// Called when data has been loaded from file.
		/// Utilise the data as required.
		/// Return true if data successfully used otherwise false.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		protected abstract bool OnDataLoaded(T data);
		
	}
}