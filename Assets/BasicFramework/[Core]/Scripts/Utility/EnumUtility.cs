using System;
using System.Linq;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
    public static class EnumUtility 
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        public static bool LayerInLayerMask(int layer, LayerMask layerMask)
        {
            return layerMask == (layerMask | (1 << layer));
        }

        public static T EnumNameToEnumValue<T>(string enumName)
            where T : struct, Enum
        {
            if (Enum.TryParse(enumName, out T enumValue))
            {
                return enumValue;
            }
			
            Debug.LogError($"Could not convert enum {enumName} to enum type {typeof(T).Name}");

            return default;
        }

        public static T[] GetEnumArray<T>()
            where T : struct, Enum
        {
            return (T[]) Enum.GetValues(typeof(T)).Cast<T>();
        }
        
    }
}