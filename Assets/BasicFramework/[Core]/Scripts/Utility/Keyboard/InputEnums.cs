namespace BasicFramework.Core.BasicTypes
{
    
    public enum ButtonInputStates
    {
        Down, 
        Held, 
        Up
    }
    
}