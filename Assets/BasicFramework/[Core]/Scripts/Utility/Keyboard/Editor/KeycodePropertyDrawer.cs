using BasicFramework.Core.Utility.Editor;
using UnityEngine;
using UnityEditor;

namespace BasicFramework.Core.Utility.Keyboard.Editor
{
	[CustomPropertyDrawer(typeof(KeyCode))]
	public class KeycodePropertyDrawer : BasicBasePropertyDrawer
	{

		// **************************************** VARIABLES ****************************************\\
		
		// Note: using property.isExpanded to determine if we are polling or not
		
		private const float BUTTON_WIDTH = 60f;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = EditorGUI.PrefixLabel(position, label);

			var rectKeyCode = new Rect(TempRect.x, TempRect.y, TempRect.width - BUTTON_WIDTH - BUFFER_WIDTH, TempRect.height);
			var rectButton  = new Rect(TempRect.x + rectKeyCode.width + BUFFER_WIDTH, TempRect.y, BUTTON_WIDTH, TempRect.height);
			
			if (!property.isExpanded)
			{
				EditorGUI.PropertyField(rectKeyCode, property, GUIContent.none);

				if (!GUI.Button(rectButton, "POLL")) return;
				
				GUI.FocusControl(null);
				property.isExpanded = true;
				return;
			}

			EditorGUI.LabelField(rectKeyCode, "Press Key...");

			if(GUI.Button(rectButton, "Cancel"))
			{
				property.isExpanded = false;
				return;
			}

			var guiEvent = Event.current;

			var key = KeyCode.None;
			
			if (guiEvent.isKey)
			{
				key = guiEvent.keyCode;

				property.enumValueIndex = KeyboardEditorUtility.KeyCodeToEnumIndex(property, key);

				property.isExpanded = false;
			}
			else if (guiEvent.isMouse)
			{
				switch (guiEvent.button)
				{
					case 0:		key = KeyCode.Mouse0;	break;
					case 1:		key = KeyCode.Mouse1;	break;
					case 2:		key = KeyCode.Mouse2;	break;
				}

				property.enumValueIndex = KeyboardEditorUtility.KeyCodeToEnumIndex(property, key);

				property.isExpanded = false;
			}
		}
		
	}
}


