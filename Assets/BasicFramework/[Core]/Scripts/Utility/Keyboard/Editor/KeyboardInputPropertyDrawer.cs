using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Keyboard.Editor
{
	[CustomPropertyDrawer(typeof(KeyboardInput))]
	public class KeyboardInputPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded)
			{
				return;
			}
			
			var key = property.FindPropertyRelative("_key");
			var state = property.FindPropertyRelative("_state");
			var anyModifier = property.FindPropertyRelative("_anyModifier");
			var modifier = property.FindPropertyRelative("_modifier");

			EditorGUI.PropertyField(TempRect, key);
			TempRect.y += EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT;
			
			EditorGUI.PropertyField(TempRect, state);
			TempRect.y += EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT;
			
			EditorGUI.PropertyField(TempRect, anyModifier);
			TempRect.y += EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT;
			
			GUI.enabled = PrevGuiEnabled && !anyModifier.boolValue;
			
			EditorGUI.PropertyField(TempRect, modifier);
			TempRect.y += EditorGUIUtility.singleLineHeight + BUFFER_HEIGHT;
			
			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return !property.isExpanded ? SingleLineHeight : EditorGUIUtility.singleLineHeight * 5 + BUFFER_HEIGHT * 5;
		}
		
	}
}