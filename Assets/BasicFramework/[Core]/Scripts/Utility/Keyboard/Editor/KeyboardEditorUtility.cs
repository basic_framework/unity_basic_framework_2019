using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Keyboard.Editor
{
    public static class KeyboardEditorUtility
    {
        
        // **************************************** VARIABLES ****************************************\\
        
        
        // ****************************************  METHODS  ****************************************\\
        
        /// <summary>
        /// Gets the int value of the given KeyCode based on the SerializedProperty KeyCode representation
        /// </summary>
        /// <param name="keyCodeProperty"></param>
        /// <param name="keyCode"></param>
        /// <returns></returns>
        public static int KeyCodeToEnumIndex(SerializedProperty keyCodeProperty, KeyCode keyCode)
        {
            var keyCodeNames = keyCodeProperty.enumNames;
			
            for (var i = 0; i < keyCodeNames.Length; i++) {

                if (keyCodeNames[i] == keyCode.ToString())
                {
                    return i;
                }
            }
			
            return 0;
        }
    }
}