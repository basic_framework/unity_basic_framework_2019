using System;

namespace BasicFramework.Core.Utility.Keyboard
{
    [Flags]
    public enum KeyboardModifiers
    {
        None = 0,
        Ctrl = 1 << 0,
        Alt = 1 << 1,
        Shift = 1 << 2,
        CtrlAlt = Ctrl | Alt,
        CtrlShift = Ctrl | Shift,
        AltShift = Alt | Shift,
        CtrlAltShift = Ctrl | Alt | Shift
    }
}