using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Utility.Keyboard    
{
    [Serializable]
    public struct KeyboardInput 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        public static KeyboardInput Default = 
            new KeyboardInput(KeyCode.None, ButtonInputStates.Down, true, KeyboardModifiers.None);
		
        [SerializeField] private KeyCode _key;

        [SerializeField] private ButtonInputStates _state;

        [Tooltip("Works regardless of any modifiers")]
        [SerializeField] private bool _anyModifier;

        [SerializeField] private KeyboardModifiers _modifier;
        
        public bool Value
        {
            get
            {
                if(!_anyModifier && KeyboardUtility.ModifierState != _modifier) return false;

                switch (_state)
                {
                    case ButtonInputStates.Down: 	return Input.GetKeyDown(_key);
                    case ButtonInputStates.Held: 	return Input.GetKey(_key);
                    case ButtonInputStates.Up: 	return Input.GetKeyUp(_key);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
		
        // ****************************************  METHODS  ****************************************\\
		
        public KeyboardInput(KeyCode key, ButtonInputStates state, bool anyModifier, KeyboardModifiers modifier)
        {
            _key = key;
            _state = state;
            _anyModifier = anyModifier;
            _modifier = modifier;
        }

    }
}