using UnityEngine;

namespace BasicFramework.Core.Utility.Keyboard
{
    public static class KeyboardUtility
    {

        // **************************************** VARIABLES ****************************************\\

        private static int _lastFrameModifierUpdated;
		
        private static KeyboardModifiers _modifierState;
        public static KeyboardModifiers ModifierState
        {
            get
            {
                if (_lastFrameModifierUpdated == Time.frameCount) return _modifierState;
				
                _lastFrameModifierUpdated = Time.frameCount;

                _modifierState = GetModifierState();

                return _modifierState;
            }
        }


        // ****************************************  METHODS  ****************************************\\

        private static KeyboardModifiers GetModifierState()
        {

            var modifierState = (KeyboardModifiers) 0;
            
            var ctrl = 
                Input.GetKey(KeyCode.LeftControl)
                || Input.GetKey(KeyCode.RightControl);

            var alt =
                Input.GetKey(KeyCode.LeftAlt) 
                || Input.GetKey(KeyCode.RightAlt)
                || Input.GetKey(KeyCode.LeftCommand) 
                || Input.GetKey(KeyCode.RightCommand);

            var shift = 
                Input.GetKey(KeyCode.LeftShift) 
                || Input.GetKey(KeyCode.RightShift);

            if (ctrl)
            {
                modifierState |= KeyboardModifiers.Ctrl;
            }

            if (alt)
            {
                modifierState |= KeyboardModifiers.Alt;
            }

            if (shift)
            {
                modifierState |= KeyboardModifiers.Shift;
            }
            
            return modifierState;
        }

        public static float KeyboardAxis(KeyCode positiveKey, KeyCode negativeKey)
        {
            if (Input.GetKey(positiveKey))
            {
                return 1f;
            }

            if (Input.GetKey(negativeKey))
            {
                return -1f;
            }

            return 0f;
        }
		
    }
}