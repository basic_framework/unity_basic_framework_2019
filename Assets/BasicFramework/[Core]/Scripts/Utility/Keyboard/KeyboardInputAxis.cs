﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.Utility.Keyboard
{
	[Serializable]
	public class KeyboardInputAxis 
	{
		
		// TODO: Update to KeyboardInputFloat, add easing to value, keep track of frame and time between updates
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private KeyCode _keyCodePositive;
		[SerializeField] private KeyCode _keyCodeNegative;

		[SerializeField] private KeyCode[] _keyCodeModifiers;

		public float Value
		{
			get
			{
				foreach (var keyCodeModifier in _keyCodeModifiers)
				{
					if (!Input.GetKey(keyCodeModifier)) return 0;
				}

				var rawValue = 0;
				
				if (Input.GetKey(_keyCodePositive))
				{
					rawValue = 1;
				}
				else if (Input.GetKey(_keyCodeNegative))
				{
					rawValue = -1;
				}

				return rawValue;
			}
		}

		// ****************************************  METHODS  ****************************************\\

		public KeyboardInputAxis()
		{
			_keyCodePositive = KeyCode.None;
			_keyCodeNegative = KeyCode.None;
			_keyCodeModifiers = new KeyCode[0];
		}
		
		public KeyboardInputAxis(KeyCode keyCodePositive, KeyCode keyCodeNegative)
		{
			_keyCodePositive = keyCodePositive;
			_keyCodeNegative = keyCodeNegative;
			_keyCodeModifiers = new KeyCode[0];
		}
		
		public KeyboardInputAxis(KeyCode keyCodePositive, KeyCode keyCodeNegative,
			KeyCode[] keyCodeModifiers)
		{
			_keyCodePositive = keyCodePositive;
			_keyCodeNegative = keyCodeNegative;
			
			_keyCodeModifiers = keyCodeModifiers != null ? keyCodeModifiers : new KeyCode[0];
		}

	}
}