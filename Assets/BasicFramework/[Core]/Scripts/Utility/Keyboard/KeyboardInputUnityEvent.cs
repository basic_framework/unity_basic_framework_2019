using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;
using UnityEngine.Events;

namespace TutorialsBasicFramework.Core.Utility.Keyboard
{
    [AddComponentMenu("Basic Framework/Core/Utility/Keyboard/Keyboard Input Unity Event")]
    public class KeyboardInputUnityEvent : MonoBehaviour 
    {

        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private KeyboardInput _keyboardInput = KeyboardInput.Default;
		
        [Space][Header("Events")]
        [SerializeField] private UnityEvent _enterActioned = default;
        [SerializeField] private UnityEvent _stayActioned = default;
        [SerializeField] private UnityEvent _exitActioned = default;

        private bool _prevActioned;
		
		
        // ****************************************  METHODS  ****************************************\\
		
        private void Update()
        {
            var actioned = _keyboardInput.Value;

            if (_prevActioned != actioned)
            {
                _prevActioned = actioned;
				
                if(actioned) _enterActioned.Invoke();
                else 		 _exitActioned.Invoke();
            }
			
            if(!actioned) return;
            _stayActioned.Invoke();
        }
        
    }
}