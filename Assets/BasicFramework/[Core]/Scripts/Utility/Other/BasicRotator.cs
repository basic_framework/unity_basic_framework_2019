﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public class BasicRotator : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Scale of the gizmo drawn axis")]
		[SerializeField] private float _editorRotateAxisScale = 1f;
		
		[Tooltip("Color of the gizmos")]
		[SerializeField] private Color _editorRotateAxisColor = Color.magenta;
		

		[Tooltip("The transform that will be rotated")]
		[SerializeField] private Transform _transformRotating = default;
		
		[Tooltip("Is the pivot point offset in world space AND relative to where the transform started")]
		[SerializeField] private bool _offsetInWorldSpace = true;
		
		[Tooltip("The pivot point offset local to the rotating transform")]
		[SerializeField] private Vector3 _offsetPivotPointRaw = Vector3.zero;

		[Tooltip("Is the axis of rotation in world space")]
		[SerializeField] private bool _axisInWorldSpace = default;

		[Tooltip("The axis that the object will rotate around")]
		[SerializeField] private Vector3 _axisOfRotationRaw = Vector3.up;

		[Tooltip("Rotation speed in deg/sec")]
		[SerializeField] private float _speedRotation = 60f;


		public Transform TransformRotating => _transformRotating;

		public bool OffsetInWorldSpace
		{
			get => _offsetInWorldSpace;
			set => _offsetInWorldSpace = value;
		}

		public Vector3 OffsetPivotPointRaw
		{
			get => _offsetPivotPointRaw;
			set => _offsetPivotPointRaw = value;
		}

		public bool AxisInWorldSpace
		{
			get => _axisInWorldSpace;
			set => _axisInWorldSpace = value;
		}

		public Vector3 AxisOfRotationRaw
		{
			get => _axisOfRotationRaw;
			set => _axisOfRotationRaw = value;
		}

		public float SpeedRotation
		{
			get => _speedRotation;
			set => _speedRotation = value;
		}
		

		private Vector3 OffsetPivotPointWorldSpace => _offsetInWorldSpace
			? _positionStart + _offsetPivotPointRaw
			: _transformRotating.TransformPoint(_offsetPivotPointRaw);  
		
		private Vector3 AxisOfRotationWorldSpace => _axisInWorldSpace 
			? _axisOfRotationRaw 
			: _transformRotating.rotation * _axisOfRotationRaw;

		private Vector3 _positionStart;
//		private Quaternion _rotationStart;
		

		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_transformRotating = transform;
		}

		private void OnValidate()
		{
			_editorRotateAxisScale = Mathf.Max(_editorRotateAxisScale, 0.01f);

			if (_transformRotating == null) return;
			_positionStart = _transformRotating.position;
		}

		private void Start()
		{
			_positionStart = _transformRotating.position;
//			_rotationStart = _transformRotating.rotation;
		}

		private void Update()
		{
			var pivotPoint = OffsetPivotPointWorldSpace;
			var axisOfRotation = AxisOfRotationWorldSpace;
			var delta = _speedRotation * Time.deltaTime;
				
			_transformRotating.RotateAround(pivotPoint, axisOfRotation, delta);
		}
		
		private void OnDrawGizmos()
		{
			if (_transformRotating == null) return;
			
			var prevColor = Gizmos.color;
			Gizmos.color = _editorRotateAxisColor;

			var pivotPoint = OffsetPivotPointWorldSpace;
			var axisOfRotation = AxisOfRotationWorldSpace;
			
			// Draw ray
			Gizmos.DrawWireSphere(pivotPoint, 0.05f * _editorRotateAxisScale);
			Gizmos.DrawRay(pivotPoint, axisOfRotation * _editorRotateAxisScale);
			
			Gizmos.color = prevColor;
		}
		
	}
}