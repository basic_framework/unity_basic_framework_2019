﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	[CustomEditor(typeof(BasicRotator))]
	public class BasicRotatorEditor : BasicBaseEditor<BasicRotator>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var editorRotateAxisScale = serializedObject.FindProperty("_editorRotateAxisScale");
			var editorRotateAxisColor = serializedObject.FindProperty("_editorRotateAxisColor");

			EditorGUILayout.PropertyField(editorRotateAxisScale);
			EditorGUILayout.PropertyField(editorRotateAxisColor);
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var transformRotating = serializedObject.FindProperty("_transformRotating");
			
			var offsetInWorldSpace = serializedObject.FindProperty("_offsetInWorldSpace");
			var offsetPivotPointRaw = serializedObject.FindProperty("_offsetPivotPointRaw");
			
			var axisInWorldSpace = serializedObject.FindProperty("_axisInWorldSpace");
			var axisOfRotationRaw = serializedObject.FindProperty("_axisOfRotationRaw");
			
			var speedRotation = serializedObject.FindProperty("_speedRotation");
			
			Space();
			SectionHeader($"{nameof(BasicRotator).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformRotating);

			Space();
			EditorGUILayout.PropertyField(offsetInWorldSpace);
			EditorGUILayout.PropertyField(offsetPivotPointRaw);
			
			Space();
			EditorGUILayout.PropertyField(axisInWorldSpace);
			BasicEditorGuiLayout.Vector3DirectionDropdown(axisOfRotationRaw);
			
			Space();
			EditorGUILayout.PropertyField(speedRotation);
		}
		
	}
}