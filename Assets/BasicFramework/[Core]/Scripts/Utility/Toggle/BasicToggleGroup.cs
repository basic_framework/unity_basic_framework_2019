using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle
{
    [AddComponentMenu("Basic Framework/Core/Utility/Toggle/Basic Toggle Group")]
    public class BasicToggleGroup : MonoBehaviour
    {
        
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("The maximum number of Basic Toggles that can be on at once, 0 => unlimited")]
        [SerializeField] private int _maximumSelectedAtOnce = 1;
        
        [Tooltip("Is it possible for none of the toggles to be on")]
        [SerializeField] private bool _allowNoneSelected = default;
        
        [Tooltip("These toggles will be added to the group on start")]
        [SerializeField] private BasicToggle[] _startToggles = new BasicToggle[0];

        [Tooltip("Raised when a BasicToggle value is changed to true")]
        [SerializeField] private BasicToggleUnityEvent _toggleSelected = default;
        
        [Tooltip("Raised when a BasicToggle value is changed to false")]
        [SerializeField] private BasicToggleUnityEvent _toggleDeselected = default;
        
        public int MaximumSelectedAtOnce
        {
            get => _maximumSelectedAtOnce;
            set
            {
                if (value < 0) value = 0;
                if (_maximumSelectedAtOnce == value) return;
                _maximumSelectedAtOnce = value;

                if (!enabled) return;
                if (value <= 0) return;
                for (int i = _selectedToggles.Count - 1; i >= value; i--)
                {
                    _selectedToggles[i].IsOn = false;
                }
            }
        }

        public bool AllowNoneSelected
        {
            get => _allowNoneSelected;
            set
            {
                if (_allowNoneSelected == value) return;
                _allowNoneSelected = value;
                
                if (!enabled) return;
                if (_allowNoneSelected) return;
                if (_managedToggles.Count == 0) return;
                if (_selectedToggles.Count > 0) return;
                _managedToggles[0].IsOn = true;
            }
        }
        
        private readonly List<BasicToggle> _managedToggles = new List<BasicToggle>();
        
        public BasicToggle[] ManagedToggles => _managedToggles.ToArray();
        
        private readonly List<BasicToggle> _selectedToggles = new List<BasicToggle>();
        

        // ****************************************  METHODS  ****************************************\\

        private void OnEnable()
        {
            if (_managedToggles.Count == 0) return;
			
            _selectedToggles.Clear();
			
            foreach (var managedToggle in _managedToggles)
            {
                if (managedToggle.IsOn)
                {
                    if (_selectedToggles.Count >= _maximumSelectedAtOnce)
                    {
                        managedToggle.IsOn = false;
                    }
                    else
                    {
                        _selectedToggles.Add(managedToggle);
                    }
                }

                managedToggle.ValueChanged.Subscribe(BasicToggleStateChanged);
            }

            if (_allowNoneSelected || _selectedToggles.Count != 0) return;
            _managedToggles[0].IsOn = true;
        }

        private void OnDisable()
        {
            foreach (var managedToggle in _managedToggles)
            {
                managedToggle.ValueChanged.Unsubscribe(BasicToggleStateChanged);
            }
			
            _selectedToggles.Clear();
        }
        
        private void Start()
        {
            foreach (var toggle in _startToggles)
            {
                AddBasicToggle(toggle);
            }
        }

        public void AddBasicToggle(BasicToggle basicToggle)
        {
            if (basicToggle == null) return;
            if (_managedToggles.Contains(basicToggle)) return;
			
            _managedToggles.Add(basicToggle);

            if (!enabled) return;
			
            if (!_allowNoneSelected && _selectedToggles.Count == 0)
            {
                basicToggle.IsOn = true;
            }
			
            basicToggle.ValueChanged.Subscribe(BasicToggleStateChanged);
			
            BasicToggleStateChanged(basicToggle);
        }

        public void RemoveBasicToggle(BasicToggle basicToggle)
        {
            if (basicToggle == null) return;
            if (!_managedToggles.Remove(basicToggle)) return;

            _selectedToggles.Remove(basicToggle);
			
            if (!enabled) return;
			
            basicToggle.ValueChanged.Unsubscribe(BasicToggleStateChanged);
			
            if (_managedToggles.Count == 0) return;

            if (_allowNoneSelected || _selectedToggles.Count > 0) return;

            _managedToggles[0].IsOn = true;
        }
        
        public void DeselectAll()
        {
            foreach (var managedToggle in _managedToggles)
            {
                managedToggle.IsOn = false;
            }
        }
        
        private void BasicToggleStateChanged(BasicToggle basicToggle)
        {
            if (!_managedToggles.Contains(basicToggle))
            {
                Debug.LogError($"{name} => {typeof(BasicToggleGroup).Name} is not managing {basicToggle.name} {typeof(BasicToggle).Name}");
                return;
            }

            if (basicToggle.IsOn)
            {
                if (_selectedToggles.Contains(basicToggle)) return;
				
                _selectedToggles.Add(basicToggle);

                if (_maximumSelectedAtOnce > 0 && _selectedToggles.Count > _maximumSelectedAtOnce)
                {
                    _selectedToggles[0].IsOn = false;
                }

                _toggleSelected.Invoke(basicToggle);
                return;
            }
            
            if (!_selectedToggles.Contains(basicToggle)) return;
			
            if(!_allowNoneSelected && _selectedToggles.Count <= 1)
            {
                basicToggle.IsOn = true;
                return;
            }

            _selectedToggles.Remove(basicToggle);
			
            _toggleDeselected.Invoke(basicToggle);
        }

    }
}