﻿using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle
{
	[AddComponentMenu("Basic Framework/Core/Utility/Toggle/Game Object Toggler")]
	public class GameObjectToggler : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		public enum SetActiveOnStates
		{
			None,
			Awake,
			Start
		}
		
		[SerializeField] private GameObject[] _gameObjects = new GameObject[0];
		
		[Tooltip("Only one GameObject can be active at a time")]
		[SerializeField] private bool _radioGroup = default;

		[Tooltip("Sets all GameObject active states to SetGameObjectsActive in the selected method")]
		[SerializeField] private SetActiveOnStates _setActiveOn = default;
		
		[Tooltip("Set GameObjects active state to this in the SetGameObjectsActiveOn method")]
		[SerializeField] private bool _setActive = default;

		public GameObject[] GameObject => _gameObjects;

		public bool RadioGroup => _radioGroup;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		private void Awake()
		{
			if (_setActiveOn != SetActiveOnStates.Awake) return;
			SetAllGameObjectsActive(_setActive);
		}

		private void Start()
		{
			if (_setActiveOn != SetActiveOnStates.Start) return;
			SetAllGameObjectsActive(_setActive);
		}
		
		public void SetAllGameObjectsActive(bool active)
		{
			foreach (var go in _gameObjects)
			{
				go.SetActive(active);
			}
		}
	
	}
}