using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility.Toggle
{
    [AddComponentMenu("Basic Framework/Core/Utility/Toggle/Basic Toggle")]
    public class BasicToggle : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [Tooltip("Raise the value changed events even if the monobehaviour is disabled")]
        [SerializeField] private bool _raiseWhileDisabled = true;
        
        [Tooltip("If the toggle is on or off")]
        [SerializeField] private bool _isOn = default;
        
        [Tooltip("Events raised when the toggle state changes from on to off or vice versa")]
        [SerializeField] private BoolPlusUnityEvent _isOnChanged = default;

        public bool RaiseWhileDisabled
        {
            get => _raiseWhileDisabled;
            set => _raiseWhileDisabled = value;
        }
        
        public bool IsOn
        {
            get => _isOn;
            set
            {
                if (_isOn == value) return;
                _isOn = value;
                IsOnUpdated(_isOn);
            }
        }
        
        public BoolPlusUnityEvent IsOnChanged => _isOnChanged;
        
        public readonly BasicEvent<BasicToggle> ValueChanged = new BasicEvent<BasicToggle>(); 
		
		
        // ****************************************  METHODS  ****************************************\\

        private void OnEnable()
        {
            IsOnUpdated(_isOn);
        }

        private void Start()
        {
            IsOnUpdated(_isOn);
        }

        private void IsOnUpdated(bool isOn)
        {
            ValueChanged.Raise(this);
            
            if (!_raiseWhileDisabled) return;
            _isOnChanged.Raise(isOn);
        }

        public void Toggle()
        {
            IsOn = !_isOn;
        }

        public void EditorRaise()
        {
            if (!Application.isEditor) return;
            IsOnUpdated(_isOn);
        }

    }
	
    [Serializable]
    public class BasicToggleUnityEvent : UnityEvent<BasicToggle>{}
    
    [Serializable]
    public class BasicToggleArrayUnityEvent : UnityEvent<BasicToggle[]>{}
	
    [Serializable]
    public class BasicToggleValueChangedUnityEvent : CompareValueUnityEventBase<BasicToggle>{}
}