﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle.Editor
{
	[CustomEditor(typeof(GameObjectRadioGroup))]
	public class GameObjectRadioGroupEditor : BasicBaseEditor<GameObjectRadioGroup>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		private ReorderableList _gameobjectsReorderableList;
		
		private ReorderableList _activeGameObjectCompareReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var gameObjects = serializedObject.FindProperty("_gameObjects");
			_gameobjectsReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, gameObjects);

			_gameobjectsReorderableList.displayAdd = false;


			var activeGameObjectChangedCompare = serializedObject.FindProperty("_activeGameObjectChangedCompare");
			_activeGameObjectCompareReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, activeGameObjectChangedCompare, true);

			
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var activeGameObject = serializedObject.FindProperty("_activeGameObject");

			GUI.enabled = false;

			BasicEditorGuiLayout.PropertyFieldCanBeNull(activeGameObject);
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var gameObjects = serializedObject.FindProperty("_gameObjects");
			var startActiveGameObject = serializedObject.FindProperty("_startActiveGameObject");
			var allowNoneSelected = serializedObject.FindProperty("_allowNoneSelected");
			var setActiveGameObjectOnStart = serializedObject.FindProperty("_setActiveGameObjectOnStart");
			
			
			Space();
			SectionHeader("Game Object Radio Group Properties");

			EditorGUILayout.PropertyField(setActiveGameObjectOnStart);

			GUI.enabled = PrevGuiEnabled && setActiveGameObjectOnStart.boolValue;
			BasicEditorGuiLayout.PropertyFieldCanBeNull(startActiveGameObject);
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.PropertyField(allowNoneSelected);
			
			BasicEditorGuiLayout.DragAndDropAreaIntoList<GameObject>(gameObjects, "Drop GameObjects Here");

			BasicEditorArrayUtility.RemoveNullReferences(gameObjects);
			BasicEditorArrayUtility.RemoveDuplicateReferences(gameObjects);
			
			if (Event.current.type == EventType.DragPerform) return;
			
			DrawGameObjectList();
			
			//_gameobjectsReorderableList.DoLayoutList();
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var activeGameObjectChanged = serializedObject.FindProperty("_activeGameObjectChanged");

			Space();
			SectionHeader("Game Object Radio Group Events");
			
			EditorGUILayout.PropertyField(activeGameObjectChanged);
			_activeGameObjectCompareReorderableList.DoLayoutList();
		}

		private void DrawGameObjectList()
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");
			
			var removeIndex = -1;
			var moveUpIndex = -1;
			var moveDownIndex = -1;
			Object selection = null;
			
			
			for (int i = 0; i < gameObjects.arraySize; i++)
			{
				var element = gameObjects.GetArrayElementAtIndex(i);
				
				if(!element.objectReferenceValue) continue;
				
				var gameObject = element.objectReferenceValue as GameObject;
				if(!gameObject) continue;

				EditorGUILayout.BeginHorizontal();
				
				EditorGUI.BeginChangeCheck();
				var active = EditorGUILayout.Toggle(gameObject.name, gameObject.activeSelf, GUILayout.ExpandWidth(true));
				if (EditorGUI.EndChangeCheck())
				{
					TypedTarget.SetActiveGameObject(active ? gameObject : null);
				}
				
				if (GUILayout.Button("UP", GUILayout.Width(60f)))
				{
					moveUpIndex = i;
				}
				
				if (GUILayout.Button("DOWN", GUILayout.Width(60f)))
				{
					moveDownIndex = i;
				}

				if (GUILayout.Button("PING", GUILayout.Width(60f)))
				{
					selection = gameObject;
				}
				
				if (GUILayout.Button("REMOVE", GUILayout.Width(80f)))
				{
					removeIndex = i;
				}
				
				EditorGUILayout.EndHorizontal();
			}

			if (selection != null)
			{
				EditorGUIUtility.PingObject(selection);
			}

			if (moveUpIndex > 0)
			{
				gameObjects.MoveArrayElement(moveUpIndex, moveUpIndex - 1);
			}

			if (moveDownIndex >= 0 && moveDownIndex < gameObjects.arraySize - 1)
			{
				gameObjects.MoveArrayElement(moveDownIndex, moveDownIndex + 1);
			}
			
			
			
			if (removeIndex < 0) return;
			BasicEditorArrayUtility.RemoveArrayElement(gameObjects, removeIndex);
		}
	}
}