using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle.Editor
{
	[CustomEditor(typeof(BasicToggleGroup))][CanEditMultipleObjects]
	public class BasicToggleGroupEditor : BasicBaseEditor<BasicToggleGroup>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawSettingFields();
		}

		private void DrawSettingFields()
		{
			SectionHeader("Settings");
			
			var maximumSelectedAtOnce = serializedObject.FindProperty("_maximumSelectedAtOnce");
			var allowNoneSelected = serializedObject.FindProperty("_allowNoneSelected");
			var startToggles = serializedObject.FindProperty("_startToggles");

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(maximumSelectedAtOnce);
			if (EditorGUI.EndChangeCheck() && Application.isPlaying)
			{
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.MaximumSelectedAtOnce = maximumSelectedAtOnce.intValue;
				}
			}

			if (maximumSelectedAtOnce.intValue < 0)
			{
				maximumSelectedAtOnce.intValue = 0;
			}
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(allowNoneSelected);
			if (EditorGUI.EndChangeCheck() && Application.isPlaying)
			{
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.AllowNoneSelected = allowNoneSelected.boolValue;
				}
			}

			Space();
			
			if (!Application.isPlaying)
			{
				EditorGUILayout.PropertyField(startToggles, true);
			}
			else
			{
				SectionHeader(GetGuiContent("Managed Toggles"));
				foreach (var basicToggle in TypedTarget.ManagedToggles)
				{
					basicToggle.IsOn = EditorGUILayout.Toggle(basicToggle.name, basicToggle.IsOn);
				}
			}
			
		}

		protected override void DrawEventFields()
		{
			SectionHeader("Events");
			
			var toggleSelected = serializedObject.FindProperty("_toggleSelected");
			var toggleDeselected = serializedObject.FindProperty("_toggleDeselected");

			EditorGUILayout.PropertyField(toggleSelected);
			EditorGUILayout.PropertyField(toggleDeselected);
		}
		
	}
}