﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle.Editor
{
	[CustomEditor(typeof(GameObjectToggler))][CanEditMultipleObjects]
	public class GameObjectTogglerEditor : BasicBaseEditor<GameObjectToggler>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\


		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			DrawSettingFields();
			
			Space(8f);
			DrawEditorFields();
			
			Space(8f);
			DrawGameObjectFields();
		}
		
		private void DrawSettingFields()
		{
			SectionHeader("Settings");
			
			var setGameObjectsActiveOn = serializedObject.FindProperty("_setActiveOn");
			var setGameObjectsActive = serializedObject.FindProperty("_setActive");

			EditorGUILayout.PropertyField(setGameObjectsActiveOn);

			GUI.enabled = PrevGuiEnabled && setGameObjectsActiveOn.enumValueIndex != 0;

			EditorGUILayout.PropertyField(setGameObjectsActive);

			GUI.enabled = PrevGuiEnabled;
		}
		
		private void DrawEditorFields()
		{
			SectionHeader("Editor Controls");
			
			var radioGroup = serializedObject.FindProperty("_radioGroup");
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(radioGroup);
			if (EditorGUI.EndChangeCheck() && radioGroup.boolValue)
			{
				UpdateRadioGroup();
			}

			EditorGUILayout.BeginHorizontal();
			
			if (GUILayout.Button("Set All Active")) 	{ SetAllActive(true); }
			if (GUILayout.Button("Set All Inactive")) 	{ SetAllActive(false); }
			
			EditorGUILayout.EndHorizontal();
		}
		
		private void DrawGameObjectFields()
		{
			SectionHeader("GameObjects");
			
			var radioGroup = serializedObject.FindProperty("_radioGroup");
			var gameObjects = serializedObject.FindProperty("_gameObjects");

			if (BasicEditorGuiLayout.DragAndDropAreaIntoList<GameObject>(gameObjects, "Drop GameObjects Here"))
			{
				BasicEditorArrayUtility.RemoveDuplicateReferences(gameObjects);
				BasicEditorArrayUtility.RemoveNullReferences(gameObjects);
				
				if(radioGroup.boolValue) UpdateRadioGroup();
			}
	
			if (Event.current.type == EventType.DragPerform) return;
			
			Space();
			
			EditorGUILayout.BeginHorizontal();
			
			if (GUILayout.Button("Add Children"))
			{
				AddChildren();
			}
			
			if (GUILayout.Button("Remove All"))
			{
				RemoveAll();
			}
			
			EditorGUILayout.EndHorizontal();
			
			Space();
			
			var removeIndex = -1;
			Object selection = null;
			
			for (int i = 0; i < gameObjects.arraySize; i++)
			{
				var element = gameObjects.GetArrayElementAtIndex(i);
				
				if(!element.objectReferenceValue) continue;
				
				var gameObject = element.objectReferenceValue as GameObject;
				if(!gameObject) continue;

				EditorGUILayout.BeginHorizontal();
				
				EditorGUI.BeginChangeCheck();
				var active = EditorGUILayout.Toggle(gameObject.name, gameObject.activeSelf, GUILayout.ExpandWidth(true));
				if (EditorGUI.EndChangeCheck() && radioGroup.boolValue)
				{
					UpdateRadioGroup(active ? i : -1);
				}
				else
				{
					gameObject.SetActive(active);
				}
				
				if (GUILayout.Button("PING", GUILayout.Width(80f)))
				{
					selection = gameObject;
				}
				
				if (GUILayout.Button("REMOVE", GUILayout.Width(80f)))
				{
					removeIndex = i;
				}
				
				EditorGUILayout.EndHorizontal();
			}

			if (selection)
			{
				EditorGUIUtility.PingObject(selection);
			}
			
			if (removeIndex < 0) return;
			BasicEditorArrayUtility.RemoveArrayElement(gameObjects, removeIndex);
		}

		private void UpdateRadioGroup(int activeIndex)
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");

			for (int i = 0; i < gameObjects.arraySize; i++)
			{
				var element = gameObjects.GetArrayElementAtIndex(i);
				
				if(!element.objectReferenceValue) continue;
				
				var gameObject = element.objectReferenceValue as GameObject;
				if(!gameObject) continue;
				
				gameObject.SetActive(i == activeIndex);
			}
		}
		
		private void UpdateRadioGroup()
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");
			var firstActivated = false;

			if (gameObjects.arraySize == 0) return;

			for (int i = 0; i < gameObjects.arraySize; i++)
			{
				var element = gameObjects.GetArrayElementAtIndex(i);
				
				if(!element.objectReferenceValue) continue;
				
				var gameObject = element.objectReferenceValue as GameObject;
				if(!gameObject) continue;

				if (!gameObject.activeSelf) { continue; }

				if (firstActivated)
				{
					gameObject.SetActive(false);
					continue;
				}

				firstActivated = true;
			}
		}
		
		private void SetAllActive(bool active)
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");

			for (int i = 0; i < gameObjects.arraySize; i++)
			{
				var element = gameObjects.GetArrayElementAtIndex(i);
				
				if(!element.objectReferenceValue) continue;
				
				var gameObject = element.objectReferenceValue as GameObject;
				if(!gameObject) continue;
				
				gameObject.SetActive(active);
			}
		}

		private void AddChildren()
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");

			var parent = TypedTarget.gameObject.transform;
			
			foreach (Transform child in parent)
			{
				BasicEditorArrayUtility.AddArrayElement(gameObjects, child.gameObject);
			}
			
			BasicEditorArrayUtility.RemoveDuplicateReferences(gameObjects);
			BasicEditorArrayUtility.RemoveNullReferences(gameObjects);
		}

		private void RemoveAll()
		{
			var gameObjects = serializedObject.FindProperty("_gameObjects");
			BasicEditorArrayUtility.RemoveAllArrayElements(gameObjects);
		}
		
	}
}