using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.Utility.Toggle.Editor
{
    [CustomEditor(typeof(BasicToggle))][CanEditMultipleObjects]
    public class BasicToggleEditor : BasicBaseEditor<BasicToggle>
    {
	
        // **************************************** VARIABLES ****************************************\\

        private bool _editorRaise;
        
        protected override bool UpdateConstantlyInPlayMode => true;
        
	
        // ****************************************  METHODS  ****************************************\\

        protected override void AfterApplyingModifiedProperties()
        {
            base.AfterApplyingModifiedProperties();

            if (!_editorRaise) return;
            _editorRaise = false;
				
            foreach (var typedTarget in TypedTargets)
            {
                typedTarget.EditorRaise();
            }
        }

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
            
            var raiseWhileDisabled = serializedObject.FindProperty("_raiseWhileDisabled");
            var isOn = serializedObject.FindProperty("_isOn");
            var isOnChanged = serializedObject.FindProperty("_isOnChanged");
			
            EditorGUILayout.PropertyField(raiseWhileDisabled);
            
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(isOn);
            if (EditorGUI.EndChangeCheck())
            {
                _editorRaise = true;
            }
			
            EditorGUILayout.PropertyField(isOnChanged, true);
        }
        
    }
}