using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle.Editor
{
	[CustomEditor(typeof(BoolSingleToggleGroup))][CanEditMultipleObjects]
	public class BoolSingleToggleGroupEditor : BasicBaseEditor<BoolSingleToggleGroup> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawSettingFields();
		}
		
		private void DrawSettingFields()
		{
			SectionHeader("Settings");
			
			var maximumSelectedAtOnce = serializedObject.FindProperty("_maximumSelectedAtOnce");
			var allowNoneSelected = serializedObject.FindProperty("_allowNoneSelected");
			var startBoolSingles = serializedObject.FindProperty("_startBoolSingles");

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(maximumSelectedAtOnce);
			if (EditorGUI.EndChangeCheck() && Application.isPlaying)
			{
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.MaximumSelectedAtOnce = maximumSelectedAtOnce.intValue;
				}
			}

			if (maximumSelectedAtOnce.intValue < 0)
			{
				maximumSelectedAtOnce.intValue = 0;
			}
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(allowNoneSelected);
			if (EditorGUI.EndChangeCheck() && Application.isPlaying)
			{
				foreach (var typedTarget in TypedTargets)
				{
					typedTarget.AllowNoneSelected = allowNoneSelected.boolValue;
				}
			}

			Space();
			
			if (!Application.isPlaying)
			{
				EditorGUILayout.PropertyField(startBoolSingles, true);
			}
			else
			{
				SectionHeader(GetGuiContent("Managed Bool Single"));
				foreach (var boolSingle in TypedTarget.ManagedBoolSingles)
				{
					boolSingle.Value = EditorGUILayout.Toggle(boolSingle.name, boolSingle.Value);
				}
			}
		}

		protected override void DrawEventFields()
		{
			SectionHeader("Events");
			
			var boolSingleSelected = serializedObject.FindProperty("_boolSingleSelected");
			var boolSingleDeselected = serializedObject.FindProperty("_boolSingleDeselected");

			EditorGUILayout.PropertyField(boolSingleSelected);
			EditorGUILayout.PropertyField(boolSingleDeselected);
		}
		
	}
}