using System.Collections.Generic;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle
{
	[AddComponentMenu("Basic Framework/Core/Utility/Toggle/Bool Single Toggle Group")]
	public class BoolSingleToggleGroup : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The maximum number of Basic Toggles that can be on at once, 0 => unlimited")]
        [SerializeField] private int _maximumSelectedAtOnce = 1;
        
        [Tooltip("Is it possible for none of the toggles to be on")]
        [SerializeField] private bool _allowNoneSelected = default;
        
        [Tooltip("These BoolSingles will be added to the group on start")]        
        [SerializeField] private BoolSingle[] _startBoolSingles = new BoolSingle[0];

        [Tooltip("Raised when a BoolSingles value is changed to true")]
        [SerializeField] private BoolSingleUnityEvent _boolSingleSelected = default;
        
        [Tooltip("Raised when a BoolSingles value is changed to false")]
        [SerializeField] private BoolSingleUnityEvent _boolSingleDeselected = default;
        
        public int MaximumSelectedAtOnce
        {
            get => _maximumSelectedAtOnce;
            set
            {
                if (value < 0) value = 0;
                if (_maximumSelectedAtOnce == value) return;
                _maximumSelectedAtOnce = value;

                if (!enabled) return;
                if (value <= 0) return;
                for (int i = _selectedBoolSingles.Count - 1; i >= value; i--)
                {
                    _selectedBoolSingles[i].Value = false;
                }
            }
        }

        public bool AllowNoneSelected
        {
            get => _allowNoneSelected;
            set
            {
                if (_allowNoneSelected == value) return;
                _allowNoneSelected = value;
                
                if (!enabled) return;
                if (_allowNoneSelected) return;
                if (_managedBoolSingles.Count == 0) return;
                if (_selectedBoolSingles.Count > 0) return;
                _managedBoolSingles[0].Value = true;
            }
        }
        
        private readonly List<BoolSingle> _managedBoolSingles = new List<BoolSingle>();
        
        public BoolSingle[] ManagedBoolSingles => _managedBoolSingles.ToArray();
        
        private readonly List<BoolSingle> _selectedBoolSingles = new List<BoolSingle>();
        
	
		// ****************************************  METHODS  ****************************************\\
	
		private void OnEnable()
		{
			if (_managedBoolSingles.Count == 0) return;
			
			_selectedBoolSingles.Clear();
			
			foreach (var managedBoolSingle in _managedBoolSingles)
			{
				if (managedBoolSingle.Value)
				{
					if (_selectedBoolSingles.Count >= _maximumSelectedAtOnce)
					{
						managedBoolSingle.Value = false;
					}
					else
					{
						_selectedBoolSingles.Add(managedBoolSingle);
					}
				}

				managedBoolSingle.ValueChanged.Subscribe(BoolSingleValueChanged);
			}

			if (_allowNoneSelected || _selectedBoolSingles.Count != 0) return;
			_managedBoolSingles[0].Value = true;
		}

		private void OnDisable()
		{
			foreach (var managedBoolSingle in _managedBoolSingles)
			{
				managedBoolSingle.ValueChanged.Unsubscribe(BoolSingleValueChanged);
			}
			
			_selectedBoolSingles.Clear();
		}

		private void Start()
		{
			foreach (var toggle in _startBoolSingles)
			{
				AddBoolSingleToggle(toggle);
			}
		}

		public void AddBoolSingleToggle(BoolSingle boolSingle)
		{
			if (boolSingle == null) return;
			if (_managedBoolSingles.Contains(boolSingle)) return;
			
			_managedBoolSingles.Add(boolSingle);

			if (!enabled) return;
			
			if (!_allowNoneSelected && _selectedBoolSingles.Count == 0)
			{
				boolSingle.Value = true;
			}
			
			boolSingle.ValueChanged.Subscribe(BoolSingleValueChanged);
			
			BoolSingleValueChanged(boolSingle);
		}

		public void RemoveBoolSingleToggle(BoolSingle boolSingle)
		{
			if (boolSingle == null) return;
			if (!_managedBoolSingles.Remove(boolSingle)) return;

			_selectedBoolSingles.Remove(boolSingle);
			
			if (!enabled) return;
			
			boolSingle.ValueChanged.Unsubscribe(BoolSingleValueChanged);
			
			if (_managedBoolSingles.Count == 0) return;

			if (_allowNoneSelected || _selectedBoolSingles.Count > 0) return;

			_managedBoolSingles[0].Value = true;
		}
		
		public void DeselectAll()
		{
			foreach (var managedBoolSingles in _managedBoolSingles)
			{
				managedBoolSingles.Value = false;
			}
		}
		
		private void BoolSingleValueChanged(ScriptableSingleBase<bool> boolSingle)
		{
			var typedBoolSingle = boolSingle as BoolSingle;
			if (typedBoolSingle == null)
			{
				Debug.LogError($"{name} => {typeof(BoolSingleToggleGroup).Name} can not convert {boolSingle.name} to {typeof(BoolSingle).Name}");
				return;
			}
			
			if (!_managedBoolSingles.Contains(typedBoolSingle))
			{
				Debug.LogError($"{name} => {typeof(BoolSingleToggleGroup).Name} is not managing {boolSingle.name} {typeof(BoolSingle).Name}");
				return;
			}
			
			if (boolSingle.Value)
			{
				if (_selectedBoolSingles.Contains(typedBoolSingle)) return;
				
				_selectedBoolSingles.Add(typedBoolSingle);

				if (_maximumSelectedAtOnce > 0 && _selectedBoolSingles.Count > _maximumSelectedAtOnce)
				{
					_selectedBoolSingles[0].Value = false;
				}

				_boolSingleSelected.Invoke(typedBoolSingle);
				return;
			}
			
			if (!_selectedBoolSingles.Contains(typedBoolSingle)) return;
			
			if(!_allowNoneSelected && _selectedBoolSingles.Count <= 1)
			{
				boolSingle.Value = true;
				return;
			}

			_selectedBoolSingles.Remove(typedBoolSingle);
			
			_boolSingleDeselected.Invoke(typedBoolSingle);
		}
		
	}
}