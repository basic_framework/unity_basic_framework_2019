﻿using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.Utility.Toggle
{
	[AddComponentMenu("Basic Framework/Core/Utility/Toggle/Game Object Radio Group")]
	public class GameObjectRadioGroup : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The list of gameobjects that are being managed. " +
		         "Can only activate a gameobject if it is in the list.")]
		[SerializeField] private List<GameObject> _gameObjects = new List<GameObject>();
		
		[Tooltip("The currently active GameObject")]
		[SerializeField] private GameObject _activeGameObject;
		
		[Tooltip("Allow no GameObject to be selected, i.e. allow all inactive")]
		[SerializeField] private bool _allowNoneSelected = true;
		
		
		[Tooltip("Should the active gameobject be set on start")]
		[SerializeField] private bool _setActiveGameObjectOnStart = default;
		
		[Tooltip("If the active gameobject is null, this GameObject will be set active on start")]
		[SerializeField] private GameObject _startActiveGameObject = default;

		

		[SerializeField] private GameObjectUnityEvent _activeGameObjectChanged = default;
		[SerializeField] private GameObjectCompareUnityEvent[] _activeGameObjectChangedCompare = default;

		
		public GameObjectUnityEvent ActiveGameObjectChanged => _activeGameObjectChanged;

		
		
		public GameObject ActiveGameObject
		{
			get => _activeGameObject;
			private set
			{
				if (!_gameObjects.Contains(value))
				{
					value = null;
				}

				if (!_allowNoneSelected && value == null ) return;
				
				if (_activeGameObject == value) return;
				_activeGameObject = value;
				ActiveGameObjectUpdated(_activeGameObject);
			}
		}

		private bool _activeGameObjectSet;


		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		private void OnValidate()
		{
			UpdateActiveGameObject();
		}

		private void Start()
		{
			if (!_activeGameObjectSet && _setActiveGameObjectOnStart)
			{
				_activeGameObject = _startActiveGameObject;
			}

			if (!_allowNoneSelected && _activeGameObject == null && _gameObjects.Count != 0)
			{
				_activeGameObject = _gameObjects[0];
			}
			
			ActiveGameObjectUpdated(_activeGameObject);
		}


		//***** property updates *****\\
		
		private void ActiveGameObjectUpdated(GameObject activeGameObject)
		{
			_activeGameObjectSet = true;
			
			UpdateActiveGameObject();
			
			_activeGameObjectChanged.Invoke(activeGameObject);

			foreach (var compareUnityEvent in _activeGameObjectChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(activeGameObject);
			}
		}
		
		
		//***** modifying gameobject list *****\\
		
		public void ClearGameObjects()
		{
			_gameObjects.Clear();
		}
		
		public void AddGameObject(GameObject otherGameObject)
		{
			if (otherGameObject == null) return;
			if (otherGameObject == gameObject) return;
			if (_gameObjects.Contains(otherGameObject)) return;
			_gameObjects.Add(otherGameObject);
		}

		public void RemoveGameObject(GameObject otherGameObject)
		{
			if (!_gameObjects.Contains(otherGameObject)) return;
			_gameObjects.Remove(otherGameObject);
			
			if (otherGameObject != _activeGameObject) return;
			ActiveGameObject = _allowNoneSelected || _gameObjects.Count == 0 ? null : _gameObjects[0];
		}
		
		public void AddAndSetActiveGameObject(GameObject otherGameObject)
		{
			AddGameObject(otherGameObject);
			SetActiveGameObject(otherGameObject);
		}
		
		
		//***** functions *****\\

		private void UpdateActiveGameObject()
		{
			foreach (var go in _gameObjects)
			{
				go.SetActive(go == _activeGameObject);
			}
		}
		
		public void SetActiveGameObject(GameObject otherGameObject)
		{
			if (otherGameObject != null && !_gameObjects.Contains(otherGameObject))
			{
				// TODO: Log
				Debug.LogWarning($"{name}/{GetType().Name} => " +
				                 $"Can not set active GameObject to {otherGameObject.name} as it is not in the list");
				return;
			}
			
			ActiveGameObject = otherGameObject;
		}
		
	}
}