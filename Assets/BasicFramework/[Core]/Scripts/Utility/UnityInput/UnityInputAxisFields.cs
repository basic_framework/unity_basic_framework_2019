﻿

namespace BasicFramework.Core.Utility.UnityInput
{
    public enum UnityInputAxisTypes
    {
        KeyOrMouseButton = 0,
        MouseMovement = 1,
        JoystickAxis = 2
    }
		
    public class UnityInputAxisFields
    {
		
        // **************************************** VARIABLES ****************************************\\
        
        public string Name;
		
        public string DescriptiveName;
        public string DescriptiveNegativeName;
        public string NegativeButton;
        public string PositiveButton;
        public string AltNegativeButton;
        public string AltPositiveButton;
			
        public float Gravity;
        public float Dead;
        public float Sensitivity;
		
        public bool Snap = false;
        public bool Invert = false;
		
        public UnityInputAxisTypes Type;
		
        public int Axis;
        public int JoyNum;
        
        
        // **************************************** METHODS ****************************************\\

        
    }
}