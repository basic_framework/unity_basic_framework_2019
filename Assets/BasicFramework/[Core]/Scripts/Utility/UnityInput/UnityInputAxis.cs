using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput
{
    [Serializable]
    public struct UnityInputAxis 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        public static UnityInputAxis Default = new UnityInputAxis("", false, 1);
		
        [SerializeField] private string _axisName;
        [SerializeField] private bool _useRaw;
        [SerializeField] private float _scaler;

        private bool _axisExists;
        private bool _axisChecked;

        public bool AxisExists => _axisExists;

        public string AxisName => _axisName;
        public bool UseRaw => _useRaw;
        public float Scaler => _scaler;

        public float Value
        {
            get
            {
                if (!_axisChecked)
                {
                    _axisChecked = true;
                    _axisExists = AxisName != "" && UnityInputUtility.IsAxisInUnityInputManager(AxisName, Application.isEditor);                
                }

                if (!_axisExists) return 0;
				
                return (_useRaw ? Input.GetAxisRaw(_axisName) : Input.GetAxis(_axisName)) * _scaler;
            }
        }


        // ****************************************  METHODS  ****************************************\\
		
        public UnityInputAxis(string axisName, bool useRaw, float scaler)
        {
            _axisName = axisName;
            _useRaw = useRaw;
            _scaler = scaler;
            _axisExists = false;
            _axisChecked = false;
        }

        public void ResetAxisExistsCheck()
        {
            _axisChecked = false;
        }
    }
}