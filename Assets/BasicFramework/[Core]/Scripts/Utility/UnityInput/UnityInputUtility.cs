using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput
{
    public static class UnityInputUtility
    {
        
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        public static bool IsAxisInUnityInputManager(string axisName, bool logWarning = false)
        {
            try
            {
                Input.GetButton(axisName);
                return true;
            }
            catch (Exception exception)
            {
                if (logWarning)
                {
                    Debug.LogWarning(exception);
                }
		
                return false;
            }
        }
        
        
    }
}