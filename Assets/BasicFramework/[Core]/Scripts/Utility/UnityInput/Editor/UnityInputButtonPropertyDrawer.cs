using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput.Editor
{
    [CustomPropertyDrawer(typeof(UnityInputButton), true)]
    public class UnityInputButtonPropertyDrawer : BasicBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\

        private const float KEY_STATE_WIDTH = 90f; 
        
	
        // ****************************************  METHODS  ****************************************\\
		
        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var buttonName = property.FindPropertyRelative("_buttonName");
            var state = property.FindPropertyRelative("_state");
            
            TempRect = EditorGUI.PrefixLabel(TempRect, label);
            TempRect.width -= KEY_STATE_WIDTH + BUFFER_WIDTH;
            
            UnityInputEditorUtility.UnityAxisNameField(TempRect, buttonName);

            TempRect.x += TempRect.width + BUFFER_WIDTH;
            TempRect.width = KEY_STATE_WIDTH;
            
            EditorGUI.PropertyField(TempRect, state, GUIContent.none);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return SingleLineHeight;
        }
        
    }
}