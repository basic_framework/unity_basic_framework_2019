namespace BasicFramework.Core.Utility.UnityInput.Editor
{
	public static class UnityInputControllerEditorUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private const float STICK_DEADZONE = 0.19f;
		private const float TRIGGER_DEADZONE = 0.1f;
		
		
		// TODO: Add axis bindings for mac os x and linux, use if endif compiler directives	
		// Note: Naming based on xbox 360 controller, mapping based on windows os
		
		// Null
		private const string NULL = "null_joy";		// Empty input slot when no input is wanted
		
		// Analog Sticks
		private const string LEFT_X = "ls_x_joy";		// Left Stick X (Axis 1)
		private const string LEFT_Y = "ls_y_joy";		// Left Stick Y (Axis 2)
		private const string LEFT_S = "ls_press_joy";	// Left Stick Press (Button 8)

		private const string RIGHT_X = "rs_x_joy";		// Right Stick X (Axis 4)
		private const string RIGHT_Y = "rs_y_joy";		// Right Stick Y (Axis 5)
		private const string RIGHT_S = "rs_press_joy";	// Right Stick Press (Button 9)
		
		// Shoulders
		private const string LB = "lb_joy";		// Left Bumper (Button 4)
		private const string RB = "rb_joy";		// Right Bumper (Button 5)
		private const string LT = "lt_joy";		// Left Trigger (Axis 9)
		private const string RT = "rt_joy";		// Right Trigger (Axis 10)
		
		
		// D-Pad
		private const string DPAD_X = "dx_joy";	// D-Pad X (Axis 6)
		private const string DPAD_Y = "dy_joy"; // D-Pad Y (Axis 7)
		
//		private const string DPAD_UP = "up_joy";
//		private const string DPAD_DOWN = "down_joy";
//		private const string DPAD_LEFT = "left_joy";
//		private const string DPAD_RIGHT = "right_joy";
		
		// Face Buttons
		private const string FA = "fa_joy";			// Face Button A (Button 0)
		private const string FB = "fb_joy";			// Face Button B (Button 1)
		private const string FX = "fx_joy";			// Face Button X (Button 2)
		private const string FY = "fy_joy";			// Face Button Y (Button 3)
		private const string BACK = "back_joy";		// Back Button (Button 6)
		private const string START = "start_joy";	// Start Button (Button 7)
		
//		private const string XBOX = "xbox_bf_joy";
	
		// ****************************************  METHODS  ****************************************\\
	
		//***** Auto Setup InputManager *****\\
		
		public static void AddControllerAxes()
		{
			// Note: These mappings are for windows only, need to be changed for linux/mac
			
			//Empty
			UnityInputEditorUtility.AddJoystickButton(NULL, -1);
			
			// Analog Sticks
			UnityInputEditorUtility.AddJoystickAxis(LEFT_X, 1, false, STICK_DEADZONE);
			UnityInputEditorUtility.AddJoystickAxis(LEFT_Y, 2, true, STICK_DEADZONE);
			UnityInputEditorUtility.AddJoystickButton(LEFT_S, 8);
			
			UnityInputEditorUtility.AddJoystickAxis(RIGHT_X, 4, false, STICK_DEADZONE);
			UnityInputEditorUtility.AddJoystickAxis(RIGHT_Y, 5, true, STICK_DEADZONE);
			UnityInputEditorUtility.AddJoystickButton(RIGHT_S, 9);
			
			// D-Pad
			UnityInputEditorUtility.AddJoystickAxis(DPAD_X, 6);
			UnityInputEditorUtility.AddJoystickAxis(DPAD_Y, 7);
			
			// Shoulder
			UnityInputEditorUtility.AddJoystickAxis(LT, 9, false, TRIGGER_DEADZONE);
			UnityInputEditorUtility.AddJoystickAxis(RT, 10, false, TRIGGER_DEADZONE);
			
			UnityInputEditorUtility.AddJoystickButton(LB, 4);
			UnityInputEditorUtility.AddJoystickButton(RB, 5);
						
			// Buttons
			UnityInputEditorUtility.AddJoystickButton(FA, 0);
			UnityInputEditorUtility.AddJoystickButton(FB, 1);
			UnityInputEditorUtility.AddJoystickButton(FX, 2);
			UnityInputEditorUtility.AddJoystickButton(FY, 3);
			
			UnityInputEditorUtility.AddJoystickButton(BACK, 6);
			UnityInputEditorUtility.AddJoystickButton(START, 7);
		}
		
		public static void RemoveControllerAxes()
		{
			//Empty
			UnityInputEditorUtility.RemoveAxis(NULL);
			
			// Analog Sticks
			UnityInputEditorUtility.RemoveAxis(LEFT_X);
			UnityInputEditorUtility.RemoveAxis(LEFT_Y);
			UnityInputEditorUtility.RemoveAxis(LEFT_S);
			
			UnityInputEditorUtility.RemoveAxis(RIGHT_X);
			UnityInputEditorUtility.RemoveAxis(RIGHT_Y);
			UnityInputEditorUtility.RemoveAxis(RIGHT_S);
			
			// D-Pad
			UnityInputEditorUtility.RemoveAxis(DPAD_X);
			UnityInputEditorUtility.RemoveAxis(DPAD_Y);
			
			// Shoulder
			UnityInputEditorUtility.RemoveAxis(LT);
			UnityInputEditorUtility.RemoveAxis(RT);
			
			UnityInputEditorUtility.RemoveAxis(LB);
			UnityInputEditorUtility.RemoveAxis(RB);

			// Buttons
			UnityInputEditorUtility.RemoveAxis(FA);
			UnityInputEditorUtility.RemoveAxis(FB);
			UnityInputEditorUtility.RemoveAxis(FX);
			UnityInputEditorUtility.RemoveAxis(FY);
			
			UnityInputEditorUtility.RemoveAxis(BACK);
			UnityInputEditorUtility.RemoveAxis(START);
		}
		
	}
}