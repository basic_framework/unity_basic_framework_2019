using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput.Editor
{
	[CustomPropertyDrawer(typeof(UnityInputAxis), true)]
	public class UnityInputAxisPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private static readonly GUIContent RawGuiContent = new GUIContent("R", "Use the raw value for this axis");
		private const float RAW_LABEL_WIDTH = 12f;
		private const float RAW_FIELD_WIDTH = 25f;
		
		private static readonly GUIContent ScaleGuiContent = new GUIContent("S", "The scaler value that will be applied to the axis");
		private const float SCALE_LABEL_WIDTH = 14f;
		private const float SCALE_FIELD_WIDTH = 60f;
	
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var axisName = property.FindPropertyRelative("_axisName");
			var useRaw = property.FindPropertyRelative("_useRaw");
			var scaler = property.FindPropertyRelative("_scaler");

			position = EditorGUI.PrefixLabel(position, label);

			var nameRect = new Rect(
				position.x, 
				position.y, 
				position.width - (BUFFER_WIDTH * 2) - RAW_FIELD_WIDTH - SCALE_FIELD_WIDTH,
				position.height
			);
			
			var rawRect = new Rect(
				nameRect.xMax + BUFFER_WIDTH,
				nameRect.position.y,
				RAW_FIELD_WIDTH,
				nameRect.height
			);
			
			var scalerRect = new Rect(
				rawRect.xMax + BUFFER_WIDTH,
				rawRect.position.y,
				SCALE_FIELD_WIDTH,
				nameRect.height
			);

			var prevLabelWidth = EditorGUIUtility.labelWidth; 
			
			UnityInputEditorUtility.UnityAxisNameField(nameRect, axisName);
			
			EditorGUIUtility.labelWidth = RAW_LABEL_WIDTH;
			EditorGUI.PropertyField(rawRect, useRaw, RawGuiContent);

			EditorGUIUtility.labelWidth = SCALE_LABEL_WIDTH;
			EditorGUI.PropertyField(scalerRect, scaler, ScaleGuiContent);
			
			EditorGUIUtility.labelWidth = prevLabelWidth;		
		}
		
	}
}