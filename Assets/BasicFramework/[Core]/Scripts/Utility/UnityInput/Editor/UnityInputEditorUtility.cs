using System.Collections.Generic;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput.Editor
{
    public static class UnityInputEditorUtility
    {
        // **************************************** VARIABLES ****************************************\\
		
        private const float AXIS_NAME_LABEL_WIDTH = 35f;
		
		private static readonly GUIContent RawGuiContent = new GUIContent("R", "Use the raw value for this axis");
		private const float RAW_LABEL_WIDTH = 12f;
		private const float RAW_FIELD_WIDTH = 25f;
		
		private static readonly GUIContent ScaleGuiContent = new GUIContent("S", "The scaler value that will be applied to the axis");
		private const float SCALE_LABEL_WIDTH = 14f;
		private const float SCALE_FIELD_WIDTH = 60f;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		public static void UnityAxisNameField(Rect position, SerializedProperty axisName)
		{
			var names = GetInputManagerNames();
			var selected = -1;

			for (int i = 0; i < names.Length; i++)
			{
				if (names[i] != axisName.stringValue) continue;
				selected = i;
				break;
			}
			
			selected = EditorGUI.Popup(position, selected, names);

			if ((selected < 0 || selected >= names.Length) && !string.IsNullOrEmpty(axisName.stringValue))
			{
				var prevEnabled = GUI.enabled;
				GUI.enabled = false;

				var prevColor = EditorStyles.label.normal.textColor;
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.boldLabel, 	BasicEditorStyles.Colors.LightRed);
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.label, 		BasicEditorStyles.Colors.LightRed);
			
				EditorGUILayout.TextField("^ Current", axisName.stringValue, EditorStyles.boldLabel);
				
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.boldLabel, prevColor);
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.label, prevColor);
				GUI.enabled = prevEnabled;
			}
			
			if (selected >= 0 && selected < names.Length)
			{
				axisName.stringValue = names[selected];
			}
			
		}

		public static void UnityAxisNameField(SerializedProperty axisName)
		{
			// Does not change string if it does not appear in list
			var names = GetInputManagerNames();
			var selected = -1;

			for (int i = 0; i < names.Length; i++)
			{
				if (names[i] != axisName.stringValue) continue;
				selected = i;
				break;
			}

			selected = EditorGUILayout.Popup(axisName.displayName, selected, names);
			
			if ((selected < 0 || selected >= names.Length) && !string.IsNullOrEmpty(axisName.stringValue))
			{
				var prevEnabled = GUI.enabled;
				GUI.enabled = false;

				var prevColor = EditorStyles.label.normal.textColor;
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.boldLabel, 	BasicEditorStyles.Colors.LightRed);
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.label, 		BasicEditorStyles.Colors.LightRed);
			
				EditorGUILayout.TextField("^ Current", axisName.stringValue, EditorStyles.boldLabel);
				
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.boldLabel, prevColor);
				BasicEditorStyles.ChangeGuiStyleTextColor(EditorStyles.label, prevColor);
				GUI.enabled = prevEnabled;
			}

			if (selected >= 0 && selected < names.Length)
			{
				axisName.stringValue = names[selected];
			}
		}
		
		public static void UnityAxisNameField(SerializedProperty axisName, SerializedProperty useRaw)
		{
			var prevLabelWidth = EditorGUIUtility.labelWidth;
			
			EditorGUILayout.BeginHorizontal();
			
			UnityAxisNameField(axisName);
			
			EditorGUIUtility.labelWidth = RAW_LABEL_WIDTH;
			EditorGUILayout.PropertyField(useRaw, RawGuiContent, GUILayout.MaxWidth(RAW_FIELD_WIDTH));
			EditorGUIUtility.labelWidth = prevLabelWidth;
			
			EditorGUILayout.EndHorizontal();
		}
		
		public static void UnityAxisNameField(SerializedProperty axisName, SerializedProperty useRaw, SerializedProperty scaler)
		{
			var prevLabelWidth = EditorGUIUtility.labelWidth;
			
			EditorGUILayout.BeginHorizontal();
			
			UnityAxisNameField(axisName);
			
			EditorGUIUtility.labelWidth = RAW_LABEL_WIDTH;
			EditorGUILayout.PropertyField(useRaw, RawGuiContent, GUILayout.MaxWidth(RAW_FIELD_WIDTH));
			
			EditorGUIUtility.labelWidth = SCALE_LABEL_WIDTH;
			EditorGUILayout.PropertyField(scaler, ScaleGuiContent, GUILayout.MaxWidth(SCALE_FIELD_WIDTH));
			
			EditorGUIUtility.labelWidth = prevLabelWidth;
			
			EditorGUILayout.EndHorizontal();
		}
		
		
		/// <summary>Gets the list of Axes names contained in Unity InputManager</summary>
		/// <returns>An array containing the Axes names in Unity InputManager</returns>
		public static string[] GetInputManagerNames()
		{
			// TODO: Some null checking
			
			var inputManagerSerializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
			var axesProperty = inputManagerSerializedObject.FindProperty("m_Axes");
			
			var names = new List<string>();

			axesProperty.Next(true);
			axesProperty.Next(true);
			
			while (axesProperty.Next(false))
			{
				var axis = axesProperty.Copy();
				axis.Next(true);

				if (names.Contains(axis.stringValue)) continue;
				names.Add(axis.stringValue);
			}
			
			return names.ToArray();
		}

		public static void AddJoystickAxis(string name, int axisId, bool invert = false, float dead = 0.19f, int joyNum = 0)
		{
			AddAxis(new UnityInputAxisFields
			{
				Name = name,
				Type = UnityInputAxisTypes.JoystickAxis,
				Axis = axisId,
				Invert = invert,
				Gravity = 0,
				Sensitivity = 1,
				Dead = dead,
				JoyNum = joyNum
			});
		}

		public static void AddJoystickButton(string name, int buttonId, int joyNum = 0)
		{
			AddAxis(new UnityInputAxisFields
			{
				Name = name,
				PositiveButton = "joystick button " + buttonId,
				Gravity = 1000,
				Sensitivity = 1000,
				Dead = 0.001f,
				JoyNum = joyNum
			});
		}
		
		public static bool AxisDefined(string axisName)
		{
			var serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
			var axesProperty = serializedObject.FindProperty("m_Axes");

			axesProperty.Next(true);
			axesProperty.Next(true);
			while (axesProperty.Next(false))
			{
				var axis = axesProperty.Copy();
				axis.Next(true);
				if (axis.stringValue == axisName) return true;
			}
			return false;
		}
		
		public static void AddAxis(UnityInputAxisFields axisFields, bool multipleEntries = false)
		{
			if (axisFields == null) return;
			
			var axisDefined = AxisDefined(axisFields.Name);
			
			if (!multipleEntries && axisDefined) return;
					
			var serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
			var axesProperty = serializedObject.FindProperty("m_Axes");
			
			serializedObject.Update();
			axesProperty.arraySize++;
			serializedObject.ApplyModifiedProperties();
		
			serializedObject.Update();
			
			var axisProperty = axesProperty.GetArrayElementAtIndex(axesProperty.arraySize - 1);
		
			BasicEditorUtility.GetChildProperty(axisProperty, "m_Name").stringValue = axisFields.Name;
			BasicEditorUtility.GetChildProperty(axisProperty, "descriptiveName").stringValue = axisFields.DescriptiveName;
			BasicEditorUtility.GetChildProperty(axisProperty, "descriptiveNegativeName").stringValue = axisFields.DescriptiveNegativeName;
			BasicEditorUtility.GetChildProperty(axisProperty, "negativeButton").stringValue = axisFields.NegativeButton;
			BasicEditorUtility.GetChildProperty(axisProperty, "positiveButton").stringValue = axisFields.PositiveButton;
			BasicEditorUtility.GetChildProperty(axisProperty, "altNegativeButton").stringValue = axisFields.AltNegativeButton;
			BasicEditorUtility.GetChildProperty(axisProperty, "altPositiveButton").stringValue = axisFields.AltPositiveButton;
			BasicEditorUtility.GetChildProperty(axisProperty, "gravity").floatValue = axisFields.Gravity;
			BasicEditorUtility.GetChildProperty(axisProperty, "dead").floatValue = axisFields.Dead;
			BasicEditorUtility.GetChildProperty(axisProperty, "sensitivity").floatValue = axisFields.Sensitivity;
			BasicEditorUtility.GetChildProperty(axisProperty, "snap").boolValue = axisFields.Snap;
			BasicEditorUtility.GetChildProperty(axisProperty, "invert").boolValue = axisFields.Invert;
			BasicEditorUtility.GetChildProperty(axisProperty, "type").intValue = (int)axisFields.Type;
			BasicEditorUtility.GetChildProperty(axisProperty, "axis").intValue = axisFields.Axis - 1;
			BasicEditorUtility.GetChildProperty(axisProperty, "joyNum").intValue = axisFields.JoyNum;
		
			serializedObject.ApplyModifiedProperties();
		}

		public static SerializedProperty[] GetAxis(string axisName)
		{
			var axes = new List<SerializedProperty>();
						
			var inputManagerSerializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
			var axesProperty = inputManagerSerializedObject.FindProperty("m_Axes");

			for (int i = 0; i < axesProperty.arraySize; i++)
			{
				var axisProperty = axesProperty.GetArrayElementAtIndex(i);
				var name = BasicEditorUtility.GetChildProperty(axisProperty, "m_Name").stringValue;
				if(name != axisName) continue;
				axes.Add(axisProperty);
			}
			
			return axes.ToArray();
		}
		
		public static UnityInputAxisFields[] GetUnityAxis(string axisName)
		{
			var axes = GetAxis(axisName);

			var unityAxes = new List<UnityInputAxisFields>();

			foreach (var axis in axes)
			{
				unityAxes.Add(new UnityInputAxisFields
				{
					Name 					= BasicEditorUtility.GetChildProperty(axis, "m_Name").stringValue,
					DescriptiveName 		= BasicEditorUtility.GetChildProperty(axis, "descriptiveName").stringValue,
					DescriptiveNegativeName = BasicEditorUtility.GetChildProperty(axis, "descriptiveNegativeName").stringValue,
					NegativeButton 			= BasicEditorUtility.GetChildProperty(axis, "negativeButton").stringValue,
					PositiveButton 			= BasicEditorUtility.GetChildProperty(axis, "positiveButton").stringValue,
					AltNegativeButton 		= BasicEditorUtility.GetChildProperty(axis, "altNegativeButton").stringValue,
					AltPositiveButton 		= BasicEditorUtility.GetChildProperty(axis, "altPositiveButton").stringValue,
					Gravity 				= BasicEditorUtility.GetChildProperty(axis, "gravity").floatValue,
					Dead 					= BasicEditorUtility.GetChildProperty(axis, "dead").floatValue,
					Sensitivity 			= BasicEditorUtility.GetChildProperty(axis, "sensitivity").floatValue,
					Snap 					= BasicEditorUtility.GetChildProperty(axis, "snap").boolValue,
					Invert 					= BasicEditorUtility.GetChildProperty(axis, "invert").boolValue,
					Type 					= (UnityInputAxisTypes) BasicEditorUtility.GetChildProperty(axis, "type").intValue,
					Axis 					= BasicEditorUtility.GetChildProperty(axis, "axis").intValue + 1,
					JoyNum 					= BasicEditorUtility.GetChildProperty(axis, "joyNum").intValue
				});
			}
			
			return unityAxes.ToArray();
		}

		public static void RemoveAxis(string axisName, bool removeAll = true)
		{
			var serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
			var axesProperty = serializedObject.FindProperty("m_Axes");
			
			serializedObject.Update();
			
			for (var i = axesProperty.arraySize - 1; i >= 0; i--)
			{
				var axisProperty = axesProperty.GetArrayElementAtIndex(i);

				if (axisName != BasicEditorUtility.GetChildProperty(axisProperty, "m_Name").stringValue) continue;
				BasicEditorArrayUtility.RemoveArrayElement(axesProperty, i);
				if (!removeAll) break;
			}

			serializedObject.ApplyModifiedProperties();
		}
    }
}