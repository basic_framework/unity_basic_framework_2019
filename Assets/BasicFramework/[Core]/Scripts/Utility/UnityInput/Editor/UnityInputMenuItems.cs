using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.Utility.UnityInput.Editor
{
    public static class UnityInputMenuItems
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        [MenuItem(BasicEditorUtility.MENU_ITEM_ROOT + "Input/Core/Add Controller Axes", priority = 0)]
        public static void AddControllerAxes()
        {
            UnityInputControllerEditorUtility.AddControllerAxes();
        }
		
        [MenuItem(BasicEditorUtility.MENU_ITEM_ROOT + "Input/Core/Remove Controller Axes", priority = 1)]
        public static void RemoveControllerAxes()
        {
            UnityInputControllerEditorUtility.RemoveControllerAxes();
        }
	
    }
}