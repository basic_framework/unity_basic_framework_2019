using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput
{
    [Serializable]
    public struct UnityInputAxisSplit 
    {
	
        // **************************************** VARIABLES ****************************************\\
		
        public static UnityInputAxisSplit Default = 
            new UnityInputAxisSplit(UnityInputAxis.Default, UnityInputAxis.Default);

        [Tooltip("Uses the actual input value instead of its absolute. " +
                 "Clamps depending if positive or negative value. " +
                 "Example: Positive raw value is -1, gets clamped to 0 b/c it is being used as the positive value")]
        [SerializeField] private bool _useRawInputValues;
		
        [Tooltip("The unity input axis used for the positive value")]
        [SerializeField] private UnityInputAxis _positiveAxis;
        
        [Tooltip("The unity input axis used for the negative value")]
        [SerializeField] private UnityInputAxis _negativeAxis;
        
	
        // ****************************************  METHODS  ****************************************\\

        public UnityInputAxisSplit(UnityInputAxis negativeAxis,
            UnityInputAxis positiveAxis, bool useRawInputValues = false)
        {
            _negativeAxis = negativeAxis;
            _positiveAxis = positiveAxis;
            _useRawInputValues = useRawInputValues;
        }

        public float Value
        {
            get
            {
                var positiveValue = _useRawInputValues ? _positiveAxis.Value : Mathf.Abs(_positiveAxis.Value);
                if (positiveValue > 0)
                {
                    return positiveValue;
                }
				
                var negativeValue = _useRawInputValues ? _negativeAxis.Value : Mathf.Abs(_negativeAxis.Value) * -1f;
                if (negativeValue < 0)
                {
                    return negativeValue;
                }

                return 0;
            }
        }
	
    }
}