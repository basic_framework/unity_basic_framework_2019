﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Keyboard;
using UnityEngine;

namespace BasicFramework.Core.Utility.UnityInput
{
    [Serializable]
    public struct UnityInputButton 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        public static UnityInputButton Default = new UnityInputButton("", ButtonInputStates.Down);
		
        [SerializeField] private string _buttonName;
        [SerializeField] private ButtonInputStates _state;
		
        private bool _buttonExists;
        private bool _buttonChecked;

        public string ButtonName => _buttonName;
		
        public bool Value
        {
            get
            {
                if (!_buttonChecked)
                {
                    _buttonChecked = true;
                    _buttonExists = ButtonName != "" && UnityInputUtility.IsAxisInUnityInputManager(ButtonName, Application.isEditor);
                }

                if (!_buttonExists) return false;
				
                switch (_state)
                {
                    case ButtonInputStates.Down: 	return Input.GetButtonDown(_buttonName);
                    case ButtonInputStates.Held:	return Input.GetButton(_buttonName);
                    case ButtonInputStates.Up:	return Input.GetButtonUp(_buttonName);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
		

        // ****************************************  METHODS  ****************************************\\

        public UnityInputButton(string buttonName, ButtonInputStates state)
        {
            _buttonName = buttonName;
            _state = state;
            _buttonExists = false;
            _buttonChecked = false;
        }
		
        public void ResetButtonExistsCheck()
        {
            _buttonChecked = false;
        }

    }
}