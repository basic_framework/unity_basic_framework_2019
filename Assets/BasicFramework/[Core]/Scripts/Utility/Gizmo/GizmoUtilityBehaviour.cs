﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public enum GizmoShapeOptions
	{
		CubeWire,
		CubeMesh,
		SphereWire,
		SphereMesh
	}
	
	[AddComponentMenu("Basic Framework/Core/Utility/Gizmo Utility Behaviour")]
	public class GizmoUtilityBehaviour : MonoBehaviour 
	{
		
		// TODO: Extend so that you can choose what to draw
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Transform _transform = default;
		
		[Tooltip("Should the Gizmo be drawn even if the object is not selected")]
		[SerializeField] private bool _drawAlways = false;
		
		[SerializeField] private GizmoShapeOptions _gizmoShapeOption = default;
		
		[SerializeField] private float _size = 0.1f;
		
		[SerializeField] private Color _color = Color.magenta;
		
		
	
		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_transform = transform;
		}

		private void OnDrawGizmos()
		{
			if (!_drawAlways) return;
			if (_transform == null) return;
			DrawGizmos();
		}

		private void OnDrawGizmosSelected()
		{
			if (_drawAlways) return;
			if (_transform == null) return;
			DrawGizmos();
		}

		private void DrawGizmos()
		{
			var prevColor = Gizmos.color;
			Gizmos.color = _color;

			var position = _transform.position;
			var rotation = _transform.rotation;
			
			switch (_gizmoShapeOption)
			{
				case GizmoShapeOptions.CubeWire:
					Gizmos.DrawWireCube(position, Vector3.one * _size);
					break;
				
				case GizmoShapeOptions.CubeMesh:
					Gizmos.DrawCube(position, Vector3.one * _size);
					break;
				
				case GizmoShapeOptions.SphereWire:
					Gizmos.DrawWireSphere(position, _size * 0.5f);
					break;
				
				case GizmoShapeOptions.SphereMesh:
					Gizmos.DrawSphere(position, _size * 0.5f);
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			Gizmos.color = prevColor;
		}
	}
}