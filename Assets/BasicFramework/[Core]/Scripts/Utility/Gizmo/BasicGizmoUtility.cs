﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public static class BasicGizmoUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		public static void DrawCube(Vector3 center, Quaternion orientation, Vector3 size)
		{
			var up = orientation * Vector3.up;
			var right = orientation * Vector3.right;
			var forward = orientation * Vector3.forward;
			
			size.x = Mathf.Abs(size.x);
			size.y = Mathf.Abs(size.y);
			size.z = Mathf.Abs(size.z);

			var extents = size * 0.5f;
			
			var min = center - orientation * extents;
			var max = center + orientation * extents;
			
			
			var prevGizmoColor = Gizmos.color;
			
			Gizmos.color = Color.red;
			var start = min;
			var end = min + right * size.x;
			Gizmos.DrawLine(start, end);

			start += up * size.y;
			end += up * size.y;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - right * size.x;
			Gizmos.DrawLine(start, end);
			
			start -= up * size.y;
			end -= up * size.y;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = Color.green;
			start = min;
			end = min + up * size.y;
			Gizmos.DrawLine(start, end);

			start += forward * size.z;
			end += forward * size.z;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - up * size.y;
			Gizmos.DrawLine(start, end);
			
			start -= forward * size.z;
			end -= forward * size.z;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = Color.blue;
			start = min;
			end = min + forward * size.z;
			Gizmos.DrawLine(start, end);

			start += right * size.x;
			end += right * size.x;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - forward * size.z;
			Gizmos.DrawLine(start, end);
			
			start -= right * size.x;
			end -= right * size.x;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = prevGizmoColor;
		}
		
		public static void DrawBounds(Transform transform, Bounds bounds, Space space = Space.Self)
		{
			var up = transform.up;
			var right = transform.right;
			var forward = transform.forward;
			
			var lossyScale = transform.lossyScale;
			var size = bounds.extents * 2;
			size.x *= lossyScale.x;
			size.y *= lossyScale.y;
			size.z *= lossyScale.z;

			var min = space == Space.Self ? transform.TransformPoint(bounds.min) : bounds.min;
			var max = space == Space.Self ? transform.TransformPoint(bounds.max) : bounds.max;

			// Possibly simplify
//			switch (space)
//			{
//				case Space.World:
//					DrawCube(bounds.center, Quaternion.identity, size);
//					break;
//				
//				case Space.Self:
//					DrawCube(transform.TransformPoint(bounds.center), transform.rotation, size);
//					break;
//				
//				default:
//					throw new ArgumentOutOfRangeException(nameof(space), space, null);
//			}

			var prevGizmoColor = Gizmos.color;
			
			Gizmos.color = Color.red;
			var start = min;
			var end = min + right * size.x;
			Gizmos.DrawLine(start, end);

			start += up * size.y;
			end += up * size.y;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - right * size.x;
			Gizmos.DrawLine(start, end);
			
			start -= up * size.y;
			end -= up * size.y;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = Color.green;
			start = min;
			end = min + up * size.y;
			Gizmos.DrawLine(start, end);

			start += forward * size.z;
			end += forward * size.z;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - up * size.y;
			Gizmos.DrawLine(start, end);
			
			start -= forward * size.z;
			end -= forward * size.z;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = Color.blue;
			start = min;
			end = min + forward * size.z;
			Gizmos.DrawLine(start, end);

			start += right * size.x;
			end += right * size.x;
			Gizmos.DrawLine(start, end);
			
			start = max;
			end = max - forward * size.z;
			Gizmos.DrawLine(start, end);
			
			start -= right * size.x;
			end -= right * size.x;
			Gizmos.DrawLine(start, end);
			
			
			Gizmos.color = prevGizmoColor;
		}


		public static void DrawDistance(Vector3 start, Vector3 end, Color color, float endCapRayLength = 0.05f)
		{
			var direction = end - start;
			var rotation = MathUtility.Direction2Quaternion(direction);
			
			var colorOld = Gizmos.color;
			Gizmos.color = color;
			
			Gizmos.DrawLine(start, end);
			
			Gizmos.DrawRay(start, rotation * Vector3.up * endCapRayLength);
			Gizmos.DrawRay(start, rotation * Vector3.down 	* endCapRayLength);
			Gizmos.DrawRay(start, rotation * Vector3.right 	* endCapRayLength);
			Gizmos.DrawRay(start, rotation * Vector3.left 	* endCapRayLength);
			
			Gizmos.DrawRay(end, rotation * Vector3.up 	* endCapRayLength);
			Gizmos.DrawRay(end, rotation * Vector3.down 		* endCapRayLength);
			Gizmos.DrawRay(end, rotation * Vector3.right 		* endCapRayLength);
			Gizmos.DrawRay(end, rotation * Vector3.left 		* endCapRayLength);

			Gizmos.color = colorOld;
		}
		

		public static void DrawAxis(Transform transform, float length = 1, bool positiveAxisOnly = true)
		{
			DrawAxis(transform.position, transform.rotation, length, positiveAxisOnly);
		}
		
		public static void DrawAxis(Vector3 position, Quaternion rotation, float length = 1, bool positiveAxisOnly = true)
		{
			var colorPrev = Gizmos.color;

			length *= positiveAxisOnly ? 1 : 0.5f;
			
			Gizmos.color = new Color(1f, 0f, 0f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.right * length);
			if (!positiveAxisOnly)
			{
				Gizmos.DrawRay(position, rotation * Vector3.left * length);
			}
			
			Gizmos.color = new Color(0f, 1f, 0f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.up * length);
			if (!positiveAxisOnly)
			{
				Gizmos.DrawRay(position, rotation * Vector3.down * length);
			}
			
			Gizmos.color = new Color(0f, 0f, 1f, 1f);
			Gizmos.DrawRay(position, rotation * Vector3.forward * length);
			if (!positiveAxisOnly)
			{
				Gizmos.DrawRay(position, rotation * Vector3.back * length);
			}
			
			Gizmos.color = colorPrev;
		}
	}
}