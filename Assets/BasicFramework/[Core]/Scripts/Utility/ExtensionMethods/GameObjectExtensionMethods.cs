using UnityEngine;

namespace BasicFramework.Core.Utility.ExtensionMethods
{
    public static class GameObjectExtensionMethods 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public static bool ExistsInActiveScenes(this GameObject gameObject)
        {
            return gameObject.scene.IsValid();
        }

        /// <summary>
        /// Caluclates the bounds of the gameobject given all the child mesh renderers.
        /// Bounds are in local space, i.e. the bounds centre is relative the the gameobject pose
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static Bounds GetBounds(this GameObject gameObject)
        {
            var renderers = gameObject.GetComponentsInChildren<Renderer>();
            
            var bounds = new Bounds();
            
            if (renderers.Length == 0) return bounds;

            var rendererBounds = renderers[0].bounds;
            var worldMinBound = rendererBounds.min;
            var worldMaxBound = rendererBounds.max;

            for (var index = 1; index < renderers.Length; index++)
            {
                var meshRenderer = renderers[index];

                // Bounds is in world space
                rendererBounds = meshRenderer.bounds;

                var minBound = rendererBounds.min;
                if (minBound.x < worldMinBound.x) worldMinBound.x = minBound.x;
                if (minBound.y < worldMinBound.y) worldMinBound.y = minBound.y;
                if (minBound.z < worldMinBound.z) worldMinBound.z = minBound.z;

                var maxBound = rendererBounds.max;
                if (maxBound.x > worldMaxBound.x) worldMaxBound.x = maxBound.x;
                if (maxBound.y > worldMaxBound.y) worldMaxBound.y = maxBound.y;
                if (maxBound.z > worldMaxBound.z) worldMaxBound.z = maxBound.z;
            }
            
            bounds.min = gameObject.transform.InverseTransformPoint(worldMinBound);
            bounds.max = gameObject.transform.InverseTransformPoint(worldMaxBound);

            return bounds;
        }
        
    }
}