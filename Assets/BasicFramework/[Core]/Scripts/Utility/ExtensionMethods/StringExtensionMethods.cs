﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace BasicFramework.Core.Utility.ExtensionMethods
{
	public static class StringExtensionMethods 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		public static string CamelCaseToHumanReadable(this string value)
		{
			return Regex.Replace(value, "(?!^)([A-Z])", " $1");
		}
	
	}
}