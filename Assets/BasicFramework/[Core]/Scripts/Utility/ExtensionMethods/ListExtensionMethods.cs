using System.Collections.Generic;

namespace BasicFramework.Core.Utility.ExtensionMethods
{
	public static class ListExtensionMethods 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		public static bool IsNullOrEmpty<T>(this List<T> list)
		{
			return list == null || list.Count == 0;
		}
		
		public static bool ListEquivalent<T>(this List<T> list, List<T> other, bool ordered = true)
		{
			if (other == null) return false;
			
			if (list.Count != other.Count) return false;

			if (ordered)
			{
				for (int i = 0; i < list.Count; i++)
				{
					if(Equals(list[i], other[i])) continue;
					return false;
				}

				return true;
			}
			
			var tempList = new List<T>(other);
			
			foreach (var value in list)
			{
				if(tempList.Remove(value)) continue;
				return false;
			}

			return true;
		}
		
		public static bool ListEquivalent<T>(this List<T> list, T[] other, bool ordered = true)
		{
			if (other == null) return false;
			
			if (list.Count != other.Length) return false;
			
			if (ordered)
			{
				for (int i = 0; i < list.Count; i++)
				{
					if(Equals(list[i], other[i])) continue;
					return false;
				}

				return true;
			}
			
			var tempList = new List<T>(other);
			
			foreach (var value in list)
			{
				if(tempList.Remove(value)) continue;
				return false;
			}

			return true;
		}
		
		public static void CopyArrayToList<T>(this List<T> list, T[] array)
		{
			// Sets the values of the list, adding new entries as necessary
			for (int i = 0; i < array.Length; i++)
			{
				if (list.Count > i)
				{
					list[i] = array[i];
					continue;
				}
			        
				list.Add(array[i]);
			}

			// Removes any extra entries
			for (int i = list.Count; i > array.Length; i--)
			{
				list.RemoveAt(i - 1);
			}
		}

		public static void Resize<T>(this List<T> list, int newCount)
		{
			if (list.Count == newCount) return;

			if (list.Count >= newCount)
			{
				list.RemoveRange(newCount, list.Count - newCount);
			}
			else
			{
				for (int i = list.Count; i < newCount; i++)
				{
					list.Add(default);
				}
			}
		}
		
		
		public static bool HasDuplicates<T>(this List<T> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				var current = list[i];

				for (int j = i + 1; j < list.Count; j++)
				{
					if(!Equals(current, list[j])) continue;
					return true;
				}
			}

			return false;
		}
		
		public static void RemoveDuplicates<T>(this List<T> list)
		{
			for (int i = list.Count - 1; i >= 0; i--)
			{
				var current = list[i];

				for (int j = 0; j < i; j++)
				{
					if (!Equals(current, list[j])) continue;
					list.RemoveAt(i);
					break;
				}
			}
		}


		public static bool HasNulls<T>(this List<T> list)
		{
			foreach (var element in list)
			{
				if (Equals(element, null)) return true;
			}

			return false;
		}
		
		public static void RemoveNulls<T>(this List<T> list)
		{
			for (int i = list.Count - 1; i >= 0; i--)
			{
				if(!Equals(list[i], null)) continue;
				list.RemoveAt(i);
			}
		}
		
		public static void RemoveDuplicatesAndNulls<T>(this List<T> list)
		{
			list.RemoveNulls();
			list.RemoveDuplicates();
		}

	}
}