﻿using System.Collections.Generic;

namespace BasicFramework.Core.Utility.ExtensionMethods
{
	public static class ArrayExtensionMethods 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		public static bool IsNullOrEmpty<T>(this T[] array)
		{
			return array == null || array.Length == 0;
		}

		public static T[] Resize<T>(this T[] array, uint newSize)
		{
//			if (array == null)
//			{
//				array = new T[0];
//			}
			
			if(array.Length == newSize) return array;

			var newArray = new T[newSize];

			for (int i = 0; i < newArray.Length; i++)
			{
				if (i >= array.Length) break;
				newArray[i] = array[i];
			}

			return newArray;
		}
		
		public static int IndexOf<T>(this T[] array, T value)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (Equals(array[i], value)) return i;
			}

			return -1;
		}

		public static bool HasDuplicates<T>(this T[] array)
		{
			for (int i = 0; i < array.Length; i++)
			{
				var current = array[i];

				for (int j = i + 1; j < array.Length; j++)
				{
					if(!Equals(current, array[j])) continue;
					return true;
				}
			}

			return false;
		}
		
		public static T[] RemoveDuplicates<T>(this T[] array)
		{
			if (!array.HasDuplicates()) return array;
			
			var tempValues = new List<T>(array);

			for (int i = tempValues.Count - 1; i >= 0; i--)
			{
				var current = tempValues[i];

				for (int j = 0; j < i; j++)
				{
					if (!Equals(current, tempValues[j])) continue;
					tempValues.RemoveAt(i);
					break;
				}
			}

			return tempValues.ToArray();
		}


		public static bool HasNulls<T>(this T[] array)
		{
			foreach (var element in array)
			{
				if (Equals(element, null)) return true;
			}

			return false;
		}
		
		public static T[] RemoveNulls<T>(this T[] array)
		{
			if (!array.HasNulls()) return array;
			
			var tempValues = new List<T>(array);

			for (int i = tempValues.Count - 1; i >= 0; i--)
			{
				if(!Equals(tempValues[i], null)) continue;
				tempValues.RemoveAt(i);
			}

			return tempValues.ToArray();
		}
		
		
		public static T[] RemoveDuplicatesAndNulls<T>(this T[] array)
		{
			if (!array.HasNulls() && !array.HasDuplicates()) return array;
			
			var tempValues = new List<T>(array);

			for (int i = tempValues.Count - 1; i >= 0; i--)
			{
				var current = tempValues[i];

				if (Equals(current, null))
				{
					tempValues.RemoveAt(i);
					continue;
				}
				
				for (int j = 0; j < i; j++)
				{
					if (!Equals(current, tempValues[j])) continue;
					tempValues.RemoveAt(i);
				}
			}

			return tempValues.ToArray();
		}


		public static bool ArrayEquivalent<T>(this T[] array, List<T> other, bool ordered = true)
		{
			if (other == null) return false;
			
			if (array.Length != other.Count) return false;
			
			if (ordered)
			{
				for (int i = 0; i < array.Length; i++)
				{
					if(Equals(array[i], other[i])) continue;
					return false;
				}

				return true;
			}
			
			var list = new List<T>(other);
			
			foreach (var value in array)
			{
				if(list.Remove(value)) continue;
				return false;
			}

			return true;
		}

		public static bool ArrayEquivalent<T>(this T[] array, T[] other, bool ordered = true)
		{
			if (other == null) return false;
			
			if (array.Length != other.Length) return false;

			if (ordered)
			{
				for (int i = 0; i < array.Length; i++)
				{
					if(Equals(array[i], other[i])) continue;
					return false;
				}

				return true;
			}
			
			
			var list = new List<T>(other);
			
			foreach (var value in array)
			{
				if(list.Remove(value)) continue;
				return false;
			}

			return true;
		}
		
		public static bool Contains<T>(this T[] array, T value)
		{
			foreach (var element in array)
			{
				if (!Equals(element, value)) continue;
				return true;
			}

			return false;
		}
		
		public static T[] CloneArray<T>(this T[] array)
		{
			var clone = new T[array.Length];

			for (int i = 0; i < clone.Length; i++)
			{
				clone[i] = array[i];
			}

			return clone;
		}
	
		public static T[] Sync<T>(this T[] array, List<T> other)
		{
			if (array.ArrayEquivalent(other)) return array;

			array = array.Resize((uint) other.Count);

			for (int i = 0; i < array.Length; i++)
			{
				array[i] = other[i];
			}

			return array;
		}
		
		public static T[] Sync<T>(this T[] array, T[] other)
		{
			if (array.ArrayEquivalent(other)) return array;

			array = array.Resize((uint) other.Length);

			for (int i = 0; i < array.Length; i++)
			{
				array[i] = other[i];
			}

			return array;
		}
	}
}