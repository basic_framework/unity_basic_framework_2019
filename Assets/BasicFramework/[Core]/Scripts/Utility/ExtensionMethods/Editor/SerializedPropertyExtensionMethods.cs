﻿using System.Text.RegularExpressions;
using UnityEditor;

namespace BasicFramework.Core.Utility.Editor
{
	public static class SerializedPropertyExtensionMethods 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		public static string GetPropertyTypeName(this SerializedProperty serializedProperty)
		{
			var type = serializedProperty.type;
			var match = Regex.Match(type, @"PPtr<\$(.*?)>");
			
			if (!match.Success) return type;
			type = match.Groups[1].Value;
			return type;
		}

	}
}