﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public static class QuaternionExtensionMethods 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		public static bool IsValid(this Quaternion quaternion)
		{
			if (float.IsNaN(quaternion.x)) return false;
			if (float.IsNaN(quaternion.y)) return false;
			if (float.IsNaN(quaternion.z)) return false;
			if (float.IsNaN(quaternion.w)) return false;

			return true;
		}
	
	}
}