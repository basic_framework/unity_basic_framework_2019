using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
    public static class ReflectionUtility
    {
		
        public static Type[] GetDerivedTypes(Type baseType)
        {
            return GetDerivedTypes(baseType, Assembly.GetAssembly(baseType));
        }
		
        public static Type[] GetDerivedTypes(Type baseType, Assembly assembly)
        {
            // Get all types from the given assembly
            var types = assembly.GetTypes();
            var derivedTypes = new List<Type>();

            for (int i = 0, count = types.Length; i < count; i++)
            {
                var type = types[i];
                if (IsSubclassOf(type, baseType))
                {
                    // The current type is derived from the base type,
                    // so add it to the list
                    derivedTypes.Add(type);
                }
            }

            return derivedTypes.ToArray();
        }

        public static bool IsSubclassOf(Type type, Type baseType)
        {
            if (type == null || baseType == null || type == baseType)
                return false;

            if (baseType.IsGenericType == false)
            {
                if (type.IsGenericType == false)
                    return type.IsSubclassOf(baseType);
            }
            else
            {
                baseType = baseType.GetGenericTypeDefinition();
            }

            type = type.BaseType;
            Type objectType = typeof(object);

            while (type != objectType && type != null)
            {
                Type curentType = type.IsGenericType ?
                    type.GetGenericTypeDefinition() : type;
                if (curentType == baseType)
                    return true;

                type = type.BaseType;
            }

            return false;
        }

        public static Type[] GetComponentTypes(GameObject gameObject, bool includeChildren = false)
        {
            var components = includeChildren
                ? gameObject.GetComponentsInChildren<Component>()
                : gameObject.GetComponents<Component>();
			
            var types = new List<Type>();
			
            types.Add(typeof(GameObject));

            foreach (var component in components)
            {
                var type = component.GetType();
				
                if (types.Contains(type)) { continue; }
                types.Add(type);
            }

            return types.ToArray();
        }
		
    }
}