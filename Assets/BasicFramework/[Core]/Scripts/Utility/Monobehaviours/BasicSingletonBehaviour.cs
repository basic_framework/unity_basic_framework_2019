﻿using System.Collections.Generic;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Utility/Singleton Game Object")]
	public class BasicSingletonBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The key that will be used to identify this singleton, if another singleton gameobject already " +
		         "exists with this key, this gameobject will be destroyed")]
		[SerializeField] private StringSingleReference _key = new StringSingleReference("key");
		
		[Tooltip("Persist the gameobject across scenes. Note: will not work if parent is not null")]
		[SerializeField] private bool _dontDestroyOnLoad = true;

		
		public string Key => Application.isPlaying || _key.IsValueValid ? _key.Value : default;
		
		private static List<BasicSingletonBehaviour> _singletons = new List<BasicSingletonBehaviour>();
		
	
		// ****************************************  METHODS  ****************************************\\
		
		private void Awake()
		{
			if (!Application.isPlaying) return;

			if (SingletonWithKeyExists())
			{
				Debug.LogWarning($"{name}/{GetType().Name} => " +
				                 $"Singleton key \"{Key}\" already exists. Destroying \"{name}\" GameObject");
				Destroy(gameObject);
				return;
			}
			
			if (!_singletons.Contains(this))
			{
				_singletons.Add(this);
			}
			
			if (!_dontDestroyOnLoad) return;
			if (transform.parent != null)
			{
				Debug.LogError($"{name}/{GetType().Name} => " +
				               $"need to be at top of heirarchy to set DontDestroyOnLoad");
				return;
			}
			
			DontDestroyOnLoad(gameObject);
		}

		private void OnEnable()
		{
			if (Application.isPlaying) return;
			_singletons.Add(this);
		}

		private void OnDisable()
		{
			_singletons.Remove(this);
		}

		public bool SingletonWithKeyExists()
		{
			_singletons.RemoveNulls();

			foreach (var singleton in _singletons)
			{
				if(singleton == null) continue;
				if(singleton == this) continue;
				if(singleton.Key != Key) continue;
				return true;
			}

			return false;
		}
		
	}
}