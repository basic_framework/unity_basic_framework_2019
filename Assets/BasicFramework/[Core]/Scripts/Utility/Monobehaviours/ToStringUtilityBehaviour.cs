﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[AddComponentMenu("Basic Framework/Core/Utility/To String Utility")]
	public class ToStringUtilityBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The number of decimal places to use for types that have decimals")]
		[SerializeField] private int _decimalPlaces = 2;
		
		[Tooltip("Raised when a value is converted to string")]
		[SerializeField] private StringUnityEvent _valueConverted = default;

		
		public int DecimalPlaces
		{
			get => _decimalPlaces;
			set => _decimalPlaces = value;
		}

		public StringUnityEvent ValueConverted => _valueConverted;


		// ****************************************  METHODS  ****************************************\\
		
		public void ToStringBool(bool value)
		{
			_valueConverted.Invoke(value.ToString());
		}

		public void ToStringInt(int value)
		{
			_valueConverted.Invoke(value.ToString());
		}

		public void ToStringFloat(float value)
		{
			value = MathUtility.RoundToDecimalPlace(value, _decimalPlaces);
			_valueConverted.Invoke(value.ToString());
		}

		public void ToStringNormalizedFloat(NormalizedFloat normalizedFloat)
		{
			var valueFloat = MathUtility.RoundToDecimalPlace(normalizedFloat.Value, _decimalPlaces);
			_valueConverted.Invoke(valueFloat.ToString());
		}
		
	}
}