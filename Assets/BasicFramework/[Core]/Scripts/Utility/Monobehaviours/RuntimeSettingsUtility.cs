﻿using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	public class RuntimeSettingsUtility : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		
		
		[SerializeField] private RuntimePlatformUnityEvent _runtimePlatformChanged = default;
		
		[SerializeField] private RuntimePlatformCompareUnityEvent[] _runtimePlatformChangedCompare = default;


		

		public RuntimePlatformUnityEvent RuntimePlatformChanged => _runtimePlatformChanged;

		
		private RuntimePlatform _runtimePlatform;
		
		public RuntimePlatform RuntimePlatform => _runtimePlatform;


		// ****************************************  METHODS  ****************************************\\
		
		private void OnValidate()
		{
			_runtimePlatform = Application.platform;
		}

		private void Awake()
		{
			_runtimePlatform = Application.platform;
		}

		private void Start()
		{
			RuntimePlatformUpdated(_runtimePlatform);
		}

		private void RuntimePlatformUpdated(RuntimePlatform runtimePlatform)
		{
			_runtimePlatformChanged.Invoke(runtimePlatform);

			foreach (var compareUnityEvent in _runtimePlatformChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(runtimePlatform);
			}
		}
	}
}