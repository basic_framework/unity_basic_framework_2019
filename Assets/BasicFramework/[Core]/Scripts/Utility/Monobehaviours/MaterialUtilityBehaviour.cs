﻿using System.Collections.Generic;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[AddComponentMenu("Basic Framework/Core/Utility/Material Utility")]
	public class MaterialUtilityBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		// NOTE: HAS ISSUE WHEN GETTING SPECIFIC INDEXED MATERIAL FROM PROPERTY BLOCK
		// TODO: Get property block before setting data, set data, set renderer property block
		
		[SerializeField] private bool _useRenderer = true;
		
		[SerializeField] private Material _material = default;

		[SerializeField] private Renderer _renderer = default;
		
		[Tooltip("If the material index is out of range all materials of the renderer will be updated")]
		[SerializeField] private int _materialIndex = -1;

		[SerializeField] private string _materialPropertyName = default;


		public bool UseRenderer
		{
			get => _useRenderer;
			set
			{
				if (_useRenderer == value) return;
				_useRenderer = value;
				UpdateIsValidMaterial();
			}
		}

		public Material Material
		{
			get => _material;
			set
			{
				if (_material == value) return;
				_material = value;
				UpdateIsValidMaterial();
			}
		}

		public Renderer Renderer
		{
			get => _renderer;
			set
			{
				if (_renderer == value) return;
				_renderer = value;
				RendererUpdated(value);
			}
		}
		
		public int MaterialIndex
		{
			get => _materialIndex;
			set => _materialIndex = value;
		}
		
		public string MaterialPropertyName
		{
			get => _materialPropertyName;
			set => _materialPropertyName = value;
		}

		
		private bool _isValidMaterial;
		public bool IsValidMaterial => _isValidMaterial;
		
		
		private MaterialPropertyBlock _materialPropertyBlock = default;
		
		private readonly List<Material> _listMaterials = new List<Material>();
		


		// ****************************************  METHODS  ****************************************\\

		private void OnValidate()
		{
			UpdateIsValidMaterial();
		}

		private void Awake()
		{
			_materialPropertyBlock = new MaterialPropertyBlock();
			
			UpdateIsValidMaterial();
			RendererUpdated(_renderer);
		}

//		private void Start()
//		{
//			_materialPropertyBlock = new MaterialPropertyBlock();
//			
//			UpdateIsValidMaterial();
//			MaterialIndexUpdated(_materialIndex);
//			RendererUpdated(_renderer);
//		}


		//***** property updates *****\\

		private void RendererUpdated(Renderer value)
		{
			UpdateIsValidMaterial();
			
			if (!_isValidMaterial)
			{
				_listMaterials.Clear();
			}
			else
			{
				_renderer.GetMaterials(_listMaterials);
			}
		}
		
		
		//***** functions *****\\

		private void UpdateIsValidMaterial()
		{
			_isValidMaterial = !_useRenderer && _material != null 
			                   || _useRenderer && _renderer != null;
		}
		
		private void  UpdateMaterialPropertyBlock()
		{
			if (_materialIndex < 0 || _materialIndex >= _listMaterials.Count)
			{
				// Index is out of range, get all the material properties
				_renderer.GetPropertyBlock(_materialPropertyBlock);
			}
			else
			{
				// Get the material properties at the index
				_renderer.GetPropertyBlock(_materialPropertyBlock, _materialIndex);
			}
		}

		
		
		public void EnableKeyword()
		{
			EnableKeyword(MaterialPropertyName);
		}
		
		public void EnableKeyword(string value)
		{
			if(!IsValidMaterial) return;

			if (!_useRenderer)
			{
				_material.EnableKeyword(value);
			}
			else
			{
				if (_materialIndex >= 0 && _materialIndex < _listMaterials.Count)
				{
					_listMaterials[_materialIndex].EnableKeyword(value);
				}
				else
				{
					foreach (var material in _listMaterials)
					{
						material.EnableKeyword(value);
					}
				}
			}
		}
		
		public void DisableKeyword()
		{
			DisableKeyword(MaterialPropertyName);
		}
		
		public void DisableKeyword(string value)
		{
			if(!IsValidMaterial) return;

			if (!_useRenderer)
			{
				_material.DisableKeyword(value);
			}
			else
			{
				if (_materialIndex >= 0 && _materialIndex < _listMaterials.Count)
				{
					_listMaterials[_materialIndex].DisableKeyword(value);
				}
				else
				{
					foreach (var material in _listMaterials)
					{
						material.DisableKeyword(value);
					}
				}
			}
		}

		
		
		public void SetColor(ColorSingle colorSingle)
		{
			SetColor(colorSingle.Value);
		}
		
		public void SetColor(Color value)
		{
			if(!IsValidMaterial) return;
			
			if (!_useRenderer)
			{
				_material.SetColor(_materialPropertyName, value);
			}
			else
			{
				UpdateMaterialPropertyBlock();
				_materialPropertyBlock.SetColor(_materialPropertyName, value);
				_renderer.SetPropertyBlock(_materialPropertyBlock);
			}
		}


		
		public void SetFloat(NormalizedFloatSingle normalizedFloatSingle)
		{
			SetFloat(normalizedFloatSingle.Value.Value);
		}
		
		public void SetFloat(FloatSingle floatSingle)
		{
			SetFloat(floatSingle.Value);
		}

		public void SetFloat(NormalizedFloat normalizedFloat)
		{
			SetFloat(normalizedFloat.Value);
		}
		
		public void SetFloat(float value)
		{
			if(!IsValidMaterial) return;
			
			if (!_useRenderer)
			{
				_material.SetFloat(_materialPropertyName, value);
			}
			else
			{
				UpdateMaterialPropertyBlock();
				_materialPropertyBlock.SetFloat(_materialPropertyName, value);
				_renderer.SetPropertyBlock(_materialPropertyBlock);
			}
		}
		
		
		
		public void SetInt(IntSingle intSingle)
		{
			SetInt(intSingle.Value);
		}
		
		public void SetInt(int value)
		{
			if(!IsValidMaterial) return;
			
			if (!_useRenderer)
			{
				_material.SetInt(_materialPropertyName, value);
			}
			else
			{
				UpdateMaterialPropertyBlock();
				_materialPropertyBlock.SetInt(_materialPropertyName, value);
				_renderer.SetPropertyBlock(_materialPropertyBlock);
			}
		}
		
		
//		public void SetTexture(TextureSingle texture)
//		{
//			SetInt(intSingle.Value);
//		}
		
		public void SetTexture(Texture value)
		{
			if(!IsValidMaterial) return;
			
			if (!_useRenderer)
			{
				_material.SetTexture(_materialPropertyName, value);
			}
			else
			{
				UpdateMaterialPropertyBlock();
				_materialPropertyBlock.SetTexture(_materialPropertyName, value);
				_renderer.SetPropertyBlock(_materialPropertyBlock);
			}
		}

		public void SetMaterial(Material material)
		{
			if (!_useRenderer) return;
			
			var materials = _renderer.materials;
			
			if(_materialIndex < 0 || _materialIndex >= materials.Length) return;
			materials[_materialIndex] = material;
			_renderer.materials = materials;
		}
		
	}
}