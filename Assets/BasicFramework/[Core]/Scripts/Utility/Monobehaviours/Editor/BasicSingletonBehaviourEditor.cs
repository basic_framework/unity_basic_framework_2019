﻿using UnityEditor;

namespace BasicFramework.Core.Utility.Editor
{
	[CustomEditor(typeof(BasicSingletonBehaviour))]
	public class BasicSingletonBehaviourEditor : BasicBaseEditor<BasicSingletonBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();

			EditorGUILayout.HelpBox(
				$"GameObject will be destroyed on awake if another singleton " +
				$"gameobject with key \"{TypedTarget.Key}\" already exists in scene",
				MessageType.Info);
			
			var anotherExists = TypedTarget.SingletonWithKeyExists();

			if (anotherExists)
			{
				EditorGUILayout.HelpBox(
					$"Another singleton with key \"{TypedTarget.Key}\" exists", 
					MessageType.Warning);
			}
			else
			{
				EditorGUILayout.HelpBox(
					$"Singleton key \"{TypedTarget.Key}\" is currently unique", 
					MessageType.Info);
			}

			
		}

		protected override void DrawPropertyFields()
		{
			//base.DrawPropertyFields();
			
			var key = serializedObject.FindProperty("_key");
			var dontDestroyOnLoad = serializedObject.FindProperty("_dontDestroyOnLoad");
			
			Space();
			SectionHeader("Singleton Properties");
			EditorGUILayout.PropertyField(key);
			EditorGUILayout.PropertyField(dontDestroyOnLoad);
			
			if (!dontDestroyOnLoad.boolValue || TypedTarget.transform.parent == null) return;
			
			EditorGUILayout.HelpBox(
				"GameObject needs to be at the top of the heirarchy " +
				"(i.e. parent == null) for DontDestroyOnLoad to work", 
				MessageType.Warning);
		}
	}
}