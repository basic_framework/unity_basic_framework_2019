﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
    [CustomEditor(typeof(BasicUtilityBehaviour))]
    public class BasicUtilityBehaviourEditor : BasicBaseEditor<BasicUtilityBehaviour>
    {
	
        // **************************************** VARIABLES ****************************************\\

        protected override bool HasDebugSection => true;
		
		
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawDebugFields()
        {
            base.DrawDebugFields();

            var editorNote = serializedObject.FindProperty("_editorNote");

            EditorGUILayout.PropertyField(editorNote);
        }

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
			
            var destroyComponentOnAwake = serializedObject.FindProperty("_destroyComponentOnAwake");
            var dontDestroyOnLoad = serializedObject.FindProperty("_dontDestroyOnLoad");
			
            Space();
            SectionHeader($"{nameof(BasicUtilityBehaviour).CamelCaseToHumanReadable()} Properties");

            GUI.enabled = PrevGuiEnabled && !dontDestroyOnLoad.boolValue;
			
            EditorGUILayout.PropertyField(destroyComponentOnAwake);

            GUI.enabled = PrevGuiEnabled && !destroyComponentOnAwake.boolValue;
			
            EditorGUILayout.PropertyField(dontDestroyOnLoad);

            GUI.enabled = PrevGuiEnabled;
        }

        protected override void DrawEventFields()
        {
            base.DrawEventFields();
			
            var utilityUnityEvent = serializedObject.FindProperty("_utilityUnityEvent");
			
            Space();
            SectionHeader($"{nameof(BasicUtilityBehaviour).CamelCaseToHumanReadable()} Events");
			
            EditorGUILayout.PropertyField(utilityUnityEvent);

        }
    }
}