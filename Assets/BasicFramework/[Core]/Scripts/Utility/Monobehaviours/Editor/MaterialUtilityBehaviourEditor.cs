﻿using System.Collections.Generic;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Editor
{
	[CustomEditor(typeof(MaterialUtilityBehaviour))]
	public class MaterialUtilityBehaviourEditor : BasicBaseEditor<MaterialUtilityBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		private string[] _materialNames = {"All"};
		private readonly List<Material> _materialsList = new List<Material>();

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.IsValidMaterial).CamelCaseToHumanReadable(),
				TypedTarget.IsValidMaterial.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var useRenderer = serializedObject.FindProperty("_useRenderer");
			var material = serializedObject.FindProperty("_material");
			var renderer = serializedObject.FindProperty("_renderer");
			var materialIndex = serializedObject.FindProperty("_materialIndex");
			var materialPropertyName = serializedObject.FindProperty("_materialPropertyName");

			
			Space();
			SectionHeader($"{nameof(MaterialUtilityBehaviour).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(materialPropertyName);
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			EditorGUILayout.PropertyField(useRenderer);
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying && !useRenderer.boolValue;

			if (useRenderer.boolValue)
				BasicEditorGuiLayout.PropertyFieldCanBeNull(material);
			else
				BasicEditorGuiLayout.PropertyFieldNotNull(material);

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying && useRenderer.boolValue;
			
			if (!useRenderer.boolValue)
				BasicEditorGuiLayout.PropertyFieldCanBeNull(renderer);
			else
				BasicEditorGuiLayout.PropertyFieldNotNull(renderer);

			DrawMaterialIndexField(renderer, materialIndex);
			
			GUI.enabled = PrevGuiEnabled;
		}

		private void DrawMaterialIndexField(SerializedProperty renderer, SerializedProperty materialIndex)
		{
			var rendererObject = renderer.objectReferenceValue as Renderer; 
			var rendererIsNull = rendererObject == null;
			
			if (rendererIsNull)
			{
				var guiEnabled = GUI.enabled;
				GUI.enabled = false;
				EditorGUILayout.PropertyField(materialIndex);
				GUI.enabled = guiEnabled;
				return;
			}
			
			rendererObject.GetSharedMaterials(_materialsList);

			var count = (uint) _materialsList.Count + 1;

			_materialNames = _materialNames.Resize(count);

			for (int i = 1; i < count; i++)
			{
				_materialNames[i] = $"{i - 1} - {_materialsList[i - 1].name}";
			}

			var index = 0;
			
			if (materialIndex.intValue >= 0 && materialIndex.intValue < _materialsList.Count)
			{
				index = materialIndex.intValue + 1;
			}
			
			index = EditorGUILayout.Popup(GetGuiContent(materialIndex), index, _materialNames);
			
			materialIndex.intValue  = index - 1;
		}
		
	}
}