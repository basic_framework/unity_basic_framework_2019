﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[CustomEditor(typeof(RuntimeSettingsUtility))]
	public class RuntimeSettingsUtilityEditor : BasicBaseEditor<RuntimeSettingsUtility>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		private ReorderableList _reorderableListRuntimePlatformCompare;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var runtimePlatformChangedCompare = serializedObject.FindProperty("_runtimePlatformChangedCompare");

			_reorderableListRuntimePlatformCompare =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, runtimePlatformChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.RuntimePlatform).CamelCaseToHumanReadable(), 
				TypedTarget.RuntimePlatform.ToString());
			
			
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var runtimePlatformChanged = serializedObject.FindProperty("_runtimePlatformChanged");
			
			Space();
			SectionHeader($"{nameof(RuntimeSettingsUtility).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(runtimePlatformChanged);

			_reorderableListRuntimePlatformCompare.DoLayoutList();
		}
	}
}