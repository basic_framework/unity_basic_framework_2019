﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[AddComponentMenu("Basic Framework/Core/Utility/Game Object Bounds Viewer")]
	public class GameObjectBoundsViewer : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private Bounds _bounds;

		[SerializeField] private bool _disableViewer = default;
		
		
		public Bounds Bounds => _bounds;
		
		
		// ****************************************  METHODS  ****************************************\\

		private void OnDrawGizmosSelected()
		{
			if(_disableViewer) return;
			_bounds = gameObject.GetBounds();
			BasicGizmoUtility.DrawBounds(transform, _bounds);
		}
	
	}
}