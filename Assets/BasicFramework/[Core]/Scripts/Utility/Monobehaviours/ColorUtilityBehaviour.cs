﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.Utility
{
	[AddComponentMenu("Basic Framework/Core/Utility/Color Utility")]
	public class ColorUtilityBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private ColorUnityEvent _colorUnityEvent = default;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		public void RaiseColorUnityEvent(Color color)
		{
			_colorUnityEvent.Invoke(color);
		}
	
		public void RaiseColorUnityEvent(ColorSingle colorSingle)
		{
			_colorUnityEvent.Invoke(colorSingle.Value);
		}
	
	}
}