using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.Utility
{
    [AddComponentMenu("Basic Framework/Core/Utility/Basic Utility")]
    public class BasicUtilityBehaviour : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        [TextArea(4, 10)]
        [SerializeField] private string _editorNote = default;

#if UNITY_EDITOR
        /// <summary>DO NOT USE</summary>
        protected string EditorNote => _editorNote;
#endif
        
        [SerializeField] private bool _destroyComponentOnAwake = default;
        [SerializeField] private bool _dontDestroyOnLoad = default;

        [Tooltip("A UnityEvent you can raise in editor for any purpose")]
        [SerializeField] private UnityEvent _utilityUnityEvent = default;

        
        public UnityEvent UtilityUnityEvent => _utilityUnityEvent;


        // ****************************************  METHODS  ****************************************\\

        //***** mono *****\\
        
        private void Awake()
        {
            if (_destroyComponentOnAwake)
            {
                Destroy(this);
                return;
            }
            
            if (!_dontDestroyOnLoad) return;
            DontDestroyOnLoad(gameObject);
        }
        
        
        //***** functions *****\\

        //** GameObjects **\\

        public void ToggleGameObjectActive(GameObject gameObj)
        {
            gameObj.SetActive(!gameObj.activeSelf);
        }
		
        public void DestroyGameobject(GameObject gameObj)
        {
            Destroy(gameObj);
        }

        public void DestroyComponent(MonoBehaviour component)
        {
            Destroy(component);
        }

        public void DontDestroyOnLoadGameObject(GameObject gameObj)
        {
            DontDestroyOnLoad(gameObj);
        }


        //** Application **\\

        public void SetSleepTimeout(bool neverSleep)
        {
            UnityEngine.Screen.sleepTimeout = neverSleep ? SleepTimeout.NeverSleep : SleepTimeout.SystemSetting;
        }
		
        public void QuitApplication()
        {
            if (Application.isEditor)
            {
                Debug.Log("Requested to Quit Application");
            }
			
            Application.Quit();
        }


        public void RaiseUtilityUnityEvent()
        {
            _utilityUnityEvent.Invoke();
        }
        
    }
}