using System;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Utility
{
    [AddComponentMenu("Basic Framework/Core/Debug/FPS Counter")]
    public class FpsCounter : MonoBehaviour 
    {

        // **************************************** VARIABLES ****************************************\\
		
        const float UPDATE_RATE_HZ 	= 4;					// Updates per second
        const float UPDATE_RATE_SEC = 1f / UPDATE_RATE_HZ;	// Seconds per update
		
        [SerializeField] private FloatUnityEvent _framesPerSecondChanged = new FloatUnityEvent();
        [SerializeField] private StringUnityEvent _framesPerSecondChangedString = new StringUnityEvent();


        public FloatUnityEvent FramesPerSecondChanged => _framesPerSecondChanged;
        public StringUnityEvent FramesPerSecondChangedString => _framesPerSecondChangedString;


        private int _frameCount;		// Counts how many frames have occured between the fps update
        private float _dt;				// Time between updating fps

        private float _framesPerSecond;
        public float FramesPerSecond
        {
	        get => _framesPerSecond;
	        private set
	        {
		        if (Math.Abs(_framesPerSecond - value) < Mathf.Epsilon) return;
		        _framesPerSecond = value;
		        FramesPerSecondUpdated(_framesPerSecond);
	        }
        }

       


        // ****************************************  METHODS  ****************************************\\
		
        private void Update () 
        {
            _frameCount++;							// Increase the frame count
            _dt += Time.unscaledDeltaTime;			// Increase the time since last update

            // Check if it is time to update our fps count
            if (_dt < UPDATE_RATE_SEC) return;
            
            FramesPerSecond = _frameCount / _dt;	// Work out new fps
            _frameCount = 0;						// Reset framecount
            _dt -= UPDATE_RATE_SEC;					// More accurate than setting dt to 0 (though not 100% sure why)
        }
		
        private void FramesPerSecondUpdated(float framesPerSecond)
        {
	        _framesPerSecondChanged.Invoke(framesPerSecond);
	        _framesPerSecondChangedString.Invoke(Mathf.FloorToInt(framesPerSecond).ToString());
        }
        
    }
}