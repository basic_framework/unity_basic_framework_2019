using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Utility.Editor
{
    [CustomEditor(typeof(FpsCounter))]
    public class FpsCounterEditor : BasicBaseEditor<FpsCounter>
    {
	    
        // **************************************** VARIABLES ****************************************\\

        protected override bool UpdateConstantlyInPlayMode => true;
        
        protected override bool HasDebugSection => true;



        // ****************************************  METHODS  ****************************************\\

        protected override void DrawDebugFields()
        {
            base.DrawDebugFields();
            
            EditorGUILayout.LabelField("Frames Per Second", TypedTarget.FramesPerSecond.ToString());
        }

        protected override void DrawEventFields()
        {
            base.DrawEventFields();
            
            Space();
            
            SectionHeader("Events");

            var framesPerSecondChanged = serializedObject.FindProperty("_framesPerSecondChanged");
            var framesPerSecondChangedString = serializedObject.FindProperty("_framesPerSecondChangedString");
   
            EditorGUILayout.PropertyField(framesPerSecondChanged);
            EditorGUILayout.PropertyField(framesPerSecondChangedString);
        }

    }
}