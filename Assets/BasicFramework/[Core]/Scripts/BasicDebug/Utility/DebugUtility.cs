using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BasicFramework.Core.BasicDebug.Utility
{
	public enum LogMessageTypes
	{
		Info,
		Warning,
		Error
	}
	
    public static class DebugUtility 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
        public static void DrawRotation(Transform transform, float axisLength = 1)
        {
            DrawOrientation(transform.position, transform.rotation, axisLength);
        }
		
        public static void DrawOrientation(Vector3 position, Quaternion rotation, float axisLength = 1)
        {
            Debug.DrawRay(position, rotation * Vector3.up * axisLength, 		Color.green);
            Debug.DrawRay(position, rotation * Vector3.forward * axisLength, 	Color.blue);
            Debug.DrawRay(position, rotation * Vector3.right * axisLength, 	Color.red);
        }

        public static void Log(Object unityObject, string message, LogMessageTypes logMessageType = LogMessageTypes.Info)
        {
	        var output = unityObject == null ? "" : $"<b>{unityObject.name}/{unityObject.GetType().Name} => </b> ";
	        output += message;

	        switch (logMessageType)
	        {
		        case LogMessageTypes.Info:
			        Debug.Log(output);
			        break;
		        case LogMessageTypes.Warning:
			        Debug.LogWarning(output);
			        break;
		        case LogMessageTypes.Error:
			        Debug.LogError(output);
			        break;
		        default:
			        throw new ArgumentOutOfRangeException(nameof(logMessageType), logMessageType, null);
	        }
        }

        public static void DrawPath(Vector3[] pathPoints, Color color)
        {
	        DrawPath(pathPoints, pathPoints.Length, color);
        }
        
        public static void DrawPath(Vector3[] pathPoints, int count, Color color)
        {
	        count = Mathf.Min(pathPoints.Length, count);
	        
	        if (count < 2) return;
			
	        Vector3 start, end;
			
	        start = pathPoints[0];
			
	        for (int i = 1; i < count; i++)
	        {
		        end = pathPoints[i];
		        Debug.DrawLine(start, end, color);
		        start = end;
	        }
        }
        
        public static void DrawPath(List<Vector3> pathPoints, Color color)
        {
	        var start = pathPoints[0];
			
	        for (int i = 1; i < pathPoints.Count; i++)
	        {
		        var end = pathPoints[i];
		        Debug.DrawLine(start, end, color);
		        start = end;
	        }
        }

    }
}