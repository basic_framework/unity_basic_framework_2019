using BasicFramework.Core.BasicDebug.Utility;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/Debug/Debug Third Person Driver")]
	[RequireComponent(typeof(CharacterController))]
	public class DebugThirdPersonDriver : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private CharacterController _characterController = default;
		[SerializeField] private Transform _cameraTransform = default;

		[Tooltip("Maximum speed in m/sec")]
		[SerializeField] private float _speed = 5;
		
		[Tooltip("How high the character jumps")]
		[SerializeField] private float _jumpHeight = 2f;
		
		[Tooltip("How long it takes to get to the apex of the jump")]
		[SerializeField] private float _timeToApex = 0.35f;
		
		[Tooltip("How fast the transform turns to make forward facing the movement direction")]
		[SerializeField] private float _rotateRate = 540f;
		
		private float _jumpVelocity;
		private float _gravity;
		private float _verticalVelocity;
		

		// ****************************************  METHODS  ****************************************\\
		
		private void Reset()
		{
			var mainCam = Camera.main;
			if (mainCam != null)
			{
				_cameraTransform = mainCam.transform;
			}
			 
			_characterController = GetComponent<CharacterController>();
		}

		private void OnValidate()
		{
			UpdateGravityAndJumpVelocity();
		}

		private void Awake()
		{
			_characterController = GetComponent<CharacterController>();
			
			UpdateGravityAndJumpVelocity();
		}
		
		private void Update()
		{
			//***** Process Input *****\\
			
			var moveInput = Vector3.zero;
	
			if (Input.GetKey(KeyCode.W)) 		{ moveInput.z = 1f; }
			else if (Input.GetKey(KeyCode.S)) 	{ moveInput.z = -1f; }
			
			if (Input.GetKey(KeyCode.D)) 		{ moveInput.x = 1f; }
			else if (Input.GetKey(KeyCode.A)) 	{ moveInput.x = -1f;}
			
			
			var up = Vector3.up;

			var cameraToTransform = MathUtility.RemoveAxisFromVector(transform.position - _cameraTransform.position, up);
			var moveRotation = MathUtility.Direction2Quaternion(cameraToTransform, up); 
			
			DebugUtility.DrawOrientation(transform.position, moveRotation);
			
			var xzVelocity = moveInput * _speed;

			var jump = Input.GetKeyDown(KeyCode.Space);
			
			if (jump) 									{ _verticalVelocity = _jumpVelocity; }
			else if (_characterController.isGrounded) 	{ _verticalVelocity = _gravity * Time.deltaTime  * -1f; }
			else 										{ _verticalVelocity -= _gravity * Time.deltaTime ; }
			
			var moveVelocity = new Vector3(xzVelocity.x, _verticalVelocity, xzVelocity.z);
			
			_characterController.Move(moveRotation * (moveVelocity * Time.deltaTime));

			if (!(xzVelocity.sqrMagnitude > float.Epsilon)) return;
			
			var endRotation = MathUtility.Direction2Quaternion(moveRotation * xzVelocity, up);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, endRotation, _rotateRate * Time.deltaTime);
		}
		
		private void UpdateGravityAndJumpVelocity()
		{
			_gravity = 2 * _jumpHeight / Mathf.Pow(_timeToApex, 2);
			_jumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(_gravity) * _jumpHeight);
		}
		
	}
}