using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Editor
{
	[CustomEditor(typeof(DebugFirstPersonDriver))]
	public class DebugFirstPersonDriverEditor : BasicBaseEditor<DebugFirstPersonDriver>
	{

		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_characterController").isExpanded;
			set => serializedObject.FindProperty("_characterController").isExpanded = value;
		}
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var characterController = serializedObject.FindProperty("_characterController");

			GUI.enabled = false;
			BasicEditorGuiLayout.PropertyFieldNotNull(characterController);
			GUI.enabled = PrevGuiEnabled;
			
			Space();
			SectionHeader("Controls");
			EditorGUILayout.LabelField("Move Forward|Back", "W|S");
			EditorGUILayout.LabelField("Move Right|Left", "D|A");
			EditorGUILayout.LabelField("Jump", "Spacebar");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawGeneralFields();

			Space();
			DrawCameraFields();

			Space();
			DrawMovementFields();
			
			Space();
			DrawRotationFields();
		}

		private void DrawGeneralFields()
		{
			var cursorVisible = serializedObject.FindProperty("_cursorVisible");

			EditorGUILayout.LabelField("General", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(cursorVisible);
		}

		private void DrawMovementFields()
		{
			var speed = serializedObject.FindProperty("_speed");
			var jumpHeight = serializedObject.FindProperty("_jumpHeight");
			var timeToApex = serializedObject.FindProperty("_timeToApex");

			EditorGUILayout.LabelField("Movement", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(speed);

			EditorGUILayout.PropertyField(jumpHeight);
			EditorGUILayout.PropertyField(timeToApex);
		}

		private void DrawRotationFields()
		{
			var invertPitch = serializedObject.FindProperty("_invertPitch");
			var absRotateSensitivity = serializedObject.FindProperty("_absoluteRotationSensitivity");
			var maxPitchAngle = serializedObject.FindProperty("_maxPitchAngle");

			EditorGUILayout.LabelField("Rotation", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(invertPitch);
			EditorGUILayout.PropertyField(absRotateSensitivity);

			EditorGUILayout.PropertyField(maxPitchAngle);
		}

		private void DrawCameraFields()
		{
			var camera = serializedObject.FindProperty("_camera");
			
			EditorGUILayout.LabelField("Camera", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(camera);
			DrawCameraHeightSlider();
			
			var cameraComponent = camera.objectReferenceValue as Camera;
			
			if (cameraComponent == null)
			{
				camera.objectReferenceValue = GetCameraInChildren();
			}
			
			DrawCameraButtons();
			ValidateCamera();
		}

		private void DrawCameraHeightSlider()
		{

			var prevGuiEnabled = GUI.enabled;
			
			var cameraHeight = serializedObject.FindProperty("_cameraHeight");
			
			if (Selection.objects.Length != 1)
			{
				GUI.enabled = false;
				EditorGUILayout.PropertyField(cameraHeight);
				GUI.enabled = prevGuiEnabled;
				return;
			}
			
			var ccDriver = target as DebugFirstPersonDriver;
			if (ccDriver == null) return;
			
			var characterContoller = ccDriver.GetComponent<CharacterController>();
			var maxEyeHeight = characterContoller == null ? 2 : characterContoller.height;
			cameraHeight.floatValue = EditorGUILayout.Slider(cameraHeight.displayName, cameraHeight.floatValue, 0, maxEyeHeight);
			
		}

		private Camera GetCameraInChildren()
		{
			if (Selection.objects.Length != 1) return null;
			
			var ccDriver = target as DebugFirstPersonDriver;
			if (ccDriver == null) return null;
			
			var cameras = ccDriver.gameObject.GetComponentsInChildren<Camera>();

			foreach (var camera in cameras)
			{
				// Only get camera that is first level deep
				if (camera.transform.parent != ccDriver.transform) continue;
				if (camera.gameObject == ccDriver.gameObject) continue;
				return camera;
			}

			return null;
		}

		private void DrawCameraButtons()
		{
			if (Selection.objects.Length != 1) return;
			
			var prevGuiEnabled = GUI.enabled;
			
			var camera = serializedObject.FindProperty("_camera");
			var cameraComponent = camera.objectReferenceValue as Camera;
			//var isMainCamera = cameraComponent == Camera.main;

			if (cameraComponent != null)
			{
				EditorGUILayout.BeginHorizontal();
				
				if (GUILayout.Button("Remove Camera"))
				{
					camera.objectReferenceValue = null;
					cameraComponent.transform.parent = null; 
					//if (isMainCamera) { cameraComponent.transform.parent = null; }
				}

				if (cameraComponent == Camera.main) { GUI.enabled = false; }
				
				if (GUILayout.Button("Destroy Camera"))
				{
					camera.objectReferenceValue = null;
					Undo.DestroyObjectImmediate(cameraComponent.gameObject);
				}
				
				GUI.enabled = prevGuiEnabled;
				
				EditorGUILayout.EndHorizontal();

				return;
			}
			
			var ccDriver = target as DebugFirstPersonDriver;
			if (ccDriver == null) return;
			
			var mainCamera = Camera.main;
			
			EditorGUILayout.BeginHorizontal();

			if (mainCamera == null || mainCamera.gameObject == ccDriver.gameObject) GUI.enabled = false;
			if (GUILayout.Button("Setup Main Camera"))
			{
				camera.objectReferenceValue = mainCamera;
				mainCamera.transform.parent = ccDriver.transform;
			}
			GUI.enabled = prevGuiEnabled;
				
			if (GUILayout.Button("Setup New Camera"))
			{
				cameraComponent = new GameObject("FirstPersonCamera").AddComponent<Camera>();
				cameraComponent.transform.parent = ccDriver.transform;
				cameraComponent.gameObject.AddComponent<AudioListener>();
				camera.objectReferenceValue = cameraComponent;
			}

			EditorGUILayout.EndHorizontal();
		}
		
		private void ValidateCamera()
		{
			if (Selection.objects.Length != 1) return;

			var ccDriver = target as DebugFirstPersonDriver;
			if (ccDriver == null) return;
			
			var camera = serializedObject.FindProperty("_camera");
			var cameraComponent = camera.objectReferenceValue as Camera;

			if (cameraComponent == null) return;
			if (cameraComponent.gameObject == ccDriver.gameObject)
			{
				camera.objectReferenceValue = null;
				Debug.LogError("Camera can not be on the same gameobject as DebugFirstPersonDriver");
				return;
			}

			if (cameraComponent.transform.parent != ccDriver.transform)
			{
				cameraComponent.transform.parent = ccDriver.transform;
			}
			
			var cameraHeight = serializedObject.FindProperty("_cameraHeight");

			var cc = ccDriver.GetComponent<CharacterController>();
			var localY = cameraHeight.floatValue - cc.height * 0.5f + cc.center.y;
			
			cameraComponent.transform.localPosition = new Vector3(0, localY, 0);
		}
		
	}
}