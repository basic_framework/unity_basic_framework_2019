using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Editor
{
	[CustomEditor(typeof(DebugThirdPersonDriver))]
	public class DebugThirdPersonDriverEditor : BasicBaseEditor<DebugThirdPersonDriver>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_characterController").isExpanded;
			set => serializedObject.FindProperty("_characterController").isExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var characterController = serializedObject.FindProperty("_characterController");

			GUI.enabled = false;
			BasicEditorGuiLayout.PropertyFieldNotNull(characterController);
			GUI.enabled = PrevGuiEnabled;
			
			Space();
			SectionHeader("Controls");
			EditorGUILayout.LabelField("Move Forward|Back", "W|S");
			EditorGUILayout.LabelField("Move Right|Left", "D|A");
			EditorGUILayout.LabelField("Jump", "Spacebar");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawGeneralFields();
			
			Space();
			DrawMovementFields();
		}

		private void DrawGeneralFields()
		{
			var cameraTransform = serializedObject.FindProperty("_cameraTransform");
			
			EditorGUILayout.LabelField("General", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(cameraTransform);
		}

		private void DrawMovementFields()
		{
			var speed 		= serializedObject.FindProperty("_speed");
			var jumpHeight 	= serializedObject.FindProperty("_jumpHeight");
			var timeToApex 	= serializedObject.FindProperty("_timeToApex");
			var rotateRate 	= serializedObject.FindProperty("_rotateRate");
			
			EditorGUILayout.LabelField("Movement", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(speed);
			EditorGUILayout.PropertyField(jumpHeight);
			EditorGUILayout.PropertyField(timeToApex);
			EditorGUILayout.PropertyField(rotateRate);
		}

	}
}