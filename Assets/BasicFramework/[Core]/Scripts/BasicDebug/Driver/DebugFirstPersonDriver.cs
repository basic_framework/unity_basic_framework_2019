using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/Debug/Debug First Person Driver")]
	[RequireComponent(typeof(CharacterController))]
	public class DebugFirstPersonDriver : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		//***** General *****\\
		
		[SerializeField] private bool _cursorVisible = true;
		
		[SerializeField] private Camera _camera = default;
		[SerializeField] private float _cameraHeight = 1.7f;

		[SerializeField] private CharacterController _characterController = default;
		
		public bool CursorVisible
		{
			get => Cursor.visible;
			set => Cursor.visible = _cursorVisible = value;
		}

		public Camera Camera => _camera;

		
		//***** Move *****\\	
		
		[Tooltip("Maximum speed in m/sec")]
		[SerializeField] private float _speed = 5;
		
		[SerializeField] private float _jumpHeight = 2f;
		[SerializeField] private float _timeToApex = 0.35f;
		
		private float _jumpVelocity;
		private float _gravity;
		private float _verticalVelocity;
		
		
		//***** Rotation *****\\
		
		[SerializeField] private bool _invertPitch = true;
		
		[Tooltip("The multiplying factor for absolute rotation (make negative to invert). Pitch (x), Yaw (y)")]
		[SerializeField] private Vector2 _absoluteRotationSensitivity = new Vector2(0.5f, 0.5f);

		[SerializeField][Range(50, 89)] private float _maxPitchAngle = 70;

		private Vector3 _lastMousePosition;
		private Vector3 _mouseDelta;
		

		// ****************************************  METHODS  ****************************************\\
		
		private void Reset()
		{
			_characterController = GetComponent<CharacterController>();
		}

		private void OnValidate()
		{
			Cursor.visible = _cursorVisible;
			UpdateGravityAndJumpVelocity();
		}

		private void OnEnable()
		{
			Cursor.visible = _cursorVisible;
		}

		private void Awake()
		{
			// Just in case, get the character controller
			_characterController = GetComponent<CharacterController>();

			if (_camera != null)
			{
				var cameraTransform = _camera.transform;
				cameraTransform.localRotation = Quaternion.identity;
				cameraTransform.localPosition = new Vector3(0, _cameraHeight, 0);
			}
			
			UpdateGravityAndJumpVelocity();
		}

		private void Update()
		{
			//***** Process Input *****\\
			
			_mouseDelta = Input.mousePosition - _lastMousePosition;
			_lastMousePosition = Input.mousePosition;

			var moveInput = Vector3.zero;
	
			if (Input.GetKey(KeyCode.W)) 		{ moveInput.z = 1f; }
			else if (Input.GetKey(KeyCode.S)) 	{ moveInput.z = -1f; }
			
			if (Input.GetKey(KeyCode.D)) 		{ moveInput.x = 1f; }
			else if (Input.GetKey(KeyCode.A)) 	{ moveInput.x = -1f;}
			
			
			//***** Rotation *****\\
			
			var absRotateInput = _mouseDelta;
			
			var rotation = new Vector2(
				absRotateInput.x * _absoluteRotationSensitivity.x,
				absRotateInput.y * _absoluteRotationSensitivity.y * (_invertPitch ? -1 : 1)
				);

			// Pitch
			var camLocalRotation = _camera.transform.localRotation.eulerAngles;

			camLocalRotation.x = camLocalRotation.x + rotation.y;
			camLocalRotation.x = MathUtility.WrapValue(camLocalRotation.x, -180, 180);			
			camLocalRotation.x = Mathf.Clamp(camLocalRotation.x, _maxPitchAngle * -1, _maxPitchAngle);

			_camera.transform.localRotation = Quaternion.Euler(camLocalRotation);

			// Yaw
			transform.Rotate(0, rotation.x, 0, Space.Self);
			
			
			//***** Movement ******\\
			
			var motion = transform.rotation * (moveInput * _speed * Time.deltaTime);

			var jump = Input.GetKeyDown(KeyCode.Space);

			if (jump) 									{ _verticalVelocity = _jumpVelocity; }
			else if (_characterController.isGrounded) 	{ _verticalVelocity = _gravity * Time.deltaTime  * -1f; }
			else 										{ _verticalVelocity -= _gravity * Time.deltaTime ; }

			motion += _verticalVelocity * Time.deltaTime * transform.up;

			_characterController.Move(motion);

		}

		public void UpdateGravityAndJumpVelocity()
		{
			_gravity = 2 * _jumpHeight / Mathf.Pow(_timeToApex, 2);
			_jumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(_gravity) * _jumpHeight);
		}
		
	}
}