using UnityEngine;

namespace BasicFramework.Core.BasicDebug
{
    [AddComponentMenu("Basic Framework/Core/Debug/Log Utility Component")]
    public class LogUtilityBehaviour : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

	
        // ****************************************  METHODS  ****************************************\\
		
        public void Log(string message)
        {
            Debug.Log(message);
        }

        public void LogWarning(string message)
        {
            Debug.LogWarning(message);
        }
		
        public void LogError(string message)
        {
            Debug.LogError(message);
        }
	
    }
}