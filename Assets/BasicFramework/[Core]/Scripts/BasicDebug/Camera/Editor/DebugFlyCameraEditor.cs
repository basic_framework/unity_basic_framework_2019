using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Editor
{
	[CustomEditor(typeof(DebugFlyCamera))]
	public class DebugFlyCameraEditor : BasicBaseEditor<DebugFlyCamera>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_cursorVisible").isExpanded;
			set => serializedObject.FindProperty("_cursorVisible").isExpanded = value;
		}
		
		
		private static GUIStyle _headerStyle;
		
		private static readonly Color EnabledColor = BasicEditorStyles.Colors.MediumGreen;
		private static readonly Color DisabledColor = BasicEditorStyles.Colors.LightRed;
	
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			SectionHeader("Controls");
			EditorGUILayout.LabelField("Rotate Yaw:", 		"RMB + Mouse X");
			EditorGUILayout.LabelField("Rotate Pitch:", 	"RMB + Mouse Y");
			
			Space();
			EditorGUILayout.LabelField("Move Forward|Back:", 	"RMB + W|S");
			EditorGUILayout.LabelField("Move Right|Left:", 	"RMB + D|A");
			EditorGUILayout.LabelField("Move Up|Down:", 		"RMB + E|Q");
			EditorGUILayout.LabelField("Move Fast:", 			"Hold Shift");
			EditorGUILayout.LabelField("Move Slow:", 			"Hold Ctrl");
			
			Space();
			EditorGUILayout.LabelField("Pan Left|Right:", 	"MMB + Mouse X");
			EditorGUILayout.LabelField("Pan UP|Down:", 	"MMB + Mouse Y");
			
			Space();
			EditorGUILayout.LabelField("Zoom Step:", 	"Mouse Scroll");
			EditorGUILayout.LabelField("Zoom Smooth:", 	"Alt + RMB + Mouse X");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			if (_headerStyle == null)
			{
				_headerStyle = new GUIStyle(EditorStyles.boldLabel);
			}
			
			Space();
			DrawGeneralFields();
			
			Space();
			DrawRotationFields();
			
			Space();
			DrawTranslationFields();
			
			Space();
			DrawPanningFields();
			
			Space();
			DrawZoomingFields();
		}
	
		private void DrawGeneralFields()
		{
			EditorGUILayout.LabelField("General", EditorStyles.boldLabel);
			
			var cursorVisible = serializedObject.FindProperty("_cursorVisible");
			var targetTransform = serializedObject.FindProperty("_targetTransform");
			var scaler = serializedObject.FindProperty("_scaler");

			
			BasicEditorGuiLayout.PropertyFieldNotNull(targetTransform);
			EditorGUILayout.PropertyField(cursorVisible);
			
			// Scaler			
			EditorGUILayout.PropertyField(scaler);

			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("0.01"))	scaler.floatValue = 0.01f;
			if (GUILayout.Button("0.1"))	scaler.floatValue = 0.1f;
			if (GUILayout.Button("1"))		scaler.floatValue = 1f;
			if (GUILayout.Button("10"))	scaler.floatValue = 10f;
			if (GUILayout.Button("100"))	scaler.floatValue = 100f;
			EditorGUILayout.EndHorizontal();
		}
		
		private void DrawRotationFields()
		{
			var prevEnabled = GUI.enabled;
			var prevGuiColor = GUI.color;
			
			var rotateEnabled = serializedObject.FindProperty("_rotationEnabled");
			var invertVertical = serializedObject.FindProperty("_invertVertical");
			var rotateSensitivity = serializedObject.FindProperty("_rotateSensitivity");

			var color = rotateEnabled.boolValue 
				? EnabledColor
				: DisabledColor;

			GUI.color = color;
			BasicEditorStyles.ChangeGuiStyleTextColor(_headerStyle, color);
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("Rotation", _headerStyle, GUILayout.Width(EditorGUIUtility.labelWidth));
			EditorGUILayout.PropertyField(rotateEnabled, GUIContent.none, GUILayout.ExpandWidth(false));
			
			EditorGUILayout.EndHorizontal();
			
			GUI.color = prevGuiColor;
			
			GUI.enabled = rotateEnabled.boolValue;
			EditorGUILayout.PropertyField(invertVertical);
			EditorGUILayout.PropertyField(rotateSensitivity);
			GUI.enabled = prevEnabled;
		}

		private void DrawTranslationFields()
		{
			var prevEnabled = GUI.enabled;
			var prevGuiColor = GUI.color;
			
			var translationEnabled = serializedObject.FindProperty("_translationEnabled");
			var speedRange = serializedObject.FindProperty("_speedRange");
			var acceleration = serializedObject.FindProperty("_acceleration");
			
			var color = translationEnabled.boolValue 
				? EnabledColor
				: DisabledColor;

			GUI.color = color;
			BasicEditorStyles.ChangeGuiStyleTextColor(_headerStyle, color);
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("Translation", _headerStyle, GUILayout.Width(EditorGUIUtility.labelWidth));
			EditorGUILayout.PropertyField(translationEnabled, GUIContent.none, GUILayout.ExpandWidth(false));
			
			EditorGUILayout.EndHorizontal();
			
			GUI.color = prevGuiColor;
			
			GUI.enabled = translationEnabled.boolValue;
			//EditorGUILayout.LabelField("Current Speed: " + unitySceneCamera.CurrentSpeed);
			EditorGUILayout.PropertyField(speedRange);
			EditorGUILayout.PropertyField(acceleration);
			GUI.enabled = prevEnabled;
		}

		private void DrawPanningFields()
		{
			var prevEnabled = GUI.enabled;
			var prevGuiColor = GUI.color;
			
			var panningEnabled = serializedObject.FindProperty("_panningEnabled");
			var panSensitivity = serializedObject.FindProperty("_panSensitivity");
			var invertPan = serializedObject.FindProperty("_invertPan");
			
			var color = panningEnabled.boolValue 
				? EnabledColor
				: DisabledColor;

			GUI.color = color;
			BasicEditorStyles.ChangeGuiStyleTextColor(_headerStyle, color);
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("Panning", _headerStyle, GUILayout.Width(EditorGUIUtility.labelWidth));
			EditorGUILayout.PropertyField(panningEnabled, GUIContent.none, GUILayout.ExpandWidth(false));
			
			EditorGUILayout.EndHorizontal();
			
			GUI.color = prevGuiColor;
			
			GUI.enabled = panningEnabled.boolValue;
			EditorGUILayout.PropertyField(invertPan);
			EditorGUILayout.PropertyField(panSensitivity);
			GUI.enabled = prevEnabled;
		}

		private void DrawZoomingFields()
		{
			var prevEnabled = GUI.enabled;
			var prevGuiColor = GUI.color;
			
			var zoomingEnabled = serializedObject.FindProperty("_zoomingEnabled");
			var zoomStepResolution = serializedObject.FindProperty("_zoomStepResolution");
			var zoomSensitivity = serializedObject.FindProperty("_zoomSensitivity");
			
			var color = zoomingEnabled.boolValue 
				? EnabledColor
				: DisabledColor;

			GUI.color = color;
			BasicEditorStyles.ChangeGuiStyleTextColor(_headerStyle, color);
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("Zoom", _headerStyle, GUILayout.Width(EditorGUIUtility.labelWidth));
			EditorGUILayout.PropertyField(zoomingEnabled, GUIContent.none, GUILayout.ExpandWidth(false));
			
			EditorGUILayout.EndHorizontal();
			
			GUI.color = prevGuiColor;
			
			GUI.enabled = zoomingEnabled.boolValue;
			EditorGUILayout.PropertyField(zoomStepResolution);
			EditorGUILayout.PropertyField(zoomSensitivity);
			GUI.enabled = prevEnabled;
		}

	}
}