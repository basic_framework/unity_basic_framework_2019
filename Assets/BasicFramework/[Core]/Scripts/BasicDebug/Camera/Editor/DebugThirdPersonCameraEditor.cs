using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug.Editor
{
	[CustomEditor(typeof(DebugThirdPersonCamera))]
	public class DebugThirdPersonCameraEditor : BasicBaseEditor<DebugThirdPersonCamera>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_editorPitch").isExpanded;
			set => serializedObject.FindProperty("_editorPitch").isExpanded = value;
		}
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			SectionHeader("Controls");
			var requireRmb = serializedObject.FindProperty("_requireRmb");

			EditorGUILayout.LabelField("Rotate Yaw:", 		requireRmb.boolValue ? "RMB + Mouse X" : "Mouse X");
			EditorGUILayout.LabelField("Rotate Pitch:", 	requireRmb.boolValue ? "RMB + Mouse Y" : "Mouse Y");
			
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Pan Target Up|Down:", 	"MMB + Mouse Y");
			
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Zoom Step:", 	"Mouse Scroll");
			EditorGUILayout.LabelField("Zoom Smooth:", 	"RMB + Up|Down Arrow");
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawEditorFields();
			
			Space();
			DrawTargetFields();
			
			Space();
			DrawInputFields();
			
			Space();
			DrawScalerField();
		}

		private void DrawEditorFields()
		{
			var editorPitch = serializedObject.FindProperty("_editorPitch");
			var editorYaw = serializedObject.FindProperty("_editorYaw");
			
			EditorGUILayout.LabelField("Editor", EditorStyles.boldLabel);

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying; 
			EditorGUILayout.PropertyField(editorPitch);
			EditorGUILayout.PropertyField(editorYaw);
			GUI.enabled = PrevGuiEnabled;
		}

		private void DrawTargetFields()
		{
			var cameraTarget = serializedObject.FindProperty("_target");
			var useTargetUp = serializedObject.FindProperty("_useTargetUp");
			var targetOffset = serializedObject.FindProperty("_targetOffset");
			var lookOffset = serializedObject.FindProperty("_lookOffset");
			var distanceFromTarget = serializedObject.FindProperty("_distanceFromTarget");
			
			EditorGUILayout.LabelField("Target", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(cameraTarget);
			EditorGUILayout.PropertyField(targetOffset);
			EditorGUILayout.PropertyField(lookOffset);
			EditorGUILayout.PropertyField(useTargetUp);
			EditorGUILayout.PropertyField(distanceFromTarget);
		}

		private void DrawInputFields()
		{
			var requireRmb = serializedObject.FindProperty("_requireRmb");
			var cameraMode = serializedObject.FindProperty("_cameraMode");
			
			var invertPitch = serializedObject.FindProperty("_invertPitch");
			var pitchSensitivity = serializedObject.FindProperty("_pitchSensitivity");
			var yawSensitivity = serializedObject.FindProperty("_yawSensitivity");
			
			EditorGUILayout.LabelField("Input", EditorStyles.boldLabel);
			
			EditorGUILayout.PropertyField(requireRmb);
			EditorGUILayout.PropertyField(cameraMode);
			
			EditorGUILayout.PropertyField(invertPitch);
			EditorGUILayout.PropertyField(pitchSensitivity);
			EditorGUILayout.PropertyField(yawSensitivity);
		}

		private void DrawScalerField()
		{
			var scaler = serializedObject.FindProperty("_scaler");
			
			EditorGUILayout.PropertyField(scaler);

			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("0.01"))	scaler.floatValue = 0.01f;
			if (GUILayout.Button("0.1"))	scaler.floatValue = 0.1f;
			if (GUILayout.Button("1"))		scaler.floatValue = 1f;
			if (GUILayout.Button("10"))	scaler.floatValue = 10f;
			if (GUILayout.Button("100"))	scaler.floatValue = 100f;
			EditorGUILayout.EndHorizontal();
		}

		protected override void DrawSceneGUI(SerializedObject sceneGuiSerializedObject)
		{
			base.DrawSceneGUI(sceneGuiSerializedObject);
			
			if (Application.isPlaying) return;
			
			var targetTransform = sceneGuiSerializedObject.FindProperty("_target").objectReferenceValue as Transform;
			var offset = sceneGuiSerializedObject.FindProperty("_targetOffset");

			if (targetTransform == null) return;
			
			var targetPosition = targetTransform.TransformPoint(offset.vector3Value);

			var newPosition = Handles.PositionHandle(targetPosition, Quaternion.identity);

			offset.vector3Value = targetTransform.InverseTransformPoint(newPosition);
		}

	}
}