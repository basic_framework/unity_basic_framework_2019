using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug
{
	[ExecuteAlways]
	[AddComponentMenu("Basic Framework/Core/Debug/Debug Third Person Camera")]
	public class DebugThirdPersonCamera : MonoBehaviour 
	{

		// ****************************************  VARIABLES  ****************************************\\

		private const float MIN_SENSITIVITY = 0.1f;
		private const float MAX_SENSITIVITY = 1f;
		private const float MIN_PITCH = -60f;
		private const float MAX_PITCH = 60f;
		
		private const float PAN_OFFSET_SENSITIVITY = 0.01f;
		private const int ZOOM_STEPS = 10;
		private const float ZOOM_DURATION = 1f;
		
		private enum CameraModes
		{
			Orbit,
			Strafe
		}
		
		//***** Editor *****\\
		
		[Range(MIN_PITCH, MAX_PITCH)]
		[SerializeField] private float _editorPitch = default;

		[Range(-180, 180)] 
		[SerializeField] private float _editorYaw = default;
		
		private float _prevEditorYaw;

		private float _absolutePitchAngle = 25f;

		private Quaternion _orientationToTarget;
		private Vector3 _requiredPosition;
		
		
		//***** Target *****\\
		
		[Tooltip("The target transform that the camera will be moving relative to and looking at")]
		[SerializeField] private Transform _target = default;
		
		[Tooltip("Offset from Target transform position, in target local space")]
		[SerializeField] private Vector3 _targetOffset = default;
		
		[Tooltip("Offset from target position for the camera to look at")]
		[SerializeField] private Vector3 _lookOffset = default;
		
		[Tooltip("Should the transform pitch align to the target up or world up")]
		[SerializeField] private bool _useTargetUp = default;
		
		[Tooltip("Min|Current|Max => Distance from target")]
		[SerializeField] private RangedFloat _distanceFromTarget = new RangedFloat(1, 3, 4);


		//***** Input *****\\
		
		[Tooltip("Does the right Mouse Button must be held to turn")]
		[SerializeField] private bool _requireRmb = default;
		
		[SerializeField] private CameraModes _cameraMode = default;
		
		[SerializeField] private bool _invertPitch = default;
		
		[Range(MIN_SENSITIVITY, MAX_SENSITIVITY)] 
		[SerializeField] private float _pitchSensitivity = 0.25f;
		
		[Range(MIN_SENSITIVITY, MAX_SENSITIVITY)] 
		[SerializeField]private float _yawSensitivity = 0.25f;

		[Tooltip("Scales zoom and pan speeds")]
		[Range(0.01f, 100f)]
		[SerializeField] private float _scaler = 1;
		
		private Vector3 _lastMousePosition;
		private Vector3 _mouseDelta;
		

		// ****************************************  METHODS  ****************************************\\
		
		private void Start()
		{
			if (!Application.isPlaying) return;
			
			transform.position = _target.position - _target.forward;
			_orientationToTarget = Quaternion.identity;
			
			_lastMousePosition = Input.mousePosition;
		}

		private void LateUpdate()
		{
			if (!Application.isPlaying)
			{
				if (!_target) return;
				
				var yawDelta = _editorYaw - _prevEditorYaw;
				_prevEditorYaw = _editorYaw;
				UpdatePose(yawDelta, _editorPitch);

				return;
			}
			
			//***** Process Input *****\\
			_mouseDelta = Input.mousePosition - _lastMousePosition;
			_lastMousePosition = Input.mousePosition;

			var relativeYawRotation = 0f;

			if (!_requireRmb || Input.GetKey(KeyCode.Mouse1))
			{
				relativeYawRotation = _mouseDelta.x * _yawSensitivity;

				_absolutePitchAngle += _mouseDelta.y * _pitchSensitivity * (_invertPitch ? 1 : -1);
				_absolutePitchAngle  = Mathf.Clamp(_absolutePitchAngle, MIN_PITCH, MAX_PITCH);
			}
			
			
			Zoom();
			PanTargetOffset();
			
			//***** Update POSE *****\\

			UpdatePose(relativeYawRotation, _absolutePitchAngle);
		}

		private void Zoom()
		{
			var scrollResolution = _distanceFromTarget.Range / ZOOM_STEPS;
			_distanceFromTarget.Value += Input.mouseScrollDelta.y * scrollResolution * -1;

			if (!Input.GetKey(KeyCode.Mouse1)) return;

			var zoomSpeed = _distanceFromTarget.Range / ZOOM_DURATION;
			var zoomAxis = Input.GetKey(KeyCode.DownArrow) ? -1 : Input.GetKey(KeyCode.UpArrow) ? 1 : 0;
			_distanceFromTarget.Value += zoomAxis * Time.deltaTime * zoomSpeed * -1;
		}
		
		private void PanTargetOffset()
		{
			if (!Input.GetKey(KeyCode.Mouse2)) return;
			
			var position = _target.TransformPoint(_targetOffset);
			position += _mouseDelta.y * PAN_OFFSET_SENSITIVITY * _scaler * _target.up;

			_targetOffset = _target.InverseTransformPoint(position);
		}

		private void UpdatePose(float deltaYaw, float absPitch)
		{
			//***** Calculate Orientation (Camera to Target) *****\\
			var targetPosition = _target.TransformPoint(_targetOffset);
			var targetUp = _useTargetUp ? _target.up : Vector3.up;
			
			// Calculates such that up is aligned with our target up and z points toward our target
			var toTarget = Vector3.zero;

			switch (_cameraMode)
			{
				case CameraModes.Orbit:
					toTarget = MathUtility.RemoveAxisFromVector(targetPosition - _requiredPosition, targetUp);
					break;
				case CameraModes.Strafe:
					toTarget = MathUtility.RemoveAxisFromVector(_orientationToTarget * Vector3.forward, targetUp);
					break;
			}
			
			_orientationToTarget = MathUtility.Direction2Quaternion(toTarget, targetUp);
			
			// Rotate around targetUp (Yaw)
			_orientationToTarget = MathUtility.RotateQuaternion(_orientationToTarget, deltaYaw, targetUp);

			// Rotate around local x (Pitch)
			_orientationToTarget = MathUtility.RotateQuaternion(_orientationToTarget, absPitch, _orientationToTarget * Vector3.right);
			
			_requiredPosition = targetPosition + _orientationToTarget * (Vector3.back * _distanceFromTarget.Value * _scaler);
			
			
			//***** Move Transform *****\\
			var transform1 = transform;
			transform1.position = _requiredPosition;
			
			//***** Rotate Transform (Look at target) *****\\
			var lookAtTarget = targetPosition + _orientationToTarget * _lookOffset;
			var rotation = MathUtility.Direction2Quaternion(lookAtTarget - transform1.position, targetUp);
			transform.rotation = rotation;
		}

		public void SetCameraToOrbit() 	{ _cameraMode = CameraModes.Orbit; }
		public void SetCameraToStrafe() { _cameraMode = CameraModes.Strafe; }
		
		private void OnDrawGizmosSelected()
		{
			if (!_target) return;
			
			var prevGizmoColor = Gizmos.color;
			
			var targetPosition = _target.TransformPoint(_targetOffset);
			
			var lookAtPosition = targetPosition + _orientationToTarget * _lookOffset;

			// Target
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(targetPosition, 0.1f);
			
			// Look at Position
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(lookAtPosition, 0.025f);
			
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(transform.position, targetPosition);
			
			Gizmos.color = prevGizmoColor;
		}
		
	}
}