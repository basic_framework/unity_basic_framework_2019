﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility.Screen;
using UnityEngine;

namespace BasicFramework.Core.BasicDebug
{
	[AddComponentMenu("Basic Framework/Core/Debug/Debug Fly Camera")]
	public class DebugFlyCamera : MonoBehaviour 
	{
	
		// TODO:
		// 		- Have a scaling factor that all values are based off of, i.e. the way unity adjust to the size of the focused object
		//		- Default scaling valuse, i.e. mm, cm, m, km, inch, foot. Default will be m i.e. scale 1 is metre

		// **************************************** VARIABLES ****************************************\\
		
		//***** General *****\\

		[Tooltip("The transform that will be driven by this component")]
		[SerializeField] private Transform _targetTransform = default;
		[SerializeField] private bool _cursorVisible = true;
		
		private Vector3 _lastMousePosition;
		private float _metrePerPixel;

		private Vector3 _mouseDelta;
		public Vector3 MouseDelta => _mouseDelta;
		
		public bool CursorVisible
		{
			get => _cursorVisible;
			set
			{
				if (_cursorVisible == value) return;
				_cursorVisible = value;

				if (Application.isPlaying)
				{
					Cursor.visible = value;
				}
			}
		}
		
		[Tooltip("Scales the values using this value. Assuming 1 is the metre scale.")]
		[Range(0.01f, 100f)]
		[SerializeField] private float _scaler = 1;
		
		
		//***** Rotation *****\\
		
		[SerializeField] private bool _rotationEnabled = true;
		[SerializeField] private bool _invertVertical = true;
		[SerializeField] private RangedFloat _rotateSensitivity = new RangedFloat(0, 0.25f, 1);
		
		public bool RotationEnabled
		{
			get => _rotationEnabled;
			set => _rotationEnabled = value;
		}
		
		public bool InvertVertical
		{
			get => _invertVertical;
			set => _invertVertical = value;
		}
		
		
		//***** Translation *****\\
		
		[SerializeField] private bool _translationEnabled = true;
		
		[Tooltip("Min: The speed when Ctrl is held\nMiddle: The normal speed\nMax: The speed when Shift is held")]
		[SerializeField] private RangedFloat _speedRange = new RangedFloat(0.2f, 5f, 10f);
		
		[Tooltip("Acceleration (m/s^2) of speed when translation held")]
		[SerializeField] private RangedFloat _acceleration = new RangedFloat(0f, 0.25f, 1f);
		
		public bool TranslationEnabled
		{
			get => _translationEnabled;
			set => _translationEnabled = value;
		}
		
		public float Acceleration => _acceleration.Value;

		private float _prevBaseSpeed;
		private float _heldSpeed;
		
		private float _currentSpeed;
		public float CurrentSpeed => _currentSpeed;

		private float ForwardInput 		=> Input.GetKey(KeyCode.W) ? 1 : (Input.GetKey(KeyCode.S) ? -1 : 0);
		private float VerticalInput 	=> Input.GetKey(KeyCode.E) ? 1 : (Input.GetKey(KeyCode.Q) ? -1 : 0);
		private float HorizontalInput 	=> Input.GetKey(KeyCode.D) ? 1 : (Input.GetKey(KeyCode.A) ? -1 : 0);


		//***** Panning *****\\
		
		[SerializeField] private bool _panningEnabled = true;
		[SerializeField] private bool _invertPan = true;
		[SerializeField] private RangedFloat _panSensitivity = new RangedFloat(1f, 50f, 100f);
		
		public bool PanningEnabled
		{
			get => _panningEnabled;
			set => _panningEnabled = value;
		}
		
		public bool InvertPan
		{
			get => _invertPan;
			set => _invertPan = value;
		}
		
		
		//***** Zooming *****\\
		
		private const float ZOOM_MOUSE_DELTA_SCALER = 0.01f;
		
		[SerializeField] private bool _zoomingEnabled = true;
		[SerializeField] private RangedFloat _zoomStepResolution = new RangedFloat(0.01f, 1f, 2f);
		[SerializeField] private RangedFloat _zoomSensitivity = new RangedFloat(0.01f, 1f, 2f);
		
		public bool ZoomingEnabled
		{
			get => _zoomingEnabled;
			set => _zoomingEnabled = value;
		}

		
		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_targetTransform = transform;
		}

		private void OnValidate()
		{
			// Stop the minimum speed from going negative
			if (_speedRange.Min < 0.001f)
			{
				_speedRange.Min = 0.001f;
			}

			if (Application.isPlaying)
			{
				Cursor.visible = _cursorVisible;
			}
		}

		private void Awake()
		{
			if (_targetTransform != null) return;
			_targetTransform = transform;
		}

		private void Start()
		{
			Cursor.visible = _cursorVisible;
			_lastMousePosition = Input.mousePosition;
			_metrePerPixel = ScreenUtility.Dpi / BasicMath.UnitConversion.LengthConversion.INCH_TO_METER;
		}

		private void Update()
		{
			// Get change in mouse position
			_mouseDelta = Input.mousePosition - _lastMousePosition;
			_lastMousePosition = Input.mousePosition;

			RotateCamera();
			TranslateCamera();
			PanCamera();
			ZoomCamera();
		}

		private void RotateCamera()
		{
			if (!_rotationEnabled) return;
			
			// RMB needs to be held
			if (!Input.GetMouseButton(1) || 
			    Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) return;
			
			var mouseInput = _mouseDelta * _rotateSensitivity.Value;

			var position = _targetTransform.position;
			_targetTransform.RotateAround(position, Vector3.up, mouseInput.x);
			_targetTransform.RotateAround(position, _targetTransform.right, mouseInput.y * (_invertVertical ? -1 : 1));			
		}

		private void TranslateCamera()
		{
			if (!_translationEnabled) return;
			
			// RMB needs to be held
			if (!Input.GetMouseButton(1)) return;
			
			var direction = new Vector3(HorizontalInput, VerticalInput, ForwardInput).normalized;

			if (Math.Abs(direction.sqrMagnitude) < Mathf.Epsilon)
			{
				_currentSpeed = 0;
				_heldSpeed = 0;
				_prevBaseSpeed = 0;
			}
			else
			{
				var baseSpeed = Input.GetKey(KeyCode.LeftShift)
					? _speedRange.Max
					: (Input.GetKey(KeyCode.LeftControl) ? _speedRange.Min : _speedRange.Value);

				if (Math.Abs(_prevBaseSpeed - baseSpeed) > Mathf.Epsilon)
				{
					_prevBaseSpeed = baseSpeed;
					_heldSpeed = 0;
				}

				_heldSpeed += _acceleration.Value * Time.deltaTime;
				
				_currentSpeed = baseSpeed + _heldSpeed;
			}
			
			_targetTransform.Translate(_currentSpeed * _scaler * Time.deltaTime * direction);
		}
		
		private void PanCamera()
		{
			if (!_panningEnabled) return; 
			
			// Middle mouse needs to be held
			if (!Input.GetMouseButton(2)) return;
			
			_targetTransform.Translate(_mouseDelta * _panSensitivity.Value / _metrePerPixel  * _scaler * (_invertPan ? -1 : 1));			
		}

		private void ZoomCamera()
		{
			if (!_zoomingEnabled) return;
			
			_targetTransform.Translate(Input.mouseScrollDelta.y * _zoomStepResolution.Value * _scaler * Vector3.forward);

			if ((Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
			    && Input.GetMouseButton(1))
			{
				_targetTransform.Translate(_mouseDelta.x * _zoomSensitivity.Value * ZOOM_MOUSE_DELTA_SCALER * _scaler * Vector3.forward);
			}
		}
	}
}