
Script groups, select top most fitting group.
 e.g. if a callback is also virtual, put it in the callback group as that comes first 

//***** abstract *****\\
- abstract functions that need to be overwritten

//***** mono *****\\
- monobehaviour callbacks (Awake, Start, Update, etc.)

//***** implemented *****\\
- functions that need to be implemented from interfaces

//***** overrides *****\\
- overriden functions, whether they are abstract or virtual

//***** callbacks *****\\
- callbacks for events (Prefix "On", e.g. OnValueChanged)

//***** property update *****\\
- called when a properties value changes (Suffix "Updated", e.g. ValueUpdated)
- argument of changed value
- possible argument of previous value (as necessary)

//***** virtual *****\\
- any virtual functions that may be overriden

//***** function *****\\
- any other functions