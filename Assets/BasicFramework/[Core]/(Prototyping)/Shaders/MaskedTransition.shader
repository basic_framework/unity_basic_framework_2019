﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "BasicFramework/Transitions/MaskedTransition"
{
Properties
	{
	    [Header(Automation)]
	    _Progress("Progress", Range (0, 1)) = 0
		
		[Space][Header(Mask)]
		[MaterialToggle] _InvertMaskAlpha("Invert", Float) = 0
		_MaskTex ("Image", 2D) = "white" {}
		
		
		[Space][Header(Background Image)]
		_Tint ("Tint", Color) = (1,1,1,1)
		_TransitionTex ("Image", 2D) = "black" {}
		
		
		
		[Space][Header(Rendered Image (DO NOT MODIFY))]
		_MainTex ("Image", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex VertexProgram
			#pragma fragment FragmentProgram
			
			#include "UnityCG.cginc"

            struct VertexData
            {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
            };
            
            struct Interpolators
            {
                float4 position : SV_POSITION;
                float2 uvMain : TEXCOORD0;
                float2 uvTransition : TEXCOORD1;
                float2 uvMask : TEXCOORD2;
            };

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			
			sampler2D _TransitionTex;
			float4 _TransitionTex_ST;
			
			sampler2D _MaskTex;
			float4 _MaskTex_ST;
			
			float _Progress;
			fixed4 _Tint;
			float _InvertMaskAlpha;

			Interpolators VertexProgram (VertexData v)
			{
				Interpolators i; 
                i.position = UnityObjectToClipPos(v.position);
                i.uvMain = v.uv * _MainTex_ST.xy + _MainTex_ST.zw;
                i.uvTransition = v.uv * _TransitionTex_ST.xy + _TransitionTex_ST.zw;
                i.uvMask = v.uv * _MaskTex_ST.xy + _MaskTex_ST.zw;
                return i;
			}
			
			// float4 was fixed4
			float4 FragmentProgram(Interpolators i) : SV_Target
			{
			    // The rgba color of our transition material at the appropriate uv position
				fixed4 mask = tex2D(_MaskTex, i.uvMask);
	
				// The transition texture color for this pixel	
				float4 result = tex2D(_TransitionTex, i.uvTransition);
				
				if(!_InvertMaskAlpha)
				{
				    // Return the original texture if our mask value is greater than the progress value
				    if(_Progress == 0 || mask.b > _Progress) 
				        return tex2D(_MainTex, i.uvMain);
				}
				else
				{
				    // Invert the progress value so that the transition still goes from 0-1, not 1-0
				    _Progress = 1 - _Progress;
				   
				    if(_Progress == 1 || mask.b < _Progress) 
				        return tex2D(_MainTex, i.uvMain);
				}
				
				// Tint the result, if we are not using an image will probably want to use black
				result *= _Tint;
				 
				return result;
			}
			ENDCG
		}
	}
}
