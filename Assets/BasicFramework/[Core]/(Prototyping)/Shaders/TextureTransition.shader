﻿Shader "BasicFramework/Transitions/TextureTransition"
{
	Properties 
	{
	    [Header(Automation)]
	    _Progress       ("Progress", Range(0,1)) = 0
	    
	    [Space][Header(Background Image)]
		_Tint		    ("Tint", Color) = (1, 1, 1, 1)
		_TransitionTex	("Image", 2D) = "black" {}
		
        [Space][Header(Rendered Image (DO NOT MODIFY))]
		_MainTex	    ("Image", 2D) = "white" {}
	}

	SubShader 
	{
		Pass 
		{
			CGPROGRAM
			#pragma vertex VertexProgram
			#pragma fragment FragmentProgram

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			float4 _MainTex_ST;
			
			sampler2D _TransitionTex;
			float4 _TransitionTex_ST;
			float4 _Tint;
			float _Progress;

            struct VertexData
            {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
            };
            
            struct Interpolators
            {
                float4 position : SV_POSITION;
                float2 uvMain : TEXCOORD0;
                float2 uvTransition : TEXCOORD1;
            };
            
            Interpolators VertexProgram(VertexData v)
            {
                Interpolators i; 
                i.position = UnityObjectToClipPos(v.position);
                i.uvMain = v.uv * _MainTex_ST.xy + _MainTex_ST.zw;
                i.uvTransition = v.uv * _TransitionTex_ST.xy + _TransitionTex_ST.zw;
                return i;
            }

            // SV_TARGET was COLOR
			float4 FragmentProgram(Interpolators i) : SV_TARGET 
			{
				// Lerping 
				float4 mainTexColor = tex2D(_MainTex, i.uvMain);
				float4 result = mainTexColor;

				float4 transitionTextureColor = tex2D(_TransitionTex, i.uvTransition);
				transitionTextureColor *= _Tint;
				
				result.rgb = lerp(mainTexColor.rgb, transitionTextureColor.rgb, _Progress);
				result.rgb *= lerp((1.0).xxx, transitionTextureColor, _Progress);
				
				return result;
			}
			ENDCG
		}
	}
}
