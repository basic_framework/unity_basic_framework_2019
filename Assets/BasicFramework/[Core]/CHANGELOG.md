
## Unreleased - [00-02]

### Added

- 

### Changed

- 

### Bug Fixes

-

### Removed

-


## Unreleased - [00-01] - 20YY-MM-DD

### Added

- BasicAnimation: Simple animation related scripts
- BasicAnimation/Transition: Scripts that transition values from one state to another over time (e.g. tweener)

- BasicDebug: Debug classes/behaviours useful during development

- BasicGui: Gui related scripts
- BasicGui/Pointer: Raycast based ui input system (useful for VR)
- BasicGUI/Components: Components to extend UnityEngine.UI components (e.g. Text, Button, Slider, Dropdown, etc)

- BasicInteractions: A group of scripts that provide interactability with the 3D environment

- BasicMath: Helpful mathematical scripts

- BasicMedia: Scripts relating to media functionality (Images, Audio, Video)

- BasicMonoBehaviours: A group of useful mono behaviours

- BasicSceneManagement: Classes related to scene management

- BasicTypes: A group of custom variable types

- PrototypingResources: Some basic resources that will help to build quick prototypes

- ScriptableEvents: A system that uses ScriptableObjects for events

- ScriptableInputs: A system that uses ScriptableObjects for inputs

- ScriptableVariables: A system that uses ScriptableObjects for variables

- Utility/Screen: Added screen fading scripts. Create screen fading components by inheriting from ScreenFaderBase and adding logic to control how the screen is faded
