#Basic Framework 2019 - Core


What features does the core package offer
- ScriptableInputs
- ScriptableVariables
- ScriptableEvents
- Tweening

##ScriptableInput vs ScriptableVariable vs ScriptableEvent

ScriptableInput - is best used when you want a frequently updated property (every frame) that is not persistent

ScriptableVariable - is best used when you want a persistent value that can be accessed/changed at any time (usually infrequently)

ScriptableEvent - is best used when you want an infrequent value without a persistent state 


#Licenses
- MIT