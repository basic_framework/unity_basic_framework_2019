﻿using UnityEngine;

namespace BasicFramework.Core.Utility.Examples
{
	public class SaveLoadExampleData : SaveLoadDataBase<ExampleData>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		public override string FileDirectory => Application.persistentDataPath + "/BasicFramework";
		public override string FileName => "example_data.json";


		[SerializeField] private ExampleData _exampleData = default;
		
	
		// ****************************************  METHODS  ****************************************\\

		
		//***** overrides *****\\
		
		protected override ExampleData GetSaveData()
		{
			return _exampleData;
		}

		protected override bool OnDataLoaded(ExampleData data)
		{
			_exampleData.StringData = data.StringData;
			_exampleData.FloatData = data.FloatData;
			_exampleData.IntData = data.IntData;
			_exampleData.BoolData = data.BoolData;
			
			return true;
		}
	}
}