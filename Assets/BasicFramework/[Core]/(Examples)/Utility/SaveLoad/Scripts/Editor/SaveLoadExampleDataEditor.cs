﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Core.Utility.Examples.Editor
{
	[CustomEditor(typeof(SaveLoadExampleData))]
	public class SaveLoadExampleDataEditor : SaveLoadDataBaseEditor<ExampleData, SaveLoadExampleData>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var exampleData = serializedObject.FindProperty("_exampleData");

			Space();
			SectionHeader($"{nameof(SaveLoadExampleData).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(exampleData);
		}
	
	}
}