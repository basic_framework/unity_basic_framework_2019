﻿using System;
using UnityEngine;

namespace BasicFramework.Core.Utility.Examples
{
	[Serializable]
	public class ExampleData 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private string _stringData = "Hello World";

		[SerializeField] private float _floatData = 0.5f;

		[SerializeField] private int _intData = -1;

		[SerializeField] private bool _boolData = false;


		public string StringData
		{
			get => _stringData;
			set => _stringData = value;
		}

		public float FloatData
		{
			get => _floatData;
			set => _floatData = value;
		}

		public int IntData
		{
			get => _intData;
			set => _intData = value;
		}

		public bool BoolData
		{
			get => _boolData;
			set => _boolData = value;
		}

		// ****************************************  METHODS  ****************************************\\


	}
}