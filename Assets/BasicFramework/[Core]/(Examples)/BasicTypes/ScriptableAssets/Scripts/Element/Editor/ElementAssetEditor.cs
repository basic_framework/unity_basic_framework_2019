using BasicFramework.Core.BasicTypes.Editor;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAsset))]
    public class ElementAssetEditor : ScriptableAssetBaseEditor<ElementAsset>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();
			
            Space();
            DrawElementFields();

        }

        private void DrawElementFields()
        {
            var weakAgainst = serializedObject.FindProperty("_weakAgainst");
			
            SectionHeader("Element");

            BasicEditorGuiLayout.PropertyFieldNotNull(weakAgainst);
        }
    }
}