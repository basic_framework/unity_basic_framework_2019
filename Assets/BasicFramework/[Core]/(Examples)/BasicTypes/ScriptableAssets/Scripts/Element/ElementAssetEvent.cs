using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Examples
{
    [CreateAssetMenu(
	    menuName = "Basic Framework/Core Examples/Scriptable Assets/Element Asset Event", 
	    fileName = "event_asset_element_")
    ]
    public class ElementAssetEvent : ScriptableEventBase<ElementAsset>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}