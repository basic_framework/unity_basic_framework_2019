using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Examples
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core Examples/Scriptable Assets/Element Asset Single", 
        fileName = "single_asset_element_")
    ]
    public class ElementAssetSingle : SingleObjectBase<ElementAsset>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
    [Serializable]
    public class ElementAssetSingleReference : SingleObjectReferenceBase<ElementAsset, ElementAssetSingle>
    {
        public ElementAssetSingleReference()
        {
        }

        public ElementAssetSingleReference(ElementAsset value)
        {
            _constantValue = value;
        }
    }
}