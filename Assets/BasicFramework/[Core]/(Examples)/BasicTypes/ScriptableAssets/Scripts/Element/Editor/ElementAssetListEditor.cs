using BasicFramework.Core.ScriptableVariables.List.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAssetList))][CanEditMultipleObjects]
    public class ElementAssetListEditor : ScriptableListBaseEditor<ElementAsset, ElementAssetList>
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        
        // ****************************************  METHODS  ****************************************\\
        
    }
    
    [CustomPropertyDrawer(typeof(ElementAssetList))]
    public class ElementAssetListPropertyDrawer : ScriptableListBasePropertyDrawer
    {
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
}