﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Examples
{
	[AddComponentMenu("Basic Framework/Core Examples/Scriptable Assets/Element Asset Single Listener")]
	public class ElementAssetSingleListener : SingleObjectListenerBase
	<
		ElementAsset, 
		ElementAssetSingle, 
		ElementAssetUnityEvent, 
		ElementAssetCompareUnityEvent
	>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}