using BasicFramework.Core.ScriptableVariables.List;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Examples
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core Examples/Scriptable Assets/Element Asset List", 
        fileName = "list_asset_element_")
    ]
    public class ElementAssetList : ScriptableListBase<ElementAsset>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}