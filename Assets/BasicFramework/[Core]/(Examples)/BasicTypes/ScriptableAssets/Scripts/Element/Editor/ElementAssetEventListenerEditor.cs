﻿using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAssetEventListener))][CanEditMultipleObjects]
    public class ElementAssetEventListenerEditor : ScriptableEventListenerBaseEditor
    <
        ElementAsset, 
        ElementAssetEvent, 
        ElementAssetUnityEvent,
        ElementAssetEventListener
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}