using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAssetSingle))][CanEditMultipleObjects]
    public class ElementAssetSingleEditor : SingleObjectBaseEditor<ElementAsset, ElementAssetSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(ElementAssetSingle))]
    public class ElementAssetSinglePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(ElementAssetSingleReference))]
    public class ElementAssetSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
	    
        // **************************************** VARIABLES ****************************************\\
	    
	    
        // ****************************************  METHODS  ****************************************\\

    }
}