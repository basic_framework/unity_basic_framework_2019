using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Core.BasicTypes.Examples
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Core Examples/Scriptable Assets/Element Asset", 
        fileName = "asset_element_")
    ]
    public class ElementAsset : ScriptableAssetBase 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        [SerializeField] private ElementAsset _weakAgainst = default;
		
        public ElementAsset WeakAgainst => _weakAgainst;
        

        // ****************************************  METHODS  ****************************************\\


    }
	
    [Serializable]
    public class ElementAssetUnityEvent : UnityEvent<ElementAsset>{}
    
    [Serializable]
    public class ElementAssetCompareUnityEvent : CompareValueUnityEventBase<ElementAsset>{}
	
    [Serializable]
    public class ElementAssetArrayUnityEvent : UnityEvent<ElementAsset[]>{}

}