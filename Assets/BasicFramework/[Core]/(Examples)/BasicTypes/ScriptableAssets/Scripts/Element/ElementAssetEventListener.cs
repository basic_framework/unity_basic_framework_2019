﻿using BasicFramework.Core.ScriptableEvents;
using UnityEngine;

namespace BasicFramework.Core.BasicTypes.Examples
{
    [AddComponentMenu("Basic Framework/Core Examples/Scriptable Assets/Element Asset Event Listener")]
    public class ElementAssetEventListener : ScriptableEventListenerBase<ElementAsset, ElementAssetEvent, ElementAssetUnityEvent>
    {
				
        // **************************************** VARIABLES ****************************************\\

        
        // ****************************************  METHODS  ****************************************\\

        
    }
}