﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAssetSingleListener))][CanEditMultipleObjects]
    public class ElementAssetSingleListenerEditor : SingleObjectListenerBaseEditor
    <
        ElementAsset, 
        ElementAssetSingle, 
        ElementAssetUnityEvent,
        ElementAssetCompareUnityEvent,
        ElementAssetSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}