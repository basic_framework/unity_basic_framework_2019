using BasicFramework.Core.ScriptableEvents.Editor;
using UnityEditor;

namespace BasicFramework.Core.BasicTypes.Examples.Editor
{
    [CustomEditor(typeof(ElementAssetEvent))][CanEditMultipleObjects]
    public class ElementAssetEventEditor : ScriptableEventBaseEditor<ElementAsset, ElementAssetEvent>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }

    [CustomPropertyDrawer(typeof(ElementAssetEvent))]
    public class ElementAssetEventPropertyDrawer : ScriptableEventBasePropertyDrawer<ElementAsset, ElementAssetEvent>
    {
		
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
}