﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Communication.Websocket.Editor
{
    [CustomEditor(typeof(WebsocketSettingsSingleListener))][CanEditMultipleObjects]
    public class WebsocketSettingsSingleListenerEditor : SingleStructListenerBaseEditor
    <
        WebsocketSettings, 
        WebsocketSettingsSingle, 
        WebsocketSettingsUnityEvent,
        WebsocketSettingsCompareUnityEvent,
        WebsocketSettingsSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}