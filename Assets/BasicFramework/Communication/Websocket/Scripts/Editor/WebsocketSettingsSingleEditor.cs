﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Websocket.Editor
{
    [CustomEditor(typeof(WebsocketSettingsSingle))][CanEditMultipleObjects]
    public class WebsocketSettingsSingleEditor : SingleStructBaseEditor<WebsocketSettings, WebsocketSettingsSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\

        protected override void DrawPropertyFields()
        {
            base.DrawPropertyFields();

            Space();
            Space();
            if (!GUILayout.Button("Set to Default Websocket Settings")) return;
            foreach (var typedTarget in TypedTargets)
            {
                Undo.RecordObject(typedTarget, "reset websocket settings to default");
			        
                typedTarget.Value = WebsocketSettings.Default;
                typedTarget.EditorSetDefaultValueToValue();
            }
        }

        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(WebsocketSettingsSingle))]
    public class WebsocketSettingsSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(WebsocketSettingsSingleReference))]
    public class WebsocketSettingsSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}