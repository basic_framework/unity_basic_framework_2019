﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Websocket.Editor
{
	[CustomPropertyDrawer(typeof(WebsocketSettings))]
	public class WebsocketSettingsPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\


		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			if (!property.isExpanded) return;

			var useLocalNetwork = property.FindPropertyRelative("_useLocalNetwork");
			
			var url = property.FindPropertyRelative("_url");
			
			var hostname = property.FindPropertyRelative("_hostname");
			var port = property.FindPropertyRelative("_port");
			var serviceName = property.FindPropertyRelative("_serviceName");
			
			EditorGUI.PropertyField(TempRect, useLocalNetwork);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			if (useLocalNetwork.boolValue)
			{
				EditorGUI.LabelField(TempRect, "URL", 
					WebsocketSettings.GetUrl(hostname.stringValue, port.intValue, serviceName.stringValue));
			}
			else
			{
				EditorGUI.PropertyField(TempRect, url);
			}
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			GUI.enabled = PrevGuiEnabled && useLocalNetwork.boolValue;

			EditorGUI.PropertyField(TempRect, hostname);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			EditorGUI.PropertyField(TempRect, port);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			EditorGUI.PropertyField(TempRect, serviceName);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded)
			{
				return height;
			}
			
			// Use local network
			height += BUFFER_HEIGHT + SingleLineHeight;
			
			// URL display
			height += BUFFER_HEIGHT + SingleLineHeight;
			
			height += BUFFER_HEIGHT + SingleLineHeight;
			height += BUFFER_HEIGHT + SingleLineHeight;
			height += BUFFER_HEIGHT + SingleLineHeight;
			
			return height;
		}
	}
}