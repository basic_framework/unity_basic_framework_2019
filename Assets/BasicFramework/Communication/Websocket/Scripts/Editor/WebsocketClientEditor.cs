﻿using BasicFramework.Communication.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Websocket.Editor
{
	[CustomEditor(typeof(WebsocketClient))]
	public class WebsocketClientEditor : CommunicationBaseEditor<WebsocketClient> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawChildDebugFields()
		{
			GUI.enabled = false;
			EditorGUILayout.LabelField("Websocket Url", TypedTarget.WebsocketUrl);
			EditorGUILayout.LabelField("Websocket State", TypedTarget.WebSocketState.ToString());
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var websocketSettings = serializedObject.FindProperty("_websocketSettings");
			
			Space();
			SectionHeader("Websocket Port Client Properties");
			
			EditorGUILayout.PropertyField(websocketSettings, true);
		}
		
	}
}