﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Communication.Websocket
{
    [AddComponentMenu("Basic Framework/Communication/Websocket/Websocket Settings Single Listener")]
    public class WebsocketSettingsSingleListener : SingleStructListenerBase
    <
        WebsocketSettings, 
        WebsocketSettingsSingle, 
        WebsocketSettingsUnityEvent, 
        WebsocketSettingsCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}