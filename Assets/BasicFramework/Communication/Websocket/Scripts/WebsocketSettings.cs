﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Communication.Websocket
{
	[Serializable]
	public struct WebsocketSettings 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		public static WebsocketSettings Default = new WebsocketSettings("localhost", 7777, "");
		
		[Tooltip("Is the websocket server running on the same network as the client")]
		[SerializeField] private bool _useLocalNetwork;
		
		[Tooltip("The wide area network websocket server url")]
		[SerializeField] private string _url;
		
		[Tooltip("The local network hostname or IP address")]
		[SerializeField] private string _hostname;
		
		[Tooltip("The port the service is running on")]
		[Range(0, 65535)]
		[SerializeField] private int _port;
		
		[Tooltip("The name of the websocket service to connect to (if required)")]
		[SerializeField] private string _serviceName;

		
		
		// The URL for the current settings
		public string Url => _useLocalNetwork ? LocalUrl : _url;
		
		private string LocalUrl => GetUrl(_hostname, _port, _serviceName);
		
	
		// ****************************************  METHODS  ****************************************\\

		public WebsocketSettings(string url)
		{
			_useLocalNetwork = false;

			_url = url;
			
			_hostname = "localhost";
			_port = 7777;
			_serviceName = "";
		}
		
		public WebsocketSettings(string hostname, int port, string serviceName)
		{
			_useLocalNetwork = true;
			
			_hostname = hostname;
			_port = port;
			_serviceName = serviceName;

			_url = "ws://echo.websocket.org";
		}
		
		public static string GetUrl(string hostname, int port, string serviceName)
		{
			return $"ws://{hostname}:{port}/{serviceName}";
		}

		public override string ToString()
		{
			return Url;
		}
	}
	
	[Serializable]
	public class WebsocketSettingsUnityEvent : UnityEvent<WebsocketSettings>{}
	
	[Serializable]
	public class WebsocketSettingsCompareUnityEvent : CompareValueUnityEventBase<WebsocketSettings>{}
}