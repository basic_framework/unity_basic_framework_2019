﻿using System;
using UnityEngine;
using NativeWebSocket;

namespace BasicFramework.Communication.Websocket
{
	[AddComponentMenu("Basic Framework/Communication/Websocket/Websocket Client")]
	public class WebsocketClient : CommunicationBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private WebsocketSettingsSingleReference _websocketSettings = 
			new WebsocketSettingsSingleReference(WebsocketSettings.Default);
		
		
		public WebSocketState WebSocketState
		{
			get
			{
				if (_threadWebsocket == null) return WebSocketState.Closed;
				lock (_threadWebsocket) { return _threadWebsocket.State; }
			}
		}
		
		public string WebsocketUrl => 
			ThreadOpen || !_websocketSettings.IsValueValid 
				? _threadUrl 
				: _websocketSettings.Value.Url;
		
		// Thread use only
		private WebSocket _threadWebsocket;
		private string _threadUrl;
		private float _connectingTimer;
		
		public override float ConnectingTimer => _connectingTimer;
		

		// ****************************************  METHODS  ****************************************\\
		
		protected override void ConnectionThread()
		{
			_threadUrl = _websocketSettings.Value.Url;
			_threadWebsocket = new WebSocket(_threadUrl);

			_threadWebsocket.OnOpen += OnOpen;
			_threadWebsocket.OnMessage += OnMessage;
			_threadWebsocket.OnError += OnError;
			_threadWebsocket.OnClose += OnClose;
			
			while (!CloseConnectionFlag)
			{
				ConnectThread();
				
				if(CloseConnectionFlag) break;
				
				ConnectedThread();		
				
				if(CloseConnectionFlag || !AutomaticallyReconnect) break;
			}
			
			if (ConnectionState != ConnectionStates.Disconnected)
			{
				EnqueueConnectionState(ConnectionStates.Disconnecting);
			}
			
			// Cleanup
			try
			{
				var closeTask = _threadWebsocket.Close();
			}
			catch (Exception exception)
			{
				Debug.LogError($"{exception.Message}: {exception.StackTrace}");
			}
			
			EnqueueConnectionState(ConnectionStates.Disconnected);
			
			lock (_threadWebsocket)
			{
				_threadWebsocket = null;
			}
		}
		
		private void ConnectThread()
		{
			EnqueueConnectionState(ConnectionStates.Connecting);
			
			_connectingTimer = 0f;
			
			var connectTask = _threadWebsocket.Connect();

			while (!CloseConnectionFlag && _threadWebsocket.State == WebSocketState.Connecting)
			{
				_connectingTimer += SleepConnectionThread();
			}
			
			if(_threadWebsocket.State != WebSocketState.Open) return;

			EnqueueConnectionState(ConnectionStates.Connected);
		}
		
		private void ConnectedThread()
		{
			while (!CloseConnectionFlag && _threadWebsocket.State == WebSocketState.Open)
			{
				// Send any required data
				try
				{
					var outputMessage = ReadFromOutputBuffer();
					if (!string.IsNullOrEmpty(outputMessage))
					{
						_threadWebsocket.SendText(outputMessage);
					}
				}
				catch (Exception exception)
				{
					Debug.LogError($"{exception.Message}: {exception.StackTrace}");
				}

				SleepConnectionThread();
			}
		}
		
		
		//***** Websocket Callbacks *****\\
		
		private void OnOpen()
		{
			LogDebugMessage("Websocket Callback OnOpen");
		}

		private void OnMessage(byte[] data)
		{
			// convert the data into a string message
			var message = System.Text.Encoding.UTF8.GetString(data);
			LogDebugMessage("Websocket Callback OnMessage: " + message);
			WriteToInputBuffer(message);
		}

		private void OnError(string errorMsg)
		{
			LogDebugMessage("Websocket Callback OnError: " + errorMsg);
		}

		private void OnClose(WebSocketCloseCode closeCode)
		{
			LogDebugMessage("Websocket Callback OnClose: " + closeCode);
		}
	
	}
}