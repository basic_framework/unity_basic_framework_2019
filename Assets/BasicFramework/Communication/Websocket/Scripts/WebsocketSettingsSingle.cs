﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Communication.Websocket
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Communication/Websocket/Websocket Settings", 
        fileName = "_websocket_settings_single")
    ]
    public class WebsocketSettingsSingle : SingleStructBase<WebsocketSettings> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(WebsocketSettingsSingle value)
        {
            Value = value.Value;
        }
        
    }

    [Serializable]
    public class WebsocketSettingsSingleReference : SingleStructReferenceBase<WebsocketSettings, WebsocketSettingsSingle>
    {
        public WebsocketSettingsSingleReference()
        {
        }

        public WebsocketSettingsSingleReference(WebsocketSettings value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class WebsocketSettingsSingleUnityEvent : UnityEvent<WebsocketSettingsSingle>{}
	
    [Serializable]
    public class WebsocketSettingsSingleArrayUnityEvent : UnityEvent<WebsocketSettingsSingle[]>{}
    
}