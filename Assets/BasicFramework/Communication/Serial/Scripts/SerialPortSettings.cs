﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Communication.Serial
{
	[Serializable]
	public struct SerialPortSettings
	{
	
		// **************************************** VARIABLES ****************************************\\

		public static SerialPortSettings Default =
			new SerialPortSettings(0, new BaudRate(BaudRates._115200), 100, 100, 1000);
		
		[SerializeField] private int _port;
		[SerializeField] private BaudRate _baudRate;

		[SerializeField] private int _readTimeoutMs;
		[SerializeField] private int _writeTimeoutMs;
		
		[SerializeField] private int _reconnectionDelayMilliseconds;

		public string PortString => (_port <= 9 ? "COM" : "\\\\.\\COM") + _port;
		
		public int Port
		{
			get => _port;
			set => _port = value;
		}
		
		public BaudRate BaudRate
		{
			get => _baudRate;
			set => _baudRate = value;
		}

		public int ReadTimeoutMs
		{
			get => _readTimeoutMs;
			set => _readTimeoutMs = value;
		}

		public int WriteTimeoutMs
		{
			get => _writeTimeoutMs;
			set => _writeTimeoutMs = value;
		}

		public int ReconnectionDelayMilliseconds
		{
			get => _reconnectionDelayMilliseconds;
			set => _reconnectionDelayMilliseconds = value;
		}
		

		// ****************************************  METHODS  ****************************************\\

		public SerialPortSettings(int port, BaudRate baudRate, int readTimeoutMs, int writeTimeoutMs,
			int reconnectionDelayMilliseconds)
		{
			_port = port;
			_baudRate = baudRate;
			
			_readTimeoutMs = readTimeoutMs;
			_writeTimeoutMs = writeTimeoutMs;
			_reconnectionDelayMilliseconds = reconnectionDelayMilliseconds;
		}

		public override string ToString()
		{
			return $"Port: {_port}, Baud Rate: {_baudRate.StringRate}";
		}
	}
	
	[Serializable]
	public class SerialPortSettingsUnityEvent : UnityEvent<SerialPortSettings>{}
	
	[Serializable]
	public class SerialPortSettingsCompareUnityEvent : CompareValueUnityEventBase<SerialPortSettings>{}
}