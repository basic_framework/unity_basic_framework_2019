﻿using BasicFramework.Communication.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Serial.Editor
{
	[CustomEditor(typeof(SerialPortClient))]
	public class SerialPortClientEditor : CommunicationBaseEditor<SerialPortClient>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawChildDebugFields()
		{
			GUI.enabled = false;
			EditorGUILayout.LabelField("Current Port", TypedTarget.ConnectedPort);
			EditorGUILayout.LabelField("Current Baud Rate", TypedTarget.ConnectedBaudRate.ToString());
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var serialPortSettings = serializedObject.FindProperty("_serialPortSettings");
			
			Space();
			SectionHeader("Serial Port Client Properties");

			EditorGUILayout.PropertyField(serialPortSettings, true);
		}
		
	}
}