﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Serial.Editor
{
    [CustomPropertyDrawer(typeof(BaudRate))]
    public class BaudRatePropertyDrawer : BasicBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var baudRate = property.FindPropertyRelative("_rate");

            EditorGUI.PropertyField(position, baudRate, new GUIContent(label));
        }
    }
}