﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Communication.Serial.Editor
{
    [CustomEditor(typeof(SerialPortSettingsSingleListener))][CanEditMultipleObjects]
    public class SerialPortSettingsSingleListenerEditor : SingleStructListenerBaseEditor
    <
        SerialPortSettings, 
        SerialPortSettingsSingle, 
        SerialPortSettingsUnityEvent,
        SerialPortSettingsCompareUnityEvent,
        SerialPortSettingsSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}