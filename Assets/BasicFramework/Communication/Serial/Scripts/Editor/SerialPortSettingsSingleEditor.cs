﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Serial.Editor
{
    [CustomEditor(typeof(SerialPortSettingsSingle))][CanEditMultipleObjects]
    public class SerialPortSettingsSingleEditor : SingleStructBaseEditor<SerialPortSettings, SerialPortSettingsSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\

        protected override void DrawPropertyFields()
        {
	        base.DrawPropertyFields();

	        Space();
	        Space();
	        if (!GUILayout.Button("Set to Default Serial Port Settings")) return;
	        foreach (var typedTarget in TypedTargets)
	        {
		        Undo.RecordObject(typedTarget, "reset serial port settings to default");
			        
		        typedTarget.Value = SerialPortSettings.Default;
		        typedTarget.EditorSetDefaultValueToValue();
	        }
        }

        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(SerialPortSettingsSingle))]
    public class SerialPortSettingsSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(SerialPortSettingsSingleReference))]
    public class SerialPortSettingsSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}