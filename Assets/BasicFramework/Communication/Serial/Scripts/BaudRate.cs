﻿using UnityEngine;

namespace BasicFramework.Communication.Serial
{
    public enum BaudRates
    {
        _300,
        _600,
        _1200,
        _2400,
        _4800,
        _9600,
        _14400,
        _19200,
        _28800,
        _38400,
        _57600,
        _115200
    }
	
    [System.Serializable]
    public struct BaudRate
    {
		
        // **************************************** VARIABLES ****************************************\\
		
        [SerializeField] private BaudRates _rate;

        public BaudRates Rate
        {
            get => _rate;
            set => _rate = value;
        }

        public int IntRate => int.Parse(StringRate);

        public string StringRate => _rate.ToString().Replace("_", "");

        // ****************************************  METHODS  ****************************************\\

        public BaudRate(BaudRates baudRate)
        {
            _rate = baudRate;
        }
		
        public static implicit operator int(BaudRate baudRate)
        {
            return baudRate.IntRate;
        }
		
    }
}