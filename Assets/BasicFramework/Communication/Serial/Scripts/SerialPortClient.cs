﻿using System;
using System.IO.Ports;
using UnityEngine;

namespace BasicFramework.Communication.Serial
{
	[AddComponentMenu("Basic Framework/Communication/Serial/Serial Port Client")]
	public class SerialPortClient : CommunicationBase 
	{

		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private SerialPortSettingsSingleReference _serialPortSettings 
			= new SerialPortSettingsSingleReference(SerialPortSettings.Default);

		
		public SerialPortSettings SerialPortSettings => _serialPortSettings.Value;

		// Possible race conditions with _threadSerialPort
		public string ConnectedPort
		{
			get
			{
				if (_threadSerialPort == null) return "N/A";
				return  _threadSerialPort.PortName;
			}
		}

		public int ConnectedBaudRate
		{
			get
			{
				if (_threadSerialPort == null) return 0;
				return _threadSerialPort.BaudRate;
			}
		}
		
		private int _connectedBaudRate;

		// Thread use only
		private SerialPortSettings _threadSerialPortSetting; 
		private SerialPort _threadSerialPort;
		
		private float _connectingTimer;
		public override float ConnectingTimer => _connectingTimer;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override void ConnectionThread()
		{
			_threadSerialPortSetting = _serialPortSettings.Value;
			
			_threadSerialPort = new SerialPort(_threadSerialPortSetting.PortString, _threadSerialPortSetting.BaudRate)
			{
				ReadTimeout = _threadSerialPortSetting.ReadTimeoutMs,
				WriteTimeout = _threadSerialPortSetting.WriteTimeoutMs
			};
			
			while (!CloseConnectionFlag)
			{
				ConnectThread();
				
				if(CloseConnectionFlag) break;
				
				ConnectedThread();		
				
				if(CloseConnectionFlag || !AutomaticallyReconnect) break;
			}
			
			if (ConnectionState != ConnectionStates.Disconnected)
			{
				EnqueueConnectionState(ConnectionStates.Disconnecting);
			}
			
			// Cleanup
			try
			{
				_threadSerialPort.Close();
			}
			catch (Exception exception)
			{
				Debug.LogError($"{exception.Message}: {exception.StackTrace}");
			}
			
			EnqueueConnectionState(ConnectionStates.Disconnected);

			lock (_threadSerialPort)
			{
				_threadSerialPort = null;
			}

		}

		private void ConnectThread()
		{
			EnqueueConnectionState(ConnectionStates.Connecting);
			
			var reconnectTimer = 0f;
			_connectingTimer = 0;
			
			while (!CloseConnectionFlag)
			{
				if (reconnectTimer > 0)
				{
					var sleepTime = SleepConnectionThread();
					reconnectTimer -= sleepTime;
					_connectingTimer += sleepTime;
					continue;
				}
				
				try
				{
					_threadSerialPort.Open();
				}
				catch (Exception exception)
				{
					Debug.LogError($"{exception.Message}: {exception.StackTrace}" );
					reconnectTimer = _threadSerialPortSetting.ReconnectionDelayMilliseconds / 1000f;
				}
				
				if(!_threadSerialPort.IsOpen) continue;

				// This should clear the buffer
				_threadSerialPort.ReadExisting();
				
				EnqueueConnectionState(ConnectionStates.Connected);
				
				break;
			}
		}

		private void ConnectedThread()
		{
			while (!CloseConnectionFlag && _threadSerialPort.IsOpen)
			{
				try
				{
					// Output, TODO: possibly loop through all before continuing
					var outputMessage = ReadFromOutputBuffer();
					if (!string.IsNullOrEmpty(outputMessage))
					{
						_threadSerialPort.WriteLine(outputMessage);
					}

					// Input, TODO: possibly loop through all before continuins
					// TODO: See if this check works
					if (_threadSerialPort.BytesToRead > 0)
					{
						WriteToInputBuffer(_threadSerialPort.ReadLine());
					}
					
				}
				catch (TimeoutException)
				{
					// ReadLine will timeout if there is nothing to read
					// TODO: Find working way to first check if there is something to read from the buffer
				}
				catch (Exception exception)
				{
					Debug.LogError($"{exception.Message}: {exception.StackTrace}");
				}
					
				// No need to spam the read
				SleepConnectionThread();
			}
		}
		
	}
}