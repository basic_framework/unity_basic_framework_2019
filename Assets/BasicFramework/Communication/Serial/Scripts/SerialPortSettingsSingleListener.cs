﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Communication.Serial
{
    [AddComponentMenu("Basic Framework/Communication/Serial/Serial Port Settings Single Listener")]
    public class SerialPortSettingsSingleListener : SingleStructListenerBase
    <
        SerialPortSettings, 
        SerialPortSettingsSingle, 
        SerialPortSettingsUnityEvent, 
        SerialPortSettingsCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}