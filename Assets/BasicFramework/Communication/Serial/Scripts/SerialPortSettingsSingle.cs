﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Communication.Serial
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Communication/Serial/Serial Port Settings", 
        fileName = "_serial_port_settings_single")
    ]
    public class SerialPortSettingsSingle : SingleStructBase<SerialPortSettings> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(SerialPortSettingsSingle value)
        {
            Value = value.Value;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    [Serializable]
    public class SerialPortSettingsSingleReference : SingleStructReferenceBase<SerialPortSettings, SerialPortSettingsSingle>
    {
        public SerialPortSettingsSingleReference()
        {
        }

        public SerialPortSettingsSingleReference(SerialPortSettings value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class SerialPortSettingsSingleUnityEvent : UnityEvent<SerialPortSettingsSingle>{}
	
    [Serializable]
    public class SerialPortSettingsSingleArrayUnityEvent : UnityEvent<SerialPortSettingsSingle[]>{}
    
}