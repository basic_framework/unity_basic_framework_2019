
## Unreleased - [00-02]

### Added

- 

### Changed

- 

### Bug Fixes

-

### Removed

-


## Unreleased - [00-01] - 20YY-MM-DD

### Added

- Common: Common communication base class

- Serial: SerialPortSettings and SerialPortClient to setup and manage a SerialPort connection

- Websocket: WebsocketSettings and WebsocketClient to setup and manage a Websocket connection
