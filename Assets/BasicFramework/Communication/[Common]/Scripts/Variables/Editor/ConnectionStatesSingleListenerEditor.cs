using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Communication.Editor
{
    [CustomEditor(typeof(ConnectionStatesSingleListener))][CanEditMultipleObjects]
    public class ConnectionStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
        ConnectionStates, 
        ConnectionStatesSingle, 
        ConnectionStatesUnityEvent,
        ConnectionStatesCompareUnityEvent,
        ConnectionStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}