using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.Communication.Editor
{
    [CustomEditor(typeof(ConnectionStatesSingle))][CanEditMultipleObjects]
    public class ConnectionStatesSingleEditor : SingleStructBaseEditor<ConnectionStates, ConnectionStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(ConnectionStatesSingle))]
    public class ConnectionStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(ConnectionStatesSingleReference))]
    public class ConnectionStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}