using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Communication
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Communication/Connection States Single", 
        fileName = "_connection_states_single")
    ]
    public class ConnectionStatesSingle : SingleStructBase<ConnectionStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(ConnectionStatesSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class ConnectionStatesSingleReference : SingleStructReferenceBase<ConnectionStates, ConnectionStatesSingle>
    {
        public ConnectionStatesSingleReference()
        {
        }

        public ConnectionStatesSingleReference(ConnectionStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class ConnectionStatesSingleUnityEvent : UnityEvent<ConnectionStatesSingle>{}
	
    [Serializable]
    public class ConnectionStatesSingleArrayUnityEvent : UnityEvent<ConnectionStatesSingle[]>{}
    
}