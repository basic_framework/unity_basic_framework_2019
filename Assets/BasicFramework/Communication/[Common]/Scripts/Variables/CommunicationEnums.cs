﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.Communication
{
	public enum ConnectionStates : ushort
	{
		Disconnected,
		Connecting,
		Connected,
		Disconnecting
	}
	
	[Serializable]
	public class ConnectionStatesUnityEvent : UnityEvent<ConnectionStates>{}

	[Serializable]
	public class ConnectionStatesCompareUnityEvent : CompareValueUnityEventBase<ConnectionStates>{}
}