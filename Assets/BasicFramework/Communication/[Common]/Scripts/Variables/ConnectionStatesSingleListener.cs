using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.Communication
{
    [AddComponentMenu("Basic Framework/Communication/Connection States Single Listener")]
    public class ConnectionStatesSingleListener : SingleStructListenerBase
    <
        ConnectionStates, 
        ConnectionStatesSingle, 
        ConnectionStatesUnityEvent, 
        ConnectionStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}