﻿using System;
using System.Collections.Generic;
using System.Threading;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.Utility.Attributes;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Communication
{
	public abstract class CommunicationBase : MonoBehaviour 
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		private const float THREAD_UPDATE_RATE_HZ = 50;
		private const int THREAD_SLEEP_TIME_MS = (int) (1 / THREAD_UPDATE_RATE_HZ * 1000);

		[Tooltip("Should the connection state be logged when it changes")]
		[SerializeField] private bool _logConnectionState = true;
		
		[Tooltip("Should the communication messages be logged. E.g. state changes incoming/outgoing messages")]
		[SerializeField] private bool _debugLoggingEnabled = default;

		[Tooltip("The message that will be sent when the editor send button is pressed")]
		[SerializeField] private string _debugMessage = "Hello World";
		
		
		[EnumFlag("The setup callbacks to try and connect during")]
		[SerializeField] private SetupCallbackFlags _setupCallbacksConnect = default;
		
//		[Tooltip("Will try to connect on start")]
//		[SerializeField] private bool _connectOnStart = default;
		
		[Tooltip("If the communication disconnects without being asked to it will automatically try to reconnect")]
		[SerializeField] private bool _automaticallyReconnect = default;

		
		[Tooltip("The buffer used to store incoming data")]
		[SerializeField] private StringBuffer _inputBuffer = default;
		
		[Tooltip("The buffer used to store outgoing data")]
		[SerializeField] private StringBuffer _outputBuffer = default;

		
		[Tooltip("Raised when a new message is received")]
		[SerializeField] private StringUnityEvent _messageReceived = default;
		
		[Tooltip("Raised when the connection state changes")]
		[SerializeField] private ConnectionStatesUnityEvent _connectionStateChanged = default;
		
		[Tooltip("Custom events raised when the state is changed")]
		[SerializeField] private ConnectionStatesCompareUnityEvent[] _connectionStateChangedCompare = new ConnectionStatesCompareUnityEvent[0];


		public StringUnityEvent MessageReceived => _messageReceived;

		public ConnectionStatesUnityEvent ConnectionStateChanged => _connectionStateChanged;


		// This value will only be set on the unity thread
		private ConnectionStates _connectionState = ConnectionStates.Disconnected;
		public ConnectionStates ConnectionState
		{
			get => _connectionState;
			private set
			{
				if (_connectionState == value) return;
				_connectionState = value;
				ConnectionStateUpdated(value);
			}
		}

		// Use a queue because connection state may be changed within a thread
		private readonly Queue<ConnectionStates> _connectionStateChangedEventQueue = new Queue<ConnectionStates>();

		public bool AutomaticallyReconnect
		{
			get => _automaticallyReconnect;
			set => _automaticallyReconnect = value;
		}
		
		public abstract float ConnectingTimer{ get; }

		// Only here to stop console warning
		protected string DebugMessage => _debugMessage;
		
		
		//Thread Stuff
		
		private string _threadSafeName;
		public string ThreadSafeName => _threadSafeName;

		private bool _threadSafeEnabled;
		
		private Thread _thread;
		public bool ThreadOpen => _thread != null;
		
		private Thread _unityThread;
		
		private bool _closeConnectionFlag;
		public bool CloseConnectionFlag => _closeConnectionFlag;


		// ****************************************  METHODS  ****************************************\\
		
		//***** abstract *****\\
		
		protected abstract void ConnectionThread();


		//***** Mono *****\\
		
		protected virtual void Awake()
		{
			_threadSafeName = name;
			_threadSafeEnabled = true;
			_unityThread = Thread.CurrentThread;
			
			if ((_setupCallbacksConnect & SetupCallbackFlags.Awake) == SetupCallbackFlags.Awake)
			{
				Connect();
			}
		}
		
		protected virtual void OnEnable()
		{
			_threadSafeEnabled = true;
			
			if ((_setupCallbacksConnect & SetupCallbackFlags.Enable) == SetupCallbackFlags.Enable)
			{
				Connect();
			}
		}

		protected virtual void OnDisable()
		{
			_threadSafeEnabled = false;
			
			Disconnect();

			DequeConnectionStates();
			if (_connectionState != ConnectionStates.Disconnected)
			{
				ConnectionState = ConnectionStates.Disconnecting;
			}
			ConnectionState = ConnectionStates.Disconnected;
		}

		protected virtual void Start()
		{
			if ((_setupCallbacksConnect & SetupCallbackFlags.Start) == SetupCallbackFlags.Start)
			{
				Connect();
			}
			
			ConnectionStateUpdated(_connectionState);
		}

		protected virtual void Update()
		{
			DequeConnectionStates();
			
			var message = ReadFromInputBuffer();

			// Read all of the queued incoming messages
			while (!string.IsNullOrEmpty(message))
			{
				LogDebugMessage($"IN: {message}");

				_messageReceived.Invoke(message);
				
				// Read the next message so that we don't get stuck in this while loop
				message = ReadFromInputBuffer();
			}	
		}
		
		
		//***** Connection *****\\
		
		public void Connect()
		{
			if (!_threadSafeEnabled)
			{
				LogDebugMessage("client is disabled, cannot create connection");
				return;
			}
			
			if (_thread != null) return;
			_closeConnectionFlag = false;
			
			ClearBuffers();

			_thread = new Thread(ConnectionThreadBase);
			
			_thread.Start();
		}

		public void Disconnect()
		{
			if (_thread == null) return;
			_closeConnectionFlag = true;
		}
		
		private void ConnectionThreadBase()
		{
			LogDebugMessage("***** Thread Started *****");
			
			ConnectionThread();
			
			lock (_thread)
			{
				_thread = null;
			}
			
			LogDebugMessage("***** Thread Stopped *****");
		}
		
		/// <summary>Sleeps the connection thread and returns the amount of time it was slept</summary>
		/// <returns>The amount of time (in seconds) the thread slept</returns>
		protected float SleepConnectionThread()
		{
			if (Thread.CurrentThread == _unityThread) return 0;
			var prevTime = DateTime.Now;
			Thread.Sleep(THREAD_SLEEP_TIME_MS);
			return (float) (DateTime.Now - prevTime).TotalSeconds;
		}
		
		public void SendMessageOverConnection(string message)
		{
			WriteToOutputBuffer(message);
		}

		
		//***** Connection state *****\\

		protected void EnqueueConnectionState(ConnectionStates connectionState)
		{
			if (_logConnectionState)
			{
				Debug.LogWarning($"{_threadSafeName}/{GetType().Name.CamelCaseToHumanReadable()} => " +
				                 $"{connectionState}");
			}

			if (!_threadSafeEnabled) return;
			
			lock (_connectionStateChangedEventQueue)
			{
				_connectionStateChangedEventQueue.Enqueue(connectionState);
			}
		}

		private void DequeConnectionStates()
		{
			lock (_connectionStateChangedEventQueue)
			{
				while (_connectionStateChangedEventQueue.Count > 0)
				{
					ConnectionState = _connectionStateChangedEventQueue.Dequeue();
				}
			}
		}
		
		private void ConnectionStateUpdated(ConnectionStates connectionState)
		{
			_connectionStateChanged.Invoke(connectionState);

			foreach (var stateChangedEvent in _connectionStateChangedCompare)
			{
				stateChangedEvent.SetCurrentValue(connectionState);
			}
		}

		
		//***** Utility *****\\
		
		protected void LogDebugMessage(string message)
		{
			if (!_debugLoggingEnabled) return;
			Debug.LogWarning($"{_threadSafeName}/{GetType().Name.CamelCaseToHumanReadable()} => {message}");
		}

		
		//***** Buffer Management ******\\
		
		protected void ClearBuffers()
		{
			lock (_inputBuffer)
			{
				_inputBuffer.ClearBuffer();
			}

			lock (_outputBuffer)
			{
				_outputBuffer.ClearBuffer();
			}
		}

		protected void WriteToInputBuffer(string message)
		{
			if (string.IsNullOrEmpty(message)) return;
			
			lock (_inputBuffer)
			{
				_inputBuffer.WriteToBuffer(message, _debugLoggingEnabled);
			}
		}

		private string ReadFromInputBuffer()
		{
			lock (_inputBuffer)
			{
				return !_inputBuffer.Available ? null : _inputBuffer.ReadOldest();
			}
		}
		
		// This is essentially the send function
		private void WriteToOutputBuffer(string message)
		{
			if (string.IsNullOrEmpty(message)) return;
			if (ConnectionState != ConnectionStates.Connected)
			{
				LogDebugMessage("not connected, could not add message to output buffer");
				return;
			}

			lock (_outputBuffer)
			{
				_outputBuffer.WriteToBuffer(message, _debugLoggingEnabled);
			}
		}
		
		// This should be accessed by the child class when sending messages
		protected string ReadFromOutputBuffer()
		{
			string message = null;
			
			lock (_outputBuffer)
			{
				message = _outputBuffer.Available ? _outputBuffer.ReadOldest() : null;
			}

			if (_debugLoggingEnabled && !string.IsNullOrEmpty(message))
			{
				LogDebugMessage($"OUT: {message}");
			}
			
			return message;
		}
		
	}
}