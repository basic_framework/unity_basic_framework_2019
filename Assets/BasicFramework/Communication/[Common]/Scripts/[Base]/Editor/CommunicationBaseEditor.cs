﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.Communication.Editor
{
	public abstract class CommunicationBaseEditor<T> : BasicBaseEditor<T>
		where T : CommunicationBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private ReorderableList _connectionStateChangedReorderableList;

		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_debugLoggingEnabled").isExpanded;
			set => serializedObject.FindProperty("_debugLoggingEnabled").isExpanded = value;
		}
		

		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();
			
			var connectionStateChangedEvents = serializedObject.FindProperty("_connectionStateChangedCompare");

			_connectionStateChangedReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, connectionStateChangedEvents, true);
		}

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();
			
			EditorGUILayout.HelpBox("Connection is automatically closed OnDisable", MessageType.Info);
		}

		protected sealed override void DrawDebugFields()
		{
			var logConnectionState = serializedObject.FindProperty("_logConnectionState");
			var debugLoggingEnabled = serializedObject.FindProperty("_debugLoggingEnabled");
			var debugMessage = serializedObject.FindProperty("_debugMessage");

			EditorGUILayout.PropertyField(logConnectionState);
			EditorGUILayout.PropertyField(debugLoggingEnabled);
			
			// Send
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.PropertyField(debugMessage);
			
			GUI.enabled = PrevGuiEnabled
			              && Application.isPlaying
			              && TypedTarget.ConnectionState == ConnectionStates.Connected;
			
			if (GUILayout.Button("Send", GUILayout.Width(60)))
			{
				TypedTarget.SendMessageOverConnection(debugMessage.stringValue);
			}
			
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();
			
			
			GUI.enabled = false;
			EditorGUILayout.LabelField("Connection State", TypedTarget.ConnectionState.ToString());
			EditorGUILayout.LabelField("Connecting Timer", TypedTarget.ConnectingTimer.ToString());
			
			Space();
			SectionHeader("Thread");
			EditorGUILayout.LabelField("Thread Open", TypedTarget.ThreadOpen.ToString());
			EditorGUILayout.LabelField("Close Connection Thread", TypedTarget.CloseConnectionFlag.ToString());
			GUI.enabled = PrevGuiEnabled;
			
			
			DrawChildDebugFields();
			
			// Connect
			EditorGUILayout.BeginHorizontal();
			
			GUI.enabled = PrevGuiEnabled 
			              && Application.isPlaying
			              && TypedTarget.ConnectionState != ConnectionStates.Connecting
			              && TypedTarget.ConnectionState != ConnectionStates.Connected;
			
			if (GUILayout.Button("Connect"))
			{
				TypedTarget.Connect();
			}
			
			GUI.enabled = PrevGuiEnabled 
			              && Application.isPlaying 
			              && TypedTarget.ConnectionState != ConnectionStates.Disconnecting 
			              && TypedTarget.ConnectionState != ConnectionStates.Disconnected;
			
			if (GUILayout.Button("Disconnect"))
			{
				TypedTarget.Disconnect();
			}

			GUI.enabled = PrevGuiEnabled;

			EditorGUILayout.EndHorizontal();
			
			Space();
		}
		
		protected virtual void DrawChildDebugFields(){}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			DrawCommunicationBaseFields();
		}
		
		protected void DrawCommunicationBaseFields()
		{
			SectionHeader("Communication Base Settings");

			var setupCallbacksConnect = serializedObject.FindProperty("_setupCallbacksConnect");
			var automaticallyReconnect = serializedObject.FindProperty("_automaticallyReconnect");
			
			EditorGUILayout.PropertyField(setupCallbacksConnect);
			EditorGUILayout.PropertyField(automaticallyReconnect);

			
			//***** Buffers *****\\
			var inputBuffer = serializedObject.FindProperty("_inputBuffer");
			EditorGUILayout.PropertyField(inputBuffer);
			
			var outputBuffer = serializedObject.FindProperty("_outputBuffer");
			EditorGUILayout.PropertyField(outputBuffer);
		}
		
		protected override void DrawEventFields()
		{
			SectionHeader("Events");
			
			var messageReceived = serializedObject.FindProperty("_messageReceived");
			var connectionStateChanged = serializedObject.FindProperty("_connectionStateChanged");

			EditorGUILayout.PropertyField(messageReceived);
			EditorGUILayout.PropertyField(connectionStateChanged);
			_connectionStateChangedReorderableList.DoLayoutList();
		}
		
	}
}