﻿using UnityEditor;
using UnityEngine;

namespace BasicFramework.Communication.Editor
{
	[CustomEditor(typeof(CommunicationSimulator))]
	public class CommunicationSimulatorEditor : CommunicationBaseEditor<CommunicationSimulator> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawChildDebugFields()
		{
			base.DrawChildDebugFields();

			var debugSimulatorLoggingEnabled = serializedObject.FindProperty("_debugSimulatorLoggingEnabled");
			var debugSimulatorMessage = serializedObject.FindProperty("_debugSimulatorMessage");
			
			Space();
			SectionHeader("Simulator");
			
			EditorGUILayout.PropertyField(debugSimulatorLoggingEnabled);
			
			GUI.enabled = false;
			EditorGUILayout.LabelField("Simulator Connection State", TypedTarget.SimulatorConnectionState.ToString());
			GUI.enabled = PrevGuiEnabled;
			
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.PropertyField(debugSimulatorMessage);
			
			GUI.enabled = PrevGuiEnabled
			              && Application.isPlaying
			              && TypedTarget.ConnectionState == ConnectionStates.Connected;
			
			if (GUILayout.Button("Send", GUILayout.Width(60)))
			{
				TypedTarget.SimulatorSendMessage(debugSimulatorMessage.stringValue);
			}
			
			GUI.enabled = PrevGuiEnabled;
			
			EditorGUILayout.EndHorizontal();
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var simulatedConnectDelay = serializedObject.FindProperty("_simulatedConnectDelay");
			var simulatedDiscconnectDelay = serializedObject.FindProperty("_simulatedDiscconnectDelay");
			
			Space();
			SectionHeader("Simulated Communication Properties");

			EditorGUILayout.PropertyField(simulatedConnectDelay);
			EditorGUILayout.PropertyField(simulatedDiscconnectDelay);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var simulatorMessageReceived = serializedObject.FindProperty("_simulatorMessageReceived");
			
			Space();
			SectionHeader("Simulator Events");
			EditorGUILayout.PropertyField(simulatorMessageReceived);
		}
		
	}
}