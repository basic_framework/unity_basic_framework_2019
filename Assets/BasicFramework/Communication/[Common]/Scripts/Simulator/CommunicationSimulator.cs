﻿using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;

namespace BasicFramework.Communication
{
	[AddComponentMenu("Basic Framework/Communication/Communication Simulator")]
	public class CommunicationSimulator : CommunicationBase 
	{

		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private float _simulatedConnectDelay = default;
		[SerializeField] private float _simulatedDiscconnectDelay = default;

		[Tooltip("Should the communication messages be logged. E.g. state changes incoming/outgoing messages")]
		[SerializeField] private bool _debugSimulatorLoggingEnabled = default;
		
		[Tooltip("The debug message that will be simulated as received when button is pressed")]
		[SerializeField] private string _debugSimulatorMessage = "Hello World";

		[Tooltip("The message received by the communication simulator, should not be tied to non simulated logic")]
		[SerializeField] private StringUnityEvent _simulatorMessageReceived = default;
		
		
		// Only here to stop console warning
		public bool DebugSimulatorLoggingEnabled
		{
			get => _debugSimulatorLoggingEnabled;
			set => _debugSimulatorLoggingEnabled = value;
		}

		protected string DebugSimulatorMessage => _debugSimulatorMessage;

		public StringUnityEvent SimulatorMessageReceived => _simulatorMessageReceived;
		

		private ConnectionStates _simulatorConnectionState = ConnectionStates.Disconnected;
		public ConnectionStates SimulatorConnectionState
		{
			get => _simulatorConnectionState;
			private set
			{
				if (_simulatorConnectionState == value) return;
				_simulatorConnectionState = value;
				SimulatedConnectionStateUpdated(_simulatorConnectionState);
			}
		}

		private float _connectingTimer;
		public override float ConnectingTimer => _connectingTimer;
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		protected override void Update()
		{
			base.Update();

			if (_simulatorConnectionState != ConnectionStates.Connected) return;
			
			var message = ReadFromOutputBuffer();
			
			while (!string.IsNullOrEmpty(message))
			{
				LogDebugSimulatorMessage($"Simulator Receiving: {message}");
				_simulatorMessageReceived.Invoke(message);
				message = ReadFromOutputBuffer();
			}

		}

		
		//***** property updated *****\\

		private void SimulatedConnectionStateUpdated(ConnectionStates localConnectionState)
		{
			EnqueueConnectionState(localConnectionState);
		}

		
		//***** simulator *****\\
		
		/// <summary>
		/// Should be used by the logic simulating the other end of the connection.
		/// This should not be called by non simulator related logic
		/// </summary>
		/// <param name="message"></param>
		public void SimulatorSendMessage(string message)
		{
			if (_simulatorConnectionState != ConnectionStates.Connected) return;
			LogDebugSimulatorMessage($"Simulator Sending: {message}");
			WriteToInputBuffer(message);
		}
		
		private void LogDebugSimulatorMessage(string message)
		{
			if (!_debugSimulatorLoggingEnabled) return;
			Debug.LogWarning($"{ThreadSafeName}/{GetType().Name.CamelCaseToHumanReadable()} => {message}");
		}

		
		//***** thread *****\\
		
		protected override void ConnectionThread()
		{
			
			while (!CloseConnectionFlag)
			{
				
				//***** Connecting *****\\
				SimulatorConnectionState = ConnectionStates.Connecting;

				_connectingTimer = 0;
				
				while (!CloseConnectionFlag && _connectingTimer < _simulatedConnectDelay)
				{
					_connectingTimer += SleepConnectionThread();
				}

				if (CloseConnectionFlag) break;
				
				SimulatorConnectionState = ConnectionStates.Connected;

				//***** Connected *****\\
				while (!CloseConnectionFlag && SimulatorConnectionState == ConnectionStates.Connected)
				{
					SleepConnectionThread();
				}

				if(CloseConnectionFlag || !AutomaticallyReconnect) break;
			}
			
			
			//***** Disconnect *****\\
			
			if (SimulatorConnectionState != ConnectionStates.Disconnected)
			{
				SimulatorConnectionState = ConnectionStates.Disconnecting;
				
				var disconnectingTimer = 0f;

				while (disconnectingTimer < _simulatedDiscconnectDelay)
				{
					disconnectingTimer += SleepConnectionThread();
				}
			}
			
			SimulatorConnectionState = ConnectionStates.Disconnected;
			
		}
		
	}
}