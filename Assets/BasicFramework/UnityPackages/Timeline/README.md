#Timeline (Basic Framework Integration)

__Note: Only add this package if you have the _Timeline_ package installed.
You can install it from the PackageManager window. 
If you do not have it installed you will get errors.
Delete the root folder to remove the errors.__

This plugin integrates Basic Frameworks Core systems with the Timeline package.


##Custom Playable Creation 

To use the package inherit from either the Clip Set or Marker Set of scripts depending if you want to create timeline clips or markers.

Note: You will get errors until all scripts in the set have been created

In the following templates replace __TType__ with the set name. 
Replace __TBinding__ with the binding type (must inherit from UnityEngine.Object).
If you don't want a binding remove it. 

###Clip Set

1. TTypeClip     : BasicClipBase<TTypePlayable>
2. TTypePlayable : BasicPlayableBase<TTypeClip>
3. TTypeMixer    : BasicMixerBase<TTypeClip, TTypePlayable, TTypeTrack, TBinding>
4. TTypeTrack    : BasicTrackBase<TTypeClip, TTypePlayable, TTypeMixer>


#### 1 - Clip
Creates clips on the track with a variable of type __TTypePlayable__

```csharp
using BasicFramework.UnityPackages.Timeline;

public class TTypeClip : BasicClipBase<TTypePlayable> 
{
	
	// **************************************** VARIABLES ****************************************\\
	
	
	// ****************************************  METHODS  ****************************************\\
	
	
}
```

#### 2 - Playable
Tells unity what to do when the playhead is on the clip.
When using a Mixer it is reduced to a container for the data related to the clip. 

```csharp
using System;
using BasicFramework.UnityPackages.Timeline;

[Serializable]
public class TTypePlayable : BasicPlayableBase<TTypeClip>
{
	
	// **************************************** VARIABLES ****************************************\\
	
	// TODO: Add properties here
	
	// ****************************************  METHODS  ****************************************\\
	
	
}
```



#### 3 - Mixer
Handles behaviour of multiple clips on the track.
Runs through all the clips and calculates the output 
given the weight of each clip (determined by the playhead position).
It then outputs the data using the track binding (if required).

Note: If you don't need binding you can remove the TBinding parameter. 
You will have to replace the binding type in OnProcessFrame to Object.

```csharp
using System;
using BasicFramework.UnityPackages.Timeline;
using UnityEngine.Playables;

[Serializable]
public class TTypeMixer : BasicMixerBase<TTypeClip, TTypePlayable, TTypeTrack, TBinding> 
{
	
	// **************************************** VARIABLES ****************************************\\
	
	
	// ****************************************  METHODS  ****************************************\\
	
	protected override void OnProcessFrame(Playable playable, FrameData info, 
    		MixerData<TTypeClip, TTypePlayable, TTypeTrack, TBinding> mixerData)
    {
   		switch (mixerData.MixerClipState)
   		{
   			case MixerClipStates.FullTrack:
   				break;
   			case MixerClipStates.TrackToClip:
   				break;
   			case MixerClipStates.ClipToTrack:
   				break;
   			case MixerClipStates.ClipToClip:
   				break;
   			case MixerClipStates.FullClip:
   				break;
    		default:
    			throw new ArgumentOutOfRangeException();
    	}
    		
    	//assign the result to the bound object
    	throw new NotImplementedException();
    }
	
}
```

#### 4 - Track
Controls creating a track. Creates the Mixer for the track.
Binds timeline properties to clips

Note: If you don't need binding you can remove the TrackBindingType directive

```csharp
using BasicFramework.UnityPackages.Timeline;
using UnityEngine.Timeline;

[TrackClipType(typeof(TTypeClip))]
[TrackBindingType(typeof(TBinding))]
public class TTypeTrack : BasicTrackBase<TTypeClip, TTypePlayable, TTypeMixer> 
{
	
	// **************************************** VARIABLES ****************************************\\
	
	// TODO: Add properties here
	
	// ****************************************  METHODS  ****************************************\\

	
}
```


###Marker Set

1. TTypeMarker        : BasicMarkerBase
2. TTypeReceiver      : BasicReceiverBase<TTypeMarker>
3. TTypeMarkerTrack   : BasicMarkerTrackBase

#### 1 - Marker

```csharp
using BasicFramework.UnityPackages.Timeline;

public class TTypeMarker : BasicMarkerBase
{
	
	// **************************************** VARIABLES ****************************************\\
	
	
	// ****************************************  METHODS  ****************************************\\
		
}
```



#### 2 - Receiver

```csharp
using System;
using BasicFramework.UnityPackages.Timeline;
using UnityEngine.Playables;

public class TTypeReceiver : BasicReceiverBase<TTypeMarker>
{
	
	// **************************************** VARIABLES ****************************************\\
	
	
	// ****************************************  METHODS  ****************************************\\

	protected override void OnNotify(Playable origin, TTypeMarker marker, object context)
	{
		throw new NotImplementedException();
	}
		
}
```

#### 2 - MarkerTrack


```csharp
using BasicFramework.UnityPackages.Timeline;
using UnityEngine.Timeline;

[TrackBindingType(typeof(TTypeReceiver))]
public class TTypeMarkerTrack : BasicMarkerTrackBase 
{
	
	// **************************************** VARIABLES ****************************************\\
	
	
	// ****************************************  METHODS  ****************************************\\
	
			
}
```

