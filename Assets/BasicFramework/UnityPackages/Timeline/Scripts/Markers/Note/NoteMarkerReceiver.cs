﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline
{
    [AddComponentMenu("Basic Framework/Unity Packages/Timeline/Markers/Note Marker Receiver")]
    public class NoteMarkerReceiver : BasicReceiverBase<NoteMarker>
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private bool _dontLog = default;

        // ****************************************  METHODS  ****************************************\\
		
        protected override void OnNotify(Playable origin, NoteMarker marker, object context)
        {
            if (_dontLog) return;
            if (!marker.LogNote) return;
            Debug.Log($"{name} / {nameof(NoteMarkerReceiver)} => {marker.Note}");
        }
		
    }
}