﻿using System.ComponentModel;
using UnityEngine;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[DisplayName("Basic Markers/Note Marker")]
	[CustomStyle("NoteMarker")]
	public class NoteMarker : BasicMarkerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Should the note be logged to the console when the marker is triggered")]
		[SerializeField] private bool _logNote = true;
		
		[TextArea]
		[SerializeField] private string _note = default;


		public bool LogNote => _logNote;

		public string Note => _note;
		

		// ****************************************  METHODS  ****************************************\\


	}
}