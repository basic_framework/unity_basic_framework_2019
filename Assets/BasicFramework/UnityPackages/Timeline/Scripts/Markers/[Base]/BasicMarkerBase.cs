﻿using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	public abstract class BasicMarkerBase : Marker, INotification, INotificationOptionProvider 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[EnumFlag("Options for how the notification is emitted", true)]
		[SerializeField] private NotificationFlags _notificationFlags = NotificationFlags.TriggerInEditMode;
		
		
		// ****************************************  METHODS  ****************************************\\

		public NotificationFlags flags => _notificationFlags;

		public virtual PropertyName id { get; }
		
	}
}