﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	public abstract class BasicReceiverBase<TMarker> : MonoBehaviour, INotificationReceiver 
		where TMarker : Marker
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected abstract void OnNotify(Playable origin, TMarker marker, object context);

		public void OnNotify(Playable origin, INotification notification, object context)
		{
			var marker = notification as TMarker;
			if (marker == null) return;
			OnNotify(origin, marker, context);
		}
		
	}
}