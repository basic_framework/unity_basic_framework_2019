﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
    [DisplayName("Basic Markers/Target Marker")]
    [CustomStyle("TargetMarker")]
    public class TargetMarker : Marker 
    {
	
        // **************************************** VARIABLES ****************************************\\

        public string MarkerName => name;
		
        private readonly List<TargetMarker> _targetMarkers = new List<TargetMarker>();
		

        // ****************************************  METHODS  ****************************************\\

        public List<TargetMarker> GetTargetMarkersInTimeline()
        {
            var timelineAsset = TimelineMarkerUtility.GetTimelineAsset(this);

            var tracks = timelineAsset.GetOutputTracks();

            _targetMarkers.Clear();

            foreach (var track in tracks)
            {
                foreach (var marker in track.GetMarkers())
                {
                    var targetMarker = marker as TargetMarker;
                    if(targetMarker == null) continue;
                    _targetMarkers.Add(targetMarker);
                }
            }
			
            return _targetMarkers;
        }

    }
}