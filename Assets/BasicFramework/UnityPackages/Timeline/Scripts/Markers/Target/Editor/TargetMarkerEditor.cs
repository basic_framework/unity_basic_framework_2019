﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomEditor(typeof(TargetMarker))]
	public class TargetMarkerEditor : BasicBaseEditor<TargetMarker>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			
			
			var targetMarkers = TypedTarget.GetTargetMarkersInTimeline();
			var markerName = TypedTarget.name;

			var markerUnique = true;
			
			foreach (var targetMarker in targetMarkers)
			{
				if(targetMarker == TypedTarget) continue;
				if(targetMarker.name != markerName) continue;
				markerUnique = false;
				break;
			}
			
			EditorGUILayout.HelpBox(
				$"The marker name IS {(markerUnique ? "" : "NOT ")}unique in this timeline asset", 
				markerUnique ? MessageType.Info : MessageType.Warning);
			
			
			var nameProp = serializedObject.FindProperty("m_Name");
			var timeProp = serializedObject.FindProperty("m_Time");
			
			EditorGUILayout.PropertyField(nameProp);
			EditorGUILayout.PropertyField(timeProp);

		}
		
		
	}
}