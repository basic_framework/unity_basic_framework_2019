﻿using UnityEngine;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	public static class TimelineMarkerUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
		public static TimelineAsset GetTimelineAsset(IMarker marker)
		{
			// Dodgy
			var playableAsset = marker.parent.parent;
			var trackAsset = playableAsset as TrackAsset;
			var timelineAsset = playableAsset as TimelineAsset;
			
			while (timelineAsset == null)
			{
				playableAsset = trackAsset.parent;
				trackAsset = playableAsset as TrackAsset;
				timelineAsset = playableAsset as TimelineAsset;
			}

			return timelineAsset;
		}
	
	}
}