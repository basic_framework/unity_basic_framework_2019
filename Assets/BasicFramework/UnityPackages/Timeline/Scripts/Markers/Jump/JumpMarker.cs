﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{

	[Serializable]
	public class JumpMarkerConditionCouple
	{
		
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private bool _enabled = true;
		
		[SerializeField] private TargetMarker _targetMarker = default;

		[SerializeField] private ScriptableSingleCondition _jumpCondition = default;


		public bool Enabled
		{
			get => _enabled;
			set => _enabled = value;
		}

		public TargetMarker TargetMarker => _targetMarker;
		

		// ****************************************  METHODS  ****************************************\\

		public bool CheckCondition()
		{
			return _enabled && _jumpCondition.CheckCondition();
		}
		
	}
	
	[DisplayName("Basic Markers/Jump Marker")]
	[CustomStyle("JumpMarker")]
	public class JumpMarker : BasicMarkerBase
	{
		
		// **************************************** VARIABLES ****************************************\\

		[TextArea] 
		[SerializeField] private string _developerNote = default;
		
		[Tooltip("The marker to jump to if no conditions are met")]
		[SerializeField] private TargetMarker _targetMarkerDefault = default;
		
		[SerializeField] private JumpMarkerConditionCouple[] _jumpConditions = default;
		
		
		public string DeveloperNote => _developerNote;
		
		public TargetMarker TargetMarkerDefault => _targetMarkerDefault;

		public JumpMarkerConditionCouple[] JumpConditions => _jumpConditions;
		

		private readonly List<TargetMarker> _targetMarkers = new List<TargetMarker>();


		// ****************************************  METHODS  ****************************************\\

		public List<TargetMarker> GetTargetMarkersInTimeline()
		{
			var timelineAsset = TimelineMarkerUtility.GetTimelineAsset(this);

			var tracks = timelineAsset.GetOutputTracks();

			_targetMarkers.Clear();

			foreach (var track in tracks)
			{
				foreach (var marker in track.GetMarkers())
				{
					var targetMarker = marker as TargetMarker;
					if(targetMarker == null) continue;
					_targetMarkers.Add(targetMarker);
				}
			}
			
			return _targetMarkers;
		}

	}
}