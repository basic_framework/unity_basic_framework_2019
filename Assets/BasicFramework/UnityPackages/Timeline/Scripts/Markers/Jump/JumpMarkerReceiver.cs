﻿using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline
{
    [AddComponentMenu("Basic Framework/Unity Packages/Timeline/Markers/Jump Marker Receiver")]
    public class JumpMarkerReceiver : BasicReceiverBase<JumpMarker>
    {
	
        // **************************************** VARIABLES ****************************************\\
		
		

        // ****************************************  METHODS  ****************************************\\

        protected override void OnNotify(Playable origin, JumpMarker marker, object context)
        {
            var targetMarker = marker.TargetMarkerDefault;
			
            for (var i = 0; i < marker.JumpConditions.Length; i++)
            {
                var condition = marker.JumpConditions[i];
                if (!condition.CheckCondition()) continue;

                targetMarker = condition.TargetMarker;
                if (condition.TargetMarker != null)	break;

                Debug.LogError($"{name}/{GetType().Name.CamelCaseToHumanReadable()} => " +
                               $"condition [{i}] met but cannot jump because {nameof(condition.TargetMarker)} is null");
                return;
            }
			
            if (targetMarker == null) return;
			
            var timelinePlayable = origin.GetGraph().GetRootPlayable(0);
            timelinePlayable.SetTime(targetMarker.time);
        }

    }
}