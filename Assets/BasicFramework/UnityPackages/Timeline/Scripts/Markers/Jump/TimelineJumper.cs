﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[AddComponentMenu("Basic Framework/Unity Packages/Timeline/Markers/Timeline Jumper")]
	public class TimelineJumper : MonoBehaviour 
	{
		
		// TODO: Hook into PlayableDirector and find all the TargetMarkers
		// TODO: Have a function to input the string identifier for the marker to jump to and jump to the marker
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private PlayableDirector _playableDirector = default;
		
		
		public PlayableDirector PlayableDirector => _playableDirector;

		private readonly List<TargetMarker> _targetMarkers = new List<TargetMarker>();
		public List<TargetMarker> TargetMarkers => _targetMarkers;
		

		// ****************************************  METHODS  ****************************************\\
		
		private void Reset()
		{
			_playableDirector = GetComponent<PlayableDirector>();
		}

		private void Awake()
		{
			UpdateTargetMarkerList();
		}
		
		
		private void UpdateTargetMarkerList()
		{
			var timelineAsset = (TimelineAsset) _playableDirector.playableAsset;
			var timelineTracks = timelineAsset.GetOutputTracks();
			
			_targetMarkers.Clear();
			
			foreach (var track in timelineTracks)
			{
				foreach (var marker in track.GetMarkers())
				{
					var targetMarker = marker as TargetMarker;
					if(targetMarker == null) continue;
					_targetMarkers.Add(targetMarker);
				}
			}
		}
		
		
		//***** Playable Director *****\\

		public void Play()
		{
			_playableDirector.Play();
		}

		public void Pause()
		{
			_playableDirector.Pause();
		}

		public void Stop()
		{
			_playableDirector.Stop();
		}
		
		public void JumpToTargetMarkerNoReturn(string markerName)
		{
			JumpToTargetMarker(markerName);
		}
		
		public bool JumpToTargetMarker(string markerName)
		{
			foreach (var targetMarker in _targetMarkers)
			{
				if(targetMarker.MarkerName != markerName) continue;
				_playableDirector.time = targetMarker.time;
				return true;
			}
			
			return false;
		}
		
		
		//***** Editor *****\\

		public void EditorUpdateProperties()
		{
			if (!Application.isEditor) return;
			if (Application.isPlaying) return;
			if (_playableDirector == null) return;
			
			UpdateTargetMarkerList();
		}
	}
}