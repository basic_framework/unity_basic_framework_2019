﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomEditor(typeof(TimelineJumper))]
	public class TimelineJumperEditor : BasicBaseEditor<TimelineJumper>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_playableDirector").isExpanded;
			set => serializedObject.FindProperty("_playableDirector").isExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			GUI.enabled = false;
			
			Space();
			SectionHeader("Available Markers");

			var targetMarkers = TypedTarget.TargetMarkers;

			foreach (var targetMarker in targetMarkers)
			{
				EditorGUILayout.BeginHorizontal();
				
				EditorGUILayout.TextField("Marker Name", targetMarker.MarkerName);
				
				GUI.enabled = PrevGuiEnabled;
				
				if (GUILayout.Button("Copy", GUILayout.Width(60)))
				{
					GUIUtility.systemCopyBuffer = targetMarker.MarkerName;
				}

				GUI.enabled = false;

				EditorGUILayout.EndHorizontal();
			}
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			//base.DrawPropertyFields();

			TypedTarget.EditorUpdateProperties();
			
			SectionHeader("Timeline Jumper Properties");
			var playableDirector = serializedObject.FindProperty("_playableDirector");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(playableDirector);
		}
	}
}