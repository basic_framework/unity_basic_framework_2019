﻿using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomEditor(typeof(JumpMarker))]
	public class JumpMarkerEditor : BasicBaseEditor<JumpMarker>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float BUFFER_HEIGHT = BasicBasePropertyDrawer.BUFFER_HEIGHT;
		private const float BUFFER_HEIGHT_END = 6f;
		
		private const float TARGET_TIME_WIDTH = 60f;
		
		private const string NO_TARGET_STRING = "None";
		
		private string[] _targetMarkerNames = new string[0];

		

		private ReorderableList _reorderableListJumpConditions;
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var jumpConditions = serializedObject.FindProperty("_jumpConditions");

			_reorderableListJumpConditions =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, jumpConditions);

			_reorderableListJumpConditions.drawElementCallback = OnDrawElementJumpCondition;

			_reorderableListJumpConditions.elementHeightCallback += OnElementHeightJumpCondition;
		}
		
		protected override void DrawPropertyFields()
		{
			var nameProp = serializedObject.FindProperty("m_Name");
			var timeProp = serializedObject.FindProperty("m_Time");
			var notificationFlags = serializedObject.FindProperty("_notificationFlags");
			var targetMarkerDefault = serializedObject.FindProperty("_targetMarkerDefault");
			
			var developerNote = serializedObject.FindProperty("_developerNote");
			
			EditorGUILayout.PropertyField(developerNote);
			
			Space();
			SectionHeader($"{nameof(JumpMarker).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(nameProp);
			EditorGUILayout.PropertyField(timeProp);
			EditorGUILayout.PropertyField(notificationFlags);
			
			Space();
			
			DrawTargetMarkerField(targetMarkerDefault, Rect.zero, true);
			
			_reorderableListJumpConditions.DoLayoutList();
		}
		
		
		private void OnDrawElementJumpCondition(Rect rect, int index, bool isactive, bool isfocused)
		{
			var jumpConditions = serializedObject.FindProperty("_jumpConditions");
			var element = jumpConditions.GetArrayElementAtIndex(index);

			var enabled = element.FindPropertyRelative("_enabled");
			var targetMarker = element.FindPropertyRelative("_targetMarker");
			var jumpCondition = element.FindPropertyRelative("_jumpCondition");

			var tempRect = rect;
			tempRect.y += BUFFER_HEIGHT;
			
			
			tempRect.height = EditorGUI.GetPropertyHeight(enabled, true);
			EditorGUI.PropertyField(tempRect, enabled, true);
			tempRect.y += BUFFER_HEIGHT + tempRect.height;


			GUI.enabled = PrevGuiEnabled && enabled.boolValue;
			
			tempRect.height = EditorGUI.GetPropertyHeight(targetMarker, true);
			DrawTargetMarkerField(targetMarker, tempRect);
			//EditorGUI.PropertyField(tempRect, targetMarker, true);
			tempRect.y += BUFFER_HEIGHT + tempRect.height;
			
			tempRect.height = EditorGUI.GetPropertyHeight(jumpCondition, true);
			EditorGUI.PropertyField(tempRect, jumpCondition, true);
			tempRect.y += BUFFER_HEIGHT + tempRect.height;

			tempRect.y += BUFFER_HEIGHT_END;
			
			GUI.enabled = PrevGuiEnabled;
		}
		
		private float OnElementHeightJumpCondition(int index)
		{
			var jumpConditions = serializedObject.FindProperty("_jumpConditions");
			var element = jumpConditions.GetArrayElementAtIndex(index);
			
			var enabled = element.FindPropertyRelative("_enabled");
			var targetMarker = element.FindPropertyRelative("_targetMarker");
			var jumpCondition = element.FindPropertyRelative("_jumpCondition");

			var height = BUFFER_HEIGHT_END;
			height += BUFFER_HEIGHT + EditorGUI.GetPropertyHeight(enabled, true);
			height += BUFFER_HEIGHT + EditorGUI.GetPropertyHeight(targetMarker, true);
			height += BUFFER_HEIGHT + EditorGUI.GetPropertyHeight(jumpCondition, true);
			
			return height;
		}

		private void DrawTargetMarkerField(SerializedProperty targetMarkerProperty, Rect rect, bool useEditorGuiLayout = false)
		{
			var targetMarkers = TypedTarget.GetTargetMarkersInTimeline();
			var count = targetMarkers.Count;

			if (_targetMarkerNames.Length != count + 1)
			{
				_targetMarkerNames = new string[count + 1];
				_targetMarkerNames[0] = NO_TARGET_STRING;
			}
			
			for (int i = 0; i < count; i++)
			{
				_targetMarkerNames[i + 1] = targetMarkers[i].name;
			} 
			
			var targetMarker = targetMarkerProperty.objectReferenceValue == null
				? null
				: targetMarkerProperty.objectReferenceValue as TargetMarker;
			
			var selected = targetMarker == null ? NO_TARGET_STRING : targetMarker.name;

			if (useEditorGuiLayout)
			{
				EditorGUILayout.BeginHorizontal();
				selected = BasicEditorGuiLayout.StringPopupField(
					GetGuiContent(targetMarkerProperty), 
					selected, 
					_targetMarkerNames);
			}
			else
			{
				rect.width -= TARGET_TIME_WIDTH - BUFFER_HEIGHT;
				selected = BasicEditorGui.StringPopupField(
					rect, 
					GetGuiContent(targetMarkerProperty), 
					selected,
					_targetMarkerNames);
				rect.x += rect.width + BUFFER_HEIGHT;
				rect.width = TARGET_TIME_WIDTH;
			}
			
			for (int i = 0; i < _targetMarkerNames.Length; i++)
			{
				if(_targetMarkerNames[i] != selected) continue;
				targetMarker = i == 0 ? null : targetMarkers[i - 1];
				break;
			}

			targetMarkerProperty.objectReferenceValue = targetMarker;

			var targetMarkerTime = targetMarker == null ? 0 : MathUtility.RoundToDecimalPlace((float) targetMarker.time, 3);

			var guiEnabled = GUI.enabled;
			GUI.enabled = false;
			
			if (useEditorGuiLayout)
			{
				EditorGUILayout.LabelField(targetMarkerTime.ToString(), GUILayout.Width(TARGET_TIME_WIDTH));
				EditorGUILayout.EndHorizontal();
			}
			else
			{
				EditorGUI.LabelField(rect,targetMarkerTime.ToString());
			}
			
			GUI.enabled = guiEnabled;
		}
		
	}
}