﻿using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[TrackBindingType(typeof(JumpMarkerReceiver))]
	[TrackColor(0, 0, 1)]
	public class JumpMarkerTrack : BasicMarkerTrackBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}