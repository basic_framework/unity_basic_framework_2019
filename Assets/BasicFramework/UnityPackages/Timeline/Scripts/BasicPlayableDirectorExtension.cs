﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline
{
	[ExecuteAlways]
	[RequireComponent(typeof(PlayableDirector))]
	[AddComponentMenu("Basic Framework/Unity Packages/Timeline/Playable Director Extension")]
	public class BasicPlayableDirectorExtension : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[HideInInspector]
		[SerializeField] private PlayableDirector _playableDirector = default;

		[SerializeField] private bool _play;
		[SerializeField] private bool _pause;
		[SerializeField] private bool _stop;


		public PlayableDirector PlayableDirector => _playableDirector;

		public PlayableGraph PlayableGraph => _playableDirector.playableGraph;
		
	
		// ****************************************  METHODS  ****************************************\\

		private void OnValidate()
		{
			if (_play)
			{
				_play = false;
				_playableDirector.Play();
			}
			
			if (_pause)
			{
				_pause = false;
				_playableDirector.Pause();
			}
			
			if (_stop)
			{
				_stop = false;
				_playableDirector.Stop();
			}
		}

		private void Reset()
		{
			_playableDirector = GetComponent<PlayableDirector>();
		}
		
		private void Awake()
		{
			_playableDirector = GetComponent<PlayableDirector>();
		}
		
		private void OnEnable()
		{
			_playableDirector.played += OnPlayableDirectorPlayed;
			_playableDirector.paused += OnPlayableDirectorPaused;
			_playableDirector.stopped += OnPlayableDirectorStopped;
		}
		
		private void OnDisable()
		{
			_playableDirector.played -= OnPlayableDirectorPlayed;
			_playableDirector.paused -= OnPlayableDirectorPaused;
			_playableDirector.stopped -= OnPlayableDirectorStopped;
		}

		public void SetSpeed(float speed)
		{
			var playableGraph = PlayableGraph;
			if (!playableGraph.IsValid()) return;
			playableGraph.GetRootPlayable(0).SetSpeed(speed);
		}
	
		
		private void OnPlayableDirectorPlayed(PlayableDirector playableDirector)
		{
			Debug.Log($"{playableDirector.name}/{nameof(PlayableDirector)} => Played");
		}

		private void OnPlayableDirectorPaused(PlayableDirector playableDirector)
		{
			Debug.Log($"{playableDirector.name}/{nameof(PlayableDirector)} => Paused");
		}

		private void OnPlayableDirectorStopped(PlayableDirector playableDirector)
		{
			Debug.Log($"{playableDirector.name}/{nameof(PlayableDirector)} => Stopped");
		}
		
	}
}