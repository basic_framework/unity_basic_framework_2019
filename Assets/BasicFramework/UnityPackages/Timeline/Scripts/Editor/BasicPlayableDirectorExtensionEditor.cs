﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomEditor(typeof(BasicPlayableDirectorExtension))]
	public class BasicPlayableDirectorExtensionEditor : BasicBaseEditor<BasicPlayableDirectorExtension>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var playableDirector = serializedObject.FindProperty("_playableDirector");

			GUI.enabled = false;

			BasicEditorGuiLayout.PropertyFieldNotNull(playableDirector);

			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			DrawDefaultInspector();
		}
	}
}