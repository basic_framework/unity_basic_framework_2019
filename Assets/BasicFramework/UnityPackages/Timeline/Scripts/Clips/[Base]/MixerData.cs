﻿using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace BasicFramework.UnityPackages.Timeline
{
	public enum MixerClipStates
	{
		FullTrack,
		TrackToClip,
		ClipToTrack,
		ClipToClip,
		FullClip,
	}
	
	public class MixerData<TClip, TPlayable, TTrack>
		where TClip : BasicClipBase
		where TPlayable : BasicPlayableBase<TClip>, new()
		where TTrack : BasicTrackBase, new()
	{
		
		// **************************************** VARIABLES ****************************************\\

		/// <summary>
		/// The TrackAsset associated with the mixer
		/// </summary>
		public TTrack TrackAsset;

		/// <summary>
		/// The playable that is easing or blending in
		/// </summary>
		private TPlayable _playableIn;
		public TPlayable PlayableIn
		{
			get => _playableIn;
			set
			{
				_playableIn = value;
				if(_playablePlayingFull != null) return;
				_mixerClipState = _playableOut == null ? MixerClipStates.TrackToClip : MixerClipStates.ClipToClip;
			}
		}

		/// <summary>
		/// The playable that is fully playing
		/// </summary>
		private TPlayable _playablePlayingFull;
		public TPlayable PlayablePlayingFull
		{
			get => _playablePlayingFull;
			set
			{
				_playablePlayingFull = value;
				_mixerClipState = MixerClipStates.FullClip;
			}
		}
		
		/// <summary>
		/// The playable that is easing or blending out
		/// </summary>
		private TPlayable _playableOut;
		public TPlayable PlayableOut
		{
			get => _playableOut;
			set
			{
				_playableOut = value;
				if(_playablePlayingFull != null) return;
				_mixerClipState = _playableIn == null ? MixerClipStates.ClipToTrack : MixerClipStates.ClipToClip;
			}
		}

		/// <summary>
		/// An enumerable of all the playables managed by the mixer
		/// </summary>
		private IEnumerable<TPlayable> _playables;
		public IEnumerable<TPlayable> Playables
		{
			get => _playables;
			set => _playables = value;
		}


		/// <summary>
		/// The state of the mixer, e.g. if it is mixing between clips, the track, fully playing a clip, etc.
		/// </summary>
		private MixerClipStates _mixerClipState;
		public MixerClipStates MixerClipState => _mixerClipState;

		/// <summary>
		/// Is the mixer currenlty mixing values with the track, i.e. does the track currently have any weight
		/// </summary>
		public bool MixingWithTrack =>
			!(_mixerClipState == MixerClipStates.FullClip || MixerClipState == MixerClipStates.ClipToClip);
		

		// **************************************** METHODS ****************************************\\
		
		public virtual void ResetProcessFrameData()
		{
			_playableIn = null;
			_playableOut = null;
			_playablePlayingFull = null;
			
			_playables = null;
			
			_mixerClipState = MixerClipStates.FullTrack;
		}


		/// <summary>
		/// Gets the last playable that had its full weight played 
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public TPlayable GetLastFullyWeightedPlayable()
		{
			if (Playables == null) return null;
			
			TPlayable lastCompleted = default;

			foreach (var input in Playables)
			{
				var clip = input.Clip;
				var clipState = clip.ClipState;

				switch (clipState)
				{
					case ClipStates.Waiting:
					case ClipStates.EasingIn:
					case ClipStates.BlendingIn:
						break;
					
					case ClipStates.PlayingFullWeight:
					case ClipStates.BlendingOut:
					case ClipStates.EasingOut:
						return input;
						
					case ClipStates.Ended:
						
						if (lastCompleted == null)
						{
							lastCompleted = input;
							break;
						}

						var clipEnd = clip.End;
						var lastPlayedEnd = lastCompleted.Clip.End;

						// Make sure end time is not the same
						if (Math.Abs(clipEnd - lastPlayedEnd) > double.Epsilon)
						{
							if (clipEnd > lastPlayedEnd)
							{
								lastCompleted = input;
							}
							break;
						}
						
						var clipStart = clip.Start;
						var lastPlayedStart = lastCompleted.Clip.Start;
						
						// Make sure start time is not the same
						if (Math.Abs(clipStart - lastPlayedStart) > double.Epsilon)
						{
							if (clipStart > lastPlayedStart)
							{
								lastCompleted = input;
							}
							break;
						}
						
						// clips fully overlapping, the last one is the one that was blended in to
						if (clip.EaseInDuration > 0)
						{
							lastCompleted = input;
						}
						
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			return lastCompleted;
		}
		
		/// <summary>
		/// Gets the last played playable, if no clip is playing, this is the clip that finished last. 
		/// If a clip is playing, this is the most recent playing clip, i.e. the one blending in if in blend state
		/// </summary>
		/// <param name="inputs"></param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public TPlayable GetLastPlayedPlayable()
		{
			if (Playables == null) return null;
			
			TPlayable lastPlayed = default;

			foreach (var input in Playables)
			{
				var clip = input.Clip;
				var clipState = clip.ClipState;

				switch (clipState)
				{
					case ClipStates.Waiting:
						break;
					
					case ClipStates.EasingIn:
					case ClipStates.BlendingIn:
					case ClipStates.PlayingFullWeight:
						return input;
						
					case ClipStates.BlendingOut:
						// Blending in to a clip, the BlendIn clip will be the last played
						break;
						
					case ClipStates.EasingOut:
						return input;

					case ClipStates.Ended:
						
						if (lastPlayed == null)
						{
							lastPlayed = input;
							break;
						}

						var clipEnd = clip.End;
						var lastPlayedEnd = lastPlayed.Clip.End;

						// Make sure end time is not the same
						if (Math.Abs(clipEnd - lastPlayedEnd) > double.Epsilon)
						{
							if (clipEnd > lastPlayedEnd)
							{
								lastPlayed = input;
							}
							break;
						}
						
						var clipStart = clip.Start;
						var lastPlayedStart = lastPlayed.Clip.Start;
						
						// Make sure start time is not the same
						if (Math.Abs(clipStart - lastPlayedStart) > double.Epsilon)
						{
							if (clipStart > lastPlayedStart)
							{
								lastPlayed = input;
							}
							break;
						}
						
						// clips fully overlapping, the last one is the one that was blended in to
						if (clip.EaseInDuration > 0)
						{
							lastPlayed = input;
						}
						
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			return lastPlayed;
		}
		
	}
	
	public class MixerData<TClip, TPlayable, TTrack, TBinding> : MixerData<TClip, TPlayable, TTrack>
		where TClip : BasicClipBase
		where TPlayable : BasicPlayableBase<TClip>, new()
		where TBinding : Object
		where TTrack : BasicTrackBase, new()
	{
		
		// **************************************** VARIABLES ****************************************\\
		
		/// <summary>
		/// The track binding
		/// </summary>
		public TBinding Binding;

		
		// **************************************** METHODS ****************************************\\
		
		public override void ResetProcessFrameData()
		{
			base.ResetProcessFrameData();

			Binding = null;
		}
	}
}