﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.Playables;
using Object = UnityEngine.Object;

namespace BasicFramework.UnityPackages.Timeline
{
	public abstract class BasicMixerBase : PlayableBehaviour
	{
		// **************************************** VARIABLES ****************************************\\
		
		public abstract void SetTrackAsset(BasicTrackBase trackAsset);
		
		// ****************************************  METHODS  ****************************************\\
		
	}
	
	// No track binding
	public abstract class BasicMixerBase<TClip, TPlayable, TTrack> : BasicMixerBase
		where TClip : BasicClipBase
		where TPlayable : BasicPlayableBase<TClip>, new()
		where TTrack : BasicTrackBase, new()
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private readonly List<TPlayable> _playableBehaviours = new List<TPlayable>();

		
		private readonly MixerData<TClip, TPlayable, TTrack> _mixerData = new MixerData<TClip, TPlayable, TTrack>();
		
		protected virtual MixerData<TClip, TPlayable, TTrack> MixerData => _mixerData;
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** virtual/abstract *****\\
		
		// NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
		protected abstract void OnProcessFrame(Playable playable, FrameData info, object playerData, MixerData<TClip, TPlayable, TTrack> mixerData);
		
		
		//***** Callbacks *****\\
		
		public sealed override void SetTrackAsset(BasicTrackBase trackAsset)
		{
			MixerData.TrackAsset = trackAsset as TTrack;
		}
		
		public sealed override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
			var mixerData = MixerData;
			mixerData.ResetProcessFrameData();
			
			var trackAsset = mixerData.TrackAsset;
			
			var time = trackAsset.PlayableDirector.time;
			var inputCount = playable.GetInputCount(); //get the number of all clips on this track
			var trackWeight = 1f;
			
			_playableBehaviours.Resize(inputCount);
			
			for (int i = 0; i < inputCount; i++)
			{
				var playableBehaviour = ((ScriptPlayable<TPlayable>) playable.GetInput(i)).GetBehaviour();
				var weight = playable.GetInputWeight(i);
				
				var clip = playableBehaviour.Clip;
				clip.MixerUpdate(weight, time);

				var clipState = clip.ClipState;

				switch (clipState)
				{
					case ClipStates.Waiting:
						break;
					
					case ClipStates.EasingIn:
					case ClipStates.BlendingIn:
						mixerData.PlayableIn = playableBehaviour;
						break;
					
					case ClipStates.PlayingFullWeight:
						mixerData.PlayablePlayingFull = playableBehaviour;
						break;
					
					case ClipStates.BlendingOut:
					case ClipStates.EasingOut:
						mixerData.PlayableOut = playableBehaviour;
						break;
					
					case ClipStates.Ended:
						break;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
	
				_playableBehaviours[i] = playableBehaviour;
				
				if(trackWeight <= 0) continue;
				trackWeight -= weight;
			}

			mixerData.Playables = _playableBehaviours;
			trackAsset.Weight = trackWeight;
			
			OnProcessFrame(playable, info, playerData, mixerData);
		}
		
	}

	// With track binding
	public abstract class BasicMixerBase<TClip, TPlayable, TTrack,  TBinding> : BasicMixerBase<TClip, TPlayable, TTrack>
		where TClip : BasicClipBase
		where TPlayable : BasicPlayableBase<TClip>, new()
		where TTrack : BasicTrackBase, new()
		where TBinding : Object
	{
		
		// **************************************** VARIABLES ****************************************\\

		private readonly MixerData<TClip, TPlayable, TTrack, TBinding> _mixerData = 
			new MixerData<TClip, TPlayable, TTrack, TBinding>();
		
		protected override MixerData<TClip, TPlayable, TTrack> MixerData => _mixerData;
		

		// ****************************************  METHODS  ****************************************\\

		//***** virtual/abstract *****\\
		
		// NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
		//protected abstract void OnProcessFrame(Playable playable, FrameData info, TBinding binding, IEnumerable<TPlayable> inputs);
		protected abstract void OnProcessFrame(Playable playable, FrameData info, MixerData<TClip, TPlayable, TTrack, TBinding> mixerData);
		
		
		//***** Callbacks *****\\
		
		protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, 
			MixerData<TClip, TPlayable, TTrack> mixerData)
		{
			var trackBinding = playerData as TBinding;
			if (trackBinding == null)
			{
				Debug.LogError($"{nameof(GetType)} => Could not get track binding of {nameof(TBinding)}");
				return;
			}

			_mixerData.Binding = trackBinding;
			
			OnProcessFrame(playable, info, _mixerData);
		}
		
	}
}