﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	
	[CustomEditor(typeof(BasicClipBase), true)]
	public class BasicClipBaseEditor : BasicClipBaseEditor<BasicClipBase>
	{
	}
	
	[CustomEditor(typeof(BasicClipBase), true)]
	public class BasicClipBaseEditor<TClip> : BasicBaseEditor<TClip>
		where TClip : BasicClipBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool UpdateConstantlyInPlayMode => true;

		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		private static bool _debugExpanded = true;
		
		protected override bool DebugSectionIsExpanded
		{
			get => _debugExpanded;
			set => _debugExpanded = value;
		}
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			GUI.enabled = false;

			var playableDirector = TypedTarget.PlayableDirector;
			
			EditorGUILayout.ObjectField("Playable Director", playableDirector, typeof(PlayableDirector), true);

			Space();
			EditorGUILayout.LabelField("Is Clip Playing", TypedTarget.IsClipPlaying.ToString());
			EditorGUILayout.LabelField("Clip State", TypedTarget.ClipState.ToString().CamelCaseToHumanReadable());
			EditorGUILayout.Slider("Weight", TypedTarget.Weight, 0, 1);
			EditorGUILayout.Slider("Progress", (float)TypedTarget.Progress, 0, 1);
			
//			Space();
//			EditorGUILayout.LabelField("Has Blend In", TypedTarget.HasBlendIn.ToString());
//			EditorGUILayout.LabelField("Has Blend Out", TypedTarget.HasBlendOut.ToString().CamelCaseToHumanReadable());
//			
//			EditorGUILayout.LabelField("Blend In Duration", TypedTarget.BlendInDuration.ToString());
//			EditorGUILayout.LabelField("Blend Out Duration", TypedTarget.BlendOutDuration.ToString().CamelCaseToHumanReadable());
			
			GUI.enabled = true;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			SectionHeader("Clip Properties");
			
			DrawDefaultInspector();
		}
	
	}
}