﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline
{
	[CustomEditor(typeof(BasicTrackBase), true)]
	public class BasicTrackBaseEditor : BasicTrackBaseEditor<BasicTrackBase>
	{
	}

	public abstract class BasicTrackBaseEditor<TBasicTrack> : BasicBaseEditor<TBasicTrack>
		where TBasicTrack : BasicTrackBase
	{
		
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			GUI.enabled = false;
			
			var playableDirector = TypedTarget.PlayableDirector;
			var playableDirectorNotNull = playableDirector != null;
			
			EditorGUILayout.ObjectField("Playable Director", playableDirector, typeof(PlayableDirector), true);
			
			Space();
			SectionHeader("Playback");
			EditorGUILayout.LabelField("Is Any Clip Playing", TypedTarget.IsAnyClipPlaying.ToString());
			EditorGUILayout.Slider("Track Weight", TypedTarget.Weight, 0, 1);

			Space();
			SectionHeader("Advanced Playback");
			EditorGUILayout.LabelField("Duration", (playableDirectorNotNull ? playableDirector.duration : 0).ToString());
			EditorGUILayout.LabelField("Time", (playableDirectorNotNull ? playableDirector.time : 0).ToString());
			EditorGUILayout.Slider("Progress", playableDirectorNotNull ? TypedTarget.Progress : 0 , 0, 1);
			
			GUI.enabled = true;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			Space();
			SectionHeader($"{nameof(BasicTrackBase).CamelCaseToHumanReadable()} Properties");
			DrawDefaultInspector();
		}
	}
}