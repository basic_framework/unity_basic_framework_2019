﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	
	public abstract class BasicTrackBase : TrackAsset 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private PlayableDirector _playableDirector;
		public PlayableDirector PlayableDirector
		{
			get => _playableDirector;
			set => _playableDirector = value;
		}

		private float _weight = 1f;
		public float Weight
		{
			get => _weight;
			set => _weight = value;
		}
		
		public bool IsAnyClipPlaying => Weight < 1f;
		public float Progress => Mathf.Clamp01((float) (PlayableDirector.time / PlayableDirector.duration));
		

		// ****************************************  METHODS  ****************************************\\


	}
	
	public abstract class BasicTrackBase<TClip, TPlayable, TMixer> : BasicTrackBase
		where TClip : BasicClipBase<TPlayable>
		where TPlayable : BasicPlayableBase, new()
		where TMixer : BasicMixerBase, new()
	{
		
		// **************************************** VARIABLES ****************************************\\

		public TMixer TrackMixer { get; private set; }


		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			var mixerScriptPlayable = ScriptPlayable<TMixer>.Create(graph, inputCount);

			TrackMixer = mixerScriptPlayable.GetBehaviour();
			TrackMixer.SetTrackAsset(this);
			
			PlayableDirector = graph.GetResolver() as PlayableDirector;
			
			foreach (var timelineClip in GetClips())
			{
				var basicClip = timelineClip.asset as TClip;
				if (basicClip == null) continue;
				basicClip.PlayableDirector = PlayableDirector;
				basicClip.TimelineClip = timelineClip;
				//basicClip.UpdateTimingData();
			}
			
			return mixerScriptPlayable;
		}
		
	}
}