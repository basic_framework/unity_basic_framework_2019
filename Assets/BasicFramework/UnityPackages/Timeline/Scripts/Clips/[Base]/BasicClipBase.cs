﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	
	public abstract class BasicClipBase : PlayableAsset 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private PlayableDirector _playableDirector;
		public PlayableDirector PlayableDirector
		{
			get => _playableDirector;
			set
			{
				if (_playableDirector == value) return;
				_playableDirector = value;
				PlayableDirectorUpdated(_playableDirector);
			}
		}
		
		private TimelineClip _timelineClip;
		public TimelineClip TimelineClip
		{
			get => _timelineClip;
			set
			{
				if (_timelineClip == value) return;
				_timelineClip = value;
				TimelineClipUpdated(_timelineClip);
			}
		}
		
		private double _progress;
		public double Progress => _progress;
		
		private float _weight;
		public float Weight => _weight;
		
		private ClipStates _clipState;
		public ClipStates ClipState => _clipState;


		public double Duration => _timelineClip.end - _timelineClip.start;
		public bool IsClipPlaying => Weight > Mathf.Epsilon;
		
		
		public double Start => TimelineClip.start;
		public double End => TimelineClip.end;

		public double EaseInDuration => TimelineClip.easeInDuration;
		public double EaseOutDuration => TimelineClip.easeOutDuration;

		public bool HasBlendIn => TimelineClip.hasBlendIn;
		public bool HasBlendOut => TimelineClip.hasBlendOut;

		public double BlendInDuration => TimelineClip.blendInDuration;
		public double BlendOutDuration => TimelineClip.blendOutDuration;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected virtual void PlayableDirectorUpdated(PlayableDirector playableDirector){}
		
		protected virtual void TimelineClipUpdated(TimelineClip timelineClip){}

		public void MixerUpdate(float weight, double time)
		{
			_weight = weight;

			if (time <= Start)
			{
				_progress = 0;
			}
			else if (time >= End)
			{
				_progress = 1;
			}
			else
			{
				_progress = MathUtility.Map(time, Start, End, 0, 1);
				_progress = Math.Max(0, _progress);
				_progress = Math.Min(1, _progress);
			}
			
			_clipState = Math.Abs(_weight) < Mathf.Epsilon
				? _progress < 0.5 ? ClipStates.Waiting : ClipStates.Ended
				: GetClipStateInBetween(time);
		}

		private ClipStates GetClipStateInBetween(double time)
		{
			double compareTime;

			if (_timelineClip.hasBlendIn)
			{
				compareTime = _timelineClip.start + _timelineClip.blendInDuration;
				if (time < compareTime) return ClipStates.BlendingIn;
			}
			else
			{
				compareTime = _timelineClip.start + _timelineClip.easeInDuration;
				if (time < compareTime) return ClipStates.EasingIn;
			}
			
			
			if (_timelineClip.hasBlendOut)
			{
				compareTime = _timelineClip.end - _timelineClip.blendOutDuration;
				if (time > compareTime) return ClipStates.BlendingOut;
			}
			else
			{
				compareTime = _timelineClip.end - _timelineClip.easeOutDuration;
				if (time > compareTime) return ClipStates.EasingOut;
			}

			return ClipStates.PlayingFullWeight;
		}

	}
	
	public abstract class BasicClipBase<TPlayable> : BasicClipBase
		where TPlayable : BasicPlayableBase, new()
	{
	
		// **************************************** VARIABLES ****************************************\\

		public TPlayable Template = default;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
		{
			var playable = ScriptPlayable<TPlayable>.Create(graph, Template);
			
			var playableBehaviour = playable.GetBehaviour();
			playableBehaviour.SetBasicClip(this);
			
			return playable;
		}
	
	}
}