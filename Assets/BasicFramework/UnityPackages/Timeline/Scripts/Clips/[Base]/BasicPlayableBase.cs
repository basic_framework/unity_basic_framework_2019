﻿using System;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public abstract class BasicPlayableBase : PlayableBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		

		// ****************************************  METHODS  ****************************************\\

		public abstract void SetBasicClip(BasicClipBase basicClipBase);

	}
	
	[Serializable]
	public abstract class BasicPlayableBase<TClip> : BasicPlayableBase
		where TClip : BasicClipBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		private TClip _clip;
		public TClip Clip => _clip;
		

		// ****************************************  METHODS  ****************************************\\

		public sealed override void SetBasicClip(BasicClipBase basicClipBase)
		{
			_clip = basicClipBase as TClip;
			ClipUpdated(_clip);
		}
		
		protected virtual void ClipUpdated(TClip clip){}
	}
}