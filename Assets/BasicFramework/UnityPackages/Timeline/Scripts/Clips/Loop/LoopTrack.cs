﻿using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[TrackClipType(typeof(LoopClip))]
	public class LoopTrack : BasicTrackBase<LoopClip, LoopPlayable, LoopMixer> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// TODO: Add properties here
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	
	}
}