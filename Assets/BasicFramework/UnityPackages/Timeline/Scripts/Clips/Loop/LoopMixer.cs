﻿using System;
using UnityEngine.Playables;
using Object = System.Object;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class LoopMixer : BasicMixerBase<LoopClip, LoopPlayable, LoopTrack> 
	{
	
		//TODO: LoopClipMode, BoolSingle, Count, EventWithBoolReturn
		
		// **************************************** VARIABLES ****************************************\\
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnProcessFrame(Playable playable, FrameData info, object binding, 
			MixerData<LoopClip, LoopPlayable, LoopTrack> mixerData)
		{
			// Determine the output values given the inputs
			foreach (var input in mixerData.Playables)
			{
				if(!input.LoopAtEnd) continue;
				
				
				// Check if we passed the end of the clip
				var basicClip = input.Clip;

				var playableDirector = basicClip.PlayableDirector;
				var progressPrevious = basicClip.ProgressPrevious;
				var progress = basicClip.Progress;
				basicClip.ProgressPrevious = progress;
				
				if (progressPrevious >= 1f) continue;
				if (progress < 1f) continue;
				if (playableDirector.time > basicClip.End + input.ClipEndBuffer) continue;
				
				

				// TODO: Check if director wrap mode is none, jump to and start playing again
				if(playableDirector.state == PlayState.Paused) continue;
				
				// Perform loop
				playableDirector.time = basicClip.Start;

				input.LoopCount++;
				
				input.Looped.Invoke();

				return;
			}
		}
		
//		protected override void OnProcessFrame(Playable playable, FrameData info, Object binding, IEnumerable<LoopPlayable> inputs)
//		{
//			// Determine the output values given the inputs
//			foreach (var input in inputs)
//			{
//				if(!input.LoopAtEnd) continue;
//				
//				
//				// Check if we passed the end of the clip
//				var basicClip = input.Clip;
//
//				var progressPrevious = basicClip.ProgressPrevious;
//				var progress = basicClip.Progress;
//				basicClip.ProgressPrevious = progress;
//				
//				if (progressPrevious >= 1f) continue;
//				if (progress < 1f) continue;
//				
//				
//				
//				var playableDirector = basicClip.PlayableDirector;
//				
//				// TODO: Check if director wrap mode is none, jump to and start playing again
//				if(playableDirector.state == PlayState.Paused) continue;
//				
//				// Perform loop
//				playableDirector.time = basicClip.Start;
//
//				input.LoopCount++;
//				
//				input.Looped.Invoke();
//
//				return;
//			}
//		}
		
	}
}