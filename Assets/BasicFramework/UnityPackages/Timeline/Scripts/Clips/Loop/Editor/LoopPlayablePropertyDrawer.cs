﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomPropertyDrawer(typeof(LoopPlayable))]
	public class LoopPlayablePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect.height = SingleLineHeight;
			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			if (!property.isExpanded) return;
			
			var loopMode = property.FindPropertyRelative("_loopMode");
			var maxLoopCount = property.FindPropertyRelative("_maxLoopCount");
			var loopCondition = property.FindPropertyRelative("_loopCondition");
			var looped = property.FindPropertyRelative("_looped");
			var clipEndBuffer = property.FindPropertyRelative("_clipEndBuffer");
			
			
			TempRect.height = GetPropertyHeight(clipEndBuffer, true);
			EditorGUI.PropertyField(TempRect, clipEndBuffer, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			TempRect.height = GetPropertyHeight(loopMode, true);
			EditorGUI.PropertyField(TempRect, loopMode, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			var loopModeEnum =
				EnumUtility.EnumNameToEnumValue<LoopModes>(loopMode.enumNames[loopMode.enumValueIndex]);
			
			switch (loopModeEnum)
			{
				case LoopModes.Infinite:
					TempRect.height = SingleLineHeight;
					EditorGUI.LabelField(TempRect, "Infinite Mode", "Clip Will Loop Until the End of Time");
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					break;
				
				case LoopModes.Condition:
					TempRect.height = GetPropertyHeight(loopCondition, true);
					EditorGUI.PropertyField(TempRect, loopCondition, true);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					break;
				
				case LoopModes.Count:
					TempRect.height = GetPropertyHeight(maxLoopCount, true);
					EditorGUI.PropertyField(TempRect, maxLoopCount, true);
					TempRect.y += BUFFER_HEIGHT + TempRect.height;
					break;

				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			TempRect.height = GetPropertyHeight(looped, true);
			EditorGUI.PropertyField(TempRect, looped, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;

			var loopMode = property.FindPropertyRelative("_loopMode");
			var maxLoopCount = property.FindPropertyRelative("_maxLoopCount");
			var loopCondition = property.FindPropertyRelative("_loopCondition");
			var looped = property.FindPropertyRelative("_looped");
			var clipEndBuffer = property.FindPropertyRelative("_clipEndBuffer");

			height += BUFFER_HEIGHT + GetPropertyHeight(loopMode, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(clipEndBuffer, true);

			var loopModeEnum =
				EnumUtility.EnumNameToEnumValue<LoopModes>(loopMode.enumNames[loopMode.enumValueIndex]);

			switch (loopModeEnum)
			{
				case LoopModes.Infinite:
					height += BUFFER_HEIGHT + SingleLineHeight;
					break;
				
				case LoopModes.Condition:
					height += BUFFER_HEIGHT + GetPropertyHeight(loopCondition, true);
					break;
				
				case LoopModes.Count:
					height += BUFFER_HEIGHT + GetPropertyHeight(maxLoopCount, true);
					break;

				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			height += BUFFER_HEIGHT + GetPropertyHeight(looped, true);
			
			return height;
		}
		
	}
}