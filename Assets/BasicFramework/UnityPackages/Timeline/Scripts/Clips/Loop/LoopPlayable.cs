﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class LoopPlayable : BasicPlayableBase<LoopClip>
	{
	
		// TODO: Need a reliable way to reset the loop count
		
		// **************************************** VARIABLES ****************************************\\
		
		// TODO: Add properties here

		[Tooltip("Infinite: The clip will continue to loop forever\n" +
		         "Condition: Loops if the condition is met\n" +
		         "Count: Loops until the count is reached")]
		[SerializeField] private LoopModes _loopMode = LoopModes.Count;

		[Tooltip("The number of times it must loop before it will continue")]
		[SerializeField] private int _maxLoopCount = 1;

		[Tooltip("The maximum amount of time after the loop the playhead can be for the clip to loop.\n" +
		         "This is required to allow jumping out of the clip")]
		[SerializeField] private double _clipEndBuffer = 0.25f;

		[Tooltip("Loops if the condition is met")]
		[SerializeField] private ScriptableSingleCondition _loopCondition = default;

		[Tooltip("Raised when the clip loops")]
		[SerializeField] private UnityEvent _looped = default;


		public double ClipEndBuffer => _clipEndBuffer;

		public bool LoopAtEnd
		{
			get
			{
				switch (_loopMode)
				{
					case LoopModes.Infinite:
						return true;
					
					case LoopModes.Condition:
						return (Application.isPlaying || _loopCondition.IsValid) && _loopCondition.CheckCondition();

					case LoopModes.Count:
						return _loopCount < _maxLoopCount;
					
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
		
		public UnityEvent Looped => _looped;

		private int _loopCount;
		public int LoopCount
		{
			get => _loopCount;
			set
			{
				if (_loopCount == value) return;
				_loopCount = value;
				LoopCountUpdated(_loopCount);
			}
		}


		// ****************************************  METHODS  ****************************************\\

		protected override void ClipUpdated(LoopClip clip)
		{
			base.ClipUpdated(clip);

			if (clip == null) return;
			
			LoopCountUpdated(_loopCount);
		}
		

		private void LoopCountUpdated(int loopCount)
		{
			Clip.TimelineClip.displayName = $"{nameof(LoopClip)} -> {loopCount}";
		}
		
		
	}
}