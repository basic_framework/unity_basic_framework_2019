﻿using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline
{
	public enum LoopModes
	{
		Infinite,
		Condition,
		Count
	}
}