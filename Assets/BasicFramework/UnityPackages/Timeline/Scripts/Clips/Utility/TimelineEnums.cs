﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.UnityPackages.Timeline
{
	
	public enum ClipStates
	{
		Waiting,
		EasingIn,
		BlendingIn,
		PlayingFullWeight,
		BlendingOut,
		EasingOut,
		Ended,
	}
	
	[Serializable]
	public class ClipStatesUnityEvent : UnityEvent<ClipStates>{}

	[Serializable]
	public class ClipStatesCompareUnityEvent : CompareValueUnityEventBase<ClipStates>{}


	public enum TrackMixModes
	{
		MixWithTrack,
		LastPlayedMixed,
		LastPlayed,
		DoNothing,
	}
}