﻿using System;

namespace BasicFramework.UnityPackages.Timeline
{
	[Flags]
	public enum EventRaiseModeFlags
	{
		ClipPlayingChange 	= 1 << 0,
		BlendEaseInChange	= 1 << 1,
		FullWeightChange	= 1 << 2,
		BlendEaseOutChange	= 1 << 3,
		ClipEnded			= 1 << 4
	}
}