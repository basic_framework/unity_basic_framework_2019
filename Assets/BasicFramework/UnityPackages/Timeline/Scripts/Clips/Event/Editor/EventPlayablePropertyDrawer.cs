﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.Editor
{
	[CustomPropertyDrawer(typeof(EventPlayable))]
	public class EventPlayablePropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float LOOP_COUNT_FIELD_WIDTH = 80;
		private const float BUFFER_MULTIPLIER = 2;
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect.height = SingleLineHeight;
			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			if (!property.isExpanded) return;

			var eventRaiseMode = property.FindPropertyRelative("_eventRaiseMode");
			var clipPlayingChanged = property.FindPropertyRelative("_clipPlayingChanged");
			var blendEaseInChanged = property.FindPropertyRelative("_blendEaseInChanged");
			var fullWeightChanged = property.FindPropertyRelative("_fullWeightChanged");
			var blendEaseOutChanged = property.FindPropertyRelative("_blendEaseOutChanged");
			var clipEnded = property.FindPropertyRelative("_clipEnded");
			
			TempRect.height = GetPropertyHeight(eventRaiseMode, true);
			EditorGUI.PropertyField(TempRect, eventRaiseMode);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			

			var raiseMode = (EventRaiseModeFlags) eventRaiseMode.intValue;

			GUI.enabled = PrevGuiEnabled && (raiseMode & EventRaiseModeFlags.ClipPlayingChange) != 0;
			TempRect.height = GetPropertyHeight(clipPlayingChanged, true);
			EditorGUI.PropertyField(TempRect, clipPlayingChanged, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT * BUFFER_MULTIPLIER;
			
			GUI.enabled = PrevGuiEnabled && (raiseMode & EventRaiseModeFlags.BlendEaseInChange) != 0;
			TempRect.height = GetPropertyHeight(blendEaseInChanged, true);
			EditorGUI.PropertyField(TempRect, blendEaseInChanged, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT * BUFFER_MULTIPLIER;
			
			GUI.enabled = PrevGuiEnabled && (raiseMode & EventRaiseModeFlags.FullWeightChange) != 0;
			TempRect.height = GetPropertyHeight(fullWeightChanged, true);
			EditorGUI.PropertyField(TempRect, fullWeightChanged, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT * BUFFER_MULTIPLIER;
			
			GUI.enabled = PrevGuiEnabled && (raiseMode & EventRaiseModeFlags.BlendEaseOutChange) != 0;
			TempRect.height = GetPropertyHeight(blendEaseOutChanged, true);
			EditorGUI.PropertyField(TempRect, blendEaseOutChanged, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT * BUFFER_MULTIPLIER;
			
			GUI.enabled = PrevGuiEnabled && (raiseMode & EventRaiseModeFlags.ClipEnded) != 0;
			TempRect.height = GetPropertyHeight(clipEnded, true);
			EditorGUI.PropertyField(TempRect, clipEnded, true);
			TempRect.y += TempRect.height + BUFFER_HEIGHT * BUFFER_MULTIPLIER;

			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) return height;
			
			var eventRaiseMode = property.FindPropertyRelative("_eventRaiseMode");
			var clipPlayingChanged = property.FindPropertyRelative("_clipPlayingChanged");
			var blendEaseInChanged = property.FindPropertyRelative("_blendEaseInChanged");
			var fullWeightChanged = property.FindPropertyRelative("_fullWeightChanged");
			var blendEaseOutChanged = property.FindPropertyRelative("_blendEaseOutChanged");
			var clipEnded = property.FindPropertyRelative("_clipEnded");
			
			height += BUFFER_HEIGHT + GetPropertyHeight(eventRaiseMode, true);
			height += BUFFER_HEIGHT * BUFFER_MULTIPLIER + GetPropertyHeight(clipPlayingChanged, true);
			height += BUFFER_HEIGHT * BUFFER_MULTIPLIER + GetPropertyHeight(blendEaseInChanged, true);
			height += BUFFER_HEIGHT * BUFFER_MULTIPLIER + GetPropertyHeight(fullWeightChanged, true);
			height += BUFFER_HEIGHT * BUFFER_MULTIPLIER + GetPropertyHeight(blendEaseOutChanged, true);
			height += BUFFER_HEIGHT * BUFFER_MULTIPLIER + GetPropertyHeight(clipEnded, true);
			
			return height;
		}
		
	}
}