﻿using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[TrackClipType(typeof(EventClip))]
	public class EventTrack : BasicTrackBase<EventClip, EventPlayable, EventMixer> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// TODO: Add properties here
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	
	}
}