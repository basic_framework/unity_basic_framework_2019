﻿
namespace BasicFramework.UnityPackages.Timeline
{
	public class EventClip : BasicClipBase<EventPlayable> 
	{
	
		// **************************************** VARIABLES ****************************************\\

		public float WeightPrevious { get; set; }
		public ClipStates ClipStatePrevious { get; set; }
		public double ProgressPrevious { get; set; }
		

		// ****************************************  METHODS  ****************************************\\


	}
}