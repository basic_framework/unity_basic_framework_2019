﻿using System;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class EventPlayable : BasicPlayableBase<EventClip> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[EnumFlag("Under which circumstances should the events be raised")]
		[SerializeField] private EventRaiseModeFlags _eventRaiseMode = default;
		
		[Tooltip("Raised when the clip starts/stops being played, at any weight")]
		[SerializeField] private BoolPlusUnityEvent _clipPlayingChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the clip state enters/exits blend in or ease in")]
		[SerializeField] private BoolPlusUnityEvent _blendEaseInChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the clip state enters/exits full weight")]
		[SerializeField] private BoolPlusUnityEvent _fullWeightChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the clip state enters/exits blend out or ease out")]
		[SerializeField] private BoolPlusUnityEvent _blendEaseOutChanged = new BoolPlusUnityEvent();
		
		[Tooltip("Raised when the clip ends or if the clip is passed, i.e. the playhead never lands on the clip")]
		[SerializeField] private UnityEvent _clipEnded = new UnityEvent();
		
		
		// TODO: Add properties here
		
		public EventRaiseModeFlags EventRaiseMode => _eventRaiseMode;

		public BoolPlusUnityEvent ClipPlayingChanged => _clipPlayingChanged;

		public BoolPlusUnityEvent BlendEaseInChanged => _blendEaseInChanged;

		public BoolPlusUnityEvent FullWeightChanged => _fullWeightChanged;

		public BoolPlusUnityEvent BlendEaseOutChanged => _blendEaseOutChanged;

		public UnityEvent ClipEnded => _clipEnded;


		// ****************************************  METHODS  ****************************************\\


	}
}