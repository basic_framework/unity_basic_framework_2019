﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class EventMixer : BasicMixerBase<EventClip, EventPlayable, EventTrack> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, MixerData<EventClip, EventPlayable, EventTrack> mixerData)
		{
			var trackAsset = mixerData.TrackAsset;
			if (trackAsset.PlayableDirector.state == PlayState.Paused) return;
			
			// Evaluate and Raise the necessary events
			foreach (var input in mixerData.Playables)
			{
				RaiseEvents(input);
			}
        
		}
		

		private void RaiseEvents(EventPlayable eventPlayable)
		{
			var basicClip = eventPlayable.Clip;
			var raiseMode = eventPlayable.EventRaiseMode;

			if (raiseMode == 0) return;

			var weightPrevious = basicClip.WeightPrevious;
			var weight = basicClip.Weight;
			basicClip.WeightPrevious = weight;
	    
			var clipStatePrevious = basicClip.ClipStatePrevious;
			var clipState = basicClip.ClipState;
			basicClip.ClipStatePrevious = clipState;

			
	    
			if ((raiseMode & EventRaiseModeFlags.ClipPlayingChange) != 0)
			{
				var prevClipPlaying = weightPrevious > Mathf.Epsilon;
				var currClipPlaying = weight > Mathf.Epsilon;

				if (prevClipPlaying != currClipPlaying)
				{
					eventPlayable.ClipPlayingChanged.Raise(currClipPlaying);
				}
			}
		
			if ((raiseMode & EventRaiseModeFlags.BlendEaseInChange) != 0)
			{
				var prevBlendEaseIn =
					clipStatePrevious == ClipStates.BlendingIn || clipStatePrevious == ClipStates.EasingIn;

				var currBlendEaseIn = clipState == ClipStates.BlendingIn || clipState == ClipStates.EasingIn;

				if (prevBlendEaseIn != currBlendEaseIn)
				{
					eventPlayable.BlendEaseInChanged.Raise(currBlendEaseIn);
				}
			}
		
			if ((raiseMode & EventRaiseModeFlags.FullWeightChange) != 0)
			{
				var prevFullWeight = Math.Abs(weightPrevious - 1) < Mathf.Epsilon;
				var currFullWeight = Math.Abs(weight - 1) < Mathf.Epsilon;

				if (prevFullWeight != currFullWeight)
				{
					eventPlayable.FullWeightChanged.Raise(currFullWeight);
				}
			}
		
			if ((raiseMode & EventRaiseModeFlags.BlendEaseOutChange) != 0)
			{
				var prevBlendEaseOut =
					clipStatePrevious == ClipStates.BlendingOut || clipStatePrevious == ClipStates.EasingOut;

				var currBlendEaseOut = clipState == ClipStates.BlendingOut || clipState == ClipStates.EasingOut;

				if (prevBlendEaseOut != currBlendEaseOut)
				{
					eventPlayable.BlendEaseOutChanged.Raise(currBlendEaseOut);
				}
			}
			
			if ((raiseMode & EventRaiseModeFlags.ClipEnded) == 0) return;
			
			var progressPrevious = basicClip.ProgressPrevious;
			var progress = basicClip.Progress;
			basicClip.ProgressPrevious = progress;
			
			if (progressPrevious >= 1f) return;
			if (progress < 1f) return;
			eventPlayable.ClipEnded.Invoke();
		}

	}
}