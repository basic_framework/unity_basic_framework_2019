﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class BoolSingleMixer : ScriptableSingleMixerBase
    <
        BoolSingleClip, 
        BoolSinglePlayable, 
        BoolSingleTrack, 
        bool, 
        BoolSingle
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, 
            MixerData<BoolSingleClip, BoolSinglePlayable, BoolSingleTrack> mixerData)
        {
            base.ProcessMixerData(mixerData, mixerData.TrackAsset.SingleTrackData);
        }

        protected override bool Lerp(bool start, bool end, NormalizedFloat weight)
        {
            return weight == NormalizedFloat.Zero ? start : end;
        }
    }
}