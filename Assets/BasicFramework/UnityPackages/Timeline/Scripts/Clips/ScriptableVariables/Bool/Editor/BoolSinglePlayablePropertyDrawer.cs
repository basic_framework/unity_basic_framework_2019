﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
	[CustomPropertyDrawer(typeof(BoolSinglePlayable))]
	public class BoolSinglePlayablePropertyDrawer : ScriptableSinglePlayableBasePropertyDrawer
	<
		BoolSingleClip, 
		bool, 
		BoolSingle, 
		BoolSinglePlayable
	> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}