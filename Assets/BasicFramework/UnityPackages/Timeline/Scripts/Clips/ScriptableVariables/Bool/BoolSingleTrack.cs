﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	[Serializable]
	public class BoolTrackData : ScriptableSingleTrackData<bool, BoolSingle>{}

	[TrackClipType(typeof(BoolSingleClip))]
    public class BoolSingleTrack : ScriptableSingleTrackBase
    <
        BoolSingleClip, 
        BoolSinglePlayable, 
        BoolSingleMixer,
        bool,
        BoolSingle,
        BoolTrackData
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        // TODO: Add properties here
	
        
        // ****************************************  METHODS  ****************************************\\

	
    }
    
}