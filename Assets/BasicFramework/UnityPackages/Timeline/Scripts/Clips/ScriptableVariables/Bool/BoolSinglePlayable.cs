﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	
    [Serializable]
    public class BoolSinglePlayable : ScriptableSinglePlayableBase
    <
        BoolSingleClip, 
        bool, 
        BoolSingle
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        // TODO: Add properties here
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}