﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
    [CustomPropertyDrawer(typeof(BasicPoseSinglePlayable))]
    public class BasicPoseSinglePlayablePropertyDrawer : ScriptableSinglePlayableBasePropertyDrawer
    <
        BasicPoseSingleClip, 
        BasicPose, 
        BasicPoseSingle, 
        BasicPoseSinglePlayable
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}