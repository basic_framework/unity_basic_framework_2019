﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	[Serializable]
    public class BasicPoseSinglePlayable : ScriptableSinglePlayableBase
    <
        BasicPoseSingleClip, 
        BasicPose, 
        BasicPoseSingle
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        // TODO: Add properties here
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}