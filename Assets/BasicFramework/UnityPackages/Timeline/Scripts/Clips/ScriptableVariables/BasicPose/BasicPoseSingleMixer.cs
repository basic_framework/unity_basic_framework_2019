﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class BasicPoseSingleMixer : ScriptableSingleMixerBase
    <
	    BasicPoseSingleClip, 
	    BasicPoseSinglePlayable, 
	    BasicPoseSingleTrack, 
	    BasicPose, 
	    BasicPoseSingle
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
        //***** overrides *****\\
        
        protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, MixerData<BasicPoseSingleClip, BasicPoseSinglePlayable, BasicPoseSingleTrack> mixerData)
        {
	        base.ProcessMixerData(mixerData, mixerData.TrackAsset.SingleTrackData);
        }

        protected override BasicPose Lerp(BasicPose start, BasicPose end, NormalizedFloat weight)
        {
	        var lerpPosition = Vector3.Lerp(start.Position, end.Position, weight.Value);
	        
	        //var startRotation = start.Rotation.IsValid() ?
//	        Debug.Log($"Start Rotation: {start.Rotation}, End Rotation: {end.Rotation}");
	        
	        var lerpRotation = Quaternion.Lerp(start.Rotation, end.Rotation, weight.Value);
	        return new BasicPose(lerpPosition, lerpRotation);
        }
        
    }
}