﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class BasicPoseTrackData : ScriptableSingleTrackData<BasicPose, BasicPoseSingle>{}

    [TrackClipType(typeof(BasicPoseSingleClip))]
    public class BasicPoseSingleTrack : ScriptableSingleTrackBase
    <
        BasicPoseSingleClip, 
        BasicPoseSinglePlayable, 
        BasicPoseSingleMixer,
        BasicPose,
        BasicPoseSingle,
        BasicPoseTrackData
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        // TODO: Add properties here
	
        
        // ****************************************  METHODS  ****************************************\\

	
    }
    
}