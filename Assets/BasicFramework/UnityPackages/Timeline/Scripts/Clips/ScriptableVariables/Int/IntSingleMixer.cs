﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class IntSingleMixer : ScriptableSingleMixerBase
    <
        IntSingleClip, 
        IntSinglePlayable, 
        IntSingleTrack, 
        int, 
        IntSingle
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        
        //***** overrides *****\\
        
        protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, 
            MixerData<IntSingleClip, IntSinglePlayable, IntSingleTrack> mixerData)
        {
            base.ProcessMixerData(mixerData, mixerData.TrackAsset.SingleTrackData);
        }

        protected override int Lerp(int start, int end, NormalizedFloat weight)
        {
            return Mathf.FloorToInt(weight.MapToRange(start, end));
        }
        
    }
}