﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class IntTrackData : ScriptableSingleTrackData<int, IntSingle>{}

    [TrackClipType(typeof(IntSingleClip))]
    public class IntSingleTrack : ScriptableSingleTrackBase
    <
        IntSingleClip, 
        IntSinglePlayable, 
        IntSingleMixer,
        int,
        IntSingle,
        IntTrackData
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        // TODO: Add properties here
	
        
        // ****************************************  METHODS  ****************************************\\

	
    }
    
}