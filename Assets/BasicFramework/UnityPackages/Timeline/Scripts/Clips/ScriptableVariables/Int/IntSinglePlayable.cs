﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	
    [Serializable]
    public class IntSinglePlayable : ScriptableSinglePlayableBase
    <
        IntSingleClip, 
        int, 
        IntSingle
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        // TODO: Add properties here
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
}