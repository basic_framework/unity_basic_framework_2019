﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
    [CustomPropertyDrawer(typeof(IntSinglePlayable))]
    public class IntSinglePlayablePropertyDrawer : ScriptableSinglePlayableBasePropertyDrawer
    <
        IntSingleClip, 
        int, 
        IntSingle, 
        IntSinglePlayable
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}