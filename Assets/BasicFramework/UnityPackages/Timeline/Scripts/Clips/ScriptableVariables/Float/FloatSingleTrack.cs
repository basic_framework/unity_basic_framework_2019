﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class FloatTrackData : ScriptableSingleTrackData<float, FloatSingle>{}

    [TrackClipType(typeof(FloatSingleClip))]
    public class FloatSingleTrack : ScriptableSingleTrackBase
    <
        FloatSingleClip, 
        FloatSinglePlayable, 
        FloatSingleMixer,
        float,
        FloatSingle,
        FloatTrackData
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        // TODO: Add properties here
	
        
        // ****************************************  METHODS  ****************************************\\

	
    }
    
}