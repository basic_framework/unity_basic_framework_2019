﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class FloatSingleMixer : ScriptableSingleMixerBase
    <
        FloatSingleClip, 
        FloatSinglePlayable, 
        FloatSingleTrack, 
        float, 
        FloatSingle
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        
        //***** overrides *****\\
        
        protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, 
            MixerData<FloatSingleClip, FloatSinglePlayable, FloatSingleTrack> mixerData)
        {
            base.ProcessMixerData(mixerData, mixerData.TrackAsset.SingleTrackData);
        }

        protected override float Lerp(float start, float end, NormalizedFloat weight)
        {
            return weight.MapToRange(start, end);
        }
        
    }
}