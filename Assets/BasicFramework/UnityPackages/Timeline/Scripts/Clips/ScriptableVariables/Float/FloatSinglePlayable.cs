﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	
	[Serializable]
    public class FloatSinglePlayable : ScriptableSinglePlayableBase
    <
        FloatSingleClip, 
        float, 
        FloatSingle
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        // TODO: Add properties here
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
    
}