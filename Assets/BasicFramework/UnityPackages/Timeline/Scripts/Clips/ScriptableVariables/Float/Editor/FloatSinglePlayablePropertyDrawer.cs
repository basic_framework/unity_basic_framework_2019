﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
    [CustomPropertyDrawer(typeof(FloatSinglePlayable))]
    public class FloatSinglePlayablePropertyDrawer : ScriptableSinglePlayableBasePropertyDrawer
    <
        FloatSingleClip, 
        float, 
        FloatSingle, 
        FloatSinglePlayable
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}