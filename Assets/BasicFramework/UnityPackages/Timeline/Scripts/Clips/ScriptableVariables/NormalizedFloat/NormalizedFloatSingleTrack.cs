﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
    [Serializable]
    public class NormalizedFloatTrackData : ScriptableSingleTrackData<NormalizedFloat, NormalizedFloatSingle>{}

    [TrackClipType(typeof(NormalizedFloatSingleClip))]
    public class NormalizedFloatSingleTrack : ScriptableSingleTrackBase
    <
        NormalizedFloatSingleClip, 
        NormalizedFloatSinglePlayable, 
        NormalizedFloatSingleMixer,
        NormalizedFloat,
        NormalizedFloatSingle,
        NormalizedFloatTrackData
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
        
        // TODO: Add properties here
	
        
        // ****************************************  METHODS  ****************************************\\

	
    }
    
}