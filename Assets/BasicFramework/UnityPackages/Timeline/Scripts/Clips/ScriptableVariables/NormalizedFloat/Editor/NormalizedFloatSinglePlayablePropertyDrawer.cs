﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
    [CustomPropertyDrawer(typeof(NormalizedFloatSinglePlayable))]
    public class NormalizedFloatSinglePlayablePropertyDrawer : ScriptableSinglePlayableBasePropertyDrawer
    <
        NormalizedFloatSingleClip, 
        NormalizedFloat, 
        NormalizedFloatSingle, 
        NormalizedFloatSinglePlayable
    > 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}