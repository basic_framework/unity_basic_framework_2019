﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine.Playables;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	[Serializable]
	public class NormalizedFloatSingleMixer : ScriptableSingleMixerBase
	<
		NormalizedFloatSingleClip, 
		NormalizedFloatSinglePlayable, 
		NormalizedFloatSingleTrack, 
		NormalizedFloat, 
		NormalizedFloatSingle
	> 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		
		//***** overrides *****\\

		protected override void OnProcessFrame(Playable playable, FrameData info, object playerData, 
			MixerData<NormalizedFloatSingleClip, NormalizedFloatSinglePlayable, NormalizedFloatSingleTrack> mixerData)
		{
			base.ProcessMixerData(mixerData, mixerData.TrackAsset.SingleTrackData);
		}

		protected override NormalizedFloat Lerp(NormalizedFloat start, NormalizedFloat end, NormalizedFloat weight)
		{
			return new NormalizedFloat(weight.MapToRange(start.Value, end.Value));
		}
		
	}
}