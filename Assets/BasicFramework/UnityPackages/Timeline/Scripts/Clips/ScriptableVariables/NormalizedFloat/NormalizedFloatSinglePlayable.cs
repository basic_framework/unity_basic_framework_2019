﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{

	[Serializable]
    public class NormalizedFloatSinglePlayable : ScriptableSinglePlayableBase
    <
        NormalizedFloatSingleClip, 
        NormalizedFloat, 
        NormalizedFloatSingle
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        // TODO: Add properties here
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}