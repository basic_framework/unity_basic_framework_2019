﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	[Serializable]
	public class ScriptableSinglePlayableBase<TClip, T, TScriptableSingle> : BasicPlayableBase<TClip>
		where TClip : BasicClipBase
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private bool _useSingleValue = default;
		[SerializeField] private TScriptableSingle _singleValue = default;
		[SerializeField] private T _value = default;
		
		
		public T Value => _useSingleValue ? _singleValue.Value : _value;
		

		// ****************************************  METHODS  ****************************************\\


	}
}