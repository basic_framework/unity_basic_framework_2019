﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	public abstract class ScriptableSingleTrackBase<TClip, TPlayable, TMixer, T, TScriptableSingle, TTrackData>
		: BasicTrackBase<TClip, TPlayable, TMixer>
		where TClip : BasicClipBase<TPlayable>
		where TPlayable : BasicPlayableBase<TClip>, new()
		where TMixer : BasicMixerBase, new()
		where TScriptableSingle : ScriptableSingleBase<T>
		where TTrackData : ScriptableSingleTrackData<T, TScriptableSingle>
	{

	// **************************************** VARIABLES ****************************************\\

	[SerializeField] private TTrackData _singleTrackData = default;
	
	
	public TTrackData SingleTrackData => _singleTrackData;
	

	// ****************************************  METHODS  ****************************************\\


	}

}