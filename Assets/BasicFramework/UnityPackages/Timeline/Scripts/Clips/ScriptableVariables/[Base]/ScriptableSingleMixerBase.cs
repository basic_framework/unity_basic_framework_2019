﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	public abstract class ScriptableSingleMixerBase<TClip, TPlayable, TTrack, T, TScriptableSingle> 
		: BasicMixerBase<TClip, TPlayable, TTrack>
		where TClip : BasicClipBase
		where TPlayable : ScriptableSinglePlayableBase<TClip, T, TScriptableSingle>, new()
		where TTrack : BasicTrackBase, new()
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		
		// ****************************************  METHODS  ****************************************\\

		//***** abstract *****\\

		protected abstract T Lerp(T start, T end, NormalizedFloat weight);
		
		
		//***** functions *****\\
		
		protected void ProcessMixerData(
			MixerData<TClip, TPlayable, TTrack> mixerData, 
			ScriptableSingleTrackData<T, TScriptableSingle> trackData)
		{
			var weight = NormalizedFloat.Zero;
			T start;
			T end;
			
			var trackAsset = mixerData.TrackAsset;
			
			var playableIn = mixerData.PlayableIn;
			var playableFull = mixerData.PlayablePlayingFull;
			var playableOut = mixerData.PlayableOut;
			
			T trackValue = default;
			
			
			if (mixerData.MixingWithTrack)
			{
				switch (trackData.TrackMixMode)
				{
					case TrackMixModes.MixWithTrack:
						
						trackValue = trackData.Value;
						break;
				
					case TrackMixModes.LastPlayedMixed:
					
						var lastFullyWeight = mixerData.GetLastFullyWeightedPlayable();
						trackValue = lastFullyWeight != null 
							? lastFullyWeight.Value 
							: playableIn != null 
								? playableIn.Value 
								: trackData.ScriptableSingle.Value;
						break;
				
					case TrackMixModes.LastPlayed:
					
						var lastPlayed = mixerData.GetLastPlayedPlayable();
					
						trackValue = lastPlayed != null 
							? lastPlayed.Value 
							: trackData.ScriptableSingle.Value;
						break;

				
					case TrackMixModes.DoNothing:
						
						if (playableFull != null)
						{
							trackValue = playableFull.Value;
						}
						else if (playableIn != null)
						{
							trackValue = playableIn.Value;
						}
						else if (playableOut != null)
						{
							trackValue = playableOut.Value;
						}
						else
						{
							trackValue = trackData.ScriptableSingle.Value;
						}
						break;
				
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			
			switch (mixerData.MixerClipState)
			{
				case MixerClipStates.FullTrack:
					start = end = trackValue;
					break;
				
				case MixerClipStates.TrackToClip:
					start = trackValue;
					end = playableIn.Value;
					weight = new NormalizedFloat(playableIn.Clip.Weight);
					break;
				
				case MixerClipStates.ClipToTrack:
					start = playableOut.Value;
					end = trackValue;
					weight = new NormalizedFloat(trackAsset.Weight);
					break;
				
				case MixerClipStates.ClipToClip:
					start = playableOut.Value;
					end = playableIn.Value;
					weight = new NormalizedFloat(playableIn.Clip.Weight);
					break;
				
				case MixerClipStates.FullClip:
					start = end = playableFull.Value;
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			//return Lerp(start, end, weight);
			if (!Application.isPlaying && trackData.ScriptableSingle == null)
			{
				Debug.LogError(
					$"Timeline ({trackAsset.timelineAsset.name}) " +
				               $"track ({trackAsset.name}) " +
				               $"missing reference to a \"{typeof(TScriptableSingle).Name}\"");
				return;
			}
			
			trackData.ScriptableSingle.Value = Lerp(start, end, weight);
		}
		

	}
}