﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
	public abstract class ScriptableSinglePlayableBasePropertyDrawer<TClip, T, TScriptableSingle, TSinglePlayable> 
		: BasicBasePropertyDrawer
		where TClip : BasicClipBase
		where TScriptableSingle : ScriptableSingleBase<T>
		where TSinglePlayable : ScriptableSinglePlayableBase<TClip, T, TScriptableSingle>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var useSingleValue = property.FindPropertyRelative("_useSingleValue");
			var singleValue = property.FindPropertyRelative("_singleValue");
			var value = property.FindPropertyRelative("_value");
			
			TempRect = position;
			
			
			TempRect.height = GetPropertyHeight(useSingleValue, true);
			EditorGUI.PropertyField(TempRect, useSingleValue, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			
			GUI.enabled = PrevGuiEnabled && useSingleValue.boolValue;
			TempRect.height = GetPropertyHeight(singleValue, true);
			if (useSingleValue.boolValue)
				BasicEditorGui.PropertyFieldNotNull(TempRect, singleValue, true);
			else
				BasicEditorGui.PropertyFieldCanBeNull(TempRect, singleValue, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			
			GUI.enabled = PrevGuiEnabled && !useSingleValue.boolValue;
			TempRect.height = GetPropertyHeight(value, true);
			EditorGUI.PropertyField(TempRect, value, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			GUI.enabled = PrevGuiEnabled;
			
//			TempRect.height = GetPropertyHeight(property, true);
//			EditorGUI.PropertyField(TempRect, property, true);
//			TempRect.y += BUFFER_HEIGHT + TempRect.height;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var useSingleValue = property.FindPropertyRelative("_useSingleValue");
			var singleValue = property.FindPropertyRelative("_singleValue");
			var value = property.FindPropertyRelative("_value");
			
			var height = GetPropertyHeight(useSingleValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(singleValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(value, true);

//			height +=  BUFFER_HEIGHT + GetPropertyHeight(property, true);
			
			return height;
		}
	}
}