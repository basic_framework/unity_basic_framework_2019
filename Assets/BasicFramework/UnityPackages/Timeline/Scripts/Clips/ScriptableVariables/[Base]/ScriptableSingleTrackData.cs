﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables
{
	[Serializable]
	public abstract class ScriptableSingleTrackData{}
	
	[Serializable]
	public abstract class ScriptableSingleTrackData<T, TScriptableSingle> : ScriptableSingleTrackData
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The Scriptable Single whose value will be set by the timeline")]
		[SerializeField] private TScriptableSingle _scriptableSingle = default;

		[Tooltip("Mix With Track => Mixes with the track value" +
		         "\nLast Played Mixed => Set's the value to the last completed clip, mixes in to the new clip using this value" +
		         "\nLast Played => Set's the value to the last played clip (either completed or currently playing, NO MIXING)" +
		         "\nDo Nothing => Does not change the value when mixing with the track")]
		[SerializeField] private TrackMixModes _trackMixMode = TrackMixModes.MixWithTrack;
		
		
		[SerializeField] private bool _useSingleValue = default;
		[SerializeField] private TScriptableSingle _singleValue = default;
		[SerializeField] private T _value = default;
		
		
		public TScriptableSingle ScriptableSingle => _scriptableSingle;
		
		public TrackMixModes TrackMixMode => _trackMixMode;
		
		public T Value => _useSingleValue ? _singleValue.Value : _value;

		
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}