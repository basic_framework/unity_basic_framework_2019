﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline.ScriptableVariables.Editor
{
	[CustomPropertyDrawer(typeof(ScriptableSingleTrackData), true)]
	public class ScriptableSingleTrackDataPropertyDrawer : BasicBasePropertyDrawer 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var scriptableSingle = property.FindPropertyRelative("_scriptableSingle");
			var trackMixMode = property.FindPropertyRelative("_trackMixMode");

			var useSingleValue = property.FindPropertyRelative("_useSingleValue");
			var singleValue = property.FindPropertyRelative("_singleValue");
			var value = property.FindPropertyRelative("_value");
			
			TempRect = position;
			
			TempRect.height = GetPropertyHeight(scriptableSingle, true);
			BasicEditorGui.PropertyFieldNotNull(
				TempRect, 
				scriptableSingle, 
				GetGuiContent(
					scriptableSingle.GetPropertyTypeName().CamelCaseToHumanReadable(), 
					scriptableSingle.tooltip), true);
			
			//BasicEditorGui.PropertyFieldNotNull(TempRect, scriptableSingle);
			TempRect.y += BUFFER_HEIGHT * 3 + TempRect.height;
			
			TempRect.height = GetPropertyHeight(trackMixMode, true);
			EditorGUI.PropertyField(TempRect, trackMixMode, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			var trackMixModeEnum =
				EnumUtility.EnumNameToEnumValue<TrackMixModes>(
					trackMixMode.enumNames[trackMixMode.enumValueIndex]);

			var valueEnabled = trackMixModeEnum == TrackMixModes.MixWithTrack;

			GUI.enabled = PrevGuiEnabled && valueEnabled;
			TempRect.height = GetPropertyHeight(useSingleValue, true);
			EditorGUI.PropertyField(TempRect, useSingleValue, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			GUI.enabled = PrevGuiEnabled && valueEnabled && useSingleValue.boolValue;
			TempRect.height = GetPropertyHeight(singleValue, true);
			if (useSingleValue.boolValue)
			{
				BasicEditorGui.PropertyFieldNotNull(TempRect, singleValue, true);
			}
			else
			{
				BasicEditorGui.PropertyFieldCanBeNull(TempRect, singleValue, true);
			}
//			EditorGUI.PropertyField(TempRect, singleValue);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;
			
			GUI.enabled = PrevGuiEnabled && valueEnabled && !useSingleValue.boolValue;
			TempRect.height = GetPropertyHeight(value, true);
			EditorGUI.PropertyField(TempRect, value, true);
			TempRect.y += BUFFER_HEIGHT + TempRect.height;

			GUI.enabled = PrevGuiEnabled;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var scriptableSingle = property.FindPropertyRelative("_scriptableSingle");
			var trackMixMode = property.FindPropertyRelative("_trackMixMode");
			
			var useSingleValue = property.FindPropertyRelative("_useSingleValue");
			var singleValue = property.FindPropertyRelative("_singleValue");
			var value = property.FindPropertyRelative("_value");

			var height = GetPropertyHeight(scriptableSingle, true);

			height += BUFFER_HEIGHT * 2;
			height += BUFFER_HEIGHT + GetPropertyHeight(trackMixMode, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(useSingleValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(singleValue, true);
			height += BUFFER_HEIGHT + GetPropertyHeight(value, true);
			
			return height;
		}
		
	}
}