﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class LightMixer : BasicMixerBase<LightClip, LightPlayable, LightTrack, Light>
	{
	
		// **************************************** VARIABLES ****************************************\\
		

		// ****************************************  METHODS  ****************************************\\

		protected override void OnProcessFrame(Playable playable, FrameData info, MixerData<LightClip, LightPlayable, LightTrack, Light> mixerData)
		{
			
			var intensity = 0f;
			var color = Color.black;
			
			var binding = mixerData.Binding;
			var trackAsset = mixerData.TrackAsset;
			
			var trackWeight = trackAsset.Weight;
			
			intensity += trackAsset.Intensity * trackWeight;
			color += trackAsset.Color * trackWeight;

			if (!trackAsset.IsAnyClipPlaying)
			{
				binding.intensity = intensity;
				binding.color = color;
				return;
			}
			
			// Determine the output values given the inputs
			foreach (var input in mixerData.Playables)
			{
				var basicClip = input.Clip;
				
				var inputWeight = basicClip.Weight;

				if (inputWeight <= 0) continue;
				//Debug.Log($"Start: {clipData.Start}, End: {clipData.End}, Time: {clipData.TimeThroughClip}");
				intensity += input.Intensity * inputWeight;
				color += input.Color * inputWeight;
			}
			
			//assign the result to the bound object
			binding.intensity = intensity;
			binding.color = color;
		}
		
//		protected override void OnProcessFrame(Playable playable, FrameData info, Light binding, IEnumerable<LightPlayable> inputs)
//		{
//			var intensity = 0f;
//			var color = Color.black;
//
//			var trackWeight = TrackAsset.Weight;
//			
//			intensity += TrackAsset.Intensity * trackWeight;
//			color += TrackAsset.Color * trackWeight;
//
//			if (!TrackAsset.IsAnyClipPlaying)
//			{
//				binding.intensity = intensity;
//				binding.color = color;
//				return;
//			}
//			
//			// Determine the output values given the inputs
//			foreach (var input in inputs)
//			{
//				var basicClip = input.Clip;
//				
//				var inputWeight = basicClip.Weight;
//
//				if (inputWeight <= 0) continue;
//				//Debug.Log($"Start: {clipData.Start}, End: {clipData.End}, Time: {clipData.TimeThroughClip}");
//				intensity += input.Intensity * inputWeight;
//				color += input.Color * inputWeight;
//			}
//			
//			//assign the result to the bound object
//			binding.intensity = intensity;
//			binding.color = color;
//		}
		
	}
}