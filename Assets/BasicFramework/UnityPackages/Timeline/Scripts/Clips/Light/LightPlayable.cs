﻿using System;
using UnityEngine;

namespace BasicFramework.UnityPackages.Timeline
{
	[Serializable]
	public class LightPlayable : BasicPlayableBase<LightClip>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Color _color = Color.white;
		
		[SerializeField] private float _intensity = 1f;
		

		public Color Color => _color;
		
		public float Intensity => _intensity;


		// ****************************************  METHODS  ****************************************\\


	}
}

