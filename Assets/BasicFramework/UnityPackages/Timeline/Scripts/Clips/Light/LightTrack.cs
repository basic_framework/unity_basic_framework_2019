﻿using UnityEngine;
using UnityEngine.Timeline;

namespace BasicFramework.UnityPackages.Timeline
{
	
	[TrackClipType(typeof(LightClip))]
	[TrackBindingType(typeof(Light))]
	public class LightTrack : BasicTrackBase<LightClip, LightPlayable, LightMixer>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private Color _color = default;
		[SerializeField] private float _intensity = 1f;
		
		public Color Color => _color;
		public float Intensity => _intensity;


		// ****************************************  METHODS  ****************************************\\
		
		
	}
	
}