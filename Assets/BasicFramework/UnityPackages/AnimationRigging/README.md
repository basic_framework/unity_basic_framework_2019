#Animation Rigging (Basic Framework Integration)

__Note: Only add this package if you have the _Animation Rigging_ package installed.
You can install it from the PackageManager window. 
If you do not have it installed you will get errors.
Delete the root folder to remove the errors.__

This plugin integrates Basic Frameworks Core systems with the Animation Rigging package.


##Humanoid Bones

### Head
- Neck (Optional)
- Head
- Left Eye (Optional)
- Right Eye (Optional)
- Jaw (Optional)

### Body
- Hips
- Spine
- Chest (Optional)
- Upper Chest (Optional)

### Arms (Left and Right)
- Shoulder (Optional)
- Upper Arm
- Lower Arm
- Hand

### Hands (Left and Right) - Completely Options
- Proximal (Thumb, Index, Middle, Ring, Little)
- Intermedial (Thumb, Index, Middle, Ring, Little)
- Distal (Thumb, Index, Middle, Ring, Little)

### Legs (Left and Right)
- Upper Leg
- Lower Leg
- Foot
- Toes (Optional)



##Setup Animation Rig - Control

- Set rig to Humanoid
- Configure bones (including optional bones such as chest, upper chest, neck, eyes, jaw)
- Add Rig Builder component to root (same gameobject as Animator)
- (Optional) Add bone renderers 
- Add animation_rig_control prefab


- Align overrides to associated bones (hips, spine, chest, Upper Chest, Neck, Head)
__Note: If missing optional bones align them to the last bone that is available, e.g. Upper Chest align to Chest__
- Connect overrides to their associated bones (where available)
- Align targets (pelvis => hips, spine, upper chest, head)


- Connect arm rigs (Root => Upper Arm, Mid => Lower Arm, Tip => Hand)
- Align arm ik targets to hand
- Arm hint reference Lower Arm, offset to be back from bone and a bit lower


- Connect leg rigs (Root => Upper Leg, Mid => Lower Leg, Tip => Foot)
- Align leg ik targets to foot
- Leg hint reference Lower Leg, offset to be in front of bone





