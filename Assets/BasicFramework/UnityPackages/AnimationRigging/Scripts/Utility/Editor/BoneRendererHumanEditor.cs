﻿using System.Collections.Generic;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility.Editor
{
	[CustomEditor(typeof(BoneRendererHuman))]
	public class BoneRendererHumanEditor : BasicBaseEditor<BoneRendererHuman>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var boneRenderer = serializedObject.FindProperty("_boneRenderer");
			var animator = serializedObject.FindProperty("_animator");

			var animatorObject = animator.objectReferenceValue as Animator;
			var isHuman = animatorObject != null && animatorObject.isHuman;
			
			EditorGUILayout.LabelField("Is Human", isHuman.ToString());
			
			GUI.enabled = false;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(boneRenderer);
			
			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var animator = serializedObject.FindProperty("_animator");
			var boneRenderer = serializedObject.FindProperty("_boneRenderer");
			var sectionsToRender = serializedObject.FindProperty("_sectionsToRender");
			
			Space();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(animator);
			EditorGUILayout.PropertyField(sectionsToRender);

			var sections = (HumanBoneSections) sectionsToRender.intValue;

			var boneRendererObject = boneRenderer.objectReferenceValue as BoneRenderer;
			
			var animatorObject = animator.objectReferenceValue as Animator;

			var isHuman = animatorObject != null && animatorObject.isHuman;

			GUI.enabled = PrevGuiEnabled && isHuman && boneRendererObject != null;

			if (GUILayout.Button("Update Bones"))
			{
				var boneList = new List<Transform>();

				var hasUpperChest = animatorObject.GetBoneTransform(HumanBodyBones.UpperChest) != null;
				var hasChest = animatorObject.GetBoneTransform(HumanBodyBones.Chest) != null;
				
				if ((sections & HumanBoneSections.Head) == HumanBoneSections.Head)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Neck));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Head));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftEye));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightEye));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Jaw));
				}
				
				if ((sections & HumanBoneSections.Spine) == HumanBoneSections.Spine)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Hips));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Spine));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Chest));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.UpperChest));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Neck));
				}
				
				if ((sections & HumanBoneSections.LeftArm) == HumanBoneSections.LeftArm)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftHand));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftLowerArm));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftUpperArm));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftShoulder));

					if (hasUpperChest)
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.UpperChest));
					else if (hasChest)
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Chest));
					else
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Spine));
				}
				
				if ((sections & HumanBoneSections.RightArm) == HumanBoneSections.RightArm)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightHand));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightLowerArm));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightUpperArm));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightShoulder));

					if (hasUpperChest)
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.UpperChest));
					else if (hasChest)
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Chest));
					else
						boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Spine));
				}
				
				if ((sections & HumanBoneSections.LeftHand) == HumanBoneSections.LeftHand)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftHand));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftThumbProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftThumbIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftThumbDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftIndexProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftIndexIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftIndexDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftMiddleProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftMiddleIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftMiddleDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftRingProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftRingIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftRingDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftLittleProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftLittleIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftLittleDistal));
				}
				
				if ((sections & HumanBoneSections.RightHand) == HumanBoneSections.RightHand)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightHand));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightThumbProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightThumbIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightThumbDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightIndexProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightIndexIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightIndexDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightMiddleProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightMiddleIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightMiddleDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightRingProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightRingIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightRingDistal));
					
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightLittleProximal));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightLittleIntermediate));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightLittleDistal));
				}
				
				if ((sections & HumanBoneSections.LeftLeg) == HumanBoneSections.LeftLeg)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Hips));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftUpperLeg));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftLowerLeg));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftFoot));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.LeftToes));
				}
				
				if ((sections & HumanBoneSections.RightLeg) == HumanBoneSections.RightLeg)
				{
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.Hips));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightUpperLeg));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightLowerLeg));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightFoot));
					boneList.Add(animatorObject.GetBoneTransform(HumanBodyBones.RightToes));
				}
				
				boneList.RemoveDuplicates();
				boneList.RemoveNulls();

				Undo.RecordObject(boneRendererObject, "Update bones");
				
				boneRendererObject.transforms = boneList.ToArray();
			}
			
			GUI.enabled = PrevGuiEnabled;
		}
	}
}