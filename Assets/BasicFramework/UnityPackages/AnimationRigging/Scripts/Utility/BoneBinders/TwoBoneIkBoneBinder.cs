﻿using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility
{
	[AddComponentMenu("Basic Framework/Unity Packages/Animation Rigging/Utility/Bone Binder/Two Bone IK Bone Binder")]
	public class TwoBoneIkBoneBinder : HumanBoneBinderBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private TwoBoneIKConstraint _twoBoneIkConstraint = default;

		[SerializeField] private HumanBodyBones _boneRoot = default;
		[SerializeField] private HumanBodyBones _boneMid = default;
		[SerializeField] private HumanBodyBones _boneTip = default;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void Reset()
		{
			base.Reset();

			_twoBoneIkConstraint = GetComponent<TwoBoneIKConstraint>();
		}

		protected override void BindBones()
		{
			if (_twoBoneIkConstraint == null) return;

			var data = _twoBoneIkConstraint.data;

			data.root = Animator.GetBoneTransform(_boneRoot);
			data.mid = Animator.GetBoneTransform(_boneMid);
			data.tip = Animator.GetBoneTransform(_boneTip);

			_twoBoneIkConstraint.data = data;
		}
		
	}
}