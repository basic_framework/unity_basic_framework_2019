﻿using UnityEngine;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility
{
    [ExecuteAlways]
    public abstract class HumanBoneBinderBase : MonoBehaviour 
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private Animator _animator = default;
		

        protected Animator Animator => _animator;
		

        public bool IsHuman => _animator != null && _animator.isHuman;
		

        // ****************************************  METHODS  ****************************************\\

		
        //***** abstract *****\\

        protected abstract void BindBones();
		
		
        //***** mono *****\\
		
        protected virtual void Reset()
        {
            _animator = GetComponentInParent<Animator>();
        }

        protected virtual void Awake()
        {
            if (_animator == null)
            {
                _animator = GetComponentInParent<Animator>();
            }

            if (!IsHuman) return;
            BindBones();

            if (!Application.isPlaying) return;
            Destroy(this);
        }

        protected virtual void Update()
        {
            if (Application.isPlaying) return;
			
            if (_animator == null)
            {
                _animator = GetComponentInParent<Animator>();
            }
			
            if (!IsHuman) return;
            BindBones();
        }
		
    }
}