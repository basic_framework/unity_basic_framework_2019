﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility.Editor
{
	public abstract class HumanBoneBinderBaseEditor<T> : BasicBaseEditor<T>
		where T : HumanBoneBinderBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var animator = serializedObject.FindProperty("_animator");

			var animatorObject = animator.objectReferenceValue as Animator;
			var isHuman = animatorObject != null && animatorObject.isHuman;
			
			EditorGUILayout.LabelField("Is Human", isHuman.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var animator = serializedObject.FindProperty("_animator");
			
			Space();
			SectionHeader($"{nameof(HumanBoneBinderBase).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(animator);
		}
		
	}
}