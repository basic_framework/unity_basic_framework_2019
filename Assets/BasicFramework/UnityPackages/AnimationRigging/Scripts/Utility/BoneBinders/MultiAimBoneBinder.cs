﻿using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility
{
	[AddComponentMenu("Basic Framework/Unity Packages/Animation Rigging/Utility/Bone Binder/Multi-Aim Bone Binder")]
	public class MultiAimBoneBinder : HumanBoneBinderBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private MultiAimConstraint _multiAimConstraint = default;

		[SerializeField] private HumanBodyBones _boneConstrained = default;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void Reset()
		{
			base.Reset();

			_multiAimConstraint = GetComponent<MultiAimConstraint>();
		}

		protected override void BindBones()
		{
			if(_multiAimConstraint == null) return;
			
			var data = _multiAimConstraint.data;

			data.constrainedObject = Animator.GetBoneTransform(_boneConstrained);

			_multiAimConstraint.data = data;
		}
		
	}
}