﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility.Editor
{
	[CustomEditor(typeof(MultiAimBoneBinder))][CanEditMultipleObjects]
	public class MultiAimBoneBinderEditor : HumanBoneBinderBaseEditor<MultiAimBoneBinder>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			//base.DrawPropertyFields();
			
			base.DrawPropertyFields();
			
			var multiAimConstraint = serializedObject.FindProperty("_multiAimConstraint");
			var boneConstrained = serializedObject.FindProperty("_boneConstrained");
			
			Space();
			SectionHeader($"{nameof(MultiAimBoneBinder).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(multiAimConstraint);
			BasicEditorGuiLayout.PropertyFieldNotNull(boneConstrained);
			
			EditorGUILayout.BeginHorizontal();
			
			
//			if (GUILayout.Button("Spine"))
//			{
//				boneConstrained.enumValueIndex = (int) HumanBodyBones.Spine;
//			}
			
			if (GUILayout.Button("Left Eye"))
			{
				boneConstrained.enumValueIndex = (int) HumanBodyBones.LeftEye;
			}

			if (GUILayout.Button("Head"))
			{
				boneConstrained.enumValueIndex = (int) HumanBodyBones.Head;
			}
			
			if (GUILayout.Button("Right Eye"))
			{
				boneConstrained.enumValueIndex = (int) HumanBodyBones.RightEye;
			}
			
			
			
			EditorGUILayout.EndHorizontal();
		}
	}
}