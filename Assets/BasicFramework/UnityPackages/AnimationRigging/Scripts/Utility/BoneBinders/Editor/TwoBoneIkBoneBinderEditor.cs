﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility.Editor
{
	[CustomEditor(typeof(TwoBoneIkBoneBinder))][CanEditMultipleObjects]
	public class TwoBoneIkBoneBinderEditor : HumanBoneBinderBaseEditor<TwoBoneIkBoneBinder>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var twoBoneIkConstraint = serializedObject.FindProperty("_twoBoneIkConstraint");
			var boneRoot = serializedObject.FindProperty("_boneRoot");
			var boneMid = serializedObject.FindProperty("_boneMid");
			var boneTip = serializedObject.FindProperty("_boneTip");
			
			Space();
			SectionHeader($"{nameof(TwoBoneIkBoneBinder).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(twoBoneIkConstraint);

			Space();
			
			EditorGUILayout.BeginHorizontal();
			
			if (GUILayout.Button("Left Arm"))
			{
				boneRoot.enumValueIndex = (int) HumanBodyBones.LeftUpperArm;
				boneMid.enumValueIndex = (int) HumanBodyBones.LeftLowerArm;
				boneTip.enumValueIndex = (int) HumanBodyBones.LeftHand;
			}
			
			if (GUILayout.Button("Right Arm"))
			{
				boneRoot.enumValueIndex = (int) HumanBodyBones.RightUpperArm;
				boneMid.enumValueIndex = (int) HumanBodyBones.RightLowerArm;
				boneTip.enumValueIndex = (int) HumanBodyBones.RightHand;
			}
			
			if (GUILayout.Button("Left Leg"))
			{
				boneRoot.enumValueIndex = (int) HumanBodyBones.LeftUpperLeg;
				boneMid.enumValueIndex = (int) HumanBodyBones.LeftLowerLeg;
				boneTip.enumValueIndex = (int) HumanBodyBones.LeftFoot;
			}
			
			if (GUILayout.Button("Right Leg"))
			{
				boneRoot.enumValueIndex = (int) HumanBodyBones.RightUpperLeg;
				boneMid.enumValueIndex = (int) HumanBodyBones.RightLowerLeg;
				boneTip.enumValueIndex = (int) HumanBodyBones.RightFoot;
			}
			
			EditorGUILayout.EndHorizontal();
			
			Space();
			EditorGUILayout.PropertyField(boneRoot);
			EditorGUILayout.PropertyField(boneMid);
			EditorGUILayout.PropertyField(boneTip);
		}
	}
}