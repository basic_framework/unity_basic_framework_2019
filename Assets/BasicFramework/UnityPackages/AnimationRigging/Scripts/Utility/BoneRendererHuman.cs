﻿using BasicFramework.Core.Utility.Attributes;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace BasicFramework.UnityPackages.AnimationRigging.Utility
{
	[RequireComponent(typeof(BoneRenderer))]
	[AddComponentMenu("Basic Framework/Unity Packages/Animation Rigging/Utility/Bone Renderer Human")]
	public class BoneRendererHuman : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Animator _animator = default;
		
		[SerializeField] private BoneRenderer _boneRenderer = default;

		[EnumFlag("The bone sections to add and render")]
		[SerializeField] private HumanBoneSections _sectionsToRender = (HumanBoneSections) ~0;
		
		
		public Animator Animator => _animator;
		
		public BoneRenderer BoneRenderer => _boneRenderer;
		
		public HumanBoneSections SectionsToRender => _sectionsToRender;


		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_animator = GetComponentInParent<Animator>();
			_boneRenderer = GetComponent<BoneRenderer>();
		}

		private void OnValidate()
		{
			if (_boneRenderer != null) return;
			_boneRenderer = GetComponent<BoneRenderer>();
		}

	}
}