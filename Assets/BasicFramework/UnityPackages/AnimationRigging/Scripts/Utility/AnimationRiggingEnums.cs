﻿using System;
using UnityEngine;

namespace BasicFramework.UnityPackages.AnimationRigging
{
	[Flags]
	public enum HumanBoneSections
	{
		Head 		= 1 << 0,
		Spine		= 1 << 1,
		LeftArm		= 1 << 2,
		RightArm	= 1 << 3,
		LeftHand	= 1 << 4,
		RightHand	= 1 << 5,
		LeftLeg		= 1 << 6,
		RightLeg	= 1 << 7,
	}
	
	
}