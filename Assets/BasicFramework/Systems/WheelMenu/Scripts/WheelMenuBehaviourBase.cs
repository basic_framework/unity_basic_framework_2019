﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Systems.WheelMenu
{
	public abstract class WheelMenuBehaviourBase : MonoBehaviour 
	{
		
		// Make this base, get direction from child
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Prefab of the wheel menu options. Make sure it is appropriate for the options count")]
		[SerializeField] private WheelMenuOptionBehaviour _prefabOption = default;

		[Tooltip("The options parent rect transform")]
		[SerializeField] private RectTransform _rectTransformParentOptions = default;
		
		[Tooltip("The data for each of the wheel menu options")]
		[SerializeField] private WheelMenuOptionData[] _optionDatas = default;
		
		
		private readonly List<WheelMenuOptionBehaviour> _listOptions = new List<WheelMenuOptionBehaviour>();

		private int OptionCount => _listOptions.Count;

		private float _deltaAngle;
		private float _halfDeltaAngle;

		private bool _hoveredOptionNotNull;
		public bool HoveredOptionNotNull => _hoveredOptionNotNull;


		private WheelMenuOptionBehaviour _hoveredOption;
		public WheelMenuOptionBehaviour HoveredOption
		{
			get => _hoveredOption;
			private set
			{
				if (_hoveredOption == value) return;
				var valuePrevious = _hoveredOption;
				_hoveredOption = value;
				HoveredOptionUpdated(value, valuePrevious);
			}
		}

		private float _durationHovered;
		public float DurationHovered
		{
			get => _durationHovered;
			private set
			{
				if (Math.Abs(_durationHovered - value) < Mathf.Epsilon) return;
				_durationHovered = value;
				DurationHoveredUpdated(value);
			}
		}
		

		// ****************************************  METHODS  ****************************************\\

		//***** abstract *****\\
		
		protected abstract Vector2 GetPointingDirection();
		

		//***** mono *****\\

		private void Awake()
		{
			SetupWheelMenu();
		}

		protected virtual void Start()
		{
			HoveredOptionUpdated(_hoveredOption, null);
			DurationHoveredUpdated(_durationHovered);
		}

		protected virtual void Update()
		{
			var inputDirection = GetPointingDirection();
			HoveredOption = GetOptionFromDirection(inputDirection);

			if (!_hoveredOptionNotNull) return;

			DurationHovered += Time.deltaTime;
		}
		
		
		//***** property updates *****\\
		
		private void HoveredOptionUpdated(WheelMenuOptionBehaviour value, WheelMenuOptionBehaviour valuePrevious)
		{
			_hoveredOptionNotNull = value != null;
			DurationHovered = 0;
			
			if (valuePrevious != null)
			{
				valuePrevious.FillAmount = 0;
			}
			
			if (_hoveredOptionNotNull)
			{
				
			}
		}
		
		private void DurationHoveredUpdated(float value)
		{
			OnDurationHoveredUpdated(value);
		}
		
		
		//***** virtual *****\\
		
		protected virtual void OnDurationHoveredUpdated(float duration)
		{
			if(!_hoveredOptionNotNull) return;
			_hoveredOption.FillAmount = duration > 0 ? 1f : 0f;
		}
		

		//***** functions *****\\

		public void SelectOption()
		{
			if (!_hoveredOptionNotNull) return;
			_hoveredOption.OptionData.OnSelect.Invoke();
		}
		
		private void SetupWheelMenu()
		{
			foreach (Transform child in _rectTransformParentOptions)
			{
				var childOption = child.GetComponent<WheelMenuOptionBehaviour>();
				if(childOption == null) continue;
				Destroy(childOption.gameObject);
			}
			
			_listOptions.Clear();

			_deltaAngle = 360f / _optionDatas.Length;
			_halfDeltaAngle = _deltaAngle * 0.5f;

			for (int i = 0; i < _optionDatas.Length; i++)
			{
				var angle = i * _deltaAngle;
				var option = Instantiate(_prefabOption, _rectTransformParentOptions);
				option.RectTransform.localRotation = Quaternion.Euler(Vector3.forward * angle);
				option.OptionData = _optionDatas[i];
				
				_listOptions.Add(option);
			}
		}

		private WheelMenuOptionBehaviour GetOptionFromDirection(Vector2 direction)
		{
			if (direction.sqrMagnitude < Mathf.Epsilon) return null;

			var angle = Vector2.SignedAngle(Vector2.up, direction) + _halfDeltaAngle;
			angle = MathUtility.WrapValue(angle, 0, 360);

			var index = Mathf.FloorToInt(angle / _deltaAngle);
			var option = _listOptions[index];
			
//			Debug.DrawRay(_rectTransformParentOptions.position, Vector3.up, Color.green);
//			Debug.DrawRay(_rectTransformParentOptions.position, direction, Color.magenta);
//			Debug.Log($"Angle: {angle}, index {index}, option {option.OptionData.OptionText}");

			return option;
		}
	}
}