﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.Systems.WheelMenu
{
	public class WheelMenuBehaviourBasic : WheelMenuBehaviourBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("Hold in the direction of the option to activate it")]
		[SerializeField] private bool _holdToActivate = default;
		
		[Tooltip("How long you have to hold the option before it is activated")]
		[SerializeField] private float _durationHoverActivate = 1f;

		[Tooltip("Input for the direction the user is pointing on the wheel menu")]
		[SerializeField] private Axis2Input _inputDirection = default;

		[Tooltip("The action used to select the hovered option. " +
		         "Note: Can be null if using hold to activate")]
		[SerializeField] private ActionInput _inputSelect = default;
		
		private NormalizedFloat _progressHover;
		public NormalizedFloat ProgressHover
		{
			get => _progressHover;
			private set
			{
				if (_progressHover == value) return;
				_progressHover = value;
				ProgressHoverUpdated(value);
			}
		}
		
	
		// ****************************************  METHODS  ****************************************\\

		//***** implementation *****\\

		protected override Vector2 GetPointingDirection() => _inputDirection.Value;


		//***** mono *****\\

		protected override void Start()
		{
			base.Start();
			
			ProgressHoverUpdated(_progressHover);
		}

		protected override void Update()
		{
			base.Update();

			if (!HoveredOptionNotNull) return;

			if (!_holdToActivate && _inputSelect.Value)
			{
				SelectOption();
			}
			
		}


		//***** property updates *****\\
		
		private void ProgressHoverUpdated(NormalizedFloat value)
		{
			if (!HoveredOptionNotNull) return;
			HoveredOption.FillAmount = value.Value;
			
			if (!_holdToActivate || value != NormalizedFloat.One) return;
			SelectOption();
//			HoveredOption.OptionData.OnSelect.Invoke();
		}
		
		
		
		//***** overrides *****\\

		protected override void OnDurationHoveredUpdated(float duration)
		{
//			base.OnDurationHoveredUpdated(duration);
			
			var progress = duration > Mathf.Epsilon ? NormalizedFloat.One : NormalizedFloat.Zero;
			
			if (_holdToActivate && Math.Abs(_durationHoverActivate) > Mathf.Epsilon)
			{
				progress.Value = duration / _durationHoverActivate;
			}
			
			ProgressHover = progress;
		}
		
		
		
	}
}