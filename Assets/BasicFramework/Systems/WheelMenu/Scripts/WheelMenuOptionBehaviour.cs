﻿using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.Systems.WheelMenu
{
	public class WheelMenuOptionBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The icon image for this option")]
		[SerializeField] private Image _imageIcon = default;
		
		[Tooltip("The title text for this option")]
		[SerializeField] private Text _textTitle = default;
		
		[Tooltip("The image used to overlay/highlight the option")]
		[SerializeField] private Image _imageFill = default;
		
		[Tooltip("In case the fill doesn't go from 0 to 1")]
		[SerializeField] private FloatRange _rangeFill = new FloatRange(0.5f, 1);
		
		
		private RectTransform _rectTransform;
		public RectTransform RectTransform => _rectTransform;


		private WheelMenuOptionData _optionData = default;
		public WheelMenuOptionData OptionData
		{
			get => _optionData;
			set
			{
				if (_optionData == value) return;
				_optionData = value;
				OptionDataUpdated(value);
			}
		}

		public float FillAmount
		{
			get => _imageFill.fillAmount;
			set => _imageFill.fillAmount = _rangeFill.RangedToNormalizedValue(value);
		}
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Awake()
		{
			_rectTransform = transform as RectTransform;
			FillAmount = 0;
		}

		private void Start()
		{
			OptionDataUpdated(_optionData);
		}


		//***** property updated *****\\
		
		private void OptionDataUpdated(WheelMenuOptionData value)
		{
			if (_imageIcon != null && _imageIcon != null)
			{
				_imageIcon.sprite = value?.OptionIcon;
			}

			if (_textTitle != null && _textTitle != null)
			{
				_textTitle.text = value?.OptionText;
			}
		}
	}
}