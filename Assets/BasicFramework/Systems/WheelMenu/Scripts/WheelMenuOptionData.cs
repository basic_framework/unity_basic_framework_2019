﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Systems.WheelMenu
{
	[Serializable]
	public class WheelMenuOptionData 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("Menu option text")]
		[SerializeField] private string _optionText = default;
		
		[Tooltip("Menu option image")]
		[SerializeField] private Sprite _optionIcon = default;
		
		[Tooltip("Raised when the option is selected")]
		[SerializeField] private UnityEvent _onSelect = default;

		
		public Sprite OptionIcon => _optionIcon;

		public string OptionText => _optionText;

		public UnityEvent OnSelect => _onSelect;


		// ****************************************  METHODS  ****************************************\\


	}
}