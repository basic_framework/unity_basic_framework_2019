﻿using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Systems.WheelMenu.Editor
{
	[CustomEditor(typeof(WheelMenuOptionBehaviour))]
	public class WheelMenuOptionBehaviourEditor : BasicBaseEditor<WheelMenuOptionBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var imageIcon = serializedObject.FindProperty("_imageIcon");
			var textTitle = serializedObject.FindProperty("_textTitle");
			var imageFill = serializedObject.FindProperty("_imageFill");
			var rangeFill = serializedObject.FindProperty("_rangeFill");
			
			Space();
			SectionHeader($"{nameof(WheelMenuOptionBehaviour)} Properties");
			
			
			BasicEditorGuiLayout.PropertyFieldCanBeNull(imageIcon);
			BasicEditorGuiLayout.PropertyFieldCanBeNull(textTitle);
			BasicEditorGuiLayout.PropertyFieldNotNull(imageFill);
			EditorGUILayout.PropertyField(rangeFill);
		}
	}
}