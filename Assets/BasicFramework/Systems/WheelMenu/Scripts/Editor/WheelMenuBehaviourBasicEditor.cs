﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Systems.WheelMenu.Editor
{
	[CustomEditor(typeof(WheelMenuBehaviourBasic))]
	public class WheelMenuBehaviourBasicEditor : WheelMenuBehaviourBaseEditor<WheelMenuBehaviourBasic>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var holdToActivate = serializedObject.FindProperty("_holdToActivate");
			var durationHoverActivate = serializedObject.FindProperty("_durationHoverActivate");
			var inputDirection = serializedObject.FindProperty("_inputDirection");
			var inputSelect = serializedObject.FindProperty("_inputSelect");
			
			Space();
			SectionHeader($"{nameof(WheelMenuBehaviourBasic).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.PropertyField(holdToActivate, GetGuiContent(holdToActivate.displayName,
				"Activate option by holding for a duration | how long to hold"));

			GUI.enabled = PrevGuiEnabled && holdToActivate.boolValue;
			EditorGUILayout.PropertyField(durationHoverActivate, GUIContent.none);
			GUI.enabled = PrevGuiEnabled;

			EditorGUILayout.EndHorizontal();
			
			BasicEditorGuiLayout.PropertyFieldNotNull(inputDirection);

			if (holdToActivate.boolValue)
			{
				GUI.enabled = false;
				BasicEditorGuiLayout.PropertyFieldCanBeNull(inputSelect);
				GUI.enabled = PrevGuiEnabled;
			}
			else
			{
				BasicEditorGuiLayout.PropertyFieldNotNull(inputSelect);
			}
			
		}
	}
}