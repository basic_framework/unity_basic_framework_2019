﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.Systems.WheelMenu.Editor
{
	public abstract class WheelMenuBehaviourBaseEditor<T> : BasicBaseEditor<T>
		where T : WheelMenuBehaviourBase
	{
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var prefabOption = serializedObject.FindProperty("_prefabOption");
			var rectTransformParentOptions = serializedObject.FindProperty("_rectTransformParentOptions");
			var optionDatas = serializedObject.FindProperty("_optionDatas");
		
			Space();
			SectionHeader($"{nameof(WheelMenuBehaviourBase).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(prefabOption);
			BasicEditorGuiLayout.PropertyFieldNotNull(rectTransformParentOptions);
		
			Space();
			SectionHeader("Options");
			EditorGUILayout.HelpBox("The wheel menu options start from the top and goes counter clockwise. " +
			                        "\nE.g. if you have FOUR options:" +
			                        "\n\tElement 0 = TOP" +
			                        "\n\tElement 1 = Left" +
			                        "\n\tElement 2 = Bottom" +
			                        "\n\tElement 3 = Right", 
				MessageType.Info);
			var countOptions = optionDatas.arraySize;
			countOptions = EditorGUILayout.IntField("Count", countOptions);
			countOptions = Mathf.Max(2, countOptions);
			BasicEditorArrayUtility.SetArraySize(optionDatas, countOptions);

			for (int i = 0; i < countOptions; i++)
			{
				var optionData = optionDatas.GetArrayElementAtIndex(i);
				EditorGUILayout.PropertyField(optionData);
			}
		}
	}
}