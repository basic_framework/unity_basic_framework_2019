﻿using System;
using BasicFramework.Core.BasicAnimation.InverseKinematics;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicMath.Utility;
using RootMotion.FinalIK;
using UnityEngine;

namespace BasicFramework.ThirdParty.FinalIk
{
	[AddComponentMenu("Basic Framework/Third Party/Final IK/VR IK/Basic VR IK Behaviour")]
	public class BasicVrikBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		//TODO: Create different settings for each tracked part of the body, head, arms, legs
		
		private const float MIN_WEIGHT_CHANGE_DURATION = 0.001f;


		[Tooltip("The VRIK component to use as reference")]
		[SerializeField] private VRIK _vrik = default;

		[Tooltip("How long it takes to go from 0 to 1 weight or Vice-Versa")]
		[SerializeField] private float _weightChangeDuration = 1;

		[SerializeField] private EasingTypes _weightChangeEasingType = default;

		[Tooltip("The IK target for the head bone")]
		[SerializeField] private BasicIkTarget _basicIkTargetHead = default;
		
		[Tooltip("The IK target for the left hand bone")]
		[SerializeField] private BasicIkTarget _basicIkTargetHandLeft = default;
		
		[Tooltip("The IK target for the right hand bone")]
		[SerializeField] private BasicIkTarget _basicIkTargetHandRight = default;


		public VRIK Vrik => _vrik;

		public float WeightChangeDuration
		{
			get => _weightChangeDuration;
			set
			{
				
				value = Mathf.Max(MIN_WEIGHT_CHANGE_DURATION, _weightChangeDuration);
				if(Math.Abs(_weightChangeDuration - value) < Mathf.Epsilon) return;
				_weightChangeDuration = value;
				WeightChangeDurationUpdated(value);
			}
		}

		public BasicIkTarget BasicIkTargetHead => _basicIkTargetHead;

		public BasicIkTarget BasicIkTargetHandLeft => _basicIkTargetHandLeft;

		public BasicIkTarget BasicIkTargetHandRight => _basicIkTargetHandRight;

		
		private IKSolverVR.Spine _ikSolverSpine;
		public IKSolverVR.Spine IkSolverSpine => _ikSolverSpine;
		
		private IKSolverVR.Arm _ikSolverArmLeft;
		public IKSolverVR.Arm IkSolverArmLeft => _ikSolverArmLeft;
		
		private IKSolverVR.Arm _ikSolverArmRight;
		public IKSolverVR.Arm IkSolverArmRight => _ikSolverArmRight;
		
		
		private VrikTargetFlags _activeVrikTargets;
		public VrikTargetFlags ActiveVrikTargets => _activeVrikTargets;

		private float _rawWeightHead;
		private float _rawWeightHandLeft;
		private float _rawWeightHandRight;

		private float _weightChangeSpeed;


		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_vrik = transform.root.GetComponentInChildren<VRIK>();
		}

		private void OnValidate()
		{
			_weightChangeDuration = Mathf.Max(MIN_WEIGHT_CHANGE_DURATION, _weightChangeDuration);
			WeightChangeDurationUpdated(_weightChangeDuration);
		}

		private void Awake()
		{
			SetupIkSolvers();
		}

		private void OnEnable()
		{
			OnIkTargetIsTrackingChanged(_basicIkTargetHead);
			OnIkTargetIsTrackingChanged(_basicIkTargetHandLeft);
			OnIkTargetIsTrackingChanged(_basicIkTargetHandRight);
			
			ManageIkTargetSubscriptions(true);
		}
		
		private void OnDisable()
		{
			ManageIkTargetSubscriptions(false);
		}

		private void Start()
		{
			WeightChangeDurationUpdated(_weightChangeDuration);
		}


		private void Update()
		{
			_rawWeightHead 		= UpdateHead(_rawWeightHead);
			_rawWeightHandLeft 	= UpdateArm(_ikSolverArmLeft, VrikTargetFlags.HandLeft, _rawWeightHandLeft);
			_rawWeightHandRight = UpdateArm(_ikSolverArmRight, VrikTargetFlags.HandRight, _rawWeightHandRight);
		}
		

		//***** callbacks *****\\

		private void OnIkTargetIsTrackingChanged(BasicIkTarget value)
		{
			if (value == null) return;
			
			var isTracked = value.TargetActive;
			VrikTargetFlags flag = 0;
			
			if (value == _basicIkTargetHead)			flag = VrikTargetFlags.Head;
			else if (value == _basicIkTargetHandLeft)	flag = VrikTargetFlags.HandLeft;
			else if (value == _basicIkTargetHandRight)	flag = VrikTargetFlags.HandRight;
			
			if (isTracked)
				_activeVrikTargets |= flag;
			else
				_activeVrikTargets &= ~flag;
		}
		
		
		//***** property updates *****\\
		
		private void WeightChangeDurationUpdated(float value)
		{
			_weightChangeSpeed = 1 / value;
		}
		

		//***** functions *****\\
		
		
		//** setup **\\

		private void SetupIkSolvers()
		{
			
			// Head
			_ikSolverSpine = _vrik.solver.spine;
			_ikSolverSpine.positionWeight = 0;
			_ikSolverSpine.rotationWeight = 0;
			_rawWeightHead = 0; 
			if (_basicIkTargetHead != null)
			{
				_ikSolverSpine.headTarget = _basicIkTargetHead.TransformTarget;
			}
			
			
			// Arm Left
			_ikSolverArmLeft = _vrik.solver.leftArm;
			_ikSolverArmLeft.positionWeight = 0;
			_ikSolverArmLeft.rotationWeight = 0;
			_rawWeightHandLeft = 0;
			if (_basicIkTargetHandLeft != null)
			{
				_ikSolverArmLeft.target = _basicIkTargetHandLeft.TransformTarget;
			}
			
			
			// Arm Right
			_ikSolverArmRight = _vrik.solver.rightArm;
			_ikSolverArmRight.positionWeight = 0;
			_ikSolverArmRight.rotationWeight = 0;
			_rawWeightHandRight = 0;
			if (_basicIkTargetHandRight != null)
			{
				_ikSolverArmRight.target = _basicIkTargetHandRight.TransformTarget;
			}
		}
		
		private void ManageIkTargetSubscriptions(bool subscribe)
		{
			if (_basicIkTargetHead != null)
			{
				if (subscribe)
					_basicIkTargetHead.OnTargetActiveChanged.Subscribe(OnIkTargetIsTrackingChanged);
				else
					_basicIkTargetHead.OnTargetActiveChanged.Unsubscribe(OnIkTargetIsTrackingChanged);
			}
			
			if (_basicIkTargetHandLeft != null)
			{
				if (subscribe)
					_basicIkTargetHandLeft.OnTargetActiveChanged.Subscribe(OnIkTargetIsTrackingChanged);
				else
					_basicIkTargetHandLeft.OnTargetActiveChanged.Unsubscribe(OnIkTargetIsTrackingChanged);
			}
			
			if (_basicIkTargetHandRight != null)
			{
				if (subscribe)
					_basicIkTargetHandRight.OnTargetActiveChanged.Subscribe(OnIkTargetIsTrackingChanged);
				else
					_basicIkTargetHandRight.OnTargetActiveChanged.Unsubscribe(OnIkTargetIsTrackingChanged);
			}
		}
		
		
		//** ik target updates **\\
		
		private float UpdateHead(float rawWeight)
		{
			var delta = Time.deltaTime * _weightChangeSpeed;
			
			var targetWeight = (_activeVrikTargets & VrikTargetFlags.Head) == VrikTargetFlags.Head ? 1 : 0;
			
			rawWeight = MathUtility.MoveTo(rawWeight,targetWeight, delta);
			
			var easedWeight = Easing.Ease(_weightChangeEasingType, rawWeight);
			
			_ikSolverSpine.positionWeight = easedWeight;
			_ikSolverSpine.rotationWeight = easedWeight;

			return rawWeight;
		}

		private float UpdateArm(IKSolverVR.Arm solverArm, VrikTargetFlags flagHand, float rawWeight)
		{
			var delta = Time.deltaTime * _weightChangeSpeed;
			
			var targetWeight = (_activeVrikTargets & flagHand) == flagHand ? 1 : 0;
			
			rawWeight = MathUtility.MoveTo(rawWeight,targetWeight,delta);
			
			var easedWeight = Easing.Ease(_weightChangeEasingType, rawWeight);
			
			solverArm.positionWeight = easedWeight;
			solverArm.rotationWeight = easedWeight;

			return rawWeight;
		}

	}
}