﻿using System;
using UnityEngine;

namespace BasicFramework.ThirdParty.FinalIk
{

	[Flags]
	public enum VrikTargetFlags
	{
		Head 		= 1 << 0,
		Pelvis 		= 1 << 1,
		HandLeft 	= 1 << 2,
		HandRight 	= 1 << 3,
		LegLeft		= 1 << 4,
		LegRight	= 1 << 5,
	}
	
}