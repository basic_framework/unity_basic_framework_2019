﻿using BasicFramework.Core.BasicAnimation.InverseKinematics;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using RootMotion.FinalIK;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.FinalIk.Editor
{
	[CustomEditor(typeof(BasicVrikBehaviour))]
	public class BasicVrikBehaviourEditor : BasicBaseEditor<BasicVrikBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		protected override bool UpdateConstantlyInPlayMode => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.ActiveVrikTargets).CamelCaseToHumanReadable(),
				TypedTarget.ActiveVrikTargets.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var vrik = serializedObject.FindProperty("_vrik");
			var weightChangeDuration = serializedObject.FindProperty("_weightChangeDuration");
			var weightChangeEasingType = serializedObject.FindProperty("_weightChangeEasingType");
			var vrikObject = vrik.objectReferenceValue as VRIK;
			var vrikIsNull = vrikObject == null;
			
			var basicIkTargetHead = serializedObject.FindProperty("_basicIkTargetHead");
			var basicIkTargetHandLeft = serializedObject.FindProperty("_basicIkTargetHandLeft");
			var basicIkTargetHandRight = serializedObject.FindProperty("_basicIkTargetHandRight");

			SerializedObject vrikSerializedObject = null;
			SerializedProperty spine = null;
			SerializedProperty armLeft = null;
			SerializedProperty armRight = null;

			if (!vrikIsNull)
			{
				vrikSerializedObject = new SerializedObject(vrikObject);
				
				vrikSerializedObject.Update();
				
				var solver = vrikSerializedObject.FindProperty("solver");

				spine = solver.FindPropertyRelative("spine");
				armLeft = solver.FindPropertyRelative("leftArm");
				armRight = solver.FindPropertyRelative("rightArm");
			}
			
			
			Space();
			SectionHeader($"{nameof(BasicVrikBehaviour).CamelCaseToHumanReadable()} Properties");

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			BasicEditorGuiLayout.PropertyFieldNotNull(vrik);
			GUI.enabled = PrevGuiEnabled;
			
			BasicEditorGuiLayout.PropertyFieldNotNull(weightChangeDuration);
			BasicEditorGuiLayout.PropertyFieldNotNull(weightChangeEasingType);
			
			Space();
			DrawHeadFields(basicIkTargetHead, spine);
			
			Space();
			DrawArmFields(basicIkTargetHandLeft, armLeft, true);
			
			Space();
			DrawArmFields(basicIkTargetHandRight, armRight, false);
			
			if (vrikSerializedObject != null)
			{
				vrikSerializedObject.ApplyModifiedProperties();
			}
			
		}

		private void DrawHeadFields(SerializedProperty ikTargetHead, SerializedProperty spineSolver)
		{
			
			SectionHeader("Head");
			
//			ikTargetHead.isExpanded = EditorGUILayout.Foldout(ikTargetHead.isExpanded,
//				GetGuiContent("Head", ikTargetHead.tooltip), BasicEditorStyles.BoldFoldout);
//
//			if (!ikTargetHead.isExpanded) return;

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			BasicEditorGuiLayout.PropertyFieldCanBeNull(ikTargetHead);
			GUI.enabled = PrevGuiEnabled;

			if (spineSolver == null)
			{
				EditorGUILayout.LabelField("Spine", "NULL");
				return;
			}
			
			var trackedIkTargetHeadObject = ikTargetHead.objectReferenceValue as BasicIkTarget;
				
			EditorGUILayout.PropertyField(spineSolver);

			if (trackedIkTargetHeadObject != null)
			{
				spineSolver.FindPropertyRelative("headTarget").objectReferenceValue =
					trackedIkTargetHeadObject.TransformTarget;
			}
		}
		
		
		private void DrawArmFields(SerializedProperty ikTargetArm, SerializedProperty armSolver, bool isLeftArm)
		{
			var armName = isLeftArm ? "Left Arm" : "Right Arm";
			
			SectionHeader(armName);
			
//			ikTargetArm.isExpanded = EditorGUILayout.Foldout(ikTargetArm.isExpanded,
//				GetGuiContent(armName, ikTargetArm.tooltip), BasicEditorStyles.BoldFoldout);
//			
//			if(!ikTargetArm.isExpanded) return;
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			BasicEditorGuiLayout.PropertyFieldCanBeNull(ikTargetArm);
			GUI.enabled = PrevGuiEnabled;

			if (armSolver == null)
			{
				EditorGUILayout.LabelField("Arm", "NULL");
				return;
			}

			var trackedIkTargetHandLeftObject = ikTargetArm.objectReferenceValue as BasicIkTarget;
				
			EditorGUILayout.PropertyField(armSolver);
				
			if (trackedIkTargetHandLeftObject != null)
			{
				armSolver.FindPropertyRelative("target").objectReferenceValue =
					trackedIkTargetHandLeftObject.TransformTarget;
			}
		}
		
	}
}