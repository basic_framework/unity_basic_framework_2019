﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicAnimation.Humanoid;
using BasicFramework.Core.BasicAnimation.InverseKinematics;
using BasicFramework.Core.BasicMath;
using BasicFramework.Core.BasicMath.Utility;
using RootMotion.FinalIK;
using UnityEngine;

namespace BasicFramework.ThirdParty.FinalIk
{
	[AddComponentMenu("Basic Framework/Third Party/Final IK/Basic Humanoid Look At Ik")]
	public class BasicHumanoidLookAtIk : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float WEIGHT_CHANGE_DURATION = 0.6f;
		private const float WEIGHT_CHANGE_SPEED = 1 / WEIGHT_CHANGE_DURATION;
		
		//[Tooltip("Sets the LookAtIk bones on the start callback")]
		//[SerializeField] private bool _updateBonesOnStart = true;
		
		[Tooltip("The humanoid to reference for the bones")]
		[SerializeField] private BasicHumanoid _basicHumanoid = default;

		[Tooltip("The look at ik component to set the bones to and control")]
		[SerializeField] private LookAtIK _lookAtIk = default;
		
		[Tooltip("The target to look at, set this to null if you want to control this manually")]
		[SerializeField] private BasicIkTarget _lookAtIkTarget = default;


		public BasicHumanoid BasicHumanoid => _basicHumanoid;

		public LookAtIK LookAtIk => _lookAtIk;

		public BasicIkTarget LookAtIkTarget => _lookAtIkTarget;

		private bool _notNullLookAtTarget;


		private readonly List<IKSolverLookAt.LookAtBone> _tempLookAtBones = new List<IKSolverLookAt.LookAtBone>();
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Reset()
		{
			_basicHumanoid = transform.root.GetComponentInChildren<BasicHumanoid>();
			_lookAtIk = GetComponentInChildren<LookAtIK>();
		}

		private void Awake()
		{
			_notNullLookAtTarget = _lookAtIkTarget != null;
		}

		private void OnEnable()
		{
			OnLookAtTargetChanged(_lookAtIkTarget);
			_lookAtIkTarget.OnTargetTransformChanged.Subscribe(OnLookAtTargetChanged);
		}
		
		private void OnDisable()
		{
			_lookAtIkTarget.OnTargetTransformChanged.Unsubscribe(OnLookAtTargetChanged);
		}

		private void Start()
		{
			//if (!_updateBonesOnStart) return;
			UpdateLookAtIk();
			if (!_notNullLookAtTarget) return;
			_lookAtIk.solver.IKPositionWeight = 0;
		}

		private void Update()
		{
			if (!_notNullLookAtTarget) return;

			var targetWeight = _lookAtIkTarget.TargetActive ? 1 : 0;

			var easedWeight = Easing.Ease(EasingTypes.EaseInOutQuad, targetWeight);

			_lookAtIk.solver.IKPositionWeight = 
				MathUtility.MoveTo(
					_lookAtIk.solver.IKPositionWeight, 
					easedWeight,
				Time.deltaTime * WEIGHT_CHANGE_SPEED);
		}


		//***** callbacks *****\\
		
		private void OnLookAtTargetChanged(BasicIkTarget value)
		{
			if (_lookAtIk == null) return;
			_lookAtIk.solver.target = value.TransformTarget;
		}
		
		
		//***** functions *****\\
		
		public void UpdateLookAtIk()
		{
			if (_basicHumanoid == null) return;
			if (_lookAtIk == null) return;

			var solver = _lookAtIk.solver;
			
			// Target
			if (_lookAtIkTarget != null)
			{
				solver.target = _lookAtIkTarget.TransformTarget;
			}
			
			// Head
			solver.head.transform = _basicHumanoid.GetBone(HumanBodyBones.Head);
			
			// Eyes
			_tempLookAtBones.Clear();
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.RightEye));
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.LeftEye));
			solver.eyes = _tempLookAtBones.ToArray();

			// Spine
			_tempLookAtBones.Clear();
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.Spine));
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.Chest));
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.UpperChest));
			AddTempLookAtBone(_basicHumanoid.GetBone(HumanBodyBones.Neck));
			solver.spine = _tempLookAtBones.ToArray();
		}

		private void AddTempLookAtBone(Transform transformTemp)
		{
			if (transformTemp == null) return;
			_tempLookAtBones.Add(new IKSolverLookAt.LookAtBone(transformTemp));
		}
		
	}
}