﻿using BasicFramework.Core.BasicAnimation.Humanoid;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using RootMotion.FinalIK;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.FinalIk.Editor
{
	[CustomEditor(typeof(BasicHumanoidLookAtIk))]
	public class BasicHumanoidLookAtIkEditor : BasicBaseEditor<BasicHumanoidLookAtIk>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private bool _updateBones;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			//var updateBonesOnStart = serializedObject.FindProperty("_updateBonesOnStart");
			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid");
			var lookAtIk = serializedObject.FindProperty("_lookAtIk");
			var lookAtIkTarget = serializedObject.FindProperty("_lookAtIkTarget");
			
			Space();
			SectionHeader($"{nameof(BasicHumanoidLookAtIk).CamelCaseToHumanReadable()} Properties");

			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			//EditorGUILayout.PropertyField(updateBonesOnStart);
			BasicEditorGuiLayout.PropertyFieldNotNull(basicHumanoid);
			BasicEditorGuiLayout.PropertyFieldNotNull(lookAtIk);
			
			Space();
			BasicEditorGuiLayout.PropertyFieldCanBeNull(lookAtIkTarget);

			GUI.enabled = PrevGuiEnabled 
			              && !Application.isPlaying
			              && basicHumanoid.objectReferenceValue != null 
			              && lookAtIk.objectReferenceValue != null;
			
			Space();
			if (GUILayout.Button(GetGuiContent(nameof(TypedTarget.UpdateLookAtIk).CamelCaseToHumanReadable(),
				"Update the bones and target of the Look At Ik")))
			{
				_updateBones = true;
			}

			GUI.enabled = PrevGuiEnabled;
			
			DrawLookAtIkFields();
		}
		
		protected override void AfterApplyingModifiedProperties()
		{
			base.AfterApplyingModifiedProperties();

			if (!_updateBones) return;
			_updateBones = false;
			UpdateLookAtIkFields();
		}

		private void DrawLookAtIkFields()
		{
			var lookAtIkObject = serializedObject.FindProperty("_lookAtIk").objectReferenceValue as LookAtIK;
			
			SerializedObject lookAtIkSerializedObject = null;
			
			Space();
			SectionHeader($"{nameof(LookAtIK).CamelCaseToHumanReadable()} Properties");
			
			if (lookAtIkObject != null)
			{
				lookAtIkSerializedObject = new SerializedObject(lookAtIkObject);
				lookAtIkSerializedObject.Update();
			}

			var missingLookAtIkMessage = $"Please reference a {nameof(LookAtIK).CamelCaseToHumanReadable()} component";
			
			if (lookAtIkSerializedObject != null)
			{
				var solver = lookAtIkSerializedObject.FindProperty("solver");
				var lookAtTarget 	= solver.FindPropertyRelative("target");
				var ikSolverWeight 	= solver.FindPropertyRelative("IKPositionWeight");

				GUI.enabled = PrevGuiEnabled && TypedTarget.LookAtIkTarget == null;
				BasicEditorGuiLayout.PropertyFieldCanBeNull(lookAtTarget, GetGuiContent("Look At Target"));
				EditorGUILayout.PropertyField(ikSolverWeight, GetGuiContent("IK Solver Weight"));
				GUI.enabled = PrevGuiEnabled;
			}
			else
			{
				EditorGUILayout.LabelField("Look At Target", 	missingLookAtIkMessage);
				EditorGUILayout.LabelField("IK Solver Weight",	missingLookAtIkMessage);
			}


			lookAtIkSerializedObject?.ApplyModifiedProperties();
		}
		
		private void UpdateLookAtIkFields()
		{
			var basicHumanoid = serializedObject.FindProperty("_basicHumanoid").objectReferenceValue as BasicHumanoid;
			var lookAtIk = serializedObject.FindProperty("_lookAtIk").objectReferenceValue as LookAtIK;
			
			if (basicHumanoid == null) return;
			if (lookAtIk == null) return;
			
			Undo.RecordObject(lookAtIk, $"Updating {lookAtIk.name}/{nameof(LookAtIK)} Bones");
			TypedTarget.UpdateLookAtIk();
		}
		
	}
}