
## Unreleased - [00-02]

### Added

- 

### Changed

- 

### Bug Fixes

-

### Removed

-


## Unreleased - [00-01] - 20YY-MM-DD

### Added

- BasicNetworkManager: Extends NetworkManager and provides additional functionality

- NetworkBehavioutCallbacks: Add this component to any networked gameobject to recieve UnityEvent callbacks on network events such as OnStartClient

