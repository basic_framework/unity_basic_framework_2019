# Mirror Networking Notes

##Network Behaviour Call Order

### Setup
- _Awake_
- _OnEnable_
- __OnStartServer__
- __OnStartAuthority__
- __OnStartClient__
- __OnStartLocalPlayer__
- _Start_

### Teardown
- __OnNetworkDestroy__ (I think it get called first, need to setup a better test)
- _OnDisable_
- __OnNetworkDestroy__??
- _OnDestroy_