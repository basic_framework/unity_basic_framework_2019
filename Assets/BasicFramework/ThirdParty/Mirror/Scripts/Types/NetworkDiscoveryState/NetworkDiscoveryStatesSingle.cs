﻿using System;
using UnityEngine;
using UnityEngine.Events;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.ThirdParty.Mirror
{
    [CreateAssetMenu(menuName = "Basic Framework/Third Party/Mirror/Network Discovery States Single", fileName = "single_network_discovery_state_")]
    public class NetworkDiscoveryStatesSingle : SingleStructBase<NetworkDiscoveryStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(NetworkDiscoveryStatesSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class NetworkDiscoveryStatesSingleReference : SingleStructReferenceBase<NetworkDiscoveryStates, NetworkDiscoveryStatesSingle>
    {
        public NetworkDiscoveryStatesSingleReference()
        {
        }

        public NetworkDiscoveryStatesSingleReference(NetworkDiscoveryStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class NetworkDiscoveryStatesSingleUnityEvent : UnityEvent<NetworkDiscoveryStatesSingle>{}
	
    [Serializable]
    public class NetworkDiscoveryStatesSingleArrayUnityEvent : UnityEvent<NetworkDiscoveryStatesSingle[]>{}
    
}