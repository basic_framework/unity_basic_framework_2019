﻿using UnityEngine;
using BasicFramework.Core.ScriptableVariables.Single;

namespace BasicFramework.ThirdParty.Mirror
{
    [AddComponentMenu("Basic Framework/Third Party/Mirror/Network Discovery States Single Listener")]
    public class NetworkDiscoveryStatesSingleListener : SingleStructListenerBase
    <
        NetworkDiscoveryStates, 
        NetworkDiscoveryStatesSingle, 
        NetworkDiscoveryStatesUnityEvent, 
        NetworkDiscoveryStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}