﻿using UnityEditor;
using BasicFramework.Core.ScriptableVariables.Single.Editor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(NetworkDiscoveryStatesSingle))][CanEditMultipleObjects]
    public class NetworkDiscoveryStatesSingleEditor : SingleStructBaseEditor<NetworkDiscoveryStates, NetworkDiscoveryStatesSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(NetworkDiscoveryStatesSingle))]
    public class NetworkDiscoveryStatesSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(NetworkDiscoveryStatesSingleReference))]
    public class NetworkDiscoveryStatesSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    	    
            // **************************************** VARIABLES ****************************************\\
    	    
    	    
            // ****************************************  METHODS  ****************************************\\
    
    }
}