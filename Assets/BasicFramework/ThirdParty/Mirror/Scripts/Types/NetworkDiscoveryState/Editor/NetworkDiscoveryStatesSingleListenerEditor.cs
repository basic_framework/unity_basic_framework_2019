﻿using UnityEditor;
using BasicFramework.Core.ScriptableVariables.Single.Editor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(NetworkDiscoveryStatesSingleListener))][CanEditMultipleObjects]
    public class NetworkDiscoveryStatesSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    NetworkDiscoveryStates, 
	    NetworkDiscoveryStatesSingle, 
	    NetworkDiscoveryStatesUnityEvent,
	    NetworkDiscoveryStatesCompareUnityEvent,
	    NetworkDiscoveryStatesSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}