﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Mirror
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Third Party/Mirror/Network Manager Mode Single", 
        fileName = "single_network_manager_mode_")
    ]
    public class NetworkManagerModeSingle : SingleStructBase<NetworkManagerMode> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(NetworkManagerModeSingle value)
        {
            Value = value.Value;
        }


        public void SetOffline()
        {
            Value = NetworkManagerMode.Offline;
        }
        
        public void SetHost()
        {
            Value = NetworkManagerMode.Host;
        }
        
        public void SetServer()
        {
            Value = NetworkManagerMode.ServerOnly;
        }
        
        public void SetClient()
        {
            Value = NetworkManagerMode.ClientOnly;
        }

    }

    [Serializable]
    public class NetworkManagerModeSingleReference : SingleStructReferenceBase<NetworkManagerMode, NetworkManagerModeSingle>
    {
        public NetworkManagerModeSingleReference()
        {
        }

        public NetworkManagerModeSingleReference(NetworkManagerMode value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class NetworkManagerModeSingleUnityEvent : UnityEvent<NetworkManagerModeSingle>{}
	
    [Serializable]
    public class NetworkManagerModeSingleArrayUnityEvent : UnityEvent<NetworkManagerModeSingle[]>{}
    
}