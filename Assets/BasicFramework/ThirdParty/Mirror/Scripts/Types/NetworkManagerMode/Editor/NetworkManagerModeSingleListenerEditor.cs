﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(NetworkManagerModeSingleListener))][CanEditMultipleObjects]
    public class NetworkManagerModeSingleListenerEditor : SingleStructListenerBaseEditor
    <
        NetworkManagerMode, 
        NetworkManagerModeSingle, 
        NetworkManagerModeUnityEvent,
        NetworkManagerModeCompareUnityEvent,
        NetworkManagerModeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        protected override void DrawEventFields()
        {
            var isServerOrHost = serializedObject.FindProperty("_isServerOrHost");
            
            Space();
            SectionHeader($"{nameof(NetworkManagerModeSingleListener).CamelCaseToHumanReadable()} Properties");

            EditorGUILayout.PropertyField(isServerOrHost);
            
            base.DrawEventFields();
        }
    }
}