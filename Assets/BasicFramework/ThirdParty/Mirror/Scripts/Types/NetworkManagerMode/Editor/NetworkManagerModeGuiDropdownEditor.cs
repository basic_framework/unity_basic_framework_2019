﻿using BasicFramework.Core.BasicGui.Components.Editor;
using Mirror;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(NetworkManagerModeGuiDropdown))]
    public class NetworkManagerModeGuiDropdownEditor : EnumGuiDropdownBaseEditor
    <
        NetworkManagerMode, 
        NetworkManagerModeUnityEvent, 
        NetworkManagerModeCompareUnityEvent, 
        NetworkManagerModeGuiDropdown
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}