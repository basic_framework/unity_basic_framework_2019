﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using Mirror;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(NetworkManagerModeSingle))][CanEditMultipleObjects]
    public class NetworkManagerModeSingleEditor : SingleStructBaseEditor<NetworkManagerMode, NetworkManagerModeSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(NetworkManagerModeSingle))]
    public class NetworkManagerModeSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(NetworkManagerModeSingleReference))]
    public class NetworkManagerModeSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}