﻿using BasicFramework.Core.BasicGui.Components;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	[AddComponentMenu("Basic Framework/Third Party/Mirror/Gui/Network Manager Mode Gui Dropdown")]
    public class NetworkManagerModeGuiDropdown : EnumGuiDropdownBase
    <
	    NetworkManagerMode, 
	    NetworkManagerModeUnityEvent, 
	    NetworkManagerModeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}