﻿using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.ScriptableVariables.Single;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
    [AddComponentMenu("Basic Framework/Third Party/Mirror/Network Manager Mode Single Listener")]
    public class NetworkManagerModeSingleListener : SingleStructListenerBase
    <
        NetworkManagerMode, 
        NetworkManagerModeSingle, 
        NetworkManagerModeUnityEvent, 
        NetworkManagerModeCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\

        [SerializeField] private BoolPlusUnityEvent _isServerOrHost = default;
        

        // ****************************************  METHODS  ****************************************\\

        protected override void OnRaiseValueChanged(ScriptableSingleBase<NetworkManagerMode> scriptableSingleBase)
        {
            base.OnRaiseValueChanged(scriptableSingleBase);

            var mode = scriptableSingleBase.Value;
            _isServerOrHost.Raise(mode == NetworkManagerMode.ServerOnly || mode == NetworkManagerMode.Host);
        }
    }
}