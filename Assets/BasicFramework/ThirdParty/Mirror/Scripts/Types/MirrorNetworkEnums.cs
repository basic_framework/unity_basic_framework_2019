﻿using System;
using BasicFramework.Core.BasicTypes;
using Mirror;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Mirror
{
	public enum PlayerSpawnMethodsExtended
	{
		Random,
		RoundRobin,
//		DedicatedSlot,
		FirstFree,
		RandomFree
	}

//	public enum NetworkConnectionModes
//	{
//		Offline,
//		ServerOnly,
//		ClientOnly,
//		Host
//	}

	[Serializable]
	public class NetworkManagerModeUnityEvent : UnityEvent<NetworkManagerMode>{}

	[Serializable]
	public class NetworkManagerModeCompareUnityEvent : CompareValueUnityEventBase<NetworkManagerMode>{}
	
	
	public enum NetworkDiscoveryStates
	{
		Stopped,
		Searching,
		Advertising,
	}
	
	[Serializable]
	public class NetworkDiscoveryStatesUnityEvent : UnityEvent<NetworkDiscoveryStates>{}

	[Serializable]
	public class NetworkDiscoveryStatesCompareUnityEvent : CompareValueUnityEventBase<NetworkDiscoveryStates>{}
}