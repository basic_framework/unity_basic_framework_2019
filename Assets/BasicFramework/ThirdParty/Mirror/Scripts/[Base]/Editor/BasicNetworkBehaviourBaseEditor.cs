﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    public abstract class BasicNetworkBehaviourBaseEditor<TNetworkBehaviour> : BasicBaseEditor<TNetworkBehaviour>
        where TNetworkBehaviour : BasicNetworkBehaviourBase
    {
		
        // REFERENCE => NetworkBehaviourInspector.cs
	
        // **************************************** VARIABLES ****************************************\\

        protected virtual bool DrawSyncSettings => true;


        // ****************************************  METHODS  ****************************************\\
		
//		protected override void DrawPropertyFields()
//		{
//			base.DrawPropertyFields();
//			
//			Space();
//			SectionHeader($"{nameof(BasicNetworkBehaviourBase).CamelCaseToHumanReadable()} Properties");
//			
//		}
		
        protected override void DrawBottomOfInspectorFields()
        {
            base.DrawBottomOfInspectorFields();

            if (!DrawSyncSettings) return;
			
            var syncMode = serializedObject.FindProperty("syncMode");
            var syncInterval = serializedObject.FindProperty("syncInterval");
			
            Space();
            SectionHeader("Sync Settings");

            EditorGUILayout.PropertyField(syncMode);
            EditorGUILayout.PropertyField(syncInterval);
        }
		
    }
}