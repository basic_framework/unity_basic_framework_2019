﻿using BasicFramework.Core.Utility.Editor;
using Mirror;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
	public abstract class NetworkManagerBaseEditor<T> : BasicBaseEditor<T>
		where T : NetworkManager
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		private ReorderableList _spawnPrefabsReorderableList;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();
			
			var spawnPrefabsProperty = serializedObject.FindProperty("spawnPrefabs");

			_spawnPrefabsReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, spawnPrefabsProperty);

			_spawnPrefabsReorderableList.drawHeaderCallback = DrawHeader;
			_spawnPrefabsReorderableList.drawElementCallback = DrawChild;
			_spawnPrefabsReorderableList.onReorderCallback = Changed;
			_spawnPrefabsReorderableList.onRemoveCallback = RemoveButton;
			_spawnPrefabsReorderableList.onChangedCallback = Changed;
			_spawnPrefabsReorderableList.onAddCallback = AddButton;
			_spawnPrefabsReorderableList.elementHeight = 16;
		}
		
		protected override void DrawPropertyFields()
		{
			DrawConfigurationFields();
			
			//Space();
			DrawSceneManagementFields();
			
			//Space();
			DrawNetworkInfoFields();
			
			//Space();
			DrawAuthenticatorFields();
			
			//Space();
			DrawPlayerObjectFields();
			
			Space();
			DrawSpawnPrefabsList();
		}
		
		protected virtual void DrawConfigurationFields()
		{
			var dontDestroyOnLoad = serializedObject.FindProperty("dontDestroyOnLoad");
			var runInBackground = serializedObject.FindProperty("runInBackground");
			var startOnHeadless = serializedObject.FindProperty("startOnHeadless");
			var showDebugMessages = serializedObject.FindProperty("showDebugMessages");
			var serverTickRate = serializedObject.FindProperty("serverTickRate");

			EditorGUILayout.PropertyField(dontDestroyOnLoad);
			EditorGUILayout.PropertyField(runInBackground);
			EditorGUILayout.PropertyField(startOnHeadless);
			EditorGUILayout.PropertyField(showDebugMessages);
			EditorGUILayout.IntSlider(serverTickRate, 1, 60);
		}
		
		protected virtual void DrawSceneManagementFields()
		{
			var offlineScene = serializedObject.FindProperty("offlineScene");
			var onlineScene = serializedObject.FindProperty("onlineScene");

			var networkSceneName = NetworkManager.networkSceneName;
			if (string.IsNullOrEmpty(networkSceneName)) networkSceneName = "NULL";
			
			EditorGUILayout.PropertyField(offlineScene);
			EditorGUILayout.PropertyField(onlineScene);
			EditorGUILayout.LabelField("Network Scene Name", networkSceneName);
		}
		
		protected virtual  void DrawNetworkInfoFields()
		{
			var transport = serializedObject.FindProperty("transport");
			var networkAddress = serializedObject.FindProperty("networkAddress");
			var maxConnections = serializedObject.FindProperty("maxConnections");

			EditorGUILayout.PropertyField(transport);
			EditorGUILayout.PropertyField(networkAddress);
			EditorGUILayout.PropertyField(maxConnections);
		}

		protected virtual  void DrawAuthenticatorFields()
		{
			var authenticator = serializedObject.FindProperty("authenticator");
			
			EditorGUILayout.PropertyField(authenticator);
		}

		protected virtual  void DrawPlayerObjectFields()
		{
			var playerPrefab = serializedObject.FindProperty("playerPrefab");
			var autoCreatePlayer = serializedObject.FindProperty("autoCreatePlayer");
			//var playerSpawnMethod = serializedObject.FindProperty("playerSpawnMethod");

			EditorGUILayout.PropertyField(playerPrefab);
			EditorGUILayout.PropertyField(autoCreatePlayer);
			//EditorGUILayout.PropertyField(playerSpawnMethod);
		}
		
		protected virtual  void DrawSpawnPrefabsList()
		{
			SectionHeader("Spawnable Prefabs");
			_spawnPrefabsReorderableList.DoLayoutList();
		}
		
		private void DrawHeader(Rect headerRect)
		{
			GUI.Label(headerRect, "Registered Spawnable Prefabs:");
		}
		
		private void DrawChild(Rect r, int index, bool isActive, bool isFocused)
        {
	        var spawnPrefabsProperty = serializedObject.FindProperty("spawnPrefabs");
            var prefab = spawnPrefabsProperty.GetArrayElementAtIndex(index);
            var go = (GameObject)prefab.objectReferenceValue;

            GUIContent label;
            if (go == null)
            {
                label = new GUIContent("Empty", "Drag a prefab with a NetworkIdentity here");
            }
            else
            {
                var identity = go.GetComponent<NetworkIdentity>();
                label = new GUIContent(go.name, identity != null ? "AssetId: [" + identity.assetId + "]" : "No Network Identity");
            }

            var newGameObject = (GameObject)EditorGUI.ObjectField(r, label, go, typeof(GameObject), false);

            if (newGameObject == go) return;
            
            if (newGameObject != null && !newGameObject.GetComponent<NetworkIdentity>())
            {
	            Debug.LogError("Prefab " + newGameObject + " cannot be added as spawnable as it doesn't have a NetworkIdentity.");
	            return;
            }
            prefab.objectReferenceValue = newGameObject;
        }

		private void Changed(ReorderableList list)
        {
            EditorUtility.SetDirty(target);
        }

		private void AddButton(ReorderableList list)
        {
	        var spawnPrefabsProperty = serializedObject.FindProperty("spawnPrefabs");
	        spawnPrefabsProperty.arraySize += 1;
            list.index = spawnPrefabsProperty.arraySize - 1;

            var obj = spawnPrefabsProperty.GetArrayElementAtIndex(spawnPrefabsProperty.arraySize - 1);
            obj.objectReferenceValue = null;

            _spawnPrefabsReorderableList.index = _spawnPrefabsReorderableList.count - 1;

            Changed(list);
        }

		private void RemoveButton(ReorderableList list)
        {
	        var spawnPrefabsProperty = serializedObject.FindProperty("spawnPrefabs");
	        spawnPrefabsProperty.DeleteArrayElementAtIndex(_spawnPrefabsReorderableList.index);

	        if (list.index < spawnPrefabsProperty.arraySize) return;
	        list.index = spawnPrefabsProperty.arraySize - 1;
        }
	
	}
}