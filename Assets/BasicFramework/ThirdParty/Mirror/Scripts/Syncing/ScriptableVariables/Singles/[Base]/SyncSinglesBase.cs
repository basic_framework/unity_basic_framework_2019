﻿using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	public abstract class SyncSinglesBase : BasicNetworkBehaviourBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SyncVar] 
		private bool _syncListInitialised;
		public bool SyncListInitialised
		{
			get => _syncListInitialised;
			protected set => _syncListInitialised = value;
		}

		// Remove
		public bool HasAuthorityOfSyncList => isServer || hasAuthority;

		protected abstract int SyncCount { get; }
		
		public abstract bool IsInstance { get; }
		
	
		// ****************************************  METHODS  ****************************************\\
	
		//***** abstract *****\\
		
		protected abstract void InitialiseSyncList();


		// Synchronize the sync list with all the single values
		protected abstract void SyncListToSingles();
		
		protected abstract void SyncSinglesToList();

		protected abstract void RegisterCallbacks(bool subscribe);

		protected abstract void CleanSinglesArray();
		
		
		//***** mono *****\\

		protected virtual void Awake()
		{
			if (!IsInstance)
			{
				var typeName = GetType().Name.CamelCaseToHumanReadable();
				Debug.LogError($"{name}/{typeName} => " +
				               $"You can not have multiple INSTANCE's of \"{typeName}\" in the same scene. " +
				               $"This instance will be ignored.");
				enabled = false;
				return;
			}
			
			if (SyncCount == 0)
			{
				enabled = false;
				return;
			}
			
			CleanSinglesArray();
		}
		
		protected virtual void OnEnable()
		{
			if (!IsInstance)
			{
				enabled = false;
				return;
			}
			
			if (SyncListInitialised)
			{
				if (HasAuthorityOfSyncList)
					SyncListToSingles();
				else
					SyncSinglesToList();
			}
			
			RegisterCallbacks(true);
		}

		protected virtual void OnDisable()
		{
			if (!IsInstance || SyncCount == 0) return;
			
			RegisterCallbacks(false);
		}

		protected virtual void Start()
		{
			InitialiseSyncList();
			
			// Clients don't have authority to initialise sync list so if it is not ready don't update
			if (!SyncListInitialised) return;
			
			SyncSinglesToList();
		}
		
		
		//***** callbacks *****\\

		public override void OnStartAuthority()
		{
			base.OnStartAuthority();
			
			InitialiseSyncList();
			
			SyncListToSingles();
		}
	
		
		//***** functions *****\\

		protected void LogNoAuthorityError(string singleName, string singleValue, string syncValue)
		{
			Debug.LogWarning($"{name}/{GetType().Name.CamelCaseToHumanReadable()} => " +
			                 $"does not have authority to set the sync list values. " +
			                 $"\"{singleName}\'s value ({singleValue}) is being reset to synced value ({syncValue})");
		}
		
	}
}