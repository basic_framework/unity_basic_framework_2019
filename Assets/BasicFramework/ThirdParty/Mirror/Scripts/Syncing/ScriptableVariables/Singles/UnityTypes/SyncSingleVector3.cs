﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	[Serializable]
	public class SyncListVector3 : SyncList<Vector3> {}
	
	[AddComponentMenu("Basic Framework/Third Party/Mirror/Sync Singles Vector 3")]
	public class SyncSinglesVector3 : SyncSinglesBase 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private static SyncSinglesVector3 _instance = default;
		

		[SerializeField] private Vector3Single[] _singles = default;
		
		
		private readonly SyncListVector3 _syncList = new SyncListVector3();
		
		
		protected override int SyncCount => _singles.Length;
		
		public override bool IsInstance
		{
			get
			{
				if (_instance == null) _instance = this;
				return _instance == this;
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		protected override void CleanSinglesArray() => _singles.RemoveDuplicatesAndNulls();
		
		protected override void RegisterCallbacks(bool subscribe)
		{
			if (subscribe)
			{
				_syncList.Callback += OnSyncListChanged;

				foreach (var single in _singles)
				{
					single.ValueChanged.Subscribe(OnSingleChanged);
				}
			}
			else
			{
				_syncList.Callback -= OnSyncListChanged;
			
				foreach (var single in _singles)
				{
					single.ValueChanged.Unsubscribe(OnSingleChanged);
				}
			}
		}

		protected override void InitialiseSyncList()
		{
			if (SyncListInitialised) return;
			if (!HasAuthorityOfSyncList) return;

			foreach (var single in _singles)
			{
				_syncList.Add(single.Value);
			}

			SyncListInitialised = true;
		}

		protected override void SyncListToSingles()
		{
			foreach (var single in _singles)
			{
				OnSingleChanged(single);
			}
		}

		protected override void SyncSinglesToList()
		{
			for (int i = 0; i < _syncList.Count; i++)
			{
				var value = _syncList[i];
				OnSyncListChanged(SyncListVector3.Operation.OP_SET, i, value, value);
			}
		}


		//***** callbacks *****\\
		
		private void OnSyncListChanged(SyncListVector3.Operation op, int itemindex, Vector3 olditem, Vector3 newitem)
		{
			if (HasAuthorityOfSyncList)	return;
			_singles[itemindex].Value = newitem;
		}
		
		private void OnSingleChanged(ScriptableSingleBase<Vector3> single)
		{
			if (!SyncListInitialised) return;
			
			var index = _singles.IndexOf(single);

			var syncValue = _syncList[index];
			var singleValue = single.Value;
			
			if(syncValue == singleValue) return;
			
			if (HasAuthorityOfSyncList)
			{
				_syncList[index] = singleValue;
			}
			else
			{
				LogNoAuthorityError(single.name, singleValue.ToString(), syncValue.ToString());
				single.Value = syncValue;
			}
		}
		
	}
}