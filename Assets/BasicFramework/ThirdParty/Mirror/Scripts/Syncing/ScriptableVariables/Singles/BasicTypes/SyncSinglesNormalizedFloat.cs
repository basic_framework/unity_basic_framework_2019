﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	[AddComponentMenu("Basic Framework/Third Party/Mirror/Sync Singles Normalized Float")]
	public class SyncSinglesNormalizedFloat : SyncSinglesBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private static SyncSinglesNormalizedFloat _instance = default;
		

		[SerializeField] private NormalizedFloatSingle[] _singles = default;
		
		
		private readonly SyncListFloat _syncList = new SyncListFloat();
		
		
		protected override int SyncCount => _singles.Length;
		
		public override bool IsInstance
		{
			get
			{
				if (_instance == null) _instance = this;
				return _instance == this;
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		protected override void CleanSinglesArray() => _singles.RemoveDuplicatesAndNulls();
		
		protected override void RegisterCallbacks(bool subscribe)
		{
			if (subscribe)
			{
				_syncList.Callback += OnSyncListChanged;

				foreach (var single in _singles)
				{
					single.ValueChanged.Subscribe(OnSingleChanged);
				}
			}
			else
			{
				_syncList.Callback -= OnSyncListChanged;
			
				foreach (var single in _singles)
				{
					single.ValueChanged.Unsubscribe(OnSingleChanged);
				}
			}
		}

		protected override void InitialiseSyncList()
		{
			if (SyncListInitialised) return;
			if (!HasAuthorityOfSyncList) return;

			foreach (var single in _singles)
			{
				_syncList.Add(single.Value.Value);
			}

			SyncListInitialised = true;
		}

		protected override void SyncListToSingles()
		{
			foreach (var single in _singles)
			{
				OnSingleChanged(single);
			}
		}

		protected override void SyncSinglesToList()
		{
			for (int i = 0; i < _syncList.Count; i++)
			{
				var value = _syncList[i];
				OnSyncListChanged(SyncListFloat.Operation.OP_SET, i, value, value);
			}
		}


		//***** callbacks *****\\
		
		private void OnSyncListChanged(SyncListFloat.Operation op, int itemindex, float olditem, float newitem)
		{
			if (HasAuthorityOfSyncList)	return;
			_singles[itemindex].Value = new NormalizedFloat(newitem);
		}
		
		private void OnSingleChanged(ScriptableSingleBase<NormalizedFloat> single)
		{
			if (!SyncListInitialised) return;
			
			var index = _singles.IndexOf(single);

			var syncValue = _syncList[index];
			var singleValue = single.Value.Value;
			
			if(Math.Abs(syncValue - singleValue) < Mathf.Epsilon) return;
			
			if (HasAuthorityOfSyncList)
			{
				_syncList[index] = singleValue;
			}
			else
			{
				LogNoAuthorityError(single.name, singleValue.ToString(), syncValue.ToString());
				single.Value = new NormalizedFloat(syncValue);
			}
		}
		
	}
}