﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror
{
	[CustomEditor(typeof(SyncSinglesBool))]
	public class SyncSinglesBoolEditor : SyncSinglesBaseEditor
	<
		bool, 
		BoolSingle, 
		SyncSinglesBool
	>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}