﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
	public class SyncSinglesBaseEditor<T, TSingle, TSync> : BasicNetworkBehaviourBaseEditor<TSync>
		where TSingle : ScriptableSingleBase<T>
		where TSync : SyncSinglesBase
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		protected override bool HasDebugSection => true;

		private ReorderableList _reorderableListSingles;
		
		private bool _detectedMultipleInstances;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();
			
			var singles = serializedObject.FindProperty("_singles");

			_reorderableListSingles =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, singles);

			_reorderableListSingles.displayAdd = false;
			
			_detectedMultipleInstances = FindObjectsOfType<TSync>().Length > 1;
		}

		protected override void DrawTopOfInspectorMessage()
		{
			base.DrawTopOfInspectorMessage();

			var typeName = typeof(TSync).Name.CamelCaseToHumanReadable();
			var isInstance = TypedTarget.IsInstance;
			
			if (Application.isPlaying && isInstance || !Application.isPlaying && !_detectedMultipleInstances)
			{
				EditorGUILayout.HelpBox(
					"To control the synced variable this must be the SERVER, or have AUTHORITY", 
					MessageType.Info);
			}
			else if (!Application.isPlaying)
			{
				EditorGUILayout.HelpBox(
					$"Detected multiple instances of \"{typeName}\" in the scene." +
					$"\nThis is NOT allowed.\nPlease combine into one.", 
					MessageType.Error);
			}
			else
			{
				EditorGUILayout.HelpBox(
					$"This instance of \"{typeName}\" has been disabled." +
					$"\nThere can only be one INSTANCE of \"{typeName}\" in the scene",
					MessageType.Error);
			}
			
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			
			EditorGUILayout.LabelField(
				$"{nameof(TypedTarget.IsInstance).CamelCaseToHumanReadable()}", 
				(Application.isPlaying && TypedTarget.IsInstance).ToString());
			
			EditorGUILayout.LabelField(
				$"{nameof(TypedTarget.SyncListInitialised).CamelCaseToHumanReadable()}", 
				TypedTarget.SyncListInitialised.ToString());
			
			EditorGUILayout.LabelField(
				$"{nameof(TypedTarget.HasAuthorityOfSyncList).CamelCaseToHumanReadable()}", 
				TypedTarget.HasAuthorityOfSyncList.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var singles = serializedObject.FindProperty("_singles");
			
			Space();
			SectionHeader($"{typeof(TSync).Name.CamelCaseToHumanReadable()} Properties");

			var message = Application.isPlaying 
				? $"Cannot Add {typeof(TSingle).Name.CamelCaseToHumanReadable()}'s in Play Mode" 
				: $"Drop Synced {typeof(TSingle).Name.CamelCaseToHumanReadable()}'s Here";
			
			GUI.enabled = PrevGuiEnabled && !Application.isPlaying;
			
			BasicEditorGuiLayout.DragAndDropAreaIntoList<TSingle>(singles, message);
			BasicEditorArrayUtility.RemoveNullReferences(singles);
			BasicEditorArrayUtility.RemoveDuplicateReferences(singles);
			
			if (Application.isPlaying)
			{
				var count = singles.arraySize;
				
				SectionHeader($"Synced {typeof(TSingle).Name.CamelCaseToHumanReadable()}'s");
				
				for (int i = 0; i < count; i++)
				{
					Space(8f);
					var singleProperty = singles.GetArrayElementAtIndex(i);
					
					BasicEditorGuiLayout.PropertyFieldNotNull(singleProperty);
				}
			}
			else
			{
				_reorderableListSingles.DoLayoutList();
			}
			
			GUI.enabled = PrevGuiEnabled;
			
		}
	
	}
}