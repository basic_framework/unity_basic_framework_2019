﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesVector3))]
    public class SyncSinglesVector3Editor : SyncSinglesBaseEditor
    <
        Vector3, 
        Vector3Single, 
        SyncSinglesVector3
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}