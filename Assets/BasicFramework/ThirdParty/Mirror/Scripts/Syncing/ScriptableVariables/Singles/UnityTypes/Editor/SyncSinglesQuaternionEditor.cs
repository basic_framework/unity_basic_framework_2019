﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesQuaternion))]
    public class SyncSinglesQuaternionEditor : SyncSinglesBaseEditor
    <
        Quaternion, 
        QuaternionSingle, 
        SyncSinglesQuaternion
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}