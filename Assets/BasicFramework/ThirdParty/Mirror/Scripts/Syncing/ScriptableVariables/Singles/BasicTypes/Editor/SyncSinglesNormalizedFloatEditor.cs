﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesNormalizedFloat))]
    public class SyncSinglesNormalizedFloatEditor : SyncSinglesBaseEditor
    <
        NormalizedFloat, 
        NormalizedFloatSingle, 
        SyncSinglesNormalizedFloat
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}