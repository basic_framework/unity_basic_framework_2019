﻿//using System;
//using BasicFramework.Core.BasicTypes;
//using BasicFramework.Core.ScriptableVariables.Single;
//using BasicFramework.Core.Utility.ExtensionMethods;
//using Mirror;
//using UnityEngine;
//
//namespace BasicFramework.ThirdParty.Mirror
//{
//	[Serializable]
//	public class SyncListBasicPose : SyncList<BasicPose> {}
//	
//	[AddComponentMenu("Basic Framework/Third Party/Mirror/Sync Singles Basic Pose")]
//	public class SyncSinglesBasicPose : SyncSinglesBase 
//	{
//	
//		// **************************************** VARIABLES ****************************************\\
//
//		// TODO: Modify to use a Vector3 and a Quaternion
//		
//		private static SyncSinglesBasicPose _instance = default;
//		
//
//		[SerializeField] private BasicPoseSingle[] _singles = default;
//		
//		
//		private readonly SyncListBasicPose _syncList = new SyncListBasicPose();
//		
//		
//		protected override int SyncCount => _singles.Length;
//		
//		public override bool IsInstance
//		{
//			get
//			{
//				if (_instance == null) _instance = this;
//				return _instance == this;
//			}
//		}
//
//
//		// ****************************************  METHODS  ****************************************\\
//		
//		//***** overrides *****\\
//		
//		protected override void CleanSinglesArray() => _singles.RemoveDuplicatesAndNulls();
//		
//		protected override void RegisterCallbacks(bool subscribe)
//		{
//			if (subscribe)
//			{
//				_syncList.Callback += OnSyncListChanged;
//
//				foreach (var single in _singles)
//				{
//					single.ValueChanged.Subscribe(OnSingleChanged);
//				}
//			}
//			else
//			{
//				_syncList.Callback -= OnSyncListChanged;
//			
//				foreach (var single in _singles)
//				{
//					single.ValueChanged.Unsubscribe(OnSingleChanged);
//				}
//			}
//		}
//
//		protected override void InitialiseSyncList()
//		{
//			if (SyncListInitialised) return;
//			if (!HasAuthorityOfSyncList) return;
//
//			foreach (var single in _singles)
//			{
//				_syncList.Add(single.Value);
//			}
//
//			SyncListInitialised = true;
//		}
//
//		protected override void SyncListToSingles()
//		{
//			foreach (var single in _singles)
//			{
//				OnSingleChanged(single);
//			}
//		}
//
//		protected override void SyncSinglesToList()
//		{
//			for (int i = 0; i < _syncList.Count; i++)
//			{
//				var value = _syncList[i];
//				OnSyncListChanged(SyncListBasicPose.Operation.OP_SET, i, value, value);
//			}
//		}
//
//
//		//***** callbacks *****\\
//		
//		private void OnSyncListChanged(SyncListBasicPose.Operation op, int itemindex, BasicPose olditem, BasicPose newitem)
//		{
//			if (HasAuthorityOfSyncList)	return;
//			_singles[itemindex].Value = newitem;
//		}
//		
//		private void OnSingleChanged(ScriptableSingleBase<BasicPose> single)
//		{
//			if (!SyncListInitialised) return;
//			
//			var index = _singles.IndexOf(single);
//
//			var syncValue = _syncList[index];
//			var singleValue = single.Value;
//			
//			if(syncValue == singleValue) return;
//			
//			if (HasAuthorityOfSyncList)
//			{
//				_syncList[index] = singleValue;
//			}
//			else
//			{
//				LogNoAuthorityError(single.name, singleValue.ToString(), syncValue.ToString());
//				single.Value = syncValue;
//			}
//		}
//		
//	}
//}