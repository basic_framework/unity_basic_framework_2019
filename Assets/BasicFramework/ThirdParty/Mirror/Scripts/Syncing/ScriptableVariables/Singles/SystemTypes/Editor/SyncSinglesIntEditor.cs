﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesInt))]
    public class SyncSinglesIntEditor : SyncSinglesBaseEditor
    <
        int, 
        IntSingle, 
        SyncSinglesInt
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}