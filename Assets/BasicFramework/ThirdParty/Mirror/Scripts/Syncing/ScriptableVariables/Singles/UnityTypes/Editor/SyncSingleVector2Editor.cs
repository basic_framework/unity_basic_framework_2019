﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesVector2))]
    public class SyncSinglesVector2Editor : SyncSinglesBaseEditor
    <
        Vector2, 
        Vector2Single, 
        SyncSinglesVector2
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}