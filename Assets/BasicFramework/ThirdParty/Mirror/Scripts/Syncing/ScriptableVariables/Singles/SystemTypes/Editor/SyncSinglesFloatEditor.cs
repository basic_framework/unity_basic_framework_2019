﻿using BasicFramework.Core.ScriptableVariables.Single;
using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Mirror
{
    [CustomEditor(typeof(SyncSinglesFloat))]
    public class SyncSinglesFloatEditor : SyncSinglesBaseEditor
    <
        float, 
        FloatSingle, 
        SyncSinglesFloat
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
	
	
    }
}