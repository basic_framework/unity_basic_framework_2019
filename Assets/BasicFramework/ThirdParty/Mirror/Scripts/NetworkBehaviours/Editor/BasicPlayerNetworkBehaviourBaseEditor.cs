﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
	[CustomEditor(typeof(BasicPlayerNetworkBehaviour))]
	public class BasicPlayerNetworkBehaviourEditor : BasicPlayerNetworkBehaviourBaseEditor<BasicPlayerNetworkBehaviour>{}
	
	public abstract class BasicPlayerNetworkBehaviourBaseEditor<TBasicPlayerNetworkBehaviour> : BasicBaseEditor<TBasicPlayerNetworkBehaviour>
		where TBasicPlayerNetworkBehaviour : BasicPlayerNetworkBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.IsLocalPlayer).CamelCaseToHumanReadable(), 
				TypedTarget.IsLocalPlayer.ToString());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.PlayerIndex).CamelCaseToHumanReadable(), 
				TypedTarget.PlayerIndex.ToString());
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();

			var isLocalPlayerChanged = serializedObject.FindProperty("_isLocalPlayerChanged");
			var playerIndexChanged = serializedObject.FindProperty("_playerIndexChanged");
			
			Space();
			SectionHeader($"{nameof(BasicPlayerNetworkBehaviour).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(isLocalPlayerChanged);
			EditorGUILayout.PropertyField(playerIndexChanged);
		}

		protected override void DrawBottomOfInspectorFields()
		{
			base.DrawBottomOfInspectorFields();
			
			var syncMode = serializedObject.FindProperty("syncMode");
			var syncInterval = serializedObject.FindProperty("syncInterval");
			
			Space();
			SectionHeader($"{nameof(NetworkBehaviour).CamelCaseToHumanReadable()} Sync Settings");

			EditorGUILayout.PropertyField(syncMode);
			EditorGUILayout.PropertyField(syncInterval);
		}
	
	}
}