﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	[RequireComponent(typeof(NetworkIdentity))]
	public class BasicPlayerNetworkBehaviour : NetworkBehaviour 
	{
		
		// This can be used as a base script for a network player, or as a component on a network player gameobject
		
		// Note: Start may be called before OnStartServer/OnStartClient, etc. so we should also raise event in those
		
		//	Network Callback Order
		//		- Awake
		// 		- OnEnable
		//		- OnStartServer
		//		- OnStartAuthority
		//		- OnStartClient
		//		- OnStartLocalPlayer
		//		- Start
	
		// **************************************** VARIABLES ****************************************\\
		
		
		// List of players managed on the server
		private static readonly List<BasicPlayerNetworkBehaviour> ServerPlayers = new List<BasicPlayerNetworkBehaviour>();
		

		[SerializeField] private IntUnityEvent _playerIndexChanged = default;
		[SerializeField] private BoolPlusUnityEvent _isLocalPlayerChanged = default;
		
		
		private bool _isLocalPlayer;
		public bool IsLocalPlayer
		{
			get => _isLocalPlayer;
			private set
			{
				if (_isLocalPlayer == value) return;
				_isLocalPlayer = value;
				IsLocalPlayerUpdated(value);
			}
		}
		
		[SyncVar(hook = nameof(PlayerIndexUpdated))]
		private int _playerIndex = -1;
		public int PlayerIndex
		{
			get => _playerIndex;
			private set
			{
				var basicNetworkManager = BasicNetworkManager.Instance;
				if (basicNetworkManager == null) return;
				if (!basicNetworkManager.IsServer) return;
				
				if (_playerIndex == value) return;
				var oldValue = _playerIndex;
				_playerIndex = value;
				PlayerIndexUpdated(oldValue, value);
			}
		}

		
		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\

		protected virtual void Start()
		{
			PlayerIndexUpdated(_playerIndex, _playerIndex);
			IsLocalPlayerUpdated(_isLocalPlayer);
		}
		

		//***** overrides *****\\

		public override void OnStartServer()
		{
			base.OnStartServer();
			
			// Update player list
			for (int i = ServerPlayers.Count - 1; i >= 0; i--)
			{
				if(ServerPlayers[i] != null) continue;
				ServerPlayers.RemoveAt(i);
			}
			
			ServerPlayers.Add(this);

			// Assign a player index
			for (int i = 0; i < ServerPlayers.Count; i++)
			{
				var indexAvailable = true;

				foreach (var serverPlayer in ServerPlayers)
				{
					if(serverPlayer._playerIndex != i) continue;
					indexAvailable = false;
					break;
				}
				
				if(!indexAvailable) continue;
				PlayerIndex = i;
				break;
			}
		}

		public override void OnStartLocalPlayer()
		{
			base.OnStartLocalPlayer();

			IsLocalPlayer = isLocalPlayer;
		}
		

		//***** property updates *****\\

		protected virtual void PlayerIndexUpdated(int oldValue, int newValue)
		{
			_playerIndexChanged.Invoke(newValue);
		}
		
		protected virtual void IsLocalPlayerUpdated(bool value)
		{
			_isLocalPlayerChanged.Raise(value);
		}

	}
}