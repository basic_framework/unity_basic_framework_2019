﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
    [CustomEditor(typeof(BasicNetworkManager), true)]
    [CanEditMultipleObjects]
    public class BasicNetworkManagerEditor : BasicNetworkManagerEditor<BasicNetworkManager> {}
	
    public class BasicNetworkManagerEditor<T> : NetworkManagerBaseEditor<T>
        where T : BasicNetworkManager
    {
	
        // **************************************** VARIABLES ****************************************\\

        private ReorderableList _networkManagerModeChangedCompare;
        
	
        // ****************************************  METHODS  ****************************************\\

        protected override void OnEnable()
        {
            base.OnEnable();

            var networkManagerModeChangedCompare = serializedObject.FindProperty("_networkManagerModeChangedCompare");

            _networkManagerModeChangedCompare =
                BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, networkManagerModeChangedCompare,
                    true);
        }

        protected override void DrawPlayerObjectFields()
        {
            base.DrawPlayerObjectFields();
			
            var playerSpawnMethod = serializedObject.FindProperty("_playerSpawnMethod");
			
            EditorGUILayout.PropertyField(playerSpawnMethod);
        }
		
        protected override void DrawEventFields()
        {
            var networkManagerModeChanged = serializedObject.FindProperty("_networkManagerModeChanged");
            var networkConnectionsCountChanged = serializedObject.FindProperty("_networkConnectionsCountChanged");
            
            Space();
            SectionHeader($"{nameof(BasicNetworkManager).CamelCaseToHumanReadable()} Events");
            
            EditorGUILayout.PropertyField(networkConnectionsCountChanged);
            EditorGUILayout.PropertyField(networkManagerModeChanged);
            _networkManagerModeChangedCompare.DoLayoutList();
        }
		
    }
}