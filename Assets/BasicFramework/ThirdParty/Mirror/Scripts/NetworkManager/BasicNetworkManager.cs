﻿using System;
using System.Collections.Generic;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.SceneManagement;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BasicFramework.ThirdParty.Mirror
{
	[AddComponentMenu("Basic Framework/Third Party/Mirror/Basic Network Manager")]
	public class BasicNetworkManager : NetworkManager 
	{
	
		// Note: Extends the Network manager to provide more spawning option and callback events
		//		 This can be inherited and extended, inherit from BasicNetworkManagerEditor<T> to make editor class 
		
		//	Network Callback Order
		//		- OnStartServer
		//		- OnStartHost
		//		- OnStartClient
		//		- OnStopHost
		//		- OnStopClient
		//		- OnStopServer

		
		// **************************************** VARIABLES ****************************************\\
		
		
		public static BasicNetworkManager Instance => singleton as BasicNetworkManager;
		
		
		[Tooltip("The method that will be used to place the player prefabs on spawn")]
		[SerializeField] private PlayerSpawnMethodsExtended _playerSpawnMethod = PlayerSpawnMethodsExtended.Random;

		[Tooltip("Raised when the number of current network connections changes (the number of clients connected to the network)")]
		[SerializeField] private IntUnityEvent _networkConnectionsCountChanged = default;
		
		// If using DontDestroyOnLoad don't assign scene references
		[Tooltip("Raised when the network manager mode changes, option (Offline, ServerOnly, ClientOnly, Host")]
		[SerializeField] private NetworkManagerModeUnityEvent _networkManagerModeChanged = default;
		
		[Tooltip("Raised when the network manager mode changes and the condition is met")]
		[SerializeField] private NetworkManagerModeCompareUnityEvent[] _networkManagerModeChangedCompare = default;


		public IntUnityEvent NetworkConnectionsCountChanged => _networkConnectionsCountChanged;

		public NetworkManagerModeUnityEvent NetworkManagerModeChanged => _networkManagerModeChanged;


		// Is this instance running as Server, Client, Host or is it Offline
		private NetworkManagerMode _networkManagerMode;
		public NetworkManagerMode NetworkManagerMode
		{
			get => _networkManagerMode;
			private set
			{
				if(_networkManagerMode == value) return;
				_networkManagerMode = value;
				NetworkManagerModeUpdated(_networkManagerMode);
			}
		}

		/// <summary>
		/// Is the NetworkManager running as Host or ServerOnly
		/// </summary>
		public bool IsServer => _networkManagerMode == NetworkManagerMode.ServerOnly ||
		                        _networkManagerMode == NetworkManagerMode.Host;

		public string NetworkAddress
		{
			get => networkAddress;
			set => networkAddress = value;
		}
		
		private readonly HashSet<NetworkConnection> _networkConnections = new HashSet<NetworkConnection>();

		public int NetworkConnectionsCount => _networkConnections.Count;

		// Keeps track of which start positions have been used
		private readonly Dictionary<Transform, NetworkConnection> _usedStartPositions = 
			new Dictionary<Transform, NetworkConnection>();
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		public override void Start()
		{
			base.Start();

			_networkManagerMode = mode;
			NetworkManagerModeUpdated(_networkManagerMode);
			
			_networkConnectionsCountChanged.Invoke(NetworkConnectionsCount);
		}
		
		
		//***** overrides *****\\
		
		public override void OnServerConnect(NetworkConnection networkConnection)
		{
			base.OnServerConnect(networkConnection);
			
			_networkConnections.Add(networkConnection);
			
			_networkConnectionsCountChanged.Invoke(NetworkConnectionsCount);
		}
		
		public override void OnServerDisconnect(NetworkConnection networkConnection)
		{
			_networkConnections.Remove(networkConnection);
			
			_networkConnectionsCountChanged.Invoke(NetworkConnectionsCount);

			base.OnServerDisconnect(networkConnection);
		}

		public override void OnServerAddPlayer(NetworkConnection networkConnection)
		{
			var startPos = GetStartPosition(networkConnection);
			var player = startPos != null
				? Instantiate(playerPrefab, startPos.position, startPos.rotation)
				: Instantiate(playerPrefab);

			if (NetworkServer.AddPlayerForConnection(networkConnection, player)) return;
			Debug.Log("Something went wrong abort... ABORT...");
		}
		
		// Note: Mode is changed before OnStart is called, BUT, after OnStop,
		// In NetworkManager it is always be set to offline after OnStop call so we can safely set it to offline here
		public override void OnStartServer() =>	NetworkManagerMode = mode;
		public override void OnStopServer() => NetworkManagerMode = NetworkManagerMode.Offline;
		
		public override void OnStartHost() => NetworkManagerMode = mode;
		public override void OnStopHost() => NetworkManagerMode = NetworkManagerMode.Offline;
		
		public override void OnStartClient() => NetworkManagerMode = mode;
		public override void OnStopClient() => NetworkManagerMode = NetworkManagerMode.Offline;

		
//		public override void ServerChangeScene(string newSceneName)
//		{
//			base.ServerChangeScene(newSceneName);
//		}
		
//		public override void OnServerChangeScene(string newSceneName)
//		{
//			base.OnServerChangeScene(newSceneName);
//		}

		public override void OnServerSceneChanged(string sceneName)
		{
			base.OnServerSceneChanged(sceneName);
			_usedStartPositions.Clear();
		}

//		public override void OnClientChangeScene(string newSceneName, SceneOperation sceneOperation, bool customHandling)
//		{
//			base.OnClientChangeScene(newSceneName, sceneOperation, customHandling);
//		}
//
//		public override void OnClientSceneChanged(NetworkConnection conn)
//		{
//			base.OnClientSceneChanged(conn);
//			
//		}

		
		//***** properties updated *****\\

		private void NetworkManagerModeUpdated(NetworkManagerMode networkManagerMode)
		{
			_networkManagerModeChanged.Invoke(networkManagerMode);

			foreach (var compareUnityEvent in _networkManagerModeChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(networkManagerMode);
			}
		}
		
		
		//***** functions *****\\

		public void StartNetworkManager(NetworkManagerModeSingle networkManagerModeSingle)
		{
			if (networkManagerModeSingle == null) return;
			StartNetworkManager(networkManagerModeSingle.Value);
		}
		
		public void StartNetworkManager(NetworkManagerMode networkManagerMode)
		{
			if (NetworkManagerMode != NetworkManagerMode.Offline)
			{
				Debug.LogError($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
				               $"=> Cannot start network manager as {networkManagerMode}, it is already running as {NetworkManagerMode}");
				return;
			}
			
			switch (networkManagerMode)
			{
				case NetworkManagerMode.Offline:
					break;
				
				case NetworkManagerMode.ServerOnly:
					StartServer();
					break;
				
				case NetworkManagerMode.ClientOnly:
					StartClient();
					break;
				
				case NetworkManagerMode.Host:
					StartHost();
					break;
				
				default:
					throw new ArgumentOutOfRangeException(nameof(networkManagerMode), networkManagerMode, null);
			}
		}

		public void StopNetworkManager()
		{
			switch (_networkManagerMode)
			{
				case NetworkManagerMode.Offline:
					break;
				case NetworkManagerMode.ServerOnly:
					StopServer();
					break;
				case NetworkManagerMode.ClientOnly:
					StopClient();
					break;
				case NetworkManagerMode.Host:
					StopHost();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		private Transform GetStartPosition(NetworkConnection networkConnection)
		{
			// first remove any dead transforms
			startPositions.RemoveAll(t => t == null);

			if (startPositions.Count == 0) return null;

			Transform startPosition = null;
			
			switch (_playerSpawnMethod)
			{
				case PlayerSpawnMethodsExtended.Random:
					startPosition = startPositions[Random.Range(0, startPositions.Count)];
					break;
				
				case PlayerSpawnMethodsExtended.RoundRobin:
					startPosition = startPositions[startPositionIndex];
					startPositionIndex = (startPositionIndex + 1) % startPositions.Count;
					break;

				case PlayerSpawnMethodsExtended.FirstFree:
				case PlayerSpawnMethodsExtended.RandomFree:
					
					var availableStartPositions = new List<Transform>();

					for (int i = 0; i < startPositions.Count; i++)
					{
						var position = startPositions[i];

						if (_usedStartPositions.ContainsKey(position))
						{
							var usedBy = _usedStartPositions[position];
							if (usedBy == networkConnection)
							{
								startPosition = position;
								break;
							}
							
							if(_networkConnections.Contains(usedBy)) continue;
							_usedStartPositions.Remove(position);
						}

						availableStartPositions.Add(position);
					}

					if (startPosition != null) break;
					if (availableStartPositions.Count == 0) break;

					startPosition = _playerSpawnMethod == PlayerSpawnMethodsExtended.FirstFree
						? availableStartPositions[0]
						: availableStartPositions[Random.Range(0, availableStartPositions.Count)];
					
					_usedStartPositions.Add(startPosition, networkConnection);

					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			if (startPosition == null)
			{
				Debug.LogWarning($"{name}/{GetType().Name} => " +
				               $"could not find a start position for {networkConnection}");
			}
			
			return startPosition;
		}

		public void ServerChangeScene(BasicSceneAsset sceneAsset)
		{
			ServerChangeScene(sceneAsset.SceneName);
		}
		
		public void ServerChangeScene(BasicSceneAssetSingle sceneAssetSingle)
		{
			ServerChangeScene(sceneAssetSingle.Value.SceneName);
		}
		
	}
}