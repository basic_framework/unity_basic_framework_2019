﻿using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Mirror
{
	[AddComponentMenu("Basic Framework/Third Party/Mirror/Network Behaviour Callbacks")]
	public class NetworkBehaviourCallbacks : NetworkBehaviour 
	{
	
		// Note: Start may be called before OnStartServer/OnStartClient, etc. so we should also raise event in those
		
		//	Network Callback Order
		//		- Awake
		// 		- OnEnable
		//		- OnStartServer
		//		- OnStartAuthority
		//		- OnStartClient
		//		- OnStartLocalPlayer
		//		- Start
		
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private bool _logDebug = default;
		
		[SerializeField] private BoolPlusUnityEvent _isServer = new BoolPlusUnityEvent();
		[SerializeField] private BoolPlusUnityEvent _isServerOnly = new BoolPlusUnityEvent();
		
		[SerializeField] private BoolPlusUnityEvent _hasAuthorityChanged = new BoolPlusUnityEvent();

		[SerializeField] private BoolPlusUnityEvent _isClient = new BoolPlusUnityEvent();
		[SerializeField] private BoolPlusUnityEvent _isClientOnly = new BoolPlusUnityEvent();
		
		[SerializeField] private BoolPlusUnityEvent _isLocalPlayer = new BoolPlusUnityEvent();
		
		[SerializeField] private UnityEvent _onNetworkDestroy = new UnityEvent();
		


		// ****************************************  METHODS  ****************************************\\

		public override void OnStartServer()
		{
			//base.OnStartServer();
			_isServer.Raise(isServer);
			_isServerOnly.Raise(isServerOnly);

			if (!_logDebug) return;
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Start Server");
			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Is Server: {isServer}");
//			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Is Server Only: {isServerOnly}");

		}

		public override void OnStartAuthority()
		{
			//base.OnStartAuthority();
			_hasAuthorityChanged.Raise(hasAuthority);
			
			if (!_logDebug) return;
			
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Start Authority");
			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Has Authority: {hasAuthority}");
		}
		
		public override void OnStopAuthority()
		{
			//base.OnStopAuthority();
			_hasAuthorityChanged.Raise(hasAuthority);
			
			if (!_logDebug) return;
			
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Stop Authority");
			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Has Authority: {hasAuthority}");
		}

		public override void OnStartClient()
		{
			//base.OnStartClient();
			_isClient.Raise(isClient);
			_isServerOnly.Raise(isServerOnly);
			_isClientOnly.Raise(isClientOnly);
			
			if (!_logDebug) return;
			
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Start Client");
			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Is Client: {isClient}");
//			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Has Authority: {hasAuthority}");
//			
//			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
//			          $"=> Has Authority: {hasAuthority}");
		}

		public override void OnStartLocalPlayer()
		{
			//base.OnStartLocalPlayer();
			_isLocalPlayer.Raise(isLocalPlayer);
			
			if (!_logDebug) return;
			
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Start Local Player");
		}
		
		public override void OnNetworkDestroy()
		{
			//base.OnNetworkDestroy();
			_onNetworkDestroy.Invoke();
			
			if (!_logDebug) return;
			
			Debug.Log($"{name}/{GetType().Name.CamelCaseToHumanReadable()} " +
			          $"=> On Network Destroy");
		}
		
		private void Start()
		{
			_isServer.Raise(isServer);
			_isClient.Raise(isClient);
			_isServerOnly.Raise(isServerOnly);
			_isClientOnly.Raise(isClientOnly);
			_isLocalPlayer.Raise(isLocalPlayer);
			_hasAuthorityChanged.Raise(hasAuthority);
		}
		
	}
}