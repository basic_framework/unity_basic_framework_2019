﻿using System;
using System.Net;
using Mirror;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	public class BasicServerResponseBase : MessageBase 
	{
		
		// Add properties for whatever information you want the server to return to
		// clients for them to display or consume for establishing a connection.
		
		// Note: Variables must be public to be serialized
	
		// **************************************** VARIABLES ****************************************\\
		
		// The Universal Resource Identifier (URI) for this server
		public Uri Uri = default;

		// Prevent duplicate server appearance when a connection can be made via LAN on multiple NICs
		public long ServerId = default;

		// Note: NetworkDiscovery can be advertising while NetworkManager is offline
		//		 Let the client know if the server is actually active (i.e. NetworkManagerMode is Host or Server)
		public bool ServerActive = default;
		
		
		// The server that sent this
		// this is a property so that it is not serialized, but the client fills this up after we receive it
		private IPEndPoint _ipEndPoint;
		public IPEndPoint IpEndPoint
		{
			get => _ipEndPoint;
			set => _ipEndPoint = value;
		}
		
		// Used by BasicNetworkDiscoveryBase to timeout the server responses
		private float _timeOfResponse;
		public float TimeOfResponse
		{
			get => _timeOfResponse;
			set => _timeOfResponse = value;
		}
		
		

		// ****************************************  METHODS  ****************************************\\


	}
}