﻿using System;
using System.Collections.Generic;
using System.Net;
using BasicFramework.Core.BasicTypes.Events;
using Mirror;
using Mirror.Discovery;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Mirror
{
	public abstract class BasicNetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse> : NetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse>
		where TBasicServerRequest : BasicServerRequestBase, new()
		where TBasicServerResponse : BasicServerResponseBase, new()
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const float SERVER_TIMEOUT_DELAY_ADDITION = 1f; 
		
		
		[HideInInspector]
		[SerializeField] private float _cachedActiveDiscoveryInterval = default;

		[SerializeField] private UnityEvent _basicServerResponsesChanged = default;

		[SerializeField] private NetworkDiscoveryStatesUnityEvent _networkDiscoveryStateChanged = default;
		
		[SerializeField] private NetworkDiscoveryStatesCompareUnityEvent[] _networkDiscoveryStateChangedCompare = default;

		
		public UnityEvent BasicServerResponsesChanged => _basicServerResponsesChanged;

		public NetworkDiscoveryStatesUnityEvent NetworkDiscoveryStateChanged => _networkDiscoveryStateChanged;


		private NetworkDiscoveryStates _networkDiscoveryState = NetworkDiscoveryStates.Stopped;
		public NetworkDiscoveryStates NetworkDiscoveryState
		{
			get => _networkDiscoveryState;
			private set
			{
				if (_networkDiscoveryState == value) return;
				_networkDiscoveryState = value;
				NetworkDiscoveryStateUpdated(value);
			}
		}

		public NetworkManagerMode NetworkManagerMode => NetworkManager.singleton != null
				? NetworkManager.singleton.mode 
				: NetworkManagerMode.Offline;


		public bool ServerTimeoutEnabled => enableActiveDiscovery;
		public float ServerTimeout => _cachedActiveDiscoveryInterval + SERVER_TIMEOUT_DELAY_ADDITION;
		
		private TBasicServerResponse[] _basicServerResponsesCached = new TBasicServerResponse[0];
		public TBasicServerResponse[] BasicServerResponsesCached => _basicServerResponsesCached;

		public bool ServerFound => BasicServerResponsesCached.Length > 0;
		public TBasicServerResponse FirstResponse => ServerFound ? BasicServerResponsesCached[0] : default;
		

		private readonly List<TBasicServerResponse> _basicServerResponses = new List<TBasicServerResponse>();
		
		private readonly TBasicServerRequest _basicServerRequest = new TBasicServerRequest();
		private readonly TBasicServerResponse _basicServerResponse = new TBasicServerResponse();
		
		public readonly BasicEvent<TBasicServerResponse[]> BasicServerResponsesCachedChanged 
			= new BasicEvent<TBasicServerResponse[]>();

		
		

		// ****************************************  METHODS  ****************************************\\
	
		
		//***** mono *****\\

		private void OnDisable()
		{
			NetworkDiscoveryState = NetworkDiscoveryStates.Stopped;
			StopDiscovery();
		}
		
		public override void Start()
		{
			base.Start();

			var transport = Transport.activeTransport;
			
			try
			{
				_basicServerResponse.ServerId = RandomLong();
				_basicServerResponse.Uri = transport.ServerUri();
			}
			catch (NotImplementedException)
			{
				Debug.LogError($"Transport {transport} does not support network discovery");
				throw;
			}

			NetworkDiscoveryStateUpdated(_networkDiscoveryState);
			BasicServerResponsesUpdated(_basicServerResponses);
		}

		protected virtual void Update()
		{
			UpdateNetworkDiscoveryState();
			
			CheckServerTimeout();
		}


		//***** property updated *****\\
		
		private void NetworkDiscoveryStateUpdated(NetworkDiscoveryStates value)
		{
			OnNetworkDiscoveryStateUpdated(value);

			switch (value)
			{
				case NetworkDiscoveryStates.Stopped:
					ClearServerResponses();
					break;
				case NetworkDiscoveryStates.Searching:
					break;
				case NetworkDiscoveryStates.Advertising:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(value), value, null);
			}
			
			_networkDiscoveryStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _networkDiscoveryStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		private void BasicServerResponsesUpdated(List<TBasicServerResponse> value)
		{
			if (value.Count != _basicServerResponsesCached.Length)
			{
				_basicServerResponsesCached = value.ToArray();
			}

			_basicServerResponsesChanged.Invoke();
			
			BasicServerResponsesCachedChanged.Raise(_basicServerResponsesCached);
		}
		
		//***** virtual *****\\

		protected virtual void OnNetworkDiscoveryStateUpdated(NetworkDiscoveryStates value) {}

		protected virtual void OnGetRequest(TBasicServerRequest request){}
		
		protected virtual void OnProcessRequest(TBasicServerRequest request, IPEndPoint endpoint,
			TBasicServerResponse response){}
		
		protected virtual void OnProcessResponse(TBasicServerResponse response, IPEndPoint endpoint){}
		
		
		 //***** overrides *****\\
        
        //*** server ***\\

        /// <summary>
        /// Process the request from a client
        /// </summary>
        /// <remarks>
        /// Override if you wish to provide more information to the clients
        /// such as the name of the host player
        /// </remarks>
        /// <param name="request">Request coming from client</param>
        /// <param name="endpoint">Address of the client that sent the request</param>
        /// <returns>A message containing information about this server</returns>
        protected sealed override TBasicServerResponse ProcessRequest(TBasicServerRequest request, IPEndPoint endpoint) 
        {
	        OnProcessRequest(request, endpoint, _basicServerResponse);
	        
            var networkManagerMode = NetworkManagerMode;
            
            _basicServerResponse.ServerActive = networkManagerMode == NetworkManagerMode.Host ||
                                                networkManagerMode == NetworkManagerMode.ServerOnly; 
            
            return _basicServerResponse;
        }
    
    
        //*** client ***\\
        
        /// <summary>Create a message that will be broadcasted on the network to discover servers</summary>
        /// <remarks>Override if you wish to include additional data in the discovery message
        /// such as desired game mode, language, difficulty, etc... </remarks>
        /// <returns>An instance of ServerRequest with data to be broadcasted</returns>
        protected sealed override TBasicServerRequest GetRequest()
        {
	        OnGetRequest(_basicServerRequest);
	        
	        return _basicServerRequest;
        }
        
        /// <summary>
        /// Process the answer from a server
        /// </summary>
        /// <remarks>
        /// A client receives a reply from a server, this method processes the
        /// reply and raises an event
        /// </remarks>
        /// <param name="response">Response that came from the server</param>
        /// <param name="endpoint">Address of the server that replied</param>
        protected sealed override void ProcessResponse(TBasicServerResponse response, IPEndPoint endpoint)
        {
	        OnProcessResponse(response, endpoint);
	        
	        // we received a message from the remote endpoint
	        response.IpEndPoint = endpoint;

	        // although we got a supposedly valid url, we may not be able to resolve the provided host
	        // However we know the real ip address of the server because we just received a packet from it, so use that as host.
	        var realUri = new UriBuilder(response.Uri)
	        {
		        Host = response.IpEndPoint.Address.ToString()
	        };
			
	        response.Uri = realUri.Uri;

	        response.TimeOfResponse = Time.time;
			
	        AddServerResponse(response);
        }
        
        
        //***** functions *****\\
        
        private void UpdateNetworkDiscoveryState()
        {
	        if (serverUdpClient != null && serverUdpClient.EnableBroadcast)
	        {
		        NetworkDiscoveryState = NetworkDiscoveryStates.Advertising;
	        }
	        else if (clientUdpClient != null && clientUdpClient.EnableBroadcast)
	        {
		        NetworkDiscoveryState = NetworkDiscoveryStates.Searching;
	        }
	        else
	        {
		        NetworkDiscoveryState = NetworkDiscoveryStates.Stopped;
	        }
        }

        private void CheckServerTimeout()
        {
	        if(!ServerTimeoutEnabled) return;
	        
	        var timeCurrent = Time.time;
	        var timeout = _cachedActiveDiscoveryInterval + 1f;
			
	        for (int i = _basicServerResponses.Count - 1; i >= 0; i--)
	        {
		        var response = _basicServerResponses[i];
				
		        var timeSinceResponse = timeCurrent - response.TimeOfResponse;
				
		        if(timeSinceResponse < timeout) continue;
				
		        _basicServerResponses.RemoveAt(i);
		        BasicServerResponsesUpdated(_basicServerResponses);
	        }
        }

        private void ClearServerResponses()
        {
	        _basicServerResponses.Clear();
	        BasicServerResponsesUpdated(_basicServerResponses);
        }
        
        private void AddServerResponse(TBasicServerResponse response)
        {
	        foreach (var serverResponse in _basicServerResponses)
	        {
		        if(serverResponse.ServerId != response.ServerId) continue;
		        
		        serverResponse.Uri = response.Uri;
		        serverResponse.IpEndPoint = response.IpEndPoint;
		        serverResponse.ServerActive = response.ServerActive;
		        serverResponse.TimeOfResponse = Time.time;
		        
		        BasicServerResponsesUpdated(_basicServerResponses);
		        
		        return;
	        }
	        
	        var newResponse = new TBasicServerResponse()
	        {
		        IpEndPoint = response.IpEndPoint,
		        ServerActive = response.ServerActive,
		        ServerId = response.ServerId,
		        TimeOfResponse = Time.time,
		        Uri = response.Uri
	        };
	        
	        
	        _basicServerResponses.Add(newResponse);
	        BasicServerResponsesUpdated(_basicServerResponses);
        }
	}
}