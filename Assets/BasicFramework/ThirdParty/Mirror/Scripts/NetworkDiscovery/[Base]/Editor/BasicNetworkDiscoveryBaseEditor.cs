﻿using System;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using Mirror;
using Mirror.Discovery;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror.Editor
{
	public abstract class BasicNetworkDiscoveryBaseEditor<TBasicServerRequest, TBasicServerResponse, TBasicNetworkDiscovery> : BasicBaseEditor<TBasicNetworkDiscovery>
		where TBasicServerRequest : BasicServerRequestBase, new()
		where TBasicServerResponse : BasicServerResponseBase, new()
		where TBasicNetworkDiscovery : BasicNetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		private ReorderableList _reorderableListNetworkDiscoveryStateCompare;


		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			var networkDiscoveryStateChangedCompare = serializedObject.FindProperty("_networkDiscoveryStateChangedCompare");

			_reorderableListNetworkDiscoveryStateCompare =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, networkDiscoveryStateChangedCompare, true);
		}
		
		
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			var cachedActiveDiscoveryInterval = serializedObject.FindProperty("_cachedActiveDiscoveryInterval");

			var secretHandshake = serializedObject.FindProperty("secretHandshake");
			
			
			EditorGUILayout.LabelField($"{nameof(secretHandshake).CamelCaseToHumanReadable()}", 
				secretHandshake.longValue.ToString());
			
			EditorGUILayout.LabelField($"{nameof(TypedTarget.NetworkManagerMode).CamelCaseToHumanReadable()}", 
				TypedTarget.NetworkManagerMode.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField($"{nameof(TypedTarget.NetworkDiscoveryState).CamelCaseToHumanReadable()}", 
				TypedTarget.NetworkDiscoveryState.ToString());
				
			EditorGUILayout.LabelField($"{nameof(TypedTarget.ServerTimeout).CamelCaseToHumanReadable()}", 
				TypedTarget.ServerTimeout.ToString());
			
			
			// Connection buttons
			var networkDiscoveryState = TypedTarget.NetworkDiscoveryState;
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying && networkDiscoveryState == NetworkDiscoveryStates.Stopped;
			
			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Start Advertising"))
			{
				TypedTarget.AdvertiseServer();
			}
					
			if (GUILayout.Button("Start Searching"))
			{
				TypedTarget.StartDiscovery();
			}
			
			GUI.enabled = PrevGuiEnabled && Application.isPlaying && networkDiscoveryState != NetworkDiscoveryStates.Stopped;
			
			if (GUILayout.Button("Stop Discovery"))
			{
				TypedTarget.StopDiscovery();
			}
					
			EditorGUILayout.EndHorizontal();

			GUI.enabled = PrevGuiEnabled;
			
			
			

			cachedActiveDiscoveryInterval.isExpanded = EditorGUILayout.Foldout(cachedActiveDiscoveryInterval.isExpanded,
				nameof(TypedTarget.BasicServerResponsesCached).CamelCaseToHumanReadable());

			if (cachedActiveDiscoveryInterval.isExpanded)
			{
				EditorGUI.indentLevel++;

				foreach (var serverResponse in TypedTarget.BasicServerResponsesCached)
				{
					EditorGUILayout.LabelField(serverResponse.Uri.Host, 
						(serverResponse.ServerActive ? "ACTIVE" : "INACTIVE"));
				}
				
				EditorGUI.indentLevel--;
				Space();
			}
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			DrawNetworkDiscoveryBaseFields();
			
//			Space();
//			SectionHeader($"{nameof(BasicNetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse>).CamelCaseToHumanReadable()} Properties");
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();

			var networkDiscoveryStateChanged = serializedObject.FindProperty("_networkDiscoveryStateChanged");
			
			Space();
			SectionHeader($"{nameof(BasicNetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse>).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(networkDiscoveryStateChanged);
			_reorderableListNetworkDiscoveryStateCompare.DoLayoutList();
		}
		
		
		//***** functions *****\\

		private void DrawNetworkDiscoveryBaseFields()
		{
			var serverBroadcastListenPort = serializedObject.FindProperty("serverBroadcastListenPort");
			var enableActiveDiscovery = serializedObject.FindProperty("enableActiveDiscovery");
			var activeDiscoveryInterval = serializedObject.FindProperty("ActiveDiscoveryInterval");

			var cachedActiveDiscoveryInterval = serializedObject.FindProperty("_cachedActiveDiscoveryInterval");

			cachedActiveDiscoveryInterval.floatValue = activeDiscoveryInterval.floatValue;
			
			Space();
			SectionHeader($"{nameof(NetworkDiscoveryBase<TBasicServerRequest, TBasicServerResponse>).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(serverBroadcastListenPort);
			EditorGUILayout.PropertyField(enableActiveDiscovery);
			EditorGUILayout.PropertyField(activeDiscoveryInterval);
		}
		
	}
}