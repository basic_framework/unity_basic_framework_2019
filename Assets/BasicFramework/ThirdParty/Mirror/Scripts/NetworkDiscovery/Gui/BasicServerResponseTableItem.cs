﻿using BasicFramework.Core.BasicGui;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.ThirdParty.Mirror
{
	public class BasicServerResponseTableItem : GuiTableItemBase<BasicServerResponseBase>  
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private Text _textIpAddress = default;
		[SerializeField] private Text _textServerActive = default;


		// ****************************************  METHODS  ****************************************\\

		
		//***** overrides *****\\
		
		protected override void OnUpdateTableItem(BasicServerResponseBase value)
		{
			if (value == null)
			{
				_textIpAddress.text = "NULL";
				_textServerActive.text = "NULL";
				return;
			}
			
			_textIpAddress.text = value.Uri.Host;
			_textServerActive.text = value.ServerActive.ToString();
		}
		
	}
}