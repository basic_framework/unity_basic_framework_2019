﻿using BasicFramework.Core.BasicGui;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	public class BasicServerResponseTableManager : GuiTableManagerBase<BasicServerResponseBase, BasicServerResponseTableItem>
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private BasicNetworkDiscovery _networkDiscovery = default;


		// ****************************************  METHODS  ****************************************\\
	
		
		//***** mono *****\\
		
		private void OnEnable()
		{
			OnServerResponsesChanged(_networkDiscovery.BasicServerResponsesCached);
			
			_networkDiscovery.BasicServerResponsesCachedChanged.Subscribe(OnServerResponsesChanged);
		}

		private void OnDisable()
		{
			_networkDiscovery.BasicServerResponsesCachedChanged.Unsubscribe(OnServerResponsesChanged);
		}


		//***** callbacks *****\\
		
		private void OnServerResponsesChanged(BasicServerResponseBase[] obj)
		{
			UpdateTableItems(obj);
		}

	}
}