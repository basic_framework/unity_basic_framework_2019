﻿using BasicFramework.ThirdParty.Mirror.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Mirror
{
	[CustomEditor(typeof(BasicNetworkDiscovery))]
	public class BasicNetworkDiscoveryEditor : BasicNetworkDiscoveryBaseEditor<BasicServerRequestBase, BasicServerResponseBase, BasicNetworkDiscovery>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}