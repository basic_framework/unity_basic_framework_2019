#SteamVR (Basic Framework Integration)

__Note: Only add this package if you have _SteamVR Plugin_ installed. 
If you do not have it installed you will get errors.
Delete the root folder to remove the errors.__


This plugin integrates Basic Frameworks Core systems with the SteamVR Plugin asset.

As of time of writing, SteamVR still uses the deprecated unity XR support.
Project Settings > Player > XR Settings > Virtual Reality Supported

