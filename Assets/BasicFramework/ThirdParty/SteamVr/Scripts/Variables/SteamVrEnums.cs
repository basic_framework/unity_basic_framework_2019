﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;
using Valve.VR;

namespace BasicFramework.ThirdParty.SteamVr
{
	
	[Serializable]
	public class ETrackingResultUnityEvent : UnityEvent<ETrackingResult>{}

	[Serializable]
	public class ETrackingResultCompareUnityEvent : CompareValueUnityEventBase<ETrackingResult>{}
	
}