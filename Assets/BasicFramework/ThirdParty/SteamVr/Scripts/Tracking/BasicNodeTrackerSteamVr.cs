﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.ConcreteClasses;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.XR;
using UnityEngine;
using Valve.VR;

namespace BasicFramework.ThirdParty.SteamVr
{
	[AddComponentMenu("Basic Framework/Third Party/Steam VR/Basic Node Tracker Steam VR")]
	public class BasicNodeTrackerSteamVr : BasicNodeTrackerXrBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
//		[SerializeField] private SteamVR_Action_Pose _poseAction = SteamVR_Input.GetAction<SteamVR_Action_Pose>("Pose");
	
		[Tooltip("The device this action should apply to. Any if the action is not device specific.")]
		[SerializeField] private SteamVR_Input_Sources _inputSource = SteamVR_Input_Sources.Any;

		[SerializeField] private BoolPlusUnityEvent _deviceConnectedChanged = default;
		[SerializeField] private IntUnityEvent _deviceIndexChanged = default;
		
		[SerializeField] private ETrackingResultUnityEvent _trackingStateChanged = default;
		[SerializeField] private ETrackingResultCompareUnityEvent[] _trackingStateChangedCompare = default;

		
		public SteamVR_Input_Sources InputSource => _inputSource;

		public BoolUnityEvent DeviceConnectedChanged => _deviceConnectedChanged.Value;

		public IntUnityEvent DeviceIndexChanged => _deviceIndexChanged;

		public ETrackingResultUnityEvent TrackingStateChanged => _trackingStateChanged;

		
		private readonly SteamVR_Action_Pose _poseAction = SteamVR_Input.GetAction<SteamVR_Action_Pose>("Pose");

		private bool _deviceConnected;
		public bool DeviceConnected
		{
			get => _deviceConnected;
			private set
			{
				if (_deviceConnected == value) return;
				_deviceConnected = value;
				DeviceConnectedUpdated(value);
			}
		}

		private ETrackingResult _trackingState;
		public ETrackingResult TrackingState
		{
			get => _trackingState;
			private set
			{
				if (_trackingState == value) return;
				_trackingState = value;
				TrackingStateUpdated(value);
			}
		}

		private int _deviceIndex = -1;
		public int DeviceIndex
		{
			get => _deviceIndex;
			private set
			{
				if (_deviceIndex == value) return;
				_deviceIndex = value;
				DeviceIndexUpdated(value);
			}
		}
		
//		private Vector3 _localPosition;
//		private Quaternion _localRotation;
		

		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Awake()
		{
			SteamVR.Initialize();
		}

		private void OnEnable()
		{
			OnDeviceConnectedChanged(_poseAction, _inputSource, _poseAction[_inputSource].deviceIsConnected);
			OnTrackingChanged(_poseAction, _inputSource, _poseAction[_inputSource].trackingState);
//			OnUpdate(_poseAction, _inputSource);
			
			_poseAction[_inputSource].onDeviceConnectedChanged += OnDeviceConnectedChanged;
			_poseAction[_inputSource].onTrackingChanged += OnTrackingChanged;
//			_poseAction[_inputSource].onUpdate += OnUpdate;
//			_poseAction[_inputSource].onChange += OnChange;
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			
			_poseAction[_inputSource].onDeviceConnectedChanged -= OnDeviceConnectedChanged;
			_poseAction[_inputSource].onTrackingChanged -= OnTrackingChanged;
//			_poseAction[_inputSource].onUpdate -= OnUpdate;
//			_poseAction[_inputSource].onChange -= OnChange;
		}

		protected override void Start()
		{
			base.Start();
			
			DeviceConnectedUpdated(_deviceConnected);
			TrackingStateUpdated(_trackingState);
			DeviceIndexUpdated(_deviceIndex);
		}
		
		
		//***** overrides *****\\

		protected override bool GetIsTracking() => _trackingState == ETrackingResult.Running_OK;

		protected override BasicPose GetLocalPose()
		{
			var poseSource = _poseAction[_inputSource];
			return new BasicPose(poseSource.localPosition, poseSource.localRotation);
		}

//		protected override Vector3 GetLocalPosition() => _poseAction[_inputSource].localPosition;
//
//		protected override Quaternion GetLocalRotation() => _poseAction[_inputSource].localRotation;
		

		//***** callbacks *****\\
	
		private void OnDeviceConnectedChanged(SteamVR_Action_Pose fromaction, SteamVR_Input_Sources fromsource, bool deviceconnected)
		{
			UpdateDeviceIndex();

			DeviceConnected = deviceconnected;
		}
	
		private void OnTrackingChanged(SteamVR_Action_Pose fromaction, SteamVR_Input_Sources fromsource, ETrackingResult trackingstate)
		{
			TrackingState = trackingstate;
		}
	
//		private void OnUpdate(SteamVR_Action_Pose fromaction, SteamVR_Input_Sources fromsource)
//		{
////			UpdateDeviceIndex();
//
//			_localPosition = fromaction[fromsource].localPosition;
//			_localRotation = fromaction[fromsource].localRotation;
//		}
	
//		private void OnChange(SteamVR_Action_Pose fromaction, SteamVR_Input_Sources fromsource)
//		{
//			Debug.Log("Pose Changed");
//			//throw new NotImplementedException();
//		}
	
	
		//***** property updates *****\\
	
		private void DeviceConnectedUpdated(bool value)
		{
			_deviceConnectedChanged.Raise(value);
		}
	
		private void TrackingStateUpdated(ETrackingResult value)
		{
			_trackingStateChanged.Invoke(value);

			foreach (var compareUnityEvent in _trackingStateChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
	
		private void DeviceIndexUpdated(int value)
		{
			_deviceIndexChanged.Invoke(value);
		}
	
	
		//***** functions *****\\
	
		protected virtual void UpdateDeviceIndex()
		{
			if (!_poseAction[_inputSource].active) return;
			if (!_poseAction[_inputSource].deviceIsConnected) return;
		
			DeviceIndex = (int)_poseAction[_inputSource].trackedDeviceIndex;
		}
		
	}
}