﻿using BasicFramework.Core.XR;
using UnityEngine;
using Valve.VR;

namespace BasicFramework.ThirdParty.SteamVr
{
	[AddComponentMenu("Basic Framework/Third Party/Steam VR/Basic Rig Steam VR")]
	public class BasicRigSteamVr : BasicRigXrBase 
	{
	
		//***** Requires SteamVR Plugin asset *****\\
		
		// **************************************** VARIABLES ****************************************\\
		


		// ****************************************  METHODS  ****************************************\\
	
		//***** mono *****\\

		private void Awake()
		{
			SteamVR.Initialize();
		}
		
		//***** overrides *****\\

		protected override bool GetUserPresent() =>
			SteamVR.instance != null
			&& SteamVR.instance.GetHeadsetActivityLevel() == EDeviceActivityLevel.k_EDeviceActivityLevel_UserInteraction;
	}
	
}