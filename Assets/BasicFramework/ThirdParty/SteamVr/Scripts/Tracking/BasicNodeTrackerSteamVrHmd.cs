﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.XR;
using UnityEngine;
using Valve.VR;

namespace BasicFramework.ThirdParty.SteamVr
{
    [AddComponentMenu("Basic Framework/Third Party/Steam VR/Basic Node Tracker Steam VR HMD")]
    public class BasicNodeTrackerSteamVrHmd : BasicNodeTrackerXrBase 
    {
        // Note: Hmd tracking is handled differently to controllers in SteamVR, that why we are using a seperate class
	
        // **************************************** VARIABLES ****************************************\\

		
        // ****************************************  METHODS  ****************************************\\

        //***** overrides *****\\

        protected override bool GetIsTracking() => !SteamVR.outOfRange;

        // TODO: in the future get this value from the tracking system
        protected override BasicPose GetLocalPose()
        {
            return new BasicPose(TransformTracked.localPosition, TransformTracked.localRotation);
        }
		
    }
}