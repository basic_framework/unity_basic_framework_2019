﻿using BasicFramework.Core.XR.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.SteamVr.Editor
{
	[CustomEditor(typeof(BasicRigSteamVr))]
	public class BasicRigSteamVrEditor : BasicRigXrBaseEditor<BasicRigSteamVr>
	{
	
		// **************************************** VARIABLES ****************************************\\


		// ****************************************  METHODS  ****************************************\\
		
		
	}
}