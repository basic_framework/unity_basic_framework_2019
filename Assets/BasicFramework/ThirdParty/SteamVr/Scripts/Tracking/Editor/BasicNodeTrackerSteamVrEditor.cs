﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using BasicFramework.Core.XR.Editor;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.ThirdParty.SteamVr.Editor
{
	[CustomEditor(typeof(BasicNodeTrackerSteamVr))]
	public class BasicNodeTrackerSteamVrEditor : BasicNodeTrackerXrBaseEditor<BasicNodeTrackerSteamVr>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _reorderableListTrackingState;
		
		protected override bool HasDebugSection => true;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var trackingStateChangedCompare = serializedObject.FindProperty("_trackingStateChangedCompare");

			_reorderableListTrackingState =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, trackingStateChangedCompare, true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerSteamVr).CamelCaseToHumanReadable()} Debug");
			
			EditorGUILayout.LabelField(nameof(TypedTarget.DeviceConnected).CamelCaseToHumanReadable(), 
				TypedTarget.DeviceConnected.ToString());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.DeviceIndex).CamelCaseToHumanReadable(), 
				TypedTarget.DeviceIndex.ToString());
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingState).CamelCaseToHumanReadable(), 
				TypedTarget.TrackingState.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			//var poseAction = serializedObject.FindProperty("_poseAction");
			var inputSource = serializedObject.FindProperty("_inputSource");

			Space();
			SectionHeader($"{nameof(BasicNodeTrackerSteamVr).CamelCaseToHumanReadable()} Properties");

			//EditorGUILayout.PropertyField(poseAction);
			EditorGUILayout.PropertyField(inputSource);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			
			var deviceConnectedChanged = serializedObject.FindProperty("_deviceConnectedChanged");
			var deviceIndexChanged = serializedObject.FindProperty("_deviceIndexChanged");
			
			var trackingStateChanged = serializedObject.FindProperty("_trackingStateChanged");
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerSteamVr).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(deviceConnectedChanged);
			EditorGUILayout.PropertyField(deviceIndexChanged);
			
			EditorGUILayout.PropertyField(trackingStateChanged);
			_reorderableListTrackingState.DoLayoutList();
		}
	}
}