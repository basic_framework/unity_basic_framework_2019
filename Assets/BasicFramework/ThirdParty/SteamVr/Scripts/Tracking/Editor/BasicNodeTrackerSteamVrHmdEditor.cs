﻿using BasicFramework.Core.XR.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.SteamVr.Editor
{
	[CustomEditor(typeof(BasicNodeTrackerSteamVrHmd))]
	public class BasicNodeTrackerSteamVrHmdEditor : BasicNodeTrackerXrBaseEditor<BasicNodeTrackerSteamVrHmd>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
	
	
	}
}