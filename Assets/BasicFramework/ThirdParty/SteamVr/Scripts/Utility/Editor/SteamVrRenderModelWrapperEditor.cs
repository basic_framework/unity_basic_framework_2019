﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;

namespace BasicFramework.ThirdParty.SteamVr.Utility.Editor
{
	[CustomEditor(typeof(SteamVrRenderModelWrapper))]
	public class SteamVrRenderModelWrapperEditor : BasicBaseEditor<SteamVrRenderModelWrapper>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var steamVrNodeTracker = serializedObject.FindProperty("_nodeTrackerSteamVr");
			var steamVrRenderModel = serializedObject.FindProperty("_steamVrRenderModel");
			
			Space();
			SectionHeader($"{nameof(SteamVrRenderModelWrapper).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(steamVrNodeTracker);
			BasicEditorGuiLayout.PropertyFieldNotNull(steamVrRenderModel);
		}
	}
}