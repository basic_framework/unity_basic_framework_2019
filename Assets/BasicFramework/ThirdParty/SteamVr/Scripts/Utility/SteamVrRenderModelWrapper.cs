﻿using UnityEngine;
using Valve.VR;

namespace BasicFramework.ThirdParty.SteamVr.Utility
{
	[AddComponentMenu("Basic Framework/Third Party/Steam VR/Utility/Steam VR Render Model Wrapper")]
	public class SteamVrRenderModelWrapper : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private BasicNodeTrackerSteamVr _nodeTrackerSteamVr = default;

		[SerializeField] private SteamVR_RenderModel _steamVrRenderModel = default;


		// ****************************************  METHODS  ****************************************\\

		//***** mono *****\\
		
		private void Reset()
		{
			_nodeTrackerSteamVr = GetComponentInParent<BasicNodeTrackerSteamVr>();
			_steamVrRenderModel = GetComponentInChildren<SteamVR_RenderModel>();
		}

		private void OnEnable()
		{
			OnDeviceIndexChanged(_nodeTrackerSteamVr.DeviceIndex);
			
			_nodeTrackerSteamVr.DeviceIndexChanged.AddListener(OnDeviceIndexChanged);
		}
		
		private void OnDisable()
		{
			_nodeTrackerSteamVr.DeviceIndexChanged.RemoveListener(OnDeviceIndexChanged);
		}
		
		private void Start()
		{
			_steamVrRenderModel.SetInputSource(_nodeTrackerSteamVr.InputSource);
		}
		
		
		//***** callbacks *****\\
		
		private void OnDeviceIndexChanged(int deviceIndex)
		{
			_steamVrRenderModel.SetDeviceIndex(deviceIndex);
		}

	}
}