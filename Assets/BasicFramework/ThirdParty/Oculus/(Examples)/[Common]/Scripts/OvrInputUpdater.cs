﻿using System;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Examples
{
	public class OvrInputUpdater : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		private void Update()
		{
			OVRInput.Update();
		}
	}
}