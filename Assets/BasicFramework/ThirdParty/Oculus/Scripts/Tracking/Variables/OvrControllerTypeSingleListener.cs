﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
    [AddComponentMenu("Basic Framework/Third Party/Oculus/Ovr Controller Type Single Listener")]
    public class OvrControllerTypeSingleListener : SingleStructListenerBase
    <
        OvrControllerTypes, 
        OvrControllerTypeSingle, 
        OvrControllerTypesUnityEvent, 
        OvrControllerTypesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}