﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Oculus
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Third Party/Oculus/Ovr Tracking Confidence Single", 
        fileName = "single_ovr_tracking_confidence_")
    ]
    public class OvrTrackingConfidenceSingle : SingleStructBase<OVRPlugin.TrackingConfidence> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(OvrTrackingConfidenceSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class OvrTrackingConfidenceSingleReference : SingleStructReferenceBase<OVRPlugin.TrackingConfidence, OvrTrackingConfidenceSingle>
    {
        public OvrTrackingConfidenceSingleReference()
        {
        }

        public OvrTrackingConfidenceSingleReference(OVRPlugin.TrackingConfidence value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class OvrTrackingConfidenceSingleUnityEvent : UnityEvent<OvrTrackingConfidenceSingle>{}
	
    [Serializable]
    public class OvrTrackingConfidenceSingleArrayUnityEvent : UnityEvent<OvrTrackingConfidenceSingle[]>{}
    
}