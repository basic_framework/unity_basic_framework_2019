﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;


namespace BasicFramework.ThirdParty.Oculus
{
    [CustomEditor(typeof(OvrTrackingConfidenceSingle))][CanEditMultipleObjects]
    public class OvrTrackingConfidenceSingleEditor : SingleStructBaseEditor<OVRPlugin.TrackingConfidence, OvrTrackingConfidenceSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(OvrTrackingConfidenceSingle))]
    public class OvrTrackingConfidenceSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(OvrTrackingConfidenceSingleReference))]
    public class OvrTrackingConfidenceSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}