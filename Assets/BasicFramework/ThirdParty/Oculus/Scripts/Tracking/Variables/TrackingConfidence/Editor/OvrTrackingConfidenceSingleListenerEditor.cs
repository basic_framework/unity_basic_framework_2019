﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Oculus
{
    [CustomEditor(typeof(OvrTrackingConfidenceSingleListener))][CanEditMultipleObjects]
    public class OvrTrackingConfidenceSingleListenerEditor : SingleStructListenerBaseEditor
    <
        OVRPlugin.TrackingConfidence, 
        OvrTrackingConfidenceSingle, 
        OvrTrackingConfidenceUnityEvent,
        OvrTrackingConfidenceCompareUnityEvent,
        OvrTrackingConfidenceSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}