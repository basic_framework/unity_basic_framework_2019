﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrTrackingConfidenceUnityEvent : UnityEvent<OVRPlugin.TrackingConfidence>{}

	[Serializable]
	public class OvrTrackingConfidenceCompareUnityEvent : CompareValueUnityEventBase<OVRPlugin.TrackingConfidence>{}
	
}