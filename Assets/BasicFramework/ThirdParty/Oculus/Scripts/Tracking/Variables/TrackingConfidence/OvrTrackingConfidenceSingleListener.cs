﻿using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
    [AddComponentMenu("Basic Framework/Third Party/Oculus/Ovr Tracking Confidence Single Listener")]
    public class OvrTrackingConfidenceSingleListener : SingleStructListenerBase
    <
        OVRPlugin.TrackingConfidence, 
        OvrTrackingConfidenceSingle, 
        OvrTrackingConfidenceUnityEvent, 
        OvrTrackingConfidenceCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}