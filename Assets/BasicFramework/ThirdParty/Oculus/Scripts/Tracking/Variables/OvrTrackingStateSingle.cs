using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Oculus
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Third Party/Oculus/Ovr Tracking State Single",
        fileName = "single_ovr_tracking_state_")
    ]
    public class OvrTrackingStateSingle : SingleStructBase<OvrTrackingStates> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(OvrTrackingStateSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class OvrTrackingStateSingleReference : SingleStructReferenceBase<OvrTrackingStates, OvrTrackingStateSingle>
    {
        public OvrTrackingStateSingleReference()
        {
        }

        public OvrTrackingStateSingleReference(OvrTrackingStates value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class OvrTrackingStateSingleUnityEvent : UnityEvent<OvrTrackingStateSingle>{}
	
    [Serializable]
    public class OvrTrackingStateSingleArrayUnityEvent : UnityEvent<OvrTrackingStateSingle[]>{}
    
}