using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
    [CustomEditor(typeof(OvrTrackingStateSingleListener))][CanEditMultipleObjects]
    public class OvrTrackingStateSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    OvrTrackingStates, 
	    OvrTrackingStateSingle, 
	    OvrTrackingStatesUnityEvent,
	    OvrTrackingStatesCompareUnityEvent,
	    OvrTrackingStateSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}