using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
    [CustomEditor(typeof(OvrTrackingStateSingle))][CanEditMultipleObjects]
    public class OvrTrackingStateSingleEditor : SingleStructBaseEditor<OvrTrackingStates, OvrTrackingStateSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(OvrTrackingStateSingle))]
    public class OvrTrackingStateSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(OvrTrackingStateSingleReference))]
    public class OvrTrackingStateSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}