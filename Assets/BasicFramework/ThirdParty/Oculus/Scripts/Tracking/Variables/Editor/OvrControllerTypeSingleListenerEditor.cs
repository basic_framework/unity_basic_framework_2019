﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
    [CustomEditor(typeof(OvrControllerTypeSingleListener))][CanEditMultipleObjects]
    public class OvrControllerTypeSingleListenerEditor : SingleStructListenerBaseEditor
    <
	    OvrControllerTypes, 
	    OvrControllerTypeSingle, 
	    OvrControllerTypesUnityEvent,
	    OvrControllerTypesCompareUnityEvent,
	    OvrControllerTypeSingleListener
    >
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        
    }
}