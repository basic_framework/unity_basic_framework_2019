﻿using BasicFramework.Core.ScriptableVariables.Single.Editor;
using UnityEditor;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
    [CustomEditor(typeof(OvrControllerTypeSingle))][CanEditMultipleObjects]
    public class OvrControllerTypeSingleEditor : SingleStructBaseEditor<OvrControllerTypes, OvrControllerTypeSingle>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

    }
	
    [CustomPropertyDrawer(typeof(OvrControllerTypeSingle))]
    public class OvrControllerTypeSingleBasePropertyDrawer : ScriptableSingleBasePropertyDrawer 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
		
    }
	
    [CustomPropertyDrawer(typeof(OvrControllerTypeSingleReference))]
    public class OvrControllerTypeSingleReferenceBaseDrawer : ScriptableSingleReferenceBasePropertyDrawer
    {
    }
	
}