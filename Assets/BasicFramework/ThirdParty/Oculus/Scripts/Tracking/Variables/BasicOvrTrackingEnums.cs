﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Oculus
{
	[Flags]
	public enum UpdateModes
	{
		Update = 1 << 0,
		BeforeRender = 1 << 1,
	}
	
	
	public enum OvrTrackingStates
	{
		Stopped,
		Lost,
		Tracking
	}
	
	[Serializable]
	public class OvrTrackingStatesUnityEvent : UnityEvent<OvrTrackingStates>{}

	[Serializable]
	public class OvrTrackingStatesCompareUnityEvent : CompareValueUnityEventBase<OvrTrackingStates>{}

	
	public enum OvrControllerTypes
	{
		Touch,
		Hand
	}
	
	[Serializable]
	public class OvrControllerTypesUnityEvent : UnityEvent<OvrControllerTypes>{}

	[Serializable]
	public class OvrControllerTypesCompareUnityEvent : CompareValueUnityEventBase<OvrControllerTypes>{}


	public enum OvrTrackingOriginTypes
	{
		EyeLevel,
		FloorLevel,
		Stage
	}
	
	
	[Serializable]
	public class OvrTrackingOriginTypesUnityEvent : UnityEvent<OvrTrackingOriginTypes>{}

	[Serializable]
	public class OvrTrackingOriginTypesCompareUnityEvent : CompareValueUnityEventBase<OvrTrackingOriginTypes>{}
	
}


