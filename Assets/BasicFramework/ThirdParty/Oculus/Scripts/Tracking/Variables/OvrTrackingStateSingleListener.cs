using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
    [AddComponentMenu("Basic Framework/Third Party/Oculus/Ovr Tracking State Single Listener")]
    public class OvrTrackingStateSingleListener : SingleStructListenerBase
    <
        OvrTrackingStates, 
        OvrTrackingStateSingle, 
        OvrTrackingStatesUnityEvent, 
        OvrTrackingStatesCompareUnityEvent
    >
    {
	
        // **************************************** VARIABLES ****************************************\\


        // ****************************************  METHODS  ****************************************\\
        

    }
}