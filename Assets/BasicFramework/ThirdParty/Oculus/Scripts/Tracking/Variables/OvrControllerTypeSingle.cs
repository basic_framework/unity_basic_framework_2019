﻿using System;
using BasicFramework.Core.ScriptableVariables.Single;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.Oculus
{
    [CreateAssetMenu(
        menuName = "Basic Framework/Third Party/Oculus/Ovr Controller Type Single", 
        fileName = "single_ovr_controller_type_")
    ]
    public class OvrControllerTypeSingle : SingleStructBase<OvrControllerTypes> 
    {

        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\

        public void SetValue(OvrControllerTypeSingle value)
        {
            Value = value.Value;
        }

    }

    [Serializable]
    public class OvrControllerTypeSingleReference : SingleStructReferenceBase<OvrControllerTypes, OvrControllerTypeSingle>
    {
        public OvrControllerTypeSingleReference()
        {
        }

        public OvrControllerTypeSingleReference(OvrControllerTypes value)
        {
            _constantValue = value;
        }
    }
	
    [Serializable]
    public class OvrControllerTypeSingleUnityEvent : UnityEvent<OvrControllerTypeSingle>{}
	
    [Serializable]
    public class OvrControllerTypeSingleArrayUnityEvent : UnityEvent<OvrControllerTypeSingle[]>{}
    
}