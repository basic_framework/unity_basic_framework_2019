﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using BasicFramework.Core.XR.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomEditor(typeof(BasicRigOvr))]
	public class BasicRigOvrEditor : BasicRigXrBaseEditor<BasicRigOvr>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _trackedControllerChangedReorderableList;
		private ReorderableList _trackingOriginChangedReorderableList;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			_trackedControllerChangedReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(
					serializedObject, 
					serializedObject.FindProperty("_trackedControllerTypeChangedCompare"),
					true);

			_trackingOriginChangedReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(
					serializedObject, 
					serializedObject.FindProperty("_trackingOriginTypeChangedCompare"),
					true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			Space();
			SectionHeader($"{nameof(BasicRigOvr).CamelCaseToHumanReadable()} Debug");
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackedControllerType).CamelCaseToHumanReadable(), 
				TypedTarget.TrackedControllerType.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var trackingOriginType = serializedObject.FindProperty("_trackingOriginType");
			
			Space();
			SectionHeader($"{nameof(BasicRigOvr).CamelCaseToHumanReadable()} Properties");
			
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(trackingOriginType);
			RaiseEditorUpdate = EditorGUI.EndChangeCheck() || RaiseEditorUpdate;
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var trackingOriginChanged = serializedObject.FindProperty("_trackingOriginTypeChanged");
			var trackedControllerTypeChanged = serializedObject.FindProperty("_trackedControllerTypeChanged");
			
			Space();
			SectionHeader($"{nameof(BasicRigOvr).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(trackingOriginChanged);
			_trackingOriginChangedReorderableList.DoLayoutList();
			
			Space();
			EditorGUILayout.PropertyField(trackedControllerTypeChanged);
			_trackedControllerChangedReorderableList.DoLayoutList();
		}

		protected override void OnRaiseEditorUpdate()
		{
			base.OnRaiseEditorUpdate();
			
			TypedTarget.EditorUpdate();
		}
	}
}