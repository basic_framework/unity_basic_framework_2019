﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using BasicFramework.Core.XR.Editor;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomEditor(typeof(BasicNodeTrackerOvr))]
	public class BasicNodeTrackerOvrEditor : BasicNodeTrackerXrBaseEditor<BasicNodeTrackerOvr>
	{
	
		// **************************************** VARIABLES ****************************************\\

		private ReorderableList _trackingStateChangedReorderableList;

		protected override bool HasDebugSection => true;


		// ****************************************  METHODS  ****************************************\\
		
		protected override void OnEnable()
		{
			base.OnEnable();

			var trackingStateChangedEvents = serializedObject.FindProperty("_trackingStateChangedCompare");
			
			_trackingStateChangedReorderableList =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, trackingStateChangedEvents, true);
		}
		
		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerOvr).CamelCaseToHumanReadable()} Debug");
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingState).CamelCaseToHumanReadable(),
				TypedTarget.TrackingState.ToString().CamelCaseToHumanReadable());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var trackedNode = serializedObject.FindProperty("_trackedNode");
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerOvr).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(trackedNode);
		}


		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var trackingStateChanged = serializedObject.FindProperty("_trackingStateChanged");
			
			Space();
			SectionHeader($"{nameof(BasicNodeTrackerOvr).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(trackingStateChanged);
			_trackingStateChangedReorderableList.DoLayoutList();
		}
		
	}
}