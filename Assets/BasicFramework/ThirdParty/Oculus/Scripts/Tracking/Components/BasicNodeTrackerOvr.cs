﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.XR;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[AddComponentMenu("Basic Framework/Third Party/Oculus/Basic Node Tracker OVR")]
	public class BasicNodeTrackerOvr : BasicNodeTrackerXrBase 
	{
		//***** Requires Oculus Integration asset *****\\

		// **************************************** VARIABLES ****************************************\\
		
		[Tooltip("The type of node to track")]
		[SerializeField] private OVRPlugin.Node _trackedNode = OVRPlugin.Node.EyeCenter;
		
		[SerializeField] private OvrTrackingStatesUnityEvent _trackingStateChanged = new OvrTrackingStatesUnityEvent();
		[SerializeField] private OvrTrackingStatesCompareUnityEvent[] _trackingStateChangedCompare = new OvrTrackingStatesCompareUnityEvent[0];
		

		public OVRPlugin.Node TrackedNode
		{
			get => _trackedNode;
			set => _trackedNode = value;
		}

		public OvrTrackingStatesUnityEvent TrackingStateChanged => _trackingStateChanged;
		
		private OvrTrackingStates _trackingState = OvrTrackingStates.Stopped;
		public OvrTrackingStates TrackingState
		{
			get => _trackingState;
			private set
			{
				if (_trackingState == value) return;
				_trackingState = value;
				TrackingStateUpdated(_trackingState);
			}
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\
		
		protected override void OnDisable()
		{
			base.OnDisable();
			
			TrackingState = OvrTrackingStates.Stopped;
		}

		protected override void Start()
		{
			base.Start();
			
			TrackingStateUpdated(_trackingState);
		}

		protected override void Update()
		{
			TrackingState = OVRPlugin.GetNodePositionTracked(_trackedNode) ? OvrTrackingStates.Tracking : OvrTrackingStates.Lost;

			base.Update();
		}


		//***** overrides *****\\
		
		protected override bool GetIsTracking() => _trackingState == OvrTrackingStates.Tracking;

		protected override BasicPose GetLocalPose()
		{
			var ovrPose = OVRPlugin.GetNodePose(_trackedNode, OVRPlugin.Step.Render).ToOVRPose();
			return new BasicPose(ovrPose.position, ovrPose.orientation);
		}


		//***** property updated *****\\
		
		private void TrackingStateUpdated(OvrTrackingStates trackingState)
		{
			_trackingStateChanged.Invoke(trackingState);

			foreach (var trackingStateChangedEvent in _trackingStateChangedCompare)
			{
				trackingStateChangedEvent.SetCurrentValue(trackingState);
			}
		}
		
	}
}