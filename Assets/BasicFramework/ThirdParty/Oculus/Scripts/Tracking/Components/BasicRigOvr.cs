﻿using System;
using System.Collections;
using BasicFramework.Core.XR;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[AddComponentMenu("Basic Framework/Third Party/Oculus/Basic Rig OVR")]
	public class BasicRigOvr : BasicRigXrBase 
	{
		
		//***** Requires Oculus Integration asset *****\\
		
		// **************************************** VARIABLES ****************************************\\

		private const int DELAY_FRAMES_SET_OVR_PLUGIN = 2;

		
		// properties
		[Tooltip("The tracking origin type")]
		[SerializeField] private OvrTrackingOriginTypes _trackingOriginType = OvrTrackingOriginTypes.FloorLevel;
		
		// events
		[Tooltip("Raised when the tracking type changes")]
		[SerializeField] private OvrTrackingOriginTypesUnityEvent _trackingOriginTypeChanged = default;
		
		[Tooltip("Raised when the tracking type changes and the condition is met")]
		[SerializeField] private OvrTrackingOriginTypesCompareUnityEvent[] _trackingOriginTypeChangedCompare = default;
		
		[Tooltip("Raised when the tracked controller type changes (e.g. Touch, Hand, etc...)")]
		[SerializeField] private OvrControllerTypesUnityEvent _trackedControllerTypeChanged = default;
		
		[Tooltip("Raised when the tracked controller type changes (e.g. Touch, Hand, etc...) and the condition is met")]
		[SerializeField] private OvrControllerTypesCompareUnityEvent[] _trackedControllerTypeChangedCompare = default;
		
		
		public OvrTrackingOriginTypes TrackingOriginType
		{
			get => _trackingOriginType;
			set
			{
				if (_trackingOriginType == value) return;
				_trackingOriginType = value;
				TrackingOriginTypeUpdated(_trackingOriginType);
			}
		}

		public OvrTrackingOriginTypesUnityEvent TrackingOriginTypeChanged => _trackingOriginTypeChanged;
		
		public OvrControllerTypesUnityEvent TrackedControllerTypeChanged => _trackedControllerTypeChanged;
		
		
		private OvrControllerTypes _trackedControllerType = OvrControllerTypes.Touch;
		public OvrControllerTypes TrackedControllerType
		{
			get => _trackedControllerType;
			private set
			{
				if (_trackedControllerType == value) return;
				_trackedControllerType = value;
				TrackedControllerTypeUpdated(_trackedControllerType);
			}
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\

		protected override void Start()
		{
			base.Start();
			
			OvrInputManager.CreateOvrInputManager();
			
			TrackingOriginTypeUpdated(_trackingOriginType);
			
			TrackedControllerTypeUpdated(_trackedControllerType);
			
			// OVRPlugin needs to initialize before we can set some variables
			// Wait a few frames then do it
			StartCoroutine(DelayedSetOvrPluginStatesCoroutine());
		}

		protected override void Update()
		{
			base.Update();
			
			TrackedControllerType = GetTrackedControllerType();
		}

		
		//***** override *****\\

		protected override bool GetUserPresent() => OVRPlugin.userPresent;


		//***** Property Updated *****\\
		
		private void TrackingOriginTypeUpdated(OvrTrackingOriginTypes value)
		{
			OVRPlugin.TrackingOrigin ovrPluginOriginTypes; 
			
			switch (value)
			{
				case OvrTrackingOriginTypes.EyeLevel:
					ovrPluginOriginTypes = OVRPlugin.TrackingOrigin.EyeLevel;
					break;
				case OvrTrackingOriginTypes.FloorLevel:
					ovrPluginOriginTypes = OVRPlugin.TrackingOrigin.FloorLevel;
					break;
				case OvrTrackingOriginTypes.Stage:
					ovrPluginOriginTypes = OVRPlugin.TrackingOrigin.Stage;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(value), value, null);
			}
			
			OVRPlugin.SetTrackingOriginType(ovrPluginOriginTypes);
			
			_trackingOriginTypeChanged.Invoke(value);

			foreach (var compareUnityEvent in _trackingOriginTypeChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}

		private void TrackedControllerTypeUpdated(OvrControllerTypes ovrControllerType)
		{
			_trackedControllerTypeChanged.Invoke(ovrControllerType);

			foreach (var trackedControllerTypeChangedEvent in _trackedControllerTypeChangedCompare)
			{
				trackedControllerTypeChangedEvent.SetCurrentValue(ovrControllerType);
			}
		}

		
		//***** functions *****\\
		
		private OvrControllerTypes GetTrackedControllerType()
		{
			// If no controller tracked it will be the same as the last frame
			// Note: This will keep hands as last controller even if they went out of view
			if (!TrackingControllerHandLeft && !TrackingControllerHandRight) return _trackedControllerType;
			
			// TODO: Add controller types as necessary
			if (!OVRPlugin.GetHandTrackingEnabled())
			{
				return OvrControllerTypes.Touch;
			}

			if (TrackingControllerHandLeft &&
			    OvrInputManager.Instance.GetHandStatus(OVRPlugin.Hand.HandLeft, OVRPlugin.HandStatus.HandTracked))
			{
				return OvrControllerTypes.Hand;
			}

			if (TrackingControllerHandRight &&
			    OvrInputManager.Instance.GetHandStatus(OVRPlugin.Hand.HandRight, OVRPlugin.HandStatus.HandTracked))
			{
				return OvrControllerTypes.Hand;
			}
			
			return OvrControllerTypes.Touch;
		}

		private IEnumerator DelayedSetOvrPluginStatesCoroutine()
		{
			for (int i = 0; i < DELAY_FRAMES_SET_OVR_PLUGIN; i++)
			{
				yield return null;
			}
			
			TrackingOriginTypeUpdated(_trackingOriginType);
		}
		
		
		//***** editor *****\\

#if UNITY_EDITOR
		public void EditorUpdate()
		{
			if (!Application.isPlaying) return;
			
			TrackingOriginTypeUpdated(_trackingOriginType);
		}
#endif
		
	}
}