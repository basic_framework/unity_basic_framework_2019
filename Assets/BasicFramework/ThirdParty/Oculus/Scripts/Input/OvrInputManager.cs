﻿using System;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[AddComponentMenu("")]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_INPUT_MANAGER - 1)]
	public class OvrInputManager : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		private static OvrInputManager _instance;
		public static OvrInputManager Instance
		{
			get
			{
				if (_isQuitting) return null;
				
				if (_instance != null) return _instance;
				
				_instance = FindObjectOfType<OvrInputManager>();
				if (_instance != null) return _instance;
				
				var go = new GameObject("[OvrInputManager]");
				_instance = go.AddComponent<OvrInputManager>();
				return _instance;
			}
		}
		
		private static bool _isQuitting;
		
		
		private readonly BasicOvrHandData _handDataLeft = new BasicOvrHandData(OVRPlugin.Hand.HandLeft);
		public BasicOvrHandData HandDataLeft => _handDataLeft;
		
		private readonly BasicOvrHandData _handDataRight = new BasicOvrHandData(OVRPlugin.Hand.HandRight);
		public BasicOvrHandData HandDataRight => _handDataRight;

		[SerializeField] private bool _editorExpandLeft = default;
		protected bool __editorExpandLeft => _editorExpandLeft;
		
		[SerializeField] private bool _editorExpandRight = default;
		protected bool __editorExpandRight => _editorExpandRight;
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\
		
		private void Awake()
		{
			// TODO: Test, it should get the current instance or create a new one
			if (_instance != null && _instance != this)
			{
				Destroy(gameObject);
				return;
			}

			_instance = this;
			
			DontDestroyOnLoad(this);
		}
		
		private void Update()
		{
			OVRInput.Update();
			UpdateHandStates(OVRPlugin.Step.Render);
		}

		// Inconsistant behaviour updating hand on fixed update
//		private void FixedUpdate()
//		{
//			UpdateHandStates(OVRPlugin.Step.Physics);
//		}

		private void UpdateHandStates(OVRPlugin.Step step)
		{
			_handDataLeft.UpdateHandData(step);
			_handDataRight.UpdateHandData(step);
		}
		
		private void OnApplicationQuit()
		{
			_isQuitting = true;
		}
		
		
		//***** functions *****\\

		public static bool CreateOvrInputManager()
		{
			return Instance != null;
		}
		
		public BasicOvrHandData GetHandData(OVRPlugin.Hand hand)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return default;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft;

				case OVRPlugin.Hand.HandRight:
					return _handDataRight;

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}
		
		public OVRPlugin.HandState GetHandState(OVRPlugin.Hand hand)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return default;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.HandState;

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.HandState;

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}
		
		public OVRPlugin.TrackingConfidence GetHandConfidence(OVRPlugin.Hand hand)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return OVRPlugin.TrackingConfidence.Low;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.GetHandConfidence();

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.GetHandConfidence();

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}
		
		public bool GetHandStatus(OVRPlugin.Hand hand, OVRPlugin.HandStatus handStatus)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return false;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.GetHandStatus(handStatus);

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.GetHandStatus(handStatus);

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}
		
		public bool GetFingerIsPinching(OVRPlugin.Hand hand, OVRPlugin.HandFingerPinch fingerPinch)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return false;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.GetFingerIsPinching(fingerPinch);

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.GetFingerIsPinching(fingerPinch);

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}

		public float GetFingerPinchStrength(OVRPlugin.Hand hand, OVRPlugin.HandFingerPinch fingerPinch)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return 0f;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.GetFingerPinchStrength(fingerPinch);

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.GetFingerPinchStrength(fingerPinch);

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}

		public OVRPlugin.TrackingConfidence GetFingerConfidence(OVRPlugin.Hand hand, OVRPlugin.HandFinger finger)
		{
			switch (hand)
			{
				case OVRPlugin.Hand.None:
					return OVRPlugin.TrackingConfidence.Low;
				
				case OVRPlugin.Hand.HandLeft:
					return _handDataLeft.GetFingerConfidence(finger);

				case OVRPlugin.Hand.HandRight:
					return _handDataRight.GetFingerConfidence(finger);

				default:
					throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
			}
		}
		
	}
}
