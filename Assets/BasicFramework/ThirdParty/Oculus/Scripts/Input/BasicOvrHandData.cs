﻿using System;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class BasicOvrHandData 
	{
	
		// **************************************** VARIABLES ****************************************\\

		public OVRPlugin.Hand Handedness { get; }

		// Data can be valid and not tracked at the same time, if tracking fails pinch state stays the same as last value
		public bool IsDataValid { get; private set; }

		private OVRPlugin.HandState _handState;
		public OVRPlugin.HandState HandState => _handState;
		

		// ****************************************  METHODS  ****************************************\\

		//***** constructors *****\\
		
		public BasicOvrHandData(OVRPlugin.Hand handedness)
		{
			Handedness = handedness;
			IsDataValid = false;
		}
		
		
		//***** functions *****\\
		
		public void UpdateHandData(OVRPlugin.Step updateStep)
		{
			if (Handedness == OVRPlugin.Hand.None) return;
			IsDataValid = OVRPlugin.GetHandState(updateStep, Handedness, ref _handState);
		}

		public bool GetHandStatus(OVRPlugin.HandStatus handStatus)
		{
			if (!IsDataValid) return false;
			return (_handState.Status & handStatus) == handStatus;
		}
		

		//***** pinching *****\\
		
		// You can check if multiple fingers are pinching at once
		// Note: even when not tracking, if last tracked pose was pinching finger pinching remains true
		public bool GetFingerIsPinching(OVRPlugin.HandFingerPinch fingerPinch)
		{
			if (!IsDataValid) return false;
			return (_handState.Pinches & fingerPinch) == fingerPinch;
		}
		
		public float GetFingerPinchStrength(OVRPlugin.HandFingerPinch fingerPinch)
		{
			if (!IsDataValid) return 0;
			if (_handState.PinchStrength == null) return 0;
			if (_handState.PinchStrength.Length != (int) OVRPlugin.HandFinger.Max) return 0;
			
			if (!GetFingerIsPinching(fingerPinch)) return 0;

			var pinchStrength = 1f;

			if ((fingerPinch & OVRPlugin.HandFingerPinch.Thumb) == OVRPlugin.HandFingerPinch.Thumb)
			{
				pinchStrength *= GetFingerPinchStrengthSingle(OVRPlugin.HandFinger.Thumb);
			}
			
			if ((fingerPinch & OVRPlugin.HandFingerPinch.Index) == OVRPlugin.HandFingerPinch.Index)
			{
				pinchStrength *= GetFingerPinchStrengthSingle(OVRPlugin.HandFinger.Index);
			}
			
			if ((fingerPinch & OVRPlugin.HandFingerPinch.Middle) == OVRPlugin.HandFingerPinch.Middle)
			{
				pinchStrength *= GetFingerPinchStrengthSingle(OVRPlugin.HandFinger.Middle);
			}
			
			if ((fingerPinch & OVRPlugin.HandFingerPinch.Ring) == OVRPlugin.HandFingerPinch.Ring)
			{
				pinchStrength *= GetFingerPinchStrengthSingle(OVRPlugin.HandFinger.Ring);
			}
			
			if ((fingerPinch & OVRPlugin.HandFingerPinch.Pinky) == OVRPlugin.HandFingerPinch.Pinky)
			{
				pinchStrength *= GetFingerPinchStrengthSingle(OVRPlugin.HandFinger.Pinky);
			}
			
			return pinchStrength;
		}

		private float GetFingerPinchStrengthSingle(OVRPlugin.HandFinger finger)
		{
			return _handState.PinchStrength[(int)finger];
		}

		
		//***** confidence *****\\

		public OVRPlugin.TrackingConfidence GetHandConfidence()
		{
			return !IsDataValid ? OVRPlugin.TrackingConfidence.Low : _handState.HandConfidence;
		}
		
		public OVRPlugin.TrackingConfidence GetFingerConfidence(OVRPlugin.HandFinger finger)
		{
			if (!IsDataValid) return OVRPlugin.TrackingConfidence.Low;
			if (_handState.FingerConfidences == null) return OVRPlugin.TrackingConfidence.Low;
			if (_handState.FingerConfidences.Length != (int) OVRPlugin.HandFinger.Max)
				return OVRPlugin.TrackingConfidence.Low;
			
			return _handState.FingerConfidences[(int)finger];
		}

		

	}
}