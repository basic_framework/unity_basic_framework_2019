﻿namespace BasicFramework.ThirdParty.Oculus
{
	public enum OvrInputBoolTypes
	{
		RawButton,
		Button,
		RawTouch,
		Touch,
		RawNearTouch,
		NearTouch
	}
	
	public enum OvrInputFloatTypes
	{
		RawAxis1D,
		Axis1D,
		RawAxis2D,
		Axis2D
	}
	
	public enum OvrInputVector2Types
	{
		RawAxis2D,
		Axis2D
	}

	public enum OvrHandInputOptions
	{
		Either,
		Left,
		Right
	}
	
}