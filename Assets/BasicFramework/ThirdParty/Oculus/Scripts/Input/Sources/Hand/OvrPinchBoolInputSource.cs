﻿using System;
using BasicFramework.Core.ScriptableInputs;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrPinchBoolInputSource : BoolInputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[SerializeField] private OvrHandInputOptions _hand = default;

		[EnumFlag]
		[SerializeField] private OVRPlugin.HandFingerPinch _fingerPinch = 0;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		protected override bool GetUpdateValue()
		{
			if (_fingerPinch == 0) return false;

			switch (_hand)
			{
				case OvrHandInputOptions.Either:
					return OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandLeft, _fingerPinch) ||
					       OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandRight, _fingerPinch);

				case OvrHandInputOptions.Left:
					return OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandLeft, _fingerPinch);
					
				case OvrHandInputOptions.Right:
					return OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandRight, _fingerPinch);
					
				default:
					throw new ArgumentOutOfRangeException();
			}
			
		}
		
	}
}