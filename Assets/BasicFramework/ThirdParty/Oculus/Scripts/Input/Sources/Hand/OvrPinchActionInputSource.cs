﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableInputs;
using BasicFramework.Core.Utility.Attributes;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrPinchActionInputSource : ActionInputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[SerializeField] private OvrHandInputOptions _hand = default;

		[EnumFlag]
		[SerializeField] private OVRPlugin.HandFingerPinch _fingerPinch = 0;

		[SerializeField] private ButtonInputStates _inputState = ButtonInputStates.Down;

		private bool _previousValue;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override bool GetUpdateValue()
		{
			var currentValue = false;

			if (_fingerPinch != 0)
			{
				switch (_hand)
				{
					case OvrHandInputOptions.Either:
						currentValue =
							OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandLeft, _fingerPinch) ||
							OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandRight, _fingerPinch);
						break;

					case OvrHandInputOptions.Left:
						currentValue =
							OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandLeft, _fingerPinch);
						break;

					case OvrHandInputOptions.Right:
						currentValue =
							OvrInputManager.Instance.GetFingerIsPinching(OVRPlugin.Hand.HandRight, _fingerPinch);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			bool returnValue;
			
			switch (_inputState)
			{
				case ButtonInputStates.Down:
					returnValue = !_previousValue && currentValue;
					break;
				
				case ButtonInputStates.Held:
					returnValue = currentValue;
					break;
				
				case ButtonInputStates.Up:
					returnValue = _previousValue && !currentValue;
					break;
				
				default:
					throw new ArgumentOutOfRangeException();
			}

			_previousValue = currentValue;
			return returnValue;
		}
		
	}
}