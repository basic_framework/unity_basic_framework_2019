using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomPropertyDrawer(typeof(OvrTouchBoolInputSource))]
	public class OvrTouchBoolInputSourcePropertyDrawer : BasicBasePropertyDrawer
	{
	
		// **************************************** VARIABLES ****************************************\\

		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;

			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded) return;
			
			var enabled = property.FindPropertyRelative("_enabled");
			
			EditorGUI.PropertyField(TempRect, enabled);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			var ovrInputBoolType = property.FindPropertyRelative("_ovrInputBoolType");

			EditorGUI.PropertyField(TempRect, ovrInputBoolType);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			var ovrInputActionTypeEnum =
				EnumUtility.EnumNameToEnumValue<OvrInputBoolTypes>(ovrInputBoolType.enumNames[ovrInputBoolType.enumValueIndex]);

			string inputVariableName;
			
			switch (ovrInputActionTypeEnum)
			{
				case OvrInputBoolTypes.RawButton:		inputVariableName = "_rawButton";		break;
				case OvrInputBoolTypes.Button:			inputVariableName = "_button";			break;
				case OvrInputBoolTypes.RawTouch:		inputVariableName = "_rawTouch";		break;
				case OvrInputBoolTypes.Touch:			inputVariableName = "_touch";			break;
				case OvrInputBoolTypes.RawNearTouch:	inputVariableName = "_rawNearTouch";	break;
				case OvrInputBoolTypes.NearTouch:		inputVariableName = "_nearTouch";		break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			EditorGUI.PropertyField(TempRect, property.FindPropertyRelative(inputVariableName));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;

			if (!property.isExpanded) { return height; }
			
			height += SingleLineHeight + BUFFER_HEIGHT;
			height += SingleLineHeight + BUFFER_HEIGHT;
			height += SingleLineHeight + BUFFER_HEIGHT;
			
			return height;
		}
	}
}