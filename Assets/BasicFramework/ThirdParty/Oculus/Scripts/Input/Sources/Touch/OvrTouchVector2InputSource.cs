﻿using System;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrTouchVector2InputSource : Vector2InputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		

		[SerializeField] private OvrInputVector2Types _ovrInputVector2Type = OvrInputVector2Types.RawAxis2D;

		[SerializeField] private OVRInput.RawAxis2D _rawAxis2D = OVRInput.RawAxis2D.None;
		[SerializeField] private OVRInput.Axis2D _axis2D = OVRInput.Axis2D.None;
		
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override Vector2 GetUpdateValue()
		{
			switch (_ovrInputVector2Type)
			{
				case OvrInputVector2Types.RawAxis2D:
					return OVRInput.Get(_rawAxis2D);
				case OvrInputVector2Types.Axis2D:
					return OVRInput.Get(_axis2D);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}