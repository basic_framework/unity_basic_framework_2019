﻿using System;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrTouchActionInputSource : ActionInputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private OvrInputBoolTypes _ovrInputBoolType = OvrInputBoolTypes.RawButton;

		[SerializeField] private OVRInput.RawButton _rawButton = OVRInput.RawButton.None;
		[SerializeField] private OVRInput.Button _button = OVRInput.Button.None;
		
		[SerializeField] private OVRInput.RawTouch _rawTouch = OVRInput.RawTouch.None;
		[SerializeField] private OVRInput.Touch _touch = OVRInput.Touch.None;
		
		[SerializeField] private OVRInput.RawNearTouch _rawNearTouch = OVRInput.RawNearTouch.None;
		[SerializeField] private OVRInput.NearTouch _nearTouch = OVRInput.NearTouch.None;

		[SerializeField] private ButtonInputStates _inputState = ButtonInputStates.Down;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override bool GetUpdateValue()
		{
			switch (_ovrInputBoolType)
			{
				case OvrInputBoolTypes.RawButton:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_rawButton);
						case ButtonInputStates.Held:
							return OVRInput.Get(_rawButton);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_rawButton);
						default:
							throw new ArgumentOutOfRangeException();
					}

				case OvrInputBoolTypes.Button:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_button);
						case ButtonInputStates.Held:
							return OVRInput.Get(_button);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_button);
						default:
							throw new ArgumentOutOfRangeException();
					}
					
				case OvrInputBoolTypes.RawTouch:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_rawTouch);
						case ButtonInputStates.Held:
							return OVRInput.Get(_rawTouch);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_rawTouch);
						default:
							throw new ArgumentOutOfRangeException();
					}
					
				case OvrInputBoolTypes.Touch:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_touch);
						case ButtonInputStates.Held:
							return OVRInput.Get(_touch);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_touch);
						default:
							throw new ArgumentOutOfRangeException();
					}
					
				case OvrInputBoolTypes.RawNearTouch:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_rawNearTouch);
						case ButtonInputStates.Held:
							return OVRInput.Get(_rawNearTouch);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_rawNearTouch);
						default:
							throw new ArgumentOutOfRangeException();
					}
					
				case OvrInputBoolTypes.NearTouch:
					switch (_inputState)
					{
						case ButtonInputStates.Down:
							return OVRInput.GetDown(_nearTouch);
						case ButtonInputStates.Held:
							return OVRInput.Get(_nearTouch);
						case ButtonInputStates.Up:
							return OVRInput.GetUp(_nearTouch);
						default:
							throw new ArgumentOutOfRangeException();
					}
					
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}