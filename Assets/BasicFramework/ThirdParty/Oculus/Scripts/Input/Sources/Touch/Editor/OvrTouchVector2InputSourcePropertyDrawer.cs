﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomPropertyDrawer(typeof(OvrTouchVector2InputSource))]
	public class OvrTouchVector2InputSourcePropertyDrawer : BasicBasePropertyDrawer
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\
		
		protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TempRect = position;
			TempRect.height = SingleLineHeight;
			
			property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;

			if (!property.isExpanded) return;
			
			var ovrInputVector2Type = property.FindPropertyRelative("_ovrInputVector2Type");

			EditorGUI.PropertyField(TempRect, ovrInputVector2Type);
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
			var ovrInputVector2TypesEnum =
				EnumUtility.EnumNameToEnumValue<OvrInputVector2Types>(ovrInputVector2Type.enumNames[ovrInputVector2Type.enumValueIndex]);

			string inputVariableName;

			switch (ovrInputVector2TypesEnum)
			{
				case OvrInputVector2Types.RawAxis2D:	inputVariableName = "_rawAxis2D";	break;
				case OvrInputVector2Types.Axis2D:		inputVariableName = "_axis2D";		break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			EditorGUI.PropertyField(TempRect, property.FindPropertyRelative(inputVariableName));
			TempRect.y += TempRect.height + BUFFER_HEIGHT;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var height = SingleLineHeight;
			
			if (!property.isExpanded) { return height; }
            
			height += SingleLineHeight + BUFFER_HEIGHT;
			height += SingleLineHeight + BUFFER_HEIGHT;
			
			return height;
		}
		
	}
}