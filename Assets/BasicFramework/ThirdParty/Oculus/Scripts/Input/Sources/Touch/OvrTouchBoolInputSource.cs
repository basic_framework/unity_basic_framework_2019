﻿using System;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	[Serializable]
	public class OvrTouchBoolInputSource : BoolInputSource 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private OvrInputBoolTypes _ovrInputBoolType = OvrInputBoolTypes.RawButton;

		[SerializeField] private OVRInput.RawButton _rawButton = OVRInput.RawButton.None;
		[SerializeField] private OVRInput.Button _button = OVRInput.Button.None;
		
		[SerializeField] private OVRInput.RawTouch _rawTouch = OVRInput.RawTouch.None;
		[SerializeField] private OVRInput.Touch _touch = OVRInput.Touch.None;
		
		[SerializeField] private OVRInput.RawNearTouch _rawNearTouch = OVRInput.RawNearTouch.None;
		[SerializeField] private OVRInput.NearTouch _nearTouch = OVRInput.NearTouch.None;
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override bool GetUpdateValue()
		{
			switch (_ovrInputBoolType)
			{
				case OvrInputBoolTypes.RawButton:		return OVRInput.Get(_rawButton);
				case OvrInputBoolTypes.Button:		return OVRInput.Get(_button);

				case OvrInputBoolTypes.RawTouch:		return OVRInput.Get(_rawTouch);
				case OvrInputBoolTypes.Touch:			return OVRInput.Get(_touch);

				case OvrInputBoolTypes.RawNearTouch:	return OVRInput.Get(_rawNearTouch);
				case OvrInputBoolTypes.NearTouch:		return OVRInput.Get(_nearTouch);

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
	}
}