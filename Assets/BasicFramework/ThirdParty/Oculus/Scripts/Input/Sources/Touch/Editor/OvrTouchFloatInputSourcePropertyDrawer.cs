﻿using System;
using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
    [CustomPropertyDrawer(typeof(OvrTouchFloatInputSource))]
    public class OvrTouchFloatInputSourcePropertyDrawer : BasicBasePropertyDrawer
    {
	
        // **************************************** VARIABLES ****************************************\\
	
	
        // ****************************************  METHODS  ****************************************\\
        
        protected override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            TempRect = position;
            TempRect.height = SingleLineHeight;
            
            property.isExpanded = EditorGUI.Foldout(TempRect, property.isExpanded, label);
            TempRect.y += TempRect.height + BUFFER_HEIGHT;

            if (!property.isExpanded) return;
			
            var ovrInputFloatType = property.FindPropertyRelative("_ovrInputFloatType");

            EditorGUI.PropertyField(TempRect, ovrInputFloatType);
            TempRect.y += TempRect.height + BUFFER_HEIGHT;

            var ovrInputFloatTypeEnum =
                EnumUtility.EnumNameToEnumValue<OvrInputFloatTypes>(ovrInputFloatType.enumNames[ovrInputFloatType.enumValueIndex]);

            string inputVariableName;

            switch (ovrInputFloatTypeEnum)
            {
                case OvrInputFloatTypes.RawAxis1D:    inputVariableName = "_rawAxis1D";    break;
                case OvrInputFloatTypes.Axis1D:       inputVariableName = "_axis1D";       break;
                case OvrInputFloatTypes.RawAxis2D:    inputVariableName = "_rawAxis2D";    break;
                case OvrInputFloatTypes.Axis2D:       inputVariableName = "_axis2D";       break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            EditorGUI.PropertyField(TempRect, property.FindPropertyRelative(inputVariableName));
            TempRect.y += TempRect.height + BUFFER_HEIGHT;
			
            var inputAxisDirection = property.FindPropertyRelative("_inputAxisDirection");

            GUI.enabled = PrevGuiEnabled &&
                          (ovrInputFloatTypeEnum == OvrInputFloatTypes.RawAxis2D ||
                           ovrInputFloatTypeEnum == OvrInputFloatTypes.Axis2D);
            
            EditorGUI.PropertyField(TempRect, inputAxisDirection);
            TempRect.y += TempRect.height + BUFFER_HEIGHT;

            GUI.enabled = PrevGuiEnabled;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var height = SingleLineHeight;

            if (!property.isExpanded) { return height; }
            
            height += SingleLineHeight + BUFFER_HEIGHT;
            height += SingleLineHeight + BUFFER_HEIGHT;
            height += SingleLineHeight + BUFFER_HEIGHT;
			
            return height;
        }
        
    }
}