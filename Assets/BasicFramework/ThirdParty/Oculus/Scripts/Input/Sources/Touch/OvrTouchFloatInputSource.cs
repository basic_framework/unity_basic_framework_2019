﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.ScriptableInputs;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
    [Serializable]
    public class OvrTouchFloatInputSource : FloatInputSource 
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        

        [SerializeField] private OvrInputFloatTypes _ovrInputFloatType = OvrInputFloatTypes.RawAxis1D;

        [SerializeField] private OVRInput.RawAxis1D _rawAxis1D = OVRInput.RawAxis1D.None;
        [SerializeField] private OVRInput.Axis1D _axis1D = OVRInput.Axis1D.None;
		
        [SerializeField] private OVRInput.RawAxis2D _rawAxis2D = OVRInput.RawAxis2D.None;
        [SerializeField] private OVRInput.Axis2D _axis2D = OVRInput.Axis2D.None;

        [Tooltip("The positive 2D direction to use for the input. " +
                 "E.g. (1,0) will return the amount of input along the right axis, (0,1) will return the amount along the up axis")]
        [SerializeField] private Vector2 _inputAxisDirection = Vector2.right;
        
        //[Tooltip("False => Uses the X-Axis value, True => Uses the Y-Axis Value")]
       // [SerializeField] private bool _useYAxisValue = default;


        // ****************************************  METHODS  ****************************************\\

        protected override float GetUpdateValue()
        {
            switch (_ovrInputFloatType)
            {
                case OvrInputFloatTypes.RawAxis1D:
                    return OVRInput.Get(_rawAxis1D);
                case OvrInputFloatTypes.Axis1D:
                    return OVRInput.Get(_axis1D);
                case OvrInputFloatTypes.RawAxis2D:
                    var rawAxis2 = OVRInput.Get(_rawAxis2D);
                    return MathUtility.MagOfVectorInAxis(rawAxis2, _inputAxisDirection);
                    //return _useYAxisValue ? rawAxis2.y : rawAxis2.x;
                case OvrInputFloatTypes.Axis2D:
                    var axis2 = OVRInput.Get(_axis2D);
                    return MathUtility.MagOfVectorInAxis(axis2, _inputAxisDirection);
                    //return _useYAxisValue ? axis2.y : axis2.x;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
		
    }
}