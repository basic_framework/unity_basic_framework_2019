﻿using System;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomEditor(typeof(OvrInputManager))]
	public class OvrInputManagerEditor : BasicBaseEditor<OvrInputManager>
	{

		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		protected override bool UpdateConstantlyInPlayMode => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			var editorExpandLeft = serializedObject.FindProperty("_editorExpandLeft");
			var editorExpandRight = serializedObject.FindProperty("_editorExpandRight");
			
			Space();
			editorExpandLeft.boolValue =
				EditorGUILayout.Foldout(editorExpandLeft.boolValue, "Hand Left",  BasicEditorStyles.BoldFoldout);

			if (editorExpandLeft.boolValue)
			{
				DrawHandState(OVRPlugin.Hand.HandLeft);
			}
			
			Space();
			editorExpandRight.boolValue =
				EditorGUILayout.Foldout(editorExpandRight.boolValue, "Hand Right",  BasicEditorStyles.BoldFoldout);

			if (editorExpandRight.boolValue)
			{
				DrawHandState(OVRPlugin.Hand.HandRight);
			}
			
		}
		
		private void DrawHandState(OVRPlugin.Hand handedness)
		{
			if (handedness == OVRPlugin.Hand.None) return;

			var handData = TypedTarget.GetHandData(handedness);
			
			
			//SectionHeader($"Hand {(handedness == OVRPlugin.Hand.HandLeft ? "Left" : "Right")}");
			EditorGUILayout.LabelField("Is Data Valid", handData.IsDataValid.ToString());

			GUI.enabled = PrevGuiEnabled && handData.IsDataValid;
			
			EditorGUILayout.LabelField("Hand Confidence", 
				TypedTarget.GetHandConfidence(handedness).ToString().CamelCaseToHumanReadable());
			
			DrawHandStatus(handData);
			
			Space();
			Space();
			DrawFinger(handData, OVRPlugin.HandFinger.Max, true);
			DrawFinger(handData, OVRPlugin.HandFinger.Thumb);
			DrawFinger(handData, OVRPlugin.HandFinger.Index);
			DrawFinger(handData, OVRPlugin.HandFinger.Middle);
			DrawFinger(handData, OVRPlugin.HandFinger.Ring);
			DrawFinger(handData, OVRPlugin.HandFinger.Pinky);

			GUI.enabled = PrevGuiEnabled;
		}
		
		private void DrawHandStatus(BasicOvrHandData handData)
		{
			SectionHeader("Status");
			
			var fieldName = nameof(OVRPlugin.HandStatus.HandTracked).CamelCaseToHumanReadable();
			var isTrue = handData.GetHandStatus(OVRPlugin.HandStatus.HandTracked);
			EditorGUILayout.LabelField(fieldName, isTrue.ToString());
			
			fieldName = nameof(OVRPlugin.HandStatus.InputStateValid).CamelCaseToHumanReadable();
			isTrue = handData.GetHandStatus(OVRPlugin.HandStatus.InputStateValid);
			EditorGUILayout.LabelField(fieldName, isTrue.ToString());
			
			fieldName = nameof(OVRPlugin.HandStatus.SystemGestureInProgress).CamelCaseToHumanReadable();
			isTrue = handData.GetHandStatus(OVRPlugin.HandStatus.SystemGestureInProgress);
			EditorGUILayout.LabelField(fieldName, isTrue.ToString());
			
			fieldName = nameof(OVRPlugin.HandStatus.DominantHand).CamelCaseToHumanReadable();
			isTrue = handData.GetHandStatus(OVRPlugin.HandStatus.DominantHand);
			EditorGUILayout.LabelField(fieldName, isTrue.ToString());
			
			fieldName = nameof(OVRPlugin.HandStatus.MenuPressed).CamelCaseToHumanReadable();
			isTrue = handData.GetHandStatus(OVRPlugin.HandStatus.MenuPressed);
			EditorGUILayout.LabelField(fieldName, isTrue.ToString());
		}

		private void DrawFinger(BasicOvrHandData handData, OVRPlugin.HandFinger finger, bool isHeading = false)
		{
			OVRPlugin.HandFingerPinch fingerPinch = 0;
			
			switch (finger)
			{
				case OVRPlugin.HandFinger.Thumb:
					fingerPinch = OVRPlugin.HandFingerPinch.Thumb;
					break;
				case OVRPlugin.HandFinger.Index:
					fingerPinch = OVRPlugin.HandFingerPinch.Index;
					break;
				case OVRPlugin.HandFinger.Middle:
					fingerPinch = OVRPlugin.HandFingerPinch.Middle;
					break;
				case OVRPlugin.HandFinger.Ring:
					fingerPinch = OVRPlugin.HandFingerPinch.Ring;
					break;
				case OVRPlugin.HandFinger.Pinky:
					fingerPinch = OVRPlugin.HandFingerPinch.Pinky;
					break;
				case OVRPlugin.HandFinger.Max:
					fingerPinch = (OVRPlugin.HandFingerPinch) (~0);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(finger), finger, null);
			}

			//var isTracking = (hand.Status & OVRPlugin.HandStatus.HandTracked) == OVRPlugin.HandStatus.HandTracked; 

			var fingerName = isHeading ? "Finger" : finger.ToString();
			var confidence = isHeading ? "| Confidence" : $"| {handData.GetFingerConfidence(finger)}";
			
			
			var pinching = isHeading ? "| Pinching" : $"| {handData.GetFingerIsPinching(fingerPinch)}";
			var pinchStrength = isHeading ? "| Strength" : $"| {handData.GetFingerPinchStrength(fingerPinch)}";
			
			EditorGUILayout.BeginHorizontal();

			var style = isHeading ? EditorStyles.boldLabel : EditorStyles.label;

			EditorGUILayout.PrefixLabel(fingerName, EditorStyles.label, style);
			EditorGUILayout.LabelField(confidence, style, GUILayout.Width(90));
			EditorGUILayout.LabelField(pinching, style, GUILayout.Width(80));
			EditorGUILayout.LabelField(pinchStrength, style, GUILayout.Width(80));
			
			EditorGUILayout.EndHorizontal();
		}
		
		
	}
}