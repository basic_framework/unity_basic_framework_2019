﻿using BasicFramework.Core.Utility;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;

namespace BasicFramework.ThirdParty.Oculus.Editor
{
	[CustomEditor(typeof(BasicOvrHandBehaviour))]
	public class BasicOvrHandBehaviourEditor : BasicBaseEditor<BasicOvrHandBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;

		private ReorderableList _reorderableListTrackingConfidence;
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();

			var trackingConfidenceChangedCompare = serializedObject.FindProperty("_trackingConfidenceChangedCompare");

			_reorderableListTrackingConfidence =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, trackingConfidenceChangedCompare,
					true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.TrackingConfidence).CamelCaseToHumanReadable(),
				TypedTarget.TrackingConfidence.ToString().CamelCaseToHumanReadable());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var hand = serializedObject.FindProperty("_hand");
			var handEnum = EnumUtility.EnumNameToEnumValue<OVRPlugin.Hand>(hand.enumNames[hand.enumValueIndex]);

			
			
			Space();
			SectionHeader($"{nameof(BasicOvrHandBehaviour).CamelCaseToHumanReadable()} Properties");

			if (handEnum == OVRPlugin.Hand.None)
			{
				EditorGUILayout.HelpBox("Please pick a valid hand", MessageType.Warning);
			}
			
			EditorGUILayout.PropertyField(hand);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var trackingConfidenceChanged = serializedObject.FindProperty("_trackingConfidenceChanged");
			
			Space();
			SectionHeader($"{nameof(BasicOvrHandBehaviour).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(trackingConfidenceChanged);

			_reorderableListTrackingConfidence.DoLayoutList();
		}
		
	}
}