﻿using System;
using UnityEngine;

namespace BasicFramework.ThirdParty.Oculus
{
	public class BasicOvrHandBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private OVRPlugin.Hand _hand = default;


		[SerializeField] private OvrTrackingConfidenceUnityEvent _trackingConfidenceChanged = default;

		[SerializeField] private OvrTrackingConfidenceCompareUnityEvent[] _trackingConfidenceChangedCompare = default;
		

		public OVRPlugin.Hand Hand => _hand;

		private OVRPlugin.TrackingConfidence _trackingConfidence = OVRPlugin.TrackingConfidence.Low;
		public OVRPlugin.TrackingConfidence TrackingConfidence
		{
			get => _trackingConfidence;
			private set
			{
				if(_trackingConfidence == value) return;
				_trackingConfidence = value;
				TrackingConfidenceUpdated(value);
			}
		}

		


		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		private void Start()
		{
			TrackingConfidenceUpdated(_trackingConfidence);
		}

		private void Update()
		{
			if (_hand == OVRPlugin.Hand.None) return;
			
			var handData = GetHandData();
			
			TrackingConfidence = handData.GetHandConfidence();
		}

		
		//***** property updates *****\\
		
		private void TrackingConfidenceUpdated(OVRPlugin.TrackingConfidence value)
		{
			_trackingConfidenceChanged.Invoke(value);

			foreach (var compareUnityEvent in _trackingConfidenceChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\

		private BasicOvrHandData GetHandData()
		{
			switch (_hand)
			{
				case OVRPlugin.Hand.None:
					return null;
				
				case OVRPlugin.Hand.HandLeft:
					return OvrInputManager.Instance.HandDataLeft;

				case OVRPlugin.Hand.HandRight:
					return OvrInputManager.Instance.HandDataRight;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

	}
}