#Oculus (Basic Framework Integration)

__Note: Only add this package if you have _Oculus Integration_ installed. 
If you do not have it installed you will get errors.
Delete the root folder to remove the errors.__

__Note:  You only have to import the _Oculus/VR_ directory__

This plugin integrates Basic Frameworks Core systems with the Oculus Integration asset.

