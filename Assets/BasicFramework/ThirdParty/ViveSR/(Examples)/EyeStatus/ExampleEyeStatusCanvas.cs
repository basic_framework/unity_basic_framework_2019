﻿using System;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEngine;
using UnityEngine.UI;

namespace BasicFramework.ThirdParty.ViveSr.Examples
{
	public class ExampleEyeStatusCanvas : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[SerializeField] private BasicViveSrAnipalManager _srAnipalManager = default;

		[SerializeField] private Text _textEyeTrackingStatus = default;
		[SerializeField] private Text _textEyeDataCombined = default;
		[SerializeField] private Text _textEyeDataLeft = default;
		[SerializeField] private Text _textEyeDataRight = default;


		// ****************************************  METHODS  ****************************************\\

		private void Update()
		{
			var eyeTrackingData = BasicViveSrAnipalInputManager.EyeTrackingData;
			
			var eyeDataCombined = eyeTrackingData.EyeDataCombined;
			var eyeDataLeft = eyeTrackingData.EyeDataSingleLeft;
			var eyeDataRight = eyeTrackingData.EyeDataSingleRight;


			var messageEyeTracking = $"Eye Tracking Status\n\n" + 
			                         $"{nameof(eyeTrackingData.UserPresent)}: {eyeTrackingData.UserPresent}\n" +
			                         $"{nameof(eyeTrackingData.FrameSequence)}: {eyeTrackingData.FrameSequence}\n" +
			                         $"{nameof(eyeTrackingData.TimeStamp)}: {eyeTrackingData.TimeStamp}";

			_textEyeTrackingStatus.text = messageEyeTracking;
			

			var messageCombinedEye = $"Combined Eye Status\n\n" +
			                         $"{nameof(eyeDataCombined.IsValidConvergenceDistance)}: {eyeDataCombined.IsValidConvergenceDistance}\n" +
			                         $"{nameof(eyeDataCombined.ConvergenceDistanceMillimeters)}: {eyeDataCombined.ConvergenceDistanceMillimeters}\n" +
			                         $"\n" +
			                         $"{EyeDataBaseMessage(eyeDataCombined)}";

			_textEyeDataCombined.text = messageCombinedEye;
			
			
			var messageLeftEye = $"Left Eye Status\n\n" +
			                     $"{nameof(eyeDataLeft.EyeFrown)}: {eyeDataLeft.EyeFrown}\n" +
			                     $"{nameof(eyeDataLeft.EyeSqueeze)}: {eyeDataLeft.EyeSqueeze}\n" +
			                     $"{nameof(eyeDataLeft.EyeWide)}: {eyeDataLeft.EyeWide}\n" +
			                     $"\n" +
			                     $"{EyeDataBaseMessage(eyeDataLeft)}";

			_textEyeDataLeft.text = messageLeftEye;
			
			
			var messageRightEye = $"Right Eye Status\n\n" +
			                     $"{nameof(eyeDataRight.EyeFrown)}: {eyeDataRight.EyeFrown}\n" +
			                     $"{nameof(eyeDataRight.EyeSqueeze)}: {eyeDataRight.EyeSqueeze}\n" +
			                     $"{nameof(eyeDataRight.EyeWide)}: {eyeDataRight.EyeWide}\n" +
			                     $"\n" +
			                     $"{EyeDataBaseMessage(eyeDataRight)}";

			_textEyeDataRight.text = messageRightEye;

//			var message = $"Eye Tracking Status\n" +
//			              $"{nameof(eyeTrackingData.UserPresent)}: {eyeTrackingData.UserPresent}\n" +
//			              $"{nameof(eyeTrackingData.FrameSequence)}: {eyeTrackingData.FrameSequence}\n" +
//			              $"{nameof(eyeTrackingData.TimeStamp)}: {eyeTrackingData.TimeStamp}\n" +
//			              $"\n" +
//			              $"Combined\n" +
//			              $"{nameof(eyeDataCombined.IsValidConvergenceDistance)}: {eyeDataCombined.IsValidConvergenceDistance}\n" +
//			              $"{nameof(eyeDataCombined.ConvergenceDistanceMillimeters)}: {eyeDataCombined.ConvergenceDistanceMillimeters}\n" +
//			              $"{EyeDataBaseMessage(eyeDataCombined)}\n" +
//			              $"\n" +
//			              $"Left Eye\n" +
//			              $"{nameof(eyeDataLeft.EyeFrown)}: {eyeDataLeft.EyeFrown}\n" +
//			              $"{nameof(eyeDataLeft.EyeSqueeze)}: {eyeDataLeft.EyeSqueeze}\n" +
//			              $"{nameof(eyeDataLeft.EyeWide)}: {eyeDataLeft.EyeWide}\n" +
//			              $"{EyeDataBaseMessage(eyeDataLeft)}\n" +
//			              $"\n" +
//			              $"Right Eye\n" +
//			              $"{nameof(eyeDataRight.EyeFrown)}: {eyeDataRight.EyeFrown}\n" +
//			              $"{nameof(eyeDataRight.EyeSqueeze)}: {eyeDataRight.EyeSqueeze}\n" +
//			              $"{nameof(eyeDataRight.EyeWide)}: {eyeDataRight.EyeWide}\n" +
//			              $"{EyeDataBaseMessage(eyeDataRight)}\n" +
//			              $"";
//
//			_textEyeTrackingStatus.text = message;
		}

		private string EyeDataBaseMessage(BasicViveSrEyeDataBase eyeData)
		{
			return
				$"{nameof(eyeData.GazeOriginLocalMillimeters)} ({(eyeData.IsValidGazeOrigin ? "Valid" : "Invalid")}):\n" +
				$"{eyeData.GazeOriginLocalMillimeters}\n" +
				$"\n" +
				$"{nameof(eyeData.GazeDirectionLocal)} ({(eyeData.IsValidGazeDirection ? "Valid" : "Invalid")}):\n" +
				$"{eyeData.GazeDirectionLocal}\n" +
				$"\n" +
				$"{nameof(eyeData.EyeOpenness)} ({(eyeData.IsValidEyeOpenness ? "Valid" : "Invalid")}):\n" +
				$"{eyeData.EyeOpenness}\n" +
				$"\n" +
				$"{nameof(eyeData.PupilDiameterMillimeter)} ({(eyeData.IsValidPupilDiameter ? "Valid" : "Invalid")}):\n" +
				$"{eyeData.PupilDiameterMillimeter}\n" +
				$"\n" +
				$"{nameof(eyeData.PupilPositionInSensorArea)} ({(eyeData.IsValidPupilPosition ? "Valid" : "Invalid")}):\n" +
				$"{eyeData.PupilPositionInSensorArea}\n" +
				$"\n";
		}
	}
}