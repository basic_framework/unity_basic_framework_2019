﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr
{
	[CustomEditor(typeof(BasicViveSrAnipalGazeTracker))]
	public class BasicViveSrAnipalGazeTrackerEditor : BasicBaseEditor<BasicViveSrAnipalGazeTracker>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool HasDebugSection => true;
		

		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			EditorGUILayout.LabelField(nameof(TypedTarget.GazeValid).CamelCaseToHumanReadable(), 
				TypedTarget.GazeValid.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var transformHmd = serializedObject.FindProperty("_transformHmd");
			var transformGaze = serializedObject.FindProperty("_transformGaze");
			var eyeMode = serializedObject.FindProperty("_eyeMode");

			Space();
			SectionHeader($"{nameof(BasicViveSrAnipalGazeTracker).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformHmd);
			BasicEditorGuiLayout.PropertyFieldNotNull(transformGaze);
			EditorGUILayout.PropertyField(eyeMode);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var gazeValidChanged = serializedObject.FindProperty("_gazeValidChanged");
			
			Space();
			SectionHeader($"{nameof(BasicViveSrAnipalGazeTracker).CamelCaseToHumanReadable()} Events");
			
			EditorGUILayout.PropertyField(gazeValidChanged);
		}
	}
}