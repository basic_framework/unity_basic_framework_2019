﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr.Editor
{
	[CustomEditor(typeof(BasicViveSrAnipalManager))]
	public class BasicViveSrAnipalManagerEditor : BasicBaseEditor<BasicViveSrAnipalManager>
	{
	
		// **************************************** VARIABLES ****************************************\\

		protected override bool UpdateConstantlyInPlayMode => true;
		
		protected override bool HasDebugSection => true;

		protected override bool DebugSectionIsExpandable => true;

		protected override bool DebugSectionIsExpanded
		{
			get => serializedObject.FindProperty("_enableModuleEye").isExpanded; 
			set => serializedObject.FindProperty("_enableModuleEye").isExpanded = value;
		}
		

		private ReorderableList _reorderableListFrameworkStatusCompare;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		//***** overrides *****\\
		
		protected override void OnEnable()
		{
			base.OnEnable();

			var frameworkStatusEyeChangedCompare = serializedObject.FindProperty("_frameworkStatusEyeChangedCompare");

			_reorderableListFrameworkStatusCompare =
				BasicEditorGuiLayout.CreateBasicReorderableList(serializedObject, frameworkStatusEyeChangedCompare,
					true);
		}

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();

			EditorGUILayout.HelpBox("There can only be one instance of this component in the scene. Any duplicates will be deleted", 
				MessageType.Info);
			
			GUI.enabled = false;
			
			EditorGUILayout.LabelField(nameof(TypedTarget.FrameworkStatusEye).CamelCaseToHumanReadable(),
				TypedTarget.FrameworkStatusEye.ToString().CamelCaseToHumanReadable());

			Space();
			DrawEyeTrackingDataFields(BasicViveSrAnipalInputManager.EyeTrackingData);

			GUI.enabled = PrevGuiEnabled;
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var dontDestroyOnLoad = serializedObject.FindProperty("_dontDestroyOnLoad");
			var enableModuleEye = serializedObject.FindProperty("_enableModuleEye");
			
			SectionHeader($"{nameof(BasicViveSrAnipalManager).CamelCaseToHumanReadable()} Properties");

			EditorGUILayout.PropertyField(dontDestroyOnLoad);
			EditorGUILayout.PropertyField(enableModuleEye);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var frameworkStatusEyeChanged = serializedObject.FindProperty("_frameworkStatusEyeChanged");
			
			SectionHeader($"{nameof(BasicViveSrAnipalManager).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(frameworkStatusEyeChanged);
			_reorderableListFrameworkStatusCompare.DoLayoutList();
		}


		//***** functions *****\\
		
		private void DrawEyeTrackingDataFields(BasicViveSrEyeTrackingData eyeTrackingData)
		{
			SectionHeader("Tracking Data");
			EditorGUILayout.LabelField(nameof(eyeTrackingData.UserPresent).CamelCaseToHumanReadable(),
				eyeTrackingData.UserPresent.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(eyeTrackingData.FrameSequence).CamelCaseToHumanReadable(),
				eyeTrackingData.FrameSequence.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(eyeTrackingData.TimeStamp).CamelCaseToHumanReadable(),
				eyeTrackingData.TimeStamp.ToString().CamelCaseToHumanReadable());
			
			Space();
			SectionHeader("Combined Eye");
//			EditorGUILayout.LabelField("Eye Combined");
			DrawEyeDataCombinedFields(eyeTrackingData.EyeDataCombined);

			Space();
			SectionHeader("Left Eye");
//			EditorGUILayout.LabelField("Eye Left");
			DrawEyeDataSingleFields(eyeTrackingData.EyeDataSingleLeft);

			Space();
			SectionHeader("Right Eye");
//			EditorGUILayout.LabelField("Eye Right");
			DrawEyeDataSingleFields(eyeTrackingData.EyeDataSingleRight);
		}
		
		private void DrawEyeDataCombinedFields(BasicViveSrEyeDataCombined eyeData)
		{
			EditorGUILayout.LabelField(nameof(eyeData.IsValidConvergenceDistance).CamelCaseToHumanReadable(),
				eyeData.IsValidConvergenceDistance.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(eyeData.ConvergenceDistanceMillimeters).CamelCaseToHumanReadable(),
				eyeData.ConvergenceDistanceMillimeters.ToString().CamelCaseToHumanReadable());

			Space();
			DrawEyeDataBaseFields(eyeData);
		}
		
		private void DrawEyeDataSingleFields(BasicViveSrEyeDataSingle eyeData)
		{
			EditorGUILayout.LabelField(nameof(eyeData.EyeFrown).CamelCaseToHumanReadable(),
				eyeData.EyeFrown.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(eyeData.EyeSqueeze).CamelCaseToHumanReadable(),
				eyeData.EyeSqueeze.ToString().CamelCaseToHumanReadable());
			
			EditorGUILayout.LabelField(nameof(eyeData.EyeWide).CamelCaseToHumanReadable(),
				eyeData.EyeWide.ToString().CamelCaseToHumanReadable());

			Space();
			DrawEyeDataBaseFields(eyeData);
		}

		private void DrawEyeDataBaseFields(BasicViveSrEyeDataBase eyeData)
		{
			// TODO: display all valid options

			DrawValidVariableField(
				nameof(eyeData.EyeOpenness).CamelCaseToHumanReadable(), 
				eyeData.EyeOpenness.ToString(),
				eyeData.IsValidEyeOpenness);
			
			DrawValidVariableField(
				nameof(eyeData.GazeDirectionLocal).CamelCaseToHumanReadable(), 
				eyeData.GazeDirectionLocal.ToString(),
				eyeData.IsValidGazeDirection);
			
			DrawValidVariableField(
				nameof(eyeData.GazeOriginLocalMillimeters).CamelCaseToHumanReadable(), 
				eyeData.GazeOriginLocalMillimeters.ToString(),
				eyeData.IsValidGazeOrigin);

			DrawValidVariableField(
				nameof(eyeData.PupilDiameterMillimeter).CamelCaseToHumanReadable(), 
				eyeData.PupilDiameterMillimeter.ToString(),
				eyeData.IsValidPupilDiameter);
			
			DrawValidVariableField(
				nameof(eyeData.PupilPositionInSensorArea).CamelCaseToHumanReadable(), 
				eyeData.PupilPositionInSensorArea.ToString(),
				eyeData.IsValidPupilPosition);
			
		}

		private void DrawValidVariableField(string label, string value, bool valid)
		{
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField(label, value);
			EditorGUILayout.Toggle(valid, GUILayout.Width(40));
//			EditorGUILayout.LabelField(value, GUILayout.ExpandWidth(true));

			EditorGUILayout.EndHorizontal();
		}
	}
}