﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	public static class BasicViveSrAnipalInputManager 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private static readonly BasicViveSrEyeTrackingData _eyeTrackingDataDefault = new BasicViveSrEyeTrackingData();
		private static readonly BasicViveSrEyeTrackingData _eyeTrackingDataCurrent = new BasicViveSrEyeTrackingData();

		public static BasicViveSrEyeTrackingData EyeTrackingData => 
			IsValidEyeData ? _eyeTrackingDataCurrent : _eyeTrackingDataDefault;
		
		public static bool IsValidEyeData => Mathf.Abs(Time.frameCount - _lastFrameEyeTrackingUpdated) < 2;

		
		private static EyeData_v2 _eyeDataV2;

		private static int _lastFrameEyeTrackingUpdated;
		

		// ****************************************  METHODS  ****************************************\\

		public static void UpdateEyeTrackingData(ref EyeData_v2 eyeData)
		{
			if (Time.frameCount == _lastFrameEyeTrackingUpdated) return;
			_lastFrameEyeTrackingUpdated = Time.frameCount;
			
			EyeTrackingData.UpdateEyeTrackingData(ref eyeData);
		}

		
		public static BasicViveSrEyeDataBase GetEyeData(EyeModes eyeMode)
		{
			switch (eyeMode)
			{
				case EyeModes.Combined:
					return EyeTrackingData.EyeDataCombined;

				case EyeModes.Left:
					return EyeTrackingData.EyeDataSingleLeft;

				case EyeModes.Right:
					return EyeTrackingData.EyeDataSingleRight;

				default:
					throw new ArgumentOutOfRangeException(nameof(eyeMode), eyeMode, null);
			}
		}
		
		public static Ray GetGazeRay(Transform transformHmd, EyeModes eyeMode)
		{
			var gazePose = GetGazePose(transformHmd, eyeMode);
			return new Ray(gazePose.Position, gazePose.Rotation * Vector3.forward);
		}

		public static BasicPose GetGazePose(Transform transformHmd, EyeModes eyeMode)
		{
			var eyeData = GetEyeData(eyeMode);
			
			var gazeFlag = EyeDataValidityFlags.GazeOrigin | EyeDataValidityFlags.GazeDirection;
			var gazeValid = eyeData.GetDataValidity(gazeFlag);
			
			if (gazeValid)
			{
				return new BasicPose(
					transformHmd.TransformPoint(eyeData.GazeOriginLocalMetres),
					MathUtility.Direction2Quaternion(transformHmd.TransformDirection(eyeData.GazeDirectionLocal)));
			}

			return new BasicPose(transformHmd.position, transformHmd.rotation);
		}
		
	}
}