﻿using BasicFramework.Core.BasicGui.Pointer;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr
{
	[AddComponentMenu("Basic Framework/Third Party/Vive SR/Basic Vive SR Anipal Gaze Gui")]
	public class BasicViveSrAnipalGazePointerGui : GuiPointerBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The transform of the HMD")]
		[SerializeField] private Transform _transformHmd = default;
		
		[Tooltip("Which eye to use for this ray")]
		[SerializeField] private EyeModes _eyeMode = EyeModes.Combined;

		[Tooltip("How long the user needs to look at the same object to select it")]
		[SerializeField] private float _selectDuration = 1.5f;
		
		[Tooltip("Raised when th Gaze validity changes")]
		[SerializeField] private BoolPlusUnityEvent _gazeValidChanged = default;
		
		
		private bool _gazeValid;
		public bool GazeValid
		{
			get => _gazeValid;
			private set
			{
				if (_gazeValid == value) return;
				_gazeValid = value;
				GazeValidUpdated(value);
			}
		}
		
	
		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\

		protected override void OnDisable()
		{
			base.OnDisable();

			GazeValid = false;
		}

		protected override void Start()
		{
			base.Start();
			
			GazeValidUpdated(_gazeValid);
		}

		protected override void Update()
		{
			base.Update();
			
			var eyeData = BasicViveSrAnipalInputManager.GetEyeData(_eyeMode);

			var gazeFlag = EyeDataValidityFlags.GazeOrigin | EyeDataValidityFlags.GazeDirection;
			GazeValid = eyeData.GetDataValidity(gazeFlag);
		}
		
		
		//***** overrides *****\\
		
		protected override bool GetButtonHeld()
		{
			// TODO: check how long we have been hovering the same object
			throw new System.NotImplementedException();
		}

		protected override BasicPose GetPointerPose()
		{
			return BasicViveSrAnipalInputManager.GetGazePose(_transformHmd, _eyeMode);
		}
		
		//***** property updated *****\\
		
		private void GazeValidUpdated(bool value)
		{
			AllowPointerEnabled = value;
			
			_gazeValidChanged.Raise(value);
		}
	}
}