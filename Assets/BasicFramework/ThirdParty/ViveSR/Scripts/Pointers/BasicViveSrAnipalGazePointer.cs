﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Interactions.Pointer;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr
{
	[AddComponentMenu("Basic Framework/Third Party/Vive SR/Basic Vive SR Anipal Gaze Pointer")]
	public class BasicViveSrAnipalGazePointer : PhysicsPointerLineBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The transform of the HMD")]
		[SerializeField] private Transform _transformHmd = default;
		
		[Tooltip("Which eye to track")]
		[SerializeField] private EyeModes _eyeMode = EyeModes.Combined;

		[Tooltip("Raised when th Gaze validity changes")]
		[SerializeField] private BoolPlusUnityEvent _gazeValidChanged = default;
		
		
		public Transform TransformHmd => _transformHmd;
		
		public EyeModes EyeMode
		{
			get => _eyeMode;
			set => _eyeMode = value;
		}

		public BoolPlusUnityEvent GazeValidChanged => _gazeValidChanged;
		
		
		private bool _gazeValid;
		public bool GazeValid
		{
			get => _gazeValid;
			private set
			{
				if (_gazeValid == value) return;
				_gazeValid = value;
				GazeValidUpdated(value);
			}
		}
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** mono *****\\

		protected override void OnDisable()
		{
			base.OnDisable();

			GazeValid = false;
		}

		protected override void Start()
		{
			base.Start();
			
			GazeValidUpdated(_gazeValid);
		}

		protected override void Update()
		{
			base.Update();
			
			var eyeData = BasicViveSrAnipalInputManager.GetEyeData(_eyeMode);

			var gazeFlag = EyeDataValidityFlags.GazeOrigin | EyeDataValidityFlags.GazeDirection;
			GazeValid = eyeData.GetDataValidity(gazeFlag);
		}


		//***** overrides *****\\
		
		protected override BasicPose GetPointerPose()
		{
			return BasicViveSrAnipalInputManager.GetGazePose(_transformHmd, _eyeMode);
		}
		
		
		//***** property updated *****\\
		
		private void GazeValidUpdated(bool value)
		{
			AllowPointerEnabled = value;
			
			_gazeValidChanged.Raise(value);
		}
		
	}
}