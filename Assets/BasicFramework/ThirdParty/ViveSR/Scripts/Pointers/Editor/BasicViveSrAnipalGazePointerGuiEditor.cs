﻿using BasicFramework.Core.BasicGui.Pointer.Editor;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr.Editor
{
	[CustomEditor(typeof(BasicViveSrAnipalGazePointerGui))]
	public class BasicViveSrAnipalGazePointerGuiEditor : GuiPointerBaseEditor<BasicViveSrAnipalGazePointerGui>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			EditorGUILayout.LabelField(nameof(TypedTarget.GazeValid).CamelCaseToHumanReadable(), 
				TypedTarget.GazeValid.ToString());
		}
		
		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var transformHmd = serializedObject.FindProperty("_transformHmd");
			var eyeMode = serializedObject.FindProperty("_eyeMode");
			var selectDuration = serializedObject.FindProperty("_selectDuration");
			
			SectionHeader($"{nameof(BasicViveSrAnipalGazePointerGui).CamelCaseToHumanReadable()} Properties");
			
			EditorGUILayout.PropertyField(eyeMode);
			BasicEditorGuiLayout.PropertyFieldNotNull(transformHmd);
		}
		
		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var gazeValidChanged = serializedObject.FindProperty("_gazeValidChanged");
			
			Space();
			SectionHeader($"{nameof(BasicViveSrAnipalGazePointerGui).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(gazeValidChanged);
		}
	}
}