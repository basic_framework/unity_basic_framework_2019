﻿using BasicFramework.Core.Interactions.Pointer.Editor;
using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr.Editor
{
	[CustomEditor(typeof(BasicViveSrAnipalGazePointer))]
	public class BasicViveSrAnipalGazePointerEditor : PhysicsPointerLineBaseEditor<BasicViveSrAnipalGazePointer>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawDebugFields()
		{
			base.DrawDebugFields();
			
			Space();
			EditorGUILayout.LabelField(nameof(TypedTarget.GazeValid).CamelCaseToHumanReadable(), 
				TypedTarget.GazeValid.ToString());
		}

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();

			var transformHmd = serializedObject.FindProperty("_transformHmd");
			var eyeMode = serializedObject.FindProperty("_eyeMode");
			
			Space();
			SectionHeader($"{nameof(BasicViveSrAnipalGazePointer).CamelCaseToHumanReadable()} Properties");

			BasicEditorGuiLayout.PropertyFieldNotNull(transformHmd);
			EditorGUILayout.PropertyField(eyeMode);
		}

		protected override void DrawEventFields()
		{
			base.DrawEventFields();
			
			var gazeValidChanged = serializedObject.FindProperty("_gazeValidChanged");
			
			Space();
			SectionHeader($"{nameof(BasicViveSrAnipalGazePointer).CamelCaseToHumanReadable()} Events");

			EditorGUILayout.PropertyField(gazeValidChanged);
		}
		
	}
}