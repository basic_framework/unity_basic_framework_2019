﻿using BasicFramework.Core.Utility.Editor;
using BasicFramework.Core.Utility.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr.Editor
{
	[CustomEditor(typeof(BasicViveSrAnipalEyeBehaviour))]
	public class BasicViveSrAnipalEyeBehaviourEditor : BasicBaseEditor<BasicViveSrAnipalEyeBehaviour>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
	
		// ****************************************  METHODS  ****************************************\\

		protected override void DrawPropertyFields()
		{
			base.DrawPropertyFields();
			
			var transformEye = serializedObject.FindProperty("_transformEye");
			var eyeMode = serializedObject.FindProperty("_eyeMode");
			var transformHmd = serializedObject.FindProperty("_transformHmd");
			
			SectionHeader($"{nameof(BasicViveSrAnipalEyeBehaviour).CamelCaseToHumanReadable()} Properties");
			
			BasicEditorGuiLayout.PropertyFieldNotNull(transformEye);
			EditorGUILayout.PropertyField(eyeMode);
			BasicEditorGuiLayout.PropertyFieldNotNull(transformHmd);
		}
	}
}