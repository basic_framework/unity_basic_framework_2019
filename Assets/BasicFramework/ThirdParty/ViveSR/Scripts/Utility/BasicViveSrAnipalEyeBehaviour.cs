﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr
{
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_BASIC_TRACKING)]
	public class BasicViveSrAnipalEyeBehaviour : MonoBehaviour 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[Tooltip("The transform that will be positioned to follow the eye")]
		[SerializeField] private Transform _transformEye = default;
		
		[Tooltip("Which eye to use")]
		[SerializeField] private EyeModes _eyeMode = EyeModes.Combined;
		
		[Tooltip("The transform of the HMD")]
		[SerializeField] private Transform _transformHmd = default;


		public Transform TransformEye => _transformEye;

		public Transform TransformHmd => _transformHmd;

		public EyeModes EyeMode => _eyeMode;


		// ****************************************  METHODS  ****************************************\\

		private void Reset()
		{
			_transformEye = transform;
		}

		private void Update()
		{
			var ray = BasicViveSrAnipalInputManager.GetGazeRay(_transformHmd, _eyeMode);
			
			_transformEye.position = ray.origin;
			_transformEye.rotation = MathUtility.Direction2Quaternion(ray.direction, _transformHmd.up);
		}
		
	}
}