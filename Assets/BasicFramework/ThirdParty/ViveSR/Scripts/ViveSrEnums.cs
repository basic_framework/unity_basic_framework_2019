﻿using System;
using BasicFramework.Core.BasicTypes;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.ThirdParty.ViveSr
{

	[Flags]
	public enum EyeDataValidityFlags
	{
		GazeOrigin 					= 1 << 0,
		GazeDirection 				= 1 << 1,
		PupilDiameter 				= 1 << 2,
		EyeOpenness					= 1 << 3,
		PupilPositionInSensorArea 	= 1 << 4,
	}

	public enum EyeModes
	{
		Combined,
		Left,
		Right
	}
	
	public enum ViveSrAnipalFrameworkStatus
	{
		Stopped,
//		Start,
		Working,
		Error,
		Unsupported
	}
	
	[Serializable]
	public class ViveSrAnipalFrameworkStatusUnityEvent : UnityEvent<ViveSrAnipalFrameworkStatus>{}

	[Serializable]
	public class ViveSrAnipalFrameworkStatusCompareUnityEvent : CompareValueUnityEventBase<ViveSrAnipalFrameworkStatus>{}
	
}