﻿using System;
using BasicFramework.Core.BasicMath.Utility;
using BasicFramework.Core.BasicTypes.Events;
using BasicFramework.Core.Utility;
using UnityEngine;

namespace BasicFramework.ThirdParty.ViveSr
{
	[AddComponentMenu("Basic Framework/Third Party/Vive SR/Basic Vive SR Anipal Gaze Tracker")]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_BASIC_TRACKING)]
	public class BasicViveSrAnipalGazeTracker : MonoBehaviour 
	{
		
		// Aligns a transform to the eye tracked by the VIVE PRO EYE headset
	
		// **************************************** VARIABLES ****************************************\\
	
		[Tooltip("The transform of the HMD")]
		[SerializeField] private Transform _transformHmd = default;

		[Tooltip("The transform representing the gaze origin and direction (the transform that will be moved)")]
		[SerializeField] private Transform _transformGaze = default;
		
		[Tooltip("Which eye to track")]
		[SerializeField] private EyeModes _eyeMode = EyeModes.Combined;

		[Tooltip("Raised when th Gaze validity changes")]
		[SerializeField] private BoolPlusUnityEvent _gazeValidChanged = default;


		public Transform TransformHmd => _transformHmd;

		public Transform TransformGaze => _transformGaze;

		public EyeModes EyeMode
		{
			get => _eyeMode;
			set => _eyeMode = value;
		}

		public BoolPlusUnityEvent GazeValidChanged => _gazeValidChanged;


		private bool _gazeValid;
		public bool GazeValid
		{
			get => _gazeValid;
			private set
			{
				if (_gazeValid == value) return;
				_gazeValid = value;
				GazeValidUpdated(value);
			}
		}


		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		private void Reset()
		{
			_transformGaze = transform;
		}

		private void OnDisable()
		{
			GazeValid = false;
		}

		private void Start()
		{
			GazeValidUpdated(_gazeValid);
		}

		private void Update()
		{
			var eyeData = BasicViveSrAnipalInputManager.GetEyeData(_eyeMode);

			var gazeFlag = EyeDataValidityFlags.GazeOrigin | EyeDataValidityFlags.GazeDirection;
			GazeValid = eyeData.GetDataValidity(gazeFlag);
//			Debug.Log($"Gaze Flags: {gazeFlag}, Gaze Valid: {GazeValid}");
			
			// Possibly default values if gaze is not valid
			if (_gazeValid)
			{
				_transformGaze.position = _transformHmd.TransformPoint(eyeData.GazeOriginLocalMetres);
				_transformGaze.rotation =
					MathUtility.Direction2Quaternion(_transformHmd.TransformDirection(eyeData.GazeDirectionLocal),
						_transformHmd.up);
			}
			else
			{
				_transformGaze.position = _transformHmd.position;
				_transformGaze.rotation = _transformHmd.rotation;
			}
			
		}
		
		
		//***** property updated *****\\
		
		private void GazeValidUpdated(bool value)
		{
			_gazeValidChanged.Raise(value);
		}
		
	}
}