﻿using System;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	public class BasicViveSrEyeTrackingData : ICloneable
	{
		
		// EyeTrackingData
		//	EyeDataBase 	: ICloneable
		//	EyeDataSingle 	: EyeDataBase
		//  EyeDataCombined	: EyeDataBase
		
	
		// **************************************** VARIABLES ****************************************\\
		
		// Combined eye data
		
		// SRanipal_EyeDataType_v2.cs, EyeData_v2
		private bool _userPresent;	// Indicate if there is a user in front of HMD
		public bool UserPresent => _userPresent;
		
		private int _frameSequence;	// The frame sequence
		public int FrameSequence => _frameSequence;
		
		private int _timeStamp;		// The time when the frame was capturing. in millisecond
		public int TimeStamp => _timeStamp;

		
		// Eye Data
		private BasicViveSrEyeDataCombined _eyeDataCombined = new BasicViveSrEyeDataCombined();
		public BasicViveSrEyeDataCombined EyeDataCombined => _eyeDataCombined;
		
		private BasicViveSrEyeDataSingle _eyeDataSingleLeft = new BasicViveSrEyeDataSingle();
		public BasicViveSrEyeDataSingle EyeDataSingleLeft => _eyeDataSingleLeft;
		
		private BasicViveSrEyeDataSingle _eyeDataSingleRight = new BasicViveSrEyeDataSingle();
		public BasicViveSrEyeDataSingle EyeDataSingleRight => _eyeDataSingleRight;
		
		
		// ****************************************  METHODS  ****************************************\\
		
		//***** implementations *****\\
		
		public object Clone()
		{
			var clone = new BasicViveSrEyeTrackingData();
			
			clone._userPresent = _userPresent;
			clone._frameSequence = _frameSequence;
			clone._timeStamp = _timeStamp;

			clone._eyeDataCombined = _eyeDataCombined.Clone() as BasicViveSrEyeDataCombined;
			clone._eyeDataSingleLeft  = _eyeDataSingleLeft.Clone() as BasicViveSrEyeDataSingle;
			clone._eyeDataSingleRight = _eyeDataSingleRight.Clone() as BasicViveSrEyeDataSingle;
			
			return clone;
		}
		
		
		//***** functions *****\\

		public void UpdateEyeTrackingData(ref EyeData_v2 eyeData)
		{
			_userPresent = eyeData.no_user;
			_frameSequence = eyeData.frame_sequence;
			_timeStamp = eyeData.timestamp;
			
			_eyeDataCombined.UpdateEyeDataSCombined(ref eyeData.verbose_data.combined);
			_eyeDataSingleLeft.UpdateEyeDataSingle(eyeData.expression_data.left, ref eyeData.verbose_data.left);
			_eyeDataSingleRight.UpdateEyeDataSingle(eyeData.expression_data.right, ref eyeData.verbose_data.right);
		}

//		public void ClearData()
//		{
//			_userPresent = false;
//			_frameSequence = 0;
//			_timeStamp = 0;
//		}
	}
}