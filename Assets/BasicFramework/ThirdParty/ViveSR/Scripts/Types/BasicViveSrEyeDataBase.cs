﻿using System;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	// Look at SRanipal_EyeData.cs > SingleEyeData
	public class BasicViveSrEyeDataBase : ICloneable
	{
		// **************************************** VARIABLES ****************************************\\

		private EyeDataValidityFlags _eyeDataValidityFlags;
		
		
		// The point in the eye from which the gaze ray originates in meter miles.(right-handed coordinate system)
		// TODO: convert this to world units
		private Vector3 _gazeOriginLocalMillimeters;
		public Vector3 GazeOriginLocalMillimeters => _gazeOriginLocalMillimeters;
		public Vector3 GazeOriginLocalMetres => _gazeOriginLocalMillimeters * 0.001f;
		
		
		// The normalized gaze direction of the eye in [0,1].(right-handed coordinate system)
		private Vector3 _gazeDirectionLocal;
		public Vector3 GazeDirectionLocal => _gazeDirectionLocal;

		
		// The diameter of the pupil in meter miles
		private float _pupilDiameterMillimeter;
		public float PupilDiameterMillimeter => _pupilDiameterMillimeter;

		
		// A value representing how open the eye is
		private float _eyeOpenness;
		public float EyeOpenness => _eyeOpenness;

		
		// The normalized position of a pupil in [0,1]
		private Vector2 _pupilPositionInSensorArea;
		public Vector2 PupilPositionInSensorArea => _pupilPositionInSensorArea;


		public bool IsValidGazeOrigin => (_eyeDataValidityFlags & EyeDataValidityFlags.GazeOrigin) ==
		                                 EyeDataValidityFlags.GazeOrigin;
		
		public bool IsValidGazeDirection => (_eyeDataValidityFlags & EyeDataValidityFlags.GazeDirection) ==
		                                 EyeDataValidityFlags.GazeDirection;
		
		public bool IsValidEyeOpenness => (_eyeDataValidityFlags & EyeDataValidityFlags.EyeOpenness) ==
		                                 EyeDataValidityFlags.EyeOpenness;
		
		public bool IsValidPupilDiameter => (_eyeDataValidityFlags & EyeDataValidityFlags.PupilDiameter) ==
		                                 EyeDataValidityFlags.PupilDiameter;
		
		public bool IsValidPupilPosition => (_eyeDataValidityFlags & EyeDataValidityFlags.PupilPositionInSensorArea) ==
		                                 EyeDataValidityFlags.PupilPositionInSensorArea;
		
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** virtual *****\\

//		public virtual void ClearData()
//		{
//			_eyeDataValidityFlags = default;
//			
//			_gazeOriginMillimeters = default;
//			_gazeDirection = default;
//			_eyeOpenness = default;
//			_pupilDiameterMillimeter = default;
//			_pupilPositionInSensorArea = default;
//		}
		
		
		//***** implementations *****\\
		
		public object Clone()
		{
			var clone = new BasicViveSrEyeDataBase();

			clone._eyeDataValidityFlags = _eyeDataValidityFlags;
			
			clone._gazeOriginLocalMillimeters = _gazeOriginLocalMillimeters;
			clone._gazeDirectionLocal = _gazeDirectionLocal;
			clone._eyeOpenness = _eyeOpenness;
			clone._pupilDiameterMillimeter = _pupilDiameterMillimeter;
			clone._pupilPositionInSensorArea = _pupilPositionInSensorArea;
			
			return clone;
		}
		
		
		//***** functions *****\\

		protected void UpdateEyeData(ref SingleEyeData eyeData)
		{
			// Note: Only read from eyeData

			// Update the validity status of the data
//			_eyeDataValidityFlags = 0;

			UpdateValidityBit(EyeDataValidityFlags.GazeOrigin, 
				eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY));
			
			UpdateValidityBit(EyeDataValidityFlags.EyeOpenness, 
				eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY));
			
			UpdateValidityBit(EyeDataValidityFlags.GazeDirection, 
				eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY));
			
			UpdateValidityBit(EyeDataValidityFlags.PupilDiameter, 
				eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY));
			
			UpdateValidityBit(EyeDataValidityFlags.PupilPositionInSensorArea, 
				eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY));
			
	
//			if (eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY))
//				_eyeDataValidityFlags |= EyeDataValidityFlags.GazeOrigin;
//			else
//				_eyeDataValidityFlags &= ~EyeDataValidityFlags.GazeOrigin;
//
//			if (eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY))
//				_eyeDataValidityFlags |= EyeDataValidityFlags.EyeOpenness;
//			else
//				_eyeDataValidityFlags &= ~EyeDataValidityFlags.EyeOpenness;
//
//			if (eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY))
//				_eyeDataValidityFlags |= EyeDataValidityFlags.GazeDirection;
//			else
//				_eyeDataValidityFlags &= ~EyeDataValidityFlags.GazeDirection;
//
//			if (eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY))
//				_eyeDataValidityFlags |= EyeDataValidityFlags.PupilDiameter;
//			else
//				_eyeDataValidityFlags &= ~EyeDataValidityFlags.PupilDiameter;
//
//			if (eyeData.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY))
//				_eyeDataValidityFlags |= EyeDataValidityFlags.PupilPositionInSensorArea;
//			else
//				_eyeDataValidityFlags &= EyeDataValidityFlags.PupilPositionInSensorArea;


			// Update the local data, possibly set default values if data is invalid
			
			// I THINK THIS IS RELATIVE TO THE HEADSET POSITION, X NEEDS TO BE FLIPPED THOUGH
			_gazeOriginLocalMillimeters = eyeData.gaze_origin_mm;
			_gazeOriginLocalMillimeters.x *= -1f;

			// I THINK THIS IS RELATIVE TO THE HEADSET POSITION, X NEEDS TO BE FLIPPED THOUGH
			_gazeDirectionLocal = eyeData.gaze_direction_normalized;
			_gazeDirectionLocal.x *= -1f;
			
			_pupilDiameterMillimeter = eyeData.pupil_diameter_mm;
			_eyeOpenness = eyeData.eye_openness;
			_pupilPositionInSensorArea = eyeData.pupil_position_in_sensor_area;
		}

		private void UpdateValidityBit(EyeDataValidityFlags eyeDataValidityFlags, bool isValid)
		{
			if (isValid)
				_eyeDataValidityFlags |= eyeDataValidityFlags;
			else
				_eyeDataValidityFlags &= ~eyeDataValidityFlags;
		}

		// Note: could also return default value 
		public bool GetDataValidity(EyeDataValidityFlags eyeDataFlags)
		{
			return (_eyeDataValidityFlags & eyeDataFlags) == eyeDataFlags;
		}

	}
}