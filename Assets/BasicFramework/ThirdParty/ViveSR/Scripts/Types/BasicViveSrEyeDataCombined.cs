﻿using System;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	public class BasicViveSrEyeDataCombined : BasicViveSrEyeDataBase 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// SRanipal_EyeData.cs > CombinedEyeData
		private bool _isValidConvergenceDistance;
		public bool IsValidConvergenceDistance => _isValidConvergenceDistance;
		
		private float _convergenceDistanceMillimeters;
		public float ConvergenceDistanceMillimeters => _convergenceDistanceMillimeters;


		// ****************************************  METHODS  ****************************************\\
		
		
		//***** implementations *****\\
		
		public object Clone()
		{
			var clone = base.Clone() as BasicViveSrEyeDataCombined;

			clone._isValidConvergenceDistance = _isValidConvergenceDistance;
			clone._convergenceDistanceMillimeters = _convergenceDistanceMillimeters;

			return clone;
		}
		
		
		//***** overrides *****\\

//		public override void ClearData()
//		{
//			base.ClearData();
//
//			_isValidConvergenceDistance = default;
//			_convergenceDistanceMillimeters = default;
//		}


		//***** functions *****\\
		
		public void UpdateEyeDataSCombined(ref CombinedEyeData combinedEyeData)
		{
			base.UpdateEyeData(ref combinedEyeData.eye_data);
			
			_isValidConvergenceDistance = combinedEyeData.convergence_distance_validity;
			
			// Possibly set to default if convergence distance not valid 
			_convergenceDistanceMillimeters = combinedEyeData.convergence_distance_mm;
		}
	
	}
}