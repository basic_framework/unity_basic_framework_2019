﻿using System;
using UnityEngine;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	public class BasicViveSrEyeDataSingle : BasicViveSrEyeDataBase
	{
	
		// **************************************** VARIABLES ****************************************\\

		// SRanipal_EyeDataType_v2.cs, SingleEyeExpression
		private float _eyeFrown;		// A value representing user's frown
		public float EyeFrown => _eyeFrown;
		
		private float _eyeSqueeze;	// A value representing how the eye is closed tightly
		public float EyeSqueeze => _eyeSqueeze;
		
		private float _eyeWide;		// A value representing how open eye widely
		public float EyeWide => _eyeWide;
		

		// ****************************************  METHODS  ****************************************\\
		
		//***** implementations *****\\
		
		public object Clone()
		{
			var clone = base.Clone() as BasicViveSrEyeDataSingle;

			clone._eyeFrown = _eyeFrown;
			clone._eyeSqueeze = _eyeSqueeze;
			clone._eyeWide = _eyeWide;

			return clone;
		}
		
		
		//***** overrides *****\\

//		public override void ClearData()
//		{
//			base.ClearData();
//
//			_eyeFrown = default;
//			_eyeSqueeze = default;
//			_eyeWide = default;
//		}
		
		
		//***** functions *****\\
		
		public void UpdateEyeDataSingle(SingleEyeExpression singleEyeExpression, ref SingleEyeData eyeData)
		{
			base.UpdateEyeData(ref eyeData);
			
			_eyeFrown = singleEyeExpression.eye_frown;
			_eyeSqueeze = singleEyeExpression.eye_squeeze;
			_eyeWide = singleEyeExpression.eye_wide;
		}
	}
}