﻿using System;
using BasicFramework.Core.Utility;
using UnityEngine;
using ViveSR;
using ViveSR.anipal;
using ViveSR.anipal.Eye;

namespace BasicFramework.ThirdParty.ViveSr
{
	[AddComponentMenu("Basic Framework/Third Party/Vive SR/Basic Vive SR Anipal Manager")]
	[DefaultExecutionOrder(BasicExecutionOrder.EXECUTION_ORDER_EXTERNAL_TRACKING)]
	public class BasicViveSrAnipalManager : MonoBehaviour 
	{
		
		// TODO: Have a static class that stores our eye data

		// **************************************** VARIABLES ****************************************\\
		
		public static BasicViveSrAnipalManager Instance { get; private set; }
		public static bool InstanceNotNull { get; private set; }

		[Tooltip("Stop the gameobject from being destroyed on load.\n" +
		         "Gameobject must be at the root of the scene heirarchy for this to work.")]
		[SerializeField] private bool _dontDestroyOnLoad = true;

		[Tooltip("Should the ViveSR eye module be enabled")]
		[SerializeField] private bool _enableModuleEye = true;

		[SerializeField] private ViveSrAnipalFrameworkStatusUnityEvent _frameworkStatusEyeChanged = default;
		
		[SerializeField] private ViveSrAnipalFrameworkStatusCompareUnityEvent[] _frameworkStatusEyeChangedCompare = default;

		
		public bool EnableModuleEye => _enableModuleEye;

		public ViveSrAnipalFrameworkStatusUnityEvent FrameworkStatusEyeChanged => _frameworkStatusEyeChanged;


		private ViveSrAnipalFrameworkStatus _frameworkStatusEye;
		public ViveSrAnipalFrameworkStatus FrameworkStatusEye
		{
			get => _frameworkStatusEye;
			private set
			{
				if (_frameworkStatusEye == value) return;
				_frameworkStatusEye = value;
				FrameworkStatusEyeUpdated(value);
			}
		}

		private EyeData_v2 _eyeDataV2;
		

		// ****************************************  METHODS  ****************************************\\

		
		//***** mono *****\\

		private void Awake()
		{
			if (Instance != null)
			{
				Destroy(this);
				return;
			}

			Instance = this;
			InstanceNotNull = true;

			if (_dontDestroyOnLoad)
			{
				DontDestroyOnLoad(gameObject);
			}
			
			StartFramework();
		}

		private void OnDestroy()
		{
			StopFramework();

			Instance = null;
			InstanceNotNull = false;
		}

		private void Start()
		{
			FrameworkStatusEyeUpdated(_frameworkStatusEye);
		}

		private void Update()
		{
			// Get eye data here, yes this updates in the unity update loop,
			// but we can't do anything with the data anyway until the update loop so why not get it here
			// Could possibly use the SRanipal_Eye_API.RegisterEyeDataCallback in future for immediate updates

			if (_frameworkStatusEye == ViveSrAnipalFrameworkStatus.Working)
			{
				var result = SRanipal_Eye_API.GetEyeData_v2(ref _eyeDataV2);
				
//				Debug.Log($"{name}/{GetType().Name} => Update eye data result {result}");

				BasicViveSrAnipalInputManager.UpdateEyeTrackingData(ref _eyeDataV2);
			}
		}

		
		//***** property updates *****\\
		
		private void FrameworkStatusEyeUpdated(ViveSrAnipalFrameworkStatus value)
		{
			_frameworkStatusEyeChanged.Invoke(value);

			foreach (var compareUnityEvent in _frameworkStatusEyeChangedCompare)
			{
				compareUnityEvent.SetCurrentValue(value);
			}
		}
		
		
		//***** functions *****\\

		
		//*** setup ***\\
		
		private void StartFramework()
		{
			if (_enableModuleEye)
			{
				EnableEyeFramework();
			}
		}

		private void StopFramework()
		{
			DisableEyeFramework();
		}

		private void EnableEyeFramework()
		{
			if(_frameworkStatusEye == ViveSrAnipalFrameworkStatus.Working) return;
			
			if (!SRanipal_Eye_API.IsViveProEye())
			{
				FrameworkStatusEye = ViveSrAnipalFrameworkStatus.Unsupported;
				return;
			}

			// Is this necessary
//			FrameworkStatusEye = ViveSrAnipalFrameworkStatus.Start;

			var result = SRanipal_API.Initial(SRanipal_Eye_v2.ANIPAL_TYPE_EYE_V2, IntPtr.Zero);
			if (result == Error.WORK)
			{
				Debug.Log("[SRanipal] Initial Eye v2: " + result);
				FrameworkStatusEye = ViveSrAnipalFrameworkStatus.Working;
			}
			else
			{
				Debug.LogError("[SRanipal] Initial Eye v2: " + result);
				FrameworkStatusEye = ViveSrAnipalFrameworkStatus.Error;
			}
		}

		private void DisableEyeFramework()
		{
			if (FrameworkStatusEye != ViveSrAnipalFrameworkStatus.Working) return;
			
			var result = SRanipal_API.Release(SRanipal_Eye_v2.ANIPAL_TYPE_EYE_V2);
			if (result == Error.WORK)
			{
				Debug.Log("[SRanipal] Release Eye v2: " + result);
			}
			else
			{
				Debug.LogError("[SRanipal] Release Eye v2: " + result);
			}

			FrameworkStatusEye = ViveSrAnipalFrameworkStatus.Stopped;
		}

	}
}