#Vive SRanipal_SDK (Basic Framework Integration)

__Note: Only add this package if you have _Vive-SRanipal-Unity-Plugin.unitypackage_ imported. 
If you do not have it imported you will get errors.
Delete the root folder to remove the errors.__


This plugin integrates Basic Frameworks Core systems with the __Vive SRanipal_SDK__ that can be found [here](https://developer.vive.com/resources/vive-sense/sdk/vive-eye-tracking-sdk-sranipal/).

