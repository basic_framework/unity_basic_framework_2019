#Node Canvas (Basic Framework Integration)

__Note: Only add this package if you have Paradox Notion's _Node Canvas_ installed. 
If you do not have it installed you will get errors.
Delete the root folder to remove the errors.__

This plugin integrates Basic Frameworks Core systems with the Node Canvas asset.

