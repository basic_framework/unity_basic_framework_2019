﻿using BasicFramework.Core.ScriptableEvents;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
	[Description("Raises the referenced ScriptableEvent after the specified delay")]
	[Category("BasicFramework/Scriptable Events")]
	public class RaiseScriptableEvent : ActionTask 
	{
	
		// **************************************** VARIABLES ****************************************\\

		[RequiredField]
		[Tooltip("The ScriptableEvent that will be raised")]
		public BBParameter<ScriptableEvent> ScriptableEvent;
		
		[Tooltip("How long to wait from the start of the action before the event is raised")]
		public BBParameter<float> Delay;
		
		protected override string info => 
			$"Raise {NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString())} after {Delay} seconds";

		// ****************************************  METHODS  ****************************************\\

		protected override void OnUpdate()
		{
			if(elapsedTime < Delay.value) return;
			ScriptableEvent.value.Raise();
			EndAction();
		}

	}
}