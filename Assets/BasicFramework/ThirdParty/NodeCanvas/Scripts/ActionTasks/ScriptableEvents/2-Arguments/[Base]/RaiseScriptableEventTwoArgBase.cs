﻿using BasicFramework.Core.ScriptableEvents;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
    [Description("Raises the referenced ScriptableEvent after the specified delay")]
    public abstract class RaiseScriptableEventTwoArgBase<T0, T1, TScriptableEvent> : ActionTask
        where TScriptableEvent : ScriptableEventBase<T0, T1>
    {
	
        // **************************************** VARIABLES ****************************************\\
	
        [RequiredField]
        [Tooltip("The ScriptableEvent that will be raised")]
        public BBParameter<TScriptableEvent> ScriptableEvent;
		
        [Tooltip("The first argument value that will be used for the event")]
        public BBParameter<T0> FirstArgumentValue;
        
        [Tooltip("The second argument value that will be used for the event")]
        public BBParameter<T1> SecondArgumentValue;
		
        [Tooltip("How long to wait from the start of the action before the event is raised")]
        public BBParameter<float> Delay;
		
        protected override string info => $"Raise {NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString())} after {Delay} seconds";
		
	
        // ****************************************  METHODS  ****************************************\\
	
        protected override void OnUpdate()
        {
            if(elapsedTime < Delay.value) return;
            ScriptableEvent.value.Raise(FirstArgumentValue.value, SecondArgumentValue.value);
            EndAction();
        }
	
    }
}