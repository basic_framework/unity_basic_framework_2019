﻿using BasicFramework.Core.ScriptableEvents;
using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
	[Description("Raises the referenced ScriptableEvent after the specified delay")]
	public abstract class RaiseScriptableEventBase<T, TScriptableEvent> : ActionTask
		where TScriptableEvent : ScriptableEventBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[RequiredField]
		[Tooltip("The ScriptableEvent that will be raised")]
		public BBParameter<TScriptableEvent> ScriptableEvent;
		
		[TextAreaField(3)]
		[Tooltip("The Argument value that will be used for the event")]
		public BBParameter<T> Value;
		
		[Tooltip("How long to wait from the start of the action before the event is raised")]
		public BBParameter<float> Delay;
		
		protected override string info
		{
			get
			{
				var scriptableEvent = NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString());
				var value = NodeCanvasUtility.ParseStringForInfo(Value.ToString());
				
				return $"Raise {scriptableEvent} with {value} after {Delay} seconds";
			}
		}


		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnUpdate()
		{
			if(elapsedTime < Delay.value) return;
			ScriptableEvent.value.Raise(Value.value);
			EndAction();
		}
	
	}
	
	[Description("Raises the referenced ScriptableEvent after the specified delay")]
	public abstract class RaiseScriptableEventBase<T, TScriptableEvent, TScriptableSingle> : ActionTask
		where TScriptableEvent : ScriptableEventBase<T>
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		
		[RequiredField]
		[Tooltip("The ScriptableEvent that will be raised")]
		public BBParameter<TScriptableEvent> ScriptableEvent;
		
		[TextAreaField(3)]
		[Tooltip("The Argument value that will be used for the event")]
		public BBParameter<T> Value;
		
		[Tooltip("Use the single argument value instead of the type value")]
		public bool UseSingle;
		
		[Tooltip("The single argument value that will be used for the event")]
		public BBParameter<TScriptableSingle> Single;
		
		[Tooltip("How long to wait from the start of the action before the event is raised")]
		public BBParameter<float> Delay;
		
		protected override string info
		{
			get
			{
				var scriptableEvent = NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString());
				var argument = UseSingle
					? NodeCanvasUtility.ParseStringForInfo(Single.ToString())
					: NodeCanvasUtility.ParseStringForInfo(Value.ToString());
				
				return $"Raise {scriptableEvent} with {argument} after {Delay} seconds";
			}
		}


		// ****************************************  METHODS  ****************************************\\
		
		protected override void OnUpdate()
		{
			if(elapsedTime < Delay.value) return;
			ScriptableEvent.value.Raise(UseSingle ? Single.value.Value : Value.value);
			EndAction();
		}
		
	}
}