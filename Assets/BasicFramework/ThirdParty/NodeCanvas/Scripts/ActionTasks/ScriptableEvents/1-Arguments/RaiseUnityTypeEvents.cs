﻿using BasicFramework.Core.ScriptableEvents;
using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseColorEvent : RaiseScriptableEventBase<Color, ColorEvent, ColorSingle>
    {
    }
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseQuaternionEvent : RaiseScriptableEventBase<Quaternion, QuaternionEvent, QuaternionSingle>
    {
    }
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseVector2Event : RaiseScriptableEventBase<Vector2, Vector2Event, Vector2Single>
    {
    }
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseVector3Event : RaiseScriptableEventBase<Vector3, Vector3Event, Vector3Single>
    {
    }
	
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseGameObjectEvent : RaiseScriptableEventBase<GameObject, GameObjectEvent, GameObjectSingle>
    {
    }
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseRectTransformEvent : RaiseScriptableEventBase<RectTransform, RectTransformEvent, RectTransformSingle>
    {
    }
	
    [Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
    public class RaiseTransformEvent : RaiseScriptableEventBase<Transform, TransformEvent, TransformSingle>
    {
    }
}