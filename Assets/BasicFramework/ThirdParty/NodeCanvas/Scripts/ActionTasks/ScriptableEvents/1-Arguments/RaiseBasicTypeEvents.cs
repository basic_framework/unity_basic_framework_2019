﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents;
using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
    [Category("BasicFramework/Scriptable Events/01-Argument/Basic Types")]
    public class RaiseBasicPoseEvent : RaiseScriptableEventBase<BasicPose, BasicPoseEvent, BasicPoseSingle>
    {
    }
}