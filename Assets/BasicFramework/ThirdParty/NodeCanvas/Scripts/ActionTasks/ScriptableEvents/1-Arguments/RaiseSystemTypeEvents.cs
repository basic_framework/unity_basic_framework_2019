﻿using BasicFramework.Core.ScriptableEvents;
using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableEvents
{
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class RaiseBoolEvent : RaiseScriptableEventBase<bool, BoolEvent, BoolSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class RaiseIntEvent : RaiseScriptableEventBase<int, IntEvent, IntSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class RaiseFloatEvent : RaiseScriptableEventBase<float, FloatEvent, FloatSingle>
	{
	}

	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class RaiseStringEvent : RaiseScriptableEventBase<string, StringEvent, StringSingle>
	{
	}
}