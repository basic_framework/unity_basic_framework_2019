﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableEvents;
using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	
	//***** struct *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetBasicDateTimeSingle : SetScriptableSingleOnlyBase<BasicDateTime, BasicDateTimeSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetBasicPoseSingle : SetScriptableSingleOnlyBase<BasicPose, BasicPoseSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetFloatRangeSingle : SetScriptableSingleOnlyBase<FloatRange, FloatRangeSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetIntRangeSingle : SetScriptableSingleOnlyBase<IntRange, IntRangeSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetNormalizedFloatSingle : SetScriptableSingleOnlyBase<NormalizedFloat, NormalizedFloatSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetRangedFloatSingle : SetScriptableSingleOnlyBase<RangedFloat, RangedFloatSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetRangedIntSingle : SetScriptableSingleOnlyBase<RangedInt, RangedIntSingle>
	{
	}
	
	
	//***** class *****\\
	
	
	//***** Object *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/Basic Types")]
	public class SetScriptableEventSingle : SetScriptableSingleBase<ScriptableEvent, ScriptableEventSingle>
	{
	}
	
}