﻿using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	
	//***** struct *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetColorSingle : SetScriptableSingleBase<Color, ColorSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetKeyCodeSingle : SetScriptableSingleBase<KeyCode, KeyCodeSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetLayerMaskSingle : SetScriptableSingleBase<LayerMask, LayerMaskSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetQuaternionSingle : SetScriptableSingleBase<Quaternion, QuaternionSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetVector2Single : SetScriptableSingleBase<Vector2, Vector2Single>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetVector3Single : SetScriptableSingleBase<Vector3, Vector3Single>
	{
	}
	
	
	//***** class *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetAnimationCurveSingle : SetScriptableSingleBase<AnimationCurve, AnimationCurveSingle>
	{
	}
	
	
	//***** Object *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetCameraSingle : SetScriptableSingleBase<Camera, CameraSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetGameObjectSingle : SetScriptableSingleBase<GameObject, GameObjectSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetMaterialSingle : SetScriptableSingleBase<Material, MaterialSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetRenderTextureSingle : SetScriptableSingleBase<RenderTexture, RenderTextureSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/Unity Types")]
	public class SetTransformSingle : SetScriptableSingleBase<Transform, TransformSingle>
	{
	}
	
}