﻿using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	[Description("Set a ScriptableVariable's value to another ScriptableVariable's value or a type value")]
	public abstract class SetScriptableSingleBase<T, TScriptableSingle> : ActionTask
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		// Write
		[RequiredField]
		[Tooltip("The single value to write to (if SwitchReadWrite is false)")]
		public BBParameter<TScriptableSingle> WriteSingle;
		
		
		// Read
		[TextAreaField(3)]
		[Tooltip("The type value to read from (will be used if UseReadType is true)")]
		public BBParameter<T> ReadValue;
		
		[Tooltip("Use ReadSingle instead of ReadValue")]
		public bool UseReadSingle = default;
		
		[Tooltip("The single value to read from (will be used if UseReadType is false)")]
		public BBParameter<TScriptableSingle> ReadSingle;
		
		
		[Tooltip("Switch the value that is being read from and written to")]
		public bool SwitchReadWrite = false;

		
		protected override string info
		{
			get
			{
				var readString = UseReadSingle ? ReadSingle.ToString() : ReadValue.ToString();
				var writeString = WriteSingle.ToString();

				readString = NodeCanvasUtility.ParseStringForInfo(readString);
				writeString = NodeCanvasUtility.ParseStringForInfo(writeString);

				return SwitchReadWrite 
					? $"{readString} = {writeString}" 
					: $"{writeString} = {readString}";
			}
		}


		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnExecute()
		{
			if (!SwitchReadWrite)
			{
				WriteSingle.value.Value = UseReadSingle ? ReadSingle.value.Value : ReadValue.value ;
				EndAction();
				return;
			}

			var readValue = WriteSingle.value.Value;
			
			if (UseReadSingle)
			{
				ReadSingle.value.Value = readValue;
			}
			else
			{
				ReadValue.value = readValue;
			}

			EndAction();
		}

	}
}