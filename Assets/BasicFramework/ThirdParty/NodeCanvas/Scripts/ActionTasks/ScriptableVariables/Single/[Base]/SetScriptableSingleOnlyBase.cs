﻿using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	[Description("Set a ScriptableVariable's value to another ScriptableVariable's value")]
	public abstract class SetScriptableSingleOnlyBase<T, TScriptableSingle> : ActionTask
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[RequiredField]
		[Tooltip("The single value to write to")]
		public BBParameter<TScriptableSingle> WriteSingle;
		
		[RequiredField]
		[Tooltip("The single value to read from")]
		public BBParameter<TScriptableSingle> ReadSingle;
		
		protected override string info => $"{WriteSingle} = {ReadSingle}";


		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnExecute()
		{
			WriteSingle.value.Value = ReadSingle.value.Value;
			EndAction();
		}
	
	}
}