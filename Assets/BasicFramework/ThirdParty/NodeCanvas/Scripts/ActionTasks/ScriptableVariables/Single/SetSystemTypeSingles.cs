﻿using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	
	//***** struct *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/System Types")]
	public class SetBoolSingle : SetScriptableSingleBase<bool, BoolSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/System Types")]
	public class SetIntSingle : SetScriptableSingleBase<int, IntSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Setter/System Types")]
	public class SetFloatSingle : SetScriptableSingleBase<float, FloatSingle>
	{
	}
	
	
	//***** class *****\\
	
	[Category("BasicFramework/Scriptable Variables/Setter/System Types")]
	public class SetStringSingle : SetScriptableSingleBase<string, StringSingle>
	{
	}
}