﻿using BasicFramework.Core.BasicTypes;
using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ActionTasks.ScriptableVariables
{
	[Category("BasicFramework/Transition/Normalized Float Single Transition")]
	[Description("Linearly transitions a normalized float from zero to one (or vice-versa) over specified duration")]
	public class NormalizedFloatSingleTransition : ActionTask 
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[RequiredField]
		[Tooltip("Go from One to Zero or vice-versa")]
		public BBParameter<NormalizedFloatSingle> NormalizedFloatSingle;
		
		[Tooltip("Go from One to Zero or vice-versa")]
		public BBParameter<bool> StartAtOne;
		
		[Tooltip("How long it takes to transition")]
		public BBParameter<float> Duration = new BBParameter<float>(0.5f);
	
		
		protected override string info
		{
			get
			{
				var singleName = NodeCanvasUtility.ParseStringForInfo(NormalizedFloatSingle.ToString());
				var direction = StartAtOne.value ? "<b>one to zero</b>" : "<b>zero to one</b>";
				
				return $"Transition {singleName} from {direction} over {Duration} seconds";
			}
		}
			
		
		
		// ****************************************  METHODS  ****************************************\\

		protected override void OnExecute()
		{
			base.OnExecute();

			NormalizedFloatSingle.value.Value = StartAtOne.value ? NormalizedFloat.One : NormalizedFloat.Zero;
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();
			
			var duration = Duration.value;
			duration = Mathf.Max(0.001f, duration);
			
			var startAtOne = StartAtOne.value;
			var rawProgress = elapsedTime / duration;
			if (startAtOne)
			{
				rawProgress = 1f - rawProgress;
			}

			var value = new NormalizedFloat(rawProgress);
			NormalizedFloatSingle.value.Value = value;

			var compareValue = startAtOne ? NormalizedFloat.Zero : NormalizedFloat.One;
			if (value != compareValue) return;
			EndAction(true);
		}
		
	}
}