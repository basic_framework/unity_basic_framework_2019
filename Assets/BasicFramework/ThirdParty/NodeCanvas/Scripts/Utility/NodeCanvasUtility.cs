﻿using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas
{
	public static class NodeCanvasUtility 
	{
	
		// **************************************** VARIABLES ****************************************\\

		private const int MAX_INFO_STRING_LENGTH = 20;
		private const int MAX_INFO_STRING_SUBSTRING_LENGTH = MAX_INFO_STRING_LENGTH - 3;

		// ****************************************  METHODS  ****************************************\\

		public static string ParseStringForInfo(string value)
		{
			if (value.Length <= MAX_INFO_STRING_LENGTH) return value;
			
			var bolded = value.Contains("<b>");

			if (bolded)
			{
				value = value.Replace("<b>", "");
				value = value.Replace("</b>", "");
			}
			
			if (value.Length > MAX_INFO_STRING_LENGTH)
			{
				value = $"{value.Substring(0, MAX_INFO_STRING_SUBSTRING_LENGTH)}...";
			}

			return !bolded ? value : $"<b>{value}</b>";
		}
		

	}
}