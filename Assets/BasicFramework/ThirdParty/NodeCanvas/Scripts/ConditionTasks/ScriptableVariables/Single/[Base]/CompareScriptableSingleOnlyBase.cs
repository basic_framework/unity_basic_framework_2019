﻿using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableVariables
{
	[Description("Compares the value of one ScriptableVariable to another value of another ScriptableVariable")]
	public abstract class CompareScriptableSingleOnlyBase<T, TScriptableSingle> : ConditionTask
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// Left

		[Tooltip("The single value used on the left")]
		[RequiredField]
		public BBParameter<TScriptableSingle> LeftSingle;
		
		
		// Right

		[Tooltip("The single value used on the right")]
		[RequiredField]
		public BBParameter<TScriptableSingle> RightSingle;
		
		
		protected override string info => 
			$"{NodeCanvasUtility.ParseStringForInfo(LeftSingle.ToString())} " + 
			$"== {NodeCanvasUtility.ParseStringForInfo(RightSingle.ToString())}";
		

		// ****************************************  METHODS  ****************************************\\
	
		protected override bool OnCheck()
		{
			return LeftSingle.value.CompareToValue(RightSingle.value.Value);
		}

	}
}