﻿using BasicFramework.Core.ScriptableVariables.Single;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableVariables
{
	[Description("Compares a ScriptableVariables value to another value of the same type")]
	public abstract class CompareScriptableSingleBase<T, TScriptableSingle> : ConditionTask
		where TScriptableSingle : ScriptableSingleBase<T>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		// Left
		[Tooltip("The single value used on the left")]
		[RequiredField]
		public BBParameter<TScriptableSingle> LeftSingle;
		
		
		// Right
		[Tooltip("The type value used on the right")]
		public BBParameter<T> RightValue;
		
		[Space]
		[Tooltip("Use RightSingle instead of the RightValue")]
		public bool UseRightSingle = default;
		
		[Tooltip("The single value used on the right")]
		public BBParameter<TScriptableSingle> RightSingle;
		
		
		protected override string info
		{
			get
			{
				var leftString = LeftSingle.ToString();
				var rightString = UseRightSingle ? RightSingle.ToString() : RightValue.ToString();

				leftString = NodeCanvasUtility.ParseStringForInfo(leftString);
				rightString = NodeCanvasUtility.ParseStringForInfo(rightString);
				
				return $"{leftString} == {rightString}";
			}
		}
	
		// ****************************************  METHODS  ****************************************\\

		protected override bool OnCheck()
		{
			var rightValue = UseRightSingle ? RightSingle.value.Value : RightValue.value;

			return LeftSingle.value.CompareToValue(rightValue);
		}
	}
}