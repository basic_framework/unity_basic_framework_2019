﻿//using BasicFramework.Core.BasicAnimation.Transitioner;
//using BasicFramework.Core.BasicTypes;
//using BasicFramework.Core.Interactions.Teleport;
//using BasicFramework.Core.ScriptableEvents;
//using BasicFramework.Core.ScriptableVariables.Single;
//using ParadoxNotion.Design;
//
//namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableVariables
//{
//	
//	//***** struct *****\\
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareBasicDateTimeSingle : CompareScriptableSingleOnlyBase<BasicDateTime, BasicDateTimeSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareBasicPoseSingle : CompareScriptableSingleOnlyBase<BasicPose, BasicPoseSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareFloatRangeSingle : CompareScriptableSingleOnlyBase<FloatRange, FloatRangeSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareIntRangeSingle : CompareScriptableSingleOnlyBase<IntRange, IntRangeSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareNormalizedFloatSingle : CompareScriptableSingleOnlyBase<NormalizedFloat, NormalizedFloatSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareRangedFloatSingle : CompareScriptableSingleOnlyBase<RangedFloat, RangedFloatSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareRangedIntSingle : CompareScriptableSingleOnlyBase<RangedInt, RangedIntSingle>
//	{
//	}
//	
//	
//	//***** class *****\\
//	
//	
//	//***** Object *****\\
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareScriptableEventSingle : CompareScriptableSingleBase<ScriptableEvent, ScriptableEventSingle>
//	{
//	}
//	
//	
//	//***** Enums *****\\
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareTransitionStatesSingle : CompareScriptableSingleBase<TransitionStates, TransitionStatesSingle>
//	{
//	}
//	
//	[Category("BasicFramework/Scriptable Variables/Compare/Basic Types")]
//	public class CompareTeleportStatesSingle : CompareScriptableSingleBase<TeleportStates, TeleportStatesSingle>
//	{
//	}
//	
//}