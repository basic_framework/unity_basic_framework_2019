﻿using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableVariables
{
	
	//***** struct *****\\
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareColorSingle : CompareScriptableSingleBase<Color, ColorSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareKeyCodeSingle : CompareScriptableSingleBase<KeyCode, KeyCodeSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareLayerMaskSingle : CompareScriptableSingleBase<LayerMask, LayerMaskSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareQuaternionSingle : CompareScriptableSingleBase<Quaternion, QuaternionSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareVector2Single : CompareScriptableSingleBase<Vector2, Vector2Single>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareVector3Single : CompareScriptableSingleBase<Vector3, Vector3Single>
	{
	}
	
	
	//***** class *****\\
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareAnimationCurveSingle : CompareScriptableSingleBase<AnimationCurve, AnimationCurveSingle>
	{
	}
	
	
	//***** Object *****\\
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareCameraSingle : CompareScriptableSingleBase<Camera, CameraSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareGameObjectSingle : CompareScriptableSingleBase<GameObject, GameObjectSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareMaterialSingle : CompareScriptableSingleBase<Material, MaterialSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareRenderTextureSingle : CompareScriptableSingleBase<RenderTexture, RenderTextureSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/Unity Types")]
	public class CompareTransformSingle : CompareScriptableSingleBase<Transform, TransformSingle>
	{
	}
	
}