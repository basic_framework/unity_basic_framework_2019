﻿using BasicFramework.Core.ScriptableVariables.Single;
using ParadoxNotion.Design;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableVariables
{
	
	//***** struct *****\\
	
	[Category("BasicFramework/Scriptable Variables/Compare/System Types")]
	public class CompareBoolSingle : CompareScriptableSingleBase<bool, BoolSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/System Types")]
	public class CompareIntSingle : CompareScriptableSingleBase<int, IntSingle>
	{
	}
	
	[Category("BasicFramework/Scriptable Variables/Compare/System Types")]
	public class CompareFloatSingle : CompareScriptableSingleBase<float, FloatSingle>
	{
	}
	
	
	//***** class *****\\
	
	[Category("BasicFramework/Scriptable Variables/Compare/System Types")]
	public class CompareStringSingle : CompareScriptableSingleBase<string, StringSingle>
	{
	}
	
}