﻿using BasicFramework.Core.ScriptableInputs;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableInputs
{
	[Description("Checks to see if the input value is true")]
	public abstract class CheckBoolScriptableInputBase<TInputSource, TScriptableInput> : ConditionTask
		where TInputSource : InputSourceBase<bool>
		where TScriptableInput : InputTypeBase<bool, TInputSource> 
	{
	
		// **************************************** VARIABLES ****************************************\\
		
		[RequiredField]
		[Tooltip("The ScriptableInput whose value will be compared")]
		public BBParameter<TScriptableInput> ScriptableInput;
		
		protected override string info => $"{NodeCanvasUtility.ParseStringForInfo(ScriptableInput.ToString())} == TRUE";
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override bool OnCheck()
		{
			return ScriptableInput.value.Value;
		}
		
	}
}