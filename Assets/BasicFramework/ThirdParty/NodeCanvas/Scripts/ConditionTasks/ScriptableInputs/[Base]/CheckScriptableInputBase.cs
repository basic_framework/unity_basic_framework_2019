﻿using BasicFramework.Core.ScriptableInputs;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableInputs
{
	[Description("Checks if the input's value is equal to the desired value")]
	public abstract class CheckScriptableInputBase<T, TInputSource, TScriptableInput> : ConditionTask
		where TInputSource : InputSourceBase<T>
		where TScriptableInput : InputTypeBase<T, TInputSource>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
//		[Tooltip("Should we still do the check if the input is disabled?")]
//		public BBParameter<bool> CheckIfInputDisabled;
		
		[RequiredField]
		[Tooltip("The ScriptableInput whose value will be compared")]
		public BBParameter<TScriptableInput> ScriptableInput;
		
		[Tooltip("The value that will be compared against")]
		public BBParameter<T> CompareValue;
		
		protected override string info =>
			$"{NodeCanvasUtility.ParseStringForInfo(ScriptableInput.ToString())} " +
			$"== {NodeCanvasUtility.ParseStringForInfo(CompareValue.ToString())}";


		// ****************************************  METHODS  ****************************************\\

		protected override bool OnCheck()
		{
			//if (!CheckIfInputDisabled.value && !ScriptableInput.value.Enabled) return false;
			return Equals(ScriptableInput.value.Value, CompareValue.value);
		}
	}
}