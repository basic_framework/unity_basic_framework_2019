﻿using BasicFramework.Core.ScriptableInputs;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableInputs
{

	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckBoolInput : CheckBoolScriptableInputBase<BoolInputSource, BoolInput>
	{
	}
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckActionInput : CheckBoolScriptableInputBase<ActionInputSource, ActionInput>
	{
	}
	
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckFloatInput : CheckScriptableInputBase<float, FloatInputSource, FloatInput>
	{
	}
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckAxisInput : CheckScriptableInputBase<float, FloatInputSource, AxisInput>
	{
	}
	
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckVector2FloatInput : CheckScriptableInputBase<Vector2, Vector2InputSource, Vector2Input>
	{
	}
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckAxis2Input : CheckScriptableInputBase<Vector2, Vector2InputSource, Axis2Input>
	{
	}
	
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckVector3FloatInput : CheckScriptableInputBase<Vector3, Vector3InputSource, Vector3Input>
	{
	}
	
	[Category("BasicFramework/Scriptable Inputs")]
	public class CheckAxis3Input : CheckScriptableInputBase<Vector3, Vector3InputSource, Axis3Input>
	{
	}
}