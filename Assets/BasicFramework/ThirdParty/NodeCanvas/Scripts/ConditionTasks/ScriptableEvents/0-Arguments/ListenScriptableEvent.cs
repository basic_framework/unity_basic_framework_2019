﻿using BasicFramework.Core.ScriptableEvents;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableEvents
{
	[Description("Listen's for the referenced ScriptableEvent")]
	[Category("BasicFramework/Scriptable Events")]
	public class ListenScriptableEvent : ConditionTask
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[RequiredField]
		[Tooltip("The ScriptableEvent that will be listened to")]
		public BBParameter<ScriptableEvent> ScriptableEvent;

		[Tooltip("Should the check return true only on the frame the event was received")]
		public BBParameter<bool> ResetReceiveFlagOnCheck = true;
		
		protected override string info => $"{NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString())} Raised";

		private bool _receivedFlag;
		
		
	
		// ****************************************  METHODS  ****************************************\\

		protected override void OnEnable()
		{
			base.OnEnable();
			
			ScriptableEvent.value.Subscribe(OnEventRaised);
		}
		
		protected override void OnDisable()
		{
			base.OnDisable();

			_receivedFlag = false;
			ScriptableEvent.value.Unsubscribe(OnEventRaised);
		}

		protected override bool OnCheck()
		{
			if (!_receivedFlag) return false;

			if (ResetReceiveFlagOnCheck.value)
			{
				_receivedFlag = false;
			}
			
			return true;
		}
		
		private void OnEventRaised()
		{
			_receivedFlag = true;
		}
		
	}
}