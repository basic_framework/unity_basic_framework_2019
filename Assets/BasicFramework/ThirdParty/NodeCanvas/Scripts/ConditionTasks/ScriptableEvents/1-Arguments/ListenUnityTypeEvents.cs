﻿using BasicFramework.Core.ScriptableEvents;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableEvents
{
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenColorEvent : ListenScriptableEventBase<Color, ColorEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenQuaternionEvent : ListenScriptableEventBase<Quaternion, QuaternionEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenVector2Event : ListenScriptableEventBase<Vector2, Vector2Event>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenVector3Event : ListenScriptableEventBase<Vector3, Vector3Event>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenGameObjectEvent : ListenScriptableEventBase<GameObject, GameObjectEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenRectTransformEvent : ListenScriptableEventBase<RectTransform, RectTransformEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/Unity Types")]
	public class ListenTransformEvent : ListenScriptableEventBase<Transform, TransformEvent>
	{
	}
}