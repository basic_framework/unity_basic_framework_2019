﻿using BasicFramework.Core.ScriptableEvents;
using ParadoxNotion.Design;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableEvents
{
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class ListenBoolEvent : ListenScriptableEventBase<bool, BoolEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class ListenIntEvent : ListenScriptableEventBase<int, IntEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class ListenFloatEvent : ListenScriptableEventBase<float, FloatEvent>
	{
	}
	
	[Category("BasicFramework/Scriptable Events/01-Argument/System Types")]
	public class ListenStringEvent : ListenScriptableEventBase<string, StringEvent>
	{
	}
	
}