﻿using BasicFramework.Core.ScriptableEvents;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace BasicFramework.ThirdParty.NodeCanvas.ConditionTasks.ScriptableEvents
{
	[Description("Listen's for the referenced ScriptableEvent")]
	public abstract class ListenScriptableEventBase<T0, TScriptableEvent> : ConditionTask
		where TScriptableEvent : ScriptableEventBase<T0>
	{
	
		// **************************************** VARIABLES ****************************************\\
	
		[RequiredField]
		[Tooltip("The ScriptableEvent that will be listened to")]
		public BBParameter<TScriptableEvent> ScriptableEvent;
		
		[Tooltip("Should the check return true only on the frame the event was received")]
		public BBParameter<bool> ResetReceiveFlagOnCheck = true;
		
		[Tooltip("Does the event argument need to be equal to the compare value")]
		public BBParameter<bool> EqualCompareValueRequired = false;

		public BBParameter<T0> CompareValue;
		
		protected override string info =>
			!EqualCompareValueRequired.value 
				? $"{NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString())} Raised" 
				: $"{NodeCanvasUtility.ParseStringForInfo(ScriptableEvent.ToString())} Raised " +
				  $"&& Arg0 == {NodeCanvasUtility.ParseStringForInfo(CompareValue.value.ToString())}";

		private bool _receivedFlag;
		
	
		// ****************************************  METHODS  ****************************************\\
	
		protected override void OnEnable()
		{
			base.OnEnable();
			
			ScriptableEvent.value.Subscribe(OnEventRaised);
		}
		
		protected override void OnDisable()
		{
			base.OnDisable();

			_receivedFlag = false;
			ScriptableEvent.value.Unsubscribe(OnEventRaised);
		}

		protected override bool OnCheck()
		{
			if (!_receivedFlag) return false;

			if (ResetReceiveFlagOnCheck.value)
			{
				_receivedFlag = false;
			}
			
			return true;
		}
		
		private void OnEventRaised(T0 arg0)
		{
			if (EqualCompareValueRequired.value && !Equals(arg0, CompareValue.value)) return;

			_receivedFlag = true;
		}
	
	}
}