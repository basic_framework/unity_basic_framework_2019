# Basic Framework 2019

__NOTE: This project is no longer being maintained by the owner__

__Requires Unity Version 2019.3.1 or later__


What is it

The only REQUIRED package is the __Core__ package

Any __Examples__ folder can be deleted


Any folder with square brackets ( example \[DependantFolder\] ) can not be deleted

Any folder with curved brackets ( example \(NonDependantFolder\) ) can safely be deleted 

## Overview
This framework contains a collection of packages useful for application/game development within unity.

## License
- MIT
 
